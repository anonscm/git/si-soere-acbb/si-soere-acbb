function graphicVariableParcelleChartExtender() {
    var plot = this.cfg;
    $.jqplot.preDrawHooks.push(resetXAxe);
    plot.seriesDefaults={
        lineWidth:2,
        markerOptions:{
                        shadow: false,
                        size: 8,
                        style: 'circle'}
    }
    console.dir(plot);
}
