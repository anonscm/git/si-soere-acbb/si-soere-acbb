/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function days_between(date1, date2) {
    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;
    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms);
    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY);
}
function resetXAxe() {
    date1 = new Date(this.axes.xaxis.min);
    date2 = new Date(this.axes.xaxis.max);
    var noDays = days_between(date1, date2);
    if (noDays > 200) {
        format = '%m/%Y';
    } else if (noDays > 2) {
        format = '%d/%m/%Y';
    } else{
        format = '%R';
    }
    tickOptions = {
        formatString : format,
        formatter : $.jqplot.DateTickFormatter/*,
        angle:-30*/
    }
    this.axes.xaxis.tickOptions = tickOptions;
    console.log("days_between = " + noDays + " format : " + format);
}

function graphicVariableChartExtender() {
    var plot = this.cfg;
    $.jqplot.preDrawHooks.push(resetXAxe);
    plot.seriesDefaults={
        lineWidth:2,
        markerOptions:{
                        shadow: false,
                        size: 8,
                        style: 'circle'}
    }
    console.dir(plot);
}
