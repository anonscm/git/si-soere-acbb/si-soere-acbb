/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.ws.synthesisws;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.inra.ecoinfo.identification.ws.subservice.ISubService;
import org.inra.ecoinfo.ws.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IService extends ISubService {

    Response getRepartition(@Context HttpServletResponse response, @QueryParam(value = "datatype") String datatype, @QueryParam(value = "agroecosysteme") String agroecosysteme, @QueryParam(value = "site") String site) throws BusinessException;

    Response getSynthesisNodes(@Context HttpServletResponse response) throws BusinessException;

}
