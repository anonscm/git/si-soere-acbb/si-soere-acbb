/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.ws.synthesisws;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Properties;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
@XmlRootElement(name = "traitement")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
class SynthesisNode {
    LocalDateTime mindate;
    LocalDateTime maxdate;
    String agroecosystemeCode;
    String agroecosysteme;
    String siteCode;
    String site;
    String datatype;
    String datatypeName;
    String dvu;
    String variableCode;
    String variableAffichage;
    String variableName;
    String uniteName;
    String uniteCode;
    private static Properties agroecosystemProperties;
    private static Properties siteProperties;
    private static Properties variableProperties;
    private static Properties uniteCodeProperties;
    private static Properties uniteNameProperties;
    private static Properties datatypeProperties;

    public SynthesisNode(ILocalizationManager localizationManager, Object[] s) {
        this.mindate = ((Timestamp)s[0]).toLocalDateTime();
        this.maxdate = ((Timestamp)s[1]).toLocalDateTime();
        String[] nodes = ((String) s[2]).split(",");
        this.agroecosystemeCode = nodes[0];
        this.siteCode = nodes[1];
        this.agroecosysteme = agroecosystemProperties.getProperty(nodes[0], nodes[0]);
        this.site = siteProperties.getProperty(nodes[1], nodes[1]);
        this.datatype = (String) s[3];
        this.dvu = (String) s[4];
        this.variableAffichage = (String) s[5];
        this.variableCode = (String) s[6];
        this.variableName = variableProperties.getProperty((String) s[6],(String) s[6]);
        this.uniteName = uniteNameProperties.getProperty((String) s[7],(String) s[7]);
        this.uniteCode = uniteCodeProperties.getProperty((String) s[8],(String) s[8]);
        this.datatypeName = datatypeProperties.getProperty((String) s[9],(String) s[9]);
    }

    public LocalDateTime getMindate() {
        return mindate;
    }

    public String getMindateString() {
        return DateUtil.getUTCDateTextFromLocalDateTime(mindate, DateUtil.DD_MM_YYYY);
    }

    public void setMindate(LocalDateTime mindate) {
        this.mindate = mindate;
    }

    public LocalDateTime getMaxdate() {
        return maxdate;
    }

    public String getMaxdateString() {
        return DateUtil.getUTCDateTextFromLocalDateTime(maxdate, DateUtil.DD_MM_YYYY);
    }

    public void setMaxdate(LocalDateTime maxdate) {
        this.maxdate = maxdate;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getAgroecosysteme() {
        return agroecosysteme;
    }

    public void setAgroecosysteme(String agroecosysteme) {
        this.agroecosysteme = agroecosysteme;
    }

    public String getDvu() {
        return dvu;
    }

    public void setDvu(String dvu) {
        this.dvu = dvu;
    }

    public String getVariableAffichage() {
        return variableAffichage;
    }

    public void setVariableAffichage(String variableAffichage) {
        this.variableAffichage = variableAffichage;
    }

    public String getVariableCode() {
        return variableCode;
    }

    public void setVariableCode(String variableCode) {
        this.variableCode = variableCode;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getUniteName() {
        return uniteName;
    }

    public void setUniteName(String uniteName) {
        this.uniteName = uniteName;
    }

    public String getUniteCode() {
        return uniteCode;
    }

    public void setUniteCode(String uniteCode) {
        this.uniteCode = uniteCode;
    }

    public static Properties getAgroecosystemProperties() {
        return agroecosystemProperties;
    }

    public static void setAgroecosystemProperties(Properties agroecosystemProperties) {
        SynthesisNode.agroecosystemProperties = agroecosystemProperties;
    }

    public String getDatatypeName() {
        return datatypeName;
    }

    public void setDatatypeName(String datatypeName) {
        this.datatypeName = datatypeName;
    }

    public String getAgroecosystemeCode() {
        return agroecosystemeCode;
    }

    public void setAgroecosystemeCode(String agroecosystemeCode) {
        this.agroecosystemeCode = agroecosystemeCode;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }
    public static void initLocalisation(ILocalizationManager localizationManager){
        SynthesisNode.agroecosystemProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME);
        SynthesisNode.siteProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        SynthesisNode.uniteCodeProperties = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_CODE);
        SynthesisNode.uniteNameProperties = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME);
        SynthesisNode.variableProperties = localizationManager.newProperties("variableacbb", VariableACBB.ENTITE_COLUMN_NAME);
        SynthesisNode.datatypeProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(DataType.class), Nodeable.ENTITE_COLUMN_NAME);
    }
}
