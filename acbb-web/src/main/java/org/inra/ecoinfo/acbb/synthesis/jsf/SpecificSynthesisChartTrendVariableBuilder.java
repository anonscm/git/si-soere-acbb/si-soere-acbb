/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.jsf;

import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.synthesis.jsf.ISynthesisChartTrenderVariableBuilder;
import org.inra.ecoinfo.synthesis.jsf.ItemDatatableVariableSynthesis;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.model.chart.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ptchernia
 */
public class SpecificSynthesisChartTrendVariableBuilder implements ISynthesisChartTrenderVariableBuilder {

    ISynthesisManager synthesisManager;
    String language = Localization.getDefaultLocalisation();


    @Override
    public <SV extends GenericSynthesisValue> ChartModel buildModel(ItemDatatableVariableSynthesis item, Class<SV> synthesisValueClass, String language) throws BusinessException {
        this.language = language;
        LineChartModel dateModel = new LineChartModel();
        Map<String, LineChartSeries> seriesMap = new HashMap();
        Map<String, Float> seriesMapNumber = new HashMap();
        LineChartSeries series = new LineChartSeries();
        final String variable = item.getVariable().getCode();
        final LocalDateTime dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, item.getDebut());
        final LocalDateTime dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, item.getFin());
        final Stream<GenericSynthesisValue> synthesisValueStream = synthesisManager.getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(dateDebut, dateFin, item.getVariableNode(), (Class<GenericSynthesisValue>) synthesisValueClass, Stream.of(Activities.extraction).collect(Collectors.toList()));
        DateAxis axis = new DateAxis();
        Duration duration = Duration.between(dateDebut, dateFin);
        duration = duration.dividedBy(20);
        axis.setMin(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(dateDebut.minus(duration), "yyyy-MM-dd", Locale.forLanguageTag(language)));
        axis.setMax(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(dateFin.plus(duration), "yyyy-MM-dd", Locale.forLanguageTag(language)));
        dateModel.getAxes().put(AxisType.X, axis);
        dateModel.setZoom(true);
        dateModel.setLegendPosition("n");
        Collector<SynthesisValueWithParcelle, TreeMap<LocalDateTime, Average>, LineChartSeries> collector = getCollector();
        synthesisValueStream
                .map(gsv -> (SynthesisValueWithParcelle) gsv)
                .collect(Collectors.groupingBy(gvs -> gvs.getParcelleCode(), collector))
                .values().stream()
                .sorted(Comparator.comparing(l -> Integer.parseInt(l.getLabel())))
                .forEach(line -> dateModel.addSeries(line));
        dateModel.setExtender("graphicVariableParcelleChartExtender");
        dateModel.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        dateModel.setLegendRows(1);
        return dateModel;
    }

    public void setSynthesisManager(ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }

    private Collector<SynthesisValueWithParcelle, TreeMap<LocalDateTime, Average>, LineChartSeries> getCollector() {
        Collector<SynthesisValueWithParcelle, TreeMap<LocalDateTime, Average>, LineChartSeries> collector =
                Collector.of(TreeMap::new,
                        this::addValue,
                        this::mergeMap,
                        this::convertMap,
                        Collector.Characteristics.CONCURRENT
                );
        return collector;
    }

    private void addValue(TreeMap<LocalDateTime, Average> map, SynthesisValueWithParcelle sv) {
        map
                .compute(sv.getDate(),
                        (k1, v1) -> {
                            if (v1 == null) {
                                return new Average(k1, sv.getParcelleCode(), sv.getValueFloat());
                            } else {
                                v1.add(sv.getValueFloat());
                                return null;
                            }
                        }
                );
    }

    <T extends SortedMap<LocalDateTime, Average>> T mergeMap(T m1, T m2) {
        m2.entrySet().parallelStream().forEach((u) -> {
            if (!m1.containsKey(u.getKey())) {
                m1.put(u.getKey(), u.getValue());
            } else {
                m1.get(u.getKey()).add(u.getValue());
            }
        });
        return m1;
    }

    LineChartSeries convertMap(SortedMap<LocalDateTime, Average> map) {
        LineChartSeries line = map.values().stream().findFirst().map(a -> new LineChartSeries(a.parcelleCode)).orElseGet(LineChartSeries::new);
        map
                .forEach((date, average) -> {
                    line.set(
                            DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(date, "yyyy-MM-dd", Locale.forLanguageTag(this.language)),
                            average.getAverage()
                    );
                });
        return line;
    }

    public class Average {

        String parcelleCode;
        LocalDateTime date;
        Double sum = null;
        int counter = 0;

        public Average(LocalDateTime date, String parcelleCode) {
            this.parcelleCode = parcelleCode.replaceAll("[^\\d]", "");
            this.date = date;
        }

        public Average(LocalDateTime date, String parcelleCode, Double d) {
            this.parcelleCode = parcelleCode.replaceAll("[^\\d]", "");
            this.date = date;
            this.sum = d;
            this.counter = 1;
        }

        public Average(LocalDateTime date, String parcelleCode, Float f) {
            this.parcelleCode = parcelleCode.replaceAll("[^\\d]", "");
            this.date = date;
            this.sum = f.doubleValue();
            this.counter = 1;
        }

        void add(Double d) {
            sum += d;
            counter++;
        }

        void add(Float f) {
            add(f.doubleValue());
        }

        void add(Average a) {
            this.counter += a.counter;
            this.sum += a.sum;
        }

        Double getAverage() {
            return sum / counter;
        }
    }
}
