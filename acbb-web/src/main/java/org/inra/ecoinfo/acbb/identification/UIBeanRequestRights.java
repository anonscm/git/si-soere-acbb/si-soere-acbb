package org.inra.ecoinfo.acbb.identification;

import com.google.common.base.Strings;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.acbb.identification.entity.RightsRequest;
import org.inra.ecoinfo.acbb.identification.entity.Sites;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.jsf.AbstractUIBeanRequestRights;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.primefaces.context.PrimeFacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import java.io.Serializable;
import java.util.*;

/**
 * @author ptcherniati
 */
@ManagedBean(name = "uiRequestRightsACBB")
@ViewScoped
public class UIBeanRequestRights extends AbstractUIBeanRequestRights<RightRequestACBBVO, RightsRequest> implements Serializable {

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(UIBeanRequestRights.class);
    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{siteDAO}")
    ISiteDAO siteDAO;
    List<SelectItem> sites = new LinkedList<>();
    RightsRequest rightRequest = new RightsRequest();

    /**
     *
     */
    public UIBeanRequestRights() {
    }

    /**
     * @return
     */
    @Override
    public String navigate() {
        return "requestRights";
    }

    /**
     * @return
     */
    @Override
    public String userNavigate() {
        return "requestUsersRights";
    }

    /**
     * @return
     */
    public String getRequiredData() {
        return rightRequest.getRequiredData();
    }

    /**
     * @param dbRequest
     * @return
     */
    @Override
    protected RightRequestACBBVO newRequestVO(RightsRequest dbRequest) {
        return new RightRequestACBBVO(dbRequest);
    }

    /**
     * @return
     */
    @Override
    protected RightRequestACBBVO getRequestVOInstance() {
        return new RightRequestACBBVO().getInstance();
    }

    /**
     * @return
     */
    public String getEmail() {
        if (Strings.isNullOrEmpty(getRequest()._request.getEmail())) {
            getRequest().setUtilisateur((Utilisateur) policyManager.getCurrentUser());
        }
        return getRequest()._request.getEmail();
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        getRequest()._request.setEmail(email);
    }

    /**
     * @return
     */
    public String getNom() {
        if (Strings.isNullOrEmpty(getRequest()._request.getNom())) {
            getRequest().setUtilisateur((Utilisateur) policyManager.getCurrentUser());
        }
        return getRequest()._request.getNom();
    }

    /**
     * @param nom
     */
    public void setNom(String nom) {
        getRequest()._request.setNom(nom);
    }

    /**
     * @return
     */
    public String getPrenom() {
        if (Strings.isNullOrEmpty(getRequest()._request.getPrenom())) {
            getRequest().setUtilisateur((Utilisateur) policyManager.getCurrentUser());
        }
        return getRequest()._request.getPrenom();
    }

    /**
     * @param prenom
     */
    public void setPrenom(String prenom) {
        getRequest()._request.setPrenom(prenom);
    }

    /**
     * @return
     */
    public List<String> getAvailablesSites() {
        List<String> availablesSites = new LinkedList();
        for (Sites site : Sites.values()) {
            availablesSites.add(site.getNom());
        }
        return availablesSites;
    }

    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    public List<SelectItem> getSites() {
        if (!CollectionUtils.isEmpty(sites)) {
            return sites;
        }
        Locale locale = PrimeFacesContext.getCurrentInstance().getViewRoot().getLocale();
        Properties agroecosystemeProperties = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME, locale);
        Properties siteProperties = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME, locale);
        Map<AgroEcosysteme, List<SelectItem>> groups = new HashMap<>();
        siteDAO.getAll().stream()
                .map(s -> (SiteACBB) s)
                .forEach((s) -> {
                    groups
                            .computeIfAbsent(s.getAgroEcosysteme(), k -> new LinkedList<>())
                            .add(
                                    new SelectItem(
                                            s.getCode(),
                                            siteProperties.getProperty(
                                                    s.getCode(), s.getCode()
                                            )
                                    )
                            );
                });
        groups.entrySet().stream()
                .forEach(selectedItemEntry -> {
                    SelectItemGroup selectItemGroup = new SelectItemGroup(agroecosystemeProperties.getProperty(selectedItemEntry.getKey().getCode(), selectedItemEntry.getKey().getCode()));
                    selectItemGroup.setSelectItems(selectedItemEntry.getValue().toArray(new SelectItem[0]));
                    sites.add(selectItemGroup);
                });
        return sites;
    }
}
