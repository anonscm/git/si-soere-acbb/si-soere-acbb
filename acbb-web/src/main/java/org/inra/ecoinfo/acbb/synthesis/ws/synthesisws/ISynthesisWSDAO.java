/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.ws.synthesisws;

import java.util.List;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
interface ISynthesisWSDAO<T> {

     List<SynthesisNode> getSynthesisNodes(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot);

     List<String> getRepartition(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot, String datatype, String agroecosysteme, String site);
     List<String> getChart(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot, String datatype, String agroecosysteme, String site, String variable);
}
