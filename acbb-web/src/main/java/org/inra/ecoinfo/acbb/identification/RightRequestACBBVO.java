/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.identification;

import org.inra.ecoinfo.acbb.identification.entity.RightsRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.jsf.AbstractRightRequestVO;
import org.inra.ecoinfo.identification.jsf.UtilisateurVO;

import java.time.LocalDateTime;

/**
 * @author ptcherniati
 */
public class RightRequestACBBVO extends AbstractRightRequestVO<RightRequestACBBVO, RightsRequest> {
    /**
     *
     */
    protected RightsRequest _request;
    /**
     *
     */
    protected UtilisateurVO utilisateurVO;

    /**
     * @param request
     */
    public RightRequestACBBVO(RightsRequest request) {
        super();
        this._request = request;
        this.utilisateurVO = new UtilisateurVO(request.getUser());
    }

    /**
     *
     */
    public RightRequestACBBVO() {
        super();
    }

    private static RightRequestACBBVO _getInstance() {
        return new RightRequestACBBVO();
    }

    /**
     * @param req
     * @return
     */
    @Override
    public RightRequestACBBVO getInstance(RightsRequest req) {
        RightRequestACBBVO instance = _getInstance();
        instance._request = req;
        return instance;
    }

    /**
     * @return
     */
    @Override
    public RightRequestACBBVO getInstance() {
        RightRequestACBBVO instance = _getInstance();
        instance.utilisateurVO = utilisateurVO;
        instance._request = new RightsRequest();
        return instance;
    }

    /**
     * @return
     */
    @Override
    public Utilisateur getUtilisateur() {
        return _request.getUser();
    }

    /**
     * @param utilisateur
     */
    @Override
    public void setUtilisateur(Utilisateur utilisateur) {
        _request.setUser(utilisateur);
        _request.setNom(utilisateur.getNom());
        _request.setPrenom(utilisateur.getPrenom());
        _request.setEmail(utilisateur.getEmail());
    }

    /**
     * @return
     */
    @Override
    public RightsRequest getRightRequest() {
        return _request;
    }

    /**
     * @return
     */
    @Override
    public LocalDateTime getCreateDate() {
        return _request.getCreateDate();
    }

    /**
     * @return
     */
    @Override
    public boolean isValidated() {
        return _request.isValidated();
    }

    /**
     * @param validated
     */
    @Override
    public void setValidated(boolean validated) {
        _request.setValidated(validated);
    }
}
