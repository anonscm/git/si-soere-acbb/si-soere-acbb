package org.inra.ecoinfo.acbb.synthesis.ws.synthesisws;

//~--- non-JDK imports --------------------------------------------------------
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.streamers.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Antoine Schellenberger
 */
public class Service extends AbstractService implements IService {

    public static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
    public static final String LOGGER_ERROR = "Method %s -> %s";
    private IPolicyManager policyManager;
    private ISynthesisWSDAO synthesisWSDAO;
    private ILocalizationManager localizationManager;
    private INotificationsManager notificationsManager;
    private IVariableACBBDAO variableACBBDAO;
    private IMgaServiceBuilder mgaServiceBuilder;
    private IMgaIOConfigurator configurator;


    @GET
    @Produces(MediaType.APPLICATION_JSON)    
    @Path("synthesisNodes")
    public Response getSynthesisNodes(@Context HttpServletResponse response) throws BusinessException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        Utilisateur currentUser;
        try {
            currentUser = (Utilisateur) policyManager.getCurrentUser();
            List<String> groups = currentUser.getAllGroups().stream()
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
            List<SynthesisNode> traitementNodes = synthesisWSDAO.getSynthesisNodes(localizationManager, groups, currentUser.getIsRoot()
            
            );
            String jsonString = new ObjectMapper().writeValueAsString(traitementNodes);
            return Response.ok().entity(jsonString).build();
        } catch (Exception ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()).concat("\n").concat(ex.getMessage()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)    
    @Path("repartition")
    public Response getRepartition(@Context HttpServletResponse response, 
            @QueryParam("datatype") String datatype,
            @QueryParam("agroecosysteme") String agroecosysteme,
            @QueryParam("site") String site
            ) throws BusinessException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        Utilisateur currentUser;
        try {
            currentUser = (Utilisateur) policyManager.getCurrentUser();
            List<String> groups = currentUser.getAllGroups().stream()
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
            List<String> repartition = synthesisWSDAO.getRepartition(localizationManager, groups, currentUser.getIsRoot(), datatype, agroecosysteme, site);
            String jsonString = new ObjectMapper().writeValueAsString(repartition);
            return Response.ok().entity(jsonString).build();
        } catch (Exception ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()).concat("\n").concat(ex.getMessage()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)    
    @Path("chart")
    public Response getChart(@Context HttpServletResponse response, 
            @QueryParam("datatype") String datatype,
            @QueryParam("agroecosysteme") String agroecosysteme,
            @QueryParam("site") String site,
            @QueryParam("variable") String variable
            ) throws BusinessException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        Utilisateur currentUser;
        try {
            currentUser = (Utilisateur) policyManager.getCurrentUser();
            List<String> groups = currentUser.getAllGroups().stream()
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
            List<String> chart = synthesisWSDAO.getChart(localizationManager, groups, currentUser.getIsRoot(), datatype, agroecosysteme, site, variable);
            String jsonString = new ObjectMapper().writeValueAsString(chart);
            return Response.ok().entity(jsonString).build();
        } catch (Exception ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()).concat("\n").concat(ex.getMessage()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        }
    }

    private Optional<Notification> getLastNotification(Utilisateur user) {
        return notificationsManager.getAllNotifications()
                .stream()
                .filter(n -> n.getUtilisateur().equals(user))
                .findFirst();
    }

    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    public void setSynthesisWSDAO(ISynthesisWSDAO synthesisWSDAO) {
        this.synthesisWSDAO = synthesisWSDAO;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setVariableACBBDAO(IVariableACBBDAO variableACBBDAO) {
        this.variableACBBDAO = variableACBBDAO;
    }

    public void setMgaServiceBuilder(IMgaServiceBuilder mgaServiceBuilder) {
        this.mgaServiceBuilder = mgaServiceBuilder;
    }

    public void setConfigurator(IMgaIOConfigurator configurator) {
        this.configurator = configurator;
    }

    
}


//~ Formatted by Jindent --- http://www.jindent.com
