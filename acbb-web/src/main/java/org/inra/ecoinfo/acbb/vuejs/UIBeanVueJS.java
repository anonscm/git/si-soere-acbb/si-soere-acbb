package org.inra.ecoinfo.acbb.vuejs;

import com.google.common.io.Files;
import com.sun.org.apache.bcel.internal.generic.IUSHR;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.ws.crypto.Digestor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ptcherniati
 */
@ManagedBean(name = "uiVuejs")
@ViewScoped
public class UIBeanVueJS implements Serializable {

    @ManagedProperty(value = "#{coreConfiguration}")
    private ICoreConfiguration configuration;
    @ManagedProperty(value = "#{policyManager}")
    private IPolicyManager policyManager;

    /**
     *
     */
    public UIBeanVueJS() {
    }

    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    public String getPath() throws NoSuchAlgorithmException {
        final String timeStamp = Integer.toString(LocalDateTime.now().toInstant(ZoneOffset.UTC).getNano());
        IUser utilisateur = policyManager.getCurrentUser();
        String signature = Digestor.digestSha1(utilisateur.getLogin() + utilisateur.getPassword() + timeStamp);
        return String.format("/rest/resources/%s/%s/%s/serviceExtractFluxMeteo", utilisateur.getLogin(), signature, timeStamp);
    }

    /**
     * @return
     */
    public String navigate() {
        return "vuejs";
    }
}
