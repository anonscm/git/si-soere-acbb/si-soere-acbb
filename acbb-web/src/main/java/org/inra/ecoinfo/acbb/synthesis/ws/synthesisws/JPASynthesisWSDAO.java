package org.inra.ecoinfo.acbb.synthesis.ws.synthesisws;

//~--- non-JDK imports --------------------------------------------------------
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author Antoine Schellenberger
 */
public class JPASynthesisWSDAO extends AbstractJPADAO<Object> implements ISynthesisWSDAO<Object> {

    @PersistenceContext
    private EntityManager em;

    /**
     *
     */
    public JPASynthesisWSDAO() {
    }

    @Override
    @Transactional
    public List<SynthesisNode> getSynthesisNodes(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot) {
        SynthesisNode.initLocalisation(localizationManager);
        String request = "select distinct min(date) over (partition by \"datatype\",site,variable) minDate,  max(date) over (partition by \"datatype\",site,variable) maxDate,\n"
                + "site, datatype,variable dvu ,v.affichage variableAffichage, cnv.code variableName, unite.\"name\" uniteName, unite.code unitecode,\n"
                + "dty.name datatypename\n"
                + "from aggregatesynthesis\n"
                + "join composite_nodeable cndvu on cndvu.code=variable\n"
                + "join datatype_variable_unite_acbb_dvu dvu ON dvu.vdt_id = cndvu.id\n"
                + "join variable v using (var_id)\n"
                + "join unite using(uni_id)\n"
                + "JOIN composite_nodeable cnv on cnv.nodeable_id=var_id\n"
                + "join composite_nodeable cnd on cnd.code=\"datatype\"\n"
                + "join datatype dty ON dty.dty_id = cnd.id\n"
                + "where %s or (array[%s]::::text[] && rights::::text[])";
        String groupsSql = groups.stream()
                .distinct()
                .collect(Collectors.joining("','", "'", "'"));
        request = String.format(request, isRoot, groupsSql);
        return (List<SynthesisNode>) entityManager.createNativeQuery(request).getResultStream()
                .map(s -> new SynthesisNode(localizationManager, (Object[]) s))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<String> getChart(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot, String datatype, String agroecosysteme, String site, String variable) {
        SynthesisNode.initLocalisation(localizationManager);
        String request = "with color as (select ARRAY['red','blue','yellow','green','purple','brown','pink','cyan','orange','fushia','black','indigo','charteuse'] color),\n"
                + "index as (\n"
                + "select unnest(color) color from color),\n"
                + "indexed_colors as (select row_number() over () r, color from  \"index\"),\n"
                + "synth as (\n"
                + "select row_number() over () r,\"datatype\", site,variable,complement,\"complementType\", "
                + "jsonb_build_object('label',complement,'data' ,array_agg(\"data\" order by to_date(data->>'x', 'dd-mm-yyyy')),'fill',false,'borderColor','red') dataset \n"
                + "from aggregatesynthesis\n"
                + "where (%s or (array[%s]::::text[] && rights::::text[]))\n"
                + "and \"datatype\"='%s' and site='%s,%s'\n"
                + "and variable = '%s'\n"
                + "group by \"datatype\", site, complement,\"complementType\", variable\n"
                + "order by \"datatype\", site, complement,\"complementType\", variable)\n"
                + "select jsonb_build_object(\n"
                + "	'datatype', \"datatype\", 'site', site, \n"
                + "	'complement', complement,\"complementType\",\"complementType\",\n"
                + "	'variable', variable, \n"
                + "	'dataset',jsonb_set(dataset,'{borderColor}', ('\"'||color||'\"')::::jsonb))::::text from synth \n"
                + "join indexed_colors using(r)";

        String groupsSql = groups.stream()
                .distinct()
                .collect(Collectors.joining("','", "'", "'"));
        request = String.format(request, isRoot, groupsSql, datatype, agroecosysteme, site, variable);
        System.out.println(request);
        return (List<String>) entityManager.createNativeQuery(request).getResultStream()
                .map(s -> (String) s)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<String> getRepartition(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot, String datatype, String agroecosysteme, String site) {
        SynthesisNode.initLocalisation(localizationManager);
        String request = "select distinct jsonb_build_object('datatype',\"datatype\", 'site',site,\n"
                + "	'variable',variable,'rangedate',jsonb_agg(distinct rangedate))::::text \n"
                + "	from aggregatesynthesis \n"
                + "join composite_nodeable cnv on cnv.code=variable\n"
                + "where \"datatype\" ='%s' \n"
                + "and (%s or (array[%s]::::text[] && rights::::text[]))\n"
                + "and site ='%s,%s'\n"
                + "group by \"datatype\", site,variable";
        String groupsSql = groups.stream()
                .distinct()
                .collect(Collectors.joining("','", "'", "'"));
        request = String.format(request, datatype, isRoot, groupsSql, agroecosysteme, site);
        return (List<String>) entityManager.createNativeQuery(request).getResultStream()
                .map(
                        s -> (String) s
                )
                .collect(Collectors.toList());
    }
}
