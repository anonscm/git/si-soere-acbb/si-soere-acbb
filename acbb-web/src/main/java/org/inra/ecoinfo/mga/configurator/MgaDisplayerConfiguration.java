package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 * @author ryahiaoui
 */
public class MgaDisplayerConfiguration extends AbstractMgaDisplayerConfiguration {

    /**
     *
     */
    public MgaDisplayerConfiguration() {

        this.addColumnNamesForInstanceType(SiteACBB.class, "PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_LOCALISATION_NAME",
                "PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_LOCALISATION_NAME",
                "PROPERTY_COLUMN_PLATEFORM_NAME", "PROPERTY_COLUMN_PLATEFORM_NAME",
                "PROPERTY_COLUMN_SITE_DEFAULT");

        this.addColumnNamesForInstanceType(Theme.class, "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME",
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME",
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME");

        this.addColumnNamesForInstanceType(DataType.class, "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME",
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME",
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME");

        this.addColumnNamesForInstanceType(VariableACBB.class, "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "Variableacbb_3", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME");

        this.addColumnNamesForInstanceType(DatatypeVariableUniteACBB.class, "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME");

        this.addColumnNamesForInstanceType(Refdata.class, "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME",
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME",
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME");

        this.addColumnNamesForInstanceType(AgroEcosysteme.class, "PROPERTY_COLUMN_AGROECOSYSTEME_NAME", "PROPERTY_COLUMN_AGROECOSYSTEME_NAME",
                "PROPERTY_COLUMN_AGROECOSYSTEME_NAME", "PROPERTY_COLUMN_AGROECOSYSTEME_NAME",
                "PROPERTY_COLUMN_AGROECOSYSTEME_NAME", "PROPERTY_COLUMN_AGROECOSYSTEME_NAME");
        this.addColumnNamesForInstanceType(Parcelle.class, "PROPERTY_COLUMN_PARCELLE_NAME", "PROPERTY_COLUMN_PARCELLE_NAME",
                "PROPERTY_COLUMN_PARCELLE_NAME", "PROPERTY_COLUMN_PARCELLE_NAME",
                "PROPERTY_COLUMN_PARCELLE_NAME", "Parcelle_5");

    }

}
