import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.deserialization.impl.DeserializationFactory;
import org.inra.ecoinfo.serialization.impl.SerializationFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.xml.parsers.ParserConfigurationException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author tcherniatinsky
 */
public class MainTest {

    /**
     *
     */
    @Test
    @Ignore
    public void serialize() {
        SerializationFactory serializationFactory;
        ApplicationContext context
                = new ClassPathXmlApplicationContext("META-INF/spring/applicationContextSerialization.xml");
        serializationFactory = (SerializationFactory) context.getBean("serializationFactory");
        CoreConfiguration configuration = (CoreConfiguration) context.getBean("coreConfiguration");
        serializationFactory.setRepositoryURI(configuration.getRepositoryURI());
        serializationFactory.serialize();
    }

    /**
     *
     */
    @Test
    public void deserialize() throws ParserConfigurationException {
        DeserializationFactory deserializationFactory;
        ApplicationContext context
                = new ClassPathXmlApplicationContext("META-INF/spring/applicationContextDeserialization.xml");
        deserializationFactory = (DeserializationFactory) context.getBean("deserializationFactory");
        CoreConfiguration configuration = (CoreConfiguration) context.getBean("coreConfiguration");
        deserializationFactory.setRepositoryURI(configuration.getRepositoryURI());
        deserializationFactory.deserialize();
    }
}
