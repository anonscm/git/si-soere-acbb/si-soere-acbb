package org.inra.ecoinfo;


/**
 * @author tcherniatinsky
 */

import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.serialization.impl.SerializationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author tcherniatinsky
 */
public class Serialization {
    private static final Logger LOGGER = LoggerFactory.getLogger(Serialization.class);
    SerializationFactory serializationFactory;

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Serialization p = new Serialization();
        p.start();
    }

    private void start() {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "META-INF/spring/applicationContextSerialization.xml");
        LOGGER.debug("startinf serialization");
        serializationFactory = (SerializationFactory) context.getBean("serializationFactory");
        CoreConfiguration configuration = (CoreConfiguration) context.getBean("coreConfiguration");
        serializationFactory.setRepositoryURI(configuration.getRepositoryURI());
        serializationFactory.serialize();
        System.exit(0);
        LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("fin de programme");
    }
}
