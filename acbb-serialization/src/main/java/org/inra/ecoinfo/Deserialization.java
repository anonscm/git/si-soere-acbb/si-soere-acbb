package org.inra.ecoinfo;


/**
 * @author tcherniatinsky
 */

import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.deserialization.impl.ModuleDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

import static org.inra.ecoinfo.deserialization.impl.UtilisateurDeserialization.LOGGER;

/**
 *
 * @author tcherniatinsky
 */
public class Deserialization {

    ModuleDeserialization moduleDeserialization;

    /**
     *
     * @param args
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public static void main(String[] args) {
        Deserialization p = new Deserialization();
        p.start();
    }

    private void start() {
        ApplicationContext context
                = new ClassPathXmlApplicationContext("META-INF/spring/applicationContextDeserialization.xml");
        moduleDeserialization = (ModuleDeserialization) context.getBean("moduleDeserialization");
        IUtilisateurDAO utilisateurDAO = (IUtilisateurDAO) context.getBean("utilisateurDAO");
        IPolicyManager policyManager = (IPolicyManager) context.getBean("policyManager");
        CoreConfiguration configuration = (CoreConfiguration) context.getBean("coreConfiguration");
        moduleDeserialization.setRepositoryURI(configuration.getRepositoryURI());
        try {
            init(policyManager, utilisateurDAO);
        } catch (BusinessException ex) {
            LOGGER.error("can't init locale", ex);
        }
        moduleDeserialization.deSerialize();
        LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("fin de programme");
        System.exit(0);
    }

    /**
     *
     * @param policyManager
     * @param utilisateurDAO
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Transactional(rollbackFor = {PersistenceException.class})
    public void init(IPolicyManager policyManager, IUtilisateurDAO utilisateurDAO) throws BusinessException {
        Utilisateur rootUtilisateur = utilisateurDAO.getAdmins().get(0);
        LOGGER.info("root user {}", rootUtilisateur.getLogin());
        ILocalizationManager localizationManager = (ILocalizationManager) ApplicationContextHolder.getContext().getBean("localizationManager");
        policyManager.setCurrentUser(rootUtilisateur);
        localizationManager.initLocale(Locale.FRANCE);
    }
}
