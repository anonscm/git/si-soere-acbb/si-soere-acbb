/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.mock;

import org.inra.ecoinfo.menu.IMenuManager;
import org.inra.ecoinfo.menu.Menu;

/**
 * @author tcherniatinsky
 */
public class MockMenuManager implements IMenuManager {
    Menu menu;

    /**
     *
     */
    public MockMenuManager() {
        menu = new Menu();
    }

    @Override
    public Menu buildRestrictedMenu() throws CloneNotSupportedException {
        return menu;
    }

    @Override
    public Menu getMenu() {
        return menu;
    }

}
