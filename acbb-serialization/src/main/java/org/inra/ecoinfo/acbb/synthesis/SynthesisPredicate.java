/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.synthesis.ISynthesisPredicate;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.impl.AbstractSynthesisPredicate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author tcherniatinsky
 */
public class SynthesisPredicate extends AbstractSynthesisPredicate {

    private static long tempIndex = -1;

    private static IParcelleDAO parcelleDAO;

    Logger lOGGER = LoggerFactory.getLogger(SynthesisPredicate.class);

    /**
     * @param pathes
     */
    public SynthesisPredicate(SortedMap<String, GenericSynthesisDatatype> pathes) {
        this.synthesisPathes = pathes;
    }

    /**
     *
     */
    public SynthesisPredicate() {
    }

    /**
     * @param pathes
     * @return
     */
    @Override
    public ISynthesisPredicate getInstance(SortedMap<String, GenericSynthesisDatatype> pathes) {
        return new SynthesisPredicate(pathes);
    }

    /**
     * @param parcelleDAO
     */
    public void setParcelleDAO(IParcelleDAO parcelleDAO) {
        this.parcelleDAO = parcelleDAO;
    }

    /**
     * @param node
     * @return
     */
    @Override
    public List<INode> apply(INode node) {
        String theme = node.getRealNode().getParent().getNodeable().getCode();
        if (theme.equals("mesures_physiques_du_sol") || theme.equals("pratiques_de_gestion") || theme.equals("vegetation")) {
            return addParcelle(node);
        } else {
            return nodesForParcelleNode(node);
        }
    }

    private List<INode> nodesForParcelleNode(INode node) {
        String[] splitPath = node.getRealNode().getPath().split(PatternConfigurator.PATH_SEPARATOR);
        if (splitPath.length < 5) {
            return Collections.EMPTY_LIST;
        }
        String pattern = String.format("%s,%s,%s,%s@@%s", splitPath[0], splitPath[1], splitPath[3], splitPath[2], splitPath[4]);
        Pattern p = Pattern.compile(pattern);
        List<INode> nodes = new LinkedList();
        for (Iterator<Map.Entry<String, GenericSynthesisDatatype>> iterator = synthesisPathes.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, GenericSynthesisDatatype> entry = iterator.next();
            Matcher m = p.matcher(entry.getKey());
            if (m.matches()) {
                nodes.add(node);
                final String codeNode = getCodeNode(splitPath[0], splitPath[1], splitPath[2], splitPath[3], splitPath[4]);
                registerNode(entry, node, codeNode);
                iterator.remove();
            }
        }
        return nodes;
    }

    private List<INode> addParcelle(INode node) {
        String[] splitPath = node.getRealNode().getPath().split(PatternConfigurator.PATH_SEPARATOR);
        List<INode> nodes = new LinkedList();
        String pattern = String.format("%s,%s,%s,(%s_.*)@@%s", splitPath[0], splitPath[1], splitPath[2], splitPath[1], splitPath[3]);
        Pattern p = Pattern.compile(pattern);
        for (Iterator<Map.Entry<String, GenericSynthesisDatatype>> iterator = synthesisPathes.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, GenericSynthesisDatatype> entry = iterator.next();
            Matcher m = p.matcher(entry.getKey());
            if (m.matches()) {
                try {
                    String parcelleCode = m.group(1);
                    INodeable parcelleNodeable = parcelleDAO.getByNKey(parcelleCode).orElseThrow(PersistenceException::new);
                    String parcellePath = String.format("%s,%s,%s", splitPath[0], splitPath[1], m.group(1));
                    String themePath = String.format("%s,%s", parcellePath, splitPath[2]);
                    String datatypePath = String.format("%s,%s", themePath, splitPath[3]);
                    Long leafRealNodeId = node.getRealNode().getId();
                    final INode themeNode = node.getParent();
                    RealNode themeRN = themeNode.getRealNode();
                    INode siteNode = themeNode.getParent();
                    RealNode siteRN = siteNode.getRealNode();
                    RealNode parcelleRN = new RealNode(siteRN, null, parcelleNodeable, parcellePath);
                    parcelleRN.setId(leafRealNodeId);
                    NodeDataSet nodeDataSetParcelle = new NodeDataSet((NodeDataSet) siteNode, null);
                    nodeDataSetParcelle.setId(tempIndex--);
                    nodeDataSetParcelle.setRealNode(parcelleRN);
                    nodeDataSetParcelle.setIsLeaf(false);
                    themeRN = new RealNode(parcelleRN, null, themeRN.getNodeable(), themePath);
                    themeRN.setId(leafRealNodeId);
                    NodeDataSet nodeDataSetTheme = new NodeDataSet(nodeDataSetParcelle, null);
                    nodeDataSetTheme.setId(tempIndex--);
                    nodeDataSetTheme.setRealNode(themeRN);
                    nodeDataSetTheme.setIsLeaf(false);
                    RealNode datatypeNode = new RealNode(themeRN, null, node.getNodeable(), datatypePath);
                    datatypeNode.setId(leafRealNodeId);
                    NodeDataSet nodeDataSetDatatype = new NodeDataSet(nodeDataSetTheme, null);
                    nodeDataSetDatatype.setId(tempIndex--);
                    nodeDataSetDatatype.setRealNode(datatypeNode);
                    nodeDataSetDatatype.setIsLeaf(true);
                    nodes.add(nodeDataSetDatatype);
                    String codeNode = getCodeNode(splitPath[0], splitPath[1], parcelleCode, splitPath[2], splitPath[3]);
                    registerNode(entry, nodeDataSetDatatype, codeNode);
                    iterator.remove();
                } catch (PersistenceException ex) {
                    lOGGER.info("can't find parcelle");
                }
            }
        }
        return nodes;
    }

    private String getCodeNode(String agroeEcosysteme, String site, String theme, String datatype, String parcelle) {
        return String.format("%s,%s,%s,%s,%s", agroeEcosysteme, site, theme, datatype, parcelle);
    }
}
