package org.inra.ecoinfo.acbb.mock;

import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author tcherniatinsky
 */
public class MockNotification implements INotificationsManager {

    /**
     *
     */
    public void buildSynthesis() {
    }

    @Override
    public void addNotification(Notification notification, String target) throws BusinessException {
        final Logger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.info(notification.getMessage());
        logger.info(notification.getBody());
    }

    @Override
    public List<Notification> getAllNoArchivedByUserLogin(String userLogin) {
        return null;
    }

    @Override
    public List<Notification> getAllNotifications() {
        return null;
    }

    @Override
    public List<Notification> getAllTransientsNotifications(LocalDateTime date) {
        return null;
    }

    @Override
    public List<Notification> getAllUserNotifications(String userLogin) {
        return null;
    }

    @Override
    public List<Notification> getArchivedNotifications(String login) {
        return null;
    }

    @Override
    public byte[] getAttachment(Long notificationId) throws BusinessException {
        return null;
    }

    @Override
    public byte[] getBody(Long notificationId) {
        return null;
    }

    @Override
    public Optional<Notification> getNotificationById(Long id) {
        return null;
    }

    @Override
    public List<Notification> getTransientsUserNotifications(String login, LocalDateTime date) {
        return null;
    }

    @Override
    public void markAllAsRead(Long utilisateurId) {

    }

    @Override
    public void markNotificationAsArchived(long id) {
    }

    @Override
    public void markNotificationAsNew(Long id) {

    }

    @Override
    public void removeAllNotification(Long id) throws BusinessException {

    }

    @Override
    public void removeNotification(long longValue) throws BusinessException {

    }
}
    
 