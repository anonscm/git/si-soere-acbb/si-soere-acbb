package org.inra.ecoinfo.foret.serviceForet;

import org.hibernate.Session;
import org.inra.ecoinfo.ws.manager.IDto;
import org.inra.ecoinfo.ws.manager.IResource;
import org.inra.ecoinfo.ws.manager.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Antoine Schellenberger
 */
public class Resource extends Manager<Dto> implements IResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(Resource.class);

    /**
     *
     */
    public Resource() {
        super();
    }

    /**
     * @param session
     * @param indexRequest
     * @param limit
     * @return
     */
    @Override
    public List<IDto> getDtoIterable(Session session, int indexRequest, int limit) {

        finish = true;
        List<IDto> inspect = new ArrayList();

        try {

            InputStream configStream = getClass().getResourceAsStream("foret.csv");

            BufferedReader configReader;
            configReader = new BufferedReader(new InputStreamReader(configStream, "UTF-8"));

            String line;
            IDto dto;

            configReader.readLine();

            while ((line = configReader.readLine()) != null) {

                String[] split = line.split("\t");

                dto = new Dto(split[0],
                        split[1],
                        Integer.parseInt(split[2]),
                        Integer.parseInt(split[3]),
                        Integer.parseInt(split[4]),
                        Integer.parseInt(split[5]),
                        Integer.parseInt(split[6]),
                        Integer.parseInt(split[7])
                );

                inspect.add(dto);
            }


        } catch (Exception ex) {
            LOGGER.error(null, ex);
        }
        return inspect;
    }

    /**
     * @param limit
     */
    @Override
    public void initResource(int limit) {
    }

    /**
     * @param offset
     */
    @Override
    public synchronized void setOffset(int offset) {
    }

    /**
     * @return
     */
    @Override
    public boolean isFinished() {
        return finish;
    }


}
