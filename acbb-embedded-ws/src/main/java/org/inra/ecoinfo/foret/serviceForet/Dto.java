package org.inra.ecoinfo.foret.serviceForet;

import org.inra.ecoinfo.ws.manager.IDto;

import javax.xml.bind.annotation.*;

/**
 * @author yahiaoui
 */
@XmlRootElement(name = "Site")
@XmlType(propOrder = {"nomSite", "typeArbre", "qtLitiere",
        "xa", "xw", "xe", "xn", "xh"})
@XmlAccessorType(XmlAccessType.FIELD)

public class Dto implements IDto {

    @XmlElement(name = "nomSite")
    private String nomSite;

    @XmlElement(name = "typeArbre")
    private String typeArbre;

    @XmlElement(name = "qtLitiere")
    private int qtLitiere;

    @XmlElement(name = "Xa")
    private int xa;

    @XmlElement(name = "Xw")
    private int xw;

    @XmlElement(name = "Xe")
    private int xe;

    @XmlElement(name = "Xn")
    private int xn;

    @XmlElement(name = "Xh")
    private int xh;

    /**
     *
     */
    public Dto() {

    }

    /**
     * @param nomSite
     * @param typeArbre
     * @param qtLitiere
     * @param xa
     * @param xw
     * @param xe
     * @param xn
     * @param xh
     */
    public Dto(String nomSite, String typeArbre, int qtLitiere, int xa, int xw, int xe, int xn, int xh) {

        this.nomSite = nomSite;
        this.typeArbre = typeArbre;
        this.qtLitiere = qtLitiere;
        this.xa = xa;
        this.xw = xw;
        this.xe = xe;
        this.xn = xn;
        this.xh = xh;
    }

    /**
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     * @param nomSite
     */
    public void setNomSite(String nomSite) {
        this.nomSite = nomSite;
    }

    /**
     * @return
     */
    public String getTypeArbre() {
        return typeArbre;
    }

    /**
     * @param typeArbre
     */
    public void setTypeArbre(String typeArbre) {
        this.typeArbre = typeArbre;
    }

    /**
     * @return
     */
    public int getQtLitiere() {
        return qtLitiere;
    }

    /**
     * @param qtLitiere
     */
    public void setQtLitiere(int qtLitiere) {
        this.qtLitiere = qtLitiere;
    }

    /**
     * @return
     */
    public int getXa() {
        return xa;
    }

    /**
     * @param xa
     */
    public void setXa(int xa) {
        this.xa = xa;
    }

    /**
     * @return
     */
    public int getXw() {
        return xw;
    }

    /**
     * @param xw
     */
    public void setXw(int xw) {
        this.xw = xw;
    }

    /**
     * @return
     */
    public int getXe() {
        return xe;
    }

    /**
     * @param xe
     */
    public void setXe(int xe) {
        this.xe = xe;
    }

    /**
     * @return
     */
    public int getXn() {
        return xn;
    }

    /**
     * @param xn
     */
    public void setXn(int xn) {
        this.xn = xn;
    }

    /**
     * @return
     */
    public int getXh() {
        return xh;
    }

    /**
     * @param xh
     */
    public void setXh(int xh) {
        this.xh = xh;
    }


}
