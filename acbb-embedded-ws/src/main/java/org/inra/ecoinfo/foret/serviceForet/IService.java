package org.inra.ecoinfo.foret.serviceForet;

import org.inra.ecoinfo.identification.ws.subservice.ISubService;

import javax.ws.rs.core.Response;

/**
 * @author yahiaoui
 */
public interface IService extends ISubService {

    /**
     * @return
     */
    public Response allsitesxml();

    /**
     * @return
     */
    public Response allsitesjson();

}
