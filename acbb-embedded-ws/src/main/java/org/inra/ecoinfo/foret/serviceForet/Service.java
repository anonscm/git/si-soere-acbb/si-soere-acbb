package org.inra.ecoinfo.foret.serviceForet;

import org.inra.ecoinfo.ws.streamers.AbstractService;
import org.inra.ecoinfo.ws.streamers.DefaultStreamerConfigurator;
import org.inra.ecoinfo.ws.streamers.StreamerOutputJson;
import org.inra.ecoinfo.ws.streamers.StreamerOutputXml;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * @author ptcherniati
 */

//curl -i -H "accept: json/plain" "http://localhost:10280/acbb-ws/rest/resources/admin/9e8be9cf0cab6a5bfa030bb5c5c80e13ec344f30/1423666505514/serviceForet"
//curl -H "accept: xml/plain" "http://147.99.222.9:10280/acbb-ws/rest/resources/foretuser/d685f4c572eb5eb96efa09ed0ee3f8bf422ffc/6981698169/serviceForet"


public class Service extends AbstractService implements IService {

    private Resource resource;

    /**
     * @return
     */
    @GET
    @Produces("xml/plain")
    @Override
    public Response allsitesxml() {

        StreamerOutputXml streamerOutputXml = getStreamerOutputXml();

        streamerOutputXml.setResource(resource);
        streamerOutputXml.setDto(Dto.class);

        streamerOutputXml.setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputXml).build();
    }

    /**
     * @return
     */
    @GET
    @Produces("json/plain")
    @Override
    public Response allsitesjson() {

        StreamerOutputJson streamerOutputJson = getStreamerOutputJson();

        streamerOutputJson.setResource(resource);
        streamerOutputJson.setDto(Dto.class);

        streamerOutputJson.setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputJson).build();
    }


    private DefaultStreamerConfigurator getDefaultStreamerConfigurator() {

        DefaultStreamerConfigurator defaultStreamerConfigurator
                = new DefaultStreamerConfigurator();
        defaultStreamerConfigurator.setNbrCores(1);
        defaultStreamerConfigurator.setRatio(1);
        defaultStreamerConfigurator.setRecorderLenght(100);
        return defaultStreamerConfigurator;
    }

    /**
     * @return
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @param resource
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }


}
