package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceDatesDispo;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.ws.manager.IDto;
import org.inra.ecoinfo.ws.utils.DateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author schellenberger
 */
@XmlRootElement(name = "Variable")
@XmlType(propOrder = {
        "codeVariable", "nomVariable", "dateMin", "dateMax", "unite"
})
@XmlAccessorType(XmlAccessType.FIELD)
public class Dto implements IDto, Serializable {

    @XmlElement(name = "codeVariable")
    private String codeVariable;
    @XmlElement(name = "nomVariable")
    private String nomVariable;
    @XmlElement(name = "dateMin")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDateTime dateMin;
    @XmlElement(name = "dateMax")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDateTime dateMax;

    @XmlElement(name = "unite")
    private String unite;

    /**
     *
     */
    public Dto() {
    }

    /**
     * @return
     */
    public String getNomVariable() {
        return nomVariable;
    }

    /**
     * @param nomVariable
     */
    public void setNomVariable(String nomVariable) {
        this.nomVariable = nomVariable;
    }

    /**
     * @return
     */
    public LocalDateTime getDateMin() {
        return dateMin;
    }

    /**
     * @param dateMin
     */
    public void setDateMin(LocalDateTime dateMin) {
        this.dateMin = dateMin;
    }

    /**
     * @return
     */
    public LocalDateTime getDateMax() {
        return dateMax;
    }

    /**
     * @param dateMax
     */
    public void setDateMax(LocalDateTime dateMax) {
        this.dateMax = dateMax;
    }

    /**
     * @return
     */
    public String getCodeVariable() {
        return codeVariable;
    }

    /**
     * @param codeVariable
     */
    public void setCodeVariable(String codeVariable) {
        this.codeVariable = codeVariable;
    }

    /**
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

}

//~ Formatted by Jindent --- http://www.jindent.com
