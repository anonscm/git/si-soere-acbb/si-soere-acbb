package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS_;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS_;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite_;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle_;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme_;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB_;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement_;
import org.inra.ecoinfo.refdata.site.Site_;
import org.inra.ecoinfo.ws.manager.IResource;
import org.inra.ecoinfo.ws.manager.Manager;
import org.inra.ecoinfo.ws.manager.QueryParam;
import org.slf4j.LoggerFactory;

import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Antoine Schellenberger
 */
public class Resource extends Manager<Dto> implements IResource<Dto> {

    private static final String CONDITION_QUERY_LIMIT_OFFSET = " limit :" + QueryParam.limit + "  " + " offset :"
            + QueryParam.offset;
    private static final String FILTER_PARCELLE = " AND pp.nom = :nomParcelle \n";
    private static final String CONDITION_PARCELLE = " _CONDITION_PARCELLE_ ";
    private final String QUERY_SQL_SWC = " WITH pp AS   ( SELECT\n"
            + "        DISTINCT  parcelle_par.*,parcelle_cn.code,\n"
            + "        profondeur  \n"
            + "    FROM\n"
            + "        sequence_swc_sswc  \n"
            + "    JOIN\n"
            + "        sous_sequence_swc_ssswc using(sswc_id)  \n"
            + "    JOIN\n"
            + "        suivi_parcelle_spa using(spa_id)  \n"
            + "    JOIN\n"
            + "        parcelle_par using(par_id)  \n"
            + "    join\n"
            + "	composite_nodeable parcelle_cn on par_id=parcelle_cn.id\n"
            + "    ORDER BY\n"
            + "        par_id,\n"
            + "        profondeur  ) \n"
            + "        SELECT\n"
            + "        DISTINCT   pp.code AS \"codeParcelle\" ,\n"
            + "        pp.nom AS \"nomParcelle\" ,\n"
            + "        pp.surface AS \"surfaceParcelle\" ,\n"
            + "        site_cn.code AS \"codeSite\" ,\n"
            + "        variable_cn.code AS \"codeVariable\" ,\n"
            + "        variable.nom AS \"nomVariable\" ,\n"
            + "        array_to_string(array_agg( DISTINCT pp.profondeur),\n"
            + "        ',')   AS \"profondeurAsString\" ,\n"
            + "        tra.code AS \"codeTraitement\" ,\n"
            + "        tra.nom AS \"nomTraitement\" ,\n"
            + "        tra.description AS \"descTraitement\" ,\n"
            + "        spa.datedebuttraitement AS \"dateDebTraitement\",\n"
            + "        vtra.datedebutversiontraitement AS \"versionTraitement\" ,\n"
            + "        array_to_string (  array_agg( DISTINCT '( ' ||  mod.code || ' ) @@ ' || mod.description) ,\n"
            + "        ' **_** ') AS \"modalites\"  \n"
            + "    FROM\n"
            + "        variable AS variable\n"
            + "    join\n"
            + "	composite_nodeable variable_cn on variable_cn.id =variable.var_id,\n"
            + "        pp \n"
            + "    join\n"
            + "	composite_nodeable site_cn on pp.id=site_cn.id\n"
            + "    JOIN\n"
            + "        suivi_parcelle_spa spa USING (par_id)  \n"
            + "    JOIN\n"
            + "        traitement_tra tra USING (tra_id)  \n"
            + "    JOIN\n"
            + "        version_traitement_vtra vtra USING(tra_id) \n"
            + "    JOIN\n"
            + "        version_traitement_modalite_vtmod USING(vtra_id) \n"
            + "    JOIN\n"
            + "        modalite_mod mod USING (mod_id) \n"
            + "    WHERE\n"
            + "        variable.affichage='SWC'  \n"
            + CONDITION_PARCELLE
            + "    GROUP BY\n"
            + "        pp.code,\n"
            + "        pp.nom,\n"
            + "        pp.surface,\n"
            + "        site_cn.code,\n"
            + "        variable_cn.code,\n"
            + "        variable.nom,\n"
            + "        tra.code,\n"
            + "        tra.nom,\n"
            + "        tra.description,\n"
            + "        spa.datedebuttraitement ,\n"
            + "        vtra.datedebutversiontraitement  " + CONDITION_QUERY_LIMIT_OFFSET;

    // tra.datedebuttraitement
    private final String QUERY_SQL_TS = " WITH pp AS   ( SELECT\n"
            + "        DISTINCT  parcelle_par.*,parcelle_cn.code,\n"
            + "        profondeur  \n"
            + "    FROM\n"
            + "        sequence_ts_sts  \n"
            + "    JOIN\n"
            + "        sous_sequence_ts_ssts using(sts_id)  \n"
            + "    JOIN\n"
            + "        suivi_parcelle_spa using(spa_id)  \n"
            + "    JOIN\n"
            + "        parcelle_par using(par_id)  \n"
            + "    join\n"
            + "	composite_nodeable parcelle_cn on par_id=parcelle_cn.id\n"
            + "    ORDER BY\n"
            + "        par_id,\n"
            + "        profondeur  ) \n"
            + "        SELECT\n"
            + "        DISTINCT   pp.code AS \"codeParcelle\" ,\n"
            + "        pp.nom AS \"nomParcelle\" ,\n"
            + "        pp.surface AS \"surfaceParcelle\" ,\n"
            + "        site_cn.code AS \"codeSite\" ,\n"
            + "        variable_cn.code AS \"codeVariable\" ,\n"
            + "        variable.nom AS \"nomVariable\" ,\n"
            + "        array_to_string(array_agg( DISTINCT pp.profondeur),\n"
            + "        ',')   AS \"profondeurAsString\" ,\n"
            + "        tra.code AS \"codeTraitement\" ,\n"
            + "        tra.nom AS \"nomTraitement\" ,\n"
            + "        tra.description AS \"descTraitement\" ,\n"
            + "        spa.datedebuttraitement AS \"dateDebTraitement\",\n"
            + "        vtra.datedebutversiontraitement AS \"versionTraitement\" ,\n"
            + "        array_to_string (  array_agg( DISTINCT '( ' ||  mod.code || ' ) @@ ' || mod.description) ,\n"
            + "        ' **_** ') AS \"modalites\"  \n"
            + "    FROM\n"
            + "        variable AS variable\n"
            + "    join\n"
            + "	composite_nodeable variable_cn on variable_cn.id =variable.var_id,\n"
            + "        pp \n"
            + "    join\n"
            + "	composite_nodeable site_cn on pp.id=site_cn.id\n"
            + "    JOIN\n"
            + "        suivi_parcelle_spa spa USING (par_id)  \n"
            + "    JOIN\n"
            + "        traitement_tra tra USING (tra_id)  \n"
            + "    JOIN\n"
            + "        version_traitement_vtra vtra USING(tra_id) \n"
            + "    JOIN\n"
            + "        version_traitement_modalite_vtmod USING(vtra_id) \n"
            + "    JOIN\n"
            + "        modalite_mod mod USING (mod_id) \n"
            + "    WHERE\n"
            + "        variable.affichage='SWC'  \n"
            + CONDITION_PARCELLE
            + "    GROUP BY\n"
            + "        pp.code,\n"
            + "        pp.nom,\n"
            + "        pp.surface,\n"
            + "        site_cn.code,\n"
            + "        variable_cn.code,\n"
            + "        variable.nom,\n"
            + "        tra.code,\n"
            + "        tra.nom,\n"
            + "        tra.description,\n"
            + "        spa.datedebuttraitement ,\n"
            + "        vtra.datedebutversiontraitement  " + CONDITION_QUERY_LIMIT_OFFSET;
    ;
    private final String QUERY_SQL_SMP = " WITH pp AS   ( SELECT\n"
            + "        DISTINCT  parcelle_par.*,parcelle_cn.code,\n"
            + "        profondeur  \n"
            + "    FROM\n"
            + "        sequence_smp_ssmp  \n"
            + "    JOIN\n"
            + "        sous_sequence_smp_sssmp using(ssmp_id)  \n"
            + "    JOIN\n"
            + "        suivi_parcelle_spa using(spa_id)  \n"
            + "    JOIN\n"
            + "        parcelle_par using(par_id)  \n"
            + "    join\n"
            + "	composite_nodeable parcelle_cn on par_id=parcelle_cn.id\n"
            + "    ORDER BY\n"
            + "        par_id,\n"
            + "        profondeur  ) \n"
            + "        SELECT\n"
            + "        DISTINCT   pp.code AS \"codeParcelle\" ,\n"
            + "        pp.nom AS \"nomParcelle\" ,\n"
            + "        pp.surface AS \"surfaceParcelle\" ,\n"
            + "        site_cn.code AS \"codeSite\" ,\n"
            + "        variable_cn.code AS \"codeVariable\" ,\n"
            + "        variable.nom AS \"nomVariable\" ,\n"
            + "        array_to_string(array_agg( DISTINCT pp.profondeur),\n"
            + "        ',')   AS \"profondeurAsString\" ,\n"
            + "        tra.code AS \"codeTraitement\" ,\n"
            + "        tra.nom AS \"nomTraitement\" ,\n"
            + "        tra.description AS \"descTraitement\" ,\n"
            + "        spa.datedebuttraitement AS \"dateDebTraitement\",\n"
            + "        vtra.datedebutversiontraitement AS \"versionTraitement\" ,\n"
            + "        array_to_string (  array_agg( DISTINCT '( ' ||  mod.code || ' ) @@ ' || mod.description) ,\n"
            + "        ' **_** ') AS \"modalites\"  \n"
            + "    FROM\n"
            + "        variable AS variable\n"
            + "    join\n"
            + "	composite_nodeable variable_cn on variable_cn.id =variable.var_id,\n"
            + "        pp \n"
            + "    join\n"
            + "	composite_nodeable site_cn on pp.id=site_cn.id\n"
            + "    JOIN\n"
            + "        suivi_parcelle_spa spa USING (par_id)  \n"
            + "    JOIN\n"
            + "        traitement_tra tra USING (tra_id)  \n"
            + "    JOIN\n"
            + "        version_traitement_vtra vtra USING(tra_id) \n"
            + "    JOIN\n"
            + "        version_traitement_modalite_vtmod USING(vtra_id) \n"
            + "    JOIN\n"
            + "        modalite_mod mod USING (mod_id) \n"
            + "    WHERE\n"
            + "        variable.affichage='SWC'  \n"
            + CONDITION_PARCELLE
            + "    GROUP BY\n"
            + "        pp.code,\n"
            + "        pp.nom,\n"
            + "        pp.surface,\n"
            + "        site_cn.code,\n"
            + "        variable_cn.code,\n"
            + "        variable.nom,\n"
            + "        tra.code,\n"
            + "        tra.nom,\n"
            + "        tra.description,\n"
            + "        spa.datedebuttraitement ,\n"
            + "        vtra.datedebutversiontraitement  " + CONDITION_QUERY_LIMIT_OFFSET;
    protected List<CriteriaQuery<Tuple>> queryTupleList = new LinkedList();
    private String parcelle;

    /**
     *
     */
    public Resource() {
        initQuery();
    }

    /**
     *
     */
    public final void initQuery() {
    }

    /**
     *
     */
    public void initQueryParameter() {

        /**
         * LIMIT & OFFSET Param are managed by Manager
         */
        if (parcelle == null) {
            queryList.add(QUERY_SQL_SWC.replace(CONDITION_PARCELLE, org.apache.commons.lang.StringUtils.EMPTY));
            queryList.add(QUERY_SQL_SMP.replace(CONDITION_PARCELLE, org.apache.commons.lang.StringUtils.EMPTY));
            queryList.add(QUERY_SQL_TS.replace(CONDITION_PARCELLE, org.apache.commons.lang.StringUtils.EMPTY));
        }

        if (parcelle != null) {
            queryList.add(QUERY_SQL_SWC.replace(CONDITION_PARCELLE, FILTER_PARCELLE));
            queryList.add(QUERY_SQL_SMP.replace(CONDITION_PARCELLE, FILTER_PARCELLE));
            queryList.add(QUERY_SQL_TS.replace(CONDITION_PARCELLE, FILTER_PARCELLE));
            queryListParameter.put("nomParcelle", parcelle);
        }

    }

    /**
     * @param parcelle
     */
    public void setParcelle(String parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * @param session
     * @param indexRequest
     * @param limit
     * @return
     */
    @Override
    public List<Dto> getDtoIterable(Session session, int indexRequest, int limit) {
        return executeSQLQuery(session, indexRequest, limit, Dto.class);
    }

    private <S extends SequenceMPS, SS extends SousSequenceMPS> CriteriaQuery<Dto> getDtoIterable(SingularAttribute<SS, S> s, Class<SS> ss, String variableCode) {
        CriteriaQuery<Dto> query = builder.createQuery(Dto.class);
        Root<SS> sousSequence = query.from(ss);
        Root<VersionDeTraitement> versionDeTraitement = query.from(VersionDeTraitement.class);
        Root<Modalite> modalite = query.from(Modalite.class);
        Root<VariableACBB> variable = query.from(VariableACBB.class);
        Join<SS, S> sequence = sousSequence.join(s);
        Join<S, SuiviParcelle> suiviParcelle = sequence.join(SequenceMPS_.suiviParcelle);
        Join<SuiviParcelle, Parcelle> parcelle = suiviParcelle.join(SuiviParcelle_.parcelle);
        Join<Parcelle, SiteACBB> site = parcelle.join(Parcelle_.site);
        Join<SuiviParcelle, TraitementProgramme> traitement = suiviParcelle.join(SuiviParcelle_.traitement);

        final Path<String> codeParcelle = parcelle.get(Parcelle_.code);
        final Path<String> nomParcelle = parcelle.get(Parcelle_.nom);
        final Path<Float> surfaceParcelle = parcelle.get(Parcelle_.surface);
        final Path<String> codeSite = site.get(Site_.code);
        final Selection<String> profondeurs = builder.function(
                "array_to_string",
                String.class,
                builder.function(
                        "array_agg",
                        String[].class,
                        sousSequence.get(SousSequenceMPS_.profondeur)
                )
        ).as(String.class).alias("profondeurAsString");
        final Path<String> codeVariable = variable.get(VariableACBB_.code);
        final Path<String> nomVariable = variable.get(VariableACBB_.name);
        final Path<String> codeTraitement = traitement.get(TraitementProgramme_.code);
        final Path<String> nomTraitement = traitement.get(TraitementProgramme_.nom);
        final Path<String> descTraitement = traitement.get(TraitementProgramme_.description);
        final Path<LocalDate> dateDebTraitement = traitement.get(TraitementProgramme_.dateDebutTraitement);
        final Path<LocalDate> versionTraitement = versionDeTraitement.get(VersionDeTraitement_.dateDebutVersionTraitement);

        Expression<String> modaliteString = builder.concat(
                builder.concat("( ", modalite.get(Modalite_.code)),
                builder.concat(" ) @@ ", modalite.get(Modalite_.description))
        );
        final Selection<String> modalites = builder.function(
                "array_to_string",
                String.class,
                builder.function(
                        "array_agg",
                        String[].class,
                        modaliteString
                )
        ).as(String.class).alias("modalites");

        query
                .where(
                        modalite.in(versionDeTraitement.join(VersionDeTraitement_.modalites)),
                        builder.equal(versionDeTraitement.join(VersionDeTraitement_.traitementProgramme), traitement),
                        builder.equal(variable.get(VariableACBB_.code), variableCode),
                        builder.equal(parcelle.get(Parcelle_.code), builder.parameter(String.class, "codeParcelle"))
                )
                .groupBy(
                        codeParcelle,
                        nomParcelle,
                        surfaceParcelle,
                        codeSite,
                        codeVariable,
                        nomVariable,
                        codeTraitement,
                        nomTraitement,
                        descTraitement,
                        dateDebTraitement,
                        versionTraitement)
                .select(
                        builder.construct(Dto.class,
                                codeParcelle.alias("codeParcelle"),
                                nomParcelle.alias("nomParcelle"),
                                surfaceParcelle.alias("surfaceParcelle"),
                                codeSite.alias("codeSite"),
                                codeVariable.alias("codeVariable"),
                                nomVariable.alias("nomVariable"),
                                profondeurs,
                                codeTraitement.alias("codeTraitement"),
                                nomTraitement.alias("nomTraitement"),
                                descTraitement.alias("descTraitement"),
                                dateDebTraitement.alias("dateDebTraitement"),
                                versionTraitement.alias("versionTraitement"),
                                modalites)
                );
        return query;
    }

    /**
     * @param session
     * @param indexRequest
     * @param limit
     * @param dtoClass
     * @return
     */
    protected List<Dto> executeSQLQuery(Session session, int indexRequest, int limit, Class dtoClass) {
        List<Dto> list = new ArrayList();

        if (indexRequest > (queryList.size() - 1)) {
            finish = true;
            return list;
        }

        try {
            NativeQuery createSQLQuery = session.createNativeQuery(
                    queryList.get(indexRequest));
            setGenericParameters(createSQLQuery, queryListParameter);
            setEntityParametersForSQLQuery(createSQLQuery);
            setJoinParametersForSQLQuery(createSQLQuery);

            int incOffset = incOffset(limit);
            setLimitOffsetSQLParameter(createSQLQuery, limit, incOffset);
            List<Object[]> interList = createSQLQuery.list();
            return interList.stream()
                    .map(e -> new Dto(e))
                    .collect(Collectors.toList());
        } catch (HibernateException ex) {
            LoggerFactory.getLogger(getClass()).error(String.format("error in executeSQLQuery indexRequest %d, limit %d, dtoClass %s", indexRequest, limit, dtoClass), ex);
        }

        return list;
    }

    private <S extends SequenceMPS, SS extends SousSequenceMPS> CriteriaQuery<Tuple> getDtoTupleIterable(SingularAttribute<SS, S> s, Class<SS> ss, String variableCode) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<SS> sousSequence = query.from(ss);
        Root<VersionDeTraitement> versionDeTraitement = query.from(VersionDeTraitement.class);
        Root<Modalite> modalite = query.from(Modalite.class);
        Root<VariableACBB> variable = query.from(VariableACBB.class);
        Join<SS, S> sequence = sousSequence.join(s);
        Join<S, SuiviParcelle> suiviParcelle = sequence.join(SequenceMPS_.suiviParcelle);
        Join<SuiviParcelle, Parcelle> parcelle = suiviParcelle.join(SuiviParcelle_.parcelle);
        Join<Parcelle, SiteACBB> site = parcelle.join(Parcelle_.site);
        Join<SuiviParcelle, TraitementProgramme> traitement = suiviParcelle.join(SuiviParcelle_.traitement);

        final Path<String> codeParcelle = parcelle.get(Parcelle_.code);
        final Path<String> nomParcelle = parcelle.get(Parcelle_.nom);
        final Path<Float> surfaceParcelle = parcelle.get(Parcelle_.surface);
        final Path<String> codeSite = site.get(Site_.code);
        final Selection<String> profondeurs = builder.function(
                "array_to_string",
                String.class,
                builder.function(
                        "array_agg",
                        String[].class,
                        sousSequence.get(SousSequenceMPS_.profondeur).as(String.class)
                )
        ).as(String.class).alias("profondeurAsString");
        final Path<String> codeVariable = variable.get(VariableACBB_.code);
        final Path<String> nomVariable = variable.get(VariableACBB_.name);
        final Path<String> codeTraitement = traitement.get(TraitementProgramme_.code);
        final Path<String> nomTraitement = traitement.get(TraitementProgramme_.nom);
        final Path<String> descTraitement = traitement.get(TraitementProgramme_.description);
        final Path<LocalDate> dateDebTraitement = traitement.get(TraitementProgramme_.dateDebutTraitement);
        final Path<LocalDate> versionTraitement = versionDeTraitement.get(VersionDeTraitement_.dateDebutVersionTraitement);

        Expression<String> modaliteString = builder.concat(
                builder.concat("( ", modalite.get(Modalite_.code)),
                builder.concat(" ) @@ ", modalite.get(Modalite_.description))
        );
        final Selection<String> modalites = builder.function(
                "array_to_string",
                String.class,
                builder.function(
                        "array_agg",
                        String[].class,
                        modaliteString
                )
        ).as(String.class).alias("modalites");

        query
                .where(
                        modalite.in(versionDeTraitement.get(VersionDeTraitement_.modalites)),
                        builder.equal(versionDeTraitement.get(VersionDeTraitement_.traitementProgramme), traitement),
                        builder.equal(variable.get(VariableACBB_.code), variableCode),
                        builder.equal(parcelle.get(Parcelle_.code), builder.parameter(String.class, "codeParcelle"))
                )
                .groupBy(
                        codeParcelle,
                        nomParcelle,
                        surfaceParcelle,
                        codeSite,
                        codeVariable,
                        nomVariable,
                        codeTraitement,
                        nomTraitement,
                        descTraitement,
                        dateDebTraitement,
                        versionTraitement)
                .multiselect(
                        codeParcelle.alias("codeParcelle"),
                        nomParcelle.alias("nomParcelle"),
                        surfaceParcelle.alias("surfaceParcelle"),
                        codeSite.alias("codeSite"),
                        codeVariable.alias("codeVariable"),
                        nomVariable.alias("nomVariable"),
                        sousSequence.get(SousSequenceMPS_.profondeur).as(String.class),
                        codeTraitement.alias("codeTraitement"),
                        nomTraitement.alias("nomTraitement"),
                        descTraitement.alias("descTraitement"),
                        dateDebTraitement.alias("dateDebTraitement"),
                        versionTraitement.alias("versionTraitement"),
                        modaliteString
                );
        return query;
    }

    /**
     * Initialize First Call
     *
     * @param limit
     */
    @Override
    public void initResource(int limit) {
        decOffset(limit);
        initQueryParameter();
    }

    /**
     * @param offset
     */
    @Override
    public synchronized void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return
     */
    @Override
    public boolean isFinished() {
        return finish;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
