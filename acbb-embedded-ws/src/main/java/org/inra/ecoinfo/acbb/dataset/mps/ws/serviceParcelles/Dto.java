package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.ws.manager.IDto;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author schellenberger
 */
@XmlRootElement(name = "Parcelle")
@XmlType(propOrder = {
        "codeParcelle", "nomParcelle", "surfaceParcelle", "codeSite", "codeVariable", "nomVariable", "profondeurs",
        "versionTraitements"
})
@XmlAccessorType(XmlAccessType.FIELD)
public class Dto implements IDto, Serializable {

    @XmlElement(name = "codeParcelle")
    private String codeParcelle;
    @XmlElement(name = "nomParcelle")
    private String nomParcelle;
    @XmlElement(name = "surfaceParcelle")
    private Float surfaceParcelle;
    @XmlElement(name = "codeSite")
    private String codeSite;
    @XmlElement(name = "codeVariable")
    private String codeVariable;
    @XmlElement(name = "nomVariable")
    private String nomVariable;

    //@XmlElementWrapper(name = "profondeurs")
    @XmlElement(name = "profondeur")
    private ProfondeurDto profondeurs;

    //@XmlElementWrapper(name = "versionTraitements")
    @XmlElement(name = "versionTraitement")
    private List<VersionTraitementDTO> versionTraitements = new LinkedList();

    @XmlTransient
    private String nomTraitement;
    @XmlTransient
    private String codeTraitement;
    @XmlTransient
    private String descTraitement;
    @XmlTransient
    @XmlElement(name = "dateDebTraitement")
    private String dateDebTraitement;
    @XmlTransient
    @XmlElement(name = "profondeurAsString")
    private String profondeurAsString;
    @XmlTransient
    @XmlElement(name = "modalites")
    private String modalites;
    @XmlTransient
    @XmlElement(name = "versionTraitement")
    private String versionTraitement;

    /**
     *
     */
    public Dto() {
        versionTraitements = new ArrayList();
    }

    public Dto(Object[] entry) {
        this.codeParcelle = (String) entry[0];
        this.nomParcelle = (String) entry[1];
        this.surfaceParcelle = (Float) entry[2];
        this.codeSite = (String) entry[3];
        this.codeVariable = (String) entry[4];
        this.nomVariable = (String) entry[5];
        setProfondeurAsString((String) entry[6]);
        this.codeTraitement = (String) entry[7];
        this.nomTraitement = (String) entry[8];
        this.descTraitement = (String) entry[9];
        setDateDebTraitement((Date) entry[10]);
        setVersionTraitement((Date) entry[11]);
        setModalites((String) entry[12]);
    }

    Dto(Object r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return
     */
    public String getNomParcelle() {
        return nomParcelle;
    }

    /**
     * @param nomParcelle
     */
    public void setNomParcelle(String nomParcelle) {
        this.nomParcelle = nomParcelle;
    }

    /**
     * @return
     */
    public Float getSurfaceParcelle() {
        return surfaceParcelle;
    }

    /**
     * @param surfaceParcelle
     */
    public void setSurfaceParcelle(Float surfaceParcelle) {
        this.surfaceParcelle = surfaceParcelle;
    }

    /**
     * @return
     */
    public String getCodeSite() {
        return codeSite;
    }

    /**
     * @param codeSite
     */
    public void setCodeSite(String codeSite) {
        this.codeSite = codeSite;
    }

    /**
     * @return
     */
    public String getCodeVariable() {
        return codeVariable;
    }

    /**
     * @param codeVariable
     */
    public void setCodeVariable(String codeVariable) {
        this.codeVariable = codeVariable;
    }

    /**
     * @return
     */
    public String getCodeParcelle() {
        return codeParcelle;
    }

    /**
     * @param codeParcelle
     */
    public void setCodeParcelle(String codeParcelle) {
        this.codeParcelle = codeParcelle;
    }

    /**
     * @return
     */
    public ProfondeurDto getProfondeurs() {
        return profondeurs;
    }

    /**
     * @param profondeurs
     */
    public void setProfondeurs(ProfondeurDto profondeurs) {
        this.profondeurs = profondeurs;
    }

    /**
     * @return
     */
    public String getProfondeurAsString() {
        return profondeurAsString;
    }

    /**
     * @param profondeurAsString
     */
    public void setProfondeurAsString(String profondeurAsString) {

        this.profondeurAsString = profondeurAsString;

        String[] profondeurString = this.profondeurAsString.split(",");
        List profondeursValues = new ArrayList();

        for (String profondeurStr : profondeurString) {
            profondeursValues.add(Integer.parseInt(profondeurStr));
        }

        this.profondeurs = new ProfondeurDto(profondeursValues);

    }

    /**
     * @return
     */
    public String getNomTraitement() {
        return nomTraitement;
    }

    /**
     * @param nomTraitement
     */
    public void setNomTraitement(String nomTraitement) {
        this.nomTraitement = nomTraitement;
    }

    /**
     * @return
     */
    public String getCodeTraitement() {
        return codeTraitement;
    }

    /**
     * @param codeTraitement
     */
    public void setCodeTraitement(String codeTraitement) {
        this.codeTraitement = codeTraitement;
    }

    /**
     * @return
     */
    public String getDescTraitement() {
        return descTraitement;
    }

    /**
     * @param descTraitement
     */
    public void setDescTraitement(String descTraitement) {
        this.descTraitement = descTraitement;
    }

    /**
     * @return
     */
    public String getDateDebTraitement() {
        return dateDebTraitement;
    }

    /**
     * @param dateDebTraitement
     */
    public void setDateDebTraitement(Date dateDebTraitement) {
        this.dateDebTraitement = DateUtil.getUTCDateTextFromLocalDateTime(
                dateDebTraitement
                        .toLocalDate()
                        .atStartOfDay(),
                DateUtil.DD_MM_YYYY);
    }

    /**
     * @param dateDebTraitement
     */
    public void setDateDebTraitement(String dateDebTraitement) {
        this.dateDebTraitement = dateDebTraitement;
    }

    /**
     * @return
     */
    public String getModalites() {
        return modalites;
    }

    /**
     * @param modalites
     */
    public void setModalites(String modalites) {
        this.modalites = modalites;

        String[] arrayModalites = modalites.split(Pattern.quote(" **_** "));
        List<ModaliteDTO> modalities = new ArrayList();

        for (String modal : arrayModalites) {
            ModaliteDTO mdto = new ModaliteDTO(modal.split(" @@ ")[0].replace("( ", org.apache.commons.lang.StringUtils.EMPTY).replace(" )", org.apache.commons.lang.StringUtils.EMPTY),
                    modal.split(" @@ ")[1]);

            modalities.add(mdto);
        }

        versionTraitements.add(new VersionTraitementDTO(versionTraitement,
                new TraitementDTO(codeTraitement, nomTraitement, dateDebTraitement, descTraitement), modalities));
    }

    /**
     * @return
     */
    public String getVersionTraitement() {
        return versionTraitement;
    }

    /**
     * @param versionTraitement
     */
    public void setVersionTraitement(Date versionTraitement) {
        this.versionTraitement = DateUtil.getUTCDateTextFromLocalDateTime(
                versionTraitement
                        .toLocalDate()
                        .atStartOfDay(),
                DateUtil.DD_MM_YYYY);
    }

    /**
     * @param versionTraitement
     */
    public void setVersionTraitement(String versionTraitement) {
        this.versionTraitement = versionTraitement;
    }

    /**
     * @return
     */
    public String getNomVariable() {
        return nomVariable;
    }

    /**
     * @param nomVariable
     */
    public void setNomVariable(String nomVariable) {
        this.nomVariable = nomVariable;
    }

    /**
     * @return
     */
    public List<VersionTraitementDTO> getVersionTraitements() {
        return versionTraitements;
    }

    /**
     * @param versionTraitements
     */
    public void setVersionTraitements(List<VersionTraitementDTO> versionTraitements) {
        this.versionTraitements = versionTraitements;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
