package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import javax.xml.bind.annotation.*;

/**
 * @author yahiaoui
 */
@XmlRootElement(name = "Traitement")
@XmlType(propOrder = {
        "codeTraitement", "nomTraitement", "dateDebTraitement", "descTraitement"
})
@XmlAccessorType(XmlAccessType.FIELD)
public class TraitementDTO {

    @XmlElement(name = "codeTraitement")
    private String codeTraitement;
    @XmlElement(name = "nomTraitement")
    private String nomTraitement;
    @XmlElement(name = "dateDebTraitement")
    private String dateDebTraitement;
    @XmlElement(name = "descTraitement")
    private String descTraitement;

    /**
     *
     */
    public TraitementDTO() {
    }

    /**
     * @param codeTraitement
     * @param nomTraitement
     * @param dateDebTraitement
     * @param descTraitement
     */
    public TraitementDTO(String codeTraitement, String nomTraitement, String dateDebTraitement, String descTraitement) {
        this.codeTraitement = codeTraitement;
        this.nomTraitement = nomTraitement;
        this.dateDebTraitement = dateDebTraitement;
        this.descTraitement = descTraitement;
    }

    /**
     * @return
     */
    public String getCodeTraitement() {
        return codeTraitement;
    }

    /**
     * @return
     */
    public String getNomTraitement() {
        return nomTraitement;
    }

    /**
     * @return
     */
    public String getDescTraitement() {
        return descTraitement;
    }

    /**
     * @return
     */
    public String getDateDebTraitement() {
        return dateDebTraitement;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
