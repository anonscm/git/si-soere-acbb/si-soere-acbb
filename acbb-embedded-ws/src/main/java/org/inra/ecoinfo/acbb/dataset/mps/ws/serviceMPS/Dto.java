package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceMPS;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.ws.manager.IDto;
import org.inra.ecoinfo.ws.utils.DateAdapter;
import org.inra.ecoinfo.ws.utils.HourAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author schellenberger
 */
@XmlRootElement(name = "Variable")
@XmlType(propOrder = {
        "codeVariable", "nomVariable", "unite", "dayDate", "timeDay", "profondeur", "nomParcelle", "nomTraitement",
        "numRepetition", "valeur"
})
@XmlAccessorType(XmlAccessType.FIELD)
public class Dto implements IDto, Serializable {

    @XmlElement(name = "unite")
    private String unite;
    @XmlElement(name = "numRepetition")
    private Integer numRepetition;
    @XmlElement(name = "nomVariable")
    private String nomVariable;
    @XmlElement(name = "codeVariable")
    private String codeVariable;
    @XmlElement(name = "valeur")
    private Float valeur;
    @XmlElement(name = "date")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDateTime dayDate;
    @XmlElement(name = "heure")
    @XmlJavaTypeAdapter(HourAdapter.class)
    private LocalDateTime timeDay;
    @XmlElement(name = "nomParcelle")
    private String nomParcelle;
    @XmlElement(name = "nomTraitement")
    private String nomTraitement;

    //    @XmlAttribute(name = "unite")
//    private String unite_profondeur = "cm" ;
//      
    private ProfondeurDto profondeur;

    @XmlTransient
    private Integer profondeurValue;

    /**
     *
     */
    public Dto() {
    }

    /**
     * @return
     */
    public LocalDateTime getDayDate() {
        return dayDate;
    }

    /**
     * @param dayDate
     */
    public void setDayDate(LocalDateTime dayLocalDateTime) {
        this.dayDate = dayDate;
    }

    /**
     * @return
     */
    public Integer getNumRepetition() {
        return numRepetition;
    }

    /**
     * @param numRepetition
     */
    public void setNumRepetition(Integer numRepetition) {
        this.numRepetition = numRepetition;
    }

    /**
     * @return
     */
    public String getNomParcelle() {
        return nomParcelle;
    }

    /**
     * @param nomParcelle
     */
    public void setNomParcelle(String nomParcelle) {
        this.nomParcelle = nomParcelle;
    }

    /**
     * @return
     */
    public String getNomTraitement() {
        return nomTraitement;
    }

    /**
     * @param nomTraitement
     */
    public void setNomTraitement(String nomTraitement) {
        this.nomTraitement = nomTraitement;
    }

    /**
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

    /**
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     * @return
     */
    public LocalDateTime getTimeDay() {
        return timeDay;
    }

    /**
     * @param timeDay
     */
    public void setTimeDay(LocalDateTime timeDay) {
        this.timeDay = timeDay;
    }

    /**
     * @return
     */
    public String getCodeVariable() {
        return codeVariable;
    }

    /**
     * @param codeVariable
     */
    public void setCodeVariable(String codeVariable) {
        this.codeVariable = codeVariable;
    }

    /**
     * @return
     */
    public String getNomVariable() {
        return nomVariable;
    }

    /**
     * @param nomVariable
     */
    public void setNomVariable(String nomVariable) {
        this.nomVariable = nomVariable;
    }

//    public String getUnite_profondeur() {
//        return unite_profondeur;
//    }
//
//    public void setUnite_profondeur(String unite_profondeur) {
//        this.unite_profondeur = unite_profondeur;
//    }

    /**
     * @return
     */
    public ProfondeurDto getProfondeur() {
        return profondeur;
    }

    /**
     * @param profondeur
     */
    public void setProfondeur(ProfondeurDto profondeur) {
        this.profondeur = profondeur;
    }

    /**
     * @return
     */
    public Integer getProfondeurValue() {
        return profondeurValue;
    }

    /**
     * @param profondeurValue
     */
    public void setProfondeurValue(Integer profondeurValue) {
        this.profondeur = new ProfondeurDto(profondeurValue);
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
