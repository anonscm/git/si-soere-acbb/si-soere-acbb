package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceMPS;

//~--- non-JDK imports --------------------------------------------------------

import javax.xml.bind.annotation.*;

/**
 * @author yahiaoui
 */
@XmlRootElement(name = "profondeur")
@XmlType(propOrder = {"unite", "valeur"})
@XmlAccessorType(XmlAccessType.FIELD)
public class ProfondeurDto {

    @XmlElement(name = "unite")
    private final String unite;

    @XmlElement(name = "valeur")
    private Integer valeur;

    /**
     *
     */
    public ProfondeurDto() {
        this.unite = "cm";
    }

    /**
     * @param valeur
     */
    public ProfondeurDto(Integer valeur) {
        this.unite = "cm";
        this.valeur = valeur;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
