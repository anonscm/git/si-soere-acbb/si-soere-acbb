package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author yahiaoui
 */
@XmlType(propOrder = {"unite", "profondeur"})
@XmlAccessorType(XmlAccessType.FIELD)
public class ProfondeurDto {

    @XmlAttribute(name = "unite")
    private final String unite;

    @XmlElement(name = "profondeur")
    private List<Integer> profondeur;

    /**
     *
     */
    public ProfondeurDto() {
        this.unite = "cm";
    }

    /**
     * @param valeurs
     */
    public ProfondeurDto(List<Integer> valeurs) {
        this.profondeur = valeurs;
        this.unite = "cm";
    }

    /**
     * @return
     */
    public List<Integer> getProfondeur() {
        return profondeur;
    }

    /**
     * @param profondeur
     */
    public void setProfondeur(List<Integer> profondeur) {
        this.profondeur = profondeur;
    }

    /**
     * @return
     */
    public String getUnite() {
        return unite;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
