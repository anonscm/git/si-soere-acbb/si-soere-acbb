package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceMPS;

//~--- non-JDK imports --------------

import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.ws.streamers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.time.DateTimeException;
import java.util.List;

/**
 * @author Antoine Schellenberger
 */
public class Service extends AbstractService implements IService {
    public static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
    public static final String DATE_FORMAT = "ddMMyyyy";
    private Resource resource;

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    @GET
    @Produces("xml/plain")
    @Override
    public Response getResourceXml(@QueryParam("dateMin") String dateMin, @QueryParam("dateMax") String dateMax,
                                   @QueryParam("parcelle") String parcelle,
                                   @QueryParam("variable") List<String> variables) {
        try {
            StreamerOutputXml streamingOutputXml = getStreamerOutputXml();

            streamingOutputXml.setResource(resource);

            if (dateMin != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMin));
            }

            if (dateMax != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMax));
            }

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            if (variables != null) {
                resource.setVariables(variables);
            }

            streamingOutputXml.setDto(Dto.class);

            /**
             * DefaultStreamerConfigurator defaultStreamerConfigurator = new
             * DefaultStreamerConfigurator();
             * defaultStreamerConfigurator.setNbrCores(2);
             * defaultStreamerConfigurator.setRatio(4);
             * defaultStreamerConfigurator.setRecorderLenght(16000);
             * streamingOutputXml.setStreamerConfigurator(defaultStreamerConfigurator);
             */
            return Response.status(Response.Status.OK).entity(streamingOutputXml).build();
        } catch (DateTimeException ex) {
            LOGGER.error(null, ex);
            return null;
        }
    }

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    @GET
    @Produces("xml/encrypted")
    @Override
    public Response getResourceXmlEncrypted(@QueryParam("dateMin") String dateMin,
                                            @QueryParam("dateMax") String dateMax, @QueryParam("parcelle") String parcelle,
                                            @QueryParam("variable") List<String> variables) {
        try {
            StreamerOutputXmlEncrypted streamingOutputXmlEncrypted = getStreamerOutputXmlEncrypted();

            streamingOutputXmlEncrypted.setDto(Dto.class);

            if (dateMin != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMin));
            }

            if (dateMax != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMax));
            }

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            if (variables != null) {
                resource.setVariables(variables);
            }

            streamingOutputXmlEncrypted.setResource(resource);

            return Response.status(Response.Status.OK).entity(streamingOutputXmlEncrypted).build();
        } catch (DateTimeException ex) {
            LOGGER.error(null, ex);

            return null;
        }
    }

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    @GET
    @Produces("json/plain")
    @Override
    public Response getResourceJson(@QueryParam("dateMin") String dateMin, @QueryParam("dateMax") String dateMax,
                                    @QueryParam("parcelle") String parcelle,
                                    @QueryParam("variable") List<String> variables) {
        try {
            StreamerOutputJson streamerOutputJson = getStreamerOutputJson();

            if (dateMin != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMin));
            }

            if (dateMax != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMax));
            }

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            if (variables != null) {
                resource.setVariables(variables);
            }

            streamerOutputJson.setResource(resource);
            streamerOutputJson.setDto(Dto.class);

            return Response.status(Response.Status.OK).entity(streamerOutputJson).build();
        } catch (DateTimeException ex) {
            LOGGER.error(null, ex);
            return null;
        }
    }

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    @GET
    @Produces("json/encrypted")
    @Override
    public Response getResourceJsonEncrypted(@QueryParam("dateMin") String dateMin,
                                             @QueryParam("dateMax") String dateMax, @QueryParam("parcelle") String parcelle,
                                             @QueryParam("variable") List<String> variables) {
        try {
            StreamerOutputJsonEncrypted streamerOutputJsonEncrypted = getStreamerOutputJsonEncrypted();

            if (dateMin != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMin));
            }

            if (dateMax != null) {
                resource.setDateMin(DateUtil.readLocalDateTimeFromText(DATE_FORMAT, dateMax));
            }

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            if (variables != null) {
                resource.setVariables(variables);
            }

            streamerOutputJsonEncrypted.setResource(resource);
            streamerOutputJsonEncrypted.setDto(Dto.class);

            return Response.status(Response.Status.OK).entity(streamerOutputJsonEncrypted).build();
        } catch (DateTimeException ex) {
            LOGGER.error(null, ex);
            return null;
        }
    }

    /**
     * @return
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @param resource
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
