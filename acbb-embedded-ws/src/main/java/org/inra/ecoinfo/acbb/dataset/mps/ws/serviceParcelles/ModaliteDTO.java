package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- JDK imports ------------------------------------------------------------

import javax.xml.bind.annotation.*;

/**
 * @author yahiaoui
 */
@XmlRootElement(name = "Modalite")
@XmlType(propOrder = {"codeModalite", "descModalite"})
@XmlAccessorType(XmlAccessType.FIELD)
public class ModaliteDTO {

    @XmlElement(name = "codeModalite")
    private String codeModalite;
    @XmlElement(name = "descModalite")
    private String descModalite;

    /**
     *
     */
    public ModaliteDTO() {
    }

    /**
     * @param codeModalite
     * @param descModalite
     */
    public ModaliteDTO(String codeModalite, String descModalite) {
        this.codeModalite = codeModalite;
        this.descModalite = descModalite;
    }

    /**
     * @return
     */
    public String getCodeModalite() {
        return codeModalite;
    }

    /**
     * @param codeModalite
     */
    public void setCodeModalite(String codeModalite) {
        this.codeModalite = codeModalite;
    }

    /**
     * @return
     */
    public String getDescModalite() {
        return descModalite;
    }

    /**
     * @param descModalite
     */
    public void setDescModalite(String descModalite) {
        this.descModalite = descModalite;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
