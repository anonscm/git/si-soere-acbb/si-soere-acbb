package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceDatesDispo;

//~--- non-JDK imports --------------------------------------------------------

import org.hibernate.Session;
import org.inra.ecoinfo.ws.manager.IResource;
import org.inra.ecoinfo.ws.manager.Manager;
import org.inra.ecoinfo.ws.manager.QueryParam;

import java.util.List;

/**
 * @author Antoine Schellenberger
 */
public class Resource extends Manager<Dto> implements IResource<Dto> {

    private static final String CONDITION_QUERY_LIMIT_OFFSET = " limit :" + QueryParam.limit + " offset :"
            + QueryParam.offset;
    private static final String QUERY_NATIVE_SMP = "SELECT "
            + " var.code  AS  \"codeVariable\" , "
            + " variable  AS  \"nomVariable\" , "
            + " MIN(date) AS \"dateMin\" , "
            + " MAX(date) AS \"dateMax\" , "
            + " u.nom     AS \"unite\" "
            + " FROM smpsynthesisvalue "
            + " INNER JOIN variable_acbb_var var "
            + "   ON ( variable = var.nom ) "
            + " INNER JOIN datatype_variable_unite_acbb_dvu dvu "
            + "   ON ( var.id = dvu.var_id )"
            + " INNER JOIN unite u "
            + "   ON ( dvu.uni_id = u.id ) "
            + " GROUP BY \"variable\" , \"codeVariable\" , \"unite\"  "
            + CONDITION_QUERY_LIMIT_OFFSET;

    private static final String QUERY_NATIVE_SWC = "SELECT "
            + "var.code AS  \"codeVariable\" , "
            + "variable AS  \"nomVariable\" , "
            + "MIN(date) AS \"dateMin\" , "
            + "MAX(date) AS \"dateMax\" , "
            + " u.nom     AS \"unite\" "
            + "FROM swcsynthesisvalue "
            + "INNER JOIN variable_acbb_var var"
            + "  ON ( variable = var.nom ) "
            + " INNER JOIN datatype_variable_unite_acbb_dvu dvu "
            + "  ON (var.id = dvu.var_id ) "
            + " INNER JOIN unite u "
            + "  ON ( dvu.uni_id = u.id ) "
            + "GROUP BY \"variable\" , \"codeVariable\" , \"unite\" "
            + CONDITION_QUERY_LIMIT_OFFSET;

    private static final String QUERY_NATIVE_TS = "SELECT "
            + " var.code AS  \"codeVariable\" , "
            + " variable AS  \"nomVariable\" , "
            + " MIN(date) AS \"dateMin\" , "
            + " MAX(date) AS \"dateMax\" , "
            + "  u.nom     AS \"unite\" "
            + " FROM tssynthesisvalue "
            + "  INNER JOIN variable_acbb_var var "
            + "    ON (variable = var.nom) "
            + "  INNER JOIN datatype_variable_unite_acbb_dvu dvu "
            + "    ON (var.id = dvu.var_id ) "
            + "  INNER JOIN unite u "
            + "    ON ( dvu.uni_id = u.id ) "
            + " GROUP BY \"variable\" , \"codeVariable\" , \"unite\" "
            + CONDITION_QUERY_LIMIT_OFFSET;

//  private static final String QUERY_NATIVE_SMP 
//          = " SELECT "
//          + " variable AS \"nomVariable\" , "
//          + " MIN(date) AS \"dateMin\" , "
//          + " MAX(date) AS \"dateMax\" "
//          + " FROM smpsynthesisvalue"
//          + " GROUP BY variable "
//          + CONDITION_QUERY_LIMIT_OFFSET;
//         
//    private static final String QUERY_NATIVE_SWC 
//           = " SELECT "
//          + " variable AS \"nomVariable\" , "
//          + " MIN(date) AS \"dateMin\" , "
//          + " MAX(date) AS \"dateMax\" "
//          + " FROM swcsynthesisvalue"
//          + " GROUP BY variable "
//          + CONDITION_QUERY_LIMIT_OFFSET;
//
//    private static final String QUERY_NATIVE_TS
//           = " SELECT "
//          + " variable AS \"nomVariable\" , "
//          + " MIN(date) AS \"dateMin\" , "
//          + " MAX(date) AS \"dateMax\" "
//          + " FROM tssynthesisvalue"
//          + " GROUP BY variable "
//          + CONDITION_QUERY_LIMIT_OFFSET;

    /**
     *
     */
    public Resource() {
    }

    /**
     *
     */
    public void initQueryParameter() {

        /**
         * LIMIT & OFFSET Param are managed by Manager
         */
//        queryList.add(QUERY_NATIVE_SMP);
//        queryList.add(QUERY_NATIVE_SWC);
//        queryList.add(QUERY_NATIVE_TS);
    }

    /**
     * Initialize First Call
     *
     * @param limit
     */
    @Override
    public void initResource(int limit) {
        decOffset(limit);
        initQueryParameter();
    }

    /**
     * @param offset
     */
    @Override
    public synchronized void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return
     */
    @Override
    public boolean isFinished() {
        return finish;
    }

    @Override
    public List<Dto> getDtoIterable(Session session, int indexRequest, int limit) {
        return executeSQLQuery(session, indexRequest, limit, Dto.class);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
