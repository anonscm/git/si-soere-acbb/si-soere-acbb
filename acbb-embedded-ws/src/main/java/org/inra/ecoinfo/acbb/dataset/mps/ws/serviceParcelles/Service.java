package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.ws.streamers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * @author Antoine Schellenberger
 */
public class Service extends AbstractService implements IService {
    public static final Logger LOGGER = LoggerFactory.getLogger(Service.class);

    private Resource resource;

    /**
     * @param parcelle
     * @return
     */
    @GET
    @Produces("xml/plain")
    @Override
    public Response getResourceXml(@QueryParam("parcelle") String parcelle) {
        try {
            StreamerOutputXml streamingOutputXml = getStreamerOutputXml();

            streamingOutputXml.setResource(resource);

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            streamingOutputXml.setDto(Dto.class);

            DefaultStreamerConfigurator defaultStreamerConfigurator = new DefaultStreamerConfigurator();

            defaultStreamerConfigurator.setNbrCores(2);
            defaultStreamerConfigurator.setRatio(1);
            defaultStreamerConfigurator.setRecorderLenght(100);
            streamingOutputXml.setStreamerConfigurator(defaultStreamerConfigurator);

            return Response.status(Response.Status.OK).entity(streamingOutputXml).build();
        } catch (Exception ex) {
            LOGGER.error(null, ex);

            return null;
        }
    }

    /**
     * @param parcelle
     * @return
     */
    @GET
    @Produces("xml/encrypted")
    @Override
    public Response getResourceXmlEncrypted(@QueryParam("parcelle") String parcelle) {
        try {
            StreamerOutputXmlEncrypted streamingOutputXmlEncrypted = getStreamerOutputXmlEncrypted();

            streamingOutputXmlEncrypted.setDto(Dto.class);

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            streamingOutputXmlEncrypted.setResource(resource);

            return Response.status(Response.Status.OK).entity(streamingOutputXmlEncrypted).build();
        } catch (Exception ex) {
            LOGGER.error(null, ex);
            return null;
        }
    }

    /**
     * @param parcelle
     * @return
     */
    @GET
    @Produces("json/plain")
    @Override
    public Response getResourceJson(@QueryParam("parcelle") String parcelle) {
        try {
            StreamerOutputJson streamerOutputJson = getStreamerOutputJson();

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            streamerOutputJson.setResource(resource);
            streamerOutputJson.setDto(Dto.class);

            return Response.status(Response.Status.OK).entity(streamerOutputJson).build();
        } catch (Exception ex) {
            LOGGER.error(null, ex);
            return null;
        }
    }

    /**
     * @param parcelle
     * @return
     */
    @GET
    @Produces("json/encrypted")
    @Override
    public Response getResourceJsonEncrypted(@QueryParam("parcelle") String parcelle) {
        try {
            StreamerOutputJsonEncrypted streamerOutputJsonEncrypted = getStreamerOutputJsonEncrypted();

            if (parcelle != null) {
                resource.setParcelle(parcelle);
            }

            streamerOutputJsonEncrypted.setResource(resource);
            streamerOutputJsonEncrypted.setDto(Dto.class);

            return Response.status(Response.Status.OK).entity(streamerOutputJsonEncrypted).build();
        } catch (Exception ex) {
            LOGGER.error(null, ex);
            return null;
        }
    }

    /**
     * @return
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @param resource
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
