package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.identification.ws.subservice.ISubService;

import javax.ws.rs.core.Response;

/**
 * @author yahiaoui
 */
public interface IService extends ISubService {

    /**
     * @param parcelle
     * @return
     */
    public Response getResourceXml(String parcelle);

    /**
     * @param parcelle
     * @return
     */
    public Response getResourceXmlEncrypted(String parcelle);

    /**
     * @param parcelle
     * @return
     */
    public Response getResourceJson(String parcelle);

    /**
     * @param parcelle
     * @return
     */
    public Response getResourceJsonEncrypted(String parcelle);
}

// ~ Formatted by Jindent --- http://www.jindent.com
