package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceMPS;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.identification.ws.subservice.ISubService;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author yahiaoui
 */
public interface IService extends ISubService {

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    public Response getResourceXml(String dateMin, String dateMax, String parcelle, List<String> variables);

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    public Response getResourceXmlEncrypted(String dateMin, String dateMax, String parcelle, List<String> variables);

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    public Response getResourceJson(String dateMin, String dateMax, String parcelle, List<String> variables);

    /**
     * @param dateMin
     * @param dateMax
     * @param parcelle
     * @param variables
     * @return
     */
    public Response getResourceJsonEncrypted(String dateMin, String dateMax, String parcelle, List<String> variables);
}


//~ Formatted by Jindent --- http://www.jindent.com
