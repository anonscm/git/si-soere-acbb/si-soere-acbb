package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceDatesDispo;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.ws.streamers.*;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * @author Antoine Schellenberger
 */
public class Service extends AbstractService implements IService {

    private Resource resource;

    /**
     * @param specifiedTypeDispo
     * @return
     */
    @GET
    @Produces("xml/plain")
    @Override
    public Response getResourceXml(@QueryParam("specifiedTypeDispo") String specifiedTypeDispo) {
        StreamerOutputXml streamingOutputXml = getStreamerOutputXml();

        /**
         * DefaultStreamerConfigurator defaultStreamerConfigurator = new
         * DefaultStreamerConfigurator();
         * defaultStreamerConfigurator.setNbrCores(2);
         * defaultStreamerConfigurator.setRatio(4);
         * defaultStreamerConfigurator.setRecorderLenght(16000);
         * streamingOutputXml.setStreamerConfigurator(defaultStreamerConfigurator);
         */
        streamingOutputXml.setDto(Dto.class);
        streamingOutputXml.setResource(resource);

        return Response.status(Response.Status.OK).entity(streamingOutputXml).build();
    }

    /**
     * @param specifiedTypeDisp
     * @return
     */
    @GET
    @Produces("xml/encrypted")
    @Override
    public Response getResourceXmlEncrypted(@QueryParam("specifiedTypeDispo") String specifiedTypeDisp) {
        StreamerOutputXmlEncrypted streamingOutputXmlEncrypted = getStreamerOutputXmlEncrypted();

        streamingOutputXmlEncrypted.setDto(Dto.class);
        streamingOutputXmlEncrypted.setResource(resource);

        return Response.status(Response.Status.OK).entity(streamingOutputXmlEncrypted).build();
    }

    /**
     * @param specifiedTypeDisp
     * @return
     */
    @GET
    @Produces("json/plain")
    @Override
    public Response getResourceJson(@QueryParam("specifiedTypeDispo") String specifiedTypeDisp) {
        StreamerOutputJson streamerOutputJson = getStreamerOutputJson();

        streamerOutputJson.setResource(resource);
        streamerOutputJson.setDto(Dto.class);

        return Response.status(Response.Status.OK).entity(streamerOutputJson).build();
    }

    /**
     * @param specifiedTypeDisp
     * @return
     */
    @GET
    @Produces("json/encrypted")
    @Override
    public Response getResourceJsonEncrypted(@QueryParam("specifiedTypeDispo") String specifiedTypeDisp) {
        StreamerOutputJsonEncrypted streamerOutputJsonEncrypted = getStreamerOutputJsonEncrypted();

        streamerOutputJsonEncrypted.setResource(resource);
        streamerOutputJsonEncrypted.setDto(Dto.class);

        return Response.status(Response.Status.OK).entity(streamerOutputJsonEncrypted).build();
    }

    /**
     * @return
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @param resource
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
//curl -i -H "accept: json/plain" "http://localhost:10280/acbb-ws/rest/resources/admin/9e8be9cf0cab6a5bfa030bb5c5c80e13ec34f30/1423666505514/inspect"
