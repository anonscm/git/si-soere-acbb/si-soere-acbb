package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceMPS;

//~--- non-JDK imports --------------------------------------------------------

import org.hibernate.Session;
import org.inra.ecoinfo.ws.manager.IResource;
import org.inra.ecoinfo.ws.manager.Manager;
import org.inra.ecoinfo.ws.manager.QueryParam;
import org.inra.ecoinfo.ws.utils.UtilsQueryBuilder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Antoine Schellenberger
 */
public class Resource extends Manager<Dto> implements IResource<Dto> {

    private static final String CONDITION_QUERY_LIMIT_OFFSET = " limit :" + QueryParam.limit + " offset :"
            + QueryParam.offset;
    private static final String NOM_PARCELLE = " par.nom = :nomParcelle ";
    private final static String DATE_MIN_QUERY_NATIVE_SWC_HVS = " sswc.date >= :dateMin ";
    private final static String DATE_MAX_QUERY_NATIVE_SWC_HVS = " sswc.date <= :dateMax ";
    private final static String DATE_BET_QUERY_NATIVE_SWC_HVS = " sswc.date between  :dateMin AND :dateMax";
    private final static String DATE_MIN_QUERY_NATIVE_SMP_TES = " ssmp.date >= :dateMin ";
    private final static String DATE_MAX_QUERY_NATIVE_SMP_TES = " ssmp.date <= :dateMax ";
    private final static String DATE_BET_QUERY_NATIVE_SMP_TES = " ssmp.date between :dateMin AND :dateMax";
    private final static String DATE_MIN_QUERY_NATIVE_SMP_TS = " sts.date >= :dateMin ";
    private final static String DATE_MAX_QUERY_NATIVE_SMP_TS = " sts.date <= :dateMax ";
    private final static String DATE_BET_QUERY_NATIVE_SMP_TS = " sts.date between :dateMin AND :dateMax ";

    private static final String QUERY_NATIVE_SWC_HVS
            = " SELECT " + " var.nom AS             \"nomVariable\" , " + " var.code AS             \"codeVariable\" , "
            + " uni.nom AS             \"unite\", " + " sswc.date AS           \"dayDate\" , "
            + " mswc.heure AS          \"timeDay\" , " + " ssswc.profondeur AS    \"profondeurValue\" , "
            + " par.nom AS             \"nomParcelle\"  , " + " tra.nom AS             \"nomTraitement\" , "
            + " vswc.num_repetition AS \"numRepetition\" , " + " vswc.valeur  " + " FROM valeur_swc_vswc vswc "
            + " INNER JOIN variable_acbb_var var on (var.id = vswc.id) "
            + " INNER JOIN datatype_variable_unite_acbb_dvu vdt on (vdt.var_id = var.id) "
            + " inner join unite uni on (uni.id = vdt.uni_id)"
            + " INNER JOIN mesure_swc_mswc mswc on (mswc.mswc_id = vswc.mswc_id) "
            + " INNER JOIN sous_sequence_swc_ssswc ssswc on (ssswc.ssswc_id = mswc.ssswc_id) "
            + " INNER JOIN sequence_swc_sswc sswc on (ssswc.sswc_id = sswc.sswc_id) "
            + " INNER JOIN suivi_parcelle_spa spa on (spa.spa_id = sswc.spa_id)"
            + " INNER JOIN parcelle_par par on (par.par_id = spa.par_id)"
            + " INNER JOIN traitement_tra tra on (tra.tra_id = spa.tra_id) ";

    private static final String QUERY_NATIVE_SMP_TES
            = " SELECT  " + " var.nom AS             \"nomVariable\" , " + " var.code AS             \"codeVariable\" , "
            + " uni.nom AS              \"unite\"," + " ssmp.date AS            \"dayDate\" ,"
            + " mswc.heure AS           \"timeDay\"," + " ssswc.profondeur AS     \"profondeurValue\","
            + " par.nom AS              \"nomParcelle\" ," + " tra.nom as              \"nomTraitement\" ,"
            + " vswc.num_repetition AS  \"numRepetition\" ," + " vswc.valeur" + " FROM valeur_smp_vsmp vswc "
            + " INNER JOIN variable_acbb_var var on (var.id = vswc.id)"
            + " INNER JOIN datatype_variable_unite_acbb_dvu vdt on (vdt.var_id = var.id)"
            + " INNER JOIN unite uni on (uni.id = vdt.uni_id)"
            + " INNER JOIN mesure_smp_msmp mswc on (mswc.msmp_id = vswc.msmp_id)"
            + " INNER JOIN sous_sequence_smp_sssmp ssswc on (ssswc.sssmp_id = mswc.sssmp_id)"
            + " INNER JOIN sequence_smp_ssmp ssmp on (ssswc.ssmp_id = ssmp.ssmp_id)"
            + " INNER JOIN suivi_parcelle_spa spa on (spa.spa_id = ssmp.spa_id)"
            + " INNER JOIN parcelle_par par on (par.par_id = spa.par_id)"
            + " INNER JOIN traitement_tra tra on (tra.tra_id = spa.tra_id) ";

    private static final String QUERY_NATIVE_TS
            = " SELECT " + " var.nom AS            \"nomVariable\" , " + " var.code AS           \"codeVariable\" , "
            + " uni.nom AS            \"unite\" , " + " sts.date AS           \"dayDate\" , "
            + " mts.heure AS          \"timeDay\" , " + " ssts.profondeur AS    \"profondeurValue\" ,"
            + " par.nom AS            \"nomParcelle\" , " + " tra.nom AS            \"nomTraitement\" , "
            + " vts.num_repetition AS \"numRepetition\" , " + " vts.valeur " + " FROM valeur_ts_vts  vts"
            + " INNER JOIN variable_acbb_var var on (var.id = vts.id)"
            + " INNER JOIN datatype_variable_unite_acbb_dvu vdt on (vdt.var_id = var.id)"
            + " INNER JOIN unite uni on (uni.id = vdt.uni_id)"
            + " INNER JOIN mesure_ts_mts mts on (mts.mts_id = vts.mts_id)"
            + " INNER JOIN sous_sequence_ts_ssts ssts on (ssts.ssts_id = mts.ssts_id)"
            + " INNER JOIN sequence_ts_sts sts on (ssts.sts_id = sts.sts_id)"
            + " INNER JOIN suivi_parcelle_spa spa on (spa.spa_id = sts.spa_id)"
            + " INNER JOIN parcelle_par par on (par.par_id = spa.par_id)"
            + " INNER JOIN traitement_tra tra on (tra.tra_id = spa.tra_id) ";

    private final String HVS = "humidite_volumique_du_sol";
    private final String TES = "tension_de_l_eau_dans_le_sol";
    private final String TS = "temperature_du_sol";

    /*
     * private final String HVS = "humidite volumique du sol";
     * private final String TES = "tension de l'eau dans le sol";
     * private final String TS  = "temperature du sol";
     */
    private LocalDateTime dateMin;
    private LocalDateTime dateMax;
    private String parcelle;
    private List<String> variables;

    /**
     *
     */
    public Resource() {
    }

    /**
     *
     */
    public void initQueryParameter() {

        /**
         * LIMIT & OFFSET Param are managed by Manager
         */
        Map<String, String> queries = new HashMap();

        /**
         * Correspondence Nom_Variavle vs Requete
         */
        queries.put(HVS, QUERY_NATIVE_SWC_HVS);
        queries.put(TES, QUERY_NATIVE_SMP_TES);
        queries.put(TS, QUERY_NATIVE_TS);

        List<String> selectQueries = UtilsQueryBuilder.selectQueries(queries, variables);
        Map<String, Object> listVariables = new HashMap();

        listVariables.put("nomParcelle", parcelle);

        Map<String, String> filterVariables = new HashMap();

        filterVariables.put("nomParcelle", NOM_PARCELLE);

        for (String query : selectQueries) {
            Map<String, LocalDateTime> dates = new HashMap();
            Map<String, List<String>> listDates = new HashMap();
            Map<String, List<String>> valueDates = new HashMap();

            if (query.equals(QUERY_NATIVE_SWC_HVS)) {
                List<String> valueDateQueryRegex_0 = new ArrayList();
                List<String> datesID = new ArrayList();

                valueDateQueryRegex_0.add(DATE_MIN_QUERY_NATIVE_SWC_HVS);
                valueDateQueryRegex_0.add(DATE_MAX_QUERY_NATIVE_SWC_HVS);
                valueDateQueryRegex_0.add(DATE_BET_QUERY_NATIVE_SWC_HVS);
                dates.put("dateMin", dateMin);
                dates.put("dateMax", dateMax);
                datesID.add("dateMin");
                datesID.add("dateMax");
                listDates.put("dateQueryOne", datesID);
                valueDates.put("dateQueryOne", valueDateQueryRegex_0);
                valueDates.put("dateQueryTT", valueDateQueryRegex_0);
            }

            if (query.equals(QUERY_NATIVE_SMP_TES)) {
                List<String> valueDateQueryRegex_1 = new ArrayList();
                List<String> datesID = new ArrayList();

                valueDateQueryRegex_1.add(DATE_MIN_QUERY_NATIVE_SMP_TES);
                valueDateQueryRegex_1.add(DATE_MAX_QUERY_NATIVE_SMP_TES);
                valueDateQueryRegex_1.add(DATE_BET_QUERY_NATIVE_SMP_TES);
                dates.put("dateMin", dateMin);
                dates.put("dateMax", dateMax);
                datesID.add("dateMin");
                datesID.add("dateMax");
                listDates.put("dateQueryTwo", datesID);
                valueDates.put("dateQueryTwo", valueDateQueryRegex_1);
            }

            if (query.equals(QUERY_NATIVE_TS)) {
                List<String> valueDateQueryRegex_2 = new ArrayList();
                List<String> datesID = new ArrayList();

                valueDateQueryRegex_2.add(DATE_MIN_QUERY_NATIVE_SMP_TS);
                valueDateQueryRegex_2.add(DATE_MAX_QUERY_NATIVE_SMP_TS);
                valueDateQueryRegex_2.add(DATE_BET_QUERY_NATIVE_SMP_TS);
                dates.put("dateMin", dateMin);
                dates.put("dateMax", dateMax);
                datesID.add("dateMin");
                datesID.add("dateMax");
                listDates.put("dateQueryThree", datesID);
                valueDates.put("dateQueryThree", valueDateQueryRegex_2);
            }

            String buildQuery = UtilsQueryBuilder.buildQuery(query, listVariables, filterVariables, dates, listDates,
                    valueDates, CONDITION_QUERY_LIMIT_OFFSET, queryListParameter);

            //queryList.add(buildQuery);
        }
    }

    /**
     * @param dateMin
     */
    public void setDateMin(LocalDateTime dateMin) {
        this.dateMin = dateMin;
    }

    /**
     * @param dateMax
     */
    public void setDateMax(LocalDateTime dateMax) {
        this.dateMax = dateMax;
    }

    /**
     * @param parcelle
     */
    public void setParcelle(String parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * @return
     */
    public List<String> getVariables() {
        return variables;
    }

    /**
     * @param variables
     */
    public void setVariables(List<String> variables) {
        this.variables = variables;
    }

    /**
     * @param session
     * @param indexRequest
     * @param limit
     * @return
     */
    @Override
    public List<Dto> getDtoIterable(Session session, int indexRequest, int limit) {
        return executeSQLQuery(session, indexRequest, limit, Dto.class);
    }

    /**
     * Initialize First Call
     *
     * @param limit
     */
    @Override
    public void initResource(int limit) {
        decOffset(limit);
        initQueryParameter();
    }

    /**
     * @param offset
     */
    @Override
    public synchronized void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return
     */
    @Override
    public boolean isFinished() {
        return finish;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
