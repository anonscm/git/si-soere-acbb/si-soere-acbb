package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceDatesDispo;

//~--- non-JDK imports --------------------------------------------------------

import org.inra.ecoinfo.identification.ws.subservice.ISubService;

import javax.ws.rs.core.Response;

/**
 * @author yahiaoui
 */
public interface IService extends ISubService {

    /**
     * @param specifiedTypeDisp
     * @return
     */
    public Response getResourceXml(String specifiedTypeDisp);

    /**
     * @param specifiedTypeDisp
     * @return
     */
    public Response getResourceXmlEncrypted(String specifiedTypeDisp);

    /**
     * @param specifiedTypeDisp
     * @return
     */
    public Response getResourceJson(String specifiedTypeDisp);

    /**
     * @param specifiedTypeDisp
     * @return
     */
    public Response getResourceJsonEncrypted(String specifiedTypeDisp);
}

// ~ Formatted by Jindent --- http://www.jindent.com
