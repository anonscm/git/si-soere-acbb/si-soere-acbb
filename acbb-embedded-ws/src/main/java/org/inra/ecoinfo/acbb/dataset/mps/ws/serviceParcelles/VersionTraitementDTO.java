package org.inra.ecoinfo.acbb.dataset.mps.ws.serviceParcelles;

//~--- non-JDK imports --------------------------------------------------------

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author yahiaoui
 */
@XmlRootElement(name = "VersionTraitement")
@XmlType(propOrder = {"versionTraitement", "traitement", "modalites"})
@XmlAccessorType(XmlAccessType.FIELD)
public class VersionTraitementDTO {

    @XmlElement(name = "dateVersionTraitement")
    private String versionTraitement;
    @XmlElement(name = "Traitement")
    private TraitementDTO traitement;
    @XmlElementWrapper(name = "Modalites")
    @XmlElement(name = "modalite")
    private List<ModaliteDTO> modalites;

    /**
     *
     */
    public VersionTraitementDTO() {
    }

    /**
     * @param versionTraitement
     * @param traitement
     * @param modalites
     */
    public VersionTraitementDTO(String versionTraitement, TraitementDTO traitement, List<ModaliteDTO> modalites) {
        this.versionTraitement = versionTraitement;
        this.modalites = modalites;
        this.traitement = traitement;
    }

    /**
     * @return
     */
    public String getVersionTraitement() {
        return versionTraitement;
    }

    /**
     * @param versionTraitement
     */
    public void setVersionTraitement(String versionTraitement) {
        this.versionTraitement = versionTraitement;
    }

    /**
     * @return
     */
    public List<ModaliteDTO> getModalites() {
        return modalites;
    }

    /**
     * @param modalites
     */
    public void setModalites(List<ModaliteDTO> modalites) {
        this.modalites = modalites;
    }

    /**
     * @return
     */
    public TraitementDTO getTraitement() {
        return traitement;
    }

    /**
     * @param traitement
     */
    public void setTraitement(TraitementDTO traitement) {
        this.traitement = traitement;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
