/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The Class SousSequenceMPS.
 *
 * @param <S1>  the generic type
 * @param <SS1> the generic type
 * @param <MI>  the generic type
 * @param <V1>  the generic type
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public abstract class SousSequenceMPS<S1 extends SequenceMPS, SS1 extends SousSequenceMPS, MI extends MesureMPS, V1 extends ValeurMPS>
        implements Serializable {

    /**
     * The Constant ATTRIBUTE_JPA_ID.
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     * The Constant ATTRIBUTE_JPA_PROFONDEUR.
     */
    public static final String ATTRIBUTE_JPA_PROFONDEUR = "profondeur";

    /**
     * The Constant ATTRIBUTE_JPA_SEQUENCE_MPS.
     */
    public static final String ATTRIBUTE_JPA_SEQUENCE_MPS = "sequenceMPS";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The profondeur @link(Integer).
     */
    @Column(name = SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR, nullable = false)
    Integer profondeur;

    /**
     * Instantiates a new sous sequence mps.
     */
    public SousSequenceMPS() {
        super();
    }

    /**
     * Instantiates a new sous sequence mps.
     *
     * @param sequence   the sequence
     * @param profondeur the profondeur
     */
    @SuppressWarnings("unchecked")
    public SousSequenceMPS(final S1 sequence, final int profondeur) {
        super();
        this.setSequenceMPS(sequence);
        if (!this.getSequenceMPS().getSousSequencesMPS().contains(this)) {
            this.getSequenceMPS().getSousSequencesMPS().add(this);
        }
        this.setProfondeur(profondeur);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesures mps.
     *
     * @return the mesures mps
     */
    public abstract List<MI> getMesuresMPS();

    /**
     * Sets the mesures mps.
     *
     * @param mesuresMPS the new mesures mps
     */
    public abstract void setMesuresMPS(List<MI> mesuresMPS);

    /**
     * Gets the profondeur.
     *
     * @return the profondeur
     */
    public Integer getProfondeur() {
        return this.profondeur;
    }

    /**
     * Sets the profondeur.
     *
     * @param profondeur the new profondeur
     */
    public void setProfondeur(final Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     * Gets the sequence mps.
     *
     * @return the sequence mps
     */
    public abstract S1 getSequenceMPS();

    /**
     * Sets the sequence mps.
     *
     * @param sequenceMPS the new sequence mps
     */
    public abstract void setSequenceMPS(S1 sequenceMPS);
}
