package org.inra.ecoinfo.acbb.dataset.mps.smp.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class MesureSMP.
 */
@Entity
@Table(name = MesureSMP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SousSequenceSMP.ID_JPA, MesureSMP.ATTRIBUTE_JPA_TIME}),
        indexes = {
                @Index(name = "msmp_sssmp_idx", columnList = SousSequenceSMP.ID_JPA),
                @Index(name = "msmp_time_idx", columnList = MesureSMP.ATTRIBUTE_JPA_TIME)})
@PrimaryKeyJoinColumn(name = MesureMPS.ATTRIBUTE_JPA_ID, referencedColumnName = MesureSMP.ID_JPA)
@AttributeOverrides({@AttributeOverride(name = MesureMPS.ATTRIBUTE_JPA_ID, column = @Column(name = MesureSMP.ID_JPA))})
public class MesureSMP extends MesureMPS<SequenceSMP, SousSequenceSMP, MesureSMP, ValeurSMP> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "msmp_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "mesure_smp_msmp";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The valeurs smp @link(List<ValeurSMP>).
     */
    @OneToMany(mappedBy = ValeurMPS.ATTRIBUTE_JPA_MEASURE_MPS, cascade = {PERSIST, MERGE, REFRESH})
    List<ValeurSMP> valeursSMP = new LinkedList();

    /**
     * The sous sequence mps @link(SousSequenceSMP).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SousSequenceSMP.ID_JPA, referencedColumnName = SousSequenceSMP.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    SousSequenceSMP sousSequenceMPS;

    /**
     * Instantiates a new mesure smp.
     */
    public MesureSMP() {
        super();
    }

    /**
     * Instantiates a new mesure smp.
     *
     * @param sousSequence       the sous sequence
     * @param time               the time
     * @param originalLineNumber the original line number
     */
    public MesureSMP(final SousSequenceSMP sousSequence, final LocalTime time,
                     final int originalLineNumber) {
        super(sousSequence, time, originalLineNumber);
    }

    /**
     * Gets the sous sequence mps @link(SousSequenceSMP).
     *
     * @return the sous sequence mps @link(SousSequenceSMP)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#getSousSequenceMPS()
     */
    @Override
    public SousSequenceSMP getSousSequenceMPS() {
        return this.sousSequenceMPS;
    }

    /**
     * Sets the sous sequence mps @link(SousSequenceSMP).
     *
     * @param sousSequenceSMP the new sous sequence mps @link(SousSequenceSMP)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#setSousSequenceMPS
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS)
     */
    @Override
    public void setSousSequenceMPS(final SousSequenceSMP sousSequenceSMP) {
        this.sousSequenceMPS = sousSequenceSMP;
    }

    /**
     * Gets the valeurs mps.
     *
     * @return the valeurs mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#getValeursMPS()
     */
    @Override
    public List<ValeurSMP> getValeursMPS() {
        return this.valeursSMP;
    }

    /**
     * Sets the valeurs mps.
     *
     * @param valeurMPS the new valeurs mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#setValeursMPS(java .util.List)
     */
    @Override
    public void setValeursMPS(final List<ValeurSMP> valeurMPS) {
        this.valeursSMP = valeurMPS;
    }
}
