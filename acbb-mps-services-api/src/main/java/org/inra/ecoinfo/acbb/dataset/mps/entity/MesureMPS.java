/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.List;

/**
 * The Class MesureMPS.
 *
 * @param <SI>  the generic type
 * @param <SSI> the generic type
 * @param <MI>  the generic type
 * @param <VI>  the generic type
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public abstract class MesureMPS<SI extends SequenceMPS, SSI extends SousSequenceMPS, MI extends MesureMPS, VI extends ValeurMPS>
        implements Serializable {

    /**
     * The Constant ATTRIBUTE_JPA_ID.
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     * The Constant ATTRIBUTE_JPA_TIME.
     */
    public static final String ATTRIBUTE_JPA_TIME = "heure";

    /**
     * The Constant ATTRIBUTE_JPA_SOUS_SEQUENCE_MPS.
     */
    public static final String ATTRIBUTE_JPA_SOUS_SEQUENCE_MPS = "sousSequenceMPS";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    static final String RESOURCE_PATH = "%s/%s/%s/%s";

    /**
     * The id <long>.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The heure @link(Date).
     */
    @Column(name = MesureMPS.ATTRIBUTE_JPA_TIME, nullable = false)
    LocalTime heure;

    /**
     * The line number @link(int).
     */
    int lineNumber;

    /**
     * Instantiates a new mesure mps.
     */
    public MesureMPS() {
        super();
    }

    /**
     * Instantiates a new mesure mps.
     *
     * @param sousSequence       the sous sequence
     * @param heure              the heure
     * @param originalLineNumber the original line number
     */
    @SuppressWarnings("unchecked")
    public MesureMPS(final SSI sousSequence, final LocalTime heure, final int originalLineNumber) {
        super();
        this.setSousSequenceMPS(sousSequence);
        this.setLineNumber(originalLineNumber);
        if (!this.getSousSequenceMPS().getMesuresMPS().contains(this)) {
            this.getSousSequenceMPS().getMesuresMPS().add(this);
        }
        this.heure = heure;
    }

    /**
     * Gets the heure.
     *
     * @return the heure
     */
    public LocalTime getHeure() {
        return this.heure;
    }

    /**
     * Sets the heure.
     *
     * @param heure the new heure
     */
    public void setHeure(final LocalTime heure) {
        this.heure = heure;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the line number.
     *
     * @return the line number
     */
    public int getLineNumber() {
        return this.lineNumber;
    }

    /**
     * Sets the line number.
     *
     * @param lineNumber the new line number
     */
    public void setLineNumber(final int lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     * Gets the sous sequence mps.
     *
     * @return the sous sequence mps
     */
    public abstract SSI getSousSequenceMPS();

    /**
     * Sets the sous sequence mps.
     *
     * @param sousSequenceMPS the new sous sequence mps
     */
    public abstract void setSousSequenceMPS(SSI sousSequenceMPS);

    /**
     * Gets the valeurs mps.
     *
     * @return the valeurs mps
     */
    public abstract List<VI> getValeursMPS();

    /**
     * Sets the valeurs mps.
     *
     * @param valeurMPS the new valeurs mps
     */
    public abstract void setValeursMPS(List<VI> valeurMPS);
}
