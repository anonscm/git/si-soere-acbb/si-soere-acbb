/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.ts.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;

import static javax.persistence.CascadeType.*;

/**
 * The Class ValeurTS.
 */
@Entity
@Table(name = ValeurTS.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        MesureTS.ID_JPA, RealNode.ID_JPA, ValeurMPS.ATTRIBUTE_JPA_NUM_REPETITION}),
        indexes = {
                @Index(name = "vts_mts_idx", columnList = MesureTS.ID_JPA),
                @Index(columnList = RealNode.ID_JPA, name = "vts_rn__idx")})
@PrimaryKeyJoinColumn(name = ValeurMPS.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurTS.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = ValeurMPS.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurTS.ID_JPA))})
public class ValeurTS extends ValeurMPS<SequenceTS, SousSequenceTS, MesureTS, ValeurTS> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vts_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "valeur_ts_vts";

    /**
     * The Constant ATTRIBUTE_JPA_MEASURE_TS.
     */
    public static final String ATTRIBUTE_JPA_MEASURE_TS = "mesureTS";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesure mps @link(MesureTS).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureTS.ID_JPA, referencedColumnName = MesureTS.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureTS mesureMPS;

    /**
     * Instantiates a new valeur ts.
     */
    public ValeurTS() {
        super();
    }

    /**
     * Instantiates a new valeur ts.
     *
     * @param value        the value
     * @param qualityClass the quality class
     */
    public ValeurTS(final Float value, final Integer qualityClass) {
        super(value, qualityClass);
    }

    /**
     * Gets the mesure mps @link(MesureTS).
     *
     * @return the mesure mps @link(MesureTS)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS#getMesureMPS()
     */
    @Override
    public MesureTS getMesureMPS() {
        return this.mesureMPS;
    }

    /**
     * Sets the mesure mps @link(MesureTS).
     *
     * @param mesureMPS the new mesure mps @link(MesureTS)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS#setMesureMPS(org.inra
     * .ecoinfo.acbb.dataset.mps.entity.MesureMPS)
     */
    @Override
    public void setMesureMPS(final MesureTS mesureMPS) {
        this.mesureMPS = mesureMPS;
    }
}
