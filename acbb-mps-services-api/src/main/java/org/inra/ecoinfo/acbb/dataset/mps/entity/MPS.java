/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.mps.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author ptcherniati
 */
@Entity(name = "mps")
@Table(name = "mps", indexes = {
        @Index(name = "check_idx", columnList = "varaffichage, profondeur,tra_id,date"),
        @Index(name = "date_idx", columnList = "date"),
        @Index(name = "idnode_idx", columnList = "idnode"),
        @Index(name = "profondeur_idx", columnList = "profondeur"),
        @Index(name = "sit_idx", columnList = "sit_id"),
        @Index(name = "spa_idx", columnList = "spa_id"),
        @Index(name = "tra_idx", columnList = "tra_id,"),
        @Index(name = "var_idx", columnList = "varaffichage")
})
public class MPS implements Serializable {

    @Id
    @Column(name = "idnode", nullable = false, columnDefinition = "bigint NOT NULL")
    Long idNode;
    @Id
    @Column(name = "date", nullable = false, columnDefinition = "date NOT NULL")
    LocalDate date;
    @Id
    @Column(name = "heure", nullable = false, columnDefinition = "time without time zone NOT NULL")
    LocalTime heure;
    @Column(name = "valeur", columnDefinition = "real")
    Float valeur;
    @Column(name = "qc", columnDefinition = "integer")
    Integer qc;
    @Column(name = "sit_id", columnDefinition = "bigint")
    Long siteId;
    @Column(name = "sitecode", columnDefinition = "character varying(255) COLLATE pg_catalog.\"default\"")
    String siteCode;
    @Column(name = "parcellenom", columnDefinition = "character varying(255) COLLATE pg_catalog.\"default\"")
    String parcellename;
    @Column(name = "bloc", columnDefinition = "character varying(255) COLLATE pg_catalog.\"default\"")
    String bloc;
    @Column(name = "repetition", columnDefinition = "character varying(255) COLLATE pg_catalog.\"default\"")
    String repetition;
    @Column(name = "tra_id", columnDefinition = "bigint")
    Long traId;
    @Column(name = "tranom", columnDefinition = "character varying(255) COLLATE pg_catalog.\"default\"")
    String traname;
    @Column(name = "traaffichage", columnDefinition = "character varying(255) COLLATE pg_catalog.\"default\"")
    String traAffichage;
    @Id
    @Column(name = "spa_id", columnDefinition = "bigint NOT NULL")
    Long spaId;
    @Column(name = "varaffichage", columnDefinition = "character varying COLLATE pg_catalog.\"default\" NOT NULL")
    @Id
    String varAffichage;
    @Id
    @Column(name = "num_repetition", columnDefinition = "integer NOT NULL")
    Integer numrepetition;
    @Id
    @Column(name = "profondeur", columnDefinition = "integer NOT NULL")
    Integer profondeur;
    public MPS() {
    }
}
