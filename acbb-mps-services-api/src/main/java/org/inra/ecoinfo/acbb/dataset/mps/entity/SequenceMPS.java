/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SequenceMPS.
 *
 * @param <S0>  the generic type
 * @param <SSO> the generic type
 * @param <MO>  the generic type
 * @param <VO>  the generic type
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public abstract class SequenceMPS<S0 extends SequenceMPS, SSO extends SousSequenceMPS, MO extends MesureMPS, VO extends ValeurMPS>
        implements Serializable {

    /**
     * The Constant ATTRIBUTE_JPA_ID.
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The version @link(VersionFile).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    VersionFile version;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = SuiviParcelle.class)
    @JoinColumn(name = SuiviParcelle.ID_JPA, referencedColumnName = SuiviParcelle.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SuiviParcelle suiviParcelle;

    /**
     * The date @link(Date).
     */
    @Column(name = SequenceMPS.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate date;

    /**
     * Instantiates a new sequence mps.
     */
    public SequenceMPS() {
        super();
    }

    /**
     * Instantiates a new sequence mps.
     *
     * @param version       the version
     * @param suiviParcelle the suivi parcelle
     * @param date          the date
     */
    public SequenceMPS(final VersionFile version, final SuiviParcelle suiviParcelle, final LocalDate date) {
        super();
        this.setVersion(version);
        this.setSuiviParcelle(suiviParcelle);
        this.setDate(date);
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDate date) {
        this.date = date;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the sous sequences mps.
     *
     * @return the sous sequences mps
     */
    public abstract List<SSO> getSousSequencesMPS();

    /**
     * Sets the sous sequences mps.
     *
     * @param sousSequencesMPS the new sous sequences mps
     */
    public abstract void setSousSequencesMPS(List<SSO> sousSequencesMPS);

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle
     */
    public void setSuiviParcelle(final SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public VersionFile getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final VersionFile version) {
        this.version = version;
    }
}
