/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps;

import org.inra.ecoinfo.acbb.extraction.IDateFormParameter;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.IParameter;

import java.util.List;
import java.util.Map;
import java.util.SortedSet;

/**
 * The Interface IMPSParameter.
 */
public interface IMPSParameter extends IParameter {

    /**
     * The Constant MPS_EXTRACTION_TYPE_CODE.
     */
    String MPS_EXTRACTION_TYPE_CODE = "mesure_physique_du_sol";

    /**
     * The Constant EUROPEAN_FORMAT.
     */
    int EUROPEAN_FORMAT = 1;

    /**
     * The Constant CLASSIC_FORMAT.
     */
    int CLASSIC_FORMAT = 2;

    /**
     * Gets the columns.
     *
     * @return the columns
     */
    Map<VersionFile, List<String>> getColumns();

    /**
     * Gets the dates years continuous form param vo.
     *
     * @return the dates years continuous form param vo
     */
    IDateFormParameter getDatesYearsContinuousFormParamVO();

    /**
     * Sets the dates years continuous form param vo.
     *
     * @param datesYearsContinuousFormParamVO the new dates years continuous form param vo
     */
    void setDatesYearsContinuousFormParamVO(
            IDateFormParameter datesYearsContinuousFormParamVO);

    /**
     * Gets the selected depth.
     *
     * @return the selected depth
     */
    SortedSet<Integer> getSelectedDepth();

    /**
     * Gets the selected mps variables.
     *
     * @return the selected mps variables
     */
    List<VariableACBB> getSelectedMPSVariables();

    /**
     * Sets the selected mps variables.
     *
     * @param selectedMPSVariables the new selected mps variables
     */
    void setSelectedMPSVariables(List<VariableACBB> selectedMPSVariables);

    /**
     * Gets the selecteds traitements.
     *
     * @return the selecteds traitements
     */
    List<TraitementProgramme> getSelectedsTraitements();

    /**
     * Sets the selecteds traitements.
     *
     * @param selectedsTraitements the new selecteds traitements
     */
    void setSelectedsTraitements(List<TraitementProgramme> selectedsTraitements);

    /**
     * Sets the selecteds depths.
     *
     * @param depths the new selecteds depths
     */
    void setSelectedsDepths(SortedSet<Integer> depths);

}
