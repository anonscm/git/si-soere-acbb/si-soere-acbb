/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;

import static javax.persistence.CascadeType.*;

/**
 * The Class ValeurSMP.
 */
@Entity
@Table(name = ValeurSMP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        MesureSMP.ID_JPA, RealNode.ID_JPA, ValeurMPS.ATTRIBUTE_JPA_NUM_REPETITION}),
        indexes = {
                @Index(name = "vsmp_msmp_idx", columnList = MesureSMP.ID_JPA),
                @Index(columnList = RealNode.ID_JPA, name = "vsmp_rn__idx")})
@PrimaryKeyJoinColumn(name = ValeurMPS.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurSMP.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = ValeurMPS.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurSMP.ID_JPA))})
public class ValeurSMP extends ValeurMPS<SequenceSMP, SousSequenceSMP, MesureSMP, ValeurSMP> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vsmp_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "valeur_smp_vsmp";

    /**
     * The Constant ATTRIBUTE_JPA_MEASURE_SMP.
     */
    public static final String ATTRIBUTE_JPA_MEASURE_SMP = "mesureSMP";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesure mps @link(MesureSMP).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureSMP.ID_JPA, referencedColumnName = MesureSMP.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureSMP mesureMPS;

    /**
     * Instantiates a new valeur smp.
     */
    public ValeurSMP() {
        super();
    }

    /**
     * Instantiates a new valeur smp.
     *
     * @param value        the value
     * @param qualityClass the quality class
     */
    public ValeurSMP(final Float value, final Integer qualityClass) {
        super(value, qualityClass);
    }

    /**
     * Gets the mesure mps @link(MesureSMP).
     *
     * @return the mesure mps @link(MesureSMP)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS#getMesureMPS()
     */
    @Override
    public MesureSMP getMesureMPS() {
        return this.mesureMPS;
    }

    /**
     * Sets the mesure mps @link(MesureSMP).
     *
     * @param mesureMPS the new mesure mps @link(MesureSMP)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS#setMesureMPS(org.inra
     * .ecoinfo.acbb.dataset.mps.entity.MesureMPS)
     */
    @Override
    public void setMesureMPS(final MesureSMP mesureMPS) {
        this.mesureMPS = mesureMPS;
    }
}
