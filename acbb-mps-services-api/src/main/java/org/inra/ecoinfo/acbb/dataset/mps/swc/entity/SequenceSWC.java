package org.inra.ecoinfo.acbb.dataset.mps.swc.entity;

import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SequenceSWC.
 */
@Entity
@Table(name = SequenceSWC.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        VersionFile.ID_JPA, SuiviParcelle.ID_JPA, SequenceMPS.ATTRIBUTE_JPA_DATE}),
        indexes = {
                @Index(name = "sswc_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "sswc_spa_idx", columnList = SuiviParcelle.ID_JPA),
                @Index(name = "sswc_date_idx", columnList = SequenceSWC.ATTRIBUTE_JPA_DATE)})
@PrimaryKeyJoinColumn(name = SequenceMPS.ATTRIBUTE_JPA_ID, referencedColumnName = SequenceSWC.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = SequenceMPS.ATTRIBUTE_JPA_ID, column = @Column(name = SequenceSWC.ID_JPA))})
public class SequenceSWC extends SequenceMPS<SequenceSWC, SousSequenceSWC, MesureSWC, ValeurSWC> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sswc_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sequence_swc_sswc";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The sous sequences mps @link(List<SousSequenceSWC>).
     */
    @OneToMany(mappedBy = SousSequenceMPS.ATTRIBUTE_JPA_SEQUENCE_MPS, cascade = {PERSIST, MERGE,
            REFRESH})
    List<SousSequenceSWC> sousSequencesMPS = new LinkedList();

    /**
     * Instantiates a new sequence swc.
     */
    public SequenceSWC() {
        super();
    }

    /**
     * Instantiates a new sequence swc.
     *
     * @param version       the version
     * @param suiviParcelle the suivi parcelle
     * @param date          the date
     */
    public SequenceSWC(final VersionFile version, final SuiviParcelle suiviParcelle, final LocalDate date) {
        super(version, suiviParcelle, date);
    }

    /**
     * Gets the sous sequences mps @link(List<SousSequenceSWC>).
     *
     * @return the sous sequences mps @link(List<SousSequenceSWC>)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS#getSousSequencesMPS
     * ()
     */
    @Override
    public List<SousSequenceSWC> getSousSequencesMPS() {
        return this.sousSequencesMPS;
    }

    /**
     * Sets the sous sequences mps @link(List<SousSequenceSWC>).
     *
     * @param sousSequencesMPS the new sous sequences mps
     * @link(List<SousSequenceSWC>) @see
     * org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS#setSousSequencesMPS
     * (java.util.List)
     */
    @Override
    public void setSousSequencesMPS(final List<SousSequenceSWC> sousSequencesMPS) {
        this.sousSequencesMPS = sousSequencesMPS;
    }
}
