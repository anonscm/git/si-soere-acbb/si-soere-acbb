/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.time.DateTimeException;
import java.util.Map;

/**
 * The Interface IRequestPropertiesMPS.
 */
public interface IRequestPropertiesMPS extends IRequestPropertiesACBB {

    /**
     * Adds the expected colum.
     *
     * @param index            the index
     * @param name             the name
     * @param hasQualityClass  the has quality class
     * @param profondeur       the profondeur
     * @param nombreRepetition the nombre repetition
     * @param minValue         the min value
     * @param maxValue         the max value
     */
    void addExpectedColum(int index, String name, boolean hasQualityClass, int profondeur,
                          int nombreRepetition, Float minValue, Float maxValue);

    /**
     * Date to string.
     *
     * @param dateString
     * @param timeString
     * @return the string
     * @throws DateTimeException the parse exception
     */
    String dateToStringYYYYMMJJHHMMSS(final String dateString, String timeString);

    /**
     * Gets the datatype.
     *
     * @return the datatype
     */
    String getDatatype();

    /**
     * Sets the datatype.
     *
     * @param datatype the new datatype
     */
    void setDatatype(String datatype);

    /**
     * Gets the expected colums.
     *
     * @return the expected colums
     */
    Map<Integer, ExpectedColumn> getExpectedColums();

    /**
     * Gets the possibles colums.
     *
     * @return the possibles colums
     */
    Map<String, ExpectedColumn> getPossiblesColums();

    /**
     * Inits the dataset descriptor.
     *
     * @param datasetDescriptor the dataset descriptor
     */
    void initDatasetDescriptor(DatasetDescriptorACBB datasetDescriptor);

    /**
     * Test nom fichier.
     *
     * @param nomfichier        the nomfichier
     * @param badsFormatsReport the bads formats report
     */
    void testNomFichier(String nomfichier, BadsFormatsReport badsFormatsReport);

}
