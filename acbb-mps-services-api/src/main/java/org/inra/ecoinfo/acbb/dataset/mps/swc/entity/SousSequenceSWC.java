package org.inra.ecoinfo.acbb.dataset.mps.swc.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SousSequenceSWC.
 */
@Entity
@Table(name = SousSequenceSWC.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceSWC.ID_JPA, SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR}),
        indexes = {
                @Index(name = "ssswc_sswc_idx", columnList = SequenceSWC.ID_JPA),
                @Index(name = "ssswc_profondeur_idx", columnList = SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR)})
@PrimaryKeyJoinColumn(name = SousSequenceMPS.ATTRIBUTE_JPA_ID, referencedColumnName = SousSequenceSWC.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = SousSequenceMPS.ATTRIBUTE_JPA_ID, column = @Column(name = SousSequenceSWC.ID_JPA))})
public class SousSequenceSWC extends
        SousSequenceMPS<SequenceSWC, SousSequenceSWC, MesureSWC, ValeurSWC> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "ssswc_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sous_sequence_swc_ssswc";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesures swc @link(List<MesureSWC>).
     */
    @OneToMany(mappedBy = MesureMPS.ATTRIBUTE_JPA_SOUS_SEQUENCE_MPS, cascade = {PERSIST, MERGE,
            REFRESH})
    List<MesureSWC> mesuresSWC = new LinkedList();

    /**
     * The sequence mps @link(SequenceSWC).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SequenceSWC.ID_JPA, referencedColumnName = SequenceSWC.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceSWC sequenceMPS;

    /**
     * Instantiates a new sous sequence swc.
     */
    public SousSequenceSWC() {
        super();
    }

    /**
     * Instantiates a new sous sequence swc.
     *
     * @param sequence   the sequence
     * @param profondeur the profondeur
     */
    public SousSequenceSWC(final SequenceSWC sequence, final int profondeur) {
        super(sequence, profondeur);
    }

    /**
     * Gets the mesures mps.
     *
     * @return the mesures mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#getMesuresMPS()
     */
    @Override
    public List<MesureSWC> getMesuresMPS() {
        return this.mesuresSWC;
    }

    /**
     * Sets the mesures mps.
     *
     * @param mesuresMPS the new mesures mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#setMesuresMPS
     * (java.util.List)
     */
    @Override
    public void setMesuresMPS(final List<MesureSWC> mesuresMPS) {
        this.mesuresSWC = mesuresMPS;

    }

    /**
     * Gets the sequence mps @link(SequenceSWC).
     *
     * @return the sequence mps @link(SequenceSWC)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#getSequenceMPS()
     */
    @Override
    public SequenceSWC getSequenceMPS() {
        return this.sequenceMPS;
    }

    /**
     * Sets the sequence mps @link(SequenceSWC).
     *
     * @param sequenceSWC the new sequence mps @link(SequenceSWC)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#setSequenceMPS
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS)
     */
    @Override
    public void setSequenceMPS(final SequenceSWC sequenceSWC) {
        this.sequenceMPS = sequenceSWC;

    }
}
