package org.inra.ecoinfo.acbb.dataset.mps;

import org.inra.ecoinfo.utils.Column;

import java.io.Serializable;

/**
 * The Class ExpectedColumn.
 */
public class ExpectedColumn extends Column implements Serializable {

    /**
     * The Constant PROPERTY_CST_VARIABLE_TYPE.
     */
    public static final String PROPERTY_CST_VARIABLE_TYPE = "variable";
    private static final long serialVersionUID = 1L;

    /**
     * The has quality class @link(boolean).
     */
    boolean hasQualityClass = false;

    /**
     * The profondeur @link(int).
     */
    int profondeur;

    /**
     * The numero repetition @link(int).
     */
    int numeroRepetition;

    /**
     * The min value @link(Float).
     */
    Float minValue;

    /**
     * The max value @link(Float).
     */
    Float maxValue;

    /**
     * Instantiates a new expected column.
     */
    public ExpectedColumn() {
        super();
    }

    /**
     * Instantiates a new expected column.
     *
     * @param column the column
     */
    public ExpectedColumn(final Column column) {
        super(column);
    }

    /**
     * Instantiates a new expected column.
     *
     * @param name             the name
     * @param hasQualityClass  the has quality class
     * @param profondeur       the profondeur
     * @param numeroRepetition the numero repetition
     * @param minValue         the min value
     * @param maxValue         the max value
     */
    public ExpectedColumn(final String name, final boolean hasQualityClass, final int profondeur,
                          final int numeroRepetition, final Float minValue, final Float maxValue) {
        super();
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.setName(name);
        this.setFlag(true);
        this.setFlagType(ExpectedColumn.PROPERTY_CST_VARIABLE_TYPE);
        this.setInull(true);
        this.setFormatType("float");
        this.setHasQualityClass(hasQualityClass);
        this.profondeur = profondeur;
        this.numeroRepetition = numeroRepetition;
    }

    /**
     * Gets the max value.
     *
     * @return the max value
     */
    public Float getMaxValue() {
        return this.maxValue;
    }

    /**
     * Gets the min value.
     *
     * @return the min value
     */
    public Float getMinValue() {
        return this.minValue;
    }

    /**
     * Gets the numero repetition.
     *
     * @return the numero repetition
     */
    public int getNumeroRepetition() {
        return this.numeroRepetition;
    }

    /**
     * Gets the profondeur.
     *
     * @return the profondeur
     */
    public int getProfondeur() {
        return this.profondeur;
    }

    /**
     * Checks for quality class.
     *
     * @return true, if successful
     */
    public boolean hasQualityClass() {
        return this.hasQualityClass;
    }

    /**
     * Sets the checks for quality class.
     *
     * @param hasQualityClass the new checks for quality class
     */
    public void setHasQualityClass(final boolean hasQualityClass) {
        this.hasQualityClass = hasQualityClass;
    }
}
