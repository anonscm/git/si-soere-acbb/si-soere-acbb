package org.inra.ecoinfo.acbb.synthesis.smp;

import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * The Class SynthesisValue.
 */
@Entity(name = "SmpSynthesisValue")
@Table(indexes = {
        @Index(name = "SmpSynthesisValue_site_idx", columnList = "site")
        ,
        @Index(name = "SmpSynthesisValue_site_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends SynthesisValueWithParcelle {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    public SynthesisValue() {
    }

    public SynthesisValue(final LocalDate date, final String sitePath, final String parcelleCode, final String variable,
                          final Double valueFloat, long idNode) {
        super(parcelleCode, date == null ? null : date.atStartOfDay(), sitePath, variable, valueFloat == null ? null : valueFloat.floatValue(), null, idNode, false);
    }
}
