package org.inra.ecoinfo.acbb.dataset.mps.smp.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SousSequenceSMP.
 */
@Entity
@Table(name = SousSequenceSMP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceSMP.ID_JPA, SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR}),
        indexes = {
                @Index(name = "sssmp_ssmp_idx", columnList = SequenceSMP.ID_JPA),
                @Index(name = "sssmp_profondeur_idx", columnList = SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR)})
@PrimaryKeyJoinColumn(name = SousSequenceMPS.ATTRIBUTE_JPA_ID, referencedColumnName = SousSequenceSMP.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = SousSequenceMPS.ATTRIBUTE_JPA_ID, column = @Column(name = SousSequenceSMP.ID_JPA))})
public class SousSequenceSMP extends
        SousSequenceMPS<SequenceSMP, SousSequenceSMP, MesureSMP, ValeurSMP> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sssmp_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sous_sequence_smp_sssmp";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesures smp @link(List<MesureSMP>).
     */
    @OneToMany(mappedBy = MesureMPS.ATTRIBUTE_JPA_SOUS_SEQUENCE_MPS, cascade = {PERSIST, MERGE,
            REFRESH})
    List<MesureSMP> mesuresSMP = new LinkedList();

    /**
     * The sequence mps @link(SequenceSMP).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SequenceSMP.ID_JPA, referencedColumnName = SequenceSMP.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceSMP sequenceMPS;

    /**
     * Instantiates a new sous sequence smp.
     */
    public SousSequenceSMP() {
        super();
    }

    /**
     * Instantiates a new sous sequence smp.
     *
     * @param sequence   the sequence
     * @param profondeur the profondeur
     */
    public SousSequenceSMP(final SequenceSMP sequence, final int profondeur) {
        super(sequence, profondeur);
    }

    /**
     * Gets the mesures mps.
     *
     * @return the mesures mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#getMesuresMPS()
     */
    @Override
    public List<MesureSMP> getMesuresMPS() {
        return this.mesuresSMP;
    }

    /**
     * Sets the mesures mps.
     *
     * @param mesuresMPS the new mesures mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#setMesuresMPS
     * (java.util.List)
     */
    @Override
    public void setMesuresMPS(final List<MesureSMP> mesuresMPS) {
        this.mesuresSMP = mesuresMPS;

    }

    /**
     * Gets the sequence mps @link(SequenceSMP).
     *
     * @return the sequence mps @link(SequenceSMP)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#getSequenceMPS()
     */
    @Override
    public SequenceSMP getSequenceMPS() {
        return this.sequenceMPS;
    }

    /**
     * Sets the sequence mps @link(SequenceSMP).
     *
     * @param sequenceSMP the new sequence mps @link(SequenceSMP)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#setSequenceMPS
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS)
     */
    @Override
    public void setSequenceMPS(final SequenceSMP sequenceSMP) {
        this.sequenceMPS = sequenceSMP;

    }
}
