package org.inra.ecoinfo.acbb.dataset.mps.swc.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class MesureSWC.
 */
@Entity
@Table(name = MesureSWC.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SousSequenceSWC.ID_JPA, MesureSWC.ATTRIBUTE_JPA_TIME}),
        indexes = {
                @Index(name = "mswc_ssswc_idx", columnList = SousSequenceSWC.ID_JPA),
                @Index(name = "mswc_time_idx", columnList = MesureSWC.ATTRIBUTE_JPA_TIME)})
@PrimaryKeyJoinColumn(name = MesureMPS.ATTRIBUTE_JPA_ID, referencedColumnName = MesureSWC.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = MesureMPS.ATTRIBUTE_JPA_ID, column = @Column(name = MesureSWC.ID_JPA))})
public class MesureSWC extends MesureMPS<SequenceSWC, SousSequenceSWC, MesureSWC, ValeurSWC> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mswc_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "mesure_swc_mswc";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The valeurs swc @link(List<ValeurSWC>).
     */
    @OneToMany(mappedBy = ValeurMPS.ATTRIBUTE_JPA_MEASURE_MPS, cascade = {PERSIST, MERGE, REFRESH})
    List<ValeurSWC> valeursSWC = new LinkedList();

    /**
     * The sous sequence mps @link(SousSequenceSWC).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SousSequenceSWC.ID_JPA, referencedColumnName = SousSequenceSWC.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    SousSequenceSWC sousSequenceMPS;

    /**
     * Instantiates a new mesure swc.
     */
    public MesureSWC() {
        super();
    }

    /**
     * Instantiates a new mesure swc.
     *
     * @param sousSequence       the sous sequence
     * @param time               the time
     * @param originalLineNumber the original line number
     */
    public MesureSWC(final SousSequenceSWC sousSequence, final LocalTime time,
                     final int originalLineNumber) {
        super(sousSequence, time, originalLineNumber);
    }

    /**
     * Gets the sous sequence mps @link(SousSequenceSWC).
     *
     * @return the sous sequence mps @link(SousSequenceSWC)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#getSousSequenceMPS()
     */
    @Override
    public SousSequenceSWC getSousSequenceMPS() {
        return this.sousSequenceMPS;
    }

    /**
     * Sets the sous sequence mps @link(SousSequenceSWC).
     *
     * @param sousSequenceSWC the new sous sequence mps @link(SousSequenceSWC)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#setSousSequenceMPS
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS)
     */
    @Override
    public void setSousSequenceMPS(final SousSequenceSWC sousSequenceSWC) {
        this.sousSequenceMPS = sousSequenceSWC;
    }

    /**
     * Gets the valeurs mps.
     *
     * @return the valeurs mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#getValeursMPS()
     */
    @Override
    public List<ValeurSWC> getValeursMPS() {
        return this.valeursSWC;
    }

    /**
     * Sets the valeurs mps.
     *
     * @param valeurMPS the new valeurs mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#setValeursMPS(java
     * .util.List)
     */
    @Override
    public void setValeursMPS(final List<ValeurSWC> valeurMPS) {
        this.valeursSWC = valeurMPS;
    }
}
