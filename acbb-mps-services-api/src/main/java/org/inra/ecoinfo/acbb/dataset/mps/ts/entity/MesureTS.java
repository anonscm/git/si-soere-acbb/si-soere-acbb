package org.inra.ecoinfo.acbb.dataset.mps.ts.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class MesureTS.
 */
@Entity
@Table(name = MesureTS.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SousSequenceTS.ID_JPA, MesureTS.ATTRIBUTE_JPA_TIME}),
        indexes = {
                @Index(name = "mts_ssts_idx", columnList = SousSequenceTS.ID_JPA),
                @Index(name = "mts_time_idx", columnList = MesureTS.ATTRIBUTE_JPA_TIME)})
@PrimaryKeyJoinColumn(name = MesureMPS.ATTRIBUTE_JPA_ID, referencedColumnName = MesureTS.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = MesureMPS.ATTRIBUTE_JPA_ID, column = @Column(name = MesureTS.ID_JPA))})
public class MesureTS extends MesureMPS<SequenceTS, SousSequenceTS, MesureTS, ValeurTS> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mts_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "mesure_ts_mts";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The valeurs ts @link(List<ValeurTS>).
     */
    @OneToMany(mappedBy = ValeurMPS.ATTRIBUTE_JPA_MEASURE_MPS, cascade = {PERSIST, MERGE, REFRESH})
    List<ValeurTS> valeursTS = new LinkedList();

    /**
     * The sous sequence mps @link(SousSequenceTS).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SousSequenceTS.ID_JPA, referencedColumnName = SousSequenceTS.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    SousSequenceTS sousSequenceMPS;

    /**
     * Instantiates a new mesure ts.
     */
    public MesureTS() {
        super();
    }

    /**
     * Instantiates a new mesure ts.
     *
     * @param sousSequence       the sous sequence
     * @param time               the time
     * @param originalLineNumber the original line number
     */
    public MesureTS(final SousSequenceTS sousSequence, final LocalTime time, final int originalLineNumber) {
        super(sousSequence, time, originalLineNumber);
    }

    /**
     * Gets the sous sequence mps @link(SousSequenceTS).
     *
     * @return the sous sequence mps @link(SousSequenceTS)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#getSousSequenceMPS()
     */
    @Override
    public SousSequenceTS getSousSequenceMPS() {
        return this.sousSequenceMPS;
    }

    /**
     * Sets the sous sequence mps @link(SousSequenceTS).
     *
     * @param sousSequenceTS the new sous sequence mps @link(SousSequenceTS)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#setSousSequenceMPS
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS)
     */
    @Override
    public void setSousSequenceMPS(final SousSequenceTS sousSequenceTS) {
        this.sousSequenceMPS = sousSequenceTS;
    }

    /**
     * Gets the valeurs mps.
     *
     * @return the valeurs mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#getValeursMPS()
     */
    @Override
    public List<ValeurTS> getValeursMPS() {
        return this.valeursTS;
    }

    /**
     * Sets the valeurs mps.
     *
     * @param valeurMPS the new valeurs mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS#setValeursMPS(java
     * .util.List)
     */
    @Override
    public void setValeursMPS(final List<ValeurTS> valeurMPS) {
        this.valeursTS = valeurMPS;
    }
}
