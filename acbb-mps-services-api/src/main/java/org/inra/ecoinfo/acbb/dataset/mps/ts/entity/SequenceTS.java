package org.inra.ecoinfo.acbb.dataset.mps.ts.entity;

import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SequenceTS.
 */
@Entity
@Table(name = SequenceTS.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        VersionFile.ID_JPA, SuiviParcelle.ID_JPA, SequenceMPS.ATTRIBUTE_JPA_DATE}),
        indexes = {
                @Index(name = "sts_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "sts_spa_idx", columnList = SuiviParcelle.ID_JPA),
                @Index(name = "sts_date_idx", columnList = SequenceTS.ATTRIBUTE_JPA_DATE)})
@PrimaryKeyJoinColumn(name = SequenceMPS.ATTRIBUTE_JPA_ID, referencedColumnName = SequenceTS.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = SequenceMPS.ATTRIBUTE_JPA_ID, column = @Column(name = SequenceTS.ID_JPA))})
public class SequenceTS extends SequenceMPS<SequenceTS, SousSequenceTS, MesureTS, ValeurTS> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sts_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sequence_ts_sts";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The sous sequences mps @link(List<SousSequenceTS>).
     */
    @OneToMany(mappedBy = SousSequenceMPS.ATTRIBUTE_JPA_SEQUENCE_MPS, cascade = {PERSIST, MERGE,
            REFRESH})
    List<SousSequenceTS> sousSequencesMPS = new LinkedList();

    /**
     * Instantiates a new sequence ts.
     */
    public SequenceTS() {
        super();
    }

    /**
     * Instantiates a new sequence ts.
     *
     * @param version       the version
     * @param suiviParcelle the suivi parcelle
     * @param date          the date
     */
    public SequenceTS(final VersionFile version, final SuiviParcelle suiviParcelle, final LocalDate date) {
        super(version, suiviParcelle, date);
    }

    /**
     * Gets the sous sequences mps @link(List<SousSequenceTS>).
     *
     * @return the sous sequences mps @link(List<SousSequenceTS>)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS#getSousSequencesMPS
     * ()
     */
    @Override
    public List<SousSequenceTS> getSousSequencesMPS() {
        return this.sousSequencesMPS;
    }

    /**
     * Sets the sous sequences mps @link(List<SousSequenceTS>).
     *
     * @param sousSequencesMPS the new sous sequences mps
     * @link(List<SousSequenceTS>) @see
     * org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS#setSousSequencesMPS
     * (java.util.List)
     */
    @Override
    public void setSousSequencesMPS(final List<SousSequenceTS> sousSequencesMPS) {
        this.sousSequencesMPS = sousSequencesMPS;
    }
}
