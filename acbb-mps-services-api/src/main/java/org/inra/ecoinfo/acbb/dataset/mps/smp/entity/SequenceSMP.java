package org.inra.ecoinfo.acbb.dataset.mps.smp.entity;

import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SequenceSMP.
 */
@Entity
@Table(name = SequenceSMP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        VersionFile.ID_JPA, SuiviParcelle.ID_JPA, SequenceMPS.ATTRIBUTE_JPA_DATE}),
        indexes = {
                @Index(name = "ssmp_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "ssmp_spa_idx", columnList = SuiviParcelle.ID_JPA),
                @Index(name = "ssmp_date_idx", columnList = SequenceMPS.ATTRIBUTE_JPA_DATE)})
@PrimaryKeyJoinColumn(name = SequenceMPS.ATTRIBUTE_JPA_ID, referencedColumnName = SequenceSMP.ID_JPA)
@AttributeOverrides({@AttributeOverride(name = SequenceMPS.ATTRIBUTE_JPA_ID, column = @Column(name = SequenceSMP.ID_JPA))})
public class SequenceSMP extends SequenceMPS<SequenceSMP, SousSequenceSMP, MesureSMP, ValeurSMP> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "ssmp_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sequence_smp_ssmp";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The sous sequences mps @link(List<SousSequenceSMP>).
     */
    @OneToMany(mappedBy = SousSequenceMPS.ATTRIBUTE_JPA_SEQUENCE_MPS, cascade = {PERSIST, MERGE,
            REFRESH})
    List<SousSequenceSMP> sousSequencesMPS = new LinkedList();

    /**
     * Instantiates a new sequence smp.
     */
    public SequenceSMP() {
        super();
    }

    /**
     * Instantiates a new sequence smp.
     *
     * @param version       the version
     * @param suiviParcelle the suivi parcelle
     * @param date          the date
     */
    public SequenceSMP(final VersionFile version, final SuiviParcelle suiviParcelle, final LocalDate date) {
        super(version, suiviParcelle, date);
    }

    /**
     * Gets the sous sequences mps @link(List<SousSequenceSMP>).
     *
     * @return the sous sequences mps @link(List<SousSequenceSMP>)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS#getSousSequencesMPS ()
     */
    @Override
    public List<SousSequenceSMP> getSousSequencesMPS() {
        return this.sousSequencesMPS;
    }

    /**
     * Sets the sous sequences mps @link(List<SousSequenceSMP>).
     *
     * @param sousSequencesMPS the new sous sequences mps
     * @link(List<SousSequenceSMP>) @see
     * org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS#setSousSequencesMPS
     * (java.util.List)
     */
    @Override
    public void setSousSequencesMPS(final List<SousSequenceSMP> sousSequencesMPS) {
        this.sousSequencesMPS = sousSequencesMPS;
    }
}
