package org.inra.ecoinfo.acbb.dataset.mps.swc.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;

import static javax.persistence.CascadeType.*;

/**
 * The Class ValeurSWC.
 */
@Entity
@Table(name = ValeurSWC.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        MesureSWC.ID_JPA, RealNode.ID_JPA, ValeurMPS.ATTRIBUTE_JPA_NUM_REPETITION}),
        indexes = {
                @Index(name = "vswc_mswc_idx", columnList = MesureSWC.ID_JPA),
                @Index(columnList = RealNode.ID_JPA, name = "vswc_rn__idx")})
@PrimaryKeyJoinColumn(name = ValeurMPS.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurSWC.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = ValeurMPS.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurSWC.ID_JPA))})
public class ValeurSWC extends ValeurMPS<SequenceSWC, SousSequenceSWC, MesureSWC, ValeurSWC> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vswc_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "valeur_swc_vswc";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesure mps @link(MesureSWC).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureSWC.ID_JPA, referencedColumnName = MesureSWC.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureSWC mesureMPS;

    /**
     * Instantiates a new valeur swc.
     */
    public ValeurSWC() {
        super();
    }

    /**
     * Instantiates a new valeur swc.
     *
     * @param value        the value
     * @param qualityClass the quality class
     */
    public ValeurSWC(final Float value, final Integer qualityClass) {
        super(value, qualityClass);
    }

    /**
     * Gets the mesure mps @link(MesureSWC).
     *
     * @return the mesure mps @link(MesureSWC)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS#getMesureMPS()
     */
    @Override
    public MesureSWC getMesureMPS() {
        return this.mesureMPS;
    }

    /**
     * Sets the mesure mps @link(MesureSWC).
     *
     * @param mesureMPS the new mesure mps @link(MesureSWC)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS#setMesureMPS(org.inra
     * .ecoinfo.acbb.dataset.mps.entity.MesureMPS)
     */
    @Override
    public void setMesureMPS(final MesureSWC mesureMPS) {
        this.mesureMPS = mesureMPS;
    }
}
