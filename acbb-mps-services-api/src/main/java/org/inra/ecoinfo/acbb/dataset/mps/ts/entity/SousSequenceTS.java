package org.inra.ecoinfo.acbb.dataset.mps.ts.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SousSequenceTS.
 */
@Entity
@Table(name = SousSequenceTS.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceTS.ID_JPA, SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR}),
        indexes = {
                @Index(name = "ssts_sts_idx", columnList = SequenceTS.ID_JPA),
                @Index(name = "sts_profondeur_idx", columnList = SousSequenceMPS.ATTRIBUTE_JPA_PROFONDEUR)})
@PrimaryKeyJoinColumn(name = SousSequenceMPS.ATTRIBUTE_JPA_ID, referencedColumnName = SousSequenceTS.ID_JPA)
@AttributeOverrides({
        @AttributeOverride(name = SousSequenceMPS.ATTRIBUTE_JPA_ID, column = @Column(name = SousSequenceTS.ID_JPA))})
public class SousSequenceTS extends SousSequenceMPS<SequenceTS, SousSequenceTS, MesureTS, ValeurTS> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "ssts_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sous_sequence_ts_ssts";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesures ts @link(List<MesureTS>).
     */
    @OneToMany(mappedBy = MesureMPS.ATTRIBUTE_JPA_SOUS_SEQUENCE_MPS, cascade = {PERSIST, MERGE,
            REFRESH})
    List<MesureTS> mesuresTS = new LinkedList();

    /**
     * The sequence mps @link(SequenceTS).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SequenceTS.ID_JPA, referencedColumnName = SequenceTS.ID_JPA)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceTS sequenceMPS;

    /**
     * Instantiates a new sous sequence ts.
     */
    public SousSequenceTS() {
        super();
    }

    /**
     * Instantiates a new sous sequence ts.
     *
     * @param sequence   the sequence
     * @param profondeur the profondeur
     */
    public SousSequenceTS(final SequenceTS sequence, final int profondeur) {
        super(sequence, profondeur);
    }

    /**
     * Gets the mesures mps.
     *
     * @return the mesures mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#getMesuresMPS()
     */
    @Override
    public List<MesureTS> getMesuresMPS() {
        return this.mesuresTS;
    }

    /**
     * Sets the mesures mps.
     *
     * @param mesuresMPS the new mesures mps
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#setMesuresMPS
     * (java.util.List)
     */
    @Override
    public void setMesuresMPS(final List<MesureTS> mesuresMPS) {
        this.mesuresTS = mesuresMPS;

    }

    /**
     * Gets the sequence mps @link(SequenceTS).
     *
     * @return the sequence mps @link(SequenceTS)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#getSequenceMPS()
     */
    @Override
    public SequenceTS getSequenceMPS() {
        return this.sequenceMPS;
    }

    /**
     * Sets the sequence mps @link(SequenceTS).
     *
     * @param sequenceTS the new sequence mps @link(SequenceTS)
     * @see org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS#setSequenceMPS
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS)
     */
    @Override
    public void setSequenceMPS(final SequenceTS sequenceTS) {
        this.sequenceMPS = sequenceTS;

    }
}
