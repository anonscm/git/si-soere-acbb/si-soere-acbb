/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.entity;

import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The Class ValeurMPS.
 *
 * @param <SZ>  the generic type
 * @param <SSZ> the generic type
 * @param <MZ>  the generic type
 * @param <VZ>  the generic type
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public abstract class ValeurMPS<SZ extends SequenceMPS, SSZ extends SousSequenceMPS, MZ extends MesureMPS, VZ extends ValeurMPS>
        implements Comparable<ValeurMPS>, Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vmps_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "mesure_mps_vmps";

    /**
     * The Constant ATTRIBUTE_JPA_ID.
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     * The Constant ATTRIBUTE_JPA_VALUE.
     */
    public static final String ATTRIBUTE_JPA_VALUE = "valeur";

    /**
     * The Constant ATTRIBUTE_JPA_QUALITY_CLASS.
     */
    public static final String ATTRIBUTE_JPA_QUALITY_CLASS = "qc";

    /**
     * The Constant ATTRIBUTE_JPA_NUM_REPETITION.
     */
    public static final String ATTRIBUTE_JPA_NUM_REPETITION = "num_repetition";
    /**
     * The Constant ATTRIBUTE_JPA_MEASURE_MPS.
     */
    public static final String ATTRIBUTE_JPA_MEASURE_MPS = "mesureMPS";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s";
    /**
     * The id <long>.
     */
    @Id
    @Column(name = ValeurMPS.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The value @link(Float).
     */
    @Column(name = ValeurMPS.ATTRIBUTE_JPA_VALUE, nullable = false)
    Float value;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    /**
     * The quality class @link(Integer).
     */
    @Column(name = ValeurMPS.ATTRIBUTE_JPA_QUALITY_CLASS, nullable = true)
    Integer qualityClass = null;

    /**
     * The num repetition @link(int).
     */
    @Column(name = ValeurMPS.ATTRIBUTE_JPA_NUM_REPETITION, nullable = false)
    int numRepetition;

    /**
     * The column header @link(ColumnHeader).
     */
    @Transient
    ColumnHeader columnHeader = null;

    /**
     * Instantiates a new valeur mps.
     */
    public ValeurMPS() {
        super();
        this.columnHeader = new ColumnHeader();
    }

    /**
     * Instantiates a new valeur mps.
     *
     * @param value        the value
     * @param qualityClass the quality class
     */
    public ValeurMPS(final Float value, final Integer qualityClass) {
        this.value = value;
        this.qualityClass = qualityClass;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(ValeurMPS) the o
     */
    @SuppressWarnings("unchecked")
    @Override
    public int compareTo(final ValeurMPS o) {
        if (o == null) {
            return -1;
        }
        return this.getColumnHeader().compareTo(o.getColumnHeader());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurMPS<?, ?, ?, ?> other = (ValeurMPS<?, ?, ?, ?>) obj;
        return Objects.equals(this.columnHeader, other.columnHeader);
    }

    /**
     * Gets the column header.
     *
     * @return the column header
     */
    public ColumnHeader getColumnHeader() {
        if (this.columnHeader == null) {
            this.columnHeader = new ColumnHeader();
        }
        return this.columnHeader;
    }

    /**
     * Gets the column name.
     *
     * @return the column name
     */
    public String getColumnName() {
        return this.getColumnHeader().toString();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesure mps.
     *
     * @return the mesure mps
     */
    public abstract MZ getMesureMPS();

    /**
     * Sets the mesure mps.
     *
     * @param mesureMPS the new mesure mps
     */
    public abstract void setMesureMPS(MZ mesureMPS);

    /**
     * Gets the num repetition.
     *
     * @return the num repetition
     */
    public int getNumRepetition() {
        return this.numRepetition;
    }

    /**
     * Sets the num repetition.
     *
     * @param numRepetition the new num repetition
     */
    public void setNumRepetition(final int numRepetition) {
        this.numRepetition = numRepetition;
    }

    /**
     * Gets the quality class.
     *
     * @return the quality class
     */
    public Integer getQualityClass() {
        return this.qualityClass;
    }

    /**
     * Sets the quality class.
     *
     * @param qualityClass the new quality class
     */
    public void setQualityClass(final Integer qualityClass) {
        this.qualityClass = qualityClass;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public VariableACBB getVariable() {
        return (VariableACBB) ((DatatypeVariableUniteACBB) realNode.getNodeable()).getVariable();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.columnHeader);
        return hash;
    }

    /**
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     * Sets the variable.
     *
     * @param realNode
     */
    public void setRealNode(final RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     * The Class ColumnHeader.
     */
    public class ColumnHeader implements Comparable<ColumnHeader> {

        /**
         * The has quality class @link(boolean).
         */
        boolean hasQualityClass = false;

        /**
         * Instantiates a new column header.
         */
        public ColumnHeader() {
            super();
            hasQualityClass = getQualityClass() != null;
        }

        /**
         * Compare to.
         *
         * @param columnHeader
         * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
         * @link(ColumnHeader) the column header
         */
        @Override
        public int compareTo(final ColumnHeader columnHeader) {
            try {
                if (this.toString().equals(columnHeader.toString())) {
                    return 0;
                }
                if (getVariable().equals(columnHeader.getSupVariable())
                        && getMesureMPS()
                        .getSousSequenceMPS()
                        .getProfondeur()
                        .equals(columnHeader.getSupMesureMPS().getSousSequenceMPS()
                                .getProfondeur())
                        && getNumRepetition() == columnHeader.getSupNumRepetition()) {
                    return 0;
                }
                if (getVariable().compareTo(columnHeader.getSupVariable()) != 0) {
                    return getVariable().compareTo(columnHeader.getSupVariable());
                }
                if (getNumRepetition() != columnHeader.getSupNumRepetition()) {
                    return getNumRepetition() < columnHeader.getSupNumRepetition() ? -1 : 1;
                }
                return this
                        .getSupMesureMPS()
                        .getSousSequenceMPS()
                        .getProfondeur()
                        .compareTo(
                                columnHeader.getSupMesureMPS().getSousSequenceMPS().getProfondeur());
            } catch (final Exception e) {
                return -1;
            }
        }

        /**
         * Equals.
         *
         * @param obj
         * @return true, if successful @see
         * java.lang.Object#equals(java.lang.Object)
         * @link(Object) the obj
         */
        @Override
        public boolean equals(final Object obj) {
            if (obj == null || !obj.getClass().equals(this.getClass())) {
                return false;
            }
            return this.toString().equals(obj.toString());
        }

        /**
         * Gets the sup id.
         *
         * @return the sup id
         */
        @SuppressWarnings(value = "unused")
        Long getSupId() {
            return getId();
        }

        /**
         * Gets the sup mesure mps.
         *
         * @return the sup mesure mps
         */
        MesureMPS getSupMesureMPS() {
            return getMesureMPS();
        }

        /**
         * Gets the sup num repetition.
         *
         * @return the sup num repetition
         */
        int getSupNumRepetition() {
            return getNumRepetition();
        }

        /**
         * Gets the sup variable.
         *
         * @return the sup variable
         */
        public VariableACBB getSupVariable() {
            return getVariable();
        }

        /**
         * Hash code.
         *
         * @return the int
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return this.toString().hashCode();
        }

        /**
         * Checks for quality class.
         *
         * @return true, if successful
         */
        public boolean hasQualityClass() {
            return this.hasQualityClass;
        }

        /**
         * Sets the checks for quality class.
         *
         * @param hasQualityClass the new checks for quality class
         */
        public void setHasQualityClass(final boolean hasQualityClass) {
            this.hasQualityClass = hasQualityClass;
        }

        /**
         * To ordered string.
         *
         * @return the string
         */
        public String toOrderedString() {
            if (getVariable() == null || getMesureMPS() == null
                    || getMesureMPS().getSousSequenceMPS().getProfondeur() == null) {
                return "nullColumn";
            }
            return String.format("%s_%05d_%05d", getVariable().getAffichage(), getNumRepetition(),
                    getMesureMPS().getSousSequenceMPS().getProfondeur());
        }

        /**
         * To string.
         *
         * @return the string
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            if (getVariable() == null || getMesureMPS() == null
                    || getMesureMPS().getSousSequenceMPS().getProfondeur() == null) {
                return "null";
            }
            return getVariable().getAffichage().concat("_")
                    .concat(Integer.toString(getNumRepetition())).concat("_")
                    .concat(getMesureMPS().getSousSequenceMPS().getProfondeur().toString());
        }
    }
}
