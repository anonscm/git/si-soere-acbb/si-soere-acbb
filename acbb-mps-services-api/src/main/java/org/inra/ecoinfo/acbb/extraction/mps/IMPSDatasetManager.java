/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps;

import org.inra.ecoinfo.acbb.extraction.jsf.ITreatmentManager;
import org.inra.ecoinfo.acbb.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * The Interface IMPSDatasetManager.
 */
public interface IMPSDatasetManager extends ITreatmentManager, IVariableManager<TraitementProgramme> {

    /**
     * Gets the availables depth by traitement and variables.
     *
     * @param selectedTraitements the selected traitements
     * @param intervals           the intervals
     * @param variables           the variables
     * @return the availables depth by traitement and variables
     */
    List<Integer> getAvailablesDepthByTraitementAndVariables(
            List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals,
            List<VariableACBB> variables);

    /**
     * Gets the available traitements.
     *
     * @param intervals the intervals
     * @return the available traitements
     */
    @Override
    List<TraitementProgramme> getAvailableTraitements(List<IntervalDate> intervals);
}
