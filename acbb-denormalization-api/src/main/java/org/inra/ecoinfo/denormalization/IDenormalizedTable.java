/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import java.util.List;

/**
 * @author ptcherniati
 */
public interface IDenormalizedTable {

    /**
     * @return
     */
    List<IDenormalizedTable> getChildrenTables();

    /**
     * @return
     */
    String getSelect();

    /**
     * @return
     */
    String getTableName();
}
