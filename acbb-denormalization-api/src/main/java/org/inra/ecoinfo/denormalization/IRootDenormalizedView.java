/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author ptcherniati
 */
public interface IRootDenormalizedView {

    /**
     * @param view
     * @throws BusinessException
     */
    void addDenormalizedView(IDenormalizedView view) throws BusinessException;

    /**
     * @return
     */
    String getRefreshRequest();

    /**
     * @return
     */
    String initAndGetCreateRequest();

}
