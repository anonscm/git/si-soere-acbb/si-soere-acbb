/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author ptcherniati
 */
public interface IRootDenormalizedTable {

    /**
     * @param table
     * @throws BusinessException
     */
    void addDenormalizedTable(IDenormalizedTable table) throws BusinessException;

    /**
     * @return
     */
    String getCreateRequest();

    /**
     * @return
     */
    String getRefreshRequest();

}
