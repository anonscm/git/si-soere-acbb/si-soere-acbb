package org.inra.ecoinfo.denormalization;

/**
 * @author ptcherniati
 */
public interface IDenormalizationManager {

    /**
     * @param rootDenormalizedTable
     */
    void addRootDenormalizedTable(IRootDenormalizedTable rootDenormalizedTable);

    /**
     * @param rootDenormalizedView
     */
    void addRootDenormalizedView(IRootDenormalizedView rootDenormalizedView);

    /**
     *
     */
    void initTables();

    /**
     *
     */
    void initViews();

    /**
     *
     */
    void refreshTables();

    /**
     *
     */
    void refreshViews();

}
