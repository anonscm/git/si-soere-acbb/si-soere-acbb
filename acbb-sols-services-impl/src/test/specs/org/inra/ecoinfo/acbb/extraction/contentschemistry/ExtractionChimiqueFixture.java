package org.inra.ecoinfo.acbb.extraction.contentschemistry;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.ACBBTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.extraction.AbstractDatesFormParam;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.impl.ContentChemistryBuildOutputDisplay;
import org.inra.ecoinfo.acbb.extraction.soil.impl.SoilParameterVO;
import org.inra.ecoinfo.acbb.extraction.soil.soiltexture.jpa.JPASoilTextureExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ACBBTransactionalTestFixtureExecutionListener.class})
public class ExtractionChimiqueFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    ITraitementDAO traitementDAO;
    ISiteACBBDAO siteACBBDAO;
    ILocalizationManager localizationManager;
    IVariableACBBDAO variableACBBDAO;
    JPASoilTextureExtractionDAO textureExtractionDAO;


    /**
     *
     */
    public ExtractionChimiqueFixture() {
        super();
        MockitoAnnotations.initMocks(this);
        this.traitementDAO = ACBBTransactionalTestFixtureExecutionListener.traitementDAO;
        this.siteACBBDAO = ACBBTransactionalTestFixtureExecutionListener.siteACBBDAO;
        this.localizationManager = ACBBTransactionalTestFixtureExecutionListener.localizationManager;
        this.variableACBBDAO = ACBBTransactionalTestFixtureExecutionListener.variableACBBDAO;
        this.textureExtractionDAO = textureExtractionDAO = (JPASoilTextureExtractionDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("soilTextureExtractionDAO");
    }

    /**
     * @param traitementsSiteCodes
     * @param variablesListNames
     * @param dateDedebut
     * @param datedeFin
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public String extract(String traitementsSiteCodes, String variablesListNames, String methodeListNumbers, String dateDedebut, String datedeFin, String filecomps, String commentaire, String affichage) throws BusinessException, PersistenceException {
        List<SiteACBB> sites = new LinkedList();
        List<TraitementProgramme> traitementProgrammes = new LinkedList();
        for (String traitementSiteCode : traitementsSiteCodes.split(",")) {
            String[] listeTraitementSiteCode = traitementSiteCode.split("@");
            Optional.ofNullable(listeTraitementSiteCode)
                    .map(code -> this.siteACBBDAO.getByPath(code[1]))
                    .filter(s -> s.isPresent())
                    .map(s -> (SiteACBB) s.get())
                    .ifPresent((s) -> {
                        sites.add(s);
                        traitementProgrammes.add(this.traitementDAO.getByNKey(s.getId(), listeTraitementSiteCode[0]).get());
                    });
        }

        final Map<String, Object> metadatasMap = new HashMap();
        metadatasMap.put(TraitementProgramme.class.getSimpleName(), traitementProgrammes);
        List<VariableACBB> variablesChemistry = new LinkedList();
        List<VariableACBB> extraVariables = new LinkedList();
        List<VariableACBB> variablesDensity = new LinkedList();
        List<VariableACBB> variablesStock = new LinkedList();
        List<VariableACBB> variablesCoarse = new LinkedList();
        List<VariableACBB> variablesTexture = new LinkedList();
        for (String variableAffichage : variablesListNames.split(",")) {
            VariableACBB variable = (VariableACBB) this.variableACBBDAO.getByAffichage(variableAffichage).orElse(null);
            if (variable == null) {
                continue;
            }
            switch (variable.getAffichage()) {
                case ContentChemistryBuildOutputDisplay.SOIL_SAMPLES:
                    variablesChemistry.add(variable);
                    extraVariables.add(variable);
                    break;
                case "N_TOT":
                    variablesChemistry.add(variable);
                    break;
                case "C_ORG":
                    variablesChemistry.add(variable);
                    break;
                case "Da_tf":
                    variablesDensity.add(variable);
                    break;
                case "Stock_C_ORG":
                    variablesStock.add(variable);
                    break;
                case "Tx_Vol_EltsGr":
                    variablesCoarse.add(variable);
                    break;
                case "txt":
                    variablesTexture.add(variable);
                    break;
                case AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE:
                    variablesChemistry.add(variable);
                    extraVariables.add(variable);
                    break;
                default:
                    break;
            }
        }
        final TreeMap<MethodeSoilTexture, Set<VariableACBB>> variablesByMethode = this.textureExtractionDAO.getVariablesTextureByMethode(methodeListNumbers.split(","));
        metadatasMap.put(MethodeSoilTexture.class.getSimpleName(), variablesByMethode);
        variablesTexture = variablesByMethode.values().stream()
                .flatMap(Set::stream)
                .distinct()
                .collect(Collectors.toList());
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("sol_texture"), variablesTexture);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("sol_analyse"), variablesChemistry);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("extra_variables"), extraVariables);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("sol_densite"), variablesDensity);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("sol_stock_calcule"), variablesStock);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("sol_elements_grossiers"), variablesCoarse);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("sol_texture"), variablesTexture);
        final DatesFormParamVO datesYearsContinuousFormParamVO = new DatesFormParamVO(this.localizationManager);
        datesYearsContinuousFormParamVO.setPeriods(this.buildNewMapPeriod());
        datesYearsContinuousFormParamVO.getPeriods().get(0).put(AbstractDatesFormParam.START_INDEX, dateDedebut);
        datesYearsContinuousFormParamVO.getPeriods().get(0).put(AbstractDatesFormParam.END_INDEX, datedeFin);
        metadatasMap.put(DatesFormParamVO.class.getSimpleName(), datesYearsContinuousFormParamVO);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);
        final SoilParameterVO parameters = new SoilParameterVO(metadatasMap);
        try {
            ACBBTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException | BusinessException e) {
            return "false : " + e.getMessage();
        }
        return "true";
    }
}
