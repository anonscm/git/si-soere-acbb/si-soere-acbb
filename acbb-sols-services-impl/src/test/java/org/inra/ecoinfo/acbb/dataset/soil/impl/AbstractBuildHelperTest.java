/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author ptcherniati
 */
public class AbstractBuildHelperTest {

    MockUtils m = MockUtils.getInstance();
    AbstractBuildHelperImpl instance;
    @Mock
    RequestPropertiesSoil requestPropertiesSoil;
    @Mock
    private List<AbstractSoilLineRecord> lines;
    /**
     *
     */
    public AbstractBuildHelperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractBuildHelperImpl();
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.instance.setDatatypeName(MockUtils.DATATYPE);
        this.instance.requestPropertiesSoil = this.requestPropertiesSoil;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getRequestPropertiesSoil method, of class AbstractBuildHelper.
     */
    @Test
    public void testGetRequestPropertiesSoil() {
        IRequestPropertiesSoil expResult = this.requestPropertiesSoil;
        IRequestPropertiesSoil result = this.instance.getRequestPropertiesSoil();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setDatatypeName method, of class AbstractBuildHelper.
     */
    @Test
    public void testSetDatatypeName() {
        Assert.assertEquals(MockUtils.DATATYPE, this.instance.datatypeName);
    }

    /**
     * Test of setDatatypeVariableUniteACBBDAO method, of class
     * AbstractBuildHelper.
     */
    @Test
    public void testSetDatatypeVariableUniteACBBDAO() {
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        Assert.assertEquals(this.m.datatypeVariableUniteDAO,
                this.instance.datatypeVariableUniteACBBDAO);
    }

    /**
     * Test of update method, of class AbstractBuildHelper.
     */
    @Test
    public void testUpdate() {
        ErrorsReport errorsReport = new ErrorsReport();
        this.instance.update(this.m.versionFile, this.lines, errorsReport,
                this.requestPropertiesSoil);
        Mockito.verify(this.m.datatypeVariableUniteDAO)
                .getAllVariableTypeDonneesByDataTypeMapByVariableCode(MockUtils.DATATYPE);
        Assert.assertEquals(this.m.versionFile, this.instance.version);
        Assert.assertEquals(this.lines, this.instance.lines);
        Assert.assertEquals(errorsReport, this.instance.errorsReport);
        Assert.assertEquals(this.requestPropertiesSoil, this.instance.requestPropertiesSoil);
    }


    /**
     * Test of build method, of class AbstractBuildHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild() throws Exception {
        AbstractBuildHelper instance = new AbstractBuildHelperImpl();
        instance.build();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRealNodeForSequenceAndVariable method, of class
     * AbstractBuildHelper.
     */
    @Test
    public void testGetRealNodeForSequenceAndVariable() {
        DatatypeVariableUniteACBB dvu = null;
        AbstractBuildHelper instance = new AbstractBuildHelperImpl();
        RealNode expResult = null;
        RealNode result = instance.getRealNodeForSequenceAndVariable(dvu);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRealNodeForSequenceAndVariableCode method, of class
     * AbstractBuildHelper.
     */
    @Test
    public void testGetRealNodeForSequenceAndVariableCode() {
        String variableCode = "";
        AbstractBuildHelper instance = new AbstractBuildHelperImpl();
        RealNode expResult = null;
        RealNode result = instance.getRealNodeForSequenceAndVariableCode(variableCode);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     *
     */
    public class AbstractBuildHelperImpl extends AbstractBuildHelper {

        /**
         * @throws PersistenceException
         */
        @Override
        public void build() throws PersistenceException {
        }
    }
}
