/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.IContentChemistryDAO;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ContentChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.MesureChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ValeurChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author ptcherniati
 */
public class BuildContentChemistryHelperTest {

    private final String campaignName = "campaignName";
    private final String layerName = "layerName";
    private final Float layerSup = 1.2F;
    private final Float layerInf = 2.1F;
    MockUtils m = MockUtils.getInstance();
    TestInstance instance;
    @Mock
    IContentChemistryDAO contentChemistryDAO;
    @Mock
    ContentChemistry contentChemistry;
    @Mock
    ContentChemistryLineRecord line;
    @Mock
    List<ContentChemistryLineRecord> lines;
    @Mock
    VariableValueContentChemistry valueContentChemistry1;
    @Mock
    VariableValueContentChemistry valueContentChemistry2;
    @Mock
    VariableACBB variable1;
    @Mock
    VariableACBB variable2;
    @Mock
    RequestPropertiesContentChemistry requestProperties;
    @Mock(name = "dbDatatypeVariableUnites")
    Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites;
    @Mock
    DatatypeVariableUniteACBB dvu1;
    @Mock
    DatatypeVariableUniteACBB dvu2;
    @Mock
    DatatypeVariableUniteACBB dvuHUT;
    @Mock
    RealNode realNode;
    @Mock
    RealNode realNodeForHumidity;
    @Mock
    AbstractMethode method1;
    @Mock
    AbstractMethode method2;
    List<VariableValueContentChemistry> variablesValues = new LinkedList();
    Map<Parcelle, Map<LocalDate, Map<Long, Map<Float, Map<Float, ContentChemistry>>>>> contentChemistries = new HashMap();
    Map<LocalDate, Map<Long, Map<Float, Map<Float, ContentChemistry>>>> contentChemistrysByDate = new HashMap();
    Map<Long, Map<Float, Map<Float, ContentChemistry>>> contentChemistrysByMethod = new HashMap();
    Map<Float, Map<Float, ContentChemistry>> contentChemistrysByLayerInf = new HashMap();
    Map<Float, ContentChemistry> contentChemistrysByLayerSup = new HashMap();
    @Mock
    MasseVegClass masseVegClass;
    ErrorsReport errorsReport;
    /**
     *
     */
    public BuildContentChemistryHelperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    protected void initVariablesValues() {
        Mockito.when(this.line.getVariablesValues()).thenReturn(this.variablesValues);
        this.variablesValues.add(this.valueContentChemistry1);
        this.variablesValues.add(this.valueContentChemistry2);
        Mockito.when(this.valueContentChemistry1.getDatatypeVariableUnite()).thenReturn(this.dvu1);
        Mockito.when(this.valueContentChemistry1.getValue()).thenReturn("1.1");
        Mockito.when(this.valueContentChemistry1.getMeasureNumber()).thenReturn(11);
        Mockito.when(this.valueContentChemistry1.getMethode()).thenReturn(this.method1);
        Mockito.when(this.valueContentChemistry1.getQualityClass()).thenReturn(12);
        Mockito.when(this.valueContentChemistry1.getStandardDeviation()).thenReturn(13F);
        Mockito.when(this.valueContentChemistry2.getDatatypeVariableUnite()).thenReturn(this.dvu2);
        Mockito.when(this.valueContentChemistry2.getValue()).thenReturn("2.1");
        Mockito.when(this.valueContentChemistry2.getMeasureNumber()).thenReturn(21);
        Mockito.when(this.valueContentChemistry2.getMethode()).thenReturn(this.method2);
        Mockito.when(this.valueContentChemistry2.getQualityClass()).thenReturn(22);
        Mockito.when(this.valueContentChemistry2.getStandardDeviation()).thenReturn(23F);
        Mockito.when(this.variable1.getCode()).thenReturn("variable1");
        Mockito.when(this.variable2.getCode()).thenReturn("variable2");
    }

    /**
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        this.instance = new TestInstance();
        this.instance.setContentChemistryDAO(this.contentChemistryDAO);
        this.instance.setDatatypeName(MockUtils.DATATYPE);
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.updateInstance();
        this.initVariablesValues();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addValeursContentChemistry method, of class
     * BuildContentChemistryHelper.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testAddValeursContentChemistry() throws PersistenceException {
        List<MesureChemistry> mesureChemistrys = new LinkedList();
        Mockito.when(this.contentChemistry.getMesures()).thenReturn(mesureChemistrys);
        when(valueContentChemistry2.getResidualHumidityValue()).thenReturn((float) ACBBUtils.CST_INVALID_BAD_MEASURE);
        when(valueContentChemistry1.getResidualHumidityValue()).thenReturn(2.2F);
        when(valueContentChemistry1.getResidualHumidityStandardDeviation()).thenReturn(5.6F);
        when(valueContentChemistry1.getResidualHumidityNumber()).thenReturn(8);
        this.instance.addValeursContentChemistry(this.line, this.contentChemistry);
        this.verifieValeur(mesureChemistrys.get(0).getValeurs().get(0), this.method1, 1.1F, this.dvu1, 11, 13F, 12, realNode);
        this.verifieValeur(mesureChemistrys.get(0).getValeurs().get(1), null, 2.2F, this.dvuHUT, 8, 5.6F, ACBBUtils.CST_INVALID_BAD_MEASURE, realNodeForHumidity);
        this.verifieValeur(mesureChemistrys.get(1).getValeurs().get(0), this.method2, 2.1F, this.dvu2, 21, 23F, 22, realNode);
    }

    /**
     * Test of build method, of class BuildContentChemistryHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild() throws Exception {
        // nominal
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(this.contentChemistry).when(this.instance)
                .getOrCreateContentChemistry(this.line);
        this.lines = new LinkedList();
        this.lines.add(this.line);
        this.updateInstance();
        Mockito.doNothing().when(this.instance)
                .addValeursContentChemistry(this.line, this.contentChemistry);
        this.instance.build();
        Mockito.verify(this.contentChemistryDAO, Mockito.times(1)).saveOrUpdate(this.contentChemistry);
        // with errormessage
        this.errorsReport.addErrorMessage("erreur");
        try {
            this.instance.build();
            Assert.fail("no exception");
        } catch (PersistenceException e) {
            Assert.assertEquals("-erreur\n", e.getMessage());
        }
    }

    /**
     * Test of build method, of class BuildContentChemistryHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild_4args() throws Exception {
        this.instance = Mockito.spy(this.instance);
        Mockito.doNothing().when(this.instance)
                .update(this.m.versionFile, this.lines, this.errorsReport, this.requestProperties);
        Mockito.doNothing().when(this.instance).build();
        this.instance.build(this.m.versionFile, this.lines, this.errorsReport,
                this.requestProperties);
    }

    /**
     * Test of createNewContentChemistry method, of class
     * BuildContentChemistryHelper.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testCreateNewContentChemistry() throws PersistenceException {
        Mockito.when(this.line.getObservation()).thenReturn("observation");
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateFin);
        Mockito.when(this.line.getRotationNumber()).thenReturn(1);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(2);
        Mockito.when(this.line.getSuiviParcelle()).thenReturn(this.m.suiviParcelle);
        Mockito.when(this.line.getCampaignName()).thenReturn(this.campaignName);
        Mockito.when(this.line.getLayerName()).thenReturn(this.layerName);
        Mockito.when(this.line.getLayerInf()).thenReturn(this.layerInf);
        Mockito.when(this.line.getLayerSup()).thenReturn(this.layerSup);
        // nominal
        ContentChemistry result = this.instance.createNewContentChemistry(this.line);
        ArgumentCaptor<ContentChemistry> contentChemistry = ArgumentCaptor
                .forClass(ContentChemistry.class);
        Mockito.verify(this.contentChemistryDAO).saveOrUpdate(contentChemistry.capture());
        Assert.assertEquals(result, contentChemistry.getValue());
        Assert.assertEquals("observation", contentChemistry.getValue().getObservation());
        Assert.assertEquals(this.m.dateFin, contentChemistry.getValue().getDate());
        Assert.assertEquals((Integer) 1, contentChemistry.getValue()
                .getVersionTraitementRealiseeNumber());
        Assert.assertEquals((Integer) 2, contentChemistry.getValue().getAnneeRealiseeNumber());
        Assert.assertEquals(this.m.suiviParcelle, contentChemistry.getValue().getSuiviParcelle());
        // with exception
        Mockito.doThrow(new PersistenceException("Erreur")).when(this.contentChemistryDAO)
                .saveOrUpdate(any(ContentChemistry.class));
        result = this.instance.createNewContentChemistry(this.line);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Une erreur inconnue est survenue lors de l'enregistrement des données. La publication a été annulée.\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getOrCreateContentChemistry method, of class
     * BuildContentChemistryHelper.
     */
    @Test
    public void testGetOrCreateContentChemistry() {
        Long method = 210L;
        Float layerInf = 133.2F;
        Float layerSup = 33.2F;
        instance.contentChemistries = this.contentChemistries;
        Mockito.when(this.line.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateDebut);
        when(method1.getId()).thenReturn(method);
        Mockito.when(this.line.getMethodeSample()).thenReturn(method1);
        Mockito.when(this.line.getLayerInf()).thenReturn(layerInf);
        Mockito.when(this.line.getLayerSup()).thenReturn(layerSup);
        this.contentChemistries.put(this.m.parcelle, this.contentChemistrysByDate);
        this.contentChemistrysByDate.put(this.m.dateDebut, this.contentChemistrysByMethod);
        this.contentChemistrysByMethod.put(method, this.contentChemistrysByLayerInf);
        this.contentChemistrysByLayerInf.put(layerInf, this.contentChemistrysByLayerSup);
        this.contentChemistrysByLayerSup.put(layerSup, this.contentChemistry);
        Mockito.when(this.masseVegClass.getId()).thenReturn(2L);
        // existing contentChemistry
        this.instance.contentChemistries = this.contentChemistries;
        ContentChemistry result = this.instance.getOrCreateContentChemistry(this.line);
        Assert.assertEquals(this.contentChemistry, result);
        // new contentChemistry
        this.contentChemistries.get(this.m.parcelle).remove(this.m.dateDebut);
        this.instance = Mockito.spy(this.instance);
        ContentChemistry bm = Mockito.mock(ContentChemistry.class);
        Mockito.doReturn(bm).when(this.instance).createNewContentChemistry(this.line);
        result = this.instance.getOrCreateContentChemistry(this.line);
        Assert.assertEquals(bm, this.contentChemistries.get(this.m.parcelle).get(this.m.dateDebut));
        Assert.assertEquals(bm, result);
        // new contentChemistrysByDate and new contentChemistry
        this.contentChemistries.get(this.m.parcelle).remove(this.m.dateDebut);
        result = this.instance.getOrCreateContentChemistry(this.line);
        Assert.assertEquals(bm, this.contentChemistries.get(this.m.parcelle).get(this.m.dateDebut));
        Assert.assertEquals(bm, result);
        // new contentChemistrysByDate and new contentChemistry
        this.contentChemistries.remove(this.m.parcelle);
        result = this.instance.getOrCreateContentChemistry(this.line);
        Assert.assertEquals(bm, this.contentChemistries.get(this.m.parcelle).get(this.m.dateDebut));
        Assert.assertEquals(bm, result);
    }

    /**
     * @throws PersistenceException
     */
    protected void updateInstance() throws PersistenceException {
        this.errorsReport = new ErrorsReport();
        Mockito.when(
                this.m.datatypeVariableUniteDAO
                        .getAllVariableTypeDonneesByDataTypeMapByVariableCode(MockUtils.DATATYPE))
                .thenReturn(this.dbDatatypeVariableUnites);
        Mockito.when(this.dbDatatypeVariableUnites.get("variable1")).thenReturn(this.dvu1);
        Mockito.when(this.dbDatatypeVariableUnites.get("variable2")).thenReturn(this.dvu2);
        Mockito.when(this.m.datatypeVariableUniteDAO.merge(this.dvu1)).thenReturn(this.dvu1);
        Mockito.when(this.m.datatypeVariableUniteDAO.merge(this.dvu2)).thenReturn(this.dvu2);
        this.instance.update(this.m.versionFile, this.lines, this.errorsReport,
                this.requestProperties);
    }

    private void verifieValeur(ValeurChemistry valeur, AbstractMethode methode,
                               Float valeurFloat, DatatypeVariableUniteACBB dvu, int measureNumber,
                               Float standardDeviation, int qualityIndex, RealNode realNode) {
        Assert.assertEquals(this.contentChemistry, valeur.getMesureChemistry().getContentChemistry());
        Assert.assertEquals(valeurFloat, valeur.getValeur());
        Assert.assertEquals(methode, valeur.getMethode());
        Assert.assertEquals(dvu, instance.variables.pop());
        Assert.assertEquals(measureNumber, valeur.getMeasureNumber());
        Assert.assertEquals(standardDeviation, valeur.getStandardDeviation());
        Assert.assertEquals(qualityIndex, valeur.getQualityIndex());
        Assert.assertEquals(realNode, valeur.getRealNode());
    }


    /**
     * Test of setContentChemistryDAO method, of class
     * BuildContentChemistryHelper.
     */
    @Test
    public void testSetContentChemistryDAO() {
        final IContentChemistryDAO dao = mock(IContentChemistryDAO.class);
        instance.setContentChemistryDAO(dao);
        Assert.assertEquals(dao, instance.contentChemistryDAO);
    }

    class TestInstance extends BuildContentChemistryHelper {

        LinkedList<DatatypeVariableUniteACBB> variables = new LinkedList();

        @Override
        protected RealNode getRealNodeForSequenceAndVariable(DatatypeVariableUniteACBB variable) {
            variables.add(variable);
            return realNode;
        }

        @Override
        protected RealNode getRealNodeForSequenceAndVariableCode(String variableCode) {
            if (AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE.equals(variableCode)) {
                variables.add(dvuHUT);
                return realNodeForHumidity;
            }
            return super.getRealNodeForSequenceAndVariableCode(variableCode);
        }

    }

}
