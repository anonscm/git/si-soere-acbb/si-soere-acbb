/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.junit.*;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
public class AbstractSoilOutputBuilderTest {

    /**
     *
     */
    public AbstractSoilOutputBuilderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getNAIfBadValueOrNVIfEmptyValue method, of class
     * AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetNAIfBadValueOrNVIfEmptyValue_Float() {
        Integer number = null;
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getNAIfBadValueOrNVIfEmptyValue(number);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNAIfBadValueOrNVIfEmptyValue method, of class
     * AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetNAIfBadValueOrNVIfEmptyValue_Integer() {
        Integer number = null;
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getNAIfBadValueOrNVIfEmptyValue(number);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFirstVariable method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetFirstVariable() {
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getFirstVariable();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addGenericColumns method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testAddGenericColumns() {
        StringBuilder stringBuilder1 = null;
        StringBuilder stringBuilder2 = null;
        StringBuilder stringBuilder3 = null;
        AbstractSoilOutputBuilder instance = null;
        instance.addGenericColumns(stringBuilder1, stringBuilder2, stringBuilder3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addObservation method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testAddObservation() {
        StringBuilder stringBuilder1 = null;
        StringBuilder stringBuilder2 = null;
        StringBuilder stringBuilder3 = null;
        AbstractSoilOutputBuilder instance = null;
        instance.addObservation(stringBuilder1, stringBuilder2, stringBuilder3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addSpecificColumns method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testAddSpecificColumns() {
        StringBuilder stringBuilder1 = null;
        StringBuilder stringBuilder2 = null;
        StringBuilder stringBuilder3 = null;
        AbstractSoilOutputBuilder instance = null;
        instance.addSpecificColumns(stringBuilder1, stringBuilder2, stringBuilder3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildBody method, of class AbstractSoilOutputBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildBody() throws Exception {
        String headers = "";
        Map<String, List> resultsDatasMap = null;
        Map<String, Object> requestMetadatasMap = null;
        AbstractSoilOutputBuilder instance = null;
        Map<String, File> expResult = null;
        Map<String, File> result = instance.buildBody(headers, resultsDatasMap, requestMetadatasMap);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildHeader method, of class AbstractSoilOutputBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildHeader() throws Exception {
        Map<String, Object> requestMetadatasMap = null;
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.buildHeader(requestMetadatasMap);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildOutput method, of class AbstractSoilOutputBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildOutput() throws Exception {
        IParameter parameters = null;
        AbstractSoilOutputBuilder instance = null;
        RObuildZipOutputStream expResult = null;
        RObuildZipOutputStream result = instance.buildOutput(parameters);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBundleSourcePath method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetBundleSourcePath() {
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getBundleSourcePath();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getColumnPatterns method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetColumnPatterns() {
        AbstractSoilOutputBuilder instance = null;
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getColumnPatterns();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtractCode method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetExtractCode() {
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getExtractCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOutPutHelper method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetOutPutHelper() {
        DatesFormParamVO datesYearsContinuousFormParamVO = null;
        List<Variable> selectedWsolVariables = null;
        PrintStream rawDataPrintStream = null;
        AbstractSoilOutputBuilder instance = null;
        AbstractSoilOutputBuilder.OutputHelper expResult = null;
        AbstractSoilOutputBuilder.OutputHelper result = instance.getOutPutHelper(datesYearsContinuousFormParamVO, selectedWsolVariables, rawDataPrintStream);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getResultCode method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetResultCode() {
        AbstractSoilOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getResultCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetSuiviParcelle() {
        Parcelle parcelle = null;
        LocalDate date = null;
        AbstractSoilOutputBuilder instance = null;
        Optional<SuiviParcelle> expResult = null;
        Optional<SuiviParcelle> result = instance.getSuiviParcelle(parcelle, date);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getValueColumns method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testGetValueColumns() {
        AbstractSoilOutputBuilder instance = null;
        List<ListeSoil> expResult = null;
        List<ListeSoil> result = instance.getValueColumns();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setColumnPatterns method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetColumnPatterns() {
        Map<String, String> columnPatterns = null;
        AbstractSoilOutputBuilder instance = null;
        instance.setColumnPatterns(columnPatterns);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatasetDescriptor method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetDatasetDescriptor() {
        DatasetDescriptor datasetDescriptor = null;
        AbstractSoilOutputBuilder instance = null;
        instance.setDatasetDescriptor(datasetDescriptor);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatatypeVariableUniteACBBDAO method, of class
     * AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetDatatypeVariableUniteACBBDAO() {
        IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO = null;
        AbstractSoilOutputBuilder instance = null;
        instance.setDatatypeVariableUniteACBBDAO(datatypeVariableUniteDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExtractCode method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetExtractCode() {
        String extractCode = "";
        AbstractSoilOutputBuilder instance = null;
        instance.setExtractCode(extractCode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListeSoilDAO method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetListeSoilDAO() {
        IListeSoilDAO listeSoilDAO = null;
        AbstractSoilOutputBuilder instance = null;
        instance.setListeSoilDAO(listeSoilDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSuiviParcelleDAO method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetSuiviParcelleDAO() {
        ISuiviParcelleDAO suiviParcelleDAO = null;
        AbstractSoilOutputBuilder instance = null;
        instance.setSuiviParcelleDAO(suiviParcelleDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVariableDAO method, of class AbstractSoilOutputBuilder.
     */
    @Test
    public void testSetVariableDAO() {
        IVariableACBBDAO variableDAO = null;
        AbstractSoilOutputBuilder instance = null;
        instance.setVariableDAO(variableDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     *
     */
    public class AbstractSoilOutputBuilderImpl extends AbstractSoilOutputBuilder {

        /**
         *
         */
        public AbstractSoilOutputBuilderImpl() {
            super("");
        }

        @Override
        public String getFirstVariable() {
            return "";
        }

        @Override
        public String getBundleSourcePath() {
            return "";
        }

        @Override
        protected OutputHelper getOutPutHelper(DatesFormParamVO datesYearsContinuousFormParamVO, List selectedWsolVariables, PrintStream rawDataPrintStream) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
