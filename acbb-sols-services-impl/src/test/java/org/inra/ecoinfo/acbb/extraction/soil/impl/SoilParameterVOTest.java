/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.utils.IntervalDate;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
public class SoilParameterVOTest {

    /**
     *
     */
    public SoilParameterVOTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAffichage method, of class SoilParameterVO.
     */
    @Test
    public void testGetAffichage() {
        SoilParameterVO instance = new SoilParameterVO();
        int expResult = 0;
        int result = instance.getAffichage();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCommentaire method, of class SoilParameterVO.
     */
    @Test
    public void testGetCommentaire() {
        SoilParameterVO instance = new SoilParameterVO();
        String expResult = "";
        String result = instance.getCommentaire();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDatesYearsContinuousFormParamVO method, of class
     * SoilParameterVO.
     */
    @Test
    public void testGetDatesYearsContinuousFormParamVO() {
        SoilParameterVO instance = new SoilParameterVO();
        DatesFormParamVO expResult = null;
        DatesFormParamVO result = instance.getDatesYearsContinuousFormParamVO();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtractionTypeCode method, of class SoilParameterVO.
     */
    @Test
    public void testGetExtractionTypeCode() {
        SoilParameterVO instance = new SoilParameterVO();
        String expResult = "";
        String result = instance.getExtractionTypeCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIntervalsDates method, of class SoilParameterVO.
     */
    @Test
    public void testGetIntervalsDates() {
        SoilParameterVO instance = new SoilParameterVO();
        List<IntervalDate> expResult = null;
        List<IntervalDate> result = instance.getIntervalsDates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedsTraitements method, of class SoilParameterVO.
     */
    @Test
    public void testGetSelectedsTraitements() {
        SoilParameterVO instance = new SoilParameterVO();
        List<TraitementProgramme> expResult = null;
        List<TraitementProgramme> result = instance.getSelectedsTraitements();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedTraitementProgrammes method, of class SoilParameterVO.
     */
    @Test
    public void testGetSelectedTraitementProgrammes() {
        SoilParameterVO instance = new SoilParameterVO();
        List<TraitementProgramme> expResult = null;
        List<TraitementProgramme> result = instance.getSelectedTraitementProgrammes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAffichage method, of class SoilParameterVO.
     */
    @Test
    public void testSetAffichage() {
        int affichage = 0;
        SoilParameterVO instance = new SoilParameterVO();
        instance.setAffichage(affichage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCommentaire method, of class SoilParameterVO.
     */
    @Test
    public void testSetCommentaire() {
        String commentaires = "";
        SoilParameterVO instance = new SoilParameterVO();
        instance.setCommentaire(commentaires);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIntervalsDate method, of class SoilParameterVO.
     */
    @Test
    public void testSetIntervalsDate() {
        List<IntervalDate> intervalDates = null;
        SoilParameterVO instance = new SoilParameterVO();
        instance.setIntervalsDate(intervalDates);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedsTraitements method, of class SoilParameterVO.
     */
    @Test
    public void testSetSelectedsTraitements() {
        List<TraitementProgramme> listTraitement = null;
        SoilParameterVO instance = new SoilParameterVO();
        instance.setSelectedsTraitements(listTraitement);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setShowSamples method, of class SoilParameterVO.
     */
    @Test
    public void testSetShowSamples() {
        SoilParameterVO instance = new SoilParameterVO();
        instance.setShowSamples();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setShowHUT method, of class SoilParameterVO.
     */
    @Test
    public void testSetShowHUT() {
        SoilParameterVO instance = new SoilParameterVO();
        instance.setShowHUT();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
