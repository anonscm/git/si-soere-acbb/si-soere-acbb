/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import com.Ostermiller.util.CSVParser;
import mockit.Mocked;
import mockit.Verifications;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesample.IMethodeSampleDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur.IMethodeAnalyseDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.BiomassMockUtils;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author ptcherniati
 */
public class ProcessRecordContentChemistryTest {

    BiomassMockUtils m = BiomassMockUtils.getInstance();
    ProcessRecordContentChemistry instance;
    @Mock
    IInterventionDAO interventionDAO;
    @Mock
    IRequestPropertiesSoil requestPropertiesSoil;
    @Mock
    IListeSoilDAO listeSoilDAO;
    @Mock
    IMethodeAnalyseDAO methodeAnalyseDAO;
    @Mock
    IMethodeSampleDAO methodeSampleDAO;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    List<DatatypeVariableUniteACBB> variables;
    @Mock
    Paturage paturage;
    @Mock
    IntervalDate intervalDate;
    @Mock
    ContentChemistryLineRecord line;
    String text = "1;ligne complète;;6;Campagne_sol_2010;0;10;horizon a;02/03/2010;20;5;701;20;4;6;602;1;3,5;4;3,2;4,3;4;3,7;603;;7,2;3;1,2";
    @Mock
    CSVParser parser;
    List<ContentChemistryLineRecord> lines = new LinkedList();
    /**
     *
     */
    public ProcessRecordContentChemistryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    protected void initInstance(ProcessRecordContentChemistry instance) {
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setMethodeAnalyseDAO(this.methodeAnalyseDAO);
        instance.setMethodeSampleDAO(this.methodeSampleDAO);
        instance.setListeSoilDAO(this.listeSoilDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setMethodeAnalyseDAO(this.methodeAnalyseDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setVersionFileDAO(this.m.versionFileDAO);
        this.instance = instance;
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.initInstance(new ProcessRecordContentChemistry());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * @param dbVariables
     * @param abstractProcessRecord
     * @throws Exception
     */
    @Test
    public void testProcessRecord(@Mocked final List<VariableACBB> dbVariables, @Mocked final AbstractProcessRecord abstractProcessRecord) throws Exception {
        Mockito.when(this.datasetDescriptor.getEnTete()).thenReturn(4);
        Mockito.when(this.m.versionFileDAO.merge(this.m.versionFile))
                .thenReturn(this.m.versionFile);
        this.initInstance(new InstanceImpl());
        // nominal
        this.instance.processRecord(((InstanceImpl) this.instance).parser, this.m.versionFile,
                BiomassMockUtils.requestPropertiesBiomasse, "UTF-8", this.datasetDescriptor);
        Assert.assertTrue(((InstanceImpl) this.instance).errorsReport.hasInfos());
        Assert.assertEquals("-true\n-true\n",
                ((InstanceImpl) this.instance).errorsReport.getInfosMessages());
        new Verifications() {
            {
                abstractProcessRecord.processRecord(((InstanceImpl) instance).parser,
                        m.versionFile, BiomassMockUtils.requestPropertiesBiomasse, "UTF-8",
                        datasetDescriptor);
            }
        };
        Mockito.verify(this.m.versionFileDAO).merge(this.m.versionFile);
        // if errorreport
        try {
            this.instance.processRecord(((InstanceImpl) this.instance).parser, this.m.versionFile,
                    BiomassMockUtils.requestPropertiesBiomasse, "UTF-8", this.datasetDescriptor);
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals("org.inra.ecoinfo.utils.exceptions.PersistenceException: -error\n",
                    e.getMessage());
        }
    }

    /**
     * Test of readLines method, of class ProcessRecordContentChemistry.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLines() throws Exception {
//        ErrorsReport errorsReport = new ErrorsReport();
//        instance = spy(new InstanceImplForReadLines());
//        long result = this.instance.readLines(parser, intervalDate, requestPropertiesSoil, datasetDescriptor, errorsReport, 0, variables, lines);
//        Assert.assertFalse(errorsReport.hasErrors());
//        Assert.assertNotNull(this.lines.get(0));
//        // with errorreport
//        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
//        this.lines.clear();
//        errorsReport.addErrorMessage("message");
//        result = this.instance.readLines(parser, intervalDate, BiomassMockUtils.requestPropertiesBiomasse,
//                this.datasetDescriptor, errorsReport, 1L, dbVariables, this.lines);
//        Assert.assertTrue(errorsReport.hasErrors());
//        Assert.assertTrue(2 == result);
    }

    /**
     * Test of readMethod method, of class ProcessRecordContentChemistry.
     */
    @Test
    public void testReadMethod() {
//        ErrorsReport errorsReport = new ErrorsReport();
//        String[] values = new String[]{"102"};
//        CleanerValues cleanerValues = new CleanerValues(values);
//        VariableValueContentChemistry variableValue = Mockito
//                .mock(VariableValueContentChemistry.class);
//        MethodeProduction methodeProduction = Mockito.mock(MethodeProduction.class);
//        MethodeAnalyse methodeAnalyse = Mockito.mock(MethodeAnalyse.class);
//        Mockito.when(this.methodeProductionDAO.getByNumberId(102L)).thenReturn(methodeProduction);
//        // nominal merthode production
//        int result = this.instance.readMethod(errorsReport, 1L, 0, cleanerValues, variableValue);
//        Assert.assertEquals(1, result);
//        Mockito.verify(variableValue, Mockito.times(1)).setMethode(methodeProduction);
//        Assert.assertFalse(errorsReport.hasErrors());
//        // nominal merthode analyse
//        values = new String[]{"103"};
//        cleanerValues = new CleanerValues(values);
//        Mockito.when(this.methodeAnalyseDAO.getByNumberId(103L)).thenReturn(methodeAnalyse);
//        result = this.instance.readMethod(errorsReport, 1L, 0, cleanerValues, variableValue);
//        Assert.assertEquals(1, result);
//        Mockito.verify(variableValue, Mockito.times(1)).setMethode(methodeAnalyse);
//        Assert.assertFalse(errorsReport.hasErrors());
//        // empty method
//        values = new String[]{"104"};
//        cleanerValues = new CleanerValues(values);
//        result = this.instance.readMethod(errorsReport, 1L, 0, cleanerValues, variableValue);
//        Assert.assertEquals(1, result);
//        Mockito.verify(variableValue, Mockito.times(2)).setMethode(
//                Matchers.any(AbstractMethode.class));
//        Assert.assertTrue(errorsReport.hasErrors());
//        Assert.assertEquals(
//                "-Ligne 1, colonne 1, la méthode \"104\" n'est pas renseignée en base de données .\n",
//                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readVariableValueContentChemistry method, of class
     * ProcessRecordContentChemistry.
     */
    @Test
    public void testReadVariableValueContentChemistry() {
//        String[] values = new String[]{"12.0", "2", "11.4", "102", "2"};
//        CleanerValues cleanerValues = new CleanerValues(values);
//        ContentChemistryLineRecord line = Mockito.mock(ContentChemistryLineRecord.class);
//        LinkedList variableValues = Mockito.mock(LinkedList.class);
//        Mockito.when(line.getVariablesValues()).thenReturn(variableValues);
//        List<DatatypeVariableUniteACBB> dbVariables = Mockito.mock(List.class);
//        DatatypeVariableUniteACBB dvu = Mockito.mock(DatatypeVariableUniteACBB.class);
//        when(dvu.getVariable()).thenReturn(m.variable);
//        ErrorsReport errorsReport = new ErrorsReport();
//        Mockito.when(dbVariables.get(1)).thenReturn(dvu);
//        Mockito.when(this.methodeAnalyseDAO.getByNumberId(102L)).thenReturn(this.methode);
//        // nominal
//        int result = this.instance.readVariableValueContentChemistry(line, errorsReport, 1L, 0,
//                cleanerValues, this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse,
//                dbVariables);
//        Assert.assertEquals(5, result);
//        ArgumentCaptor<VariableValueContentChemistry> vv = ArgumentCaptor
//                .forClass(VariableValueContentChemistry.class);
//        Mockito.verify(variableValues).add(vv.capture());
//        VariableValueContentChemistry valeur = vv.getValue();
//        Assert.assertEquals("12.0", valeur.getValue());
//        Assert.assertEquals(2, valeur.getMeasureNumber());
//        Assert.assertTrue(11.4F == valeur.getStandardDeviation());
//        Assert.assertEquals(this.methode, valeur.getMethode());
//        Assert.assertTrue(2 == valeur.getQualityClass());
//        // nominal
//        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY, "2", "11.4", "102", "2"};
//        cleanerValues = new CleanerValues(values);
//        result = this.instance.readVariableValueContentChemistry(line, errorsReport, 1L, 0,
//                cleanerValues, this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse,
//                dbVariables);
//        Assert.assertEquals(5, result);
//        Mockito.verify(variableValues, Mockito.times(1)).add(vv.capture());

    }

    /**
     * Test of buildNewLines method, of class ProcessRecordContentChemistry.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildNewLines() throws Exception {
        VersionFile version = null;
        List<? extends AbstractSoilLineRecord> lines = null;
        ErrorsReport errorsReport = null;
        IRequestPropertiesSoil requestPropertiesSoil = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        instance.buildNewLines(version, lines, errorsReport, requestPropertiesSoil);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of processRecord method, of class ProcessRecordContentChemistry.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        CSVParser parser = null;
        VersionFile versionFile = null;
        IRequestPropertiesACBB requestProperties = null;
        String fileEncoding = "";
        DatasetDescriptorACBB datasetDescriptor = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        instance.processRecord(parser, versionFile, requestProperties, fileEncoding, datasetDescriptor);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readResidualHumidityValue method, of class
     * ProcessRecordContentChemistry.
     */
    @Test
    public void testReadResidualHumidityValue() {
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        CleanerValues cleanerValues = null;
        VariableValueContentChemistry variableValue = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        int expResult = 0;
        int result = instance.readResidualHumidityValue(errorsReport, lineCount, index, cleanerValues, variableValue, datasetDescriptorACBB);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readResidualHumidityNumber method, of class
     * ProcessRecordContentChemistry.
     */
    @Test
    public void testReadResidualHumidityNumber() {
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        CleanerValues cleanerValues = null;
        VariableValueContentChemistry variableValue = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        int expResult = 0;
        int result = instance.readResidualHumidityNumber(errorsReport, lineCount, index, cleanerValues, variableValue, datasetDescriptorACBB);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readResidualHumidityStandardDeviation method, of class
     * ProcessRecordContentChemistry.
     */
    @Test
    public void testReadResidualHumidityStandardDeviation() {
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        CleanerValues cleanerValues = null;
        VariableValueContentChemistry variableValue = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        int expResult = 0;
        int result = instance.readResidualHumidityStandardDeviation(errorsReport, lineCount, index, cleanerValues, variableValue, datasetDescriptorACBB);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readMethodAnalyse method, of class ProcessRecordContentChemistry.
     */
    @Test
    public void testReadMethodAnalyse() {
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        CleanerValues cleanerValues = null;
        VariableValueContentChemistry variableValue = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        int expResult = 0;
        int result = instance.readMethodAnalyse(errorsReport, lineCount, index, cleanerValues, variableValue, false);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readMethodSample method, of class ProcessRecordContentChemistry.
     */
    @Test
    public void testReadMethodSample() {
        ErrorsReport errorsReport = null;
        AbstractSoilLineRecord line = null;
        long lineCount = 0L;
        int index = 0;
        CleanerValues cleanerValues = null;
        VariableValueContentChemistry variableValue = null;
        ProcessRecordContentChemistry instance = new ProcessRecordContentChemistry();
        int expResult = 0;
        int result = instance.readMethodSample(errorsReport, lineCount, index, cleanerValues, variableValue, line);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    class InstanceImplForReadLines extends ProcessRecordContentChemistry {

        @Override
        protected int readSample(IntervalDate intervalDate, ContentChemistryLineRecord lineRecord, ErrorsReport errorsReport, long lineCount, int index, CleanerValues cleanerValues, DatasetDescriptorACBB datasetDescriptor, IRequestPropertiesSoil requestPropertiesSoil) {
            return super.readSample(intervalDate, lineRecord, errorsReport, lineCount, index, cleanerValues, datasetDescriptor, requestPropertiesSoil); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected int readVariableValueContentChemistry(ContentChemistryLineRecord lineRecord, ErrorsReport errorsReport, long lineCount, int index, CleanerValues cleanerValues, DatasetDescriptorACBB datasetDescriptorACBB, IRequestPropertiesSoil requestPropertiesSoil, List<DatatypeVariableUniteACBB> dbVariables) {
            return super.readVariableValueContentChemistry(lineRecord, errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB, requestPropertiesSoil, dbVariables); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected int readGenericColumns(ErrorsReport errorsReport, long lineCount, CleanerValues cleanerValues, AbstractSoilLineRecord lineRecord, DatasetDescriptorACBB datasetDescriptorACBB, IRequestPropertiesSoil requestPropertiesSoil) {
            return super.readGenericColumns(errorsReport, lineCount, cleanerValues, lineRecord, datasetDescriptorACBB, requestPropertiesSoil); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected int readDateAndSetVersionTraitement(IntervalDate intervalDate, AbstractSoilLineRecord lineRecord, ErrorsReport errorsReport, long lineCount, int index, CleanerValues cleanerValues, DatasetDescriptorACBB datasetDescriptorACBB, IRequestPropertiesSoil requestPropertiesSoil) {
            return super.readDateAndSetVersionTraitement(intervalDate, lineRecord, errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB, requestPropertiesSoil); //To change body of generated methods, choose Tools | Templates.
        }

    }

    class InstanceImpl extends ProcessRecordContentChemistry {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        public CSVParser parser;
        public ErrorsReport errorsReport;
        int counter = 0;

        InstanceImpl() {
            this.parser = new CSVParser(new ByteArrayInputStream(org.apache.commons.lang.StringUtils.EMPTY.getBytes()), ';');
        }

        @Override
        protected void buildNewLines(
                VersionFile version, List<? extends AbstractSoilLineRecord> lines,
                ErrorsReport errorsReport, IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException {
            Assert.assertEquals(m.versionFile, version);
            Assert.assertEquals(ProcessRecordContentChemistryTest.this.lines, lines);
            Assert.assertNotNull(errorsReport);
            Assert.assertFalse(errorsReport.hasErrors());
            Assert.assertEquals(ProcessRecordContentChemistryTest.this.requestPropertiesSoil, requestPropertiesSoil);
            errorsReport.addInfoMessage("true");
            this.errorsReport = errorsReport;
        }

        @Override
        protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(CSVParser p,
                                                                                    Map<Integer, Column> c, DatasetDescriptorACBB dd) throws PersistenceException,
                IOException {
            return variables;
        }

        @Override
        protected long readLines(final CSVParser parser, IntervalDate intervalDate, final IRequestPropertiesSoil requestPropertiesSoil,
                                 final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                                 long lineCount, final List<DatatypeVariableUniteACBB> dbVariables,
                                 final List<ContentChemistryLineRecord> lines) throws IOException {
            Assert.assertEquals(this.parser, parser);
            Assert.assertEquals(BiomassMockUtils.requestPropertiesBiomasse, ProcessRecordContentChemistryTest.this.requestPropertiesSoil);
            Assert.assertNotNull(errorsReport);
            Assert.assertTrue(4L == lineCount);
            Assert.assertEquals(variables, dbVariables);
            Assert.assertEquals(ProcessRecordContentChemistryTest.this.lines, lines);
            errorsReport.addInfoMessage("true");
            this.errorsReport = errorsReport;
            if (2 == ++this.counter) {
                errorsReport.addErrorMessage("error");
            }
            return lineCount;
        }

        @Override
        public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
            this.versionFileDAO = versionFileDAO;
        }
    }
}
