/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;

import java.util.Map;

import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
public class TestValuesSoilTest {

    /**
     *
     */
    public TestValuesSoilTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of checkOtherTypeValue method, of class TestValuesSoil.
     */
    @Test
    public void testCheckOtherTypeValue() {
        String[] values = null;
        BadsFormatsReport badsFormatsReport = null;
        long lineNumber = 0L;
        int index = 0;
        String value = "";
        Column column = null;
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        IRequestPropertiesACBB requestPropertiesACBB = null;
        TestValuesSoil instance = new TestValuesSoil();
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
