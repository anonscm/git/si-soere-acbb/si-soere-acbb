/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.junit.*;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
public class AbstractSoilExtractorTest {

    /**
     *
     */
    public AbstractSoilExtractorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of extract method, of class AbstractSoilExtractor.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testExtract() throws Exception {
        IParameter parameters = null;
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.extract(parameters);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of extractDatas method, of class AbstractSoilExtractor.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testExtractDatas() throws Exception {
        Map<String, Object> requestMetadatasMap = null;
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        Map<String, List> expResult = null;
        Map<String, List> result = instance.extractDatas(requestMetadatasMap);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtractCode method, of class AbstractSoilExtractor.
     */
    @Test
    public void testGetExtractCode() {
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        String expResult = "";
        String result = instance.getExtractCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getResultCode method, of class AbstractSoilExtractor.
     */
    @Test
    public void testGetResultCode() {
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        String expResult = "";
        String result = instance.getResultCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of prepareRequestMetadatas method, of class AbstractSoilExtractor.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testPrepareRequestMetadatas() throws Exception {
        Map<String, Object> requestMetadatasMap = null;
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.prepareRequestMetadatas(requestMetadatasMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSoilExtractionDAO method, of class AbstractSoilExtractor.
     */
    @Test
    public void testSetSoilExtractionDAO() {
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.setSoilExtractionDAO(null);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatatypeVariableUniteACBBDAO method, of class
     * AbstractSoilExtractor.
     */
    @Test
    public void testSetDatatypeVariableUniteACBBDAO() {
        IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO = null;
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.setDatatypeVariableUniteACBBDAO(datatypeVariableUniteACBBDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExtractCode method, of class AbstractSoilExtractor.
     */
    @Test
    public void testSetExtractCode() {
        String extractCode = "";
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.setExtractCode(extractCode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSiteDAO method, of class AbstractSoilExtractor.
     */
    @Test
    public void testSetSiteDAO() {
        ISiteDAO siteDAO = null;
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.setSiteDAO(siteDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVariableDAO method, of class AbstractSoilExtractor.
     */
    @Test
    public void testSetVariableDAO() {
        IVariableDAO variableDAO = null;
        AbstractSoilExtractor instance = new AbstractSoilExtractorImpl();
        instance.setVariableDAO(variableDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     *
     */
    public class AbstractSoilExtractorImpl extends AbstractSoilExtractor {
    }

}
