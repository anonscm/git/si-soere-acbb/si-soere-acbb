/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ptcherniati
 */
public class AbstractSoilLineRecordTest {

    private final Integer anneeNumber = 2;
    private final String observation = "observation";
    private final String campaignName = "campaignName";
    private final String layerName = "layerName";
    private final Float layerSup = 1.2F;
    private final Float layerInf = 2.1F;
    private final long originalLineNumber = 12L;
    private final Integer rotationNumber = 4;
    MockUtils m = MockUtils.getInstance();
    AbstractSoilLineRecord instance;
    private LocalDate date;
    private LocalDate dateDebutTraitement;
    @Mock
    private ACBBVariableValue<ListeSoil> vv1;
    @Mock
    private ACBBVariableValue<ListeSoil> vv2;
    @Mock
    private List variablesValues;
    /**
     *
     */
    public AbstractSoilLineRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractSoilLineRecordImpl(0);
        date = LocalDate.of(2012, Month.JANUARY, 1);
        dateDebutTraitement = LocalDate.of(2011, Month.JANUARY, 1);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testCompareTo() {
        AbstractSoilLineRecord obj = new AbstractSoilLineRecordImpl(0L);
        int result = this.instance.compareTo(obj);
        Assert.assertEquals(0, result);
        obj = new AbstractSoilLineRecordImpl(11L);
        result = this.instance.compareTo(obj);
        Assert.assertEquals(1, result);
        result = obj.compareTo(this.instance);
        Assert.assertEquals(-1, result);
    }

    /**
     * Test of copy method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testCopy() {
        this.instance.setAnneeNumber(this.anneeNumber);
        this.instance.setDate(this.date);
        this.instance.setDateDebuttraitementSurParcelle(this.dateDebutTraitement);
        this.instance.setObservation(this.observation);
        this.instance.setOriginalLineNumber(this.originalLineNumber);
        this.instance.setParcelle(this.m.parcelle);
        this.instance.setRotationNumber(this.rotationNumber);
        this.instance.setSuiviParcelle(this.m.suiviParcelle);
        this.instance.setTraitementProgramme(this.m.traitement);
        this.instance.setVariablesValues(this.variablesValues);
        this.instance.setVersionDeTraitement(this.m.versionDeTraitement);
        this.instance.setCampaignName(campaignName);
        this.instance.setLayerName(layerName);
        this.instance.setLayerInf(layerInf);
        this.instance.setLayerSup(layerSup);
        AbstractSoilLineRecord copy = new AbstractSoilLineRecordImpl(2L);
        Assert.assertTrue(2L == copy.getOriginalLineNumber());
        copy.copy(this.instance);
        Assert.assertEquals(this.anneeNumber, copy.getAnneeNumber());
        Assert.assertEquals(this.date, copy.getDate());
        Assert.assertEquals(this.dateDebutTraitement, copy.getDateDebuttraitementSurParcelle());
        Assert.assertEquals(this.observation, copy.getObservation());
        Assert.assertTrue(this.originalLineNumber == copy.getOriginalLineNumber());
        Assert.assertEquals(this.m.parcelle, copy.getParcelle());
        Assert.assertEquals(this.m.traitement, copy.getTraitementProgramme());
        Assert.assertEquals(this.variablesValues, copy.getVariablesValues());
        Assert.assertEquals(this.m.versionDeTraitement, copy.getVersionDeTraitement());
        Assert.assertEquals(this.campaignName, copy.getCampaignName());
        Assert.assertEquals(this.layerName, copy.getLayerName());
        Assert.assertEquals(this.layerInf, copy.getLayerInf());
        Assert.assertEquals(this.layerSup, copy.getLayerSup());
    }

    /**
     * Test of equals method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testEquals() {
        Object obj = new AbstractSoilLineRecordImpl(0L);
        boolean result = this.instance.equals(obj);
        Assert.assertTrue(result);
        obj = new AbstractSoilLineRecordImpl(2L);
        result = this.instance.equals(obj);
        Assert.assertFalse(result);
        result = this.instance.equals("s");
        Assert.assertFalse(result);
    }

    /**
     * Test of getAnneeNumber method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetAnneeNumber() {
        Integer expResult = 32;
        this.instance.setAnneeNumber(expResult);
        Integer result = this.instance.getAnneeNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetDate() {
        this.instance.setDate(this.m.dateDebut);
        LocalDate result = this.instance.getDate();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getDateDebuttraitementSurParcelle method, of class
     * AbstractSoilLineRecord.
     */
    @Test
    public void testGetDateDebuttraitementSurParcelle() {
        this.instance.setDateDebuttraitementSurParcelle(this.m.dateDebut);
        LocalDate result = this.instance.getDateDebuttraitementSurParcelle();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getObservation method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetObservation() {
        String expResult = "observation";
        this.instance.setObservation(expResult);
        String result = this.instance.getObservation();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getOriginalLineNumber method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetOriginalLineNumber() {
        long expResult = 133L;
        this.instance.setOriginalLineNumber(expResult);
        long result = this.instance.getOriginalLineNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getParcelle method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetParcelle() {
        this.instance.setParcelle(this.m.parcelle);
        Parcelle result = this.instance.getParcelle();
        Assert.assertEquals(this.m.parcelle, result);
    }

    /**
     * Test of getRotationNumber method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetRotationNumber() {
        Integer expResult = 24;
        this.instance.setRotationNumber(expResult);
        Integer result = this.instance.getRotationNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetSuiviParcelle() {
        this.instance.setSuiviParcelle(this.m.suiviParcelle);
        SuiviParcelle result = this.instance.getSuiviParcelle();
        Assert.assertEquals(this.m.suiviParcelle, result);
    }

    /**
     * Test of getTraitementProgramme method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetTraitementProgramme() {
        this.instance.setTraitementProgramme(this.m.traitement);
        TraitementProgramme result = this.instance.getTraitementProgramme();
        Assert.assertEquals(this.m.traitement, result);
    }

    /**
     * Test of getVariablesValues method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetVariablesValues() {
        List<ACBBVariableValue> expResult = Arrays.asList(new ACBBVariableValue[]{this.vv1,
                this.vv2});
        this.instance.setVariablesValues(expResult);
        List<ACBBVariableValue> result = this.instance.getVariablesValues();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVersionDeTraitement method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetVersionDeTraitement() {
        this.instance.setVersionDeTraitement(this.m.versionDeTraitement);
        VersionDeTraitement result = this.instance.getVersionDeTraitement();
        Assert.assertEquals(this.m.versionDeTraitement, result);
    }


    /**
     * Test of getLayerInf method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetLayerInf() {
        instance.setLayerInf(layerInf);
        assertTrue(layerInf == instance.getLayerInf());
    }

    /**
     * Test of getLayerSup method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetLayerSup() {
        instance.setLayerSup(layerSup);
        assertTrue(layerSup == instance.getLayerSup());
    }

    /**
     * Test of getCampaignName method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetCampaignName() {
        instance.setCampaignName(campaignName);
        assertEquals(campaignName, instance.getCampaignName());
    }

    /**
     * Test of setLayerInf method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetLayerInf() {
        final Float layerInf = 2.1F;
        AbstractSoilLineRecord instance = null;
        instance.setLayerInf(layerInf);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLayerSup method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetLayerSup() {
        final Float layerSup = 1.2F;
        AbstractSoilLineRecord instance = null;
        instance.setLayerSup(layerSup);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCampaignName method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetCampaignName() {
        String campaignName = "";
        AbstractSoilLineRecord instance = null;
        instance.setCampaignName(campaignName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLayerName method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testGetLayerName() {
        AbstractSoilLineRecord instance = null;
        String expResult = "";
        String result = instance.getLayerName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLayerName method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetLayerName() {
        String layerName = "";
        AbstractSoilLineRecord instance = null;
        instance.setLayerName(layerName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAnneeNumber method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetAnneeNumber() {
        Integer anneeNumber = null;
        AbstractSoilLineRecord instance = null;
        instance.setAnneeNumber(anneeNumber);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDate method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetDate() {
        LocalDate date = null;
        AbstractSoilLineRecord instance = null;
        instance.setDate(date);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDateDebuttraitementSurParcelle method, of class
     * AbstractSoilLineRecord.
     */
    @Test
    public void testSetDateDebuttraitementSurParcelle() {
        LocalDate dateDebuttraitementSurParcelle = null;
        AbstractSoilLineRecord instance = null;
        instance.setDateDebuttraitementSurParcelle(dateDebuttraitementSurParcelle);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setObservation method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetObservation() {
        String observation = "";
        AbstractSoilLineRecord instance = null;
        instance.setObservation(observation);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOriginalLineNumber method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetOriginalLineNumber() {
        long originalLineNumber = 0L;
        AbstractSoilLineRecord instance = null;
        instance.setOriginalLineNumber(originalLineNumber);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParcelle method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetParcelle() {
        Parcelle parcelle = null;
        AbstractSoilLineRecord instance = null;
        instance.setParcelle(parcelle);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRotationNumber method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetRotationNumber() {
        Integer rotationNumber = null;
        AbstractSoilLineRecord instance = null;
        instance.setRotationNumber(rotationNumber);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSuiviParcelle method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetSuiviParcelle() {
        SuiviParcelle suiviParcelle = null;
        AbstractSoilLineRecord instance = null;
        instance.setSuiviParcelle(suiviParcelle);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTraitementProgramme method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetTraitementProgramme() {
        TraitementProgramme traitementProgramme = null;
        AbstractSoilLineRecord instance = null;
        instance.setTraitementProgramme(traitementProgramme);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVariablesValues method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetVariablesValues() {
        AbstractSoilLineRecord instance = null;
        instance.setVariablesValues(null);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVersionDeTraitement method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testSetVersionDeTraitement() {
        VersionDeTraitement versionDeTraitement = null;
        AbstractSoilLineRecord instance = null;
        instance.setVersionDeTraitement(versionDeTraitement);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hashCode method, of class AbstractSoilLineRecord.
     */
    @Test
    public void testHashCode() {
        AbstractSoilLineRecord instance = null;
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     *
     */
    public class AbstractSoilLineRecordImpl extends AbstractSoilLineRecord<AbstractSoilLineRecordImpl, AbstractVariableValueSoil> {

        /**
         * @param lineNumber
         */
        public AbstractSoilLineRecordImpl(long lineNumber) {
            super(lineNumber);
        }
    }

}
