/*
 *
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.test.utils;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.soil.impl.RequestPropertiesSoil;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.DateTimeException;

/**
 * @author ptcherniati
 */
public class SoilMockUtils extends MockUtils {

    /**
     *
     */
    @Mock
    public static Modalite modalite;

    /**
     *
     */
    @Mock
    public static IModaliteDAO modaliteDAO;

    /**
     *
     */
    @Mock
    public static RequestPropertiesSoil requestPropertiesSoil;

    /**
     *
     */
    @Mock
    public static IInterventionDAO interventionDAO;

    /**
     *
     */
    @Mock
    public static IListeSoilDAO listeSoilDAO;

    /**
     *
     */
    @Mock
    public static IListeItineraireDAO listeItineraireDAO;

    /**
     *
     */
    @Mock
    public static DatasetDescriptorACBB datasetDescriptorACBB;

    /**
     *
     */
    @Mock
    public static AbstractIntervention intervention;

    /**
     *
     */
    @Mock
    public static CSVParser parser;

    /**
     *
     */
    public SoilMockUtils() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * @return
     */
    public static SoilMockUtils getInstance() {

        SoilMockUtils instance = new SoilMockUtils();

        MockitoAnnotations.initMocks(instance);

        try {

            instance.initMocks();

        } catch (DateTimeException | BadExpectedValueException ex) {

            return null;

        }

        instance.initLocalization();

        return instance;
    }

}
