/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.junit.*;

import java.io.File;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
public class RecorderTest {

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        CSVParser parser = null;
        File file = null;
        String encoding = "";
        Recorder instance = new Recorder();
        instance.deleteRecord(parser, file, encoding);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filter method, of class Recorder.
     */
    @Test
    public void testFilter() {
        Set<AbstractMethode> allMethods = null;
        Recorder instance = new Recorder();
        Set<MethodeAnalyse> expResult = null;
        Set<MethodeAnalyse> result = instance.filter(allMethods);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        Recorder instance = new Recorder();
        List<MethodeAnalyse> expResult = null;
        List<MethodeAnalyse> result = instance.getAllElements();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMethodeDefinitionName method, of class Recorder.
     */
    @Test
    public void testGetMethodeDefinitionName() {
        Recorder instance = new Recorder();
        String expResult = "";
        String result = instance.getMethodeDefinitionName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMethodeEntityName method, of class Recorder.
     */
    @Test
    public void testGetMethodeEntityName() {
        Recorder instance = new Recorder();
        String expResult = "";
        String result = instance.getMethodeEntityName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMethodeLibelleName method, of class Recorder.
     */
    @Test
    public void testGetMethodeLibelleName() {
        Recorder instance = new Recorder();
        String expResult = "";
        String result = instance.getMethodeLibelleName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        MethodeAnalyse methodeAnalyse = null;
        Recorder instance = new Recorder();
        LineModelGridMetadata expResult = null;
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(methodeAnalyse);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     */
    @Test
    public void testInitModelGridMetadata() {
        Recorder instance = new Recorder();
        ModelGridMetadata<MethodeAnalyse> expResult = null;
        ModelGridMetadata<MethodeAnalyse> result = instance.initModelGridMetadata();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        CSVParser parser = null;
        File file = null;
        String encoding = "";
        Recorder instance = new Recorder();
        instance.processRecord(parser, file, encoding);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListeSoilDAO method, of class Recorder.
     */
    @Test
    public void testSetListeSoilDAO() {
        IListeSoilDAO listeSoilDAO = null;
        Recorder instance = new Recorder();
        instance.setListeSoilDAO(listeSoilDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMethodeAnalyseSoilDAO method, of class Recorder.
     */
    @Test
    public void testSetMethodeAnalyseSoilDAO() {
        IMethodeAnalyseDAO methodeAnalyseDAO = null;
        Recorder instance = new Recorder();
        instance.setMethodeAnalyseSoilDAO(methodeAnalyseDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
