/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl.VariableValueContentChemistry;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author tcherniatinsky
 */
public class AbstractProcessRecordSoilTest {

    MockUtils m = MockUtils.getInstance();
    AbstractProcessRecordSoil instance;
    @Mock(name = "listeSoilDAO")
    IListeSoilDAO listeSoilDAO;
    @Mock(name = "line")
    AbstractSoilLineRecord line;
    @Mock(name = "datasetDescriptorACBB")
    DatasetDescriptorACBB datasetDescriptorACBB;
    @Mock(name = "requestPropertiesSoil")
    RequestPropertiesSoil requestPropertiesSoil;
    @Mock(name = "variableValues")
    List<ACBBVariableValue> variableValues;
    @Mock(name = "column1")
    Column column1;
    @Mock(name = "column2")
    Column column2;
    @Mock(name = "columnNoFlag")
    Column columnNoFlag;
    @Mock(name = "variableACBB1")
    VariableACBB variableACBB1;
    @Mock(name = "variableACBB2")
    VariableACBB variableACBB2;
    @Mock(name = "dvu1")
    DatatypeVariableUniteACBB dvu1;
    @Mock(name = "dvu2")
    DatatypeVariableUniteACBB dvu2;
    @Mock(name = "parser")
    CSVParser parser;
    @Mock(name = "columnEntry")
    Map.Entry<Integer, Column> columnEntry;
    @Mock(name = "lineRecord")
    AbstractSoilLineRecord lineRecord;
    Integer rotationNumber = 2;
    Integer anneeNumber = 3;
    Integer periodeNumber = 4;
    @Mock
    Properties properties;
    @Mock(name = "vv1")
    private ACBBVariableValue<ListeSoil> vv1;
    @Mock(name = "vv2")
    private ACBBVariableValue<ListeSoil> vv2;

    /**
     *
     */
    public AbstractProcessRecordSoilTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private void initColumns() {
        when(this.column1.isFlag()).thenReturn(Boolean.TRUE);
        when(this.column1.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        when(this.column2.isFlag()).thenReturn(Boolean.TRUE);
        when(this.column2.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
    }

    /**
     * @param instance
     */
    protected void initInstance(AbstractProcessRecordSoil instance) {
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setListeACBBDAO(this.listeSoilDAO);
        instance.setListeSoilDAO(this.listeSoilDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setVersionFileDAO(this.m.versionFileDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
    }

    private void initLine() {
        when(this.line.getAnneeNumber()).thenReturn(this.anneeNumber);
        when(this.line.getParcelle()).thenReturn(this.m.parcelle);
        when(this.line.getDate()).thenReturn(this.m.dateDebut);
        when(this.line.getDateDebuttraitementSurParcelle()).thenReturn(this.m.dateFin);
        when(this.line.getObservation()).thenReturn("observation");
        when(this.line.getOriginalLineNumber()).thenReturn(12L);
        when(this.line.getRotationNumber()).thenReturn(this.rotationNumber);
        when(this.line.getSuiviParcelle()).thenReturn(this.m.suiviParcelle);
        when(this.line.getTraitementProgramme()).thenReturn(this.m.traitement);
        when(this.line.getVariablesValues()).thenReturn(this.variableValues);
        when(this.line.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractProcessRecordSoilImpl();
        this.initInstance(this.instance);
        this.variableValues = Arrays.asList(new ACBBVariableValue[]{this.vv1, this.vv2});
        when(this.columnEntry.getValue()).thenReturn(this.column1);
        this.initLine();
        this.initColumns();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getListeSoilDAO method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testGetListeSoilDAO() {
        IListeSoilDAO result = this.instance.getListeSoilDAO();
        assertEquals(this.listeSoilDAO, result);

    }

    /**
     * Test of getVariable method, of class AbstractProcessRecordSoil.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetVariable() throws Exception {
        this.instance = new AbstractProcessRecordSoilImplNoVariable();
        this.initInstance(this.instance);
        // by code
        when(this.column1.getName()).thenReturn(MockUtils.VARIABLE_AFFICHAGE);
        Optional<VariableACBB> result = this.instance.getVariable(this.columnEntry);
        assertEquals(this.m.variable, result);
        verify(this.m.variableDAO).getByAffichage(MockUtils.VARIABLE_AFFICHAGE);
        // by affichage
        when(this.column1.getRefVariableName()).thenReturn("VariableCode");
        result = this.instance.getVariable(this.columnEntry);
        assertEquals(this.m.variable, result);
        verify(this.m.variableDAO).getByCode(MockUtils.VARIABLE_CODE);
    }

    /**
     * Test of readDateAndSetVersionTraitement method, of class
     * AbstractProcessRecordSoil.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testReadDateAndSetVersionTraitement() throws BadExpectedValueException {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.DATE_DEBUT, "2",
                "3"});
        this.instance = spy(this.instance);
        doReturn(this.m.dateDebut).when(this.instance)
                .getDate(errorsReport, 1L, 1, cleanerValues, this.datasetDescriptorACBB);
        doNothing()
                .when(this.instance)
                .updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                        this.datasetDescriptorACBB, this.requestPropertiesSoil);
        when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        IntervalDate intervalDate = new IntervalDate(m.dateTimeDebut, m.dateTimeFin, DateUtil.YYYY_MM_DD_HHMMSS_SSSZ);
        // bnominal
        int result = this.instance.readDateAndSetVersionTraitement(intervalDate, this.lineRecord, errorsReport,
                1L, 1, cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertEquals(2, result);
        verify(this.instance).getDate(errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptorACBB);
        verify(this.instance).updateVersionDeTraitement(this.lineRecord, errorsReport, 1L,
                1, this.datasetDescriptorACBB, this.requestPropertiesSoil);
        // null date
        doReturn(null).when(this.lineRecord).getDate();
        result = this.instance.readDateAndSetVersionTraitement(intervalDate, this.lineRecord, errorsReport, 1L,
                1, cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertEquals(2, result);
        verify(this.instance, times(2)).getDate(errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptorACBB);
        verify(this.instance, times(1)).updateVersionDeTraitement(this.lineRecord,
                errorsReport, 1L, 1, this.datasetDescriptorACBB, this.requestPropertiesSoil);
    }

    /**
     * Test of readGenericColumns method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadGenericColumns() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(null);
        this.instance = spy(this.instance);
        doReturn(1)
                .when(this.instance)
                .readParcelle(this.lineRecord, errorsReport, 1L, 0, cleanerValues,
                        this.datasetDescriptorACBB, this.requestPropertiesSoil);
        doReturn(2).when(this.instance).readObservation(errorsReport, cleanerValues, this.lineRecord, 1);
        doReturn(4)
                .when(this.instance)
                .readRotationAnnee(cleanerValues, errorsReport, this.lineRecord, 1L, 2,
                        this.datasetDescriptorACBB);
        // nominal
        when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle, (Parcelle) null);
        int result = this.instance.readGenericColumns(errorsReport, 1L, cleanerValues,
                this.lineRecord, this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertEquals(8, result);
        // nill parcelle
        result = this.instance.readGenericColumns(errorsReport, 1L, cleanerValues, this.lineRecord,
                this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertEquals(8, result);
    }

    /**
     * Test of readObservation method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadObservation() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"observation", null});
        // nominal
        int result = this.instance.readObservation(errorsReport, cleanerValues, this.line, 2);
        assertEquals(3, result);
        verify(this.line).setObservation("observation");
        // nominal
        result = this.instance.readObservation(errorsReport, cleanerValues, this.line, 3);
        assertEquals(4, result);
        verify(this.line).setObservation(null);

    }

    /**
     * Test of readParcelle method, of class AbstractProcessRecordSoil.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testReadParcelle() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.PARCELLE_NOM,
                "badParcelle", "badParcelle"});
        // nominal
        when(this.requestPropertiesSoil.getSite()).thenReturn(this.m.site);
        int result = this.instance.readParcelle(this.lineRecord, errorsReport, 1L, 2,
                cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertEquals(3, result);
        // null parcelle
        when(this.m.parcelleDAO.getByNKey("site_badparcelle")).thenReturn(null);
        result = this.instance.readParcelle(this.lineRecord, errorsReport, 1L, 3, cleanerValues,
                this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertTrue(errorsReport.hasErrors());
        assertEquals(4, result);
        assertEquals(
                "-La parcelle badparcelle définie ligne 1 colonne 4 n'existe pas pour le site Site.\n",
                errorsReport.getErrorsMessages());
        // null parcelle
        errorsReport = new ErrorsReport();
        when(this.m.parcelleDAO.getByNKey("site_badparcelle")).thenThrow(
                new PersistenceException("erreur"));
        result = this.instance.readParcelle(this.lineRecord, errorsReport, 1L, 5, cleanerValues,
                this.datasetDescriptorACBB, this.requestPropertiesSoil);
        assertTrue(errorsReport.hasErrors());
        assertEquals(6, result);
        assertEquals(
                "-Aucune parcelle n'a été définie ligne 1 colonne 5.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readRotationAnneePeriode method, of class
     * AbstractProcessRecordSoil.
     */
    @Test
    public void testReadRotationAnnee() throws EndOfCSVLine {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"1", "2", "3"});
        int result = this.instance.readRotationAnnee(cleanerValues, errorsReport,
                this.lineRecord, 1L, 1, this.datasetDescriptorACBB);
        assertEquals(4, result);
        verify(this.lineRecord).setRotationNumber(1);
        verify(this.lineRecord).setAnneeNumber(2);
        assertEquals("3", cleanerValues.nextToken());
    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordSoil.
     */
    @Test
    public void testUpdateVersionDeTraitementNominal() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.of(this.m.suiviParcelle));
        when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.of(this.m.versionDeTraitement));
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesSoil);
        verify(this.lineRecord).setSuiviParcelle(this.m.suiviParcelle);
        verify(this.lineRecord).setTraitementProgramme(this.m.traitement);
        verify(this.lineRecord).setVersionDeTraitement(this.m.versionDeTraitement);
        verify(this.lineRecord).setDateDebuttraitementSurParcelle(this.m.dateDebut);
        assertFalse(errorsReport.hasErrors());

    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordSoil.
     */
    @Test
    public void testUpdateVersionDeTraitementNullSuiviParcelle() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        ILocalizationManager localizationManager = mock(ILocalizationManager.class);
        Properties properties = mock(Properties.class);
        this.instance.setLocalizationManager(localizationManager);
        when(this.m.traitement.getNom()).thenReturn(MockUtils.TRAITEMENT);
        when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(null);
        when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(null);
        when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(properties.getProperty(MockUtils.PARCELLE_NOM, MockUtils.PARCELLE_NOM))
                .thenReturn(MockUtils.PARCELLE_NOM);
        when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn(
                MockUtils.SITE_NOM);
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesSoil);
        verify(this.lineRecord, never()).setSuiviParcelle(this.m.suiviParcelle);
        verify(this.lineRecord, never()).setTraitementProgramme(this.m.traitement);
        verify(this.lineRecord, never()).setVersionDeTraitement(
                this.m.versionDeTraitement);
        verify(this.lineRecord, never()).setDateDebuttraitementSurParcelle(
                this.m.dateDebut);
        assertTrue(errorsReport.hasErrors());
        assertEquals(
                "-Il n'existe pas de suivi de parcelle pour la date 01/01/2012, sur la parcelle Parcelle du site Site;\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordSoil.
     */
    @Test
    public void testUpdateVersionDeTraitementNullVersionTraitement() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        ILocalizationManager localizationManager = mock(ILocalizationManager.class);
        Properties properties = mock(Properties.class);
        this.instance.setLocalizationManager(localizationManager);
        when(this.m.traitement.getNom()).thenReturn(MockUtils.TRAITEMENT);
        when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.of(this.m.suiviParcelle));
        when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(null);
        when(
                localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA,
                        TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE)).thenReturn(properties);
        when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(
                properties.getProperty(MockUtils.TRAITEMENT_AFFICHAGE,
                        MockUtils.TRAITEMENT_AFFICHAGE)).thenReturn(MockUtils.TRAITEMENT_AFFICHAGE);
        when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn(
                MockUtils.SITE_NOM);
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesSoil);
        verify(this.lineRecord).setSuiviParcelle(this.m.suiviParcelle);
        verify(this.lineRecord).setTraitementProgramme(this.m.traitement);
        verify(this.lineRecord, never()).setVersionDeTraitement(
                this.m.versionDeTraitement);
        verify(this.lineRecord, never()).setDateDebuttraitementSurParcelle(
                this.m.dateDebut);
        assertTrue(errorsReport.hasErrors());
        assertEquals(
                "-Il n'existe pas de version de traitement pour la date 01/01/2012, pour le traitement traitement (Traitement) du site Site;\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordSoil.
     */
    @Test
    public void testUpdateVersionDeTraitement() {
        ErrorsReport errorsReport = new ErrorsReport();
        instance.setLocalizationManager(MockUtils.localizationManager);
        final String affichage = "affichage";
        when(MockUtils.localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, affichage)).thenReturn(properties);
        when(MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(MockUtils.localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, affichage)).thenReturn(properties);
        when(MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(properties.getProperty(affichage, affichage)).thenReturn("un affichage");
        when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn("un nom de site");
        when(properties.getProperty(MockUtils.PARCELLE_NOM, MockUtils.PARCELLE_NOM)).thenReturn("un nom de parcelle");
        //nominal
        when(lineRecord.getParcelle()).thenReturn(m.parcelle);
        when(lineRecord.getDate()).thenReturn(m.dateDebut);
        when(m.suiviParcelle.getTraitement()).thenReturn(m.traitement);
        when(lineRecord.getTraitementProgramme()).thenReturn(m.traitement);
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(m.parcelle, m.dateDebut)).thenReturn(Optional.of(m.suiviParcelle));
        when(m.suiviParcelle.getDateDebutTraitement()).thenReturn(m.dateFin);
        when(m.versionDeTraitementDAO
                .retrieveVersiontraitement(m.traitement, m.dateDebut)).thenReturn(Optional.of(m.versionDeTraitement));
        instance.updateVersionDeTraitement(lineRecord, errorsReport, 2L, 1, datasetDescriptorACBB, requestPropertiesSoil);
        verify(lineRecord).setSuiviParcelle(m.suiviParcelle);
        verify(lineRecord).setTraitementProgramme(m.traitement);
        verify(lineRecord).setVersionDeTraitement(m.versionDeTraitement);
        verify(lineRecord).setDateDebuttraitementSurParcelle(m.dateFin);
        //with null versiondetraitement
        when(m.versionDeTraitementDAO
                .retrieveVersiontraitement(m.traitement, m.dateDebut)).thenReturn(null);
        when(m.traitement.getAffichage()).thenReturn(affichage);
        when(m.traitement.getNom()).thenReturn("nom du traitement");
        instance.updateVersionDeTraitement(lineRecord, errorsReport, 2L, 1, datasetDescriptorACBB, requestPropertiesSoil);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Il n'existe pas de version de traitement pour la date 01/01/2012, pour le traitement nom du traitement (un affichage) du site un nom de site;\n", errorsReport.getErrorsMessages());
        //with null suiviParcelle
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(m.parcelle, m.dateDebut)).thenReturn(null);
        errorsReport = new ErrorsReport();
        instance.updateVersionDeTraitement(lineRecord, errorsReport, 2L, 1, datasetDescriptorACBB, requestPropertiesSoil);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Il n'existe pas de suivi de parcelle pour la date 01/01/2012, sur la parcelle un nom de parcelle du site un nom de site;\n", errorsReport.getErrorsMessages());

    }

    /**
     * Test of readMeasureNumber method, of class ProcessRecordSoilProduction.
     */
    @Test
    public void testReadMeasureNumber() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"2"};
        CleanerValues cleanerValues = new CleanerValues(values);
        AbstractVariableValueSoil variableValue = mock(AbstractVariableValueSoil.class);
        // nominal
        int result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        assertEquals(1, result);
        verify(variableValue).setMeasureNumber(2);
        // empty mesearenumber
        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues, variableValue);
        assertEquals(1, result);
        verify(variableValue, times(1)).setMeasureNumber(1);
        assertFalse(errorsReport.hasErrors());
        // null mesearenumber
        values = new String[]{null};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues, variableValue);
        assertEquals(1, result);
        verify(variableValue, times(2)).setMeasureNumber(1);
        assertFalse(errorsReport.hasErrors());
        // bad mesearenumber
        values = new String[]{"deux"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues, variableValue);
        assertEquals(1, result);
        verify(variableValue, times(2)).setMeasureNumber(1);
        assertTrue(errorsReport.hasErrors());
        assertEquals(
                "-Ligne 1, colonne 1, le nombre de mesure \"deux\" doit être un entier positif.\n",
                errorsReport.getErrorsMessages());

    }

    /**
     * Test of readStandardDeviation method, of class
     * ProcessRecordSoilProduction.
     */
    @Test
    public void testReadStandardDeviation() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"2.3"};
        CleanerValues cleanerValues = new CleanerValues(values);
        AbstractVariableValueSoil variableValue = mock(AbstractVariableValueSoil.class);
        // nominal
        int result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        assertEquals(1, result);
        verify(variableValue, times(1)).setStandardDeviation(2.3F);
        assertFalse(errorsReport.hasErrors());
        // empty standart deviation
        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        assertEquals(1, result);
        verify(variableValue, times(1)).setStandardDeviation(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        assertFalse(errorsReport.hasErrors());
        // null standart deviation
        values = new String[]{null};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        assertEquals(1, result);
        verify(variableValue, times(2)).setStandardDeviation(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        assertFalse(errorsReport.hasErrors());
        // bad standart deviation
        values = new String[]{"deux"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        assertEquals(1, result);
        verify(variableValue, times(2)).setStandardDeviation(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Ligne 1, colonne 1, l'écart-type \"deux\" doit être un décimal.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readQualityIndex method, of class ProcessRecordSoilProduction.
     */
    @Test
    public void testReadQualityIndex() {
        String[] values = new String[]{org.apache.commons.lang.StringUtils.EMPTY, "2", "deux", "-5"};
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(values);
        VariableValueContentChemistry variableValue = mock(VariableValueContentChemistry.class);
        //null qualityIndex
        int result = instance.readQualityIndex(errorsReport, 2L, 1, cleanerValues, variableValue);
        assertTrue(2 == result);
        verify(variableValue).setQualityClass(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        //good qualityIndex
        result = instance.readQualityIndex(errorsReport, 2L, 2, cleanerValues, variableValue);
        assertTrue(3 == result);
        verify(variableValue).setQualityClass(2);
        //bad qualityIndex
        result = instance.readQualityIndex(errorsReport, 4L, 3, cleanerValues, variableValue);
        assertTrue(4 == result);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Ligne 4, colonne 4, l'indice de qualité \"deux\" doit être un entier positif.\n", errorsReport.getErrorsMessages());
        //negative qualityIndex
        errorsReport = new ErrorsReport();
        result = instance.readQualityIndex(errorsReport, 5L, 4, cleanerValues, variableValue);
        assertTrue(5 == result);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Ligne 5, colonne 5, l'indice de qualité \"-5\" doit être un entier positif.\n", errorsReport.getErrorsMessages());
    }

    /**
     * Test of readValue method, of class ProcessRecordSoilProduction.
     */
    @Test
    public void testReadValue() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"12.0", "2", "11.4", "102", "2"};
        VariableValueContentChemistry variableValue = mock(VariableValueContentChemistry.class);
        CleanerValues cleanerValues = new CleanerValues(values);
        int result = instance.readValue(errorsReport, cleanerValues, variableValue, 1L, 1);
        assertTrue(2 == result);
        verify(variableValue).setValue("12.0");
        result = instance.readValue(errorsReport, cleanerValues, variableValue, 1L, 2);
        assertTrue(3 == result);
        verify(variableValue).setValue("2");
    }

    /**
     * Test of buildNewLines method, of class AbstractProcessRecordSoil.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildNewLines() throws Exception {
        VersionFile version = null;
        List<? extends AbstractSoilLineRecord> lines = null;
        ErrorsReport errorsReport = null;
        IRequestPropertiesSoil requestPropertiesSoil = null;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        instance.buildNewLines(version, lines, errorsReport, requestPropertiesSoil);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildVariablesHeaderAndSkipHeader method, of class
     * AbstractProcessRecordSoil.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildVariablesHeaderAndSkipHeader() throws Exception {
        CSVParser parser = null;
        Map<Integer, Column> columns = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        List<DatatypeVariableUniteACBB> expResult = null;
        List<DatatypeVariableUniteACBB> result = instance.buildVariablesHeaderAndSkipHeader(parser, columns, datasetDescriptorACBB);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBoolean method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testGetBoolean() {
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int columnIndex = 0;
        CleanerValues cleanerValues = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        boolean expResult = false;
//        boolean result = instance.getBoolean(errorsReport, lineCount, columnIndex, cleanerValues, datasetDescriptorACBB);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readCampaign method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadCampaign() {
        CleanerValues cleanerValues = null;
        AbstractSoilLineRecord lineRecord = null;
        int index = 0;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        int expResult = 0;
//        int result = instance.readCampaign(cleanerValues, lineRecord, index);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListeSoilDAO method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testSetListeSoilDAO() {
        IListeSoilDAO listeSoilDAO = null;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        instance.setListeSoilDAO(listeSoilDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readMediane method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadMediane() {
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        CleanerValues cleanerValues = null;
        ACBBVariableValue<IValeurQualitative> variableValue = null;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        int expResult = 0;
        int result = instance.readMediane(errorsReport, lineCount, index, cleanerValues, variableValue);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readLayerInf method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadLayerInf() {
        CleanerValues cleanerValues = null;
        AbstractSoilLineRecord lineRecord = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        int expResult = 0;
        int result = instance.readLayerInf(cleanerValues, lineRecord, datasetDescriptorACBB, errorsReport, lineCount, index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readLayerSup method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadLayerSup() {
        CleanerValues cleanerValues = null;
        AbstractSoilLineRecord lineRecord = null;
        DatasetDescriptorACBB datasetDescriptorACBB = null;
        ErrorsReport errorsReport = null;
        long lineCount = 0L;
        int index = 0;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        int expResult = 0;
        int result = instance.readLayerSup(cleanerValues, lineRecord, datasetDescriptorACBB, errorsReport, lineCount, index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readLayerName method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testReadLayerName() {
        CleanerValues cleanerValues = null;
        AbstractSoilLineRecord lineRecord = null;
        int index = 0;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        int expResult = 0;
//        int result = instance.readLayerName(cleanerValues, lineRecord, index);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of skipValue method, of class AbstractProcessRecordSoil.
     */
    @Test
    public void testSkipValue() {
        CleanerValues cleanerValues = null;
        int index = 0;
        int nbColumsToSkip = 0;
        AbstractProcessRecordSoil instance = new AbstractProcessRecordSoilImpl();
        int expResult = 0;
        int result = instance.skipValue(cleanerValues, index, nbColumsToSkip);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of readMediane method, of class AbstractProcessRecordSoil.
     */
    /*@Test
    public void testReadMediane() {
    System.out.println("readMediane");
    ErrorsReport errorsReport = new ErrorsReport();
    final String mediane = "mediane";
    String[] values = new String[] { mediane};
    CleanerValues cleanerValues = new CleanerValues(values);
    AbstractSoilLineRecord line = mock(AbstractSoilLineRecord.class);
    // nominal
    int result = this.instance.readMediane(2, cleanerValues, vv1);
    assertEquals(1, result);
    verify(line).setNatureCouvert(mediane);
    }*/

    /**
     *
     */
    public class AbstractProcessRecordSoilImpl extends AbstractProcessRecordSoil {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        Stack<VariableACBB> variableACBBs = new Stack();

        /**
         *
         */
        public AbstractProcessRecordSoilImpl() {
            this.variableACBBs.push(variableACBB2);
            this.variableACBBs.push(variableACBB1);
        }

        /**
         * @param version
         * @param lines
         * @param errorsReport
         * @param requestPropertiesSoil
         * @throws PersistenceException
         */
        @Override
        public void buildNewLines(VersionFile version, List<? extends AbstractSoilLineRecord> lines,
                                  ErrorsReport errorsReport, IRequestPropertiesSoil requestPropertiesSoil)
                throws PersistenceException {
        }

        /**
         * @param colonneEntry
         * @return
         */
        @Override
        protected Optional<VariableACBB> getVariable(Map.Entry<Integer, Column> colonneEntry) {
            VariableACBB variable = this.variableACBBs.isEmpty() ? null : this.variableACBBs.pop();
            return Optional.of(variable);
        }
    }

    /**
     *
     */
    public class AbstractProcessRecordSoilImplNoVariable extends AbstractProcessRecordSoil {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * @param version
         * @param lines
         * @param errorsReport
         * @param requestPropertiesSoil
         * @throws PersistenceException
         */
        @Override
        public void buildNewLines(VersionFile version, List<? extends AbstractSoilLineRecord> lines,
                                  ErrorsReport errorsReport, IRequestPropertiesSoil requestPropertiesSoil)
                throws PersistenceException {
        }
    }

}
