package org.inra.ecoinfo.acbb.dataset.soil.soildensity.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class SoilDensityLineRecord extends AbstractSoilLineRecord<SoilDensityLineRecord, VariableValueSoilDensity> {

    Methodesample methodeObtention;

    /**
     * @param originalLineNumber
     */
    public SoilDensityLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }
}
