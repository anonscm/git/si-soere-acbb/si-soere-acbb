package org.inra.ecoinfo.acbb.dataset.soil.soilstock.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class SoilStockLineRecord extends AbstractSoilLineRecord<SoilStockLineRecord, VariableValueSoilStock> {

    /**
     * @param originalLineNumber
     */
    public SoilStockLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }
}
