/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.IRequestPropertiesContentChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.impl.RequestPropertiesSoil;

import java.io.Serializable;

/**
 * The Class RequestPropertiesAI.
 * <p>
 * record the general information of the file
 */
public class RequestPropertiesContentChemistry extends RequestPropertiesSoil implements
        IRequestPropertiesContentChemistry, Serializable {
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new request properties ai.
     */
    public RequestPropertiesContentChemistry() {
        super();
    }

    /**
     * @return @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getTestDuplicates()
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateContentChemistry();
    }
}
