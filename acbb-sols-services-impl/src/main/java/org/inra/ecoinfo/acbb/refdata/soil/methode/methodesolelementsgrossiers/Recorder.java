package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolelementsgrossiers;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Class Recorder.
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractMethodeRecorder<MethodeSoilCoarse> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodesolelementsgrossiers.messages";

    private IMethodeSoilCoarseDAO methodeSoilCoarseDAO;

    /**
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String numberIdString = tokenizerValues.nextToken();
                ErrorsReport errorsReport = new ErrorsReport();
                MethodeSoilCoarse methodeSoilCoarse = this.getByNumberId(numberIdString, errorsReport)
                        .orElseThrow(() -> new BusinessException("bad method"));
                this.methodeSoilCoarseDAO.remove(methodeSoilCoarse);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param allMethods
     * @return
     */
    @Override
    protected Set<MethodeSoilCoarse> filter(Set<AbstractMethode> allMethods) {
        Set<MethodeSoilCoarse> methodes = new HashSet();
        for (AbstractMethode methode : allMethods) {
            if (methode instanceof MethodeSoilCoarse) {
                methodes.add((MethodeSoilCoarse) methode);
            }
        }
        return methodes;
    }

    /**
     * @return
     */
    @Override
    protected List<MethodeSoilCoarse> getAllElements() {

        List<MethodeSoilCoarse> all = this.methodeSoilCoarseDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeDefinitionName() {
        return AbstractMethode.ATTRIBUTE_JPA_DEFINITION;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeEntityName() {
        return MethodeSoilCoarse.NAME_ENTITY_JPA;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeLibelleName() {
        return AbstractMethode.ATTRIBUTE_JPA_LIBELLE;
    }

    /**
     * @param methodeSoilCoarse
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final MethodeSoilCoarse methodeSoilCoarse) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = super
                .getNewLineModelGridMetadata(methodeSoilCoarse);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeSoilCoarse != null ? methodeSoilCoarse.getReference_donesol()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeSoilCoarse != null ? methodeSoilCoarse.getNorme()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<MethodeSoilCoarse> initModelGridMetadata() {
        this.initProperties();
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(
                        values, MethodeSoilCoarse.NAME_ENTITY_JPA);
                final Long numberId = this.getNumberId(errorsReport, lineNumber, tokenizerValues);
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String definition = this.getDefinition(errorsReport, lineNumber,
                        tokenizerValues);
                final String reference_donesol = tokenizerValues.nextToken();
                final String norme = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMethodeSoilCoarse(errorsReport, numberId, libelle,
                            definition, reference_donesol, norme);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMethodeSoilCoarse(
            AbstractCSVMetadataRecorder.ErrorsReport errorsReport, Long numberId,
            String libelle, String definition, String reference_donesol, String norme) {

        MethodeSoilCoarse methodeSoilCoarseDb = this.methodeSoilCoarseDAO.getByNKey(numberId).orElse(null);
        if (methodeSoilCoarseDb == null) {

            methodeSoilCoarseDb = new MethodeSoilCoarse(definition, libelle, numberId, reference_donesol, norme);

        } else {

            methodeSoilCoarseDb.update(definition, reference_donesol, norme);
        }
        try {

            this.methodeSoilCoarseDAO.saveOrUpdate(methodeSoilCoarseDb);

        } catch (PersistenceException e) {

            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);

        }

    }

    /**
     * @param methodeSoilCoarseDAO
     */
    public void setMethodeSoilCoarseDAO(IMethodeSoilCoarseDAO methodeSoilCoarseDAO) {
        this.methodeSoilCoarseDAO = methodeSoilCoarseDAO;
    }

}
