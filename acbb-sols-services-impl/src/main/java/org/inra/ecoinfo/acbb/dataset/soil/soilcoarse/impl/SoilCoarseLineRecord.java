package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class SoilCoarseLineRecord extends AbstractSoilLineRecord<SoilCoarseLineRecord, VariableValueSoilCoarse> {

    Methodesample methodeObtention;

    /**
     * @param originalLineNumber
     */
    public SoilCoarseLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }

    /**
     * @return
     */
    public Methodesample getMethodeObtention() {
        return methodeObtention;
    }

    void setMethodeObtention(Methodesample methodeObtention) {
        this.setMethodeSample(methodeObtention);
    }
}
