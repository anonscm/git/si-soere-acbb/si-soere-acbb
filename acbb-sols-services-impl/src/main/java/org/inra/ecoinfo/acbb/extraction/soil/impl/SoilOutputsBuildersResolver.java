/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipOutputStream;

/**
 * The Class MPSOutputsBuildersResolver.
 */
@SuppressWarnings({})
public abstract class SoilOutputsBuildersResolver extends AbstractOutputBuilder implements
        IOutputsBuildersResolver {

    /**
     * The Constant PATTERN_HEADER @link(String).
     */
    static final String PATTERN_HEADER = "%s;%s;%s(%s)";

    /**
     * The Constant EXTRACTION @link(String).
     */
    static final String EXTRACTION = "extraction";

    /**
     * The Constant SEPARATOR_TEXT.
     */
    static final String SEPARATOR_TEXT = "_";

    /**
     * The Constant FILENAME_REMINDER.
     */
    static final String FILENAME_REMINDER = "recapitulatif_extraction";

    /**
     * The Constant EXTENSION_CSV.
     */
    static final String EXTENSION_CSV = ".csv";

    /**
     * The Constant EXTENSION_TXT.
     */
    static final String EXTENSION_TXT = ".txt";

    /**
     * The Constant MAP_INDEX_0.
     */
    static final String MAP_INDEX_0 = "0";

    /**
     * The Constant REQUEST_REMINDER @link(String).
     */
    static final String REQUEST_REMINDER = "RequestReminder";

    /**
     * The LOGGER.
     */
    static final Logger LOGGER = LoggerFactory.getLogger(SoilDatasetManager.class);
    /**
     * The configuration.
     */
    protected IFileCompConfiguration fileCompConfiguration;

    /**
     *
     */
    public SoilOutputsBuildersResolver() {
        super();
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     */
    public void buidOutputFile(final IParameter parameters) throws BusinessException {
        final Set<String> datatypeNames = new HashSet();
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames, SoilOutputsBuildersResolver.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     *                           .extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        NoExtractionResultException noDataToExtract = null;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            parameters.getParameters().put(SoilExtractor.CST_RESULTS, parameters.getResults());
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
        } catch (FileNotFoundException e1) {
            throw new BusinessException("FileNotFoundException", e1);
        } catch (IOException e) {
            throw new BusinessException("IOException", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }

    /**
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

    /**
     * Builds the output print stream map.
     *
     * @param filesMap
     * @return the map
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#
     *                           buildOutputPrintStreamMap(java.util.Map)
     * @link(Map<String,File>) the files map
     */
    @Override
    protected Map<String, PrintStream> buildOutputPrintStreamMap(final Map<String, File> filesMap)
            throws BusinessException {
        final Map<String, PrintStream> outputPrintStreamMap = new HashMap();
        try {
            for (final Entry<String, File> filenameEntry : filesMap.entrySet()) {
                outputPrintStreamMap.put(filenameEntry.getKey(),
                        new PrintStream(filesMap.get(filenameEntry.getKey()),
                                Utils.ENCODING_ISO_8859_15));
            }
        } catch (final FileNotFoundException | UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        return outputPrintStreamMap;
    }

    /**
     * Builds the outputs files.
     *
     * @param filesNames
     * @param suffix
     * @return the map @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildOutputsFiles
     * (java.util.Set, java.lang.String)
     * @link(Set<String>) the files names
     * @link(String) the suffix
     */
    @Override
    protected Map<String, File> buildOutputsFiles(final Set<String> filesNames, final String suffix) {
        final Map<String, File> filesMaps = new HashMap();
        final String extractionPath = this.configuration.getRepositoryURI()
                .concat(AbstractOutputBuilder.FILE_SEPARATOR)
                .concat(AbstractOutputBuilder.EXTRACTION)
                .concat(AbstractOutputBuilder.FILE_SEPARATOR);
        for (final String filename : filesNames) {
            final String fileRandomSuffix = Long.toString(Instant.now().toEpochMilli()).concat("-")
                    .concat(Double.toString(Math.random() * 1_000_000).substring(0, 6));
            final File resultFile = new File(extractionPath.concat(filename)
                    .concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(suffix)
                    .concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(fileRandomSuffix)
                    .concat(AbstractOutputBuilder.EXTENSION_CSV));
            filesMaps.put(filename, resultFile);
        }
        return filesMaps;
    }

    /**
     * Close streams.
     *
     * @param outputPrintStreamMap
     * @link(Map<String,PrintStream>) the output print stream map @see
     * org.inra.ecoinfo.extraction.impl .AbstractOutputBuilder#closeStreams(java
     * .util.Map)
     */
    @Override
    protected void closeStreams(final Map<String, PrintStream> outputPrintStreamMap) {
        for (final PrintStream printStream : outputPrintStreamMap.values()) {
            printStream.flush();
            printStream.close();
        }
    }

    /**
     * The Class BuildOutputHelper.
     */
    protected static class BuildOutputHelper {

        /**
         * The parameter.
         */
        protected SoilParameterVO parameter;
        /**
         * The print stream.
         */
        protected PrintStream printStream;

        /**
         * Instantiates a new builds the output helper.
         *
         * @param parameter   the parameter
         * @param printStream the print stream
         * @throws NoExtractionResultException the no extract data found
         *                                     exception
         */
        BuildOutputHelper(final SoilParameterVO parameter, final PrintStream printStream)
                throws NoExtractionResultException {
            super();
            this.parameter = parameter;
            this.printStream = printStream;
        }
    }
}
