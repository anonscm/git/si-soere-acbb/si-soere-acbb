package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry;

import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ContentChemistry;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface IContentChemistryDAO extends ISoilDao<ContentChemistry> {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    Optional<ContentChemistry> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date);
}
