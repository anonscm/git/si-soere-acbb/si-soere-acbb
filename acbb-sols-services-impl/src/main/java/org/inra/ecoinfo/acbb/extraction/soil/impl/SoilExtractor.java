/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.acbb.extraction.soil.ISoilExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.soils.ISoilExtractor;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * The Class SoilExtractor.
 */
public class SoilExtractor extends MO implements IExtractor {

    /**
     * The Constant MAP_INDEX_0.
     */
    public static final String MAP_INDEX_0 = "0";
    // extraction
    /**
     * The Constant CST_RESULTS.
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.soil.messages";

    /**
     * The Constant MSG_EXTRACTION_ABORTED @link(String).
     */
    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     * The Constant PROPERTY_MSG_BADS_RIGHTS @link(String).
     */
    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";
    /**
     * autre type de donnée soil
     */
    @SuppressWarnings("rawtypes")
    protected ISoilExtractionDAO soilExtractionDAO;
    ISoilExtractor contentChemistryExtractor;
    ISoilExtractor soilDensityExtractor;
    ISoilExtractor soilCoarseExtractor;
    ISoilExtractor soilStockExtractor;
    // TODO
    ISoilExtractor soilTextureExtractor;

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IExtractor#extract(org.inra.ecoinfo.extraction
     *                           .IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        statusBar.setProgress(0);
        int extractionResult = 3;
        extractionResult = tryExtract(parameters, contentChemistryExtractor, extractionResult);
        statusBar.setProgress(10);
        extractionResult = tryExtract(parameters, soilDensityExtractor, extractionResult);
        statusBar.setProgress(20);
        extractionResult = tryExtract(parameters, soilStockExtractor, extractionResult);
        statusBar.setProgress(30);
        extractionResult = tryExtract(parameters, soilCoarseExtractor, extractionResult);
        statusBar.setProgress(40);
        extractionResult = tryExtract(parameters, soilTextureExtractor, extractionResult);
        statusBar.setProgress(50);
        // TODO
        /**
         * autre type de données soil
         */
        if (extractionResult == 0) {
            final String message = String.format(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            this.sendNotification(String.format(this.localizationManager.getMessage(SoilExtractor.BUNDLE_SOURCE_PATH,
                    SoilExtractor.MSG_EXTRACTION_ABORTED)), Notification.ERROR,
                    message, (Utilisateur) this.policyManager.getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH, message));
        }
    }

    /**
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = -1L;
        size += contentChemistryExtractor.getExtractionSize(parameters);
        size += soilDensityExtractor.getExtractionSize(parameters);
        size += soilCoarseExtractor.getExtractionSize(parameters);
        size += soilStockExtractor.getExtractionSize(parameters);
        size += soilTextureExtractor.getExtractionSize(parameters);
        return size;
    }

    /**
     * @param soilExtractionDAO the soilExtractionDAO to set
     */
    public void setSoilExtractionDAO(ISoilExtractionDAO soilExtractionDAO) {
        this.soilExtractionDAO = soilExtractionDAO;
    }

    /**
     * @param contentChemistryExtractor
     */
    public void setContentChemistryExtractor(ISoilExtractor contentChemistryExtractor) {
        this.contentChemistryExtractor = contentChemistryExtractor;
    }

    /**
     * @param soilCoarseExtractor
     */
    public void setSoilCoarseExtractor(ISoilExtractor soilCoarseExtractor) {
        this.soilCoarseExtractor = soilCoarseExtractor;
    }

    /**
     * @param soilStockExtractor
     */
    public void setSoilStockExtractor(ISoilExtractor soilStockExtractor) {
        this.soilStockExtractor = soilStockExtractor;
    }

    /**
     * @param soilTextureExtractor
     */
    public void setSoilTextureExtractor(ISoilExtractor soilTextureExtractor) {
        this.soilTextureExtractor = soilTextureExtractor;
    }

    /**
     * @param soilDensityExtractor
     */
    public void setSoilDensityExtractor(ISoilExtractor soilDensityExtractor) {
        this.soilDensityExtractor = soilDensityExtractor;
    }

    /**
     * Sets the extraction.
     *
     * @param extraction the new extraction
     * @see org.inra.ecoinfo.extraction.IExtractor#setExtraction(org.inra.ecoinfo
     * .config.Extraction)
     */
    @Override
    public final void setExtraction(final Extraction extraction) {

    }

    private int tryExtract(final IParameter parameters, ISoilExtractor extractor, int extractionResult) throws BusinessException {
        final Collection variables = (Collection) parameters.getParameters().get(
                Variable.class.getSimpleName().toLowerCase().concat(extractor.getExtractCode()));
        try {
            if (!CollectionUtils.isEmpty(variables)) {
                extractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e instanceof NoExtractionResultException) {
                return --extractionResult;
            } else {
                throw e;
            }
        }
        return extractionResult;
    }

}
