package org.inra.ecoinfo.acbb.extraction.soil.soilcoarse.impl;

import org.inra.ecoinfo.acbb.dataset.ComparatorWithFirstStringThenAlpha;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilOutputBuilder;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.PrintStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class SoilCoarseBuildOutputDisplay
        extends
        AbstractSoilOutputBuilder<SoilCoarse, ValeurSoilCoarse, SoilCoarse> {

    /**
     *
     */
    public static final String SOIL_COARSES = "soil_coarses";
    /**
     *
     */
    protected static final String FIRST_VARIABLE = "mav";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static protected final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.soil.soilcoarse.messages";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public SoilCoarseBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        return super.buildOutput(parameters);
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return SoilCoarseBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedVariables
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedVariables,
            PrintStream rawDataPrintStream) {
        return new SoilCoarseOutputHelper(rawDataPrintStream, selectedVariables);
    }

    /**
     *
     */
    protected class SoilCoarseOutputHelper extends OutputHelper<SequenceValues> {

        /**
         *
         */
        protected static final String PATTERN_11_FIELD = ";%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_5_FIELD = ";%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_3_FIELD = ";%s;%s;%s";

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         */
        protected SoilCoarseOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurSoil
         */
        @Override
        protected void addVariablesColumns(ValeurSoilCoarse valeurSoil) {
            String line = String.format(SoilCoarseOutputHelper.PATTERN_5_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getStandardDeviation()),
                    valeurSoil.getMethode().getNumberId(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesColumnsForNullVariable() {
            this.rawDataPrintStream.print(SoilCoarseOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param isHUT
         */
        protected void addVariablesColumnsForNullVariable(boolean isHUT) {
            this.rawDataPrintStream.print(SoilCoarseOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            if (isHUT) {
                this.rawDataPrintStream.print(SoilCoarseOutputHelper.PATTERN_3_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            }
        }

        /**
         * @param SoilCoarse
         * @param requestMap
         */
        @Override
        protected void printLineSpecific(SoilCoarse SoilCoarse, Map<String, Object> requestMap) {
            AbstractMethode methodesample = SoilCoarse.getMethodesample();
            String line = String.format(";%s",
                    methodesample == null ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : methodesample.getNumberId());
            if (methodesample != null) {
                addMethode(methodesample, requestMap);
            }
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected SoilCoarse getMesure(ValeurSoilCoarse valeur) {
            return valeur.getCoarse();
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected SoilCoarse getSequence(ValeurSoilCoarse valeur) {
            return getMesure(valeur);
        }

        /**
         * @param value
         * @param requestMap
         */
        @Override
        protected void printValue(SequenceValues value, Map<String, Object> requestMap) {
            Collections.sort(this.variablesAffichage, new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            while (itVariablesAffichage.hasNext()) {
                itVariablesAffichage.next();
                for (ValeurSoilCoarse valeurSoilCoarse : value.getValues()) {
                    addVariablesColumns(valeurSoilCoarse);
                    this.addMethode(valeurSoilCoarse.getMethode(), requestMap);
                }
            }
        }

        @Override
        protected SequenceValues newValues(SoilCoarse sequence, SoilCoarse mesure, ValeurSoilCoarse valeur) {
            return new SequenceValues(sequence, valeur);
        }
    }
}
