/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class TextureMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "methode_sol_texture";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return TextureMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
