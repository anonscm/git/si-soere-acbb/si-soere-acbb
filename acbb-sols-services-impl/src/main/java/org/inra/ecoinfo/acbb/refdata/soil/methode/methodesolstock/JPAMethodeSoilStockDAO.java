/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolstock;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeSoilStockDAO extends AbstractJPAMethodeDAO<MethodeSoilStock> implements
        IMethodeSoilStockDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeSoilStock> getMethodClass() {
        return MethodeSoilStock.class;
    }
}
