/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

/**
 * @author tcherniatinsky
 */
public class AbstractVariableValueSoil extends ACBBVariableValue<IValeurQualitative> {

    int measureNumber;
    float standardDeviation;

    /**
     * @param dvu
     * @param value
     * @param methode
     * @param mediane
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     */
    public AbstractVariableValueSoil(DatatypeVariableUniteACBB dvu, String value, AbstractMethode methode, float mediane, int measureNumber, float standardDeviation, int qualityIndex) {
        super(dvu, value, methode, mediane, qualityIndex);
        this.standardDeviation = standardDeviation;
        this.measureNumber = measureNumber;
    }

    /**
     * @return
     */
    public int getMeasureNumber() {
        return this.measureNumber;
    }

    /**
     * @param measureNumber
     */
    public void setMeasureNumber(int measureNumber) {
        this.measureNumber = measureNumber;
    }

    /**
     * @return
     */
    public float getStandardDeviation() {
        return this.standardDeviation;
    }

    /**
     * @param standardDeviation
     */
    public void setStandardDeviation(float standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

}
