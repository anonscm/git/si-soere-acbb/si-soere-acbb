package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse;

import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface ISoilCoarseDAO extends ISoilDao<SoilCoarse> {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    Optional<SoilCoarse> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date);
}
