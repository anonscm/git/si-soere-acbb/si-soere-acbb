/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.soildensity.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractVariableValueSoil;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

/**
 * @author ptcherniati
 */
public class VariableValueSoilDensity extends AbstractVariableValueSoil {

    /**
     * @param dvu
     */
    public VariableValueSoilDensity(DatatypeVariableUniteACBB dvu) {
        super(dvu, null, null, -9999F, -9999, -9999F, -9999);
    }

}
