/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soildensity.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soildensity.ISoilDensityDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.JPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPASoilDensityDAO
        extends JPASoilDAO<SoilDensity>
        implements ISoilDensityDAO {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    @Override
    public Optional<SoilDensity> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date) {
        CriteriaQuery<SoilDensity> query = builder.createQuery(SoilDensity.class);
        Root<SoilDensity> SoilDensity = query.from(SoilDensity.class);
        Join<SoilDensity, SuiviParcelle> suiviParcelle = SoilDensity.join(SoilDensity_.suiviParcelle);
        query.where(
                builder.equal(SoilDensity.join(SoilDensity_.version), version),
                builder.equal(suiviParcelle.join(SuiviParcelle_.parcelle), suiviParcelle),
                builder.equal(SoilDensity.get(SoilDensity_.date), date)
        );
        query.select(SoilDensity);
        return getOptional(query);
    }
}
