/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soilstock.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock_;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationSoilStockDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurSoilStock> deleteValeurs = builder.createCriteriaDelete(ValeurSoilStock.class);
        Root<ValeurSoilStock> valeur = deleteValeurs.from(ValeurSoilStock.class);
        Subquery<SoilStock> subquery = deleteValeurs.subquery(SoilStock.class);
        Root<SoilStock> soilStock = subquery.from(SoilStock.class);
        subquery.select(soilStock)
                .where(builder.equal(soilStock.get(SoilStock_.version), version));
        deleteValeurs.where(valeur.get(ValeurSoilStock_.stock).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<SoilStock> deleteSequence = builder.createCriteriaDelete(SoilStock.class);
        Root<SoilStock> sequence = deleteSequence.from(SoilStock.class);
        deleteSequence.where(builder.equal(sequence.get(SoilStock_.version), version));
        delete(deleteSequence);
    }

}
