/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.acbb.extraction.soil.ISoilExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.soil.soiltexture.jpa.JPASoilTextureExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.soils.ISoilDatasetManager;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.*;

/**
 * The Class FluxMeteoDatasetManager.
 */
public class SoilDatasetManager extends DefaultDatasetManager implements
        ISoilDatasetManager {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Map<String, ISoilExtractionDAO> extractionsDAO = new HashMap();
    Map<String, String> variablePatterns = new HashMap();

    /**
     * The security context @link(IPolicyManager). IPolicyManager policyManager;
     */
    public SoilDatasetManager() {
        super();

    }

    /**
     * @param intervalsDate
     * @return
     */
    @Override
    public Collection<? extends TraitementProgramme> getAvailableTraitements(
            List<IntervalDate> intervalsDate) {
        final Set<TraitementProgramme> availablesTraitements = new HashSet();
        for (Map.Entry<String, ISoilExtractionDAO> entry : extractionsDAO.entrySet()) {
            ISoilExtractionDAO interventionDAO = entry.getValue();
            availablesTraitements.addAll(interventionDAO.getAvailablesTraitementsForPeriode(intervalsDate, policyManager.getCurrentUser()));
        }
        return availablesTraitements;
    }

    /**
     * @param treatment
     * @param variable
     * @param returnVariables
     * @return
     */
    protected boolean testAndAddVariable(final TraitementProgramme treatment,
                                         final VariableACBB variable, List<VariableACBB> returnVariables) {
        if (this.policyManager.isRoot()
                || variablePatterns.values().stream()
                .map(pattern -> String.format(pattern, treatment.getSite().getCode(), variable.getCode()))
                .anyMatch(expression -> this.policyManager.checkActivity(policyManager.getCurrentUser(), 0, Activities.extraction.name(), expression))) {
            if (!returnVariables.contains(variable)) {
                returnVariables.add(variable);
            }
            return true;
        }
        return false;
    }

    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByTreatmentAndDatesInterval(List<TraitementProgramme> linkedList, List<IntervalDate> intervals) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        for (Map.Entry<String, ISoilExtractionDAO> entry : extractionsDAO.entrySet()) {
            String datatype = entry.getKey();
            ISoilExtractionDAO interventionDAO = entry.getValue();
            variablesByDatatype.put(datatype, interventionDAO.getAvailableVariableByTraitement(linkedList, intervals, policyManager.getCurrentUser()));
        }
        return variablesByDatatype;

    }

    @Override
    public Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> getAvailableVariablesByTreatmentAndDatesIntervalByMethod(List<TraitementProgramme> linkedList, List<IntervalDate> intervals) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        for (Map.Entry<String, ISoilExtractionDAO> entry : extractionsDAO.entrySet()) {
            if (entry.getValue() instanceof JPASoilTextureExtractionDAO) {
                JPASoilTextureExtractionDAO interventionDAO = (JPASoilTextureExtractionDAO) entry.getValue();
                return interventionDAO.getAvailableVariableByTraitementAndMethod(linkedList, intervals, policyManager.getCurrentUser());
            }
        }
        return new HashMap();
    }

    /**
     * @param extractionsDAO
     */
    public void setExtractionsDAO(Map<String, ISoilExtractionDAO> extractionsDAO) {
        this.extractionsDAO = extractionsDAO;
    }

    /**
     * @param variablePatterns
     */
    public void setVariablePatterns(Map<String, String> variablePatterns) {
        this.variablePatterns = variablePatterns;
    }

}
