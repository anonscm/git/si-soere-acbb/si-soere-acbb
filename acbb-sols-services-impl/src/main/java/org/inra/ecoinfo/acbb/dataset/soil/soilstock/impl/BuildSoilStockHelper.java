package org.inra.ecoinfo.acbb.dataset.soil.soilstock.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.ISoilStockDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildSoilStockHelper extends AbstractBuildHelper<SoilStockLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = BuildSoilStockHelper.class.getPackage().getName() + ".messages";

    Map<Parcelle, Map<LocalDate, Map<Long, Map<Float, Map<Float, SoilStock>>>>> soilStock = new HashMap();
    ISoilStockDAO SoilStockDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursSoilStock(final SoilStockLineRecord line,
                             final SoilStock soilstock) {
        for (final VariableValueSoilStock variableValue : line.getVariablesValues()) {
            final DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
            RealNode realNodeElement = getRealNodeForSequenceAndVariable(dvu);
            ValeurSoilStock valeurSoilStock;
            valeurSoilStock = new ValeurSoilStock(
                    soilstock,
                    Float.parseFloat(variableValue.getValue()),
                    realNodeElement, variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(),
                    variableValue.getQualityClass(),
                    variableValue.getMethode());
            soilstock.getValeurs().add(valeurSoilStock);
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void build() throws PersistenceException {
        for (final SoilStockLineRecord line : this.lines) {
            final SoilStock SoilStock = this.getOrCreateSoilStock(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursSoilStock(line, SoilStock);
            this.SoilStockDAO.saveOrUpdate(SoilStock);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesSoil
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesSoil the request properties Soil
     */
    public void build(final VersionFile version, final List<SoilStockLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesSoil);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    SoilStock createNewSoilStock(final SoilStockLineRecord line) {
        SoilStock SoilStock = new SoilStock(
                this.version,
                line.getDate(),
                line.getRotationNumber(),
                line.getAnneeNumber(),
                line.getSuiviParcelle(),
                line.getCampaignName(),
                line.getLayerName(),
                line.getLayerSup(),
                line.getLayerInf(),
                line.getMethodeSample()
        );
        SoilStock.setObservation(line.getObservation());
        try {
            this.SoilStockDAO.saveOrUpdate(SoilStock);
        } catch (final PersistenceException e) {
            this.errorsReport.addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return SoilStock;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    SoilStock getOrCreateSoilStock(final SoilStockLineRecord line) {
        return soilStock
                .computeIfAbsent(line.getParcelle(), kp -> new HashMap<LocalDate, Map<Long, Map<Float, Map<Float, SoilStock>>>>())
                .computeIfAbsent(line.getDate(), kd -> new HashMap<Long, Map<Float, Map<Float, SoilStock>>>())
                .computeIfAbsent(line.getMethodeSample().getId(), km -> new HashMap<Float, Map<Float, SoilStock>>())
                .computeIfAbsent(line.getLayerInf(), ki -> new HashMap<Float, SoilStock>())
                .computeIfAbsent(line.getLayerSup(), ks -> createNewSoilStock(line));
    }

    /**
     * @param biomassProductionDAO the Biomass Production DAO to set
     */
    public void setSoilStockDAO(ISoilStockDAO biomassProductionDAO) {
        this.SoilStockDAO = biomassProductionDAO;
    }
}
