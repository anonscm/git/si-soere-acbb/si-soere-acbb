/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.soildensity.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.ValeurSoilDensity;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.ValeurSoilDensity_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.AbstractJPASoilDAO;
import org.inra.ecoinfo.acbb.synthesis.soildensity.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPASoilDensityExtractionDAO extends
        AbstractJPASoilDAO<SoilDensity, ValeurSoilDensity> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurSoilDensity> getValeurSoilClass() {
        return ValeurSoilDensity.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurSoilDensity, SoilDensity> getMesureAttribute() {
        return ValeurSoilDensity_.density;
    }

    /**
     * @return
     */
    @Override
    protected Class<SoilDensity> getSoilClass() {
        return SoilDensity.class;
    }

//    
}
