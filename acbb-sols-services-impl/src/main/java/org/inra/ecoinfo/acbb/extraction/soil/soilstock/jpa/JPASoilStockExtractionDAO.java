/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.soilstock.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.AbstractJPASoilDAO;
import org.inra.ecoinfo.acbb.synthesis.soilstock.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPASoilStockExtractionDAO extends
        AbstractJPASoilDAO<SoilStock, ValeurSoilStock> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurSoilStock> getValeurSoilClass() {
        return ValeurSoilStock.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurSoilStock, SoilStock> getMesureAttribute() {
        return ValeurSoilStock_.stock;
    }

    /**
     * @return
     */
    @Override
    protected Class<SoilStock> getSoilClass() {
        return SoilStock.class;
    }
}
