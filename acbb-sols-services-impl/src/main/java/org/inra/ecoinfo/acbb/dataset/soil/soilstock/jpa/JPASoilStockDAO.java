/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soilstock.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soilstock.ISoilStockDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.JPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPASoilStockDAO
        extends JPASoilDAO<SoilStock>
        implements ISoilStockDAO {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    @Override
    public Optional<SoilStock> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date) {
        CriteriaQuery<SoilStock> query = builder.createQuery(SoilStock.class);
        Root<SoilStock> SoilStock = query.from(SoilStock.class);
        Join<SoilStock, SuiviParcelle> suiviParcelle = SoilStock.join(SoilStock_.suiviParcelle);
        query.where(
                builder.equal(SoilStock.join(SoilStock_.version), version),
                builder.equal(suiviParcelle.join(SuiviParcelle_.parcelle), suiviParcelle),
                builder.equal(SoilStock.get(SoilStock_.date), date)
        );
        query.select(SoilStock);
        return getOptional(query);
    }
}
