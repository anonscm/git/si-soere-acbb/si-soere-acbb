package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolstock;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Class Recorder.
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractMethodeRecorder<MethodeSoilStock> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodesolstock.messages";

    private IMethodeSoilStockDAO methodeSoilStockDAO;

    /**
     * @param allMethods
     * @return
     */
    @Override
    protected Set<MethodeSoilStock> filter(Set<AbstractMethode> allMethods) {
        Set<MethodeSoilStock> methodes = new HashSet();
        for (AbstractMethode methode : allMethods) {
            if (methode instanceof MethodeSoilStock) {
                methodes.add((MethodeSoilStock) methode);
            }
        }
        return methodes;
    }

    /**
     * @return
     */
    @Override
    protected List<MethodeSoilStock> getAllElements() {

        List<MethodeSoilStock> all = this.methodeSoilStockDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeDefinitionName() {
        return AbstractMethode.ATTRIBUTE_JPA_DEFINITION;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeEntityName() {
        return MethodeSoilStock.NAME_ENTITY_JPA;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeLibelleName() {
        return AbstractMethode.ATTRIBUTE_JPA_LIBELLE;
    }

    /**
     * @param methodeSoilStock
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final MethodeSoilStock methodeSoilStock) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = super
                .getNewLineModelGridMetadata(methodeSoilStock);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeSoilStock != null ? methodeSoilStock.getFichierPDF()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<MethodeSoilStock> initModelGridMetadata() {
        this.initProperties();
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(
                        values, MethodeSoilStock.NAME_ENTITY_JPA);
                final Long numberId = this.getNumberId(errorsReport, lineNumber, tokenizerValues);
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String definition = this.getDefinition(errorsReport, lineNumber,
                        tokenizerValues);
                final String fichierPDF = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMethodesample(errorsReport, numberId, libelle,
                            definition, fichierPDF);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMethodesample(
            AbstractCSVMetadataRecorder.ErrorsReport errorsReport, Long numberId,
            String libelle, String definition, String fichierPDF) {

        MethodeSoilStock methodeSoilStockDb = this.methodeSoilStockDAO.getByNKey(numberId).orElse(null);
        if (methodeSoilStockDb == null) {

            methodeSoilStockDb = new MethodeSoilStock(definition, libelle, numberId, fichierPDF);

        } else {

            methodeSoilStockDb.update(definition, fichierPDF);
        }
        try {

            this.methodeSoilStockDAO.saveOrUpdate(methodeSoilStockDb);

        } catch (PersistenceException e) {

            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);

        }

    }

    /**
     * @param methodeSoilStockDAO
     */
    public void setMethodeSoilStockDAO(IMethodeSoilStockDAO methodeSoilStockDAO) {
        this.methodeSoilStockDAO = methodeSoilStockDAO;
    }

}
