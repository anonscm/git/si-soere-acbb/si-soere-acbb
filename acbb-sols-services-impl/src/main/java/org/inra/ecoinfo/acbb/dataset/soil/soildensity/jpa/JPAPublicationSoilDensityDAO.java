/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soildensity.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity_;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.ValeurSoilDensity;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.ValeurSoilDensity_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationSoilDensityDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurSoilDensity> deleteValeurs = builder.createCriteriaDelete(ValeurSoilDensity.class);
        Root<ValeurSoilDensity> valeur = deleteValeurs.from(ValeurSoilDensity.class);
        Subquery<SoilDensity> subquery = deleteValeurs.subquery(SoilDensity.class);
        Root<SoilDensity> soilDensity = subquery.from(SoilDensity.class);
        subquery.select(soilDensity)
                .where(builder.equal(soilDensity.get(SoilDensity_.version), version));
        deleteValeurs.where(valeur.get(ValeurSoilDensity_.density).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<SoilDensity> deleteSequence = builder.createCriteriaDelete(SoilDensity.class);
        Root<SoilDensity> sequence = deleteSequence.from(SoilDensity.class);
        deleteSequence.where(builder.equal(sequence.get(SoilDensity_.version), version));
        delete(deleteSequence);
    }

}
