/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolelementsgrossiers;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeSoilCoarseDAO extends IMethodeDAO<MethodeSoilCoarse> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeSoilCoarse> getAll();
}
