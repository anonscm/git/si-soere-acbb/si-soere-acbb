package org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.ComparatorWithFirstStringThenAlpha;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ContentChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.MesureChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ValeurChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl.ProcessRecordContentChemistry;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.soil.impl.SoilParameterVO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class ContentChemistryBuildOutputDisplay
        extends
        AbstractSoilOutputBuilder<ContentChemistry, ValeurChemistry, MesureChemistry> {

    /**
     *
     */
    public static final String SOIL_SAMPLES = "soil_samples";
    /**
     *
     */
    protected static final String FIRST_VARIABLE = "mav";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static protected final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.messages";
    private static final String sol_analyse = "sol_analyse";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public ContentChemistryBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        Optional<List<VariableACBB>> variableslist = Optional.ofNullable((List<VariableACBB>) parameters.getParameters().get(Variable.class.getSimpleName().toLowerCase().concat(sol_analyse)));
        variableslist.ifPresent(v -> testIfPresentSamples(v, parameters));
        variableslist.ifPresent(v -> testIfPresentHut(v, parameters));
        return super.buildOutput(parameters);
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return ContentChemistryBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedVariables
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedVariables,
            PrintStream rawDataPrintStream) {
        return new ContentChemistryOutputHelper(rawDataPrintStream, selectedVariables);
    }

    private void testIfPresentSamples(List<VariableACBB> variables, IParameter parameters) {
        variables.stream()
                .filter(v -> v.getAffichage().equals(SOIL_SAMPLES))
                .findFirst()
                .ifPresent((variable) -> {
                    ((SoilParameterVO) parameters).setShowSamples();
                    variables.remove(variable);
                });
    }

    private void testIfPresentHut(List<VariableACBB> variables, IParameter parameters) {
        variables.stream()
                .filter(v -> v.getAffichage().equals(ProcessRecordContentChemistry.CONSTANT_HUMIDITE_RESIDUELLE))
                .findFirst()
                .ifPresent((variable) -> {
                    ((SoilParameterVO) parameters).setShowHUT();
                    variables.remove(variable);
                });
    }

    /**
     *
     */
    protected class ContentChemistryOutputHelper extends OutputHelper<SequenceMesureValues> {

        /**
         *
         */
        protected static final String PATTERN_11_FIELD = ";%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_5_FIELD = ";%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_4_FIELD = ";%s;%s;%s;%s";

        /**
         *
         */
        protected static final String FIRST_VARIABLE = "mav";

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         */
        protected ContentChemistryOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurSoil
         */
        @Override
        protected void addVariablesColumns(ValeurChemistry valeurSoil) {
            String line = String.format(ContentChemistryOutputHelper.PATTERN_5_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getStandardDeviation()),
                    Optional.ofNullable(valeurSoil.getMethode()).map(m -> m.getNumberId().toString()).orElse(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param valeurSoil
         */
        protected void addHumidityColumns(ValeurChemistry valeurSoil) {
            String line = String.format(ContentChemistryOutputHelper.PATTERN_4_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getStandardDeviation()),
                    Optional.ofNullable(valeurSoil.getMethode()).map(m -> m.getNumberId().toString()).orElse(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE)
            );
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesColumnsForNullVariable() {
            this.rawDataPrintStream.print(ContentChemistryOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param isHUT
         */
        protected void addVariablesColumnsForNullVariable(boolean isHUT) {
            this.rawDataPrintStream.print(ContentChemistryOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            if (isHUT) {
                this.rawDataPrintStream.print(ContentChemistryOutputHelper.PATTERN_4_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            }
        }

        /**
         * @param contentChemistry
         * @param requestMap
         */
        @Override
        protected void printLineSpecific(ContentChemistry contentChemistry, Map<String, Object> requestMap) {
            final Optional<MesureChemistry> mesure = contentChemistry.getMesures().stream()
                    .filter(m -> m.getDatatypeVariableUnite().getVariable().getAffichage().equals(ContentChemistryBuildOutputDisplay.SOIL_SAMPLES))
                    .findFirst();
            ValeurChemistry valeur = mesure
                    .map(m -> m.getValeurs())
                    .filter(v -> !v.isEmpty())
                    .map(v -> v.get(0))
                    .orElse(null);
            String line = String.format(";%s;%s;%s",
                    valeur == null ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeur.getValeur()),
                    valeur == null || valeur.getMeasureNumber() == ACBBUtils.CST_INVALID_EMPTY_MEASURE ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : valeur.getMeasureNumber(),
                    valeur == null ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : Optional.ofNullable(valeur.getMethode()).map(m -> m.getNumberId().toString()).orElse(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            if (valeur != null) {
                addMethode(valeur.getMethode(), requestMap);
            }
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected MesureChemistry getMesure(ValeurChemistry valeur) {
            return valeur.getMesureChemistry();
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected ContentChemistry getSequence(ValeurChemistry valeur) {
            return getMesure(valeur).getSequence();
        }

        /**
         * @param sequence
         * @param mesure
         * @param valeur
         * @return
         */
        @Override
        protected SequenceMesureValues newValues(ContentChemistry sequence, MesureChemistry mesure, ValeurChemistry valeur) {
            return new SequenceMesureValues(sequence, mesure, valeur);
        }

        /**
         * @param value
         * @param requestMap
         */
        @Override
        protected void printValue(SequenceMesureValues value, Map<String, Object> requestMap) {
            Collections.sort(this.variablesAffichage, new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            while (itVariablesAffichage.hasNext()) {
                String variableAffichage = itVariablesAffichage.next();
                boolean isPrinted = false;
                for (Map.Entry<MesureChemistry, List<ValeurChemistry>> mesureEntry : value.getMesures().entrySet()) {
                    List<ValeurChemistry> valeurs = mesureEntry.getValue();
                    Map<Boolean, List<ValeurChemistry>> collect = valeurs.stream()
                            .collect(Collectors.partitioningBy(
                                    v -> variableAffichage.equals(((DatatypeVariableUniteACBB) v.getRealNode().getNodeable()).getVariable().getAffichage()))
                            );
                    if (!collect.get(true).isEmpty()) {
                        ValeurChemistry valeurSoil = collect.get(true).get(0);
                        addVariablesColumns(valeurSoil);
                        this.addMethode(valeurSoil.getMethode(), requestMap);
                        if ((boolean) requestMap.get(ProcessRecordContentChemistry.CONSTANT_HUMIDITE_RESIDUELLE)) {
                            if (!collect.get(false).isEmpty()) {
                                valeurSoil = collect.get(false).get(0);
                                addHumidityColumns(valeurSoil);
                                this.addMethode(valeurSoil.getMethode(), requestMap);
                            } else {
                                this.rawDataPrintStream.print(ContentChemistryOutputHelper.PATTERN_4_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
                            }
                        }
                        isPrinted = true;
                        break;
                    }
                }
                if (!isPrinted) {
                    addVariablesColumnsForNullVariable((boolean) requestMap.get(ProcessRecordContentChemistry.CONSTANT_HUMIDITE_RESIDUELLE));
                }
            }
        }
    }
}
