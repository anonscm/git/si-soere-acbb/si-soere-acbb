package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.ISoilExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.soils.ISoilExtractor;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.util.*;

/**
 * @param <V>
 * @author vkoyao
 */
public abstract class AbstractSoilExtractor<V extends ValeurSoil> extends AbstractExtractor implements
        ISoilExtractor {

    protected static final String CST_EXTRACT = "EXTRACT_";
    protected ISoilExtractionDAO<V> soilExtractionDAO;
    protected String resultCode = StringUtils.EMPTY;
    protected String extractCode = StringUtils.EMPTY;

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
            final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                    .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
            final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
            if (CollectionUtils.isEmpty(selectedTraitementProgrammes) || CollectionUtils.isEmpty(selectedVariables)) {
                return 0l;
            }
            return soilExtractionDAO.getSize(selectedTraitementProgrammes, selectedVariables, intervalsDates, null);
        } catch (DateTimeException ex) {
            return -1l;
        }

    }

    /**
     * @param parameters
     * @throws BusinessException
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void extract(final IParameter parameters) throws BusinessException {
        this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        if (((SoilParameterVO) parameters).getResults().get(this.getResultCode()) == null
                || ((SoilParameterVO) parameters).getResults().get(this.getResultCode())
                .isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(this.getResultCode(),
                    resultsDatasMap);
        } else {
            ((SoilParameterVO) parameters).getResults().get(this.getResultCode())
                    .put(this.getExtractCode(), resultsDatasMap.get(this.getExtractCode()));
        }
        ((SoilParameterVO) parameters)
                .getResults()
                .get(this.getResultCode())
                .put(SoilExtractor.MAP_INDEX_0, resultsDatasMap.get(this.getExtractCode()));
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
        final List mesures = this.soilExtractionDAO.extractSoilValues(
                selectedTraitementProgrammes, selectedVariables, intervalsDates, policyManager.getCurrentUser());
        if (mesures == null || mesures.isEmpty()) {
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(this.getExtractCode(), mesures);
        return extractedDatasMap;
    }

    /**
     * @return
     */
    @Override
    public String getExtractCode() {
        return this.extractCode;
    }

    /**
     * @param extractCode the extractCode to set
     */
    public void setExtractCode(String extractCode) {
        this.extractCode = extractCode;
        this.resultCode = new StringBuffer(AbstractSoilExtractor.CST_EXTRACT).append(
                extractCode).toString();
    }

    /**
     * @return
     */
    protected String getResultCode() {
        return this.resultCode;
    }

    /**
     * @param requestMetadatasMap
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     * (java.util.Map)
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }

    /**
     * @param soilExtractionDAO the SoilExtractionDAO to set
     */
    public void setSoilExtractionDAO(ISoilExtractionDAO<V> soilExtractionDAO) {
        this.soilExtractionDAO = soilExtractionDAO;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
    }

    /**
     * Sort selected variables.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort(
                (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(
                        this.getExtractCode()), new ArrayList()), new Comparator<Variable>() {

                    @Override
                    public int compare(final Variable o1, final Variable o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });
    }
}
