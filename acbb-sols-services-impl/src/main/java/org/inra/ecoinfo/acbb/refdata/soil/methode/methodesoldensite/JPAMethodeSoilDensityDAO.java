/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoldensite;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeSoilDensityDAO extends AbstractJPAMethodeDAO<MethodeSoilDensity> implements
        IMethodeSoilDensityDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeSoilDensity> getMethodClass() {
        return MethodeSoilDensity.class;
    }
}
