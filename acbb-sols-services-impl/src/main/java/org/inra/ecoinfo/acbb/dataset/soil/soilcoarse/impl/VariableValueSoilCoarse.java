/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractVariableValueSoil;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

/**
 * @author ptcherniati
 */
public class VariableValueSoilCoarse extends AbstractVariableValueSoil {

    /**
     * @param dvu
     */
    public VariableValueSoilCoarse(DatatypeVariableUniteACBB dvu) {
        super(dvu, null, null, -9999F, -9999, -9999F, -9999);
    }

}
