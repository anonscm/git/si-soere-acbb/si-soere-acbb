/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolstock.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class StockMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "methode_sol_stock_calcule";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return StockMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
