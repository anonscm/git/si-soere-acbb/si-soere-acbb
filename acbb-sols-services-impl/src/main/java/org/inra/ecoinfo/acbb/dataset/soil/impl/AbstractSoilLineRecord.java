package org.inra.ecoinfo.acbb.dataset.soil.impl;

import org.inra.ecoinfo.acbb.dataset.soil.ILineRecordSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class AbstractLineRecord.
 *
 * @param <T>
 * @param <U>
 */
public abstract class AbstractSoilLineRecord<T extends AbstractSoilLineRecord<T, U>, U extends AbstractVariableValueSoil> implements Comparable<T>, ILineRecordSoil<U> {

    Parcelle parcelle;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    SuiviParcelle suiviParcelle;

    /**
     * The observation @link(String).
     */
    String observation = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * The rotation number @link(int).
     */
    Integer rotationNumber = 0;

    /**
     * The annee number @link(int).
     */
    Integer anneeNumber = 0;
    String campaignName;
    String layerName;
    Float layerInf;
    Float layerSup;

    /**
     * The date @link(Date).
     */
    LocalDate date;

    /**
     * The original line number <long>.
     */
    long originalLineNumber;

    /**
     * The variables values @link(List<VariableValue>).
     */
    List<U> variablesValues = new LinkedList();
    AbstractMethode methodeSample;
    private VersionDeTraitement versionDeTraitement;
    private LocalDate dateDebuttraitementSurParcelle;
    private TraitementProgramme traitementProgramme;

    /**
     * @param originalLineNumber
     */
    public AbstractSoilLineRecord(long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    @Override
    public Float getLayerInf() {
        return layerInf;
    }

    /**
     * @param layerInf
     */
    public void setLayerInf(Float layerInf) {
        this.layerInf = layerInf;
    }

    @Override
    public Float getLayerSup() {
        return layerSup;
    }

    /**
     * @param layerSup
     */
    public void setLayerSup(Float layerSup) {
        this.layerSup = layerSup;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int
     * @link(AbstractLineRecord) the o
     * @link(AbstractLineRecord) the o
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final T o) {
        if (Float.floatToRawIntBits(this.originalLineNumber) == o.getOriginalLineNumber()) {
            return 0;
        }
        return this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
    }

    /**
     * @return
     */
    @Override
    public String getCampaignName() {
        return campaignName;
    }

    /**
     * @param campaignName
     */
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    /**
     * @return
     */
    @Override
    public String getLayerName() {
        return layerName;
    }

    /**
     * @param layerName
     */
    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    /**
     * Copy.
     *
     * @param line
     * @link(AbstractLineRecord) the line {@link AbstractLineRecord} the line
     */
    public void copy(final T line) {
        this.originalLineNumber = line.getOriginalLineNumber();
        this.observation = line.getObservation();
        this.rotationNumber = line.getRotationNumber();
        this.anneeNumber = line.getAnneeNumber();
        this.suiviParcelle = line.getSuiviParcelle();
        this.versionDeTraitement = line.getVersionDeTraitement();
        this.dateDebuttraitementSurParcelle = line.getDateDebuttraitementSurParcelle();
        this.variablesValues = line.getVariablesValues();
        this.date = line.getDate();
        this.traitementProgramme = line.getTraitementProgramme();
        this.parcelle = line.getParcelle();
        this.campaignName = line.getCampaignName();
        this.layerName = line.getLayerName();
        this.layerInf = line.getLayerInf();
        this.layerSup = line.getLayerSup();
        this.methodeSample = line.getMethodeSample();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final AbstractSoilLineRecord<T, U> other = (AbstractSoilLineRecord<T, U>) obj;
        return this.originalLineNumber == other.originalLineNumber;
    }

    /**
     * @return
     */
    @Override
    public Integer getAnneeNumber() {
        return this.anneeNumber;
    }

    /**
     * @param anneeNumber
     */
    public void setAnneeNumber(Integer anneeNumber) {
        this.anneeNumber = anneeNumber;
    }

    /**
     * @return
     */
    @Override
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return
     */
    @Override
    public LocalDate getDateDebuttraitementSurParcelle() {
        return this.dateDebuttraitementSurParcelle;
    }

    /**
     * @param dateDebuttraitementSurParcelle
     */
    public void setDateDebuttraitementSurParcelle(LocalDate dateDebuttraitementSurParcelle) {
        this.dateDebuttraitementSurParcelle = dateDebuttraitementSurParcelle;
    }

    /**
     * @return
     */
    @Override
    public String getObservation() {
        return this.observation;
    }

    /**
     * @param observation
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * @return
     */
    @Override
    public long getOriginalLineNumber() {
        return this.originalLineNumber;
    }

    /**
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * @return
     */
    @Override
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    void setParcelle(Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * @return
     */
    @Override
    public Integer getRotationNumber() {
        return this.rotationNumber;
    }

    /**
     * @param rotationNumber
     */
    public void setRotationNumber(Integer rotationNumber) {
        this.rotationNumber = rotationNumber;
    }

    /**
     * @return
     */
    @Override
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * @param suiviParcelle
     */
    public void setSuiviParcelle(SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * @return
     */
    @Override
    public TraitementProgramme getTraitementProgramme() {
        return this.traitementProgramme;
    }

    /**
     * @param traitementProgramme
     */
    public void setTraitementProgramme(TraitementProgramme traitementProgramme) {
        this.traitementProgramme = traitementProgramme;
    }

    /**
     * @return
     */
    public AbstractMethode getMethodeSample() {
        return methodeSample;
    }

    /**
     * @param methodeSample
     */
    public void setMethodeSample(AbstractMethode methodeSample) {
        this.methodeSample = methodeSample;
    }

    /**
     * @return
     */
    @Override
    public List<U> getVariablesValues() {
        return this.variablesValues;
    }

    /**
     * @param variablesValues
     */
    public void setVariablesValues(List<U> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     * @return
     */
    @Override
    public VersionDeTraitement getVersionDeTraitement() {
        return this.versionDeTraitement;
    }

    /**
     * @param versionDeTraitement
     */
    public void setVersionDeTraitement(VersionDeTraitement versionDeTraitement) {
        this.versionDeTraitement = versionDeTraitement;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.originalLineNumber ^ this.originalLineNumber >>> 32);
        return hash;
    }

}
