package org.inra.ecoinfo.acbb.extraction.soil.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil_;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil_;
import org.inra.ecoinfo.acbb.extraction.jpa.AbstractACBBExtractionJPA;
import org.inra.ecoinfo.acbb.extraction.soil.ISoilExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * @param <M>
 * @param <V>
 * @author ptcherniati
 */
public abstract class AbstractJPASoilDAO<M extends AbstractSoil, V extends ValeurSoil>
        extends AbstractACBBExtractionJPA<V>
        implements ISoilExtractionDAO<V> {


    String datatypeCode;

    @Override
    public long getSize(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervalsDates, IUser user) {
        CriteriaQuery<Long> criteria = getQuery(intervalsDates, user, selectedVariables, selectedTraitementProgrammes, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;
    }

    /**
     * @return
     */
    @Override
    public String getDatatypeCode() {
        return datatypeCode;
    }

    /**
     * @param datatypeCode
     */
    public void setDatatypeCode(String datatypeCode) {
        this.datatypeCode = datatypeCode;
    }

    /**
     * @param selectedTraitementProgrammes
     * @param selectedVariables
     * @param intervals
     * @param user
     * @return
     */
    @Override
    public List<V> extractSoilValues(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user) {
        CriteriaQuery<V> query = getQuery(intervals, user, selectedVariables, selectedTraitementProgrammes, false);
        return getResultList(query);
    }

    protected CriteriaQuery getQuery(List<IntervalDate> intervals, IUser user, List<Variable> selectedVariables, List<TraitementProgramme> selectedTraitementProgrammes, boolean isCount) throws DateTimeParseException {
        CriteriaQuery query;
        if (isCount) {
            query = builder.createQuery(Long.class);
        } else {
            query = builder.createQuery(getValeurSoilClass());
        }
        Root<V> v = query.from(getValeurSoilClass());
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<V, M> s = v.join(getMesureAttribute());
        Join<V, RealNode> rn = v.join(ValeurSoil_.realNode);
        Join<RealNode, Nodeable> dvuNodeable = rn.join(RealNode_.nodeable);
        List<Predicate> and = new LinkedList();
        List<Predicate> or = new LinkedList();
        Root<NodeDataSet> nds = null;
        for (IntervalDate intervalDate : intervals) {
            List<Predicate> andForDate = new LinkedList();
            LocalDate minDate, maxDate;
            minDate = intervalDate.getBeginDate().toLocalDate();
            maxDate = intervalDate.getEndDate().toLocalDate();
            andForDate.add(builder.between(s.get(AbstractSoil_.date), minDate, maxDate));
            if (!(isCount || user.getIsRoot())) {
                nds = nds == null ? query.from(NodeDataSet.class) : nds;
                andForDate.add(builder.equal(nds.join(NodeDataSet_.realNode), rn));
                addRestrictiveRequestOnRoles(user, query, andForDate, builder, nds, s.get(AbstractSoil_.date));
            }
            or.add(builder.and(andForDate.toArray(new Predicate[0])));
        }
        and.add(builder.or(or.toArray(new Predicate[0])));
        and.add(dvu.join(DatatypeVariableUniteACBB_.variable).in(selectedVariables));
        and.add(builder.equal(dvu.get(Nodeable_.code), dvuNodeable.get(Nodeable_.code)));
        and.add(s.join(AbstractSoil_.suiviParcelle).join(SuiviParcelle_.traitement).in(selectedTraitementProgrammes));
        query.where(builder.and(and.toArray(new Predicate[0])));
        query.distinct(true);
        if (isCount) {
            query.select(builder.countDistinct(v));
        } else {
            query.select(v);
        }
        return query;
    }

    /**
     * @return
     */
    abstract protected Class<V> getValeurSoilClass();

    /**
     * @return
     */
    abstract protected Class<M> getSoilClass();

    /**
     * @return
     */
    abstract protected SingularAttribute<V, M> getMesureAttribute();

    /**
     * @return
     */
    abstract protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass();

    @Override
    protected boolean isParcelleDatatype() {
        return false;
    }


}
