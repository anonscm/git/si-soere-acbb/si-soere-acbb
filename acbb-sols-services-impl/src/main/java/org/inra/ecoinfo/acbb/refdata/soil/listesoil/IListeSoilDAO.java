/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.listesoil;

import org.inra.ecoinfo.acbb.refdata.listesacbb.IListeACBBDAO;

import java.util.List;

/**
 * The Interface IListeItineraireDAO.
 */
public interface IListeSoilDAO extends IListeACBBDAO<ListeSoil> {

    /**
     * @return
     */
    List<ListeSoil> getValueColumns();

}
