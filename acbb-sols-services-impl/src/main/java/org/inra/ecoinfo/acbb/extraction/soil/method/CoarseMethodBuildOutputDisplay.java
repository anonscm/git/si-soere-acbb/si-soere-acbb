/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolelementsgrossiers.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class CoarseMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "methode_sol_elements_grossiers";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return CoarseMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
