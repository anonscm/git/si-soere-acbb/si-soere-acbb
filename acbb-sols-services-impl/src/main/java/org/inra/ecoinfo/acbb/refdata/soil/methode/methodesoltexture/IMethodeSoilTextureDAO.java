/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeSoilTextureDAO extends IMethodeDAO<MethodeSoilTexture> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeSoilTexture> getAll();
}
