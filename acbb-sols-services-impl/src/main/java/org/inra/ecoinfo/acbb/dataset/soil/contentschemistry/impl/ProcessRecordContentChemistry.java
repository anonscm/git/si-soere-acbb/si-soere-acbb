package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur.IMethodeAnalyseDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur.MethodeAnalyse;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * @author vivianne
 */
public class ProcessRecordContentChemistry extends AbstractProcessRecordSoil {

    /**
     *
     */
    static final public String BUNDLE_NAME = ProcessRecordContentChemistry.class.getPackage().getName() + ".messages";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_MASS_VEG_CLASS = "PROPERTY_MSG_BAD_MASS_VEG_CLASS";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_NUMBER_FOR_METHOD = "PROPERTY_MSG_BAD_NUMBER_FOR_METHOD";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_METHOD = "PROPERTY_MSG_BAD_METHOD";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;
    private static final int VARIABLE_GROUP_COLONNE_NUMBER = 8;

    private IMethodeAnalyseDAO methodeAnalyseDAO;

    /**
     * @param methodeAnalyseDAO
     */
    public void setMethodeAnalyseDAO(IMethodeAnalyseDAO methodeAnalyseDAO) {
        this.methodeAnalyseDAO = methodeAnalyseDAO;
    }

    /**
     * @param version
     * @param errorsReport
     * @param lines
     * @param requestPropertiesSoil
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    protected void buildNewLines(
            VersionFile version,
            @SuppressWarnings("rawtypes") List<? extends AbstractSoilLineRecord> lines,
            ErrorsReport errorsReport, IRequestPropertiesSoil requestPropertiesSoil)
            throws PersistenceException {
        final BuildContentChemistryHelper instance = ApplicationContextHolder.getContext()
                .getBean(BuildContentChemistryHelper.class);
        instance.build(version, (List<ContentChemistryLineRecord>) lines, errorsReport,
                requestPropertiesSoil);
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param fileEncoding
     * @param requestProperties
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile localVersionFile = versionFile;
        super.processRecord(parser, localVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesSoil) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            localVersionFile = this.versionFileDAO.merge(localVersionFile);
            final List<ContentChemistryLineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, intervalDate, (IRequestPropertiesSoil) requestProperties,
                    datasetDescriptor, errorsReport, lineCount, dbVariables, lines);
            if (errorsReport.hasErrors()) {
                AbstractProcessRecordSoil.LOGGER.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(localVersionFile, lines, errorsReport,
                        (IRequestPropertiesSoil) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            /**
             * e.printStackTrace();
             */
            throw new BusinessException(e);

        }
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param intervalDate
     * @param requestPropertiesSoil
     * @param datasetDescriptor
     * @param errorsReport
     * @param lineCount
     * @param dbVariables
     * @param lines
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{IRequestPropertiesACBB the request properties
     * @link{DatasetDescriptorACBB the dataset descriptor
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{java.util.List<VariableACBB> the db variables
     * @link{java.util.List<AILineRecord> the lines
     */
    protected long readLines(final CSVParser parser, IntervalDate intervalDate, final IRequestPropertiesSoil requestPropertiesSoil,
                             final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                             long lineCount, final List<DatatypeVariableUniteACBB> dbVariables,
                             final List<ContentChemistryLineRecord> lines) throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            int index = 0;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final ContentChemistryLineRecord lineRecord = new ContentChemistryLineRecord(
                    localLineCount);
            index = this.readGenericColumns(errorsReport, localLineCount, cleanerValues,
                    lineRecord, datasetDescriptor, requestPropertiesSoil);
            index = this.readDateAndSetVersionTraitement(intervalDate, lineRecord, errorsReport, localLineCount,
                    index, cleanerValues, datasetDescriptor, requestPropertiesSoil);
            index = this.readSample(intervalDate, lineRecord, errorsReport, localLineCount,
                    index, cleanerValues, datasetDescriptor, requestPropertiesSoil);
            while (index < values.length) {
                index = this.readVariableValueContentChemistry(lineRecord, errorsReport,
                        localLineCount, index, cleanerValues, datasetDescriptor, requestPropertiesSoil,
                        dbVariables);
            }
            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesSoil
     * @param dbVariables
     * @return
     */
    protected int readVariableValueContentChemistry(final ContentChemistryLineRecord lineRecord,
                                                    final ErrorsReport errorsReport, final long lineCount, int index,
                                                    final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                                                    final IRequestPropertiesSoil requestPropertiesSoil,
                                                    List<DatatypeVariableUniteACBB> dbVariables) {
        int localIndex = index;
        DatatypeVariableUniteACBB dvu = dbVariables.get(index);
        VariableValueContentChemistry variableValue = new VariableValueContentChemistry(dvu);
        localIndex = this.readValue(errorsReport, cleanerValues, variableValue, lineCount, localIndex);
        if (Strings.isNullOrEmpty(variableValue.getValue())) {
            return skipValue(cleanerValues, localIndex, VARIABLE_GROUP_COLONNE_NUMBER);
        }
        localIndex = this.readMeasureNumber(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readStandardDeviation(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readMethodAnalyse(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue, false);
        localIndex = this.readQualityIndex(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readResidualHumidity(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue, datasetDescriptorACBB);
        lineRecord.getVariablesValues().add(variableValue);
        return localIndex;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @param datasetDescriptorACBB
     * @return
     */
    protected int readResidualHumidity(ErrorsReport errorsReport, long lineCount, int index, CleanerValues cleanerValues, VariableValueContentChemistry variableValue, DatasetDescriptorACBB datasetDescriptorACBB) {
        int localIndex = index;
        localIndex = readResidualHumidityValue(errorsReport, lineCount, localIndex, cleanerValues, variableValue, datasetDescriptorACBB);
        if (variableValue.getResidualHumidityValue() == null || ACBBUtils.CST_INVALID_BAD_MEASURE >= variableValue.getResidualHumidityValue()) {
            return skipValue(cleanerValues, localIndex, 3);
        }
        localIndex = readResidualHumidityNumber(errorsReport, lineCount, localIndex, cleanerValues, variableValue, datasetDescriptorACBB);
        localIndex = readResidualHumidityStandardDeviation(errorsReport, lineCount, localIndex, cleanerValues, variableValue, datasetDescriptorACBB);
        localIndex = readMethodAnalyse(errorsReport, lineCount, localIndex, cleanerValues, variableValue, true);
        return localIndex;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @param datasetDescriptorACBB
     * @return
     */
    protected int readResidualHumidityValue(final ErrorsReport errorsReport, final long lineCount,
                                            int index, final CleanerValues cleanerValues,
                                            final VariableValueContentChemistry variableValue, DatasetDescriptorACBB datasetDescriptorACBB) {
        Float humidityValue = getFloat(errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB);
        if (ACBBUtils.CST_INVALID_BAD_MEASURE < humidityValue) {
            variableValue.setResidualHumidityValue(humidityValue);
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @param datasetDescriptorACBB
     * @return
     */
    protected int readResidualHumidityNumber(final ErrorsReport errorsReport, final long lineCount,
                                             int index, final CleanerValues cleanerValues,
                                             final VariableValueContentChemistry variableValue, DatasetDescriptorACBB datasetDescriptorACBB) {
        int residualHumidityNumber = getInt(errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB);
        variableValue.setResidualHumidityNumber(ACBBUtils.CST_INVALID_EMPTY_MEASURE == residualHumidityNumber ? 1 : residualHumidityNumber);
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @param datasetDescriptorACBB
     * @return
     */
    protected int readResidualHumidityStandardDeviation(final ErrorsReport errorsReport, final long lineCount,
                                                        int index, final CleanerValues cleanerValues,
                                                        final VariableValueContentChemistry variableValue, DatasetDescriptorACBB datasetDescriptorACBB) {
        Float residualHumidityStandardDeviation = getFloat(errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB);
        if (ACBBUtils.CST_INVALID_BAD_MEASURE < residualHumidityStandardDeviation) {
            variableValue.setResidualHumidityStandardDeviation(residualHumidityStandardDeviation);
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @param isResidualHumidityMethod
     * @return
     */
    protected int readMethodAnalyse(final ErrorsReport errorsReport, final long lineCount, int index, final CleanerValues cleanerValues, final VariableValueContentChemistry variableValue, boolean isResidualHumidityMethod) {
        String methodString = null;
        try {
            methodString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            Long methodNumber = Long.parseLong(methodString);
            MethodeAnalyse methodeAnalyse = this.methodeAnalyseDAO.getByNumberId(methodNumber).orElse(null);
            if (methodeAnalyse == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        ProcessRecordContentChemistry.BUNDLE_NAME,
                        ProcessRecordContentChemistry.PROPERTY_MSG_BAD_METHOD), lineCount,
                        index + 1, methodString));
                return index + 1;
            }
            if (isResidualHumidityMethod) {
                variableValue.setResidualHumidityMethod(methodeAnalyse);
            } else {
                variableValue.setMethode(methodeAnalyse);
            }
        } catch (NumberFormatException e) {
            if (isResidualHumidityMethod) {
                variableValue.setResidualHumidityMethod(null);
            } else {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        ProcessRecordContentChemistry.BUNDLE_NAME,
                        ProcessRecordContentChemistry.PROPERTY_MSG_BAD_NUMBER_FOR_METHOD), lineCount,
                        index + 1, methodString));
            }
        }
        return index + 1;
    }

    /**
     * @param intervalDate
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptor
     * @param requestPropertiesSoil
     * @return
     */
    protected int readSample(IntervalDate intervalDate, ContentChemistryLineRecord lineRecord, ErrorsReport errorsReport, long lineCount, int index, CleanerValues cleanerValues, DatasetDescriptorACBB datasetDescriptor, IRequestPropertiesSoil requestPropertiesSoil) {
        Optional<DatatypeVariableUniteACBB> dvu = datatypeVariableUniteACBBDAO.getByDatatypeAndVariable(
                datatypeName,
                (VariableACBB) variableDAO.getByAffichage("soil_samples").orElse(null));
        if (!dvu.isPresent()) {
            return -99999;
        }
        VariableValueContentChemistry variableValue = new VariableValueContentChemistry(dvu.get());
        index = readValue(errorsReport, cleanerValues, variableValue, lineCount, index);
        if (Strings.isNullOrEmpty(variableValue.getValue())) {
            variableValue.setValue(ACBBUtils.PROPERTY_CST_EMPTY_MEASURE);
            variableValue.setMeasureNumber(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
            index = skipValue(cleanerValues, index, 1);
        } else {
            index = readMeasureNumber(errorsReport, lineCount, index, cleanerValues, variableValue);
        }
        index = readMethodSample(errorsReport, lineCount, index, cleanerValues, variableValue, lineRecord);
        lineRecord.getVariablesValues().add(variableValue);
        return index;
    }

}
