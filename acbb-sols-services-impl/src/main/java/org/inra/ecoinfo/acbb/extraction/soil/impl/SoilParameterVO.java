/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl.ProcessRecordContentChemistry;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.impl.ContentChemistryBuildOutputDisplay;
import org.inra.ecoinfo.acbb.extraction.soils.ISoilParameter;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.utils.IntervalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * The Class BiomasseParameterVO.
 */
public class SoilParameterVO extends DefaultParameter implements ISoilParameter {

    /**
     * The Constant Biomasse_EXTRACTION_TYPE_CODE @link(String).
     */
    static final String PARAMETER_TYPE_SOIL = "sols";
    private static final Logger LOGGER = LoggerFactory.getLogger(SoilParameterVO.class
            .getName());
    /**
     * The commentaires @link(String).
     */
    String commentaires;

    /**
     * The affichage @link(int).
     */
    int affichage;

    /**
     * Instantiates a new Biomasse parameter vo.
     */
    public SoilParameterVO() {
        this.getParameters().put(AbstractMethode.class.getSimpleName(),
                new HashSet<AbstractMethode>());
    }

    /**
     * Instantiates a new Biomasse parameter vo.
     *
     * @param metadatasMap
     * @link(Map<String,Object>) the metadatas map
     */
    public SoilParameterVO(final Map<String, Object> metadatasMap) {
        this.setParameters(metadatasMap);
        this.setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    /**
     * Gets the affichage @link(int).
     *
     * @return the affichage @link(int)
     */
    @Override
    public int getAffichage() {
        return this.affichage;
    }

    /**
     * Sets the affichage @link(int).
     *
     * @param affichage the new affichage @link(int)
     */
    @Override
    public final void setAffichage(final int affichage) {
        this.affichage = affichage;
    }

    /**
     * Gets the commentaires @link(String).
     *
     * @return the commentaires @link(String)
     */
    @Override
    public String getCommentaire() {
        return this.commentaires;
    }

    /**
     * Sets the commentaires @link(String).
     *
     * @param commentaires the new commentaires @link(String)
     */
    @Override
    public final void setCommentaire(final String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     * @return
     */
    public DatesFormParamVO getDatesYearsContinuousFormParamVO() {
        return (DatesFormParamVO) this.getParameters().get(DatesFormParamVO.class.getSimpleName());
    }

    /**
     * @return @see
     * org.inra.ecoinfo.extraction.IParameter#getExtractionTypeCode()
     */
    @Override
    public String getExtractionTypeCode() {
        return SoilParameterVO.PARAMETER_TYPE_SOIL;
    }

    /**
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<IntervalDate> getIntervalsDates() {
        return (List<IntervalDate>) this.getParameters().get(IntervalDate.class.getSimpleName());
    }

    /**
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TraitementProgramme> getSelectedsTraitements() {
        return (List<TraitementProgramme>) this.getParameters().get(TraitementProgramme.class.getSimpleName());
    }

    /**
     * @param listTraitement
     */
    @Override
    public void setSelectedsTraitements(List<TraitementProgramme> listTraitement) {
        this.getParameters().put(TraitementProgramme.class.getSimpleName(), listTraitement);
    }

    /**
     * @return the selectedTraitementProgrammes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TraitementProgramme> getSelectedTraitementProgrammes() {
        return (List<TraitementProgramme>) this.getParameters().get(
                TraitementProgramme.class.getSimpleName());
    }

    /**
     * @param intervalDates
     */
    @Override
    public void setIntervalsDate(List<IntervalDate> intervalDates) {
        this.getParameters().put(IntervalDate.class.getSimpleName(), intervalDates);
    }

    /**
     *
     */
    public void setShowSamples() {
        getParameters().computeIfAbsent(ContentChemistryBuildOutputDisplay.SOIL_SAMPLES, k -> Boolean.TRUE);
    }

    /**
     *
     */
    public void setShowHUT() {
        getParameters().computeIfAbsent(ProcessRecordContentChemistry.CONSTANT_HUMIDITE_RESIDUELLE, k -> Boolean.TRUE);
    }
}
