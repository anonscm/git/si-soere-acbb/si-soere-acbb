/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesample.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class SampleMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "methode_sol_obtention_echantillon";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return SampleMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
