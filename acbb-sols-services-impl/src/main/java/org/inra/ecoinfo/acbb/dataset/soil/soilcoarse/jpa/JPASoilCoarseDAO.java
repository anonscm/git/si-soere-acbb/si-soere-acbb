/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.ISoilCoarseDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.JPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPASoilCoarseDAO
        extends JPASoilDAO<SoilCoarse>
        implements ISoilCoarseDAO {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    @Override
    public Optional<SoilCoarse> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date) {
        CriteriaQuery<SoilCoarse> query = builder.createQuery(SoilCoarse.class);
        Root<SoilCoarse> SoilCoarse = query.from(SoilCoarse.class);
        Join<SoilCoarse, SuiviParcelle> suiviParcelle = SoilCoarse.join(SoilCoarse_.suiviParcelle);
        query.where(
                builder.equal(SoilCoarse.join(SoilCoarse_.version), version),
                builder.equal(suiviParcelle.join(SuiviParcelle_.parcelle), suiviParcelle),
                builder.equal(SoilCoarse.get(SoilCoarse_.date), date)
        );
        query.select(SoilCoarse);
        return getOptional(query);
    }
}
