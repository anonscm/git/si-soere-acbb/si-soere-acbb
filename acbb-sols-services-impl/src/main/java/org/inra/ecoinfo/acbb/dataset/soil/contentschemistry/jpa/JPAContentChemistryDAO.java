/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.IContentChemistryDAO;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ContentChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ContentChemistry_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.JPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPAContentChemistryDAO
        extends JPASoilDAO<ContentChemistry>
        implements IContentChemistryDAO {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    @Override
    public Optional<ContentChemistry> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date) {
        CriteriaQuery<ContentChemistry> query = builder.createQuery(ContentChemistry.class);
        Root<ContentChemistry> contentChemistry = query.from(ContentChemistry.class);
        Join<ContentChemistry, SuiviParcelle> suiviParcelle = contentChemistry.join(ContentChemistry_.suiviParcelle);
        query.where(
                builder.equal(contentChemistry.join(ContentChemistry_.version), version),
                builder.equal(suiviParcelle.join(SuiviParcelle_.parcelle), suiviParcelle),
                builder.equal(contentChemistry.get(ContentChemistry_.date), date)
        );
        query.select(contentChemistry);
        return getOptional(query);
    }
}
