/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolstock;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeSoilStockDAO extends IMethodeDAO<MethodeSoilStock> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeSoilStock> getAll();
}
