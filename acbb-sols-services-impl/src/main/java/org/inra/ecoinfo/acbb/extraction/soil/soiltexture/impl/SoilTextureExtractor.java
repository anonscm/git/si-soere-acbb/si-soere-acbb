package org.inra.ecoinfo.acbb.extraction.soil.soiltexture.impl;

import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.ValeurSoilTexture;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilExtractor;
import org.inra.ecoinfo.acbb.extraction.soil.soiltexture.jpa.JPASoilTextureExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.util.*;

/**
 * @author ptcherniati
 */
public class SoilTextureExtractor extends AbstractSoilExtractor<ValeurSoilTexture> {

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //nothing to do
    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
            final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                    .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
            final TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods
                    = ((TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>) requestMetadatasMap.getOrDefault(MethodeSoilTexture.class.getSimpleName(), new TreeMap<Object, Object>()));
            if (selectedMethods.isEmpty() || selectedTraitementProgrammes.isEmpty()) {
                return 0L;
            }
            return ((JPASoilTextureExtractionDAO) this.soilExtractionDAO).getSize(selectedTraitementProgrammes, selectedMethods, intervalsDates, null);
        } catch (DateTimeException ex) {
            return -1l;
        }

    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
        final TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods
                = ((TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>) requestMetadatasMap.getOrDefault(MethodeSoilTexture.class.getSimpleName(), new TreeMap()));
        final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
        if (selectedMethods.isEmpty() || selectedTraitementProgrammes.isEmpty()) {
            return new HashMap<>();
        }
        final List mesures = ((JPASoilTextureExtractionDAO) this.soilExtractionDAO).extractSoilTextureValues(
                selectedTraitementProgrammes, selectedMethods, intervalsDates, policyManager.getCurrentUser());
        if (mesures == null || mesures.isEmpty()) {
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(this.getExtractCode(), mesures);
        return extractedDatasMap;
    }

}
