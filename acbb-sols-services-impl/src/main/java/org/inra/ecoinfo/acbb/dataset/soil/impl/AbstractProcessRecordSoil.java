package org.inra.ecoinfo.acbb.dataset.soil.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesample.IMethodeSampleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author ptcherniati
 */
public abstract class AbstractProcessRecordSoil extends
        AbstractProcessRecord<ListeSoil, IListeSoilDAO> {

    /**
     *
     */
    public static final String CONSTANT_HUMIDITE_RESIDUELLE = "Hum_Res";
    /**
     * The Constant LOGGER @link(Logger).
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractProcessRecordSoil.class);
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_NUMBER_FOR_METHOD = "PROPERTY_MSG_BAD_NUMBER_FOR_METHOD";
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_METHOD = "PROPERTY_MSG_BAD_METHOD";
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_STANDARD_DEVIATION = "PROPERTY_MSG_BAD_STANDARD_DEVIATION";
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_MEASURE_NUMBER = "PROPERTY_MSG_BAD_MEASURE_NUMBER";
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_QUALITY_INDEX = "PROPERTY_MSG_BAD_QUALITY_INDEX";
    static final String PROPERTY_MSG_MISSING_PLOT_TRACKING = "PROPERTY_MSG_MISSING_PLOT_TRACKING";
    static final String PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT = "PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT";
    static final String PROPERTY_MSG_IGNORING_TYPE_INTERVENTION = "PROPERTY_MSG_IGNORING_TYPE_INTERVENTION";
    static final String PROPERTY_MSG_BAD_NUM_OR_DATE = "PROPERTY_MSG_BAD_NUM_OR_DATE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM = "PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_MISSING_DATE = "PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_MISSING_DATE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_BAD_TYPE = "PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_BAD_TYPE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_DATE = "PROPERTY_MSG_BAD_INTERVENTION_FOR_DATE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_DATE = "PROPERTY_MSG_BAD_INTERVENTION_DATE";
    static final String PROPERTY_MSG_BAD_SOIL_MEDIANE = "PROPERTY_MSG_BAD_SOIL_MEDIANE";
    static final String PROPERTY_MSG_BAD_DATE_FOR_INTERVAL = "PROPERTY_MSG_BAD_DATE_FOR_INTERVAL";
    static final String BUNDLE_NAME = AbstractProcessRecordSoil.class.getPackage().getName() + ".messages";

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    protected ISoilDao soilDao;

    /**
     *
     */
    protected IListeSoilDAO listeSoilDAO;

    /**
     *
     */
    protected IMethodeSampleDAO methodeSampleDAO;

    /**
     * Instantiates a new abstract process record soil.
     */
    public AbstractProcessRecordSoil() {
        super();
    }

    /**
     * @param methodeSampleDAO
     */
    public void setMethodeSampleDAO(IMethodeSampleDAO methodeSampleDAO) {
        this.methodeSampleDAO = methodeSampleDAO;
    }

    /**
     * Builds the new line.
     *
     * @param version               the version {@link VersionFile}
     * @param lines                 the lines
     * @param errorsReport          the errors report {@link ErrorsReport}
     * @param requestPropertiesSoil
     * @throws PersistenceException
     * @link(IRequestPropertiesSoil) the request properties soil
     * @link(IRequestPropertiesSoil) the request properties soil
     */
    protected abstract void buildNewLines(VersionFile version,
                                          List<? extends AbstractSoilLineRecord> lines, ErrorsReport errorsReport,
                                          IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException;

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorACBB
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    @Override
    protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(final CSVParser parser,
                                                                                final Map<Integer, Column> columns, final DatasetDescriptorACBB datasetDescriptorACBB)
            throws PersistenceException, IOException {
        Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites = new HashMap();
        List<DatatypeVariableUniteACBB> dvus = new LinkedList();
        dbDatatypeVariableUnites = this.datatypeVariableUniteACBBDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode(this.datatypeName);
        dbDatatypeVariableUnites.entrySet().stream()
                .filter(e -> e.getKey().equals("surface_des_echantillons"))
                .findFirst().
                ifPresent((t) -> {
                    dvus.add(t.getValue());
                });
        for (final Entry<Integer, Column> colonneEntry : columns.entrySet()) {
            if (colonneEntry.getValue().isFlag()
                    && (colonneEntry.getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE)
                    || colonneEntry.getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE)
                    || colonneEntry
                    .getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE)
                    || colonneEntry
                    .getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE))) {
                VariableACBB variable = (VariableACBB) this.variableDAO
                        .getByAffichage(colonneEntry.getValue().getName().replaceAll("_pour_.*$|_[^_]*$", ""))
                        .orElseThrow(PersistenceException::new);
                dvus.add(dbDatatypeVariableUnites.get(variable == null ? null : variable.getCode()));
            } else {
                dvus.add(null);
            }
        }
        for (int i = 0; i < datasetDescriptorACBB.getEnTete(); i++) {
            parser.getLine();
        }
        return dvus;
    }

    /**
     * @return
     */
    public IListeSoilDAO getListeSoilDAO() {
        return this.listeSoilDAO;
    }

    /**
     * @param listeSoilDAO
     */
    public void setListeSoilDAO(IListeSoilDAO listeSoilDAO) {
        this.listeSoilDAO = listeSoilDAO;
    }

    /**
     * @param colonneEntry
     * @return
     * @throws PersistenceException
     */
    protected Optional<VariableACBB> getVariable(final Entry<Integer, Column> colonneEntry)
            throws PersistenceException {
        if (Strings.isNullOrEmpty(colonneEntry.getValue().getRefVariableName())) {
            return this.variableDAO
                    .getByAffichage(colonneEntry.getValue().getName()).map(v -> (VariableACBB) v);
        } else {
            return this.variableDAO.getByCode(Utils
                    .createCodeFromString(colonneEntry.getValue().getRefVariableName())).map(v -> (VariableACBB) v);
        }
    }

    /**
     * @param intervalDate
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesSoil
     * @return
     */
    protected int readDateAndSetVersionTraitement(final IntervalDate intervalDate, final AbstractSoilLineRecord lineRecord,
                                                  final ErrorsReport errorsReport, final long lineCount, int index,
                                                  final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                                                  final IRequestPropertiesSoil requestPropertiesSoil) {
        final LocalDate measureDate = this.getDate(errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB);
        if (!intervalDate.isInto(measureDate.atStartOfDay())) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(AbstractProcessRecordSoil.BUNDLE_NAME,
                    AbstractProcessRecordSoil.PROPERTY_MSG_BAD_DATE_FOR_INTERVAL),
                    lineCount,
                    index,
                    DateUtil.getUTCDateTextFromLocalDateTime(measureDate, DateUtil.DD_MM_YYYY),
                    intervalDate.getBeginDateToString(),
                    intervalDate.getEndDateToString()));
            return index + 1;
        }
        lineRecord.setDate(measureDate);
        if (lineRecord.getDate() != null) {
            this.updateVersionDeTraitement(lineRecord, errorsReport, lineCount, index,
                    datasetDescriptorACBB, requestPropertiesSoil);
        }
        return index + 1;
    }

    /**
     * Gets the generic columns.
     *
     * @param errorsReport          the errors report {@link ErrorsReport}
     * @param lineCount             the line count long
     * @param cleanerValues         the cleaner values {@link CleanerValues}
     * @param lineRecord            the line record {@link AbstractSoilLineRecord}
     * @param datasetDescriptorACBB
     * @param requestPropertiesSoil
     * @return the generic columns
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(IRequestPropertiesSoil) the request properties soil
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(IRequestPropertiesSoil) the request properties soil
     */
    protected int readGenericColumns(final ErrorsReport errorsReport, final long lineCount,
                                     final CleanerValues cleanerValues, final AbstractSoilLineRecord lineRecord,
                                     final DatasetDescriptorACBB datasetDescriptorACBB,
                                     final IRequestPropertiesSoil requestPropertiesSoil) {
        int index = 0;
        index = this.readParcelle(lineRecord, errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB, requestPropertiesSoil);
        if (lineRecord.getParcelle() == null) {
            return index + 7;
        }
        index = this.readObservation(errorsReport, cleanerValues, lineRecord, index);
        index = this.readRotationAnnee(cleanerValues, errorsReport, lineRecord, lineCount, index, datasetDescriptorACBB);
        index = this.readCampaign(cleanerValues, lineRecord, errorsReport, lineCount, index);
        index = this.readLayerInf(cleanerValues, lineRecord, datasetDescriptorACBB, errorsReport, lineCount, index);
        index = this.readLayerSup(cleanerValues, lineRecord, datasetDescriptorACBB, errorsReport, lineCount, index);
        index = this.readLayerName(cleanerValues, lineRecord, errorsReport, index);
        return index;
    }

    /**
     * Gets the observation.
     *
     * @param cleanerValues
     * @param line
     * @param index
     * @return the observation {@link CleanerValues} the cleaner values
     * @link(CleanerValues) the cleaner values
     */
    protected int readObservation(final ErrorsReport errorReport, CleanerValues cleanerValues, AbstractSoilLineRecord line,
                                  int index) {
        try {
            line.setObservation(cleanerValues.nextToken());
        } catch (EndOfCSVLine ex) {
            errorReport.addErrorMessage(ex.setMessage(line.originalLineNumber, index).getMessage());
        }
        return index + 1;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesSoil
     * @return
     */
    protected int readParcelle(final AbstractSoilLineRecord lineRecord,
                               final ErrorsReport errorsReport, final long lineCount, final int index,
                               final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                               final IRequestPropertiesSoil requestPropertiesSoil) {
        int returnIndex = index;
        Parcelle parcelle = this.readDbParcelle(errorsReport, lineCount, returnIndex++,
                cleanerValues, datasetDescriptorACBB, requestPropertiesSoil);
        lineRecord.setParcelle(parcelle);
        return returnIndex;
    }

    /**
     * @param cleanerValues
     * @param errorsReport
     * @param lineRecord
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @return
     */
    protected int readRotationAnnee(final CleanerValues cleanerValues,
                                    final ErrorsReport errorsReport, final AbstractSoilLineRecord lineRecord,
                                    final long lineCount, int index, final DatasetDescriptorACBB datasetDescriptorACBB) {
        int localIndex = index;
        lineRecord.setRotationNumber(this.getInt(errorsReport, lineCount, localIndex++,
                cleanerValues, datasetDescriptorACBB));
        lineRecord.setAnneeNumber(this.getInt(errorsReport, lineCount, localIndex++, cleanerValues,
                datasetDescriptorACBB));
        return localIndex;
    }

    /**
     * @param cleanerValues
     * @param lineRecord
     * @param index
     * @return
     */
    protected int readCampaign(final CleanerValues cleanerValues, final AbstractSoilLineRecord lineRecord, ErrorsReport errorsReport, long lineNumber, int index) {

        try {
            lineRecord.setCampaignName(cleanerValues.nextToken());
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineNumber, index).getMessage());
        }
        return index + 1;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @param requestPropertiesSoil
     */
    protected void updateVersionDeTraitement(AbstractSoilLineRecord lineRecord,
                                             ErrorsReport errorsReport, long lineCount, int index,
                                             DatasetDescriptorACBB datasetDescriptorACBB,
                                             IRequestPropertiesSoil requestPropertiesSoil) {
        SuiviParcelle suiviParcelle;
        LocalDate localeDate = lineRecord.getDate();
        suiviParcelle = this.suiviParcelleDAO.retrieveSuiviParcelle(lineRecord.getParcelle(), localeDate).orElse(null);
        if (suiviParcelle == null) {
            String localizedParcelleName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getName(), lineRecord.getParcelle().getName());
            String localizedSiteName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getSite().getName(),
                    lineRecord.getParcelle().getSite().getName());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(AbstractProcessRecordSoil.BUNDLE_NAME,
                    AbstractProcessRecordSoil.PROPERTY_MSG_MISSING_PLOT_TRACKING),
                    DateUtil.getUTCDateTextFromLocalDateTime(localeDate, DateUtil.DD_MM_YYYY),
                    localizedParcelleName,
                    localizedSiteName));
            return;
        }
        lineRecord.setSuiviParcelle(suiviParcelle);
        lineRecord.setTraitementProgramme(suiviParcelle.getTraitement());
        VersionDeTraitement versionDeTraitement = this.versionDeTraitementDAO
                .retrieveVersiontraitement(lineRecord.getTraitementProgramme(), localeDate).orElse(null);
        if (versionDeTraitement == null) {
            String localizedTreatmentAffichage = this.localizationManager.newProperties(
                    TraitementProgramme.NAME_ENTITY_JPA,
                    TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE).getProperty(
                    lineRecord.getTraitementProgramme().getAffichage(),
                    lineRecord.getTraitementProgramme().getAffichage());
            String localizedSiteName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getSite().getName(),
                    lineRecord.getParcelle().getSite().getName());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(AbstractProcessRecordSoil.BUNDLE_NAME,
                    AbstractProcessRecordSoil.PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT),
                    DateUtil.getUTCDateTextFromLocalDateTime(localeDate, DateUtil.DD_MM_YYYY),
                    lineRecord.getTraitementProgramme().getNom(),
                    localizedTreatmentAffichage,
                    localizedSiteName));
            return;
        }
        lineRecord.setVersionDeTraitement(versionDeTraitement);
        lineRecord.setDateDebuttraitementSurParcelle(suiviParcelle.getDateDebutTraitement());
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readMediane(ErrorsReport errorsReport, long lineCount, int index, final CleanerValues cleanerValues, final ACBBVariableValue<IValeurQualitative> variableValue) {
        String valueString = null;
        try {
            valueString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(valueString)) {
            valueString = org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
        }
        Float value = null;
        try {
            value = Float.parseFloat(valueString);
            variableValue.setMediane(value);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(BUNDLE_NAME,
                    PROPERTY_MSG_BAD_SOIL_MEDIANE), lineCount, index + 1,
                    valueString));
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readMeasureNumber(final ErrorsReport errorsReport, final long lineCount,
                                    int index, final CleanerValues cleanerValues,
                                    final AbstractVariableValueSoil variableValue) {
        String measureNumberString = null;
        try {
            measureNumberString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(measureNumberString)) {
            measureNumberString = ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
        }
        try {
            int measureNumber = Integer.parseInt(measureNumberString);
            if (measureNumber < 0 && measureNumber != ACBBUtils.CST_INVALID_EMPTY_MEASURE) {
                throw new NumberFormatException("negative number");
            }
            variableValue.setMeasureNumber(measureNumber);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_MEASURE_NUMBER), lineCount,
                    index + 1, measureNumberString));
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readQualityIndex(final ErrorsReport errorsReport, final long lineCount,
                                   int index, final CleanerValues cleanerValues,
                                   final AbstractVariableValueSoil variableValue) {
        String qualityIndexString = null;
        try {
            qualityIndexString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            if (Strings.isNullOrEmpty(qualityIndexString)) {
                qualityIndexString = org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
            }
            int qualityClass = Integer.parseInt(qualityIndexString);
            if (qualityClass != org.inra.ecoinfo.acbb.utils.ACBBUtils.CST_INVALID_EMPTY_MEASURE && qualityClass < 0) {
                throw new NumberFormatException("negative number");
            }
            variableValue.setQualityClass(qualityClass);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_QUALITY_INDEX), lineCount,
                    index + 1, qualityIndexString));
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readStandardDeviation(final ErrorsReport errorsReport, final long lineCount,
                                        int index, final CleanerValues cleanerValues,
                                        final AbstractVariableValueSoil variableValue) {
        String standardDeviationString = null;
        try {
            standardDeviationString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(standardDeviationString)) {
            standardDeviationString = org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
        }
        try {
            float standardDeviation = Float.parseFloat(standardDeviationString);
            variableValue.setStandardDeviation(standardDeviation);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_STANDARD_DEVIATION), lineCount,
                    index + 1, standardDeviationString));
        }
        return index + 1;
    }

    /**
     * @param cleanerValues
     * @param variableValue
     * @param index
     * @return
     */
    protected int readValue(final ErrorsReport errorsReport, final CleanerValues cleanerValues,
                            final AbstractVariableValueSoil variableValue, long lineCount, int index) {
        String value = null;
        try {
            value = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        variableValue.setValue(value);
        return index + 1;
    }

    /**
     * @param cleanerValues
     * @param lineRecord
     * @param datasetDescriptorACBB
     * @param errorsReport
     * @param lineCount
     * @param index
     * @return
     */
    protected int readLayerInf(CleanerValues cleanerValues, AbstractSoilLineRecord lineRecord, DatasetDescriptorACBB datasetDescriptorACBB, ErrorsReport errorsReport, long lineCount, int index) {
        float layerInf = getFloat(errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB);
        lineRecord.setLayerInf(layerInf);
        return index + 1;
    }

    /**
     * @param cleanerValues
     * @param lineRecord
     * @param datasetDescriptorACBB
     * @param errorsReport
     * @param lineCount
     * @param index
     * @return
     */
    protected int readLayerSup(CleanerValues cleanerValues, AbstractSoilLineRecord lineRecord, DatasetDescriptorACBB datasetDescriptorACBB, ErrorsReport errorsReport, long lineCount, int index) {
        Float layerSup = getFloat(errorsReport, lineCount, index, cleanerValues, datasetDescriptorACBB);
        lineRecord.setLayerSup(layerSup);
        return index + 1;
    }

    /**
     * @param cleanerValues
     * @param lineRecord
     * @param index
     * @return
     */
    protected int readLayerName(CleanerValues cleanerValues, AbstractSoilLineRecord lineRecord, ErrorsReport errorsReport, int index) {

        try {
            lineRecord.setLayerName(cleanerValues.nextToken());
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineRecord.originalLineNumber, index).getMessage());
        }
        return index + 1;
    }

    /**
     * @param cleanerValues
     * @param index
     * @param nbColumsToSkip
     * @return
     */
    protected int skipValue(CleanerValues cleanerValues, int index, int nbColumsToSkip) {
        for (int j = 0; j < nbColumsToSkip; j++) {
            try {
                cleanerValues.nextToken();
            } catch (EndOfCSVLine e) {
                return index + j;
            }
        }
        return index + nbColumsToSkip;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @param line
     * @return
     */
    public int readMethodSample(final ErrorsReport errorsReport, final long lineCount, int index,
                                final CleanerValues cleanerValues, final AbstractVariableValueSoil variableValue, AbstractSoilLineRecord line) {
        String methodString = null;
        try {
            methodString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(line.originalLineNumber, index).getMessage());
        }
        if (Strings.isNullOrEmpty(methodString)) {
            variableValue.setMethode(null);
            return index + 1;
        }
        try {
            Long methodNumber = Long.parseLong(methodString);
            Methodesample methodesample = methodeSampleDAO.getByNumberId(methodNumber).orElse(null);
            if (methodesample == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        BUNDLE_NAME,
                        PROPERTY_MSG_BAD_METHOD), lineCount,
                        index + 1, methodString));
                return index + 1;
            }
            line.setMethodeSample(methodesample);
            variableValue.setMethode(methodesample);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_NUMBER_FOR_METHOD), lineCount,
                    index + 1, methodString));
        }
        return index + 1;
    }
}
