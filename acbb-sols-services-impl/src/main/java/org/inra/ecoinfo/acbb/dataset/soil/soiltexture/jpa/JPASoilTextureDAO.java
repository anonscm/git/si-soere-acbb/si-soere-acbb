/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.ISoilTextureDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.SoilTexture;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.SoilTexture_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.JPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPASoilTextureDAO
        extends JPASoilDAO<SoilTexture>
        implements ISoilTextureDAO {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    @Override
    public Optional<SoilTexture> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date) {
        CriteriaQuery<SoilTexture> query = builder.createQuery(SoilTexture.class);
        Root<SoilTexture> SoilTexture = query.from(SoilTexture.class);
        Join<SoilTexture, SuiviParcelle> suiviParcelle = SoilTexture.join(SoilTexture_.suiviParcelle);
        query.where(
                builder.equal(SoilTexture.join(SoilTexture_.version), version),
                builder.equal(suiviParcelle.join(SuiviParcelle_.parcelle), suiviParcelle),
                builder.equal(SoilTexture.get(SoilTexture_.date), date)
        );
        query.select(SoilTexture);
        return getOptional(query);
    }
}
