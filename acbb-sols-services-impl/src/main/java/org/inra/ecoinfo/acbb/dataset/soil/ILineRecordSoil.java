/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractVariableValueSoil;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import java.time.LocalDate;
import java.util.List;


/**
 * @param <U>
 * @author tcherniatinsky
 */
public interface ILineRecordSoil<U extends AbstractVariableValueSoil> {

    /**
     * @return
     */
    Integer getAnneeNumber();

    /**
     * @return
     */
    String getCampaignName();

    /**
     * @return
     */
    LocalDate getDate();

    /**
     * @return
     */
    LocalDate getDateDebuttraitementSurParcelle();

    /**
     * @return
     */
    Float getLayerInf();

    /**
     * @return
     */
    String getLayerName();

    /**
     * @return
     */
    Float getLayerSup();

    /**
     * @return
     */
    String getObservation();

    /**
     * @return
     */
    long getOriginalLineNumber();

    /**
     * @return
     */
    Parcelle getParcelle();

    /**
     * @return
     */
    Integer getRotationNumber();

    /**
     * @return
     */
    SuiviParcelle getSuiviParcelle();

    /**
     * @return
     */
    TraitementProgramme getTraitementProgramme();

    /**
     * @return
     */
    List<U> getVariablesValues();

    /**
     * @return
     */
    VersionDeTraitement getVersionDeTraitement();

}
