/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class TestHeadersSoil.
 *
 * @author Tcherniatinsky Philippe
 */
public class TestHeadersSoil extends GenericTestHeader {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(TestHeadersSoil.class
            .getName());
    private static final String BUNDLE_NAME = TestHeadersSoil.class.getPackage().getName() + ".messages";
    private static final String PROPERTY_MSG_MISSING_COLUMN = "PROPERTY_MSG_MISSING_COLUMN";
    private static final String PROPERTY_MSG_MISSING_VARIABLE = "PROPERTY_MSG_MISSING_VARIABLE";
    private static final String PROPERTY_MSG_BAD_COLUMN_NAME = "PROPERTY_MSG_BAD_COLUMN_NAME";
    private static final String PROPERTY_MSG_MANDATORY_EXTRA_COLUMN = "PROPERTY_MSG_MANDATORY_EXTRA_COLUMN";
    private static final String PROPERTY_MSG_MANDATORY_COLUMN = "PROPERTY_MSG_MANDATORY_COLUMN";
    /**
     * The datatype name @link(String).
     */
    String datatypeName;
    List<String> columnPatterns = null;
    IVariableACBBDAO variableDAO;
    String[] mandatoryVariableNames;

    /**
     * Instantiates a new test headers Soil.
     */
    public TestHeadersSoil() {
        super();
    }

    /**
     *
     * @param value
     * @param lineNumber
     * @param index
     * @param expectedColumn
     * @return
     */
    protected BusinessException errorBadColumnName(String value, long lineNumber, int index,
                                                   String expectedColumn) {
        final String message = String.format(RecorderACBB.getACBBMessageWithBundle(TestHeadersSoil.BUNDLE_NAME, TestHeadersSoil.PROPERTY_MSG_BAD_COLUMN_NAME),
                lineNumber, index + 1, value, expectedColumn);
        return new BusinessException(message);
    }

    /**
     *
     * @param pattern
     * @param variableACBB
     * @param lineNumber
     * @return
     */
    protected BusinessException errorMissingExtraColumns(String pattern, VariableACBB variableACBB,
                                                         long lineNumber) {
        final String columnName = this.getColumnName(pattern, variableACBB);
        String message = String.format(RecorderACBB.getACBBMessageWithBundle(TestHeadersSoil.BUNDLE_NAME, TestHeadersSoil.PROPERTY_MSG_MISSING_COLUMN),
                lineNumber, columnName);
        return new BusinessException(message);
    }

    /**
     *
     * @param lineNumber
     * @param index
     * @param mandatoryVariableIndex
     * @return
     */
    protected BusinessException errorMissingMandatoryExtraColumns(long lineNumber, int index,
                                                                  int mandatoryVariableIndex) {
        String message = String.format(RecorderACBB.getACBBMessageWithBundle(TestHeadersSoil.BUNDLE_NAME,
                TestHeadersSoil.PROPERTY_MSG_MANDATORY_EXTRA_COLUMN), lineNumber, index + 1,
                this.mandatoryVariableNames[mandatoryVariableIndex]);
        return new BusinessException(message);
    }

    /**
     *
     * @param lineNumber
     * @param index
     * @param columnName
     * @return
     */
    protected BusinessException errorMissingUndefinedColumns(final long lineNumber, int index,
                                                             String columnName) {
        final String message = String.format(RecorderACBB.getACBBMessageWithBundle(TestHeadersSoil.BUNDLE_NAME,
                TestHeadersSoil.PROPERTY_MSG_MANDATORY_COLUMN), lineNumber, index + 1,
                columnName);
        return new BusinessException(message);
    }

    /**
     *
     * @param variableName
     * @param lineNumber
     * @param index
     * @param columnName
     * @return
     */
    protected BusinessException errorMissingVariable(String variableName, long lineNumber,
                                                     int index, String columnName) {
        final String message = String.format(RecorderACBB.getACBBMessageWithBundle(TestHeadersSoil.BUNDLE_NAME,
                TestHeadersSoil.PROPERTY_MSG_MISSING_VARIABLE), lineNumber, index + 1,
                variableName, columnName);
        return new BusinessException(message);
    }

    /**
     *
     * @param pattern
     * @param variableACBB
     * @return
     */
    protected String getColumnName(String pattern, VariableACBB variableACBB) {
        String variableName = variableACBB == null ? "Mav" : variableACBB.getAffichage();
        return pattern.replaceAll(".*_\\((.*)\\)\\$", variableName + "_$1");
    }

    /**
     *
     * @param badsFormatsReport
     * @param lineNumber
     * @param requestPropertiesSoil
     * @param values
     * @param cleanerValues
     * @param columnToParse
     * @param datasetDescriptor
     * @return
     */
    protected int getColumns(final BadsFormatsReport badsFormatsReport, final long lineNumber,
                             final IRequestPropertiesSoil requestPropertiesSoil, String[] values,
                             final CleanerValues cleanerValues, final List<Column> columnToParse,
                             final DatasetDescriptor datasetDescriptor) {
        int index;
        String value;
        for (index = 0; index < values.length && index < datasetDescriptor.getUndefinedColumn(); index++) {
            value = null;
            try {
                value = cleanerValues.nextToken();
            } catch (EndOfCSVLine ex) {
                badsFormatsReport.addException(ex.setMessage(lineNumber, index));
            }
            Column columnTarget = null;
            for (final Column column : columnToParse) {
                if (Utils.createCodeFromString(column.getName()).equals(Utils.createCodeFromString(value))) {
                    columnTarget = column;
                    break;
                }
            }
            this.testColumnTarget(columnTarget, badsFormatsReport, lineNumber, value, index,
                    requestPropertiesSoil, columnToParse);
        }
        if (index < datasetDescriptor.getUndefinedColumn()) {
            badsFormatsReport.addException(this.errorMissingUndefinedColumns(lineNumber, index,
                    datasetDescriptor.getColumns().get(index).getName()));
        } else if (columnToParse.isEmpty() && this.columnPatterns != null) {
            try {
                index = this.testPatternColumn(badsFormatsReport, values, lineNumber, index,
                        requestPropertiesSoil, columnToParse);
                return index;
            } catch (BusinessException ex) {
                badsFormatsReport.addException(ex);
            }
        }
        return index;
    }

    /**
     *
     * @param variable
     * @param group
     * @param columnName
     * @return
     */
    protected Column getColumnTarget(VariableACBB variable, String group, String columnName) {
        Column column = new Column();
        column.setFieldName(columnName);
        column.setFlag(true);
        column.setInull(!"valeur".equals(group));
        column.setName(columnName);
        column.setRefVariableName(variable.getCode());
        column.setValueType(RecorderACBB.PROPERTY_CST_FLOAT_TYPE);
        column.setVariable(true);
        if (columnName.endsWith("IQ")) {
            column.setFlagType(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        } else {
            column.setFlagType(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);

        }
        return column;
    }

    /**
     *
     * @param group
     * @param index
     * @return
     */
    protected Optional<VariableACBB> getVariable(String group, int index) {
        return this.variableDAO.getByAffichage(group).map(v -> (VariableACBB) v);
    }

    /**
     * @param badsFormatsReport
     * @param parser
     * @param requestPropertiesSoil
     * @param lineNumber
     * @param datasetDescriptor
     * @return @throws java.io.IOException
     * @see
     * org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader#readLineHeader(org.inra.ecoinfo.utils.exceptions.BadsFormatsReport,
     * com.Ostermiller.util.CSVParser, long,
     * org.inra.ecoinfo.utils.DatasetDescriptor,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB)
     */
    @Override
    protected long readLineHeader(final BadsFormatsReport badsFormatsReport,
                                  final CSVParser parser, final long lineNumber,
                                  final DatasetDescriptor datasetDescriptor,
                                  final IRequestPropertiesACBB requestPropertiesSoil) throws IOException {
        String[] values;
        values = parser.getLine();
        final CleanerValues cleanerValues = new CleanerValues(values);
        final List<Column> columnToParse = new LinkedList(datasetDescriptor.getColumns());
        this.getColumns(badsFormatsReport, lineNumber,
                (IRequestPropertiesSoil) requestPropertiesSoil, values, cleanerValues,
                columnToParse, datasetDescriptor);
        return lineNumber + 1;
    }

    /**
     *
     * @param columnPatterns
     */
    public void setColumnPatterns(Map<String, String> columnPatterns) {
        this.columnPatterns = new LinkedList(columnPatterns.values());
    }

    /**
     * Sets the datatype name.
     *
     * @param datatypeName the new datatype name
     */
    public final void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     *
     * @param mandatoryVariableNames
     */
    public void setMandatoryVariableNames(String[] mandatoryVariableNames) {
        this.mandatoryVariableNames = mandatoryVariableNames;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableACBBDAO(IVariableACBBDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param columnTarget
     * @param badsFormatsReport
     * @param lineNumber
     * @param value
     * @param index
     * @param requestPropertiesSoil
     * @param columnToParse
     */
    protected void testColumnTarget(Column columnTarget, final BadsFormatsReport badsFormatsReport,
                                    final long lineNumber, String value, int index,
                                    final IRequestPropertiesSoil requestPropertiesSoil,
                                    final List<Column> columnToParse) {
        if (columnTarget == null) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_GENERIC_COLUMN_HEADER),
                    lineNumber, value, index + 1)));
        } else {
            requestPropertiesSoil.getValueColumns().put(index, columnTarget);
            columnToParse.remove(columnTarget);
        }
    }

    /**
     * Test headers.
     *
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile) the version file
     * @param requestProperties
     * @link(IRequestPropertiesACBB) the request properties
     * @param encoding
     * @link(String) the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @return the long
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.ITestHeaders#testHeaders(com.Ostermiller .util.CSVParser,
     *      org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *      org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *      org.inra.ecoinfo.utils.exceptions.BadsFormatsReport,
     *      org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final String encoding,
                            final BadsFormatsReport badsFormatsReport, final DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException {
        long lineNumber = 0;
        try {
            super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport,
                    datasetDescriptor);
            lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber,
                    requestProperties);
            lineNumber = this
                    .readDatatype(badsFormatsReport, parser, lineNumber, this.datatypeName);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, requestProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, requestProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 1);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, requestProperties);
            lineNumber = this.jumpLines(parser, lineNumber, 3);
        } catch (final IOException e) {
            this.getLogger().debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }

    /**
     *
     * @param it
     * @param values
     * @param lineNumber
     * @param index
     * @param variableACBB
     * @return
     * @throws BusinessException
     */
    protected String testNextColumn(Iterator<String> it, String[] values, long lineNumber,
                                    int index, VariableACBB variableACBB) throws BusinessException {
        if (it.hasNext() && index >= values.length) {
            throw this.errorMissingExtraColumns(it.next(), variableACBB, lineNumber);
        } else {
            return values[index].trim();
        }
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param index
     * @param requestPropertiesSoil
     * @param columnToParse
     * @return
     * @throws BusinessException
     */
    protected int testPatternColumn(BadsFormatsReport badsFormatsReports, String[] values, long lineNumber, int index,
                                    IRequestPropertiesSoil requestPropertiesSoil, List<Column> columnToParse)
            throws BusinessException {
        if (index >= values.length && this.mandatoryVariableNames != null
                && this.mandatoryVariableNames.length > 0) {
            throw this.errorMissingMandatoryExtraColumns(lineNumber, index, 0);
        }
        int localIndex = index;
        int mandatoryVariableIndex = 0;
        while (index < values.length) {
            Iterator<String> it = this.columnPatterns.iterator();
            boolean isVariableMandatory = this.mandatoryVariableNames != null
                    && mandatoryVariableIndex < this.mandatoryVariableNames.length;
            VariableACBB variable = isVariableMandatory ? this.getVariable(this.mandatoryVariableNames[mandatoryVariableIndex], index).orElse(null) : null;
            VariableACBB variableToCompare;
            for (localIndex = index; localIndex < index + this.columnPatterns.size(); localIndex++) {
                String value = this.testNextColumn(it, values, lineNumber, localIndex, variable);
                String columnPattern = it.next();
                Pattern pattern = Pattern.compile(columnPattern, Pattern.CASE_INSENSITIVE);
                Matcher matches = pattern.matcher(value);
                if (!matches.matches()) {
                    throw this.errorBadColumnName(value, lineNumber, localIndex,
                            this.getColumnName(columnPattern, variable));
                }
                String variableName = matches.group(1);
                String measureName = matches.group(2);
                if (AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE.equals(variableName)) {
                    variableName = matches.group(2);
                    measureName = matches.group(3);
                }
                variableToCompare = this.getVariable(variableName, localIndex).orElse(null);
                if (variableToCompare == null) {
                    badsFormatsReports.addException(this.errorMissingVariable(variableName, lineNumber, localIndex, value));
                    continue;
                }
                if (variable == null) {
                    variable = variableToCompare;
                }
                if (!variable.equals(variableToCompare)) {
                    badsFormatsReports.addException(this.errorBadColumnName(value, lineNumber, localIndex, value.replace(variableToCompare.getAffichage(), variable.getAffichage())));
                    continue;
                }
                requestPropertiesSoil.getValueColumns().put(localIndex, this.getColumnTarget(variable, measureName, value));
            }
            mandatoryVariableIndex++;
            index = localIndex;
            if (index + 1 >= values.length) {
                break;
            }
        }
        return index;
    }
}
