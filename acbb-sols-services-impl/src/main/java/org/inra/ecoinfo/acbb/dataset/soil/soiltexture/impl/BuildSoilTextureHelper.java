package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.ISoilTextureDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.MesureSoilTexture;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.SoilTexture;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.ValeurSoilTexture;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildSoilTextureHelper extends AbstractBuildHelper<SoilTextureLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = BuildSoilTextureHelper.class.getPackage().getName() + ".messages";

    Map<Parcelle, Map<LocalDate, Map<Long, Map<Float, Map<Float, SoilTexture>>>>> soilTextures = new HashMap();
    ISoilTextureDAO soilTextureDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursSoilTexture(final SoilTextureLineRecord line,
                               final SoilTexture soiltexture) {
        Map<AbstractMethode, List<VariableValueSoilTexture>> variableValuesByMethod = new HashMap<>();
        line.getVariablesValues().stream()
                .forEach(variablevalue -> variableValuesByMethod.computeIfAbsent(variablevalue.getMethode(), v -> new LinkedList<>()).add(variablevalue));
        for (Map.Entry<AbstractMethode, List<VariableValueSoilTexture>> entry : variableValuesByMethod.entrySet()) {
            MethodeSoilTexture methode = (MethodeSoilTexture) entry.getKey();
            List<VariableValueSoilTexture> value = entry.getValue();
            MesureSoilTexture mesure = soiltexture.getMesures().stream()
                    .filter(
                            m -> m.getMethode().getNumberId().equals(methode.getNumberId())
                    )
                    .filter(
                            m -> m.getSequence().equals(soiltexture)
                    )
                    .findFirst().orElse(new MesureSoilTexture(soiltexture, methode));
            soiltexture.getMesures().add(mesure);
            for (final VariableValueSoilTexture variableValue : line.getVariablesValues()) {
                final DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
                RealNode realNodeElement = getRealNodeForSequenceAndVariable(dvu);
                ValeurSoilTexture valeurSoilTexture;
                valeurSoilTexture = new ValeurSoilTexture(
                        mesure,
                        Float.parseFloat(variableValue.getValue()),
                        realNodeElement, variableValue.getMeasureNumber(),
                        variableValue.getStandardDeviation(),
                        variableValue.getQualityClass(),
                        variableValue.getMethode());
                mesure.getValeurs().add(valeurSoilTexture);
            }

        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void build() throws PersistenceException {
        for (final SoilTextureLineRecord line : this.lines) {
            final SoilTexture soilTexture = this.getOrCreateSoilTexture(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursSoilTexture(line, soilTexture);
            this.soilTextureDAO.saveOrUpdate(soilTexture);
        }

    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesSoil
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesSoil the request properties Soil
     */
    public void build(final VersionFile version, final List<SoilTextureLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesSoil);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    SoilTexture createNewSoilTexture(final SoilTextureLineRecord line) {
        SoilTexture SoilTexture = new SoilTexture(
                this.version,
                line.getDate(),
                line.getRotationNumber(),
                line.getAnneeNumber(),
                line.getSuiviParcelle(),
                line.getCampaignName(),
                line.getLayerName(),
                line.getLayerSup(),
                line.getLayerInf(),
                line.getMethodeSample()
        );
        SoilTexture.setObservation(line.getObservation());
        try {
            this.soilTextureDAO.saveOrUpdate(SoilTexture);
        } catch (final PersistenceException e) {
            this.errorsReport.addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return SoilTexture;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    SoilTexture getOrCreateSoilTexture(final SoilTextureLineRecord line) {
        return soilTextures
                .computeIfAbsent(line.getParcelle(), kp -> new HashMap<LocalDate, Map<Long, Map<Float, Map<Float, SoilTexture>>>>())
                .computeIfAbsent(line.getDate(), kd -> new HashMap<Long, Map<Float, Map<Float, SoilTexture>>>())
                .computeIfAbsent(line.getMethodeSample().getId(), km -> new HashMap<Float, Map<Float, SoilTexture>>())
                .computeIfAbsent(line.getLayerInf(), ki -> new HashMap<Float, SoilTexture>())
                .computeIfAbsent(line.getLayerSup(), ks -> createNewSoilTexture(line));
    }

    /**
     * @param biomassProductionDAO the Biomass Production DAO to set
     */
    public void setSoilTextureDAO(ISoilTextureDAO biomassProductionDAO) {
        this.soilTextureDAO = biomassProductionDAO;
    }
}
