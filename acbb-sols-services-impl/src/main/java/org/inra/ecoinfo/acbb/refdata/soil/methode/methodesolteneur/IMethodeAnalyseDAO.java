/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeAnalyseDAO extends IMethodeDAO<MethodeAnalyse> {
}
