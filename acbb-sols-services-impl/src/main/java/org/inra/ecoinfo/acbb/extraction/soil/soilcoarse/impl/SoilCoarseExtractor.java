package org.inra.ecoinfo.acbb.extraction.soil.soilcoarse.impl;

import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilExtractor;

/**
 * @author ptcherniati
 */
public class SoilCoarseExtractor extends AbstractSoilExtractor<ValeurSoilCoarse> {

}
