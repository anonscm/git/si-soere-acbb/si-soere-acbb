/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoldensite.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class DensityMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "methode_sol_densite";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return DensityMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
