/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.jpa;

import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationContentChemistryDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        List<Long> sequenceIds = getSequenceIds(version);
        if (sequenceIds.isEmpty()) {
            return;
        }
        List<Long> mesureIds = getMesureIds(sequenceIds);
        if (mesureIds.isEmpty()) {
            return;
        }
        deleteValeurs(mesureIds);
        deleteMesures(mesureIds);
        deleteSequences(sequenceIds);
    }

    /**
     * @param sequenceIds
     * @return
     */
    protected List<Long> getMesureIds(List<Long> sequenceIds) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<MesureChemistry> mesure = query.from(MesureChemistry.class);
        Join<MesureChemistry, ContentChemistry> seqM = mesure.join(MesureChemistry_.contentChemistry);
        query
                .select(mesure.get(MesureChemistry_.id))
                .where(seqM.get(ContentChemistry_.id).in(sequenceIds));
        List<Long> mesureIds = getResultList(query);
        return mesureIds;
    }

    /**
     * @param version
     * @return
     */
    protected List<Long> getSequenceIds(VersionFile version) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<ContentChemistry> sequence = query.from(ContentChemistry.class);
        Join<ContentChemistry, VersionFile> vers = sequence.join(ContentChemistry_.version);
        query
                .select(sequence.get(ContentChemistry_.id))
                .where(builder.equal(vers, version));
        List<Long> sequenceIds = getResultList(query);
        return sequenceIds;
    }

    private void deleteValeurs(List<Long> valeurIds) {
        if (CollectionUtils.isEmpty(valeurIds)) {
            return;
        }
        CriteriaDelete<ValeurChemistry> deleteValeurs = builder.createCriteriaDelete(ValeurChemistry.class);
        Root<ValeurChemistry> valeur = deleteValeurs.from(ValeurChemistry.class);
        deleteValeurs.where(valeur.get(ValeurChemistry_.mesureChemistry).get(MesureChemistry_.id).in(valeurIds));
        entityManager.createQuery(deleteValeurs).executeUpdate();
    }

    private void deleteMesures(List<Long> mesureIds) {
        if (CollectionUtils.isEmpty(mesureIds)) {
            return;
        }
        CriteriaDelete<MesureChemistry> deleteMesures = builder.createCriteriaDelete(MesureChemistry.class);
        Root<MesureChemistry> mesure = deleteMesures.from(MesureChemistry.class);
        deleteMesures.where(mesure.get(MesureChemistry_.id).in(mesureIds));
        entityManager.createQuery(deleteMesures).executeUpdate();
    }

    private void deleteSequences(List<Long> sequenceIds) {
        if (CollectionUtils.isEmpty(sequenceIds)) {
            return;
        }
        CriteriaDelete<ContentChemistry> deleteSequence = builder.createCriteriaDelete(ContentChemistry.class);
        Root<ContentChemistry> sequence = deleteSequence.from(ContentChemistry.class);
        deleteSequence.where(sequence.get(ContentChemistry_.id).in(sequenceIds));
        entityManager.createQuery(deleteSequence).executeUpdate();
    }

}
