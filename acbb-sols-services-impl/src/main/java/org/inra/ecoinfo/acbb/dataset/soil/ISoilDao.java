package org.inra.ecoinfo.acbb.dataset.soil;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;

/**
 * The Interface IInterventionDAO.
 *
 * @param <T> the generic type
 */
public interface ISoilDao<T extends AbstractSoil> extends IDAO<T> {

}
