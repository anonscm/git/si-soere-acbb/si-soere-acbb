/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoldensite;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeSoilDensityDAO extends IMethodeDAO<MethodeSoilDensity> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeSoilDensity> getAll();
}
