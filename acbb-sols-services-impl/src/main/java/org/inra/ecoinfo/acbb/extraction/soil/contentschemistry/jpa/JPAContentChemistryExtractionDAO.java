/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.*;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil_;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.AbstractJPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.synthesis.contentschemistry.SynthesisValue;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPAContentChemistryExtractionDAO extends
        AbstractJPASoilDAO<ContentChemistry, ValeurChemistry> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurChemistry> getValeurSoilClass() {
        return ValeurChemistry.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurChemistry, ContentChemistry> getMesureAttribute() {
        return null;//ValeurChemistry_.contentChemistry;
    }

    /**
     * @return
     */
    @Override
    protected Class<ContentChemistry> getSoilClass() {
        return ContentChemistry.class;
    }

    @Override
    protected CriteriaQuery getQuery(List<IntervalDate> intervals, IUser user, List<Variable> selectedVariables, List<TraitementProgramme> selectedTraitementProgrammes, boolean isCount) throws DateTimeParseException {
        CriteriaQuery query;
        if (isCount) {
            query = builder.createQuery(Long.class);
        } else {
            query = builder.createQuery(ValeurChemistry.class);
        }
        Root<ValeurChemistry> v = query.from(ValeurChemistry.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<ValeurChemistry, MesureChemistry> m = v.join(ValeurChemistry_.mesureChemistry);
        Join<MesureChemistry, ContentChemistry> s = m.join(MesureChemistry_.contentChemistry);
        Join<ValeurChemistry, RealNode> rn = v.join(ValeurSoil_.realNode);
        Join<RealNode, Nodeable> dvuNodeable = rn.join(RealNode_.nodeable);
        List<Predicate> and = new LinkedList();
        List<Predicate> or = new LinkedList();
        Root<NodeDataSet> nds = null;
        for (IntervalDate intervalDate : intervals) {
            List<Predicate> andForDate = new LinkedList();
            LocalDate minDate, maxDate;
            minDate = intervalDate.getBeginDate().toLocalDate();
            maxDate = intervalDate.getEndDate().toLocalDate();
            andForDate.add(builder.between(s.get(AbstractSoil_.date), minDate, maxDate));
            if (!(isCount || user.getIsRoot())) {
                nds = nds == null ? query.from(NodeDataSet.class) : nds;
                andForDate.add(builder.equal(nds.join(NodeDataSet_.realNode), rn));
                addRestrictiveRequestOnRoles(user, query, andForDate, builder, nds, s.get(AbstractSoil_.date));
            }
            or.add(builder.and(andForDate.toArray(new Predicate[0])));
        }
        and.add(builder.or(or.toArray(new Predicate[0])));
        and.add(dvu.join(DatatypeVariableUniteACBB_.variable).in(selectedVariables));
        and.add(builder.equal(dvu.get(Nodeable_.code), dvuNodeable.get(Nodeable_.code)));
        and.add(s.join(AbstractSoil_.suiviParcelle).join(SuiviParcelle_.traitement).in(selectedTraitementProgrammes));
        query.where(builder.and(and.toArray(new Predicate[0])));
        query.distinct(true);
        if (isCount) {
            query.select(builder.countDistinct(v));
        } else {
            query.select(v);
        }
        return query;
    }
}
