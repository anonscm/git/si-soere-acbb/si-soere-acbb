package org.inra.ecoinfo.acbb.dataset.soil.soilstock;

import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface ISoilStockDAO extends ISoilDao<SoilStock> {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    Optional<SoilStock> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date);
}
