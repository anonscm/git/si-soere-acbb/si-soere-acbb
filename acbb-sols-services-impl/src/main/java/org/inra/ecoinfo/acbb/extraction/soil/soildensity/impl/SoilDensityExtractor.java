package org.inra.ecoinfo.acbb.extraction.soil.soildensity.impl;

import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.ValeurSoilDensity;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilExtractor;

/**
 * @author ptcherniati
 */
public class SoilDensityExtractor extends AbstractSoilExtractor<ValeurSoilDensity> {

}
