/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolelementsgrossiers;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeSoilCoarseDAO extends AbstractJPAMethodeDAO<MethodeSoilCoarse> implements
        IMethodeSoilCoarseDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeSoilCoarse> getMethodClass() {
        return MethodeSoilCoarse.class;
    }
}
