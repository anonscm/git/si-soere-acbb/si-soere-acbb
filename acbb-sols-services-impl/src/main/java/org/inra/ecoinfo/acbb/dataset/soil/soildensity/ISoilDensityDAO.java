package org.inra.ecoinfo.acbb.dataset.soil.soildensity;

import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface ISoilDensityDAO extends ISoilDao<SoilDensity> {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    Optional<SoilDensity> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date);
}
