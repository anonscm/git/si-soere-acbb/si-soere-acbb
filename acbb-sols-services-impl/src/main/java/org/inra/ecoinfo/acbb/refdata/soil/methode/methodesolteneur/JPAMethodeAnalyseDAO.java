/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeAnalyseDAO extends AbstractJPAMethodeDAO<MethodeAnalyse> implements
        IMethodeAnalyseDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeAnalyse> getMethodClass() {
        return MethodeAnalyse.class;
    }

}
