/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.listesoil;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB_;
import org.inra.ecoinfo.utils.Utils;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAListeSoilDAO.
 */
public class JPAListeSoilDAO extends AbstractJPADAO<ListeSoil> implements IListeSoilDAO {

    static final String PATTERN_COUVERT = "([^0-9>]+)(>?)(\\d*)";

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeSoilDAO
     * #getAll()
     */
    @Override
    public List<ListeSoil> getAll() {
        return getAll(ListeSoil.class);
    }

    /**
     * Gets the by code.
     *
     * @param code
     * @return the by code @see
     * org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeSoilDAO
     * #getByCode(java.lang.String)
     * @link(String) the code
     */
    @Override
    public List<ListeSoil> getByName(final String code) {
        CriteriaQuery<ListeSoil> query = builder.createQuery(ListeSoil.class);
        Root<ListeSoil> li = query.from(ListeSoil.class);
        query.where(
                builder.equal(li.get(ListeACBB_.code), code)
        );
        query.select(li);
        return getResultList(query);
    }

    /**
     * Gets the by n key.
     *
     * @param nom
     * @param valeur
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeSoilDAO
     * #getByNKey(java.lang.String, java.lang.String)
     * @link(String) the code
     * @link(String) the valeur
     */
    @Override
    public Optional<ListeSoil> getByNKey(final String nom, final String valeur) {
        CriteriaQuery<ListeSoil> query = builder.createQuery(ListeSoil.class);
        Root<ListeSoil> li = query.from(ListeSoil.class);
        query.where(
                builder.equal(li.get(ListeACBB_.code), Utils.createCodeFromString(nom)),
                builder.equal(builder.lower(li.get(ListeACBB_.valeur)), Utils.createCodeFromString(valeur))
        );
        query.select(li);
        return getOptional(query);
    }

    @Override
    public List<ListeSoil> getValueColumns() {

        CriteriaQuery<ListeSoil> query = builder.createQuery(ListeSoil.class);
        Root<ListeSoil> lv = query.from(ListeSoil.class);
        query.where(
                builder.equal(lv.get(ListeACBB_.code), "soilcolumn")
        );
        query.select(lv);
        return getResultList(query);
    }
}
