package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.apache.commons.collections.ListUtils;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.IMethodeSoilTextureDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author vivianne
 */
public class ProcessRecordSoilTexture extends AbstractProcessRecordSoil {

    /**
     *
     */
    static final public String BUNDLE_NAME = ProcessRecordSoilTexture.class.getPackage().getName() + ".messages";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_NUMBER_FOR_METHOD = "PROPERTY_MSG_BAD_NUMBER_FOR_METHOD";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_METHOD = "PROPERTY_MSG_BAD_METHOD";
    static final public String PROPERTY_MISSING_VARIABLES_FOR_METHOD = "PROPERTY_MISSING_VARIABLES_FOR_METHOD";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    private IMethodeSoilTextureDAO methodeSoilTextureDAO;

    /**
     * @param methodeSoilTextureDAO
     */
    public void setMethodeSoilTextureDAO(IMethodeSoilTextureDAO methodeSoilTextureDAO) {
        this.methodeSoilTextureDAO = methodeSoilTextureDAO;
    }

    /**
     * @param version
     * @param errorsReport
     * @param lines
     * @param requestPropertiesSoil
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    protected void buildNewLines(
            VersionFile version,
            @SuppressWarnings("rawtypes") List<? extends AbstractSoilLineRecord> lines,
            ErrorsReport errorsReport, IRequestPropertiesSoil requestPropertiesSoil)
            throws PersistenceException {
        final BuildSoilTextureHelper instance = ApplicationContextHolder.getContext()
                .getBean(BuildSoilTextureHelper.class);
        instance.build(version, (List<SoilTextureLineRecord>) lines, errorsReport,
                requestPropertiesSoil);
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param fileEncoding
     * @param requestProperties
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile localVersionFile = versionFile;
        super.processRecord(parser, localVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesSoil) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            localVersionFile = this.versionFileDAO.merge(localVersionFile);
            final List<SoilTextureLineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, intervalDate, (IRequestPropertiesSoil) requestProperties,
                    datasetDescriptor, errorsReport, lineCount, dbVariables, lines);
            if (errorsReport.hasErrors()) {
                AbstractProcessRecordSoil.LOGGER.debug(errorsReport.getErrorsMessages());
                throw new BusinessException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(localVersionFile, lines, errorsReport,
                        (IRequestPropertiesSoil) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            /**
             * e.printStackTrace();
             */
            throw new BusinessException(e);

        }
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param intervalDate
     * @param requestPropertiesSoil
     * @param datasetDescriptor
     * @param errorsReport
     * @param lineCount
     * @param dbVariables
     * @param lines
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{IRequestPropertiesACBB the request properties
     * @link{DatasetDescriptorACBB the dataset descriptor
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{java.util.List<VariableACBB> the db variables
     * @link{java.util.List<AILineRecord> the lines
     */
    protected long readLines(final CSVParser parser, IntervalDate intervalDate, final IRequestPropertiesSoil requestPropertiesSoil,
                             final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                             long lineCount, final List<DatatypeVariableUniteACBB> dbVariables,
                             final List<SoilTextureLineRecord> lines) throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            int index = 0;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final SoilTextureLineRecord lineRecord = new SoilTextureLineRecord(
                    localLineCount);
            index = this.readGenericColumns(errorsReport, localLineCount, cleanerValues,
                    lineRecord, datasetDescriptor, requestPropertiesSoil);
            index = this.readDateAndSetVersionTraitement(intervalDate, lineRecord, errorsReport, localLineCount,
                    index, cleanerValues, datasetDescriptor, requestPropertiesSoil);
            index = this.readMethodSample(errorsReport, lineCount, index, cleanerValues, lineRecord);
            while (index < values.length) {
                index = this.readVariableValueSoilTexture(lineRecord, errorsReport,
                        localLineCount, index, cleanerValues, datasetDescriptor, requestPropertiesSoil,
                        dbVariables);
            }
            testVariablesValuesConformes(lineRecord, errorsReport, lineCount, datasetDescriptor, requestPropertiesSoil, dbVariables);
            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesSoil
     * @param dbVariables
     * @return
     */
    protected int readVariableValueSoilTexture(final SoilTextureLineRecord lineRecord,
                                               final ErrorsReport errorsReport, final long lineCount, int index,
                                               final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                                               final IRequestPropertiesSoil requestPropertiesSoil,
                                               List<DatatypeVariableUniteACBB> dbVariables) {
        int localIndex = index;
        DatatypeVariableUniteACBB dvu = dbVariables.get(index);
        VariableValueSoilTexture variableValue = new VariableValueSoilTexture(dvu);
        localIndex = this.readValue(errorsReport, cleanerValues, variableValue, lineCount, localIndex);
        if (Strings.isNullOrEmpty(variableValue.getValue())) {
            return skipValue(cleanerValues, localIndex, 7);
        }
        localIndex = this.readMeasureNumber(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readStandardDeviation(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readMethodSoilTexture(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readQualityIndex(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        lineRecord.getVariablesValues().add(variableValue);
        return localIndex;
    }

    protected void testVariablesValuesConformes(final SoilTextureLineRecord lineRecord,
                                                final ErrorsReport errorsReport, final long lineCount, final DatasetDescriptorACBB datasetDescriptorACBB, final IRequestPropertiesSoil requestPropertiesSoil,
                                                List<DatatypeVariableUniteACBB> dbVariables) {
        Map<AbstractMethode, List<VariableValueSoilTexture>> variablesValuesByMethod = new HashMap<>();
        lineRecord.getVariablesValues().stream()
                .forEach(variableValue -> variablesValuesByMethod.computeIfAbsent(variableValue.getMethode(), k -> new LinkedList()).add(variableValue));
        for (Map.Entry<AbstractMethode, List<VariableValueSoilTexture>> entry : variablesValuesByMethod.entrySet()) {
            MethodeSoilTexture method = (MethodeSoilTexture) entry.getKey();
            List<VariableValueSoilTexture> variablesValues = entry.getValue();
            if (method == null) {
                continue;
            }
            List<String> waitingNames = method.getComposante().stream()
                    .map(c -> c.getValeur())
                    .sorted()
                    .collect(Collectors.toList());
            List<String> registervariables = variablesValues.stream()
                    .map(variablevalue -> Utils.createCodeFromString(variablevalue.getDatatypeVariableUnite().getVariable().getAffichage()))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
            if (!ListUtils.isEqualList(waitingNames, registervariables)) {
                String expected = waitingNames.stream().collect(Collectors.joining(", "));
                Object missing = ListUtils.removeAll(waitingNames, registervariables).stream().collect(Collectors.joining(", "));
                String message = String.format(
                        localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MISSING_VARIABLES_FOR_METHOD),
                        lineCount + 1,
                        method.getNumberId(),
                        expected,
                        missing
                );
                errorsReport.addErrorMessage(message);
            }
        }
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readMethodSoilTexture(final ErrorsReport errorsReport, final long lineCount, int index,
                                        final CleanerValues cleanerValues, final VariableValueSoilTexture variableValue) {
        String methodString = null;
        try {
            methodString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            Long methodNumber = Long.parseLong(methodString);
            MethodeSoilTexture methodeSoilTexture = this.methodeSoilTextureDAO.getByNumberId(methodNumber).orElse(null);
            if (methodeSoilTexture == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        ProcessRecordSoilTexture.BUNDLE_NAME,
                        ProcessRecordSoilTexture.PROPERTY_MSG_BAD_METHOD), lineCount,
                        index + 1, methodString));
                return index + 1;
            }
            variableValue.setMethode(methodeSoilTexture);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordSoilTexture.BUNDLE_NAME,
                    ProcessRecordSoilTexture.PROPERTY_MSG_BAD_NUMBER_FOR_METHOD), lineCount,
                    index + 1, methodString));
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param lineRecord
     * @return
     */
    protected int readMethodSample(final ErrorsReport errorsReport, final long lineCount, int index,
                                   final CleanerValues cleanerValues, SoilTextureLineRecord lineRecord) {
        String methodString = null;
        try {
            methodString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            Long methodNumber = Long.parseLong(methodString);
            Methodesample methodeObtention = this.methodeSampleDAO.getByNumberId(methodNumber).orElse(null);
            if (methodeObtention == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        ProcessRecordSoilTexture.BUNDLE_NAME,
                        ProcessRecordSoilTexture.PROPERTY_MSG_BAD_METHOD), lineCount,
                        index + 1, methodString));
                return index + 1;
            }
            lineRecord.setMethodeObtention(methodeObtention);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordSoilTexture.BUNDLE_NAME,
                    ProcessRecordSoilTexture.PROPERTY_MSG_BAD_NUMBER_FOR_METHOD), lineCount,
                    index + 1, methodString));
        }
        return index + 1;
    }

}
