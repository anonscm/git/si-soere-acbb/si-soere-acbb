package org.inra.ecoinfo.acbb.extraction.soil.impl;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoldensite.MethodeSoilDensity;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolelementsgrossiers.MethodeSoilCoarse;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolstock.MethodeSoilStock;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur.MethodeAnalyse;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vkoyao
 * <p>
 * The Class FluxMeteoOutputDisplayByRow.
 */
public class SoilOutputDisplay extends SoilOutputsBuildersResolver {

    /**
     * The flux chambre build output by row @link(IOutputBuilder).
     */
    IOutputBuilder contentChemistryBuildOutput;
    IOutputBuilder soilDensityBuildOutput;
    IOutputBuilder soilStockBuildOutput;
    IOutputBuilder soilCoarseBuildOutput;
    IOutputBuilder soilTextureBuildOutput;
    IOutputBuilder soilRequestReminder;

    IOutputBuilder sampleMethodBuildOutput;
    IOutputBuilder contentChemistryMethodBuildOutput;
    IOutputBuilder soilDensityMethodBuildOutput;
    IOutputBuilder soilStockMethodBuildOutput;
    IOutputBuilder soilCoarseMethodBuildOutput;
    IOutputBuilder soilTextureMethodBuildOutput;

    // *add autre type de données biomasse*/
    // TODO

    /**
     * Instantiates a new flux meteo output display by row.
     */
    public SoilOutputDisplay() {
        super();
    }

    /**
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap,
                                          Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.
     *                           FluxMeteoOutputsBuildersResolver
     *                           #buildOutput(org.inra.ecoinfo.extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        statusBar.setProgress(50);
        this.contentChemistryBuildOutput.buildOutput(parameters);
        statusBar.setProgress(55);
        this.soilDensityBuildOutput.buildOutput(parameters);
        statusBar.setProgress(60);
        this.soilStockBuildOutput.buildOutput(parameters);
        statusBar.setProgress(65);
        this.soilCoarseBuildOutput.buildOutput(parameters);
        statusBar.setProgress(70);
        this.soilTextureBuildOutput.buildOutput(parameters);
        statusBar.setProgress(75);
        this.soilRequestReminder.buildOutput(parameters);
        statusBar.setProgress(78);
        buildOutputMethode(sampleMethodBuildOutput, parameters, Methodesample.class);
        statusBar.setProgress(81);
        buildOutputMethode(contentChemistryMethodBuildOutput, parameters, MethodeAnalyse.class);
        statusBar.setProgress(84);
        buildOutputMethode(soilDensityMethodBuildOutput, parameters, MethodeSoilDensity.class);
        statusBar.setProgress(87);
        buildOutputMethode(soilCoarseMethodBuildOutput, parameters, MethodeSoilCoarse.class);
        statusBar.setProgress(90);
        buildOutputMethode(soilStockMethodBuildOutput, parameters, MethodeSoilStock.class);
        statusBar.setProgress(95);
        buildOutputMethode(soilTextureMethodBuildOutput, parameters, MethodeSoilTexture.class);
        statusBar.setProgress(100);
        return super.buildOutput(parameters);
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * @param <T>
     * @param outputDisplay
     * @param parameters
     * @param clazz
     * @throws BusinessException
     */
    public <T extends AbstractMethode> void buildOutputMethode(final IOutputBuilder outputDisplay, final IParameter parameters, Class<T> clazz) throws BusinessException {
        if (((Set<AbstractMethode>) parameters.getParameters().get(AbstractMethode.class.getSimpleName())).stream()
                .anyMatch(m -> m.getClass().equals(clazz))) {
            outputDisplay.buildOutput(parameters);
        }
    }

    /**
     * @param metadatasMap
     * @return
     */
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        return null;
    }

    /**
     * @param contentChemistryBuildOutput
     */
    public void setContentChemistryBuildOutput(IOutputBuilder contentChemistryBuildOutput) {
        this.contentChemistryBuildOutput = contentChemistryBuildOutput;
    }

    /**
     * @param soilDensityBuildOutput
     */
    public void setSoilDensityBuildOutput(IOutputBuilder soilDensityBuildOutput) {
        this.soilDensityBuildOutput = soilDensityBuildOutput;
    }

    /**
     * @param soilStockBuildOutput
     */
    public void setSoilStockBuildOutput(IOutputBuilder soilStockBuildOutput) {
        this.soilStockBuildOutput = soilStockBuildOutput;
    }

    /**
     * @param soilCoarseBuildOutput
     */
    public void setSoilCoarseBuildOutput(IOutputBuilder soilCoarseBuildOutput) {
        this.soilCoarseBuildOutput = soilCoarseBuildOutput;
    }

    /**
     * @param soilTextureBuildOutput
     */
    public void setSoilTextureBuildOutput(IOutputBuilder soilTextureBuildOutput) {
        this.soilTextureBuildOutput = soilTextureBuildOutput;
    }

    /**
     * @param soilRequestReminder
     */
    public void setSoilRequestReminder(IOutputBuilder soilRequestReminder) {
        this.soilRequestReminder = soilRequestReminder;
    }

    /**
     * @param sampleMethodBuildOutput
     */
    public void setSampleMethodBuildOutput(IOutputBuilder sampleMethodBuildOutput) {
        this.sampleMethodBuildOutput = sampleMethodBuildOutput;
    }

    /**
     * @param contentChemistryMethodBuildOutput
     */
    public void setContentChemistryMethodBuildOutput(IOutputBuilder contentChemistryMethodBuildOutput) {
        this.contentChemistryMethodBuildOutput = contentChemistryMethodBuildOutput;
    }

    /**
     * @param soilDensityMethodBuildOutput
     */
    public void setSoilDensityMethodBuildOutput(IOutputBuilder soilDensityMethodBuildOutput) {
        this.soilDensityMethodBuildOutput = soilDensityMethodBuildOutput;
    }

    /**
     * @param soilStockMethodBuildOutput
     */
    public void setSoilStockMethodBuildOutput(IOutputBuilder soilStockMethodBuildOutput) {
        this.soilStockMethodBuildOutput = soilStockMethodBuildOutput;
    }

    /**
     * @param soilCoarseMethodBuildOutput
     */
    public void setSoilCoarseMethodBuildOutput(IOutputBuilder soilCoarseMethodBuildOutput) {
        this.soilCoarseMethodBuildOutput = soilCoarseMethodBuildOutput;
    }

    /**
     * @param soilTextureMethodBuildOutput
     */
    public void setSoilTextureMethodBuildOutput(IOutputBuilder soilTextureMethodBuildOutput) {
        this.soilTextureMethodBuildOutput = soilTextureMethodBuildOutput;
    }

}
