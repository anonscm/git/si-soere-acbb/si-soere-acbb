/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soildensity.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.soil.impl.RequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.IRequestPropertiesSoilDensity;

import java.io.Serializable;

/**
 * The Class RequestPropertiesAI.
 * <p>
 * record the general information of the file
 */
public class RequestPropertiesSoilDensity extends RequestPropertiesSoil implements
        IRequestPropertiesSoilDensity, Serializable {

    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new request properties ai.
     */
    public RequestPropertiesSoilDensity() {
        super();
    }

    /**
     * @return @see
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getTestDuplicates()
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateSoilDensity();
    }
}
