/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeSoilTextureDAO extends AbstractJPAMethodeDAO<MethodeSoilTexture> implements
        IMethodeSoilTextureDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeSoilTexture> getMethodClass() {
        return MethodeSoilTexture.class;
    }
}
