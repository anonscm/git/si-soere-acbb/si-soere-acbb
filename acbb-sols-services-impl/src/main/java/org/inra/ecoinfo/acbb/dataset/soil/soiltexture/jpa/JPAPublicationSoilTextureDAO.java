/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.jpa;

import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationSoilTextureDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        List<Long> sequenceIds = getSequenceIds(version);
        if (sequenceIds.isEmpty()) {
            return;
        }
        List<Long> mesureIds = getMesureIds(sequenceIds);
        if (mesureIds.isEmpty()) {
            return;
        }
        deleteValeurs(mesureIds);
        deleteMesures(mesureIds);
        deleteSequences(sequenceIds);
    }

    /**
     * @param sequenceIds
     * @return
     */
    protected List<Long> getMesureIds(List<Long> sequenceIds) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<MesureSoilTexture> mesure = query.from(MesureSoilTexture.class);
        Join<MesureSoilTexture, SoilTexture> seqM = mesure.join(MesureSoilTexture_.texture);
        query
                .select(mesure.get(MesureSoilTexture_.id))
                .where(seqM.get(SoilTexture_.id).in(sequenceIds));
        List<Long> mesureIds = getResultList(query);
        return mesureIds;
    }

    /**
     * @param version
     * @return
     */
    protected List<Long> getSequenceIds(VersionFile version) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<SoilTexture> sequence = query.from(SoilTexture.class);
        Join<SoilTexture, VersionFile> vers = sequence.join(SoilTexture_.version);
        query
                .select(sequence.get(SoilTexture_.id))
                .where(builder.equal(vers, version));
        List<Long> sequenceIds = getResultList(query);
        return sequenceIds;
    }

    private void deleteValeurs(List<Long> valeurIds) {
        if (CollectionUtils.isEmpty(valeurIds)) {
            return;
        }
        CriteriaDelete<ValeurSoilTexture> deleteValeurs = builder.createCriteriaDelete(ValeurSoilTexture.class);
        Root<ValeurSoilTexture> valeur = deleteValeurs.from(ValeurSoilTexture.class);
        deleteValeurs.where(valeur.get(ValeurSoilTexture_.mesureSoilTexture).get(MesureSoilTexture_.id).in(valeurIds));
        entityManager.createQuery(deleteValeurs).executeUpdate();
    }

    private void deleteMesures(List<Long> mesureIds) {
        if (CollectionUtils.isEmpty(mesureIds)) {
            return;
        }
        CriteriaDelete<MesureSoilTexture> deleteMesures = builder.createCriteriaDelete(MesureSoilTexture.class);
        Root<MesureSoilTexture> mesure = deleteMesures.from(MesureSoilTexture.class);
        deleteMesures.where(mesure.get(MesureSoilTexture_.id).in(mesureIds));
        entityManager.createQuery(deleteMesures).executeUpdate();
    }

    private void deleteSequences(List<Long> sequenceIds) {
        if (CollectionUtils.isEmpty(sequenceIds)) {
            return;
        }
        CriteriaDelete<SoilTexture> deleteSequence = builder.createCriteriaDelete(SoilTexture.class);
        Root<SoilTexture> sequence = deleteSequence.from(SoilTexture.class);
        deleteSequence.where(sequence.get(SoilTexture_.id).in(sequenceIds));
        entityManager.createQuery(deleteSequence).executeUpdate();
    }

}
