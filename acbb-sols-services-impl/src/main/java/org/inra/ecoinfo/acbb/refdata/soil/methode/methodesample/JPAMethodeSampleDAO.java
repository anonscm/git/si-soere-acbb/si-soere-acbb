/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesample;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeSampleDAO extends AbstractJPAMethodeDAO<Methodesample> implements
        IMethodeSampleDAO {

    /**
     * @return
     */
    @Override
    public Class<Methodesample> getMethodClass() {
        return Methodesample.class;
    }
}
