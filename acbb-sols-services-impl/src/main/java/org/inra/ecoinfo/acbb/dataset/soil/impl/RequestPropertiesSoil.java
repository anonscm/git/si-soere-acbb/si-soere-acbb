/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.impl;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.utils.Column;

import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Class RequestPropertiesSoil.
 */
public abstract class RequestPropertiesSoil extends AbstractRequestPropertiesACBB implements
        IRequestPropertiesSoil, Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The value columns.
     */
    @Transient
    Map<Integer, Column> valueColumns = new TreeMap();

    /**
     * Instantiates a new request properties soil.
     */
    public RequestPropertiesSoil() {
        super();
    }

    /**
     * @return
     */
    @Override
    public Map<Integer, Column> getValueColumns() {
        return this.valueColumns;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#setDateDeFin
     * (java.time.LocalDateTime)
     */
    @Override
    public final void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.DAYS, 1);
    }
}
