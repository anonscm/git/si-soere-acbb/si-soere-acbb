package org.inra.ecoinfo.acbb.extraction.soil;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * @author vkoyao
 */

/**
 * The Interface ISoilExtractionDAO.
 *
 * @param <T> the generic type
 */
public interface ISoilExtractionDAO<T extends ValeurSoil> extends IDAO<T> {

    /**
     * Extract Soil mesures.
     *
     * @param selectedTraitementProgrammes
     * @param user
     * @link(List<TraitementProgramme>) the selected traitement programmes
     * @param selectedVariables
     * @link(List<Variable>) the selected variables
     * @param intervalsDates
     * @link(List<IntervalDate>) the intervals dates
     * @return the list
     */
    List<T> extractSoilValues(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervalsDates, IUser user);

    long getSize(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervalsDates, IUser user);


    /**
     * Gets the availables variables by sites.
     *
     * @param linkedList
     * @param intervals
     * @param user
     * @link(List<SiteACBB>) the linked list
     * @return the availables variables by sites
     */
    List<NodeDataSet> getAvailableVariableByTraitement(List<TraitementProgramme> linkedList, List<IntervalDate> intervals, IUser user);

    /**
     * Gets the available traitements.
     *
     * @param intervalsDate
     * @param user
     * @link(List<IntervalDate>) the intervals date
     * @return the available traitements
     * @throws SecurityException
     */
    List<TraitementProgramme> getAvailablesTraitementsForPeriode(List<IntervalDate> intervalsDate, IUser user);

    /**
     *
     * @return
     */
    String getDatatypeCode();

}
