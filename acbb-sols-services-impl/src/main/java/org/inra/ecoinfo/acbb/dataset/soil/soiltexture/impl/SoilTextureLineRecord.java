package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class SoilTextureLineRecord extends AbstractSoilLineRecord<SoilTextureLineRecord, VariableValueSoilTexture> {

    Methodesample methodeObtention;

    /**
     * @param originalLineNumber
     */
    public SoilTextureLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }

    /**
     * @return
     */
    public Methodesample getMethodeObtention() {
        return methodeObtention;
    }

    void setMethodeObtention(Methodesample methodeObtention) {
        this.setMethodeSample(methodeObtention);
    }
}
