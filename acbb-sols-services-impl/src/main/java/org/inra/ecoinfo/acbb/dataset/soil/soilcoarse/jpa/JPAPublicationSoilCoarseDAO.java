/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse_;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationSoilCoarseDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurSoilCoarse> deleteValeurs = builder.createCriteriaDelete(ValeurSoilCoarse.class);
        Root<ValeurSoilCoarse> valeur = deleteValeurs.from(ValeurSoilCoarse.class);
        Subquery<SoilCoarse> subquery = deleteValeurs.subquery(SoilCoarse.class);
        Root<SoilCoarse> soilCoarse = subquery.from(SoilCoarse.class);
        subquery.select(soilCoarse)
                .where(builder.equal(soilCoarse.get(SoilCoarse_.version), version));
        deleteValeurs.where(valeur.get(ValeurSoilCoarse_.coarse).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<SoilCoarse> deleteSequence = builder.createCriteriaDelete(SoilCoarse.class);
        Root<SoilCoarse> sequence = deleteSequence.from(SoilCoarse.class);
        deleteSequence.where(builder.equal(sequence.get(SoilCoarse_.version), version));
        delete(deleteSequence);
    }

}
