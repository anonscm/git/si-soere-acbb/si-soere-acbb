package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * The Class Recorder.
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractMethodeRecorder<MethodeAnalyse> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.messages";
    /**
     * The LOGGER.
     */
    static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    /**
     * the constant messages
     */
    private static final String PROPERTY_MSG_BAD_EQUATION_QUALITE_PREDICTION = "PROPERTY_MSG_BAD_EQUATION_QUALITE_PREDICTION";
    private static final String CST_COMMA = ",";
    private static final String CST_COMMA_SPACE = ", ";

    /**
     * the constant messages
     */
    /**
     * the constant messages
     */
    private static final String PROPERTY_MSG_INVALID_ELEMENT_LIST = "PROPERTY_MSG_INVALID_ELEMENT_LIST";
    private static final String PROPERTY_MSG_MISSING_ELEMENT = "PROPERTY_MSG_MISSING_ELEMENT";
    /**
     * The methodeanalyse possibles @link(Map<String,List<String>>).
     */
    Map<String, List<String>> methodeAnalysePossibles;
    int lineNumber = 0;
    /**
     * the constant messages
     */

    private IListeSoilDAO listeSoilDAO;
    private IMethodeAnalyseDAO methodeAnalyseDAO;

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String numberIdString = tokenizerValues.nextToken();
                ErrorsReport errorsReport = new ErrorsReport();
                MethodeAnalyse methodeAnalyse = this.getByNumberId(numberIdString, errorsReport).orElseThrow(BusinessException::new);
                methodeAnalyse.getComposante().clear();
                this.methodeAnalyseDAO.saveOrUpdate(methodeAnalyse);
                this.methodeAnalyseDAO.remove(methodeAnalyse);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }

    }

    private String elementsToString(List<MethodeAnalyseComposant> elements) {
        StringBuilder returnString = new StringBuilder();
        for (int i = 0; i < elements.size(); i++) {
            if (i > 0) {
                returnString.append(Recorder.CST_COMMA_SPACE);
            }
            String nom = elements.get(i).getValeur();
            returnString.append(nom);
        }
        return returnString.toString();
    }

    /**
     * @param allMethods
     * @return
     */
    @Override
    protected Set<MethodeAnalyse> filter(Set<AbstractMethode> allMethods) {
        Set<MethodeAnalyse> methodes = new HashSet();
        for (AbstractMethode methode : allMethods) {
            if (methode instanceof MethodeAnalyse) {
                methodes.add((MethodeAnalyse) methode);
            }
        }
        return methodes;
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<MethodeAnalyse> getAllElements() {
        List<MethodeAnalyse> all = this.methodeAnalyseDAO.getAll();
        Collections.sort(all);
        return all;
    }

    private MethodeAnalyseComposant getElementDb(String elementName) throws PersistenceException {
        MethodeAnalyseComposant elementDb = null;
        elementDb = (MethodeAnalyseComposant) this.listeSoilDAO.getByNKey(MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS, elementName).orElse(null);
        if (elementDb == null) {
            throw new PersistenceException();
        }
        return elementDb;
    }

    @SuppressWarnings({})
    private List<MethodeAnalyseComposant> getElementsDb(
            AbstractCSVMetadataRecorder.ErrorsReport errorsReport, int lineNumber,
            AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues)
            throws BusinessException {
        List<MethodeAnalyseComposant> elementsDb = new LinkedList<>();
        String elementsListe = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(elementsListe)) {
            return elementsDb;
        }
        String[] elementsListeArray = elementsListe.split(Recorder.CST_COMMA);
        for (String element : elementsListeArray) {
            MethodeAnalyseComposant elementDB = null;
            try {
                elementDB = this.getElementDb(element.trim());
                elementsDb.add(elementDB);
            } catch (PersistenceException e) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_ELEMENT),
                        lineNumber, tokenizerValues.currentTokenIndex() - 1, element));
            }
        }
        if (elementsDb.isEmpty()) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_INVALID_ELEMENT_LIST),
                    lineNumber, tokenizerValues.currentTokenIndex() - 1));
        }
        return elementsDb;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeDefinitionName() {
        return AbstractMethode.ATTRIBUTE_JPA_DEFINITION;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeEntityName() {
        return MethodeAnalyse.NAME_ENTITY_JPA;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeLibelleName() {
        return AbstractMethode.ATTRIBUTE_JPA_LIBELLE;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param methodeAnalyse
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(Bloc) the bloc
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final MethodeAnalyse methodeAnalyse)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = super
                .getNewLineModelGridMetadata(methodeAnalyse);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        methodeAnalyse == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeAnalyse.getReference_donesol(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        methodeAnalyse == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeAnalyse.getNorme(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        methodeAnalyse == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this
                                .elementsToString(methodeAnalyse.getComposante()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        methodeAnalyse == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeAnalyse.getReference_las(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        return lineModelGridMetadata;
    }

    /**
     * @return
     */
    @Override
    protected ModelGridMetadata<MethodeAnalyse> initModelGridMetadata() {
        this.initProperties();
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(
                        values, MethodeAnalyse.NAME_ENTITY_JPA);
                final Long numberId = this.getNumberId(errorsReport, lineNumber, tokenizerValues);
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String definition = this.getDefinition(errorsReport, lineNumber,
                        tokenizerValues);
                final String reference_donesol = tokenizerValues.nextToken();
                final String norme = tokenizerValues.nextToken();
                final List<MethodeAnalyseComposant> composante = this.getElementsDb(errorsReport, lineNumber, tokenizerValues);
                final String reference_las = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMethodeAnalyse(errorsReport, numberId, libelle,
                            definition, composante, reference_donesol, norme, reference_las);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMethodeAnalyse(AbstractCSVMetadataRecorder.ErrorsReport errorsReport,
                                            Long numberId, String libelle, String definition, List<MethodeAnalyseComposant> composante, String reference_donesol, String norme, String reference_las) {
        MethodeAnalyse methodeAnalyseDb = this.methodeAnalyseDAO.getByNKey(numberId).orElse(null);
        if (methodeAnalyseDb == null) {
            methodeAnalyseDb = new MethodeAnalyse(libelle, definition, definition, numberId, composante, reference_donesol, norme, reference_las);

        } else {
            methodeAnalyseDb.update(definition, composante, reference_donesol, norme, reference_las);
        }
        try {
            this.methodeAnalyseDAO.saveOrUpdate(methodeAnalyseDb);
        } catch (PersistenceException e) {
            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);
        }
    }

    /**
     * @param methodeAnalyseDAO the methodeAnalyseDAO to set
     */
    public void setMethodeAnalyseSoilDAO(IMethodeAnalyseDAO methodeAnalyseDAO) {
        this.methodeAnalyseDAO = methodeAnalyseDAO;
    }

    /**
     * @param listeSoilDAO
     */
    public void setListeSoilDAO(IListeSoilDAO listeSoilDAO) {
        this.listeSoilDAO = listeSoilDAO;
    }

}
