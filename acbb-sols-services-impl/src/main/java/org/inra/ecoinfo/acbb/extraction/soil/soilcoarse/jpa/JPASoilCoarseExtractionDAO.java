/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.soilcoarse.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse_;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.AbstractJPASoilDAO;
import org.inra.ecoinfo.acbb.synthesis.soilcoarse.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPASoilCoarseExtractionDAO extends
        AbstractJPASoilDAO<SoilCoarse, ValeurSoilCoarse> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurSoilCoarse> getValeurSoilClass() {
        return ValeurSoilCoarse.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurSoilCoarse, SoilCoarse> getMesureAttribute() {
        return ValeurSoilCoarse_.coarse;
    }

    /**
     * @return
     */
    @Override
    protected Class<SoilCoarse> getSoilClass() {
        return SoilCoarse.class;
    }
}
