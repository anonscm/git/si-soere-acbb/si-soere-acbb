package org.inra.ecoinfo.acbb.extraction.soil.impl;

import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.inra.ecoinfo.acbb.dataset.ComparatorWithFirstStringThenAlpha;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorBuilderACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IMesureSoil;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl.ProcessRecordContentChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.impl.ContentChemistryBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.IListeSoilDAO;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.VariableDescriptor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

/**
 * @param <S>
 * @param <V>
 * @param <M>
 * @author vkoyao
 */
public abstract class AbstractSoilOutputBuilder<S extends AbstractSoil, V extends ValeurSoil, M extends IMesureSoil<S, V>>
        extends AbstractOutputBuilder {

    /**
     *
     */
    public static final String DATASET_DESCRIPTOR_PATH_PATTERN = "org/inra/ecoinfo/acbb/dataset/%s";

    /**
     *
     */
    public static final String COLUMN_VALUE = "valeur";

    /**
     *
     */
    public static final String COLUMN_AVERAGE = "moyenne";

    /**
     *
     */
    public static final String COLUMN_MEDIAN = "mediane";

    /**
     *
     */
    public static final String COLUMN_STANDARD_DEVIATION = "et";
    /**
     *
     */
    public static final String PATTERN_CSV_VARIABLE_FIELD = ";%s(%s)";

    protected static final String CST_EXTRACT = "EXTRACT_";

    /**
     * The Constant HEADER_RAW_DATA @link(String).
     */
    protected static final String PROPERTY_HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";

    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_COLUMN_NAME = "PROPERTY_HEADER_RAW_DATA_COLUMN_NAME";

    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_UNITS = "PROPERTY_HEADER_RAW_DATA_UNITS";

    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.acbb.dataset.soil.impl.messages";

    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION = "PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION";

    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION_DATE = "PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION_DATE";

    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_COLUMN_NAME_FOR_OBSERVATION_DATE = "PROPERTY_HEADER_RAW_DATA_COLUMN_NAME_FOR_OBSERVATION_DATE";

    /*
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.soil.messages";

    /**
     * The Constant MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS @link(String).
     */
    protected static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNKNOWN_VARIABLE";

    /**
     * The Constant PATTERN_END_LINE @link(String).
     */
    protected static final String PATTERN_END_LINE = "\n";
    /**
     * The Constant REGEX_CSV_FIELD @link(String).
     */
    protected static final String REGEX_CSV_FIELD = "[^;]";
    /**
     * The Constant DATASET_DESCRIPTOR_XML @link(String).
     */
    protected static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";
    protected Map<String, String> columnPatterns = null;
    protected List<String> variableColumnTypes = Arrays.asList(AbstractSoilOutputBuilder.COLUMN_VALUE,
            AbstractSoilOutputBuilder.COLUMN_AVERAGE,
            AbstractSoilOutputBuilder.COLUMN_MEDIAN,
            AbstractSoilOutputBuilder.COLUMN_STANDARD_DEVIATION);
    /**
     * The dataset descriptor @link(DatasetDescriptor).
     */
    protected DatasetDescriptor datasetDescriptor;
    protected String resultCode = org.apache.commons.lang.StringUtils.EMPTY;
    protected String extractCode = org.apache.commons.lang.StringUtils.EMPTY;
    protected ISuiviParcelleDAO suiviParcelleDAO;

    /**
     *
     */
    protected IListeSoilDAO listeSoilDAO;

    /**
     * The variable dao @link(IVariableACBBDAO).
     */
    protected IVariableACBBDAO variableDAO;

    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    protected IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     * The variables descriptor @link(Map<String,VariableDescriptor>).
     */
    protected Map<String, VariableDescriptor> variablesDescriptor = new HashMap();

    /**
     * @param datasetDescriptorPath
     */
    public AbstractSoilOutputBuilder(String datasetDescriptorPath) {
        super();
        try {
            if (this.datasetDescriptor == null) {
                this.setDatasetDescriptor(this.getDatasetDescriptor(datasetDescriptorPath));
                for (final Column column : this.datasetDescriptor.getColumns()) {
                    if (null != column.getFlagType()) {
                        switch (column.getFlagType()) {
                            case RecorderACBB.PROPERTY_CST_VARIABLE_TYPE:
                            case RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE:
                                this.variablesDescriptor.put(column.getName(),
                                        new VariableDescriptor(column.getName()));
                                break;
                            case RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE:
                                this.variablesDescriptor.get(column.getRefVariableName())
                                        .setHasQualityClass(true);
                                break;
                        }
                    }
                }
            }
        } catch (final IOException | SAXException | URISyntaxException ex) {
            AbstractOutputBuilder.LOGGER.debug(ex.getMessage(), ex);
        }
    }

    /**
     * @param number
     * @return
     */
    public String getNAIfBadValueOrNVIfEmptyValue(Integer number) {
        if (number == null || ACBBUtils.CST_INVALID_EMPTY_MEASURE == number) {
            return ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE;
        } else if (ACBBUtils.CST_INVALID_BAD_MEASURE == number) {
            return ACBBUtils.PROPERTY_CST_BAD_VALUE;
        }
        return number.toString();
    }

    /**
     * @return
     */
    protected abstract String getFirstVariable();

    protected void addColumnName(Variable variable, String columnType,
                                 List<ListeSoil> valueColumns, StringBuilder stringBuilder1,
                                 StringBuilder stringBuilder2, StringBuilder stringBuilder3,
                                 Properties propertiesVariableName, Properties propertiesUniteName,
                                 Properties propertiesValeurQualitativeValeur) throws PersistenceException {
        String localizedVariableName = propertiesVariableName.getProperty(variable.getName(), variable.getName());
        Optional<Unite> unite = this.datatypeVariableUniteACBBDAO.getUnite(this.getExtractCode(), variable.getCode());
        for (ListeSoil valueColumn : valueColumns) {
            if (valueColumn.getValeur().equals(columnType)) {
                String columnDefinition = propertiesValeurQualitativeValeur.getProperty(valueColumn.getValeur(), valueColumn.getValeur());
                String columnName = org.apache.commons.lang.StringUtils.EMPTY;
                if (columnType.startsWith(AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE.toLowerCase())) {
                    columnName = WordUtils.capitalize(columnType.replaceAll("_([^_]*)$", "_" + "pour_" + variable.getAffichage() + "_$1"));
                } else {
                    columnName = String.format("%s_%s", variable.getAffichage(), columnType);
                }
                String uniteName = org.apache.commons.lang.StringUtils.EMPTY;
                if (this.variableColumnTypes.stream()
                        .anyMatch(vt -> columnType.endsWith(vt))) {
                    uniteName = propertiesUniteName.getProperty(unite.map(u -> u.getName()).orElseGet(String::new), unite.map(u -> u.getName()).orElseGet(String::new));
                }
                columnDefinition = String.format(AbstractSoilOutputBuilder.PATTERN_CSV_VARIABLE_FIELD, localizedVariableName, columnDefinition);
                stringBuilder1.append(columnDefinition);
                stringBuilder2.append(String.format(ACBBMessages.PATTERN_1_FIELD, uniteName));
                stringBuilder3.append(String.format(ACBBMessages.PATTERN_1_FIELD, columnName));
                return;
            }
        }
    }

    /**
     * @param stringBuilder1
     * @param stringBuilder2
     * @param stringBuilder3
     */
    protected void addGenericColumns(final StringBuilder stringBuilder1,
                                     final StringBuilder stringBuilder2, final StringBuilder stringBuilder3) {
        stringBuilder1.append(this.getLocalizationManager().getMessage(
                AbstractSoilOutputBuilder.BUNDLE_PATH,
                AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA));
        stringBuilder2.append(this
                .getLocalizationManager()
                .getMessage(AbstractSoilOutputBuilder.BUNDLE_PATH, AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA_UNITS));
        stringBuilder3.append(this.getLocalizationManager().getMessage(
                AbstractSoilOutputBuilder.BUNDLE_PATH,
                AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA_COLUMN_NAME));
    }

    /**
     * @param stringBuilder1
     * @param stringBuilder2
     * @param stringBuilder3
     */
    protected void addObservation(final StringBuilder stringBuilder1,
                                  final StringBuilder stringBuilder2, final StringBuilder stringBuilder3) {
        stringBuilder1.append(this.getLocalizationManager().getMessage(
                AbstractSoilOutputBuilder.BUNDLE_PATH,
                AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION));
        stringBuilder2.append(";");
        stringBuilder3.append(this.getLocalizationManager().getMessage(
                AbstractSoilOutputBuilder.BUNDLE_PATH,
                AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION));
    }

    /**
     * @param stringBuilder1
     * @param stringBuilder2
     * @param stringBuilder3
     */
    protected void addSpecificColumns(StringBuilder stringBuilder1, StringBuilder stringBuilder2,
                                      StringBuilder stringBuilder3) {
        stringBuilder1.append(this.getLocalizationManager().getMessage(this.getBundleSourcePath(),
                AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA));
        String specificsUnits = "";
        if (this instanceof ContentChemistryBuildOutputDisplay) {
            specificsUnits = this
                    .getLocalizationManager()
                    .getMessage(this.getBundleSourcePath(),
                            AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA_UNITS);
        } else {
            specificsUnits = this
                    .getLocalizationManager()
                    .getMessage(this.getBundleSourcePath(),
                            AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA)
                    .replaceAll(AbstractSoilOutputBuilder.REGEX_CSV_FIELD,
                            org.apache.commons.lang.StringUtils.EMPTY);
        }
        stringBuilder2.append(specificsUnits);
        stringBuilder3.append(this.getLocalizationManager().getMessage(this.getBundleSourcePath(),
                AbstractSoilOutputBuilder.PROPERTY_HEADER_RAW_DATA_COLUMN_NAME));
    }

    /**
     * @param headers
     * @param requestMetadatasMap
     * @param resultsDatasMap
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String,
     * java.util.Map, java.util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        final List<Variable> selectedSoilVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(this.getExtractCode());
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            outputPrintStreamMap.get(datatypeNameEntry.getKey()).println(headers);
        }
        final PrintStream rawDataPrintStream = outputPrintStreamMap.get(this.getExtractCode());
        final OutputHelper outputHelper = this.getOutPutHelper(datesYearsContinuousFormParamVO,
                selectedSoilVariables, rawDataPrintStream);
        final List<V> valeurSoil = resultsDatasMap.get(AbstractOutputBuilder.MAP_INDEX_0);
        outputHelper.buildBody(valeurSoil, requestMetadatasMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Properties propertiesVariableName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        final Properties propertiesUniteName = this.localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME);
        final Properties propertiesValeurQualitativeValeur = this.localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA, "valeur");
        final ErrorsReport errorsReport = new ErrorsReport();
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final SortedMap<String, Variable> selectedVariablesAffichage = new TreeMap(new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
        for (final Variable variable : selectedVariables) {
            selectedVariablesAffichage.put(variable.getAffichage(), variable);
        }
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        List<ListeSoil> valueColumns = this.getValueColumns();
        try {
            this.addGenericColumns(stringBuilder1, stringBuilder2, stringBuilder3);
            if (requestMetadatasMap.containsKey(ContentChemistryBuildOutputDisplay.SOIL_SAMPLES)) {
                this.addSpecificColumns(stringBuilder1, stringBuilder2, stringBuilder3);
            }
            addValuesColumns(selectedVariablesAffichage, errorsReport, selectedVariables, requestMetadatasMap, valueColumns, stringBuilder1, stringBuilder2, stringBuilder3, propertiesVariableName, propertiesUniteName, propertiesValeurQualitativeValeur);
        } catch (final PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder1.append(AbstractSoilOutputBuilder.PATTERN_END_LINE)
                .append(stringBuilder2).append(AbstractSoilOutputBuilder.PATTERN_END_LINE)
                .append(stringBuilder3).toString();
    }

    protected void addValuesColumns(final SortedMap<String, Variable> selectedVariablesAffichage, final ErrorsReport errorsReport, final List<Variable> selectedVariables, final Map<String, Object> requestMetadatasMap, List<ListeSoil> valueColumns, final StringBuilder stringBuilder1, final StringBuilder stringBuilder2, final StringBuilder stringBuilder3, final Properties propertiesVariableName, final Properties propertiesUniteName, final Properties propertiesValeurQualitativeValeur) throws PersistenceException {
        for (Entry<String, Variable> variableEntry : selectedVariablesAffichage.entrySet()) {
            if (variableEntry.getValue() == null) {
                errorsReport
                        .addErrorMessage(String
                                .format(this
                                                .getLocalizationManager()
                                                .getMessage(
                                                        AbstractSoilOutputBuilder.BUNDLE_SOURCE_PATH,
                                                        AbstractSoilOutputBuilder.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                        selectedVariables));
                continue;
            }
            for (Entry<String, String> patternEntry : this.columnPatterns.entrySet()) {
                if (!requestMetadatasMap.containsKey(ProcessRecordContentChemistry.CONSTANT_HUMIDITE_RESIDUELLE) && patternEntry.getKey().startsWith(AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE.toLowerCase())) {
                    continue;
                }
                String columnType = patternEntry.getKey();
                this.addColumnName(variableEntry.getValue(), columnType, valueColumns, stringBuilder1,
                        stringBuilder2, stringBuilder3, propertiesVariableName,
                        propertiesUniteName, propertiesValeurQualitativeValeur);
            }
            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        }
    }

    /**
     * @param parameters
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        if (!((DefaultParameter) parameters).getResults().containsKey(this.getResultCode())
                || ((DefaultParameter) parameters).getResults().get(this.getResultCode())
                .get(this.getExtractCode()) == null
                || ((DefaultParameter) parameters).getResults().get(this.getResultCode())
                .get(this.getExtractCode()).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, this.getResultCode()));
        return null;
    }

    /**
     * @return
     */
    protected abstract String getBundleSourcePath();

    /**
     * @return
     */
    public Map<String, String> getColumnPatterns() {
        return this.columnPatterns;
    }

    /**
     * @param columnPatterns
     */
    public void setColumnPatterns(Map<String, String> columnPatterns) {
        this.columnPatterns = columnPatterns;
    }

    protected DatasetDescriptorACBB getDatasetDescriptor(String datasetDescriptorPath)
            throws IOException, SAXException, URISyntaxException {
        final String path = String.format(
                AbstractSoilOutputBuilder.DATASET_DESCRIPTOR_PATH_PATTERN,
                datasetDescriptorPath);
        return DatasetDescriptorBuilderACBB.buildDescriptorACBB(this.getClass().getClassLoader()
                .getResourceAsStream(path));
    }

    /**
     * @return
     */
    protected String getExtractCode() {
        return this.extractCode;
    }

    /**
     * @param extractCode the extractCode to set
     */
    public void setExtractCode(String extractCode) {
        this.extractCode = extractCode;
        this.resultCode = new StringBuffer(AbstractSoilOutputBuilder.CST_EXTRACT).append(
                extractCode).toString();
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param rawDataPrintStream
     * @return
     */
    protected abstract OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedWsolVariables,
            final PrintStream rawDataPrintStream);

    /**
     * @return
     */
    protected String getResultCode() {
        return this.resultCode;
    }

    /**
     * @param parcelle
     * @param date
     * @return
     */
    public Optional<SuiviParcelle> getSuiviParcelle(Parcelle parcelle, LocalDate date) {
        return suiviParcelleDAO.retrieveSuiviParcelle(parcelle, date);

    }

    /**
     * @return
     */
    protected List<ListeSoil> getValueColumns() {
        return this.listeSoilDAO.getValueColumns();
    }

    /**
     * Sets the dataset descriptor {@link DatasetDescriptor}.
     *
     * @param datasetDescriptor the new dataset descriptor
     *                          {@link DatasetDescriptor}
     */
    void setDatasetDescriptor(final DatasetDescriptor datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteDAO the new datatype unite variable acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteDAO;
    }

    /**
     * @param listeSoilDAO
     */
    public void setListeSoilDAO(IListeSoilDAO listeSoilDAO) {
        this.listeSoilDAO = listeSoilDAO;
    }

    /**
     * @param suiviParcelleDAO
     */
    public void setSuiviParcelleDAO(ISuiviParcelleDAO suiviParcelleDAO) {
        this.suiviParcelleDAO = suiviParcelleDAO;
    }

    /**
     * Sets the variable dao {@link IVariableACBBDAO}.
     *
     * @param variableDAO the new variable dao {@link IVariableACBBDAO}
     */
    public void setVariableDAO(final IVariableACBBDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     * The Class OutputHelper.
     *
     * @param <X>
     */
    public abstract class OutputHelper<X extends AbstractValues> {

        /**
         *
         */
        protected static final String PATTERN_DATE_YYYY = "YYYY";
        /**
         * Site;T(Treatment);Plot number;block number;repeat
         * number;Observation;Rotation number if it exists;Year in rotation or
         * since start of treatment;Campaign or series name;Layer upper
         * bound;Layer lower bound;Layer name;Average sampling date"or"Sample
         * collection date" The Constant PATTERN_5_FIELD @link(String).
         */
        static final String PATTERN_13_FIELD = "%s;%s(%s);%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         * The raw data print stream @link(PrintStream).
         */
        final protected PrintStream rawDataPrintStream;
        /**
         * The variables affichage @link(List).
         */
        protected final List<String> variablesAffichage = new LinkedList();

        /**
         *
         */
        protected final Properties propertiesCouvertName = localizationManager
                .newProperties(
                        ValeurQualitative.NAME_ENTITY_JPA,
                        ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        /**
         *
         */
        protected final Properties propertiesParcelleName = localizationManager
                .newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME);
        /**
         *
         */
        protected final Properties propertiesSiteName = localizationManager
                .newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        /**
         *
         */
        protected final Properties propertiesTreatmentAffichage = localizationManager
                .newProperties(
                        TraitementProgramme.NAME_ENTITY_JPA,
                        "affichage");
        /**
         *
         */
        protected final Properties propertiesValeursQualitative = localizationManager
                .newProperties(
                        ValeurQualitative.NAME_ENTITY_JPA,
                        "valeur");
        protected final SortedMap<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<S, X>>>> sortedValeurs = new TreeMap();

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         */
        public OutputHelper(final PrintStream rawDataPrintStream, final List<Variable> selectedVariables) {
            super();
            this.rawDataPrintStream = rawDataPrintStream;
            for (final Variable variable : selectedVariables) {
                this.variablesAffichage.add(variable.getAffichage());
            }
        }

        /**
         * @param valeur
         * @return
         */
        protected abstract M getMesure(V valeur);

        /**
         * @param valeur
         * @return
         */
        protected abstract S getSequence(V valeur);

        /**
         * @param valeur
         */
        public void addValeur(final V valeur) {
            final S sequence = getSequence(valeur);
            final M mesure = getMesure(valeur);
            final SiteACBB site = sequence.getSuiviParcelle().getParcelle().getSite();
            final TraitementProgramme treatment = sequence.getSuiviParcelle().getTraitement();
            final Parcelle parcelle = sequence.getSuiviParcelle().getParcelle();
            sortedValeurs
                    .computeIfAbsent(site, k -> new TreeMap<>())
                    .computeIfAbsent(treatment, k -> new TreeMap<>())
                    .computeIfAbsent(parcelle, k -> new TreeMap<>())
                    .compute(sequence, (s, v) -> createOrUpdate(s, v, mesure, valeur));
        }

        /**
         * Builds the body.
         *
         * @param valeurs
         * @param requestMap
         * @link(List<MesureFluxTours>) the mesures
         */
        public void buildBody(final List<V> valeurs, Map<String, Object> requestMap) {
            for (final V valeur : valeurs) {
                this.addValeur(valeur);
            }
            for (final Entry<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<S, X>>>> siteEntry : this.sortedValeurs.entrySet()) {
                for (final Entry<TraitementProgramme, SortedMap<Parcelle, SortedMap<S, X>>> treatmentEntry : siteEntry.getValue().entrySet()) {
                    for (final Entry<Parcelle, SortedMap<S, X>> parcelleEntry : treatmentEntry.getValue().entrySet()) {
                        for (final Entry<S, X> sequenceEntry : parcelleEntry.getValue().entrySet()) {
                            S sequence = sequenceEntry.getKey();
                            this.printLineGeneric(sequence, siteEntry.getKey(), treatmentEntry.getKey(), parcelleEntry.getKey());
                            if (requestMap.containsKey(ContentChemistryBuildOutputDisplay.SOIL_SAMPLES)) {
                                this.printLineSpecific(sequence, requestMap);
                            }
                            this.printObservation(sequence);
                            printValue(sequenceEntry.getValue(), requestMap);
                            this.rawDataPrintStream.print("\n");
                        }
                    }
                }
            }
        }

        /**
         * @param mesure
         * @param valeurs
         * @param requestMap
         */
        protected void addMesureLine(M mesure, List<V> valeurs, Map<String, Object> requestMap) {
            List<V> listValeurs = mesure.getValeurs();
            boolean isSequenceMesure = valeurs.stream().filter(v -> listValeurs.contains(v)).count() == listValeurs.size();
            Collections.sort(this.variablesAffichage, new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            while (itVariablesAffichage.hasNext()) {
                variableAffichage = itVariablesAffichage.next();
                boolean isPrinted = false;
                isPrinted = isSequenceMesure
                        ? printValuesForSequenceMesure(listValeurs, variableAffichage, requestMap, valeurs, isPrinted)
                        : printValuesForMesures(listValeurs, variableAffichage, requestMap, valeurs, isPrinted);
                if (!isPrinted) {
                    this.addVariablesColumnsForNullVariable();

                }
            }
        }

        /**
         * @param listValeurs
         * @param variableAffichage
         * @param requestMap
         * @param valeurs
         * @param isPrinted
         * @return
         */
        protected boolean printValuesForMesures(List<V> listValeurs, String variableAffichage, Map<String, Object> requestMap, List<V> valeurs, boolean isPrinted) {
            return false;
        }

        /**
         * @param listValeurs
         * @param variableAffichage
         * @param requestMap
         * @param valeurs
         * @param isPrinted
         * @return
         */
        protected boolean printValuesForSequenceMesure(List<V> listValeurs, String variableAffichage, Map<String, Object> requestMap, List<V> valeurs, boolean isPrinted) {
            for (ValeurSoil valeurSoil : listValeurs) {
                if (variableAffichage.equals(((DatatypeVariableUniteACBB) valeurSoil.getRealNode().getNodeable())
                        .getVariable().getAffichage())) {
                    this.addVariablesColumns((V) valeurSoil);
                    this.addMethode(valeurSoil.getMethode(), requestMap);
                    valeurs.remove(valeurSoil);
                    isPrinted = true;
                    break;
                }
            }
            return isPrinted;
        }

        /**
         * @param method
         * @param requestMap
         */
        protected void addMethode(AbstractMethode method, Map<String, Object> requestMap) {
            Set<AbstractMethode> methods = (Set<AbstractMethode>) requestMap.computeIfAbsent(
                    AbstractMethode.class.getSimpleName(),
                    k -> new HashSet());
            if (method != null) {
                methods.add(method);
            }
        }

        /**
         * @param valeurSoil
         */
        protected abstract void addVariablesColumns(V valeurSoil);

        /**
         *
         */
        protected abstract void addVariablesColumnsForNullVariable();

        /**
         * Prints the line generic.
         *
         * @param sequence
         * @param site
         * @param parcelle
         * @param traitementProgramme
         */
        protected void printLineGeneric(S sequence, SiteACBB site, TraitementProgramme traitementProgramme, Parcelle parcelle) {
            String line;
            final Integer rotation = sequence.getVersionTraitementRealiseeNumber();
            final Integer annee = sequence.getAnneeRealiseeNumber();
            final Bloc bloc = parcelle.getBloc();
            line = String.format(OutputHelper.PATTERN_13_FIELD,
                    this.propertiesSiteName.getProperty(site.getName(), site.getName()),
                    traitementProgramme.getNom(),
                    this.propertiesTreatmentAffichage.getProperty(traitementProgramme.getAffichage(), traitementProgramme.getAffichage()),
                    this.propertiesParcelleName.getProperty(parcelle.getName(), parcelle.getName()),
                    bloc == null ? StringUtils.EMPTY : bloc.getBlocName(),
                    bloc == null ? StringUtils.EMPTY : bloc.getRepetitionName(),
                    rotation <= 0 ? org.apache.commons.lang.StringUtils.EMPTY : rotation,
                    annee < 0 ? org.apache.commons.lang.StringUtils.EMPTY : annee,
                    sequence.getCampaignOrSet(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(sequence.getLayerSup()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(sequence.getLayerInf()),
                    sequence.getLayer(),
                    DateUtil.getUTCDateTextFromLocalDateTime(sequence.getDate(), DateUtil.DD_MM_YYYY)
            );
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param sequence
         * @param requestMap
         */
        abstract protected void printLineSpecific(S sequence, Map<String, Object> requestMap);

        /**
         * @param sequence
         * @return
         */
        protected PrintStream printObservation(final S sequence) {
            return this.rawDataPrintStream.printf(ACBBMessages.PATTERN_1_FIELD,
                    sequence.getObservation());
        }

        /**
         * @param value
         */
        protected void printVariableColumnForStringValue(String value) {
            value = Strings.isNullOrEmpty(value) ? RecorderACBB.PROPERTY_CST_NOT_AVALAIBALE_FIELD
                    : String.format(ACBBMessages.PATTERN_1_FIELD, value);
            this.rawDataPrintStream.print(value);
        }

        /**
         * @param mesure
         * @return
         */
        protected String getAnnee(AbstractSoil mesure) {
            return DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDate(), OutputHelper.PATTERN_DATE_YYYY);
        }

        /**
         * @param mesure
         * @return
         */
        protected String getDateMesure(AbstractSoil mesure) {
            return DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDate(), DateUtil.DD_MM_YYYY);
        }

        /**
         * @param sequence
         * @param v
         * @param mesure
         * @param valeur
         * @return
         */
        protected X createOrUpdate(S sequence, X v, M mesure, V valeur) {
            if (v == null) {
                v = newValues(sequence, mesure, valeur);
            } else {
                v.add(sequence, mesure, valeur);
            }
            return v;
        }

        /**
         * @param sequence
         * @param mesure
         * @param valeur
         * @return
         */
        protected abstract X newValues(S sequence, M mesure, V valeur);

        /**
         * @param values
         * @param requestMap
         */
        protected void printValue(X values, Map<String, Object> requestMap) {
            SequenceValues sequenceValues = (SequenceValues) values;
            Collections.sort(this.variablesAffichage, new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            while (itVariablesAffichage.hasNext()) {
                variableAffichage = itVariablesAffichage.next();
                boolean isPrinted = false;
                for (V valeurSoil : new LinkedList<>(sequenceValues.getValues())) {
                    if (variableAffichage.equals(((DatatypeVariableUniteACBB) valeurSoil.getRealNode().getNodeable())
                            .getVariable().getAffichage())) {
                        addVariablesColumns(valeurSoil);
                        this.addMethode(valeurSoil.getMethode(), requestMap);
                        sequenceValues.getValues().remove(valeurSoil);
                        isPrinted = true;
                        break;
                    }
                }
                if (!isPrinted) {
                    addVariablesColumnsForNullVariable();
                }
            }
        }
    }

    /**
     *
     */
    protected abstract class AbstractValues {

        /**
         *
         */
        protected S sequence = null;

        /**
         *
         */
        protected boolean isMesureSequence = false;

        /**
         * @param sequence
         * @param mesure
         * @param valeur
         */
        abstract protected void add(S sequence, M mesure, V valeur);
    }

    /**
     *
     */
    protected class SequenceValues extends AbstractValues {

        /**
         *
         */
        protected List<V> values = new LinkedList();

        /**
         * @param sequence
         * @param valeur
         */
        public SequenceValues(S sequence, V valeur) {
            this.sequence = sequence;
            this.values.add(valeur);
        }

        /**
         * @return
         */
        public List<V> getValues() {
            return values;
        }

        /**
         * @param sequence
         * @param mesure
         * @param valeur
         */
        @Override
        protected void add(S sequence, M mesure, V valeur) {
            this.values.add(valeur);
        }

    }

    /**
     *
     */
    protected class SequenceMesureValues extends AbstractValues {

        /**
         *
         */
        protected Map<M, List<V>> mesures = new HashMap();

        /**
         * @param sequence
         * @param mesure
         * @param valeur
         */
        public SequenceMesureValues(S sequence, M mesure, V valeur) {
            this.sequence = sequence;
            this.mesures.computeIfAbsent(mesure, k -> new LinkedList())
                    .add(valeur);
        }

        /**
         * @return
         */
        public Map<M, List<V>> getMesures() {
            return mesures;
        }

        /**
         * @param sequence
         * @param mesure
         * @param valeur
         */
        @Override
        protected void add(S sequence, M mesure, V valeur) {
            this.mesures.computeIfAbsent(mesure, k -> new LinkedList())
                    .add(valeur);
        }
    }
}
