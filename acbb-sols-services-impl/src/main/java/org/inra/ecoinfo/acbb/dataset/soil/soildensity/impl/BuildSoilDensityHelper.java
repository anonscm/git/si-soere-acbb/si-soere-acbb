package org.inra.ecoinfo.acbb.dataset.soil.soildensity.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.ISoilDensityDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.SoilDensity;
import org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity.ValeurSoilDensity;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildSoilDensityHelper extends AbstractBuildHelper<SoilDensityLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = BuildSoilDensityHelper.class.getPackage().getName() + ".messages";

    Map<Parcelle, Map<LocalDate, Map<Long, Map<Float, Map<Float, SoilDensity>>>>> soilDensity = new HashMap();
    ISoilDensityDAO SoilDensityDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursSoilDensity(final SoilDensityLineRecord line,
                               final SoilDensity soildensity) {
        for (final VariableValueSoilDensity variableValue : line.getVariablesValues()) {
            final DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
            RealNode realNodeElement = getRealNodeForSequenceAndVariable(dvu);
            ValeurSoilDensity valeurSoilDensity;
            valeurSoilDensity = new ValeurSoilDensity(
                    soildensity,
                    Float.parseFloat(variableValue.getValue()),
                    realNodeElement, variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(),
                    variableValue.getQualityClass(),
                    variableValue.getMethode());
            soildensity.getValeurs().add(valeurSoilDensity);
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void build() throws PersistenceException {
        for (final SoilDensityLineRecord line : this.lines) {
            final SoilDensity SoilDensity = this.getOrCreateSoilDensity(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursSoilDensity(line, SoilDensity);
            this.SoilDensityDAO.saveOrUpdate(SoilDensity);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesSoil
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesSoil the request properties Soil
     */
    public void build(final VersionFile version, final List<SoilDensityLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesSoil);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    SoilDensity createNewSoilDensity(final SoilDensityLineRecord line) {
        SoilDensity SoilDensity = new SoilDensity(
                this.version,
                line.getDate(),
                line.getRotationNumber(),
                line.getAnneeNumber(),
                line.getSuiviParcelle(),
                line.getCampaignName(),
                line.getLayerName(),
                line.getLayerSup(),
                line.getLayerInf(),
                line.getMethodeSample()
        );
        SoilDensity.setObservation(line.getObservation());
        try {
            this.SoilDensityDAO.saveOrUpdate(SoilDensity);
        } catch (final PersistenceException e) {
            this.errorsReport.addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return SoilDensity;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    SoilDensity getOrCreateSoilDensity(final SoilDensityLineRecord line) {
        return soilDensity
                .computeIfAbsent(line.getParcelle(), kp -> new HashMap<LocalDate, Map<Long, Map<Float, Map<Float, SoilDensity>>>>())
                .computeIfAbsent(line.getDate(), kd -> new HashMap<Long, Map<Float, Map<Float, SoilDensity>>>())
                .computeIfAbsent(line.getMethodeSample().getId(), km -> new HashMap<Float, Map<Float, SoilDensity>>())
                .computeIfAbsent(line.getLayerInf(), ki -> new HashMap<Float, SoilDensity>())
                .computeIfAbsent(line.getLayerSup(), ks -> createNewSoilDensity(line));
    }

    /**
     * @param biomassProductionDAO the Biomass Production DAO to set
     */
    public void setSoilDensityDAO(ISoilDensityDAO biomassProductionDAO) {
        this.SoilDensityDAO = biomassProductionDAO;
    }
}
