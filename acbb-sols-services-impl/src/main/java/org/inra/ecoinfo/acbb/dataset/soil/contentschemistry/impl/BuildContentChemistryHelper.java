package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.IContentChemistryDAO;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ContentChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.MesureChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ValeurChemistry;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildContentChemistryHelper extends AbstractBuildHelper<ContentChemistryLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = BuildContentChemistryHelper.class.getPackage().getName() + ".messages";

    Map<Parcelle, Map<LocalDate, Map<Long, Map<Float, Map<Float, ContentChemistry>>>>> contentChemistries = new HashMap();
    IContentChemistryDAO contentChemistryDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursContentChemistry(final ContentChemistryLineRecord line,
                                    final ContentChemistry contentChemistry) {
        for (final VariableValueContentChemistry variableValue : line.getVariablesValues()) {
            final DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
            RealNode realNodeElement = getRealNodeForSequenceAndVariable(dvu);
            ValeurChemistry valeurChemistry;
            MesureChemistry mesure = contentChemistry.getMesures().stream()
                    .filter(
                            m -> m.getDatatypeVariableUnite().getVariable().getAffichage().equals(dvu.getVariable().getAffichage())
                    )
                    .filter(
                            m -> m.getSequence().equals(contentChemistry)
                    )
                    .findFirst().orElse(new MesureChemistry(contentChemistry, dvu));
            contentChemistry.getMesures().add(mesure);
            valeurChemistry = new ValeurChemistry(
                    mesure,
                    Float.parseFloat(variableValue.getValue()),
                    realNodeElement, variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(),
                    variableValue.getQualityClass(),
                    variableValue.getMethode());
            mesure.getValeursChemistry().add(valeurChemistry);

            if (variableValue.getResidualHumidityValue() != null && ACBBUtils.CST_INVALID_BAD_MEASURE < variableValue.getResidualHumidityValue()) {
                RealNode realNodeResidualHumidity = getRealNodeForSequenceAndVariableCode(ProcessRecordContentChemistry.CONSTANT_HUMIDITE_RESIDUELLE);
                final Float residualHumidityValue = variableValue.getResidualHumidityValue();
                valeurChemistry = new ValeurChemistry(
                        mesure, residualHumidityValue,
                        realNodeResidualHumidity,
                        variableValue.getResidualHumidityNumber(),
                        variableValue.getResidualHumidityStandardDeviation(),
                        -9999,
                        variableValue.getResidualHumidityMethod());
                mesure.getValeursChemistry().add(valeurChemistry);
            }
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void build() throws PersistenceException {
        for (final ContentChemistryLineRecord line : this.lines) {
            final ContentChemistry contentChemistry = this.getOrCreateContentChemistry(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursContentChemistry(line, contentChemistry);
            this.contentChemistryDAO.saveOrUpdate(contentChemistry);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesSoil
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesSoil the request properties Soil
     */
    public void build(final VersionFile version, final List<ContentChemistryLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesSoil);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    ContentChemistry createNewContentChemistry(final ContentChemistryLineRecord line) {
        ContentChemistry contentChemistry = new ContentChemistry(
                this.version,
                line.getDate(),
                line.getRotationNumber(),
                line.getAnneeNumber(),
                line.getSuiviParcelle(),
                line.getCampaignName(),
                line.getLayerName(),
                line.getLayerSup(),
                line.getLayerInf(),
                line.getMethodeSample()
        );
        contentChemistry.setObservation(line.getObservation());
        try {
            this.contentChemistryDAO.saveOrUpdate(contentChemistry);
        } catch (final PersistenceException e) {
            this.errorsReport.addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return contentChemistry;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    ContentChemistry getOrCreateContentChemistry(final ContentChemistryLineRecord line) {
        return contentChemistries
                .computeIfAbsent(line.getParcelle(), kp -> new HashMap<LocalDate, Map<Long, Map<Float, Map<Float, ContentChemistry>>>>())
                .computeIfAbsent(line.getDate(), kd -> new HashMap<Long, Map<Float, Map<Float, ContentChemistry>>>())
                .computeIfAbsent(line.getMethodeSample().getId(), km -> new HashMap<Float, Map<Float, ContentChemistry>>())
                .computeIfAbsent(line.getLayerInf(), ki -> new HashMap<Float, ContentChemistry>())
                .computeIfAbsent(line.getLayerSup(), ks -> createNewContentChemistry(line));
    }

    /**
     * @param biomassProductionDAO the Biomass Production DAO to set
     */
    public void setContentChemistryDAO(IContentChemistryDAO biomassProductionDAO) {
        this.contentChemistryDAO = biomassProductionDAO;
    }
}
