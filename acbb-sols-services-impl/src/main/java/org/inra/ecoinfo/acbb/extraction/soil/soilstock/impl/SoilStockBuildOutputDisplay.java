package org.inra.ecoinfo.acbb.extraction.soil.soilstock.impl;

import org.inra.ecoinfo.acbb.dataset.ComparatorWithFirstStringThenAlpha;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.SoilStock;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilOutputBuilder;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.PrintStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class SoilStockBuildOutputDisplay
        extends
        AbstractSoilOutputBuilder<SoilStock, ValeurSoilStock, SoilStock> {

    /**
     *
     */
    public static final String SOILSTOCKS = "soil_stocks";
    /**
     *
     */
    protected static final String FIRST_VARIABLE = "mav";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static protected final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.soil.soilstock.messages";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public SoilStockBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        return super.buildOutput(parameters);
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return SoilStockBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedVariables
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedVariables,
            PrintStream rawDataPrintStream) {
        return new SoilStockOutputHelper(rawDataPrintStream, selectedVariables);
    }

    /**
     *
     */
    protected class SoilStockOutputHelper extends OutputHelper<SequenceValues> {

        /**
         *
         */
        protected static final String PATTERN_11_FIELD = ";%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_5_FIELD = ";%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_3_FIELD = ";%s;%s;%s";

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         */
        protected SoilStockOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurSoil
         */
        @Override
        protected void addVariablesColumns(ValeurSoilStock valeurSoil) {
            String line = String.format(SoilStockOutputHelper.PATTERN_5_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getStandardDeviation()),
                    valeurSoil.getMethode().getNumberId(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesColumnsForNullVariable() {
            this.rawDataPrintStream.print(SoilStockOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param isHUT
         */
        protected void addVariablesColumnsForNullVariable(boolean isHUT) {
            this.rawDataPrintStream.print(SoilStockOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            if (isHUT) {
                this.rawDataPrintStream.print(SoilStockOutputHelper.PATTERN_3_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            }
        }

        /**
         * @param SoilStock
         * @param requestMap
         */
        @Override
        protected void printLineSpecific(SoilStock SoilStock, Map<String, Object> requestMap) {
            AbstractMethode methodesample = SoilStock.getMethodesample();
            String line = String.format(";%s",
                    methodesample == null ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : methodesample.getNumberId());
            if (methodesample != null) {
                addMethode(methodesample, requestMap);
            }
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected SoilStock getMesure(ValeurSoilStock valeur) {
            return valeur.getStock();
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected SoilStock getSequence(ValeurSoilStock valeur) {
            return getMesure(valeur);
        }

        /**
         * @param value
         * @param requestMap
         */
        @Override
        protected void printValue(SequenceValues value, Map<String, Object> requestMap) {
            Collections.sort(this.variablesAffichage, new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            while (itVariablesAffichage.hasNext()) {
                itVariablesAffichage.next();
                for (ValeurSoilStock valeurSoilStock : value.getValues()) {
                    addVariablesColumns(valeurSoilStock);
                    this.addMethode(valeurSoilStock.getMethode(), requestMap);
                }
            }
        }

        @Override
        protected SequenceValues newValues(SoilStock sequence, SoilStock mesure, ValeurSoilStock valeur) {
            return new SequenceValues(sequence, valeur);
        }
    }
}
