/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractVariableValueSoil;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur.MethodeAnalyse;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;

/**
 * @author ptcherniati
 */
public class VariableValueContentChemistry extends AbstractVariableValueSoil {

    Float residualHumidityValue = -9999F;
    Integer residualHumidityNumber = -9999;
    Float residualHumidityStandardDeviation = -9999F;
    MethodeAnalyse residualHumidityMethod;

    /**
     * @param dvu
     */
    public VariableValueContentChemistry(DatatypeVariableUniteACBB dvu) {
        super(dvu, null, null, -9999F, -9999, -9999F, -9999);
    }

    /**
     * @return
     */
    public Float getResidualHumidityValue() {
        return residualHumidityValue;
    }

    /**
     * @param residualHumidityValue
     */
    public void setResidualHumidityValue(Float residualHumidityValue) {
        this.residualHumidityValue = residualHumidityValue;
    }

    /**
     * @return
     */
    public Integer getResidualHumidityNumber() {
        return residualHumidityNumber;
    }

    /**
     * @param residualHumidityNumber
     */
    public void setResidualHumidityNumber(Integer residualHumidityNumber) {
        this.residualHumidityNumber = residualHumidityNumber == null || Integer.valueOf(ACBBUtils.CST_INVALID_EMPTY_MEASURE).compareTo(residualHumidityNumber) == 0
                ? 1
                : residualHumidityNumber;
    }

    /**
     * @return
     */
    public Float getResidualHumidityStandardDeviation() {
        return residualHumidityStandardDeviation;
    }

    /**
     * @param residualHumidityStandardDeviation
     */
    public void setResidualHumidityStandardDeviation(Float residualHumidityStandardDeviation) {
        this.residualHumidityStandardDeviation = residualHumidityStandardDeviation;
    }

    /**
     * @return
     */
    public MethodeAnalyse getResidualHumidityMethod() {
        return residualHumidityMethod;
    }

    /**
     * @param residualHumidityMethod
     */
    public void setResidualHumidityMethod(MethodeAnalyse residualHumidityMethod) {
        this.residualHumidityMethod = residualHumidityMethod;
    }

}
