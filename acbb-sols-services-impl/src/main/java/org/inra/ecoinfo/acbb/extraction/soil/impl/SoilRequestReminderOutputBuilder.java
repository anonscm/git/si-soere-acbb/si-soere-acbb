/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.Variable;
import java.io.PrintStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.inra.ecoinfo.acbb.extraction.AbstractRequestReminder;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.slf4j.MDC;

/**
 * The Class MPSRequestReminderOutputBuilder.
 */
public class SoilRequestReminderOutputBuilder extends AbstractRequestReminder {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.soil.messages";
    /**
     * The Constant CST_REQUEST_REMINDER @link(String).
     */
    static final String CST_REQUEST_REMINDER = "request_reminder";
    /**
     * The Constant PROPERTY_MSG_HEADER @link(String).
     */
    static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DATES @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_DATES = "PROPERTY_MSG_SELECTED_DATES";
    /**
     * The Constant PROPERTY_MSG_SELECTED_TREATMENT @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_TREATMENT = "PROPERTY_MSG_SELECTED_TREATMENT";
    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLE @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DISPLAY @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_DISPLAY = "PROPERTY_MSG_SELECTED_DISPLAY";
    /**
     * The Constant PROPERTY_MSG_COMMENT @link(String).
     */
    static final String PROPERTY_MSG_COMMENT = "PROPERTY_MSG_COMMENT";
    private static final String PROPERTY_MSG_SELECTED_SOIL_CHEMICAL_CONTENT = "PROPERTY_MSG_SELECTED_SOIL_CHEMICAL_CONTENT";
    private static final String PROPERTY_MSG_SELECTED_SOIL_DENSITY = "PROPERTY_MSG_SELECTED_SOIL_DENSITY";
    private static final String PROPERTY_MSG_SELECTED_SOIL_STOCK = "PROPERTY_MSG_SELECTED_SOIL_STOCK";
    private static final String PROPERTY_MSG_SELECTED_SOIL_COARSE = "PROPERTY_MSG_SELECTED_SOIL_COARSE";
    private static final String PROPERTY_MSG_SELECTED_SOIL_TEXTURE = "PROPERTY_MSG_SELECTED_SOIL_TEXTURE";
    private String contentChemistryDatatype;
    private String soilDensityDatatype;
    private String soilStockDatatype;
    private String soilCoarseDatatype;
    private String soilTextureDatatype;

    /**
     * Builds the output data.
     *
     * @param requestMetadatasMap
     * @param outputPrintStreamMap
     * @link(IParameter) the parameters
     * @link(Map<String,PrintStream>) the output print stream map @see
     * org.inra.ecoinfo.acbb.extraction.mps.impl. MPSOutputsBuildersResolver#
     * buildOutputData (org.inra.ecoinfo.extraction.IParameter, java.util.Map)
     */
    protected void buildOutputData(final Map<String, Object> requestMetadatasMap,
            final Map<String, PrintStream> outputPrintStreamMap) {
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            final PrintStream outputStream = outputPrintStreamMap.get(datatypeNameEntry.getKey());
            List<Variable> selectedSoilChemicalVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.contentChemistryDatatype), new ArrayList());
            if (selectedSoilChemicalVariables != null) {
                selectedSoilChemicalVariables.addAll((List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat("extra_variables"), new ArrayList()));
            }
            final List<Variable> selectedSoilDensityVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.soilDensityDatatype), new ArrayList());
            final List<Variable> selectedSoilCoarseVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.soilCoarseDatatype), new ArrayList());
            final List<Variable> selectedSoilStockVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.soilStockDatatype), new ArrayList());
            final TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods
                    = ((TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>) requestMetadatasMap.getOrDefault(MethodeSoilTexture.class.getSimpleName(), new TreeMap()));
            buildOutputHeader(outputStream);
            final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            this.outputPrintDates(outputStream, datesYearsContinuousFormParamVO);
            this.outputPrintTraitements(outputStream, getTreatments(requestMetadatasMap));
            outputStream.println(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_REMINDER_SOURCE_PATH,
                    PROPERTY_MSG_SELECTED_VARIABLES));
            StringBuilder variablesReminder = new StringBuilder("");
            this.outputPrintVariables(outputStream,
                    SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_CHEMICAL_CONTENT,
                    selectedSoilChemicalVariables);
            appendVariables(variablesReminder, SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_CHEMICAL_CONTENT, selectedSoilChemicalVariables);
            this.outputPrintVariables(outputStream,
                    SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_DENSITY,
                    selectedSoilDensityVariables);
            appendVariables(variablesReminder, SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_DENSITY, selectedSoilDensityVariables);
            this.outputPrintVariables(outputStream,
                    SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_STOCK,
                    selectedSoilStockVariables);
            appendVariables(variablesReminder, SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_STOCK, selectedSoilStockVariables);
            this.outputPrintVariables(outputStream,
                    SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_COARSE,
                    selectedSoilCoarseVariables);
            appendVariables(variablesReminder, SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_COARSE, selectedSoilCoarseVariables);
            MDC.put("variables", variablesReminder.toString());
            this.outputPrintMethodes(outputStream,
                    SoilRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SOIL_TEXTURE,
                    selectedMethods);
            final Set<FileComp> selectedFileComps = (Set<FileComp>) requestMetadatasMap
                    .getOrDefault(FileComp.class.getSimpleName(), new TreeSet<>());
            this.outputPrintFileComp(outputStream, selectedFileComps);
            this.outputPrintCommentaire(outputStream, getCommentaire(requestMetadatasMap));
        }
    }

    private void appendVariables(StringBuilder variablesReminder, String variableListName, List<Variable> selectedVariables) {
        variablesReminder.append(
                String.format("%s:( %s)",
                        RecorderACBB.getACBBMessageWithBundle(getLocalBundle(), variableListName),
                        selectedVariables.stream()
                                .map(v -> v.getAffichage())
                                .collect(Collectors.joining(", "))));
    }

    void outputPrintMethodes(final PrintStream outputStream, String title,
            final TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedVariablesByMethode) {
        if (selectedVariablesByMethode.isEmpty()) {
            return;
        }
        final Properties propertiesNomVariable = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        outputStream.println(RecorderACBB.getACBBMessageWithBundle(SoilRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, title));
        StringBuilder methodsReminder = new StringBuilder("");
        selectedVariablesByMethode.entrySet().stream()
                .forEach(e -> {
                    outputStream.printf("\t\t%s (%s)", e.getKey().getLibelle(), e.getKey().getNumberId());
                    methodsReminder.append(String.format("\t\t%s (%s) :[", e.getKey().getLibelle(), e.getKey().getNumberId()));
                    outputStream.println();
                    e.getValue().forEach((variable) -> {
                        String localizedNom = propertiesNomVariable.getProperty(variable.getName());
                        outputStream.printf("\t\t\t%s (%s)", variable.getAffichage(),
                                Strings.isNullOrEmpty(localizedNom) ? variable.getName() : localizedNom);
                        methodsReminder.append(String.format("\t\t\t%s (%s)", variable.getAffichage(),
                                Strings.isNullOrEmpty(localizedNom) ? variable.getName() : localizedNom));
                        outputStream.println();
                        methodsReminder.append("]%n");
                    });
                });
        MDC.put("methodes", methodsReminder.toString());
        outputStream.println();
        outputStream.println();
    }

    /**
     * @param chemicalContentDatatype
     */
    public void setChemicalContentDatatype(String chemicalContentDatatype) {
        this.contentChemistryDatatype = chemicalContentDatatype;
    }

    /**
     * @param soilStockDatatype
     */
    public void setSoilStockDatatype(String soilStockDatatype) {
        this.soilStockDatatype = soilStockDatatype;
    }

    /**
     * @param soilCoarseDatatype
     */
    public void setSoilCoarseDatatype(String soilCoarseDatatype) {
        this.soilCoarseDatatype = soilCoarseDatatype;
    }

    /**
     * @param soilTextureDatatype
     */
    public void setSoilTextureDatatype(String soilTextureDatatype) {
        this.soilTextureDatatype = soilTextureDatatype;
    }

    /**
     * @param soilDensityDatatype
     */
    public void setSoilDensityDatatype(String soilDensityDatatype) {
        this.soilDensityDatatype = soilDensityDatatype;
    }

    @Override
    protected String getLocalBundle() {
        return BUNDLE_SOURCE_PATH;
    }

}
