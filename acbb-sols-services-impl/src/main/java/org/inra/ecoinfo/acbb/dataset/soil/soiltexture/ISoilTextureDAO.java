package org.inra.ecoinfo.acbb.dataset.soil.soiltexture;

import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.SoilTexture;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface ISoilTextureDAO extends ISoilDao<SoilTexture> {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     */
    Optional<SoilTexture> getByNKey(final VersionFile version, Parcelle parcelle, final LocalDate date);
}
