package org.inra.ecoinfo.acbb.extraction.soil.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.soil.ISoilDao;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;

/**
 * @param <T>
 * @author ptcherniati
 */
public class JPASoilDAO<T extends AbstractSoil> extends AbstractJPADAO<T> implements ISoilDao<T> {

    /**
     *
     **/
    public JPASoilDAO() {
        super();
    }
}
