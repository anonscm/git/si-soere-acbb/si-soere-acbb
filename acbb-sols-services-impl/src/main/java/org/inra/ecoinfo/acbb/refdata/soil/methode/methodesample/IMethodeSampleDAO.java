/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesample;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeSampleDAO extends IMethodeDAO<Methodesample> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<Methodesample> getAll();
}
