package org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity.ValeurChemistry;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilExtractor;

/**
 * @author ptcherniati
 */
public class ContentChemistryExtractor extends AbstractSoilExtractor<ValeurChemistry> {

}
