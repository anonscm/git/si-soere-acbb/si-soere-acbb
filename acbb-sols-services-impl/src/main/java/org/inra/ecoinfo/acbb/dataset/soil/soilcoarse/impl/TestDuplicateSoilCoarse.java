/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.impl;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class TestDuplicateAI.
 *
 * test if the file has duplicates lines
 */
public class TestDuplicateSoilCoarse extends AbstractTestDuplicate {

    /**
     * The Constant ACBB_DATASET_AI_BUNDLE_NAME.
     */
    public static final String ACBB_DATASET_CONTENT_CHEMISTRY_BUNDLE_NAME = TestDuplicateSoilCoarse.class.getPackage().getName() + ".messages";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE.
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The lines.
     */
    final SortedMap<String, Long> lines;

    /**
     * Instantiates a new test duplicate ai.
     */
    public TestDuplicateSoilCoarse() {
        this.lines = new TreeMap();
    }

    /**
     * Adds the lines.
     *
     * @param parcelle
     * @param layerSup
     * @param methodeSample
     * @param layerInf
     * @link{String the parcelle
     * @param date
     * @link{String the date
     * @param lineNumber
     * @link{long the line number
     */
    protected void addLine(final String parcelle, String layerSup, String layerInf, final String date, String methodeSample, final long lineNumber) {
        final String key = this.getKey(parcelle, layerSup, layerInf, date, methodeSample);
        if (!this.lines.containsKey(key)) {
            this.lines.put(key, lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateSoilCoarse.ACBB_DATASET_CONTENT_CHEMISTRY_BUNDLE_NAME,
                    TestDuplicateSoilCoarse.PROPERTY_MSG_DOUBLON_LINE), lineNumber,
                    parcelle, date, this.lines.get(key)));
        }

    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[5], values[6], values[8], values[11], lineNumber);
    }
}
