/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class AnalyseMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "methode_sol_analyse";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return AnalyseMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
