/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.soil;

import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil_;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil_;
import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock_;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.*;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.synthesis.soiltexture.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.soiltexture.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author tcherniatinsky
 */
public class SoilTextureSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurSoilTexture> v = query.from(ValeurSoilTexture.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<ValeurSoilTexture, MesureSoilTexture> m = v.join(ValeurSoilTexture_.mesureSoilTexture);
        Join<MesureSoilTexture, SoilTexture> s = m.join(MesureSoilTexture_.texture);
        final Join<ValeurSoilTexture, RealNode> varRn = v.join(ValeurSoil_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurSoilStock_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(AbstractSoil_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        Path<String> parcelleCode = s.join(AbstractSoil_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        Path<String> methodeLibelle = s.join(SoilTexture_.methodesample).get(AbstractMethode_.libelle);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                dateMesure,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                methodeLibelle,
                                builder.avg(valeur),
                                idNode
                        )
                )
                .where(
                        builder.equal(node.get(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(
                        methodeLibelle,
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);
    }

}
