/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soil.soiltexture.jpa;

import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil_;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.*;
import org.inra.ecoinfo.acbb.extraction.soil.jpa.AbstractJPASoilDAO;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode_;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture_;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeTextureComposant;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeTextureComposant_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB_;
import org.inra.ecoinfo.acbb.synthesis.soiltexture.SynthesisValue;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPASoilTextureExtractionDAO extends
        AbstractJPASoilDAO<SoilTexture, ValeurSoilTexture> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurSoilTexture> getValeurSoilClass() {
        return ValeurSoilTexture.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurSoilTexture, SoilTexture> getMesureAttribute() {
        return null;
    }

    /**
     * @return
     */
    @Override
    protected Class<SoilTexture> getSoilClass() {
        return SoilTexture.class;
    }

    public long getSize(List<TraitementProgramme> selectedTraitementProgrammes, TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods, List<IntervalDate> intervalsDates, IUser user) {
        CriteriaQuery<Long> criteria = getQuery(intervalsDates, user, selectedMethods, selectedTraitementProgrammes, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;
    }

    public List<ValeurSoilTexture> extractSoilTextureValues(List<TraitementProgramme> selectedTraitementProgrammes, TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods, List<IntervalDate> intervals, IUser user) {
        CriteriaQuery query = getQuery(intervals, user, selectedMethods, selectedTraitementProgrammes, false);
        return getResultList(query);
    }

    private CriteriaQuery getQuery(List<IntervalDate> intervals, IUser user, TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods, List<TraitementProgramme> selectedTraitementProgrammes, boolean isCount) throws DateTimeParseException {
        CriteriaQuery query;
        if (isCount) {
            query = builder.createQuery(Long.class);
        } else {
            query = builder.createQuery(getValeurSoilClass());
        }
        Root<ValeurSoilTexture> v = query.from(ValeurSoilTexture.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<ValeurSoilTexture, MesureSoilTexture> m = v.join(ValeurSoilTexture_.mesureSoilTexture);
        Join<MesureSoilTexture, SoilTexture> s = m.join(MesureSoilTexture_.texture);
        Join<SoilTexture, SuiviParcelle> suiviParcelle = s.join(SoilTexture_.suiviParcelle);
        Join<SuiviParcelle, TraitementProgramme> traitement = suiviParcelle.join(SuiviParcelle_.traitement);
        Join<ValeurSoilTexture, RealNode> rn = v.join(ValeurSoilTexture_.realNode);
        Join<ValeurSoilTexture, AbstractMethode> method = v.join(ValeurSoilTexture_.methode);
        Join<RealNode, Nodeable> dvuNodeable = rn.join(RealNode_.nodeable);
        List<Predicate> and = new LinkedList();
        List<Predicate> or = new LinkedList();
        Root<NodeDataSet> nds = null;
        for (IntervalDate intervalDate : intervals) {
            List<Predicate> andForDate = new LinkedList();
            LocalDate minDate, maxDate;
            minDate = intervalDate.getBeginDate().toLocalDate();
            maxDate = intervalDate.getEndDate().toLocalDate();
            andForDate.add(builder.between(s.get(AbstractSoil_.date), minDate, maxDate));
            if (!(isCount || user.getIsRoot())) {
                nds = nds == null ? query.from(NodeDataSet.class) : nds;
                andForDate.add(builder.equal(nds.join(NodeDataSet_.realNode), rn));
                addRestrictiveRequestOnRoles(user, query, andForDate, builder, nds, s.get(AbstractSoil_.date));
            }
            or.add(builder.and(andForDate.toArray(new Predicate[0])));
        }
        and.add(builder.or(or.toArray(new Predicate[0])));
        and.add(method.in(selectedMethods.keySet()));
        and.add(builder.equal(dvu.get(Nodeable_.code), dvuNodeable.get(Nodeable_.code)));
        and.add(traitement.in(selectedTraitementProgrammes));
        query
                .where(builder.and(and.toArray(new Predicate[0])))
                //.orderBy(o)
                .distinct(true);
        if (isCount) {
            query.select(builder.countDistinct(v));
        } else {
            query.select(v);
        }
        return query;
    }

    @Override
    public List<NodeDataSet> getAvailableVariableByTraitement(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervalsDate, IUser user) {
        return new LinkedList();
    }

    public Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> getAvailableVariableByTraitementAndMethod(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervalsDate, IUser user) {
        String query = "with nodes as (select distinct nds.*, cast(unnest(regexp_split_to_array(sv.methods,',')) as integer) from soiltexturesynthesisvalue sv\n"
                + "join realnode rn   on split_part(rn.path,',',1)||','||split_part(rn.path,',',2)=sv.site and  split_part(rn.path,';',2)=sv.variable\n"
                + "join datatype_variable_unite_acbb_dvu dvu on dvu.vdt_id=rn.id_nodeable\n"
                + "join site s on s.name = split_part(rn.path,',',2)\n"
                + "join site_acbb_sit sit ON sit.id = s.site_id\n"
                + "join composite_node_data_set nds ON nds.realnode = rn.id \n"
                + "join parcelle_par par on par.nom = split_part(sv.parcellecode,'_',2) and par.id=s.site_id\n"
                + "join suivi_parcelle_spa spa ON spa.par_id = par.par_id and tra_id in (%s) \n"
                + " and (sv.date BETWEEN '%s' and '%s'))\n"
                + " select branch_node_id, method_id from nodes \n"
                + " join methode_sol_texture m on m.number_id=nodes.unnest";
        query = String.format(query,
                selectedTraitements.stream()
                        .map(t -> t.getId().toString())
                        .collect(Collectors.joining(",")),
                DateUtil.getUTCDateTextFromLocalDateTime(intervalsDate.get(0).getBeginDate(), DateUtil.YYYY_MM_DD_FILE),
                DateUtil.getUTCDateTextFromLocalDateTime(intervalsDate.get(0).getEndDate(), DateUtil.YYYY_MM_DD_FILE)
        );
        List<Object[]> list = (List<Object[]>) entityManager.createNativeQuery(query).getResultList();
        CriteriaQuery<NodeDataSet> queryNode = builder.createQuery(NodeDataSet.class);
        Root<NodeDataSet> node = queryNode.from(NodeDataSet.class);
        queryNode
                .select(node);
        Map<Long, NodeDataSet> nodes = getResultAsStream(queryNode).collect(Collectors.toMap(k -> k.getId(), k -> k));
        CriteriaQuery<MethodeSoilTexture> queryMethode = builder.createQuery(MethodeSoilTexture.class);
        Root<MethodeSoilTexture> methode = queryMethode.from(MethodeSoilTexture.class);
        queryMethode
                .select(methode);
        Map<Long, MethodeSoilTexture> methods = getResultAsStream(queryMethode).collect(Collectors.toMap(k -> k.getId(), k -> k));
        Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> nodesByMethods = new HashMap<>();
        list.stream()
                .forEach(o -> addLine(nodesByMethods, nodes, methods, o));
        return nodesByMethods;
    }

    private void addLine(Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> nodesByMethods, Map<Long, NodeDataSet> nodes, Map<Long, MethodeSoilTexture> methods, Object[] o) {
        final MethodeSoilTexture method = methods.get(((BigInteger) o[1]).longValue());
        final NodeDataSet node = nodes.get(((BigInteger) o[0]).longValue());
        nodesByMethods
                .computeIfAbsent(method, k -> new HashMap<VariableACBB, Set<NodeDataSet>>())
                .computeIfAbsent((VariableACBB) ((DatatypeVariableUniteACBB) node.getNodeable()).getVariable(), k -> new HashSet<>())
                .add(node);
    }

    public TreeMap<MethodeSoilTexture, Set<VariableACBB>> getVariablesTextureByMethode(String[] methodeNumbers) {
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);
        Root<MethodeSoilTexture> method = query.from(MethodeSoilTexture.class);
        ListJoin<MethodeSoilTexture, MethodeTextureComposant> listComposants = method.join(MethodeSoilTexture_.composante);
        Root<VariableACBB> variable = query.from(VariableACBB.class);
        query
                .multiselect(method.alias("method"), variable.alias("variable"))
                .distinct(true)
                .where(
                        builder.and(
                                builder.equal(variable.get(VariableACBB_.code), builder.lower(listComposants.get(MethodeTextureComposant_.valeur))),
                                method.get(AbstractMethode_.numberId).in(methodeNumbers)
                        )
                );
        TreeMap<MethodeSoilTexture, Set<VariableACBB>> result = new TreeMap<>();
        getResultAsStream(query)
                .forEach(
                        e -> result
                                .computeIfAbsent(e.get("method", MethodeSoilTexture.class), k -> new TreeSet<>())
                                .add(e.get("variable", VariableACBB.class))
                );
        return result;

    }
}
