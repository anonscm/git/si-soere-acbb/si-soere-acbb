package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.ISoilCoarseDAO;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.SoilCoarse;
import org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity.ValeurSoilCoarse;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildSoilCoarseHelper extends AbstractBuildHelper<SoilCoarseLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = BuildSoilCoarseHelper.class.getPackage().getName() + ".messages";

    Map<Parcelle, Map<LocalDate, Map<Long, Map<Float, Map<Float, SoilCoarse>>>>> soilCoarse = new HashMap();
    ISoilCoarseDAO SoilCoarseDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursSoilCoarse(final SoilCoarseLineRecord line,
                              final SoilCoarse soilcoarse) {
        for (final VariableValueSoilCoarse variableValue : line.getVariablesValues()) {
            final DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
            RealNode realNodeElement = getRealNodeForSequenceAndVariable(dvu);
            ValeurSoilCoarse valeurSoilCoarse;
            valeurSoilCoarse = new ValeurSoilCoarse(
                    soilcoarse,
                    Float.parseFloat(variableValue.getValue()),
                    realNodeElement, variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(),
                    variableValue.getQualityClass(),
                    variableValue.getMethode());
            soilcoarse.getValeurs().add(valeurSoilCoarse);
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void build() throws PersistenceException {
        for (final SoilCoarseLineRecord line : this.lines) {
            final SoilCoarse SoilCoarse = this.getOrCreateSoilCoarse(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursSoilCoarse(line, SoilCoarse);
            this.SoilCoarseDAO.saveOrUpdate(SoilCoarse);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesSoil
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesSoil the request properties Soil
     */
    public void build(final VersionFile version, final List<SoilCoarseLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesSoil requestPropertiesSoil) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesSoil);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    SoilCoarse createNewSoilCoarse(final SoilCoarseLineRecord line) {
        SoilCoarse SoilCoarse = new SoilCoarse(
                this.version,
                line.getDate(),
                line.getRotationNumber(),
                line.getAnneeNumber(),
                line.getSuiviParcelle(),
                line.getCampaignName(),
                line.getLayerName(),
                line.getLayerSup(),
                line.getLayerInf(),
                line.getMethodeSample()
        );
        SoilCoarse.setObservation(line.getObservation());
        try {
            this.SoilCoarseDAO.saveOrUpdate(SoilCoarse);
        } catch (final PersistenceException e) {
            this.errorsReport.addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return SoilCoarse;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    SoilCoarse getOrCreateSoilCoarse(final SoilCoarseLineRecord line) {
        return soilCoarse
                .computeIfAbsent(line.getParcelle(), kp -> new HashMap<LocalDate, Map<Long, Map<Float, Map<Float, SoilCoarse>>>>())
                .computeIfAbsent(line.getDate(), kd -> new HashMap<Long, Map<Float, Map<Float, SoilCoarse>>>())
                .computeIfAbsent(line.getMethodeSample().getId(), km -> new HashMap<Float, Map<Float, SoilCoarse>>())
                .computeIfAbsent(line.getLayerInf(), ki -> new HashMap<Float, SoilCoarse>())
                .computeIfAbsent(line.getLayerSup(), ks -> createNewSoilCoarse(line));
    }

    /**
     * @param biomassProductionDAO the Biomass Production DAO to set
     */
    public void setSoilCoarseDAO(ISoilCoarseDAO biomassProductionDAO) {
        this.SoilCoarseDAO = biomassProductionDAO;
    }
}
