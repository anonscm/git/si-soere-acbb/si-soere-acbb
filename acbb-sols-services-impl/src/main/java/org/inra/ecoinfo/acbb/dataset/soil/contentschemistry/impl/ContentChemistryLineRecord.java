package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.impl;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractSoilLineRecord;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class ContentChemistryLineRecord extends AbstractSoilLineRecord<ContentChemistryLineRecord, VariableValueContentChemistry> {

    /**
     * @param originalLineNumber
     */
    public ContentChemistryLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }
}
