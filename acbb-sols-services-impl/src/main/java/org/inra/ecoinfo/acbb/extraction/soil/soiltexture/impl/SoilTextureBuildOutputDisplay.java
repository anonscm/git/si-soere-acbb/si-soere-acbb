package org.inra.ecoinfo.acbb.extraction.soil.soiltexture.impl;

import org.apache.commons.lang.WordUtils;
import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.MesureSoilTexture;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.SoilTexture;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity.ValeurSoilTexture;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilOutputBuilder;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class SoilTextureBuildOutputDisplay
        extends
        AbstractSoilOutputBuilder<SoilTexture, ValeurSoilTexture, MesureSoilTexture> {

    /**
     *
     */
    public static final String SOIL_TEXTURES = "soil_textures";
    /**
     *
     */
    protected static final String FIRST_VARIABLE = "mav";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static protected final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.soil.soiltexture.messages";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public SoilTextureBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        return super.buildOutput(parameters);
    }

    protected void addValuesColumns(final SortedMap<String, Variable> selectedVariablesAffichage, final ErrorsReport errorsReport, final List<Variable> selectedVariables, final Map<String, Object> requestMetadatasMap, List<ListeSoil> valueColumns, final StringBuilder stringBuilder1, final StringBuilder stringBuilder2, final StringBuilder stringBuilder3, final Properties propertiesVariableName, final Properties propertiesUniteName, final Properties propertiesValeurQualitativeValeur) throws PersistenceException {
        TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods = (TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>) requestMetadatasMap.get(MethodeSoilTexture.class.getSimpleName());
        for (Map.Entry<MethodeSoilTexture, SortedSet<VariableACBB>> methodeEntry : selectedMethods.entrySet()) {
            MethodeSoilTexture method = methodeEntry.getKey();
            SortedSet<VariableACBB> variables = methodeEntry.getValue();
            for (VariableACBB variable : variables) {
                if (variable == null) {
                    errorsReport
                            .addErrorMessage(String
                                    .format(this
                                                    .getLocalizationManager()
                                                    .getMessage(
                                                            AbstractSoilOutputBuilder.BUNDLE_SOURCE_PATH,
                                                            AbstractSoilOutputBuilder.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                            selectedVariables));
                    continue;
                }
                for (Map.Entry<String, String> patternEntry : this.columnPatterns.entrySet()) {
                    String columnType = patternEntry.getKey();
                    this.addColumnName(variable, method, columnType, valueColumns, stringBuilder1,
                            stringBuilder2, stringBuilder3, propertiesVariableName,
                            propertiesUniteName, propertiesValeurQualitativeValeur);
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        }
    }

    protected void addColumnName(Variable variable, MethodeSoilTexture method, String columnType,
                                 List<ListeSoil> valueColumns, StringBuilder stringBuilder1,
                                 StringBuilder stringBuilder2, StringBuilder stringBuilder3,
                                 Properties propertiesVariableName, Properties propertiesUniteName,
                                 Properties propertiesValeurQualitativeValeur) throws PersistenceException {
        String localizedVariableName = propertiesVariableName.getProperty(variable.getName(), variable.getName());
        Optional<Unite> unite = this.datatypeVariableUniteACBBDAO.getUnite(this.getExtractCode(), variable.getCode());
        for (ListeSoil valueColumn : valueColumns) {
            if (valueColumn.getValeur().equals(columnType)) {
                String columnDefinition = propertiesValeurQualitativeValeur.getProperty(valueColumn.getValeur(), valueColumn.getValeur());
                String columnName = org.apache.commons.lang.StringUtils.EMPTY;
                if (columnType.startsWith(AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE.toLowerCase())) {
                    columnName = WordUtils.capitalize(columnType.replaceAll("_([^_]*)$", "_" + "pour_" + variable.getAffichage() + "_$1"));
                } else {
                    columnName = String.format("%s_%s (%s)", variable.getAffichage(), columnType, method.getNumberId());
                }
                String uniteName = org.apache.commons.lang.StringUtils.EMPTY;
                if (this.variableColumnTypes.stream()
                        .anyMatch(vt -> columnType.endsWith(vt))) {
                    uniteName = propertiesUniteName.getProperty(unite.map(u -> u.getName()).orElseGet(String::new), unite.map(u -> u.getName()).orElseGet(String::new));
                }
                columnDefinition = String.format(AbstractSoilOutputBuilder.PATTERN_CSV_VARIABLE_FIELD, localizedVariableName, columnDefinition);
                stringBuilder1.append(columnDefinition);
                stringBuilder2.append(String.format(ACBBMessages.PATTERN_1_FIELD, uniteName));
                stringBuilder3.append(String.format(ACBBMessages.PATTERN_1_FIELD, columnName));
                return;
            }
        }
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return SoilTextureBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedVariables
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedVariables,
            PrintStream rawDataPrintStream) {
        return new SoilTextureOutputHelper(rawDataPrintStream, selectedVariables);
    }

    /**
     *
     */
    protected class SoilTextureOutputHelper extends OutputHelper<SequenceMesureValues> {

        /**
         *
         */
        protected static final String PATTERN_11_FIELD = ";%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_5_FIELD = ";%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_3_FIELD = ";%s;%s;%s";

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         */
        protected SoilTextureOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurSoil
         */
        @Override
        protected void addVariablesColumns(ValeurSoilTexture valeurSoil) {
            String line = String.format(SoilTextureOutputHelper.PATTERN_5_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getStandardDeviation()),
                    valeurSoil.getMethode().getNumberId(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurSoil.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesColumnsForNullVariable() {
            this.rawDataPrintStream.print(SoilTextureOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param isHUT
         */
        protected void addVariablesColumnsForNullVariable(boolean isHUT) {
            this.rawDataPrintStream.print(SoilTextureOutputHelper.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            if (isHUT) {
                this.rawDataPrintStream.print(SoilTextureOutputHelper.PATTERN_3_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
            }
        }

        /**
         * @param SoilTexture
         * @param requestMap
         */
        @Override
        protected void printLineSpecific(SoilTexture SoilTexture, Map<String, Object> requestMap) {
            AbstractMethode methodesample = SoilTexture.getMethodesample();
            String line = String.format(";%s",
                    methodesample == null ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : methodesample.getNumberId());
            if (methodesample != null) {
                addMethode(methodesample, requestMap);
            }
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected MesureSoilTexture getMesure(ValeurSoilTexture valeur) {
            return valeur.getMesureSoilTexture();
        }

        /**
         * @param valeur
         * @return
         */
        @Override
        protected SoilTexture getSequence(ValeurSoilTexture valeur) {
            return valeur.getMesureSoilTexture().getTexture();
        }

        /**
         * @param value
         * @param requestMap
         */
        @Override
        protected void printValue(SequenceMesureValues value, Map<String, Object> requestMap) {
            TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>> selectedMethods = (TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>) requestMap.get(MethodeSoilTexture.class.getSimpleName());
            selectedMethods.forEach((methode, variables) -> {
                variables.stream().forEach((variable) -> {
                    String variableAffichage = variable.getAffichage();
                    boolean isPrinted = false;
                    for (Map.Entry<MesureSoilTexture, List<ValeurSoilTexture>> mesureEntry : value.getMesures().entrySet()) {
                        List<ValeurSoilTexture> valeurs = mesureEntry.getValue();
                        MesureSoilTexture mesure = mesureEntry.getKey();
                        Map<Boolean, List<ValeurSoilTexture>> collect = valeurs.stream()
                                .collect(Collectors.partitioningBy(
                                        v -> variableAffichage.equals(((DatatypeVariableUniteACBB) v.getRealNode().getNodeable()).getVariable().getAffichage()) &&
                                                v.getMethode().equals(methode))
                                );
                        if (!collect.get(true).isEmpty()) {
                            ValeurSoilTexture valeurSoil = collect.get(true).get(0);
                            addVariablesColumns(valeurSoil);
//                            System.out.println(String.format("%s -> %s:%s, %s:%s", 
//                                    valeurSoil.getMesureSoilTexture().getTexture().getDate(), 
//                                    valeurSoil.getMethode().getNumberId(),
//                                    methode.getNumberId(),
//                                    variableAffichage,
//                                    ((DatatypeVariableUniteACBB) valeurSoil.getRealNode().getNodeable()).getVariable().getAffichage()));

                            this.addMethode(valeurSoil.getMethode(), requestMap);
                            isPrinted = true;
                            break;
                        }
                    }
                    if (!isPrinted) {
//                            System.out.println(String.format("%s -> %s:%s, %s:%s", 
//                                    value.getMesures().keySet().stream().findFirst().get().getTexture().getDate(), 
//                                    null,
//                                    methode.getNumberId(),
//                                    variableAffichage,
//                                    null));
                        addVariablesColumnsForNullVariable(false);
                    }
                });
            });
        }

        @Override
        protected SequenceMesureValues newValues(SoilTexture sequence, MesureSoilTexture mesure, ValeurSoilTexture valeur) {
            return new SequenceMesureValues(sequence, mesure, valeur);
        }
    }

    public class ComparatorWithFirstStringThenAlpha implements Comparator<String> {

        private final Object firstString;

        /**
         * @param firstString
         */
        public ComparatorWithFirstStringThenAlpha(Object firstString) {
            this.firstString = firstString;
        }

        @Override
        public int compare(String o1, String o2) {
            if (o1.equals(o2)) {
                return 0;
            } else if (o1.equals(firstString)) {
                return -1;
            } else if (o2.equals(firstString)) {
                return 1;
            } else {
                return o1.compareTo(o2);
            }
        }
    }
}
