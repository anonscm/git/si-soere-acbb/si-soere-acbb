/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.soil.impl.RequestPropertiesSoil;
import org.inra.ecoinfo.acbb.dataset.soil.soiltexture.IRequestPropertiesSoilTexture;

import java.io.Serializable;

/**
 * The Class RequestPropertiesAI.
 * <p>
 * record the general information of the file
 */
public class RequestPropertiesSoilTexture extends RequestPropertiesSoil implements
        IRequestPropertiesSoilTexture, Serializable {

    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new request properties ai.
     */
    public RequestPropertiesSoilTexture() {
        super();
    }

    /**
     * @return @see
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getTestDuplicates()
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateSoilTexture();
    }
}
