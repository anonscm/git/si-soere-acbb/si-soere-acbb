package org.inra.ecoinfo.acbb.extraction.soil.soilstock.impl;

import org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity.ValeurSoilStock;
import org.inra.ecoinfo.acbb.extraction.soil.impl.AbstractSoilExtractor;

/**
 * @author ptcherniati
 */
public class SoilStockExtractor extends AbstractSoilExtractor<ValeurSoilStock> {

}
