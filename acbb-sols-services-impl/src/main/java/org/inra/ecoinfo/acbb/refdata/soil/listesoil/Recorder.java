/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.listesoil;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * The Class Recorder.
 */

/**
 * @author koyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<ListeSoil> {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    IListeSoilDAO listeSoilDAO;

    /**
     * The properties nom fr @link(Properties).
     */
    Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * The properties nom fr @link(Properties).
     */
    Properties propertiesValeurFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesValeurEN;

    /**
     * Instantiates a new recorder.
     */
    public Recorder() {
        super();
    }

    void createOrUpdateListeSoil(final String nom, final String valeur, final ListeSoil dbListeSoil) throws PersistenceException {
        ListeSoil listeSoilToUpdate = dbListeSoil;
        if (dbListeSoil == null) {
            listeSoilToUpdate = new ListeSoil(nom, valeur);
        }
        try {
            this.listeSoilDAO.saveOrUpdate(listeSoilToUpdate);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String code = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                this.listeSoilDAO.remove(this.listeSoilDAO.getByNKey(code, valeur)
                        .orElseThrow(() -> new BusinessException("bad list element")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ListeSoil> getAllElements() {
        List<ListeSoil> listeSoils = this.listeSoilDAO.getAll();
        Collections.sort(listeSoils);
        return listeSoils;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param listevegetation
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException                                the persistence exception @see
     *                                                             org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata
     *                                                             (java.lang.Object)
     * @link(ListeSoil) the liste itineraire
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final ListeSoil listevegetation)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listevegetation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : listevegetation.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listevegetation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.propertiesNomFR.getProperty(listevegetation.getCode(), listevegetation.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listevegetation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.propertiesNomEN.getProperty(listevegetation.getCode(), listevegetation.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listevegetation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : listevegetation.getValeur(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listevegetation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.propertiesValeurFR.getProperty(listevegetation.getValeur(), listevegetation.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listevegetation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.propertiesValeurEN.getProperty(listevegetation.getValeur(), listevegetation.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<ListeSoil> initModelGridMetadata() {
        this.propertiesNomFR = this.localizationManager.newProperties(ListeACBB.NAME_ENTITY_JPA,
                ListeACBB.ATTRIBUTE_JPA_CODE, Locale.FRANCE);
        this.propertiesNomEN = this.localizationManager.newProperties(ListeACBB.NAME_ENTITY_JPA,
                ListeACBB.ATTRIBUTE_JPA_CODE, Locale.ENGLISH);
        this.propertiesValeurFR = this.localizationManager.newProperties(ListeACBB.NAME_ENTITY_JPA,
                ListeACBB.ATTRIBUTE_JPA_VALEUR, Locale.FRANCE);
        this.propertiesValeurEN = this.localizationManager.newProperties(ListeACBB.NAME_ENTITY_JPA,
                ListeACBB.ATTRIBUTE_JPA_VALEUR, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist liste itineraire.
     *
     * @param errorsReport
     * @param nom
     * @param valeur
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code
     * @link(String) the nom
     * @link(ErrorsReport) the errors report
     * @link(String) the code
     * @link(String) the nom
     */
    void persistListeSoil(final ErrorsReport errorsReport, final String nom, final String valeur)
            throws PersistenceException {
        final ListeSoil dblistListeSoil = this.listeSoilDAO.getByNKey(nom, valeur).orElse(null);
        this.createOrUpdateListeSoil(nom, valeur, dblistListeSoil);
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ListeACBB.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String nom = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                this.persistListeSoil(errorsReport, nom, valeur);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param listeVegetationDAO
     */
    public void setListeSoilDAO(IListeSoilDAO listeVegetationDAO) {
        this.listeSoilDAO = listeVegetationDAO;
    }
}
