package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesample;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample.Methodesample;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * The Class Recorder.
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractMethodeRecorder<Methodesample> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodesample.messages";
    Properties prelevementProperties;
    Properties assemblageProperties;
    Properties preparationProperties;
    private IMethodeSampleDAO methodeSampleDAO;

    /**
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String numberIdString = tokenizerValues.nextToken();
                ErrorsReport errorsReport = new ErrorsReport();
                Methodesample methodeHauteurVegetal = this.getByNumberId(numberIdString,
                        errorsReport)
                        .orElseThrow(() -> new BusinessException("bad method"));
                this.methodeSampleDAO.remove(methodeHauteurVegetal);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param allMethods
     * @return
     */
    @Override
    protected Set<Methodesample> filter(Set<AbstractMethode> allMethods) {
        Set<Methodesample> methodes = new HashSet();
        for (AbstractMethode methode : allMethods) {
            if (methode instanceof Methodesample) {
                methodes.add((Methodesample) methode);
            }
        }
        return methodes;
    }

    /**
     * @return
     */
    @Override
    protected List<Methodesample> getAllElements() {

        List<Methodesample> all = this.methodeSampleDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeDefinitionName() {
        return AbstractMethode.ATTRIBUTE_JPA_DEFINITION;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeEntityName() {
        return Methodesample.NAME_ENTITY_JPA;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeLibelleName() {
        return AbstractMethode.ATTRIBUTE_JPA_LIBELLE;
    }

    /**
     * @param methodeAnalyse
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final Methodesample methodeAnalyse) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = super
                .getNewLineModelGridMetadata(methodeAnalyse);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeAnalyse != null ? methodeAnalyse.getPrelevement()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeAnalyse != null ? getSanitizedPropertyValue(prelevementProperties, methodeAnalyse.getPrelevement())
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeAnalyse != null ? methodeAnalyse.getAssemblage()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeAnalyse != null ? getSanitizedPropertyValue(assemblageProperties, methodeAnalyse.getAssemblage())
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeAnalyse != null ? methodeAnalyse.getPreparation()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeAnalyse != null ? getSanitizedPropertyValue(preparationProperties, methodeAnalyse.getPreparation())
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, true));

        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Methodesample> initModelGridMetadata() {
        this.initProperties();
        prelevementProperties = localizationManager.newProperties(Methodesample.NAME_ENTITY_JPA, Methodesample.ATRRIBUTE_PRELEVEMENT, Locale.ENGLISH);
        assemblageProperties = localizationManager.newProperties(Methodesample.NAME_ENTITY_JPA, Methodesample.ATRRIBUTE_ASSEMBLAGE, Locale.ENGLISH);
        preparationProperties = localizationManager.newProperties(Methodesample.NAME_ENTITY_JPA, Methodesample.ATRRIBUTE_PREPARATION, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(
                        values, Methodesample.NAME_ENTITY_JPA);
                final Long numberId = this.getNumberId(errorsReport, lineNumber, tokenizerValues);
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String definition = this.getDefinition(errorsReport, lineNumber, tokenizerValues);
                final String prelevement = tokenizerValues.nextToken();
                final String assemblage = tokenizerValues.nextToken();
                final String preparation = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMethodesample(errorsReport, numberId, libelle,
                            definition, prelevement, assemblage, preparation);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMethodesample(
            AbstractCSVMetadataRecorder.ErrorsReport errorsReport, Long numberId,
            String libelle, String definition, String prelevement, String assemblage, String preparation) {

        Methodesample methodeHauteurVegetalDb = this.methodeSampleDAO.getByNKey(numberId).orElse(null);
        if (methodeHauteurVegetalDb == null) {

            methodeHauteurVegetalDb = new Methodesample(definition, libelle, numberId, prelevement, assemblage, preparation);

        } else {

            methodeHauteurVegetalDb.update(definition, prelevement, assemblage, preparation);
        }
        try {

            this.methodeSampleDAO.saveOrUpdate(methodeHauteurVegetalDb);

        } catch (PersistenceException e) {

            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);

        }

    }

    /**
     * @param methodeSampleDAO
     */
    public void setMethodeSampleDAO(IMethodeSampleDAO methodeSampleDAO) {
        this.methodeSampleDAO = methodeSampleDAO;
    }

}
