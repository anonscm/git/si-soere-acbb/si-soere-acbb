/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps.jsf;

import java.io.Serializable;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.acbb.extraction.jsf.UIAssociate;
import org.inra.ecoinfo.acbb.extraction.jsf.UIDate;
import org.inra.ecoinfo.acbb.extraction.jsf.UITreatment;
import org.inra.ecoinfo.acbb.extraction.jsf.UIVariable;
import org.inra.ecoinfo.acbb.extraction.mps.IMPSDatasetManager;
import org.inra.ecoinfo.acbb.extraction.mps.impl.MPSParameterVO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.synthesis.jpa.IIntervalDateSynthesisDAO;
import org.inra.ecoinfo.acbb.utils.vo.DepthRequestParamVO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIBeanMPS.
 */
@ManagedBean(name = "uiMPS")
@ViewScoped
public class UIBeanMPS extends AbstractUIBeanForSteps implements Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The affichage @link(String).
     */
    String affichage = "1";

    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{MPSDatasetManager}")
    IMPSDatasetManager mpsDatasetManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;
    
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;

    /**
     * The parameters request @link(ParametersRequest).
     */
    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The properties variables names @link(Properties).
     */
    Properties propertiesVariablesNames;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    @ManagedProperty(value = "#{intervalDateSynthesisDAO}")
    IIntervalDateSynthesisDAO intervalDateSynthesisDAO;
    List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypeClasses = Stream.of(
            org.inra.ecoinfo.acbb.synthesis.smp.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.swc.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.ts.SynthesisDatatype.class
    ).collect(Collectors.toList());

    UITreatment uiTreatment;
    UIDate uiDate;
    UIVariable uiVariable;
    UIAssociate uIAssociate;

    // GETTERS SETTERS - BEANS
    /**
     * Instantiates a new uI bean mps.
     */
    public UIBeanMPS() {
        super();
    }

    /**
     *
     * @return
     */
    public UITreatment getUiTreatment() {
        return uiTreatment;
    }

    /**
     *
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     *
     * @return
     */
    public UIVariable getUiVariable() {
        return uiVariable;
    }

    /**
     * Adds the all depths.
     *
     * @return the string
     */
    public final String addAllDepths() {
        this.parametersRequest.depthRequestParam.getSelectedsDepths().addAll(
                this.parametersRequest.depthRequestParam.getAvailablesDepth());
        return null;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("static-access")
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap<>();
        // TODO
        uiDate.addDatestoMap(metadatasMap);
        uiTreatment.addTreatmenttoMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final MPSParameterVO parameters = new MPSParameterVO(metadatasMap);
        if(this.parametersRequest.depthRequestParam.getAvailablesDepth().isEmpty()){
            updateDepthAvailables();
        }
        if (this.parametersRequest.depthRequestParam.getChoiceDepth()) {
            parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam.getSelectedsDepths());
            if (parameters.getSelectedDepth().isEmpty()) {
                parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam.getAvailablesDepth());
            }
        } else {
            parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam
                    .getAvailablesDepth().subSet(
                            this.parametersRequest.depthRequestParam.getDepthMin(),
                            this.parametersRequest.depthRequestParam.getDepthMax() + 1));
        }
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, parametersRequest.getEuropeanFormat()?1:2);
        return null;
    }

    private List<VariableACBB> getListVariables() {
        List<VariableACBB> variables = new LinkedList();
        uiVariable.getVariables().forEach(((k, v) -> v.forEach((m, n) -> variables.add(n.getVariable()))));
        return variables;
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public final String getAffichage() {
        return this.affichage;
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (this.getStep()) {
            case 1:
                return this.uiDate.getDateStepIsValid();
            case 2:
                return this.uiTreatment.getTraitementStepIsValid();
            case 3:
                return uiVariable.getVariableStepIsValid();
            case 4:
                return this.getParametersRequest().getDepthRequestParam().isValidDepht();
            case 5:
                return uiVariable.getVariableStepIsValid();
            case 6:
                return uiVariable.getVariableStepIsValid();
            default:
                break;
        }
        return false;
    }

    /**
     *
     * @return
     */
    public IMPSDatasetManager getMpsDatasetManager() {
        return this.mpsDatasetManager;
    }

    /**
     * Gets the parameters request.
     *
     * @return the parameters request
     */
    public final ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    final void initProperties() {
        setEnabledMotivation(true);
        uiTreatment = new UITreatment();
        uiTreatment.initProperties(localizationManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager, intervalDateSynthesisDAO, synthesisDatatypeClasses);
        uiVariable = new UIVariable();
        uiVariable.initVariable(localizationManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
        this.propertiesVariablesNames = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        this.parametersRequest.setDepthRequestParam(new DepthRequestParamVO(localizationManager));
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "MPS";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (this.getStep()) {
            case 2:
                uiTreatment.updateAgroAvailables(mpsDatasetManager, uiDate.getDatesForm1ParamVO().intervalsDate());
                break;
            case 3:
                uiVariable.updateVariablesAvailables(mpsDatasetManager, uiDate, uiTreatment);
                break;
            case 4:
                this.updateDepthAvailables();
                break;
            case 5:
                final String dateStartString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateStart();
                final String dateEndString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateEnd();
                Map<Long, UIVariable.VariableJSF> variables = uiVariable.getVariables().entrySet().stream()
                        .map(e -> e.getValue().entrySet())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                IntervalDate intervalDate = null;
                try {
                    intervalDate = new IntervalDate(dateStartString, dateEndString, DateUtil.DD_MM_YYYY);
                    uIAssociate.caseassociate(variables, intervalDate, fileCompManager);
                } catch (BadExpectedValueException ex) {
                    LoggerFactory.getLogger(UIBeanMPS.class.getName()).error(ex.getMessage(), ex);
                } catch (DateTimeParseException ex) {
                    LoggerFactory.getLogger(UIBeanMPS.class.getName()).error(ex.getMessage(), ex);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * Removes the all depths.
     *
     * @return the string
     */
    public final String removeAllDepths() {
        this.parametersRequest.depthRequestParam.getSelectedsDepths().clear();
        return null;
    }

    /**
     * Select depth.
     *
     * @return the string
     */
    public final String selectDepth() {
        this.selectDepth(this.parametersRequest.depthRequestParam.getSelectedDepth());
        return null;
    }

    /**
     * Select depth.
     *
     * @param profondeur the profondeur
     */
    public final void selectDepth(final Integer profondeur) {
        if (this.parametersRequest.depthRequestParam.getSelectedsDepths().contains(profondeur)) {
            this.parametersRequest.depthRequestParam.getSelectedsDepths().remove(profondeur);
        } else {
            this.parametersRequest.depthRequestParam.getSelectedsDepths().add(profondeur);
        }
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public final void setAffichage(final String affichage) {
        this.affichage = affichage;
    }

    /**
     * Sets the extraction manager.
     *
     * @param extractionManager the new extraction manager
     */
    public final void setExtractionManager(final IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the mPS dataset manager.
     *
     * @param mpsDatasetManager the new mPS dataset manager
     */
    public final void setMpsDatasetManager(final IMPSDatasetManager mpsDatasetManager) {
        this.mpsDatasetManager = mpsDatasetManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public final void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the parameters request.
     *
     * @param parametersRequest the new parameters request
     */
    public final void setParametersRequest(final ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    public IFileCompManager getFileCompManager() {
        return fileCompManager;
    }

    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    public void setIntervalDateSynthesisDAO(IIntervalDateSynthesisDAO intervalDateSynthesisDAO) {
        this.intervalDateSynthesisDAO = intervalDateSynthesisDAO;
    }

    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * Update depth availables.
     */
    public final void updateDepthAvailables() {
        this.parametersRequest.depthRequestParam.getAvailablesDepth().clear();
        for (final Integer profondeur : this.mpsDatasetManager
                .getAvailablesDepthByTraitementAndVariables(uiTreatment.getListTraitement(), uiDate.getDatesForm1ParamVO().intervalsDate(), getListVariables())) {
            this.parametersRequest.depthRequestParam.getAvailablesDepth().add(profondeur);
        }
        this.parametersRequest.depthRequestParam
                .setDepthMin(this.parametersRequest.depthRequestParam.getAvailablesDepth().first());
        this.parametersRequest.depthRequestParam
                .setDepthMax(this.parametersRequest.depthRequestParam.getAvailablesDepth().last());
    }

    public UIAssociate getUiAssociate() {
        return uIAssociate;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The depth request param @link(DepthRequestParamVO).
         */
        DepthRequestParamVO depthRequestParam;
        /**
         * The european format @link(boolean).
         */
        boolean europeanFormat = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the depth request param.
         *
         * @return the depth request param
         */
        public DepthRequestParamVO getDepthRequestParam() {
            return this.depthRequestParam;
        }

        /**
         * Gets the depth step is valid.
         *
         * @return the depth step is valid
         */
        public Boolean getDepthStepIsValid() {
            return this.depthRequestParam.isValidDepht();
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getEuropeanFormat() {
            return this.europeanFormat;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiDate.getDateStepIsValid() && uiTreatment.getTraitementStepIsValid()
                    && uiVariable.getVariableStepIsValid()
                    && this.depthRequestParam.getIsDepthStepValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Sets the depth request param.
         *
         * @param depthRequestParam the new depth request param
         */
        public final void setDepthRequestParam(final DepthRequestParamVO depthRequestParam) {
            this.depthRequestParam = depthRequestParam;
        }

        /**
         * Sets the european format.
         *
         * @param europeanFormat the new european format
         */
        public final void setEuropeanFormat(final boolean europeanFormat) {
            this.europeanFormat = europeanFormat;
        }
    }
}
