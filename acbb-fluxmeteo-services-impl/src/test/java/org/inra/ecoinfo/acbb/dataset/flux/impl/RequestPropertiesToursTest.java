package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * @author ptcherniati
 */
public class RequestPropertiesToursTest {

    MockUtils m = MockUtils.getInstance();
    @Mock
    ITestDuplicates testDuplicates;
    @Mock
    ISequenceFluxToursDAO sequenceFluxToursDAO;
    /**
     *
     */
    public RequestPropertiesToursTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of dateToString method, of class RequestPropertiesTours.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDateToString() throws Exception {
        String date = "25/12/1965";
        String time = "23:45";
        RequestPropertiesTours instance = new RequestPropertiesTours();
        String expResult = "19651225234500";
        String result = instance.dateToStringYYYYMMJJHHMMSS(date, time);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getNomDeFichier method, of class RequestPropertiesTours.
     */
    @Test
    public void testGetNomDeFichier() {
        VersionFile version = this.m.versionFile;
        RequestPropertiesTours instance = new RequestPropertiesTours();
        String expResult = "agro-écosysteme/site_flux_tours_01/01/2012_31/12/2012.csv";
        String result = instance.getNomDeFichier(version);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getTestDuplicates method, of class RequestPropertiesTours.
     */
    @Test
    public void testGetTestDuplicates() {
        RequestPropertiesTours instance = new RequestPropertiesTours();
        instance.setTestDuplicates(this.testDuplicates);
        ITestDuplicates expResult = this.testDuplicates;
        ITestDuplicates result = instance.getTestDuplicates();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setDateDeFin method, of class RequestPropertiesTours.
     */
    @Test
    public void testSetDateDeFin() {
        LocalDateTime dateDeFin = this.m.dateTimeFin;
        RequestPropertiesTours instance = Mockito.mock(RequestPropertiesTours.class,
                Mockito.CALLS_REAL_METHODS);
        instance.setDateDeFin(dateDeFin);
        Mockito.verify(instance).setDateDeFin(dateDeFin, ChronoUnit.MINUTES, 30);
        Assert.assertEquals(instance.getDateDeFin(), dateDeFin);
    }

    /**
     * Test of testNomFichier method, of class RequestPropertiesTours.
     */
    @Test
    public void testTestNomFichier() throws DateTimeException {
        String nomfichier = "agro-écosysteme/site_flux_tours_01-01-2012_31-12-2012.csv";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err ");
        RequestPropertiesTours instance = Mockito.mock(RequestPropertiesTours.class,
                Mockito.CALLS_REAL_METHODS);
        Mockito.when(instance.getSite()).thenReturn(this.m.site);
        Mockito.when(instance.getDateDeDebut()).thenReturn(this.m.dateTimeDebut);
        Mockito.when(instance.getDateDeFin()).thenReturn(this.m.dateTimeFin);
        Mockito.when(instance.getLocalizationManager()).thenReturn(new SpringLocalizationManager());
        // nominal case
        instance.testNomFichier(nomfichier, badsFormatsReport);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // bad site
        nomfichier = "agro-ecosysteme/site2_flux_tours_01-01-2012_31-12-2012.csv";
        instance.testNomFichier(nomfichier, badsFormatsReport);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err  :- Le nom du fichier ne correspond pas aux informations de l'entête : nom de site différents. ",
                badsFormatsReport.getMessages());
        // bad datatype
        nomfichier = "agro-écosysteme/site_flux_chambres_01-01-2012_31-12-2012.csv";
        badsFormatsReport = new BadsFormatsReport("Err ");
        instance.testNomFichier(nomfichier, badsFormatsReport);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err  :- Le nom du fichier ne correspond pas aux informations de l'entête : nom de datatype différent. ",
                badsFormatsReport.getMessages());
        // bad datedebut
        nomfichier = "agro-écosysteme/site_flux_tours_02-01-2012_31-12-2012.csv";
        badsFormatsReport = new BadsFormatsReport("Err ");
        instance.testNomFichier(nomfichier, badsFormatsReport);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err  :- Le nom du fichier ne correspond pas aux informations de l'entête : dates de début différentes. ",
                badsFormatsReport.getMessages());
        // bad datefin
        nomfichier = "agro-écosysteme/site_flux_tours_01-01-2012_30-12-2013.csv";
        badsFormatsReport = new BadsFormatsReport("Err ");
        instance.testNomFichier(nomfichier, badsFormatsReport);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err  :- Le nom du fichier ne correspond pas aux informations de l'entête : dates de fin différentes). ",
                badsFormatsReport.getMessages());
        // bad pattern
        nomfichier = "bad pattern";
        badsFormatsReport = new BadsFormatsReport("Err ");
        instance.testNomFichier(nomfichier, badsFormatsReport);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err  :- Le nom du fichier ne correspond pas aux informations de l'en-tête. ",
                badsFormatsReport.getMessages());
    }

}
