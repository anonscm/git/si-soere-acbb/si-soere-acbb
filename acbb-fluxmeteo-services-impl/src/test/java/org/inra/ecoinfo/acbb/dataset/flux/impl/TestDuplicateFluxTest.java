package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;
import org.mockito.Mockito;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public class TestDuplicateFluxTest {

    MockUtils m = MockUtils.getInstance();

    /**
     *
     */
    public TestDuplicateFluxTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addLine method, of class TestDuplicateFlux.
     */
    @Test
    public void testAddLine_4args_1() {
        TestDuplicateFlux instance = new TestDuplicateFlux(null);
        final ErrorsReport errorsReport = new ErrorsReport();
        instance.setErrorsReport(errorsReport);
        instance.addLine("datetime1", "date", "time", 1);
        Assert.assertFalse(errorsReport.hasErrors());
        instance.addLine("datetime2", "date", "time", 2);
        Assert.assertFalse(errorsReport.hasErrors());
        instance.addLine("datetime1", "date", "time", 3);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-La ligne 3 à la même date \"date\" et la même heure \"time\" que la ligne 1\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of addLine method, of class TestDuplicateFlux.
     */
    @Test
    public void testAddLine_4args_2() {
        TestDuplicateFlux instance = new TestDuplicateFlux(null);
        ErrorsReport errorsReport = new ErrorsReport();
        instance.setErrorsReport(errorsReport);
        instance.addLine(new String[]{"25/12/1970", "20;30"}, 1, null, null);
        Assert.assertFalse(errorsReport.hasErrors());
        instance.addLine(new String[]{"25/12/1970", "20;30;01"}, 2, null, null);
        Assert.assertFalse(errorsReport.hasErrors());
        instance.addLine(new String[]{"25/12/1970", "20;30"}, 3, null, null);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-La ligne 3 à la même date \"25/12/1970\" et la même heure \"20;30\" que la ligne 1\n",
                errorsReport.getErrorsMessages());
        // with dates
        instance = new TestDuplicateFluxTestImpl(null);
        errorsReport = new ErrorsReport();
        final String dateString = "25/12/1970";
        final String timeString = "20;30;01";
        final String[] datesArray = new String[]{dateString, timeString};
        instance.addLine(datesArray, 4, new String[]{"25/12/1970", "20;30;01"},
                this.m.versionFile);
        Mockito.verify(this.m.versionFile).setVersionNumber(1);
    }

    /**
     * Test of testLineForDuplicatesLineinDb method, of class TestDuplicateFlux.
     */
    @Test
    public void testTestLineForDuplicatesLineinDb() throws DateTimeException {
        final String dateDBString = "25/12/1970";
        String timeDBString = "20:30";
        String timeDBString2 = "20:31";
        LocalDate date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDBString);
        timeDBString = (timeDBString + ":00").substring(0, DateUtil.HH_MM_SS.length());
        timeDBString2 = (timeDBString2 + ":00").substring(0, DateUtil.HH_MM_SS.length());
        LocalTime time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, timeDBString);
        LocalTime time2 = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, timeDBString2);
        ISequenceFluxToursDAO sequenceFluxToursDAO = Mockito.mock(ISequenceFluxToursDAO.class);
        SequenceFluxTours sequenceFluxTours = Mockito.mock(SequenceFluxTours.class);
        List<MesureFluxTours> mesures = new LinkedList();
        MesureFluxTours mesure = Mockito.mock(MesureFluxTours.class);
        mesures.add(mesure);
        Mockito.when(sequenceFluxTours.getMesuresFluxTours()).thenReturn(mesures);
        Mockito.when(mesure.getHeure()).thenReturn(time2, time);
        Mockito.when(mesure.getLigneFichierEchange()).thenReturn(1L);
        Mockito.when(sequenceFluxTours.getVersion()).thenReturn(this.m.versionFile);
        TestDuplicateFlux instance = new TestDuplicateFlux(sequenceFluxToursDAO);
        instance.setConfiguration(this.m.datasetConfiguration);
        Mockito.when(sequenceFluxToursDAO.getSequence(date, this.m.versionFile)).thenReturn(
                Optional.of(sequenceFluxTours));
        ErrorsReport errorsReport = new ErrorsReport();
        instance.setErrorsReport(errorsReport);
        long lineNumber = 1L;
        instance.testLineForDuplicatesLineinDb(dateDBString, timeDBString, lineNumber,
                this.m.versionFile);
        Assert.assertFalse(errorsReport.hasErrors());
        instance.testLineForDuplicatesLineinDb(dateDBString, timeDBString, lineNumber,
                this.m.versionFile);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-La ligne 1 à la même date \"25/12/1970\" et la même heure \"20:30:00\" que la ligne 1 dans le fichier agro-écosysteme_site_datatype_01-01-2012_31-12-2012.csv\n",
                errorsReport.getErrorsMessages());
    }

    class TestDuplicateFluxTestImpl extends TestDuplicateFlux {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        TestDuplicateFluxTestImpl(ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO) {
            super(sequenceFluxToursDAO);
        }

        @Override
        protected void testLineForDuplicatesDateInDB(String[] dates, String dateString,
                                                     String timeString, long lineNumber, VersionFile versionFile) {
            versionFile.setVersionNumber(1);
        }
    }

}
