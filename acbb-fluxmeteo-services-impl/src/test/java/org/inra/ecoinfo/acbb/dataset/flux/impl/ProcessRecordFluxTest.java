package org.inra.ecoinfo.acbb.dataset.flux.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.flux.IMesureFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.IRequestPropertiesFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.ValeurFluxTours;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.StringReader;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * @author ptcherniati
 */
public class ProcessRecordFluxTest {

    MockUtils m = MockUtils.getInstance();
    @Mock
    IMesureFluxToursDAO<MesureFluxTours> mesureFluxToursDAO;
    @Mock
    ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO;
    @Mock
    SequenceFluxTours sequenceFluxTours;
    @Mock
    IRequestPropertiesFluxTours requestPropertiesFluxTours;
    @Mock
    FluxLineRecord line;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    List<VariableValue> variableValues = new LinkedList();
    @Mock
    VariableValue variableValue;
    @Mock
    VariableValue variableValue1;
    @Mock
    VariableValue variableValue2;
    @Mock
    Column columnDate;
    @Mock
    Column columnTime;
    @Mock
    Column column1;
    @Mock
    VariableACBB variable1;
    @Mock
    DatatypeVariableUniteACBB dvu1;
    @Mock
    RealNode realNode1;
    @Mock
    Column column2;
    @Mock
    VariableACBB variable2;
    @Mock
    DatatypeVariableUniteACBB dvu2;
    @Mock
    RealNode realNode2;
    @Mock
    Column column2qc;
    @Mock
    Map<Variable, RealNode> dbRealNodeVariable;
    LocalDateTime dateTime;
    ProcessRecordFlux instance;
    List<Column> columns = new LinkedList();
    /**
     *
     */
    public ProcessRecordFluxTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @throws PersistenceException
     * @throws DateTimeException
     */
    @Before
    public void setUp() throws PersistenceException, DateTimeException {
        MockitoAnnotations.initMocks(this);
        this.instance = new ProcessRecordFlux();
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.instance.setListeACBBDAO(this.m.listeACBBDAO);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        this.instance.setMesureFluxToursDAO(this.mesureFluxToursDAO);
        this.instance.setParcelleDAO(this.m.parcelleDAO);
        this.instance.setSequenceFluxToursDAO(this.sequenceFluxToursDAO);
        this.instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        this.instance.setTraitementDAO(this.m.traitementDAO);
        this.instance.setVariableDAO(this.m.variableDAO);
        this.instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        this.instance.setVersionFileDAO(this.m.versionFileDAO);
        doReturn(realNode1).when(dbRealNodeVariable).get(variable1);
        doReturn(realNode2).when(dbRealNodeVariable).get(variable2);
        Mockito.when(dvu1.getVariable()).thenReturn(variable1);
        Mockito.when(dvu2.getVariable()).thenReturn(variable2);

        Mockito.when(this.requestPropertiesFluxTours.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.m.parcelleDAO.merge(this.m.parcelle)).thenReturn(this.m.parcelle);
        Mockito.when(this.m.parcelle.getId()).thenReturn(1L);
        Mockito.when(this.m.versionFileDAO.merge(this.m.versionFile))
                .thenReturn(this.m.versionFile);
        Mockito.when(this.m.variableDAO.merge(this.m.variable)).thenReturn(this.m.variable);
        this.dateTime = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, "25/12/1965 13:00");
        Mockito.when(this.line.getDateTime()).thenReturn(this.dateTime);
        Mockito.when(this.line.getOriginalLineNumber()).thenReturn(1L, 2L);
        this.variableValues.add(this.variableValue);
        this.variableValues.add(this.variableValue1);
        this.variableValues.add(this.variableValue2);
        Mockito.when(this.line.getVariablesValues()).thenReturn(this.variableValues);
        Mockito.when(this.variableValue1.getDatatypeVariableUnite()).thenReturn(dvu1);
        Mockito.when(this.variableValue2.getDatatypeVariableUnite()).thenReturn(dvu2);
        Mockito.when(this.variableValue.getValue()).thenReturn(org.apache.commons.lang.StringUtils.EMPTY, "2.3", "4.2", null);
        Mockito.when(this.variableValue1.getValue()).thenReturn("2.3", (String) null);
        Mockito.when(this.variableValue2.getValue()).thenReturn("4.2", (String) null);
        Mockito.when(this.variableValue.isQualityClass()).thenReturn(false, true, null);
        Mockito.when(this.variableValue1.isQualityClass()).thenReturn(false, (Boolean) null);
        Mockito.when(this.variableValue2.isQualityClass()).thenReturn(true, (Boolean) null);
        Mockito.when(this.variableValue1.getQualityClass()).thenReturn(1, (Integer) null);
        Mockito.when(this.variableValue2.getQualityClass()).thenReturn(1, (Integer) null);

        Mockito.when(this.requestPropertiesFluxTours.getSite()).thenReturn(this.m.site);
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        Mockito.when(this.datasetDescriptor.getEnTete()).thenReturn(0);
        this.columns.add(this.columnDate);
        this.columns.add(this.columnTime);
        this.columns.add(this.column1);
        this.columns.add(this.column2);
        this.columns.add(this.column2qc);
        Mockito.when(this.columnDate.getFlagType()).thenReturn(RecorderACBB.PROPERTY_CST_DATE_TYPE);
        Mockito.when(this.columnTime.getFlagType()).thenReturn(RecorderACBB.PROPERTY_CST_TIME_TYPE);
        Mockito.when(this.column1.isFlag()).thenReturn(true);
        Mockito.when(this.column2.isFlag()).thenReturn(true);
        Mockito.when(this.column2qc.isFlag()).thenReturn(true);
        Mockito.when(this.column1.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.column2.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.column1.getName()).thenReturn("variable 1");
        Mockito.when(this.m.variableDAO.getByAffichage("variable 1")).thenReturn(Optional.of(this.variable1));
        Mockito.when(this.variable1.getAffichage()).thenReturn("variable 1");
        Mockito.when(this.m.variableDAO.merge(this.variable1)).thenReturn(this.variable1);
        Mockito.when(this.column2.getName()).thenReturn("variable 2");
        Mockito.when(this.m.variableDAO.getByAffichage("variable 2")).thenReturn(Optional.of(this.variable2));
        Mockito.when(this.variable2.getAffichage()).thenReturn("variable 2");
        Mockito.when(this.m.variableDAO.merge(this.variable2)).thenReturn(this.variable2);
        Mockito.when(this.column2.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.column2qc.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        Mockito.when(this.column2qc.getRefVariableName()).thenReturn("variable 2");

    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildMesure method, of class ProcessRecordFlux.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildMesure() throws Exception {
        SortedSet<FluxLineRecord> ligneEnErreur = new TreeSet();
        this.sequenceFluxTours = new SequenceFluxTours();
        Assert.assertTrue(this.sequenceFluxTours.getMesuresFluxTours().isEmpty());
        final ErrorsReport errorsReport = new ErrorsReport();
        Mockito.when(m.datatypeVariableUniteDAO.mergeRealNode(realNode1)).thenReturn(realNode1);
        Mockito.when(m.datatypeVariableUniteDAO.mergeRealNode(realNode2)).thenReturn(realNode2);
        // nominal case
        this.instance.buildMesure(this.line, this.sequenceFluxTours, ligneEnErreur, errorsReport,
                this.requestPropertiesFluxTours, dbRealNodeVariable);
        Assert.assertTrue(this.sequenceFluxTours.getMesuresFluxTours().size() == 1);
        MesureFluxTours mesure = this.sequenceFluxTours.getMesuresFluxTours().get(0);
        Assert.assertTrue(1L == mesure.getLigneFichierEchange());
        Assert.assertTrue(2 == mesure.getValeursFluxTours().size());
        Assert.assertNull(mesure.getValeursFluxTours().get(0).getQualityFlag());
        Assert.assertEquals(realNode1, mesure.getValeursFluxTours().get(0).getRealNode());
        Assert.assertEquals(Float.valueOf("2.3"), mesure.getValeursFluxTours().get(0).getValue());
        Assert.assertTrue(1 == mesure.getValeursFluxTours().get(1).getQualityFlag());
        Assert.assertEquals(realNode2, mesure.getValeursFluxTours().get(1).getRealNode());
        Assert.assertEquals(Float.valueOf("4.2"), mesure.getValeursFluxTours().get(1).getValue());
        Assert.assertFalse(errorsReport.hasErrors());
    }

    /**
     * Test of buildSequence method, of class ProcessRecordFlux.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildSequence() throws Exception {
        // return new instance
        Mockito.when(
                this.sequenceFluxToursDAO.getByVersionAndDateAndSuiviParcelleId(this.m.versionFile,
                        this.m.dateDebut, 1L)).thenReturn(null);
        SequenceFluxTours result = this.instance.buildSequence(this.m.versionFile,
                this.m.dateDebut, this.requestPropertiesFluxTours);
        Assert.assertNotNull(result);
        Assert.assertNotSame(this.sequenceFluxTours, result);
        // return register instance
        Mockito.when(
                this.sequenceFluxToursDAO.getByVersionAndDateAndSuiviParcelleId(this.m.versionFile,
                        this.m.dateDebut, 1L)).thenReturn(Optional.of(this.sequenceFluxTours));
        result = this.instance.buildSequence(this.m.versionFile, this.m.dateDebut,
                this.requestPropertiesFluxTours);
        Assert.assertNotNull(result);
        Assert.assertEquals(m.versionFile, result.getVersion());
        Assert.assertEquals(m.parcelle, result.getParcelle());
    }

    /**
     * Test of processRecord method, of class ProcessRecordFlux.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String fichier = "20/10/2012;00:00;23.1;12.1;0\n" + "20/10/2012;00:30;23.2;12.2;1\n"
                + "21/10/2012;00:00;23.3;12.3;2\n";
        instance = spy(instance);
        instance.setDatatypeName("datatype");
        Map<String, DatatypeVariableUniteACBB> dvus = Mockito.mock(Map.class);
        Mockito.when(m.datatypeVariableUniteDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode("datatype")).thenReturn(dvus);
        Mockito.when(variable1.getCode()).thenReturn("variable1");
        Mockito.when(variable2.getCode()).thenReturn("variable2");
        Mockito.when(dvus.get("variable1")).thenReturn(dvu1);
        Mockito.when(dvus.get("variable2")).thenReturn(dvu2);
        Map<Variable, RealNode> dvusNodes = Mockito.mock(Map.class);
        Mockito.when(m.datatypeVariableUniteDAO.getRealNodesVariables(m.dataTypeRealNode)).thenReturn(dvusNodes);
        Mockito.when(dvusNodes.get(variable1)).thenReturn(realNode1);
        Mockito.when(dvusNodes.get(variable2)).thenReturn(realNode2);
        Mockito.when(m.datatypeVariableUniteDAO.mergeRealNode(realNode1)).thenReturn(realNode1);
        Mockito.when(m.datatypeVariableUniteDAO.mergeRealNode(realNode2)).thenReturn(realNode2);
        CSVParser parser = new CSVParser(new StringReader(fichier), ';');
        String fileEncoding = "UTF-8";
        ArgumentCaptor<SequenceFluxTours> sequence = ArgumentCaptor
                .forClass(SequenceFluxTours.class);
        this.instance.processRecord(parser, this.m.versionFile, this.requestPropertiesFluxTours,
                fileEncoding, this.datasetDescriptor);
        Mockito.verify(this.sequenceFluxToursDAO, Mockito.times(2)).saveOrUpdate(sequence.capture());
        List<SequenceFluxTours> sequences = sequence.getAllValues();
        Assert.assertTrue(2 == sequences.size());
        final SequenceFluxTours sequence1 = sequences.get(0);
        Assert.assertEquals("2012-10-20", sequence1.getDateMesure().toString());
        Assert.assertEquals(this.m.versionFile, sequence1.getVersion());
        Assert.assertEquals(this.m.parcelle, sequence1.getParcelle());
        final List<MesureFluxTours> mesuresFluxTours1 = sequence1.getMesuresFluxTours();
        Assert.assertTrue(2 == mesuresFluxTours1.size());
        final MesureFluxTours mesure1 = mesuresFluxTours1.get(0);
        Assert.assertEquals(m.timeDebut, mesure1.getHeure());
        Assert.assertTrue(1 == mesure1.getLigneFichierEchange());
        final List<ValeurFluxTours> valeursFluxTours1 = mesure1.getValeursFluxTours();
        ValeurFluxTours valeur11 = valeursFluxTours1.get(0);
        Assert.assertTrue(23.1F == valeur11.getValue());
        Assert.assertNull(valeur11.getQualityFlag());
        Assert.assertEquals(realNode1, valeur11.getRealNode());
        ValeurFluxTours valeur12 = valeursFluxTours1.get(1);
        Assert.assertTrue(12.1F == valeur12.getValue());
        Assert.assertTrue(0 == valeur12.getQualityFlag());
        Assert.assertEquals(realNode2, valeur12.getRealNode());
        Assert.assertTrue(2 == valeursFluxTours1.size());
        final MesureFluxTours mesure2 = mesuresFluxTours1.get(1);
        Assert.assertEquals(DateUtil.readLocalTimeFromText(DateUtil.HH_MM, "00:30"), mesure2.getHeure());
        Assert.assertTrue(2 == mesure2.getLigneFichierEchange());
        final List<ValeurFluxTours> valeursFluxTours2 = mesure2.getValeursFluxTours();
        ValeurFluxTours valeur21 = valeursFluxTours2.get(0);
        Assert.assertTrue(23.2F == valeur21.getValue());
        Assert.assertNull(valeur21.getQualityFlag());
        Assert.assertEquals(realNode1, valeur21.getRealNode());
        ValeurFluxTours valeur22 = valeursFluxTours2.get(1);
        Assert.assertTrue(12.2F == valeur22.getValue());
        Assert.assertTrue(1 == valeur22.getQualityFlag());
        Assert.assertEquals(realNode2, valeur22.getRealNode());
        Assert.assertTrue(2 == valeursFluxTours2.size());
        final SequenceFluxTours sequence2 = sequences.get(1);
        Assert.assertEquals("2012-10-21", sequence2.getDateMesure().toString());
        Assert.assertEquals(this.m.versionFile, sequence2.getVersion());
        Assert.assertEquals(this.m.parcelle, sequence2.getParcelle());
        final List<MesureFluxTours> mesuresFluxTours2 = sequence2.getMesuresFluxTours();
        Assert.assertTrue(1 == mesuresFluxTours2.size());
        final MesureFluxTours mesure3 = mesuresFluxTours2.get(0);
        Assert.assertEquals(m.timeDebut, mesure3.getHeure());
        Assert.assertTrue(3 == mesure3.getLigneFichierEchange());
        final List<ValeurFluxTours> valeursFluxTours3 = mesure3.getValeursFluxTours();
        ValeurFluxTours valeur31 = valeursFluxTours3.get(0);
        Assert.assertTrue(23.3F == valeur31.getValue());
        Assert.assertNull(valeur31.getQualityFlag());
        Assert.assertEquals(this.realNode1, valeur31.getRealNode());
        ValeurFluxTours valeur32 = valeursFluxTours3.get(1);
        Assert.assertTrue(12.3F == valeur32.getValue());
        Assert.assertTrue(2 == valeur32.getQualityFlag());
        Assert.assertEquals(this.realNode2, valeur32.getRealNode());
        Assert.assertTrue(2 == valeursFluxTours3.size());

    }

}
