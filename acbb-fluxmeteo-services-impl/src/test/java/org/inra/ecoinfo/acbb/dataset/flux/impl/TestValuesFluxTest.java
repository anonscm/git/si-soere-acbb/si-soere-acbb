package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati
 */
public class TestValuesFluxTest {

    MockUtils m = MockUtils.getInstance();
    @Mock
    Column columnDate;
    @Mock
    Column columnTime;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    List<Column> columns = new LinkedList();
    /**
     *
     */
    public TestValuesFluxTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.columns.add(this.columnDate);
        this.columns.add(this.columnTime);
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        Mockito.when(this.columnDate.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY);
        Mockito.when(this.columnTime.getFormatType()).thenReturn(DateUtil.HH_MM);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of checkDateTypeValue method, of class TestValuesFlux.
     */
    @Test
    public void testCheckDateTypeValue() {
        String[] values = null;
        BadsFormatsReport badsFormatsReport = null;
        long lineNumber = 0L;
        int index = 0;
        String value = org.apache.commons.lang.StringUtils.EMPTY;
        Column column = null;
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        IRequestPropertiesACBB requestPropertiesACBB = null;
        TestValuesFlux instance = new TestValuesFlux();
        LocalDate result = instance.checkDateTypeValue(values, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertEquals(null, result);
    }

    /**
     * Test of checkOtherTypeValue method, of class TestValuesFlux.
     */
    @Test
    public void testCheckOtherTypeValue() {
        String[] values = null;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err");
        long lineNumber = 0L;
        int index = 0;
        String value = org.apache.commons.lang.StringUtils.EMPTY;
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        IRequestPropertiesACBB requestPropertiesACBB = null;
        TestValuesFlux instance = new TestValuesFlux();
        // isflag==false
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, value,
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // flag not RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE
        Mockito.when(this.columnTime.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.columnTime.getFlagType()).thenReturn("flag");
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, value,
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // flag is RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE
        Mockito.when(this.columnTime.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        // qc == 2
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, "2",
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // qc == 1
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, "1",
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // qc == 0
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, "0",
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // qc null
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, null,
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // qc bad
        instance.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, "un",
                this.columnTime, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err :- A la ligne 0, colonne 1, l'indice de qualité ne peut accepter que les valeurs 0,1 ou 2 ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkTimeTypeValue method, of class TestValuesFlux.
     */
    @Test
    public void testCheckTimeTypeValue() throws DateTimeException {
        String value = "12:00";
        String[] values = new String[]{MockUtils.DATE_DEBUT, value};
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err");
        long lineNumber = 1L;
        int index = 1;
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        TestValuesFlux instance = new TestValuesFlux();
        LocalTime expResult = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, value);
        LocalTime result = instance.checkTimeTypeValue(values, badsFormatsReport, lineNumber, index,
                value, this.columnTime, variablesTypesDonnees, this.datasetDescriptor,
                new RequestPropertiesTours());
        Assert.assertEquals(expResult, result);
    }

}
