package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ptcherniati
 */
public class FluxLineRecordTest {

    /**
     *
     */
    public FluxLineRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class FluxLineRecord.
     */
    @Test
    public void testCompareTo() {
        FluxLineRecord o = new FluxLineRecord();
        FluxLineRecord instance = new FluxLineRecord();
        instance.originalLineNumber = 15L;
        // equals
        Assert.assertTrue(0 == instance.compareTo(instance));
        // minor
        o.originalLineNumber = 14L;
        Assert.assertTrue(0 > instance.compareTo(o));
        // major
        o.originalLineNumber = 16L;
        Assert.assertTrue(0 < instance.compareTo(o));

    }

    /**
     * Test of copy method, of class FluxLineRecord.
     */
    @Test
    public void testCopy() {
        FluxLineRecord line = new FluxLineRecord();
        FluxLineRecord instance = new FluxLineRecord();
        line.originalLineNumber = 12L;
        line.dateTime = LocalDateTime.now();
        line.variablesValues = new LinkedList();
        instance.copy(line);
        Assert.assertEquals(line, instance);
        Assert.assertEquals(line.originalLineNumber, instance.originalLineNumber);
        Assert.assertEquals(line.dateTime, instance.dateTime);
        Assert.assertEquals(line.variablesValues, instance.variablesValues);
    }

    /**
     * Test of equals method, of class FluxLineRecord.
     */
    @Test
    public void testEquals() {
        Object obj = null;
        FluxLineRecord instance = new FluxLineRecord();
        instance.originalLineNumber = 14L;
        // equlas
        obj = instance;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        Assert.assertEquals(expResult, result);
        // not equals
        expResult = false;
        obj = org.apache.commons.lang.StringUtils.EMPTY;
        result = instance.equals(obj);
        Assert.assertEquals(expResult, result);
        obj = new FluxLineRecord();
        result = instance.equals(obj);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getDateTime method, of class FluxLineRecord.
     */
    @Test
    public void testGetDateTime() {
        FluxLineRecord instance = new FluxLineRecord();
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(2_024), DateUtil.getLocaleZoneId());
        instance.dateTime = date;
        LocalDateTime result = instance.getDateTime();
        Assert.assertEquals(date, result);
    }

    /**
     * Test of getOriginalLineNumber method, of class FluxLineRecord.
     */
    @Test
    public void testGetOriginalLineNumber() {
        FluxLineRecord instance = new FluxLineRecord();
        instance.originalLineNumber = 12L;
        Long result = instance.getOriginalLineNumber();
        Assert.assertTrue(12L == result);
    }

    /**
     * Test of getVariablesValues method, of class FluxLineRecord.
     */
    @Test
    public void testGetVariablesValues() {
        FluxLineRecord instance = new FluxLineRecord();
        final LinkedList variableValues = new LinkedList();
        instance.setVariablesValues(variableValues);
        List<VariableValue> result = instance.getVariablesValues();
        Assert.assertEquals(variableValues, result);
    }

}
