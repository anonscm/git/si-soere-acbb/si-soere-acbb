package org.inra.ecoinfo.acbb.dataset.flux.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.flux.IRequestPropertiesFluxTours;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

/**
 * @author ptcherniati
 */
public class TestHeadersFluxTest {


    MockUtils m = MockUtils.getInstance();
    @Mock
    IRequestPropertiesFluxTours requestPropertiesFluxTours;
    @Mock
    DatasetDescriptorACBB datasetDescriptorACBB;
    @Mock
    CSVParser parser;

    /**
     *
     */
    public TestHeadersFluxTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of testHeaders method, of class TestHeadersFlux.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        String encoding = "UTF-8";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport(encoding);
        TestHeadersFlux instance = new TestHeadersFluxImpl();
        instance.setLocalizationManager(new SpringLocalizationManager());
        Mockito.when(this.m.versionFile.getData()).thenReturn("data".getBytes());
        long expResult = 10L;
        long result = instance.testHeaders(this.parser, this.m.versionFile,
                this.requestPropertiesFluxTours, encoding, badsFormatsReport,
                this.datasetDescriptorACBB);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of testUnites method, of class TestHeadersFlux.
     */
    @Test
    public void testTestUnites() {
        String[] line = null;
        long lineNumber = 0L;
        TestHeadersFlux instance = new TestHeadersFlux();
        instance.setLocalizationManager(new SpringLocalizationManager());
        Mockito.when(this.m.versionFile.getData()).thenReturn("data".getBytes());
        long expResult = 1L;
        long result = instance.testUnites(line, lineNumber);
        Assert.assertEquals(expResult, result);
    }

    class TestHeadersFluxImpl extends TestHeadersFlux {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        @Override
        protected long jumpLines(CSVParser parser, long lineNumber, int numberOfJumpedLines)
                throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readBeginAndEndDates(VersionFile version,
                                            BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber,
                                            IRequestPropertiesACBB requestProperties) throws IOException {
            return lineNumber + 2;
        }

        @Override
        protected long readBeginDate(VersionFile versionFile, BadsFormatsReport badsFormatsReport,
                                     CSVParser parser, long lineNumber, IRequestPropertiesACBB requestProperties)
                throws IOException {
            throw new IOException();
        }

        @Override
        protected long readCommentaire(CSVParser parser, long lineNumber,
                                       IRequestPropertiesACBB requestProperties) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                    long lineNumber, String datatypeName) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readEmptyLine(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                     long lineNumber) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readEndDate(VersionFile versionFile, BadsFormatsReport badsFormatsReport,
                                   CSVParser parser, long lineNumber, IRequestPropertiesACBB requestProperties)
                throws IOException {
            throw new IOException();
        }

        @Override
        protected long readLineHeader(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                      long lineNumber, DatasetDescriptor datasetDescriptor,
                                      IRequestPropertiesACBB requestPropertiesACBB) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readSite(VersionFile version, BadsFormatsReport badsFormatsReport,
                                CSVParser parser, long lineNumber, IRequestPropertiesACBB requestProperties)
                throws IOException, BusinessException {
            throw new IOException();
        }

        @Override
        protected long readSiteAndParcelle(VersionFile version,
                                           BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber,
                                           IRequestPropertiesACBB requestProperties) throws IOException, BusinessException {
            return lineNumber + 1;
        }

        @Override
        protected long readTraitement(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                      long lineNumber, IRequestPropertiesACBB requestPropertiesACBB) throws IOException {
            throw new IOException();
        }

        @Override
        protected long readTraitementAndSuiviParcelle(BadsFormatsReport badsFormatsReport,
                                                      CSVParser parser, long lineNumber, IRequestPropertiesACBB requestProperties)
                throws IOException {
            throw new IOException();
        }

        @Override
        protected long readTraitementVersionAndDateDebutTraitement(
                BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber,
                IRequestPropertiesACBB requestProperties) throws IOException {
            throw new IOException();
        }
    }

}
