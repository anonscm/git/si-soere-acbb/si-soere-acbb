package org.inra.ecoinfo.acbb.extraction;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.acbb.ACBBTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ACBBTransactionalTestFixtureExecutionListener.class})
public class ExtractionFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {


    ITraitementDAO traitementDAO;
    ISiteACBBDAO siteACBBDAO;
    ILocalizationManager localizationManager;
    IVariableACBBDAO variableACBBDAO;

    /**
     *
     */
    public ExtractionFixture() {
        super();
        MockitoAnnotations.initMocks(this);
        this.traitementDAO = ACBBTransactionalTestFixtureExecutionListener.traitementDAO;
        this.siteACBBDAO = ACBBTransactionalTestFixtureExecutionListener.siteACBBDAO;
        this.localizationManager = ACBBTransactionalTestFixtureExecutionListener.localizationManager;
        this.variableACBBDAO = ACBBTransactionalTestFixtureExecutionListener.variableACBBDAO;
    }

    /**
     * @param listeSitesCodes
     * @param variablesListNames
     * @param dateDedebut
     * @param datedeFin
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     */
    public String extract(String listeSitesCodes, String variablesListNames, String dateDedebut, String datedeFin, String filecomps, String commentaire, String affichage) throws BusinessException, PersistenceException, DateTimeException {
        List<SiteACBB> sites = Stream.of(listeSitesCodes.split(","))
                .map(ss -> this.siteACBBDAO.getByPath(ss))
                .filter(s -> s.isPresent())
                .map(s -> (SiteACBB) s.get())
                .collect(Collectors.toList());
        final Map<String, Object> metadatasMap = new HashMap();
        metadatasMap.put(SiteACBB.class.getSimpleName(), sites);
        List<VariableACBB> variablesFluxTours = new LinkedList();
        List<VariableACBB> variablesFluxChambre = new LinkedList();
        List<VariableACBB> variablesMeteo = new LinkedList();
        for (String variableByDatatypeList : variablesListNames.split(";")) {
            String[] datatypeListVariable = variableByDatatypeList.split(":");
            if (datatypeListVariable.length < 2) {
                continue;
            }
            List<VariableACBB> currentList;
            switch (datatypeListVariable[0].trim().toLowerCase()) {
                case "fluxtours":
                    currentList = variablesFluxTours;
                    Variable.class.getSimpleName().toLowerCase().concat(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS);
                    break;
                case "fluxchambres":
                    currentList = variablesFluxChambre;
                    Variable.class.getSimpleName().toLowerCase().concat(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS);
                    break;
                case "meteo":
                    currentList = variablesMeteo;
                    Variable.class.getSimpleName().toLowerCase().concat(IMeteoDatatypeManager.CODE_DATATYPE_METEO);
                    break;
                default:
                    continue;
            }
            for (String variableAffichage : datatypeListVariable[1].split(",")) {
                this.variableACBBDAO.getByAffichage(variableAffichage)
                        .ifPresent(variable -> currentList.add((VariableACBB) variable));
            }
        }
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS), variablesFluxTours);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES), variablesFluxChambre);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat(IMeteoDatatypeManager.CODE_DATATYPE_METEO), variablesMeteo);
        final DatesFormParamVO datesForm1ParamVO = new DatesFormParamVO(this.localizationManager);
        datesForm1ParamVO.setDateStart(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDedebut));
        datesForm1ParamVO.setDateEnd(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, datedeFin));
        metadatasMap.put(DatesFormParamVO.class.getSimpleName(), datesForm1ParamVO);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);
        final FluxMeteoParameterVO parameters = new FluxMeteoParameterVO(metadatasMap);
        try {
            ACBBTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException | BusinessException e) {
            return "false : " + e.getMessage();
        }
        return "true";

    }
}
