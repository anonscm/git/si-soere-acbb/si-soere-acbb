/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.flux;

import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.ValeurFluxTours;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoExtractionDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;

import java.time.LocalDate;
import java.util.List;

/**
 * The Interface IFluxToursDAO.
 */
public interface IFluxToursDAO extends IFluxMeteoExtractionDAO<ValeurFluxTours> {

    /**
     * Extract flux tours.
     *
     * @param selectedSites the selected sites
     * @param dateDebut     the date debut
     * @param dateFin       the date fin
     * @param user          the value of user
     * @return the java.util.List<org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours>
     */
    List<MesureFluxTours> extractFluxTours(List<INodeable> selectedSites, LocalDate dateDebut, LocalDate dateFin, IUser user);

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @return the java.lang.Long
     */
    Long sizeFluxTours(List<INodeable> selectedSites, LocalDate dateDebut, LocalDate dateFin);
}
