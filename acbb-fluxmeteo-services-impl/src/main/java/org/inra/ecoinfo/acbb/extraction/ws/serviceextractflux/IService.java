/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.ws.serviceextractflux;

import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.inra.ecoinfo.identification.ws.subservice.ISubService;
import org.inra.ecoinfo.ws.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IService extends ISubService {
    Response getTraitementsList(@Context HttpServletResponse response) throws BusinessException;
    Response extractFluxMeteo(
            @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            String startDate,
            String endDate,
            String treatments,
            String variables,
            Integer offset,
            Integer limit,
            InputStream requestBody) throws BusinessException;

}
