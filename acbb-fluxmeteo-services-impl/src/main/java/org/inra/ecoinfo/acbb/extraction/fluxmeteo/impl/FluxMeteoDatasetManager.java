/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl;

import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.extraction.flux.IFluxToursDAO;
import org.inra.ecoinfo.acbb.extraction.fluxchambres.IFluxChambreDAO;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoDatasetManager;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.meteo.IMeteoDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.*;

/**
 * The Class FluxMeteoDatasetManager.
 */
public class FluxMeteoDatasetManager extends DefaultDatasetManager implements
        IFluxMeteoDatasetManager {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final Map<String, IFluxMeteoExtractionDAO> extractionsDAO = new HashMap();

    /**
     * Instantiates a new flux meteo dataset manager.
     */
    public FluxMeteoDatasetManager() {
        super();
    }

    /**
     * Sets the flux chambre dao.
     *
     * @param fluxChambreDAO the new flux chambre dao
     */
    public final void setFluxChambreDAO(final IFluxChambreDAO fluxChambreDAO) {
        extractionsDAO.put(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES, fluxChambreDAO);
    }

    /**
     * Sets the flux tours dao.
     *
     * @param fluxToursDAO the new flux tours dao
     */
    public final void setFluxToursDAO(final IFluxToursDAO fluxToursDAO) {
        extractionsDAO.put(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS, fluxToursDAO);
    }

    /**
     * Sets the meteo dao.
     *
     * @param meteoDAO the new meteo dao
     */
    public final void setMeteoDAO(final IMeteoDAO meteoDAO) {
        extractionsDAO.put(IMeteoDatatypeManager.CODE_DATATYPE_METEO, meteoDAO);
    }

    /**
     * @param intervalsDate
     * @return
     */
    @Override
    public Collection<? extends SiteACBB> getAvailableSites(List<IntervalDate> intervalsDate) {
        final Set<SiteACBB> availablesSites = new HashSet();
        for (Map.Entry<String, IFluxMeteoExtractionDAO> entry : extractionsDAO.entrySet()) {
            IFluxMeteoExtractionDAO dao = entry.getValue();
            availablesSites.addAll(dao.getAvailablesSiteForPeriode(intervalsDate, policyManager.getCurrentUser()));
        }
        return availablesSites;
    }

    /**
     * @param sites
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByTreatmentAndDatesInterval(List<SiteACBB> sites, List<IntervalDate> intervals) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        for (Map.Entry<String, IFluxMeteoExtractionDAO> entry : extractionsDAO.entrySet()) {
            String datatype = entry.getKey();
            IFluxMeteoExtractionDAO interventionDAO = entry.getValue();
            variablesByDatatype.put(datatype, interventionDAO.getAvailablesVariablesBySites(sites, intervals, policyManager.getCurrentUser()));
        }
        return variablesByDatatype;

    }
}
