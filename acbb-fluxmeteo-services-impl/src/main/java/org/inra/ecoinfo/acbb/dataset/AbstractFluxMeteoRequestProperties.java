/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;

/**
 * @author ptcherniati
 */
public abstract class AbstractFluxMeteoRequestProperties extends AbstractRequestPropertiesACBB
        implements IRequestPropertiesFluxMeteo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public AbstractFluxMeteoRequestProperties() {
        super();
    }

}
