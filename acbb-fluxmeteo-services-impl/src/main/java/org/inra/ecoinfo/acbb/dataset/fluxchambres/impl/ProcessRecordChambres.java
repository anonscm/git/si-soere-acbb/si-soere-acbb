package org.inra.ecoinfo.acbb.dataset.fluxchambres.impl;

import com.Ostermiller.util.CSVParser;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IMesureFluxChambreDAO;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

/**
 * process the record of flux cahmbres files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
public class ProcessRecordChambres extends AbstractProcessRecord {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The mesure flux chambre dao.
     */
    protected IMesureFluxChambreDAO<MesureFluxChambre> mesureFluxChambreDAO;

    /**
     * Instantiates a new process record flux.
     */
    public ProcessRecordChambres() {
        super();
    }

    /**
     * Builds the mesure.
     *
     * @param mesureLines
     * @param versionFile
     * @param ligneEnErreur
     * @param errorsReport
     * @param requestPropertiesACBB
     * @throws PersistenceException       the persistence exception
     * @throws InsertionDatabaseException the insertion database exception
     * @throws DateTimeException
     * @link(List<LineRecord>) the mesure lines
     * @link(VersionFile) the version file
     * @link(SortedSet<LineRecord>) the ligne en erreur
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(List<LineRecord>) the mesure lines
     * @link(VersionFile) the version file
     * @link(SortedSet<LineRecord>) the ligne en erreur
     * @link(ErrorsReport) the errors report
     */
    void buildMesure(final Map<SuiviParcelle, List<LineRecord>> mesureLines, final VersionFile versionFile,
                     final SortedSet<LineRecord> ligneEnErreur, final ErrorsReport errorsReport,
                     final IRequestPropertiesACBB requestPropertiesACBB, Map<Variable, RealNode> dbRealNodesVariable) throws PersistenceException, DateTimeException {
        final SuiviParcelle suiviParcelle = requestPropertiesACBB.getSuiviParcelle();
        for (Entry<SuiviParcelle, List<LineRecord>> entry : mesureLines.entrySet()) {
            MesureFluxChambre mesureFluxChambre = null;
            for (LineRecord lineRecord : entry.getValue()) {
                if (mesureFluxChambre == null) {
                    mesureFluxChambre = new MesureFluxChambre();
                    mesureFluxChambre.setDate(lineRecord.getDate());
                    mesureFluxChambre.setSuiviParcelle(suiviParcelle);
                    mesureFluxChambre.setLigneFichierEchange(lineRecord.getOriginalLineNumber());
                    mesureFluxChambre.setNbrChambre(lineRecord.getNbrChambre());
                    mesureFluxChambre.setVersionFile(versionFile);
                }
                for (final VariableValue variableValue : lineRecord.getVariablesValues()) {
                    final ValeurFluxChambre ValeurFluxChambre = new ValeurFluxChambre();
                    DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
                    RealNode realNode = dbRealNodesVariable.get(dvu.getVariable());
                    ValeurFluxChambre.setRealNode(realNode);
                    if (variableValue.getValue() != null && variableValue.getValue().length() != 0) {
                        final Float value = Float.parseFloat(variableValue.getValue());
                        ValeurFluxChambre.setValeur(value);

                        ValeurFluxChambre.setMesure(mesureFluxChambre);
                        mesureFluxChambre.getValeurs().add(ValeurFluxChambre);
                    }
                }
                if (!mesureFluxChambre.getValeurs().isEmpty()) {
                    this.mesureFluxChambreDAO.saveOrUpdate(mesureFluxChambre);
                }
            }
        }
    }

    /**
     * Fill lines map.
     *
     * @param linesMap
     * @param line
     * @param date
     * @param sortedParcelles
     * @param requestProperties
     * @param requestPropertiesACBB
     * @param sortedParcelles
     * @param sortedParcelles
     * @throws PersistenceException the persistence exception
     * @link(Map<String,List<LineRecord>>) the lines map
     * @link(LineRecord) the line
     * @link(Date) the date
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(Map<String,List<LineRecord>>) the lines map
     * @link(LineRecord) the line
     * @link(Date) the date
     */
    void fillLinesMap(final Map<String, Map<SuiviParcelle, List<LineRecord>>> linesMap,
                      final LineRecord line,
                      final LocalDate date, IRequestPropertiesACBB requestPropertiesACBB,
                      SortedMap<LocalDate, SuiviParcelle> sortedParcelles) throws PersistenceException {
        SuiviParcelle suiviParcelle = null;
        try {
            suiviParcelle = ((TreeMap<LocalDate, SuiviParcelle>) sortedParcelles).headMap(
                    line.getDate(), true).get(sortedParcelles.lastKey());
        } catch (Exception e) {
            logger.debug("can't fill lines map", e);
        }
        requestPropertiesACBB.setSuiviParcelle(suiviParcelle);
        if (date != null && suiviParcelle != null) {
            final String mesureKey = date.toString().concat(suiviParcelle.getId().toString());
            if (!linesMap.containsKey(mesureKey)) {
                linesMap.put(mesureKey, new HashMap());
            }
            if (!linesMap.get(mesureKey).containsKey(suiviParcelle)) {
                linesMap.get(mesureKey).put(suiviParcelle, new LinkedList());
            }
            linesMap.get(mesureKey).get(suiviParcelle).add(line);
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties,
                              final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        super.processRecord(parser, versionFile, requestProperties, fileEncoding, datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        SortedMap<LocalDate, SuiviParcelle> sortedParcelles;
        try {
            sortedParcelles = this.suiviParcelleDAO.getSortedSuivisParcelles().get(
                    requestProperties.getParcelle());
        } catch (DateTimeException e1) {
            throw new BusinessException("can't retrieve suivi-parcelle", e1);
        }
        try {
            // Création du fichier de dépôt en base

            String[] values = null;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            long lineCount = datasetDescriptor.getEnTete();
            final Map<String, Map<SuiviParcelle, List<LineRecord>>> mesuresMapLines = new HashMap();
            while ((values = parser.getLine()) != null) {
                final CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                final List<VariableValue> variablesValues = new LinkedList();
                // On parcourt chaque colonne d'une ligne
                final LocalDate date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
                final int nbrChambre = Integer.parseInt(cleanerValues.nextToken());
                for (int actualVariableArray = 2; actualVariableArray < values.length; actualVariableArray++) {
                    if (values.length > dbVariables.size() + 2) {
                        errorsReport.addErrorMessage(String.format(this.localizationManager
                                        .getMessage("org.inra.ecoinfo.refdata.impl.messages",
                                                "PROPERTY_MSG_ERROR_UNEXPECTED_NUMBER_OF_COLUMNS"),
                                values.length, dbVariables.size() + 2));
                        return;
                    }
                    variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - 2),
                            values[actualVariableArray].replaceAll(RecorderACBB.CST_SPACE,
                                    StringUtils.EMPTY)));
                }
                final LineRecord line = new LineRecord(date, nbrChambre, variablesValues, lineCount);
                this.fillLinesMap(mesuresMapLines, line, date, requestProperties, sortedParcelles);
            }
            final SortedSet<LineRecord> ligneEnErreur = new TreeSet();
            int i = 0;
            Map<Variable, RealNode> dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
            Iterator<Entry<String, Map<SuiviParcelle, List<LineRecord>>>> iterator = mesuresMapLines.entrySet().iterator();
            while (iterator.hasNext()) {
                Entry<String, Map<SuiviParcelle, List<LineRecord>>> mesureKeyeEntry = iterator.next();
                final Map<SuiviParcelle, List<LineRecord>> mesureLines = mesuresMapLines.get(mesureKeyeEntry.getKey());
                this.buildMesure(mesureLines, versionFile, ligneEnErreur, errorsReport,
                        requestProperties, dvus);
                if (++i % 50 == 0) {
                    datatypeVariableUniteACBBDAO.flush();
                    versionFile = versionFileDAO.merge(versionFile);
                    dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
                    requestProperties.setSuiviParcelle(suiviParcelleDAO.merge(requestProperties.getSuiviParcelle()));
                }
                iterator.remove();
            }
            if (!ligneEnErreur.isEmpty()) {
                for (Iterator<LineRecord> it = ligneEnErreur.iterator(); it.hasNext(); ) {
                    LineRecord line = it.next();
                    try {
                        final Object[] doublon = this.mesureFluxChambreDAO
                                .getLinePublicationNameDoublon(line.getDate());
                        String formatDate;
                        formatDate = DateUtil.getUTCDateTextFromLocalDateTime(line.getDate(), DateUtil.DD_MM_YYYY);
                        errorsReport
                                .addErrorMessage(String.format(RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_DOUBLON_DATE),
                                        line.getOriginalLineNumber().intValue(), formatDate,
                                        doublon[0].toString(), ((Number) doublon[1]).intValue()));
                    } catch (final Exception z) {
                        errorsReport.addErrorMessage(String.format(RecorderACBB
                                        .getACBBMessage(RecorderACBB.PROPERTY_MSG_ERROR_INSERTION_MEASURE),
                                line.getOriginalLineNumber().intValue()));
                    }
                    it.remove();
                }
            }
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | NumberFormatException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Sets the mesure flux chambre dao.
     *
     * @param mesureFluxChambreDAO the new mesure flux chambre dao
     */
    public void setMesureFluxChambreDAO(
            final IMesureFluxChambreDAO<MesureFluxChambre> mesureFluxChambreDAO) {
        this.mesureFluxChambreDAO = mesureFluxChambreDAO;
    }

    /**
     * The Class LineRecord.
     * <p>
     * record one line of flux data
     */
    static class LineRecord implements Serializable {

        private static final long serialVersionUID = 1L;
        /**
         * The date @link(Date).
         */
        LocalDate date;
        /**
         * The nbr chambre @link(int).
         */
        int nbrChambre;
        /**
         * The original line number <long>.
         */
        Long originalLineNumber;
        /**
         * The variables values @link(List<VariableValue>).
         */
        List<VariableValue> variablesValues;

        /**
         * Instantiates a new line record.
         */
        LineRecord() {
        }

        /**
         * Instantiates a new line record.
         *
         * @param date               the date
         * @param nbrChambre         the nbr chambre
         * @param variablesValues    the variables values
         * @param originalLineNumber the original line number
         */
        LineRecord(final LocalDate date, final int nbrChambre,
                   final List<VariableValue> variablesValues, final Long originalLineNumber) {
            super();

            this.date = date;
            this.nbrChambre = nbrChambre;
            this.variablesValues = variablesValues;
            this.originalLineNumber = originalLineNumber;
        }

        /**
         * Copy.
         *
         * @param line the line
         */
        public void copy(final LineRecord line) {
            this.date = line.getDate();
            this.variablesValues = line.getVariablesValues();
            this.originalLineNumber = line.getOriginalLineNumber();
        }

        /**
         * Gets the date.
         *
         * @return the date
         */
        public LocalDate getDate() {
            return this.date;
        }

        /**
         * Gets the nbr chambre.
         *
         * @return the nbr chambre
         */
        public int getNbrChambre() {
            return this.nbrChambre;
        }

        /**
         * Sets the nbr chambre.
         *
         * @param nbrChambre the new nbr chambre
         */
        public void setNbrChambre(final int nbrChambre) {
            this.nbrChambre = nbrChambre;
        }

        /**
         * Gets the original line number.
         *
         * @return the original line number
         */
        public Long getOriginalLineNumber() {
            return this.originalLineNumber;
        }

        /**
         * Gets the variables values.
         *
         * @return the variables values
         */
        public List<VariableValue> getVariablesValues() {
            return this.variablesValues;
        }

        /**
         * Sets the variables values.
         *
         * @param variablesValues the new variables values
         */
        public void setVariablesValues(final List<VariableValue> variablesValues) {
            this.variablesValues = variablesValues;
        }
    }

}
