/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * The Interface IMesureFluxToursDAO.
 *
 * @param <T> the generic type
 */
public interface IMesureFluxToursDAO<T> extends IDAO<MesureFluxTours> {

    /**
     * Gets the line publication name doublon.
     *
     * @param date the date
     * @param time the time
     * @return the line publication name doublon
     */
    Object[] getLinePublicationNameDoublon(LocalDate date, LocalTime time);
}
