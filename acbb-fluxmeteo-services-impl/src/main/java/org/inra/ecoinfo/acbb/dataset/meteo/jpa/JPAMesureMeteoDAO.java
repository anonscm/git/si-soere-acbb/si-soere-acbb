/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.meteo.IMesureMeteoDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo_;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo_;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * The Class JPAMesureMeteoDAO.
 */
public class JPAMesureMeteoDAO extends AbstractJPADAO<MesureMeteo> implements
        IMesureMeteoDAO<MesureMeteo> {

    /**
     * Gets the line publication name doublon.
     *
     * @param dateTime
     * @return the line publication name doublon
     * @link(Date)
     * @link(Date) the heure
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final LocalDateTime dateTime) {
        LocalDate date;
        LocalTime time;
        try {
            date = dateTime.toLocalDate();
            time = dateTime.toLocalTime();
        } catch (DateTimeException e) {
            return null;
        }
        CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
        Root<MesureMeteo> m = query.from(MesureMeteo.class);
        query.where(
                builder.equal(m.join(MesureMeteo_.sequenceMeteo).get(SequenceMeteo_.dateMesure), date),
                builder.equal(m.get(MesureMeteo_.heure), time)
        );
        query.multiselect(
                m.join(MesureMeteo_.sequenceMeteo).join(SequenceMeteo_.version).join(VersionFile_.dataset).get(Dataset_.id),
                m.get(MesureMeteo_.ligneFichierEchange));
        return getFirstOrNull(query);
    }

}
