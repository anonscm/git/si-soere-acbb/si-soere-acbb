/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesFluxMeteo;

/**
 * The Interface IRequestPropertiesFluxChambre.
 */
public interface IRequestPropertiesFluxChambre extends IRequestPropertiesFluxMeteo {

}
