package org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl;

import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class FluxMeteoExtractor.
 */
public class FluxMeteoExtractor extends MO implements IExtractor {

    // extraction
    /**
     * The Constant CST_RESULTS.
     */
    public static final String CST_RESULTS = "extractionResults";
    /**
     * The Constant CST_RESULT_EXTRACTION_CHAMBRE_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_CHAMBRE_CODE = "extractionResultChambre";
    /**
     * The Constant CST_RESULT_EXTRACTION_TOURS_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_TOURS_CODE = "extractionResultTours";
    /**
     * The Constant CST_RESULT_EXTRACTION_METEO_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_METEO_CODE = "extractionResultMeteo";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.fluxmeteo.messages";

    /**
     * The Constant MSG_EXTRACTION_ABORTED @link(String).
     */
    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     * The Constant PROPERTY_MSG_BADS_RIGHTS @link(String).
     */
    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";

    /**
     * The flux chambre extractor.
     */
    IExtractor fluxChambreExtractor;

    /**
     * The flux tours extractor.
     */
    IExtractor fluxToursExtractor;

    /**
     * The meteo extractor.
     */
    IExtractor meteoExtractor;

    ISuiviParcelleDAO suiviParcelleDAO;

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IExtractor#extract(org.inra.ecoinfo.extraction
     *                           .IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings("unchecked")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        statusBar.setProgress(0);
        int extractionResult = 3;
        try {
            if (!CollectionUtils.isEmpty((Collection<Variable>) ((DefaultParameter) parameters).getParameters().get(
                    Variable.class.getSimpleName().toLowerCase().concat(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES)))) {
                this.fluxChambreExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        statusBar.setProgress(17);
        try {
            if (!CollectionUtils.isEmpty((Collection<Variable>) ((DefaultParameter) parameters).getParameters().get(
                    Variable.class.getSimpleName().toLowerCase().concat(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS)))) {
                this.fluxToursExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        statusBar.setProgress(34);
        try {
            if (!CollectionUtils.isEmpty((Collection<Variable>) ((DefaultParameter) parameters).getParameters().get(
                    Variable.class.getSimpleName().toLowerCase().concat(IMeteoDatatypeManager.CODE_DATATYPE_METEO)))) {
                this.meteoExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        statusBar.setProgress(50);
        if (extractionResult == 0) {
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    FluxMeteoExtractor.BUNDLE_SOURCE_PATH, this.localizationManager.getMessage(
                            FluxMeteoExtractor.BUNDLE_SOURCE_PATH,
                            FluxMeteoExtractor.MSG_EXTRACTION_ABORTED))), Notification.ERROR,
                    FluxMeteoExtractor.PROPERTY_MSG_BADS_RIGHTS, (Utilisateur) this.policyManager
                            .getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> sortedSuivisParcelles = new TreeMap();
        try {
            sortedSuivisParcelles = this.suiviParcelleDAO.getSortedSuivisParcelles();
        } catch (DateTimeException e) {
            throw new BusinessException("can't load suivi-parcelle", e);
        }
        parameters.getParameters().put(SuiviParcelle.class.getSimpleName(), sortedSuivisParcelles);
    }

    /**
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = fluxToursExtractor.getExtractionSize(parameters);
        size += fluxChambreExtractor.getExtractionSize(parameters);
        size += meteoExtractor.getExtractionSize(parameters);
        return size;
    }

    /**
     * Sets the extraction.
     *
     * @param extraction the new extraction
     * @see org.inra.ecoinfo.extraction.IExtractor#setExtraction(org.inra.ecoinfo
     * .config.Extraction)
     */
    @Override
    public final void setExtraction(final Extraction extraction) {

    }

    /**
     * Sets the flux chambre extractor.
     *
     * @param fluxChambreExtractor the new flux chambre extractor
     */
    public final void setFluxChambreExtractor(final IExtractor fluxChambreExtractor) {
        this.fluxChambreExtractor = fluxChambreExtractor;
    }

    /**
     * Sets the flux tours extractor.
     *
     * @param fluxToursExtractor the new flux tours extractor
     */
    public final void setFluxToursExtractor(final IExtractor fluxToursExtractor) {
        this.fluxToursExtractor = fluxToursExtractor;
    }

    /**
     * Sets the meteo extractor.
     *
     * @param meteoExtractor the new meteo extractor
     */
    public final void setMeteoExtractor(final IExtractor meteoExtractor) {
        this.meteoExtractor = meteoExtractor;
    }

    /**
     * @param suiviParcelleDAO the suiviParcelleDAO to set
     */
    public void setSuiviParcelleDAO(ISuiviParcelleDAO suiviParcelleDAO) {
        this.suiviParcelleDAO = suiviParcelleDAO;
    }
}
