/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.meteo;

import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoExtractionDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.List;

/**
 * The Interface IMeteoDAO.
 */
public interface IMeteoDAO extends IFluxMeteoExtractionDAO<ValeurMeteo> {

    /**
     * Extract meteo.
     *
     * @param selectedSites the selected sites
     * @param dateDebut     the date debut
     * @param dateFin       the date fin
     * @param user          the value of user
     * @return the java.util.List<org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo>
     * @throws PersistenceException the persistence exception
     */
    List<MesureMeteo> extractMeteo(List<INodeable> selectedSites, LocalDate dateDebut, LocalDate dateFin, IUser user)
            throws PersistenceException;

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @return
     */
    Long sizeMeteo(List<INodeable> selectedSites, LocalDate dateDebut, LocalDate dateFin);
}
