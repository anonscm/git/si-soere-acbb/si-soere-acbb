/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours_;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * The Class JPASequenceFluxToursDAO.
 */
public class JPASequenceFluxToursDAO extends AbstractJPADAO<SequenceFluxTours> implements
        ISequenceFluxToursDAO<SequenceFluxTours> {

    /**
     * Gets the by date and suivi parcelle id.
     *
     * @param version
     * @param date
     * @param parcelleId
     * @return the by date and suivi parcelle id @see
     * org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO#
     * getByDateAndSuiviParcelleId(java.time.LocalDateTime, java.lang.Long)
     * @link(Date) the date
     * @link(Long) the parcelle id
     */
    @Override
    public Optional<SequenceFluxTours> getByVersionAndDateAndSuiviParcelleId(final VersionFile version,
                                                                             final LocalDate date, final Long parcelleId) {
        CriteriaQuery<SequenceFluxTours> query = builder.createQuery(SequenceFluxTours.class);
        Root<SequenceFluxTours> s = query.from(SequenceFluxTours.class);
        Join<SequenceFluxTours, Parcelle> parcelle = s.join(SequenceFluxTours_.parcelle);
        query
                .select(s)
                .where(
                        builder.equal(s.join(SequenceFluxTours_.version), version),
                        builder.equal(parcelle.get(Parcelle_.id), parcelleId),
                        builder.equal(s.get(SequenceFluxTours_.dateMesure), date)
                );
        return getOptional(query);
    }

    /**
     * Gets the line publication name doublon.
     *
     * @param date
     * @return the line publication name doublon @see
     * org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO#
     * getLinePublicationNameDoublon(java.time.LocalDateTime)
     * @link(Date) the date
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final LocalDateTime date) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @param date
     * @param versionFile
     * @return
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    public Optional<SequenceFluxTours> getSequence(LocalDate date, VersionFile versionFile) {
        CriteriaQuery<SequenceFluxTours> query = builder.createQuery(SequenceFluxTours.class);
        Root<SequenceFluxTours> s = query.from(SequenceFluxTours.class);
        Join<SequenceFluxTours, VersionFile> version = s.join(SequenceFluxTours_.version);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> rn = dataset.join(Dataset_.realNode);
        query
                .select(s)
                .where(
                        builder.equal(rn, versionFile.getDataset().getRealNode()),
                        builder.equal(s.get(SequenceFluxTours_.dateMesure), date)
                );
        return getOptional(query);
    }

}
