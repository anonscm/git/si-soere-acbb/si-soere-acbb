/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.flux.impl;

import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.flux.IFluxToursDAO;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoExtractor;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class FluxToursExtractor.
 */
public class FluxToursExtractor extends AbstractExtractor {

    /**
     * The Constant FLUX_TOURS.
     */
    public static final String FLUX_TOURS = IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS;

    /**
     * The Constant MAP_INDEX_FLUXTOURS.
     */
    protected static final String MAP_INDEX_FLUXTOURS = "fluxtours";

    /**
     * The Constant MAP_INDEX_0.
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     * The flux tours dao.
     */
    private IFluxToursDAO fluxToursDAO;

    /**
     * @param parameters
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#extract(org.inra.ecoinfo
     * .extraction.IParameter)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        if (((FluxMeteoParameterVO) parameters).getResults().get(
                FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE) == null
                || ((FluxMeteoParameterVO) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE).isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(
                    FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE, resultsDatasMap);
        } else {
            ((FluxMeteoParameterVO) parameters)
                    .getResults()
                    .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE)
                    .put(FluxToursExtractor.FLUX_TOURS,
                            resultsDatasMap.get(FluxToursExtractor.MAP_INDEX_FLUXTOURS));
        }
        ((FluxMeteoParameterVO) parameters)
                .getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE)
                .put(FluxToursExtractor.MAP_INDEX_0,
                        resultsDatasMap.get(FluxToursExtractor.MAP_INDEX_FLUXTOURS));
    }

    /**
     * @param requestMetadatasMap
     * @return @throws
     * org.inra.ecoinfo.extraction.exception.NoExtractionResultException
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#extractDatas(java .
     * util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws NoExtractionResultException, BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final DatesFormParamVO datesForm1ParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<INodeable> selectedSites = ((List<SiteACBB>) requestMetadatasMap
                    .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList())).stream().collect(Collectors.toList());
            final LocalDate dateDebut = datesForm1ParamVO.getDateStart();
            final LocalDate dateFin = datesForm1ParamVO.getDateEnd();
            final List<MesureFluxTours> mesuresFluxTours = this.fluxToursDAO.extractFluxTours(
                    selectedSites, dateDebut, dateFin, policyManager.getCurrentUser());
            if (mesuresFluxTours == null || mesuresFluxTours.isEmpty()) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
            extractedDatasMap.put(FluxToursExtractor.MAP_INDEX_FLUXTOURS, mesuresFluxTours);
        } catch (final DateTimeException | BusinessException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    /**
     * @param requestMetadatasMap
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     * (java.util.Map)
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);

    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
    }

    /**
     * Sets the flux tours dao.
     *
     * @param fluxToursDAO the new flux tours dao
     */
    public final void setFluxToursDAO(final IFluxToursDAO fluxToursDAO) {
        this.fluxToursDAO = fluxToursDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
    }

    /**
     * Sort selected variables.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort(
                (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(
                        IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS), new ArrayList()), new Comparator<Variable>() {

                    @Override
                    public int compare(final Variable o1, final Variable o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });

    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesForm1ParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<INodeable> selectedSites = ((List<SiteACBB>) requestMetadatasMap
                    .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList())).stream().collect(Collectors.toList());
            final LocalDate dateDebut = datesForm1ParamVO.getDateStart();
            final LocalDate dateFin = datesForm1ParamVO.getDateEnd();
            if (selectedSites.isEmpty()) {
                return 0L;
            }
            return fluxToursDAO.sizeFluxTours(selectedSites, dateDebut, dateFin);
        } catch (DateTimeException ex) {
            return -1l;
        }

    }
}
