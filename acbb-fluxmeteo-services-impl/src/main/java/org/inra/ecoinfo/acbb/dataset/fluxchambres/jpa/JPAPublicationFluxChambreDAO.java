/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres.jpa;


import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre_;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationFluxChambreDAO.
 */
public class JPAPublicationFluxChambreDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        deleteValeurs(version);
        deleteMesures(version);
    }

    private void deleteValeurs(final VersionFile version) {
        CriteriaDelete<ValeurFluxChambre> deleteValeur = builder.createCriteriaDelete(ValeurFluxChambre.class);
        Root<ValeurFluxChambre> v = deleteValeur.from(ValeurFluxChambre.class);
        Subquery<MesureFluxChambre> subqueryMesure = deleteValeur.subquery(MesureFluxChambre.class);
        Root<MesureFluxChambre> m = subqueryMesure.from(MesureFluxChambre.class);
        Path<VersionFile> ver = m.get(MesureFluxChambre_.versionFile);
        subqueryMesure
                .select(m)
                .where(builder.equal(ver, version));
        Path<MesureFluxChambre> mDb = v.get(ValeurFluxChambre_.mesure);
        deleteValeur
                .where(mDb.in(subqueryMesure));
        delete(deleteValeur);
        flush();
    }

    private void deleteMesures(final VersionFile version) {
        CriteriaDelete<MesureFluxChambre> deleteMesure = builder.createCriteriaDelete(MesureFluxChambre.class);
        Root<MesureFluxChambre> m = deleteMesure.from(MesureFluxChambre.class);
        Path<VersionFile> ver = m.get(MesureFluxChambre_.versionFile);
        deleteMesure
                .where(builder.equal(ver, version));
        delete(deleteMesure);
        flush();
    }

}
