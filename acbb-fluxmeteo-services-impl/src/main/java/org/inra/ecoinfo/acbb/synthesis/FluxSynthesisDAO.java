/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.acbb.dataset.flux.entity.*;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.synthesis.fluxtours.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.fluxtours.SynthesisValue;

/**
 * @author tcherniatinsky
 */
public class FluxSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurFluxTours> v = query.from(ValeurFluxTours.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<MesureFluxTours, SequenceFluxTours> s = v.join(ValeurFluxTours_.mesureFluxTours).join(MesureFluxTours_.sequenceFluxTours);
        Join<ValeurFluxTours, RealNode> varRn = v.join(ValeurFluxTours_.realNode);
        Join<RealNode, RealNode> parcelleRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = parcelleRn.join(RealNode_.parent);
        Path<String> parcelleCode = parcelleRn.join(RealNode_.nodeable).get(Nodeable_.code);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurFluxTours_.value);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(SequenceFluxTours_.dateMesure);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query.select(
                builder.construct(
                        SynthesisValue.class,
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        builder.avg(valeur),
                        idNode
                )
        )
                .where(
                        builder.equal(node.join(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

}
