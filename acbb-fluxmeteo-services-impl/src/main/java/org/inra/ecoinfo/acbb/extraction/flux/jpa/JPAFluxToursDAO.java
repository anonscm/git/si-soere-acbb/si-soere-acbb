/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.flux.jpa;

import org.inra.ecoinfo.acbb.dataset.flux.entity.*;
import org.inra.ecoinfo.acbb.extraction.flux.IFluxToursDAO;
import org.inra.ecoinfo.acbb.extraction.jpa.JPAAbstractExtraction;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.fluxtours.SynthesisValue;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class JPAFluxToursDAO.
 */
public class JPAFluxToursDAO extends JPAAbstractExtraction<ValeurFluxTours> implements IFluxToursDAO {

    /**
     *
     */
    public JPAFluxToursDAO() {
        super(SynthesisValue.class, true);
    }

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @param user
     * @param isCount
     * @return
     */
    public CriteriaQuery buildQuery(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin, IUser user, boolean isCount) {

        CriteriaQuery criteria;
        if (isCount) {
            criteria = builder.createQuery(Long.class);
        } else {
            criteria = builder.createQuery(MesureFluxTours.class);
        }
        Root<ValeurFluxTours> vft = criteria.from(ValeurFluxTours.class);
        Join<ValeurFluxTours, MesureFluxTours> mft = vft.join(ValeurFluxTours_.mesureFluxTours);
        Join<MesureFluxTours, SequenceFluxTours> sft = mft.join(MesureFluxTours_.sequenceFluxTours);
        Join<ValeurFluxTours, RealNode> rnVariable = vft.join(ValeurFluxTours_.realNode);
        Join<RealNode, RealNode> rnDatatype = rnVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> rnTheme = rnDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> rnParcelle = rnTheme.join(RealNode_.parent);
        Join<RealNode, RealNode> rnSite = rnParcelle.join(RealNode_.parent);
        Join<RealNode, Nodeable> ndb = rnSite.join(RealNode_.nodeable);
        Root<DatatypeVariableUniteACBB> dvu = criteria.from(DatatypeVariableUniteACBB.class);
        Root<NodeDataSet> vns = criteria.from(NodeDataSet.class);
        ArrayList<Predicate> predicatesAnd = new ArrayList();
        predicatesAnd.add(builder.equal(rnVariable, vns.join(NodeDataSet_.realNode)));
        predicatesAnd.add(builder.equal(dvu, rnVariable.join(RealNode_.nodeable)));
        predicatesAnd.add(ndb.in(selectedSites));
        predicatesAnd.add(builder.between(sft.<LocalDate>get(SequenceFluxTours_.dateMesure), dateDebut, dateFin));
        final Path<LocalDate> dateMesure = sft.<LocalDate>get(SequenceFluxTours_.dateMesure);
        if (!isCount) {
            addRestrictiveRequestOnRoles(user, criteria, predicatesAnd, builder, vns, dateMesure);
        }
        Predicate where = builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()]));
        criteria.where(where);
        if (isCount) {
            criteria.select(builder.countDistinct(mft));
        } else {
            criteria.select(mft);
            List<Order> orders = new ArrayList();
            orders.add(builder.asc(ndb));
            orders.add(builder.asc(sft.get(SequenceFluxTours_.dateMesure)));
            criteria.orderBy(orders);
        }
        return criteria;
    }

    /**
     * Extract flux tours.
     *
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @param user          the value of user
     * @return the
     * java.util.List<org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours>
     * @link(List<SiteACBB>)
     * @link(Date)
     * @link(Date)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<MesureFluxTours> extractFluxTours(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin, final IUser user) {
        CriteriaQuery<MesureFluxTours> query = buildQuery(selectedSites, dateDebut, dateFin, user, false);
        return getResultList(query);

    }

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @return the java.lang.Long
     */
    @Override
    public Long sizeFluxTours(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin) {
        CriteriaQuery<Long> criteria = buildQuery(selectedSites, dateDebut, dateFin, null, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected boolean isParcelleDatatype() {
        return true;
    }

}
