/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.meteo;

import org.inra.ecoinfo.acbb.dataset.flux.entity.ValeurFluxTours;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.refdata.variable.Variable;

import java.util.List;

/**
 * The Interface IMeteoParameter.
 */
public interface IMeteoParameter extends IParameter {

    /**
     * Gets the dates form1 param vo.
     *
     * @return the dates form1 param vo
     */
    DatesFormParamVO getDatesYearsContinuousFormParamVO();

    /**
     * Gets the selected flux tours variables.
     *
     * @return the selected flux tours variables
     */
    List<Variable> getSelectedFluxToursVariables();

    /**
     * Sets the selected flux tours variables.
     *
     * @param selectedVariables the new selected flux tours variables
     */
    void setSelectedFluxToursVariables(List<Variable> selectedVariables);

    /**
     * Gets the selected sites.
     *
     * @return the selected sites
     */
    List<SiteACBB> getSelectedSites();

    /**
     * Gets the valeur flux tours.
     *
     * @return the valeur flux tours
     */
    List<ValeurFluxTours> getValeurFluxTours();

    /**
     * Sets the valeur flux tours.
     *
     * @param valeurFluxTours the new valeur flux tours
     */
    void setValeurFluxTours(List<ValeurFluxTours> valeurFluxTours);
}
