/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.meteo.jpa;

import org.inra.ecoinfo.acbb.dataset.meteo.entity.*;
import org.inra.ecoinfo.acbb.extraction.jpa.JPAAbstractExtraction;
import org.inra.ecoinfo.acbb.extraction.meteo.IMeteoDAO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.synthesis.meteorologie.SynthesisValue;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class JPAMeteoDAO.
 */
public class JPAMeteoDAO extends JPAAbstractExtraction<ValeurMeteo> implements IMeteoDAO {

    /**
     *
     */
    public JPAMeteoDAO() {
        super(SynthesisValue.class, false);
    }

    /**
     * Extract meteo.
     *
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @param user          the value of user
     * @return the
     * java.util.List<org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo>
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.extraction.meteo.IMeteoDAO#extractMeteo(java.util.
     *                              List, java.time.LocalDateTime, java.time.LocalDateTime)
     * @link(List<SiteACBB>)
     * @link(Date)
     * @link(Date)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<MesureMeteo> extractMeteo(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin, IUser user) throws PersistenceException {
        CriteriaQuery<MesureMeteo> criteria = buildQuery(selectedSites, dateDebut, dateFin, user, false);
        return entityManager.createQuery(criteria).getResultList();
    }

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @param user
     * @param isCount
     * @return
     */
    @SuppressWarnings("unchecked")
    public CriteriaQuery buildQuery(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin, IUser user, boolean isCount) {

        CriteriaQuery criteria;
        if (isCount) {
            criteria = builder.createQuery(Long.class);
        } else {
            criteria = builder.createQuery(MesureMeteo.class);
        }
        Root<ValeurMeteo> vm = criteria.from(ValeurMeteo.class);
        Join<ValeurMeteo, MesureMeteo> mm = vm.join(ValeurMeteo_.mesureMeteo);
        Join<MesureMeteo, SequenceMeteo> sm = mm.join(MesureMeteo_.sequenceMeteo);
        Join<ValeurMeteo, RealNode> rnVariable = vm.join(ValeurMeteo_.realNode);
        Join<RealNode, RealNode> rnDatatype = rnVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> rnTheme = rnDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> rnSite = rnTheme.join(RealNode_.parent);
        Join<RealNode, Nodeable> ndb = rnSite.join(RealNode_.nodeable);
        Root<DatatypeVariableUniteACBB> dvu = criteria.from(DatatypeVariableUniteACBB.class);
        Root<NodeDataSet> vns = criteria.from(NodeDataSet.class);
        ArrayList<Predicate> predicatesAnd = new ArrayList();
        predicatesAnd.add(builder.equal(rnVariable, vns.join(NodeDataSet_.realNode)));
        predicatesAnd.add(builder.equal(dvu, rnVariable.join(RealNode_.nodeable)));
        predicatesAnd.add(ndb.in(selectedSites));
        predicatesAnd.add(builder.between(sm.<LocalDate>get(SequenceMeteo_.dateMesure), dateDebut, dateFin));
        final Path<LocalDate> dateMesure = sm.<LocalDate>get(SequenceMeteo_.dateMesure);
        if (!isCount) {
            addRestrictiveRequestOnRoles(user, criteria, predicatesAnd, builder, vns, dateMesure);
        }
        Predicate where = builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()]));
        criteria.where(where);
        if (isCount) {
            criteria.select(builder.countDistinct(sm));
        } else {
            List<Order> orders = new ArrayList();
            orders.add(builder.asc(ndb));
            orders.add(builder.asc(sm.get(SequenceMeteo_.dateMesure)));
            criteria.orderBy(orders);
            criteria.select(mm);
        }
        return criteria;
    }

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Long sizeMeteo(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin) {
        CriteriaQuery<Long> criteria = buildQuery(selectedSites, dateDebut, dateFin, null, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;

    }

    /**
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected boolean isParcelleDatatype() {
        return false;
    }

}
