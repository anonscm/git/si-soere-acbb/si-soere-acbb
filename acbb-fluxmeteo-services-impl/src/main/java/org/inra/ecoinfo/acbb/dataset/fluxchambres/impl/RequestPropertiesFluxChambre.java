/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres.impl;

import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.AbstractFluxMeteoRequestProperties;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IRequestPropertiesFluxChambre;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * The Class RequestPropertiesFluxChambre.
 */
public class RequestPropertiesFluxChambre extends AbstractFluxMeteoRequestProperties implements
        IRequestPropertiesFluxChambre, Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * Gets the nom de fichier.
     *
     * @param version
     * @return the nom de fichier @see
     * org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#
     * getNomDeFichier(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public String getNomDeFichier(final VersionFile version) {
        if (version == null) {
            return null;
        }
        final Dataset dataset = version.getDataset();
        if (ACBBUtils.getSiteFromDataset(dataset) == null
                || ACBBUtils.getDatatypeFromDataset(version.getDataset()) == null
                || dataset.getDateDebutPeriode() == null || dataset.getDateFinPeriode() == null) {
            return StringUtils.EMPTY;
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier.append(ACBBUtils.getSiteFromDataset(dataset).getPath())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES)
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DateUtil.DD_MM_YYYY))
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DateUtil.DD_MM_YYYY))
                    .append(".")
                    .append(IRequestPropertiesACBB.FORMAT_FILE);
        } catch (final Exception e) {
            return StringUtils.EMPTY;
        }
        return nomFichier.toString();
    }

    /**
     * @return
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateChambres();
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#setDateDeFin
     * (java.time.LocalDateTime)
     */
    @Override
    public void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.DAYS, 1);
    }

    /**
     * Test dates.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#testDates
     * (org.inra.ecoinfo.dataset.BadsFormatsReport)
     */
    @Override
    public void testDates(final BadsFormatsReport badsFormatsReport) {
        super.testDates(badsFormatsReport);
        this.testNonMissingDates(badsFormatsReport);
    }
}
