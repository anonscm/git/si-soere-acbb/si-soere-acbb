/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.IRequestPropertiesMeteo;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.IOException;

/**
 * The Class TestHeadersFlux.
 *
 * @author Tcherniatinsky Philippe
 */
public class TestHeadersMeteo extends GenericTestHeader {
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new test headers flux.
     */
    public TestHeadersMeteo() {
        super();
    }

    /**
     *
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param datatypeName
     * @return
     * @throws IOException
     */
    @Override
    protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                long lineNumber, String datatypeName) throws IOException {
        long finalLineNumber = lineNumber;
        String[] values;
        values = parser.getLine();
        finalLineNumber++;
        String dataType = null;
        try {
            if (values != null && values.length > 1) {
                dataType = Utils.createCodeFromString(values[1]);
            }
            if (dataType == null
                    || !(dataType.equalsIgnoreCase(datatypeName) || IMeteoDatatypeManager.CODE_SHORT_DATATYPE_METEO
                    .equalsIgnoreCase(datatypeName))) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATATYPE),
                        finalLineNumber, 2, datatypeName)));
            }
        } catch (final IndexOutOfBoundsException e1) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_DATATYPE),
                    finalLineNumber, 2, datatypeName)));
        }
        return finalLineNumber;
    }

    /**
     * Test headers.
     *
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile) the version file
     * @param requestProperties
     * @link(IRequestPropertiesACBB) the request properties
     * @param encoding
     * @link(String) the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @return the long
     * @throws BusinessException
     *             the business exception @see
     *             org.inra.ecoinfo.acbb.dataset.ITestHeaders#testHeaders(com.Ostermiller
     *             .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *             org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *             org.inra.ecoinfo.dataset.BadsFormatsReport,
     *             org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final String encoding,
                            final BadsFormatsReport badsFormatsReport, final DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException {
        super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport,
                datasetDescriptor);
        final IRequestPropertiesMeteo requestPropertiesMeteo = (IRequestPropertiesMeteo) requestProperties;
        requestPropertiesMeteo.initDate();
        long lineNumber = 0;
        try {
            lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber,
                    requestProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    IMeteoDatatypeManager.CODE_DATATYPE_METEO);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, requestProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, requestProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 1);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, requestProperties);
            lineNumber = this.jumpLines(parser, lineNumber, 3);
        } catch (final IOException e) {
            this.getLogger().debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }

}
