/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl;

import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.extraction.AbstractRequestReminder;

/**
 * The Class FluxMeteoRequestReminderOutputBuilder.
 */
public class FluxMeteoRequestReminderOutputBuilder extends AbstractRequestReminder {

    /**
     * The cst result extraction code.
     */
    public static final String CST_RESULT_EXTRACTION_CODE = "fluxMeteoRequestReminder";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.fluxmeteo.messages";

    /**
     * The Constant PROPERTY_MSG_EXTRACTION_COMMENTS @link(String).
     */
    static final String PROPERTY_MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    /**
     * The Constant PROPERTY_MSG_EXTRACTION_DATE @link(String).
     */
    static final String PROPERTY_MSG_EXTRACTION_DATE = "PROPERTY_MSG_EXTRACTION_DATE";

    /**
     * The Constant PROPERTY_MSG_HEADER @link(String).
     */
    static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";

    /**
     * The Constant PROPERTY_MSG_SELECTED_PERIODS @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";

    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLES @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";

    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLES_CHAMBRE @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_VARIABLES_CHAMBRE = "PROPERTY_MSG_SELECTED_VARIABLES_CHAMBRE";

    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLES_TOURS @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_VARIABLES_TOURS = "PROPERTY_MSG_SELECTED_VARIABLES_TOURS";

    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLES_METEO @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_VARIABLES_METEO = "PROPERTY_MSG_SELECTED_VARIABLES_METEO";

    /**
     * The Constant PROPERTY_MSG_SELECTED_SITES @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_SITES = "PROPERTY_MSG_SELECTED_SITES";

    /**
     * The Constant PATTERN_STRING_SITES_SUMMARY @link(String).
     */
    static final String PATTERN_STRING_SITES_SUMMARY = "   %s - %s";

    /**
     * The Constant PATTERN_STRING_VARIABLES_SUMMARY @link(String).
     */
    static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";

    /**
     * The Constant PATTERN_STRING_COMMENTS_SUMMARY @link(String).
     */
    static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    /**
     * The Constant KEYMAP_COMMENTS @link(String).
     */
    static final String KEYMAP_COMMENTS = "comments";

    /**
     * Builds the body.
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java
     * .lang.String, java.util.Map, java.util.Map)
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected void buildOutputData(final Map<String, Object> requestMetadatasMap,
            final Map<String, PrintStream> outputPrintStreamMap) {
        for (final Map.Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            super.buildOutputData(requestMetadatasMap, datatypeNameEntry.getValue(),
                    Stream.of(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES, IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS, IMeteoDatatypeManager.CODE_DATATYPE_METEO).toArray(String[]::new),
                    Stream.of(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES, IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS, IMeteoDatatypeManager.CODE_DATATYPE_METEO).toArray(String[]::new)
            );
        }
    }

    @Override
    protected String getLocalBundle() {
        return BUNDLE_SOURCE_PATH;
    }
}
