/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.meteo.ISequenceMeteoDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo_;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASequenceMeteoDAO.
 */
public class JPASequenceMeteoDAO extends AbstractJPADAO<SequenceMeteo> implements
        ISequenceMeteoDAO<SequenceMeteo> {


    /**
     * @param date
     * @param versionFile
     * @return
     */
    @Override
    public Optional<SequenceMeteo> getSequenceMeteo(LocalDate date, VersionFile versionFile) {
        CriteriaQuery<SequenceMeteo> query = builder.createQuery(SequenceMeteo.class);
        Root<SequenceMeteo> s = query.from(SequenceMeteo.class);
        Join<SequenceMeteo, VersionFile> version = s.join(SequenceMeteo_.version);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> rn = dataset.join(Dataset_.realNode);
        query
                .select(s)
                .where(
                        builder.equal(rn, versionFile.getDataset().getRealNode()),
                        builder.equal(s.get(SequenceMeteo_.dateMesure), date)
                );
        return getOptional(query);
    }

}
