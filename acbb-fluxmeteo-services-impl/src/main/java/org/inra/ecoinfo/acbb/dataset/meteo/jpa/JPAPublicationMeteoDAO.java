package org.inra.ecoinfo.acbb.dataset.meteo.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationMeteoDAO.
 */
public class JPAPublicationMeteoDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     * Removes the version.
     *
     * @param version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion(org.
     *                              inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        deleteValeurs(version);
        deleteMesures(version);
        deleteSequence(version);
    }

    private void deleteValeurs(final VersionFile version) {
        CriteriaDelete<ValeurMeteo> deleteValeur = builder.createCriteriaDelete(ValeurMeteo.class);
        Root<ValeurMeteo> v = deleteValeur.from(ValeurMeteo.class);
        Subquery<MesureMeteo> subqueryMesure = deleteValeur.subquery(MesureMeteo.class);
        Root<MesureMeteo> m = subqueryMesure.from(MesureMeteo.class);
        Subquery<SequenceMeteo> subquerySequence = subqueryMesure.subquery(SequenceMeteo.class);
        Root<SequenceMeteo> s = subquerySequence.from(SequenceMeteo.class);
        Path<VersionFile> ver = s.get(SequenceMeteo_.version);
        subquerySequence
                .select(s)
                .where(builder.equal(ver, version));
        Path<SequenceMeteo> sDb = m.get(MesureMeteo_.sequenceMeteo);
        subqueryMesure
                .select(m)
                .where(sDb.in(subquerySequence));
        Path<MesureMeteo> mDb = v.get(ValeurMeteo_.mesureMeteo);
        deleteValeur
                .where(mDb.in(subqueryMesure));
        delete(deleteValeur);
        flush();
    }

    private void deleteMesures(final VersionFile version) {
        CriteriaDelete<MesureMeteo> deleteMesure = builder.createCriteriaDelete(MesureMeteo.class);
        Root<MesureMeteo> m = deleteMesure.from(MesureMeteo.class);
        Subquery<SequenceMeteo> subquerySequence = deleteMesure.subquery(SequenceMeteo.class);
        Root<SequenceMeteo> s = subquerySequence.from(SequenceMeteo.class);
        Path<VersionFile> ver = s.get(SequenceMeteo_.version);
        subquerySequence
                .select(s)
                .where(builder.equal(ver, version));
        Path<SequenceMeteo> sDb = m.get(MesureMeteo_.sequenceMeteo);
        deleteMesure
                .where(sDb.in(subquerySequence));
        delete(deleteMesure);
        flush();
    }

    private void deleteSequence(final VersionFile version) {
        CriteriaDelete<SequenceMeteo> deleteSequence = builder.createCriteriaDelete(SequenceMeteo.class);
        Root<SequenceMeteo> s = deleteSequence.from(SequenceMeteo.class);
        Path<VersionFile> ver = s.get(SequenceMeteo_.version);
        deleteSequence
                .where(builder.equal(ver, version));
        delete(deleteSequence);
        flush();
    }

}
