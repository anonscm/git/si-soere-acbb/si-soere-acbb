/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.acbb.dataset.meteo.entity.*;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.synthesis.meteorologie.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.meteorologie.SynthesisValue;

/**
 * @author tcherniatinsky
 */
public class MeteoSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurMeteo> v = query.from(ValeurMeteo.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<MesureMeteo, SequenceMeteo> s = v.join(ValeurMeteo_.mesureMeteo).join(MesureMeteo_.sequenceMeteo);
        Join<ValeurMeteo, RealNode> varRn = v.join(ValeurMeteo_.realNode);
        Join<RealNode, RealNode> dtyRn = varRn.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = dtyRn.join(RealNode_.parent).join(RealNode_.parent);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurMeteo_.value);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(SequenceMeteo_.dateMesure);
        Expression<String> context = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                dateMesure,
                                context,
                                variableCode,
                                builder.avg(v.get(ValeurMeteo_.value)),
                                idNode
                        )
                )
                .where(
                        builder.equal(node.join(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(context,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(builder.asc(context),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

}
