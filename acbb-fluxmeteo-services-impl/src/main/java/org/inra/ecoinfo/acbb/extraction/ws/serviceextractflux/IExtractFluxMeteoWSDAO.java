/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.ws.serviceextractflux;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.synthesis.fluxtours.SynthesisDatatype;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
interface IExtractFluxMeteoWSDAO<T> {

    List<TraitementNode> traitementNodes(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot);

    public String extract(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot, String startDate, String endDate, String treatments, String variables, int offset, int limit);

    List<SynthesisDatatype> getSynthesisFluxToursDatatypes();
}
