/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The Interface IMesureFluxChambreDAO.
 *
 * @param <T> the generic type
 */
public interface IMesureFluxChambreDAO<T> extends IDAO<MesureFluxChambre> {

    /**
     * Gets the by date and suivi parcelle id.
     *
     * @param date       the date
     * @param parcelleId the parcelle id
     * @return the by date and suivi parcelle id
     */
    Optional<MesureFluxChambre> getByDateAndSuiviParcelleId(LocalDate date, Long parcelleId);

    /**
     * Gets the line publication name doublon.
     *
     * @param date the date
     * @return the line publication name doublon
     */
    Object[] getLinePublicationNameDoublon(LocalDate date);
}
