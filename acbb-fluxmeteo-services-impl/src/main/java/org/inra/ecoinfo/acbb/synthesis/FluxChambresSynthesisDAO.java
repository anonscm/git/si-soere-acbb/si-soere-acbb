/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre_;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre_;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.synthesis.fluxchambre.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.fluxchambre.SynthesisValue;

/**
 * @author tcherniatinsky
 */
public class FluxChambresSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurFluxChambre> v = query.from(ValeurFluxChambre.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> varRn2 = node.join(NodeDataSet_.realNode);

        Join<ValeurFluxChambre, MesureFluxChambre> m = v.join(ValeurFluxChambre_.mesure);
        Join<ValeurFluxChambre, RealNode> varRn = v.join(ValeurFluxChambre_.realNode);
        final Join<RealNode, RealNode> themeRn = varRn.join(RealNode_.parent);
        final Join<RealNode, RealNode> datatypeRn = themeRn.join(RealNode_.parent);
        Join<RealNode, RealNode> parcelleRn = datatypeRn.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = parcelleRn.join(RealNode_.parent);
        Path<String> parcelleCode = parcelleRn.join(RealNode_.nodeable).get(Nodeable_.code);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurFluxChambre_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = m.get(MesureFluxChambre_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query.select(
                builder.construct(
                        SynthesisValue.class,
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        builder.avg(valeur),
                        idNode
                )
        )
                .where(
                        builder.equal(varRn2, varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

}
