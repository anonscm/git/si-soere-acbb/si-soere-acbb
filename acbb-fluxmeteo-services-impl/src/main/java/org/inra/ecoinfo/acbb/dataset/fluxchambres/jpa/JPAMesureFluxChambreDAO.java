/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IMesureFluxChambreDAO;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPAMesureFluxChambreDAO.
 */
public class JPAMesureFluxChambreDAO extends AbstractJPADAO<MesureFluxChambre> implements
        IMesureFluxChambreDAO<MesureFluxChambre> {

    /**
     * Gets the by date and suivi parcelle id.
     *
     * @param date
     * @param parcelleId
     * @return the by date and suivi parcelle id
     * @link(Date) the date
     * @link(Long) the parcelle id
     */
    @Override
    public Optional<MesureFluxChambre> getByDateAndSuiviParcelleId(final LocalDate date, final Long parcelleId) {
        CriteriaQuery<MesureFluxChambre> query = builder.createQuery(MesureFluxChambre.class);
        Root<MesureFluxChambre> m = query.from(MesureFluxChambre.class);
        query.where(
                builder.equal(m.get(MesureFluxChambre_.date), date),
                builder.equal(m.join(MesureFluxChambre_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.id), parcelleId)
        );
        query.select(m);
        return getOptional(query);
    }

    /**
     * Gets the line publication name doublon.
     *
     * @param date
     * @return the line publication name doublon
     * @link(Date) the date
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final LocalDate date) {
        CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
        Root<MesureFluxChambre> m = query.from(MesureFluxChambre.class);
        query.where(builder.equal(m.get(MesureFluxChambre_.date), date));
        query.multiselect(
                m.join(MesureFluxChambre_.versionFile).join(VersionFile_.dataset).get(Dataset_.id),
                m.get(MesureFluxChambre_.ligneFichierEchange));
        return getFirstOrNull(query);
    }
}
