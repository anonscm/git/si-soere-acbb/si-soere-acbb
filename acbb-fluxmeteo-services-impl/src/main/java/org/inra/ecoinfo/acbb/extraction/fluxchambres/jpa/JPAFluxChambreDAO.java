/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxchambres.jpa;

import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre_;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre_;
import org.inra.ecoinfo.acbb.extraction.fluxchambres.IFluxChambreDAO;
import org.inra.ecoinfo.acbb.extraction.jpa.JPAAbstractExtraction;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.synthesis.fluxchambre.SynthesisValue;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class JPAFluxChambreDAO.
 */
public class JPAFluxChambreDAO extends JPAAbstractExtraction<ValeurFluxChambre> implements IFluxChambreDAO {

    /**
     *
     */
    public JPAFluxChambreDAO() {
        super(SynthesisValue.class, true);
    }

    /**
     * Extract flux chambre.
     *
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @param user          the value of user
     * @return the
     * java.util.List<org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre>
     * @link(List<SiteACBB>)
     * @link(Date)
     * @link(Date)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<MesureFluxChambre> extractFluxChambre(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin, IUser user) {
        CriteriaQuery<MesureFluxChambre> query = buildQuery(selectedSites, dateDebut, dateFin, user, false);
        return getResultList(query);
    }

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @param user
     * @param isCount
     * @return
     */
    public CriteriaQuery buildQuery(final List<INodeable> selectedSites, final LocalDate dateDebut, final LocalDate dateFin, IUser user, boolean isCount) {

        CriteriaQuery criteria;
        if (isCount) {
            criteria = builder.createQuery(Long.class);
        } else {
            criteria = builder.createQuery(MesureFluxChambre.class);
        }
        Root<ValeurFluxChambre> vfc = criteria.from(ValeurFluxChambre.class);
        Join<ValeurFluxChambre, MesureFluxChambre> mfc = vfc.join(ValeurFluxChambre_.mesure);
        Join<ValeurFluxChambre, RealNode> rnVariable = vfc.join(ValeurFluxChambre_.realNode);
        Join<RealNode, RealNode> rnDatatype = rnVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> rnTheme = rnDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> rnParcelle = rnTheme.join(RealNode_.parent);
        Join<RealNode, RealNode> rnSite = rnParcelle.join(RealNode_.parent);
        Join<RealNode, Nodeable> ndb = rnSite.join(RealNode_.nodeable);
        Root<DatatypeVariableUniteACBB> dvu = criteria.from(DatatypeVariableUniteACBB.class);
        Root<NodeDataSet> vns = criteria.from(NodeDataSet.class);
        ArrayList<Predicate> predicatesAnd = new ArrayList();
        predicatesAnd.add(builder.equal(rnVariable, vns.join(NodeDataSet_.realNode)));
        predicatesAnd.add(builder.equal(dvu, rnVariable.join(RealNode_.nodeable)));
        predicatesAnd.add(ndb.in(selectedSites));
        predicatesAnd.add(builder.between(mfc.<LocalDate>get(MesureFluxChambre_.date), dateDebut, dateFin));
        final Path dateMesure = mfc.get(MesureFluxChambre_.date);
        if (!isCount) {
            addRestrictiveRequestOnRoles(user, criteria, predicatesAnd, builder, vns, dateMesure);
        }
        Predicate where = builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()]));
        criteria.where(where);
        if (isCount) {
            criteria.select(builder.countDistinct(mfc));
        } else {
            List<Order> orders = new ArrayList();
            orders.add(builder.asc(ndb));
            orders.add(builder.asc(mfc.get(MesureFluxChambre_.date)));
            criteria.orderBy(orders);
            criteria.select(mfc);
        }
        return criteria;
    }

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Long sizeFluxChambre(final List<INodeable> selectedSites,
                                final LocalDate dateDebut, final LocalDate dateFin) {
        CriteriaQuery<Long> criteria = buildQuery(selectedSites, dateDebut, dateFin, null, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected boolean isParcelleDatatype() {
        return true;
    }

}
