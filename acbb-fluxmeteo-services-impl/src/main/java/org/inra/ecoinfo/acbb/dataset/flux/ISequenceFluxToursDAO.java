/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * The Interface ISequenceFluxToursDAO.
 *
 * @param <T> the generic type
 */
public interface ISequenceFluxToursDAO<T> extends IDAO<SequenceFluxTours> {

    /**
     * Gets the by date and suivi parcelle id.
     *
     * @param version
     * @param date       the date
     * @param parcelleId the parcelle id
     * @return the by date and suivi parcelle id
     */
    Optional<SequenceFluxTours> getByVersionAndDateAndSuiviParcelleId(VersionFile version, LocalDate date,
                                                                      Long parcelleId);

    /**
     * Gets the line publication name doublon.
     *
     * @param date the date
     * @return the line publication name doublon
     */
    Object[] getLinePublicationNameDoublon(LocalDateTime date);

    /**
     * @param date
     * @param versionFile
     * @return
     */
    Optional<SequenceFluxTours> getSequence(LocalDate date, VersionFile versionFile);
}
