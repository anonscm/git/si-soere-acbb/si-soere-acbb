package org.inra.ecoinfo.acbb.dataset.meteo.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.meteo.ISequenceMeteoDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * The Class MeteoDuplicateFlux.
 * <p>
 * implementation for meteo of {@link AbstractTestDuplicate}
 * <p>
 * test the existence of duplicates in meteo files
 *
 * @author Tcherniatinsky Philippe
 */
public class TestDuplicateMeteo extends AbstractTestDuplicate {
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.meteo.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicateMeteo.class
            .getName());
    /**
     * The date time line.
     *
     * @link(SortedMap<String,SortedMap<String,SortedSet<Long>>>).
     */
    final SortedMap<String, SortedSet<Long>> dateTimeLine;
    private ISequenceMeteoDAO<SequenceMeteo> sequenceMeteoDAO;

    /**
     * Instantiates a new test duplicate flux.
     *
     * @param sequenceMeteoDAO2
     */
    public TestDuplicateMeteo(ISequenceMeteoDAO<SequenceMeteo> sequenceMeteoDAO2) {
        this.sequenceMeteoDAO = sequenceMeteoDAO2;
        this.dateTimeLine = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param date
     * @param time
     * @param lineNumber long the line number {@link String} the date string {@link String} the time string
     * @link(String) the date
     * @link(String) the time
     */
    void addLine(final String datetime, final String date, final String time, final long lineNumber) {
        if (this.dateTimeLine.containsKey(datetime)) {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateMeteo.BUNDLE_SOURCE_PATH,
                    TestDuplicateMeteo.PROPERTY_MSG_DOUBLON_LINE), lineNumber, date, time,
                    this.dateTimeLine.get(datetime).first().intValue()));
            this.dateTimeLine.get(datetime).add(lineNumber);
        } else {
            final SortedSet<Long> setLine = new TreeSet();
            setLine.add(lineNumber);
            this.dateTimeLine.put(datetime, setLine);
        }
    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @param lineNumber  long the line number {@link String[]} the values
     * @link(String[]) the values
     */
    @Override
    public void addLine(final String[] values, final long lineNumber, String[] dates,
                        VersionFile versionFile) {
        String date = values[0];
        String time = values[1];
        String localValue = String.format("%-"
                + RecorderACBB.DD_MM_YYYY_HHMMSS_READ.length() + "."
                + RecorderACBB.DD_MM_YYYY_HHMMSS_READ.length() + "s", date
                + time + ":00");
        this.addLine(localValue, date, time, lineNumber);
        if (dates != null) {
            this.testLineForDuplicatesDateInDB(dates, date, time, lineNumber, versionFile);
        }
    }

    /**
     * @param sequenceMeteoDAO
     */
    public void setSequenceMeteoDAO(ISequenceMeteoDAO<SequenceMeteo> sequenceMeteoDAO) {
        this.sequenceMeteoDAO = sequenceMeteoDAO;
    }

    /**
     * @param dateDBString
     * @param timeDBString
     * @param lineNumber
     * @param versionFile
     */
    @Override
    protected void testLineForDuplicatesLineinDb(String dateDBString, String timeDBString,
                                                 long lineNumber, VersionFile versionFile) {
        LocalDate date;
        LocalTime time;
        try {
            date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDBString);
            timeDBString = (timeDBString + ":00").substring(0, DateUtil.HH_MM_SS.length());
            time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, timeDBString);
            Optional<SequenceMeteo> sequenceMeteo = this.sequenceMeteoDAO.getSequenceMeteo(date, versionFile);
            if (sequenceMeteo.isPresent()) {
                for (MesureMeteo mesureMeteo : sequenceMeteo.get().getMesuresMeteo()) {
                    if (mesureMeteo.getHeure().equals(time)) {
                        this.errorsReport.addErrorMessage(String.format(RecorderACBB
                                        .getACBBMessageWithBundle(TestDuplicateMeteo.BUNDLE_SOURCE_PATH,
                                                ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE_IN_FILE),
                                lineNumber, dateDBString, timeDBString, mesureMeteo
                                        .getLigneFichierEchange(), sequenceMeteo.get().getVersion()
                                        .getDataset().buildDownloadFilename(this.configuration)));
                    }
                }
            }
        } catch (DateTimeException e) {
            AbstractTestDuplicate.LOGGER.info("can't test line", e);
        }
    }
}
