/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo;

import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * @param <T>
 * @author tcherniatinsky
 */
public interface IFluxMeteoExtractionDAO<T> {

    /**
     * @param intervalsDate
     * @param user
     * @return
     */
    List<SiteACBB> getAvailablesSiteForPeriode(List<IntervalDate> intervalsDate, IUser user);

    /**
     * @param sites
     * @param intervalsDate
     * @param user
     * @return
     */
    List<NodeDataSet> getAvailablesVariablesBySites(final List<SiteACBB> sites, List<IntervalDate> intervalsDate, IUser user);
}
