/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.meteo.impl;

import org.inra.ecoinfo.acbb.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;

import java.util.Comparator;
import java.util.List;

/**
 * @author tcherniatinsky
 */
public class ValeurComparator implements Comparator<ValeurMeteo> {

    List<String> listToCompare = null;


    /**
     * @param listToCompare
     */
    public ValeurComparator(List<String> listToCompare) {
        this.listToCompare = listToCompare;
    }

    private VariableACBB getVariableFromValeur(ValeurMeteo valeur) {
        return (VariableACBB) ((DatatypeVariableUniteACBB) valeur.getRealNode().getNodeable()).getVariable();
    }

    @Override
    public int compare(ValeurMeteo o1, ValeurMeteo o2) {
        return listToCompare.indexOf(getVariableFromValeur(o1).getAffichage()) - listToCompare.indexOf(getVariableFromValeur(o2).getAffichage());
    }

}
