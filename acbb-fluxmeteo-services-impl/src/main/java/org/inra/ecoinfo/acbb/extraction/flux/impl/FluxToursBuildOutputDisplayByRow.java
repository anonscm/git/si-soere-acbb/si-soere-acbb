/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.flux.impl;

import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.ValeurFluxTours;
import org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorBuilderACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoExtractor;
import org.inra.ecoinfo.acbb.extraction.impl.ComparatorVariable;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.VariableDescriptor;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.Map.Entry;

/**
 * The Class FluxToursBuildOutputDisplayByRow.
 */
public class FluxToursBuildOutputDisplayByRow extends AbstractOutputBuilder {

    /**
     * The Constant HEADER_RAW_DATA.
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";
    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.flux.messages";
    static final String BUNDLE_FLUX_METEO_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.fluxmeteo.messages";
    /**
     * The msg missing variable in references datas.
     */
    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNKNOWN_VARIABLE";
    /**
     * The Constant PATTERN_END_LINE.
     */
    static final String PATTERN_END_LINE = "\n";
    /**
     * The Constant REGEX_CSV_FIELD @link(String).
     */
    static final String REGEX_CSV_FIELD = "[^;]";
    /**
     * The Constant DATASET_DESCRIPTOR_XML.
     */
    static final String DATASET_DESCRIPTOR_XML = "org/inra/ecoinfo/acbb/dataset/flux/impl/dataset-descriptor.xml";
    private static final Logger LOGGER = LoggerFactory.getLogger(FluxToursBuildOutputDisplayByRow.class
            .getName());
    /**
     * The org.inra.ecoinfo.acbb.dataset descriptor {@link DatasetDescriptor}.
     */
    static DatasetDescriptor datasetDescriptor;
    /**
     * The comparator {@link ComparatorVariable.
     */
    final ComparatorVariable comparator = new ComparatorVariable();
    /**
     * The variable dao {@link IVariableACBBDAO}.
     */
    IVariableACBBDAO variableDAO;

    /**
     * The datatype unite variable acbbdao.
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     * The variables descriptor {@link Map}.
     */
    Map<String, VariableDescriptor> variablesDescriptor = new HashMap();

    /**
     * Instantiates a new flux tours build output display by row.
     */
    public FluxToursBuildOutputDisplayByRow() {
        super();
        try {
            if (FluxToursBuildOutputDisplayByRow.datasetDescriptor == null) {
                FluxToursBuildOutputDisplayByRow.setDatasetDescriptor(DatasetDescriptorBuilderACBB
                        .buildDescriptorACBB(this
                                .getClass()
                                .getClassLoader()
                                .getResourceAsStream(
                                        FluxToursBuildOutputDisplayByRow.DATASET_DESCRIPTOR_XML)));
            }
        } catch (final IOException | SAXException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset descriptor
     * {@link DatasetDescriptor}.
     *
     * @param datasetDescriptor the new org.inra.ecoinfo.acbb.dataset descriptor
     *                          {@link DatasetDescriptor}
     */
    static void setDatasetDescriptor(final DatasetDescriptor datasetDescriptor) {
        FluxToursBuildOutputDisplayByRow.datasetDescriptor = datasetDescriptor;
    }

    /**
     * Builds the body.
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder
     * #buildBody(java.lang.String, java.util.Map, java.util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        LocalDate localStartDate = DateUtil.MIN_LOCAL_DATE;
        LocalDate localEndDate = DateUtil.MAX_LOCAL_DATE;
        final DatesFormParamVO dateFormParam = (DatesFormParamVO) requestMetadatasMap.get(DatesFormParamVO.class.getSimpleName());
        try {
            localStartDate = dateFormParam.getDateStart();
            localEndDate = dateFormParam.getDateEnd();
        } catch (final DateTimeException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        final List<Variable> selectedFluxToursVariables = (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().toLowerCase().concat(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS));
        final Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> sortedSuivisParcelles = (Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>>) requestMetadatasMap
                .getOrDefault(SuiviParcelle.class.getSimpleName(), new ArrayList());
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS);
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            outputPrintStreamMap.get(datatypeNameEntry.getKey()).println(headers);
        }
        final PrintStream rawDataPrintStream = outputPrintStreamMap.get(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS);
        final OutputHelper outputHelper = new OutputHelper(localStartDate, localEndDate, rawDataPrintStream,
                selectedFluxToursVariables, sortedSuivisParcelles);
        final List<MesureFluxTours> mesuresFluxTours = resultsDatasMap
                .get(FluxToursExtractor.MAP_INDEX_FLUXTOURS);
        try {
            outputHelper.buildBody(mesuresFluxTours);
        } catch (DateTimeException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage());
        }
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @return the string
     * @throws BusinessException the business exception
     * @link(Map<String,Object>) the request metadatas map
     * @link(Map<String,Object>) the request metadatas map @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder
     * #buildHeader(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Properties propertiesVariableName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        final ErrorsReport errorsReport = new ErrorsReport();
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS), new ArrayList());
        final List<String> selectedVariablesAffichage = new LinkedList();
        for (final Variable variable : selectedVariables) {
            selectedVariablesAffichage.add(variable.getAffichage());
        }
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        try {
            stringBuilder1.append(this.getLocalizationManager().getMessage(
                    FluxToursBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    FluxToursBuildOutputDisplayByRow.HEADER_RAW_DATA));
            stringBuilder2.append(this
                    .getLocalizationManager()
                    .getMessage(FluxToursBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                            FluxToursBuildOutputDisplayByRow.HEADER_RAW_DATA)
                    .replaceAll(FluxToursBuildOutputDisplayByRow.REGEX_CSV_FIELD,
                            StringUtils.EMPTY));
            stringBuilder3.append(this
                    .getLocalizationManager()
                    .getMessage(FluxToursBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                            FluxToursBuildOutputDisplayByRow.HEADER_RAW_DATA)
                    .replaceAll(FluxToursBuildOutputDisplayByRow.REGEX_CSV_FIELD,
                            StringUtils.EMPTY));
            final Iterator<Column> it = FluxToursBuildOutputDisplayByRow.datasetDescriptor
                    .getColumns().iterator();
            Column column;
            VariableACBB variable;
            while (it.hasNext()) {
                column = it.next();
                if (column.isFlag()
                        && (RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType()) || RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE
                        .equals(column.getFlagType()))) {
                    variable = (VariableACBB) this.variableDAO.getByAffichage(column.getName()).orElse(null);
                    if (variable == null) {
                        errorsReport
                                .addErrorMessage(String
                                        .format(this
                                                        .getLocalizationManager()
                                                        .getMessage(
                                                                FluxToursBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                                                                FluxToursBuildOutputDisplayByRow.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                                column.getName()));
                        continue;
                    }
                    String localizedVariableName = propertiesVariableName.getProperty(variable.getName(), variable.getName());
                    if (Strings.isNullOrEmpty(localizedVariableName)) {
                        localizedVariableName = variable.getName();
                    }
                    Optional<Unite> unite = this.datatypeVariableUniteACBBDAO.getUnite(
                            IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS, variable.getCode());
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder1.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD,
                                localizedVariableName));
                    }
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder2
                                .append(String.format(
                                        ACBBMessages.PATTERN_1_FIELD,
                                        unite.map(u -> u.getCode()).orElseGet(String::new)));
                    }
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder3.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD,
                                variable.getAffichage()));
                    }
                } else if (column.isFlag()
                        && RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE
                        .equals(column.getFlagType())) {
                    if (selectedVariablesAffichage.contains(column.getRefVariableName())) {
                        stringBuilder1.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD,
                                column.getName()));
                    }
                    if (selectedVariablesAffichage.contains(column.getRefVariableName())) {
                        stringBuilder2.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD,
                                StringUtils.EMPTY));
                    }
                    if (selectedVariablesAffichage.contains(column.getRefVariableName())) {
                        stringBuilder3.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD,
                                StringUtils.EMPTY));
                    }
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder1.append(FluxToursBuildOutputDisplayByRow.PATTERN_END_LINE)
                .append(stringBuilder2).append(FluxToursBuildOutputDisplayByRow.PATTERN_END_LINE)
                .append(stringBuilder3).toString();
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception
     * @link(IParameter)
     * @link(IParameter) the parameters
     * @see org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE)
                .get(FluxToursExtractor.MAP_INDEX_FLUXTOURS) == null
                || ((DefaultParameter) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE)
                .get(FluxToursExtractor.MAP_INDEX_FLUXTOURS).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE));
        return null;
    }

    /**
     * Gets the aCBB message.
     *
     * @param key
     * @return the aCBB message
     * @link(String) the key
     * @link(String) the key
     */
    String getACBBMessage(final String key) {
        return this.getLocalizationManager().getMessage(RecorderACBB.ACBB_DATASET_BUNDLE_NAME, key);
    }

    private Map<String, VariableDescriptor> getVariablesDescriptor() {
        if (this.variablesDescriptor == null || this.variablesDescriptor.isEmpty()) {
            this.updateVariableDescriptor();
        }
        return this.variablesDescriptor;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteDAO the new datatype unite variable acbbdao
     *                                 {@link IDatatypeVariableUniteACBBDAO} the new datatype unite variable
     *                                 acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteDAO;
    }

    /**
     * @param suiviParcelleDAO
     */
    public void setSuiviParcelleDAO(ISuiviParcelleDAO suiviParcelleDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     *                    {@link IVariableACBBDAO} {@link IVariableACBBDAO} the new variable dao
     */
    public void setVariableDAO(final IVariableACBBDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    private void updateVariableDescriptor() {
        for (final Column column : FluxToursBuildOutputDisplayByRow.datasetDescriptor.getColumns()) {
            if (null != column.getFlagType()) {
                switch (column.getFlagType()) {
                    case RecorderACBB.PROPERTY_CST_VARIABLE_TYPE:
                    case RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE:
                        this.variablesDescriptor.put(column.getName(), new VariableDescriptor(
                                column.getName()));
                        break;
                    case RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE:
                        this.variablesDescriptor.get(column.getRefVariableName())
                                .setHasQualityClass(true);
                        break;
                }
            }
        }
    }

    /**
     * <used for build outputs The Class OutputHelper.
     */
    class OutputHelper {

        /**
         * The Constant PATTERN_5_FIELD.
         */
        static final String PATTERN_7_FIELD = "%s;%s;%s;%s;%s;%s;%s";
        private static final String PROPERTY_MSG_NO_DEFINED_TREATMENT = "PROPERTY_MSG_NO_DEFINED_TREATMENT";
        /**
         * The sorted mesures {@link Map}.
         */
        final SortedMap<String, SortedMap<Parcelle, SortedMap<LocalDateTime, MesureFluxTours>>> sortedMesures = new TreeMap();
        /**
         * The versions {@link List}.
         */
        final List<Long> versions = new LinkedList();
        /**
         * The raw data print stream {@link PrintStream}.
         */
        final PrintStream rawDataPrintStream;
        /**
         * The variables affichage {@link List}.
         */
        final List<String> variablesAffichage = new LinkedList();
        private final Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> sortedSuivisParcelles;
        private final Properties treatmentProperties = localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_NAME);
        private final Properties siteProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        /**
         * The date {@link Date}.
         */
        LocalDateTime localStartdate, localEndDate, localCurrentdate;
        /**
         * The site name.
         */
        String siteName;
        private Parcelle parcelle;

        /**
         * Instantiates a new output helper.
         *
         * @param localStartDate             the start date
         * @param utcEndDate                 the end date
         * @param rawDataPrintStream         the raw data print stream
         * @param selectedFluxToursVariables the selected flux tours variables
         */
        OutputHelper(final LocalDate localStartDate, final LocalDate localEndDate,
                     final PrintStream rawDataPrintStream,
                     final List<Variable> selectedFluxToursVariables,
                     Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> sortedSuivisParcelles) {
            super();
            this.localStartdate = localStartDate.atStartOfDay();
            this.localEndDate = localEndDate.atStartOfDay();
            this.rawDataPrintStream = rawDataPrintStream;
            for (final Column column : FluxToursBuildOutputDisplayByRow.datasetDescriptor
                    .getColumns()) {
                if (RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())
                        || RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE.equals(column.getFlagType())) {
                    for (final Variable variable : selectedFluxToursVariables) {
                        if (variable.getAffichage().equals(column.getName())) {
                            this.variablesAffichage.add(variable.getAffichage());
                        }
                    }
                }
            }
            this.sortedSuivisParcelles = sortedSuivisParcelles;
        }

        /**
         * Adds the mesure.
         *
         * @param mesure the mesure
         * @throws DateTimeException
         */
        public void addMesure(final MesureFluxTours mesure) throws DateTimeException {
            final SequenceFluxTours sequence = mesure.getSequenceFluxTours();
            final VersionFile version = sequence.getVersion();
            this.siteName = ACBBUtils.getSiteFromDataset(version.getDataset()).getPath();
            Parcelle parcelle = sequence.getParcelle();
            if (!this.versions.contains(version.getId())) {
                LocalDateTime beginDateLocal = version.getDataset().getDateDebutPeriode();
                LocalDateTime endDateLocal = version.getDataset().getDateFinPeriode();
                beginDateLocal = beginDateLocal.isAfter(this.localStartdate) ? beginDateLocal : localStartdate;
                if (!endDateLocal.isBefore(this.localEndDate)) {
                    endDateLocal = this.localEndDate.plus(1, ChronoUnit.DAYS);
                } else {
                    endDateLocal = endDateLocal.plus(1, ChronoUnit.MILLIS);
                }
                this.localCurrentdate = beginDateLocal;
                while (this.localCurrentdate.isBefore(endDateLocal)) {
                    sortedMesures
                            .computeIfAbsent(siteName, k -> new TreeMap<>())
                            .computeIfAbsent(parcelle, l -> new TreeMap<>())
                            .put(this.localCurrentdate, null);
                    this.localCurrentdate = this.localCurrentdate.plus(30, ChronoUnit.MINUTES);
                }
                this.versions.add(version.getId());
            }
            this.localCurrentdate = DateUtil.getLocalDateTimeFromLocalDateAndLocaltime(sequence.getDateMesure(), mesure.getHeure());
            this.sortedMesures
                    .computeIfAbsent(siteName, k -> new TreeMap<>())
                    .computeIfAbsent(parcelle, l -> new TreeMap<>())
                    .put(this.localCurrentdate, mesure);
        }

        /**
         * Adds the mesure line.
         *
         * @param mesure
         * @link(MesureFluxTours) the mesure
         * @link(MesureFluxTours) the mesure
         */
        void addMesureLine(final MesureFluxTours mesure) {
            this.printLineGeneric();
            List<ValeurFluxTours> valeursFluxTours = mesure.getValeursFluxTours();
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            while (itVariablesAffichage.hasNext()) {
                variableAffichage = itVariablesAffichage.next();
                final Iterator<ValeurFluxTours> itValeursFluxTours = valeursFluxTours.iterator();
                Optional<ValeurFluxTours> valeur = Optional.empty();
                while (itValeursFluxTours.hasNext()) {
                    Optional<ValeurFluxTours> vft = Optional.ofNullable(itValeursFluxTours.next());
                    if (variableAffichage.equals(getVariableFromValeur(vft).getAffichage())) {
                        valeur = vft;
                        break;
                    }
                }
                printValue(valeur);
            }
        }

        private VariableACBB getVariableFromValeur(Optional<ValeurFluxTours> valeur) {
            return valeur
                    .map(v -> v.getRealNode())
                    .map(rn -> (DatatypeVariableUniteACBB) rn.getNodeable())
                    .map(dvu -> (VariableACBB) dvu.getVariable())
                    .orElse(null);
        }

        private void printValue(Optional<ValeurFluxTours> valeurFluxTours) throws NumberFormatException {
            this.rawDataPrintStream.print(String.format(ACBBMessages.PATTERN_1_FIELD, ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurFluxTours.map(vft -> vft.getValue()).orElse(null))));
            if (getVariableFromValeur(valeurFluxTours) != null && getVariablesDescriptor().get(getVariableFromValeur(valeurFluxTours).getAffichage()) != null
                    && getVariablesDescriptor().get(
                    getVariableFromValeur(valeurFluxTours).getAffichage()).hasQualityClass()) {
                this.rawDataPrintStream.print(String.format(ACBBMessages.PATTERN_1_FIELD, ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurFluxTours.map(vft -> vft.getQualityFlag()).orElse(null))));
            }
        }

        private void printNAValue(String variableAffichage) throws NumberFormatException {
            rawDataPrintStream.print(String.format(ACBBMessages.PATTERN_1_FIELD,
                    getACBBMessage(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE)));
            if (getVariablesDescriptor().get(variableAffichage) != null
                    && getVariablesDescriptor().get(variableAffichage).hasQualityClass()) {
                rawDataPrintStream.print(String.format(ACBBMessages.PATTERN_1_FIELD,
                        getACBBMessage(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE)));
            }
        }

        /**
         * Adds the na line.
         */
        void addNALine() {
            this.printLineGeneric();
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            while (itVariablesAffichage.hasNext()) {
                variableAffichage = itVariablesAffichage.next();
                printNAValue(variableAffichage);
            }
        }

        /**
         * Builds the body.
         *
         * @param mesuresFluxTours the mesures
         * @throws DateTimeException
         */
        @Transactional
        public void buildBody(final List<MesureFluxTours> mesuresFluxTours) throws DateTimeException {
            Iterator<MesureFluxTours> iteratorMesure = mesuresFluxTours.iterator();
            while (iteratorMesure.hasNext()) {
                this.addMesure(iteratorMesure.next());
                iteratorMesure.remove();
            }
            Iterator<Entry<String, SortedMap<Parcelle, SortedMap<LocalDateTime, MesureFluxTours>>>> iteratorSite = this.sortedMesures.entrySet().iterator();
            while (iteratorSite.hasNext()) {
                final Entry<String, SortedMap<Parcelle, SortedMap<LocalDateTime, MesureFluxTours>>> siteEntry = iteratorSite.next();
                this.siteName = siteEntry.getKey();
                Iterator<Entry<Parcelle, SortedMap<LocalDateTime, MesureFluxTours>>> iteratorParcelle = siteEntry.getValue().entrySet().iterator();
                while (iteratorParcelle.hasNext()) {
                    Entry<Parcelle, SortedMap<LocalDateTime, MesureFluxTours>> parcelleEntry = iteratorParcelle.next();
                    this.parcelle = parcelleEntry.getKey();
                    Iterator<Entry<LocalDateTime, MesureFluxTours>> iteratorDate = parcelleEntry.getValue().entrySet().iterator();
                    while (iteratorDate.hasNext()) {
                        final Entry<LocalDateTime, MesureFluxTours> dateEntry = iteratorDate.next();
                        this.localCurrentdate = dateEntry.getKey();
                        if (dateEntry.getValue() == null) {
                            this.addNALine();
                        } else {
                            this.addMesureLine(dateEntry.getValue());
                        }
                        this.rawDataPrintStream.println();
                        iteratorDate.remove();
                    }
                    iteratorParcelle.remove();
                }
                iteratorSite.remove();
            }
        }

        public SuiviParcelle getSuiviParcelle(Parcelle parcelle, LocalDate date) {
            SuiviParcelle suiviParcelle = null;
            SortedMap<LocalDate, SuiviParcelle> sortedSp = ((TreeMap<LocalDate, SuiviParcelle>) this.sortedSuivisParcelles
                    .get(parcelle)).headMap(date, true);
            if (sortedSp.isEmpty()) {
                return suiviParcelle;
            }
            suiviParcelle = sortedSp.get(sortedSp.lastKey());
            return suiviParcelle;
        }

        /**
         * Prints the line generic.
         */
        void printLineGeneric() {
            String line;
            SuiviParcelle suiviParcelle = this.getSuiviParcelle(this.parcelle, localCurrentdate.toLocalDate());
            String treatmentName = suiviParcelle == null ? RecorderACBB.getACBBMessageWithBundle(
                    FluxToursBuildOutputDisplayByRow.BUNDLE_FLUX_METEO_SOURCE_PATH,
                    OutputHelper.PROPERTY_MSG_NO_DEFINED_TREATMENT) : treatmentProperties.getProperty(suiviParcelle.getTraitement().getNom(), suiviParcelle.getTraitement().getNom());
            final String blocName = this.parcelle.getBloc() != null ? this.parcelle.getBloc()
                    .getBlocName() : StringUtils.EMPTY;
            final String repetitionName = this.parcelle.getBloc() != null ? this.parcelle.getBloc()
                    .getRepetitionName() : StringUtils.EMPTY;
            String dateTodateString = DateUtil.getUTCDateTextFromLocalDateTime(localCurrentdate, DateUtil.DD_MM_YYYY);
            String dateToTimeString = DateUtil.getUTCDateTextFromLocalDateTime(localCurrentdate, DateUtil.HH_MM);
            if (localCurrentdate.getHour() == 0 && localCurrentdate.getMinute() == 0) {
                localCurrentdate = localCurrentdate.minus(1, ChronoUnit.DAYS);
                dateTodateString = DateUtil.getUTCDateTextFromLocalDateTime(localCurrentdate, DateUtil.DD_MM_YYYY);
                dateToTimeString = "24:00";
            }
            final String site = siteProperties.getProperty(this.parcelle.getSite().getName(), this.parcelle.getSite().getName());
            line = String.format(OutputHelper.PATTERN_7_FIELD, site,
                    treatmentName, this.parcelle.getName(), blocName, repetitionName,
                    dateTodateString, dateToTimeString);
            this.rawDataPrintStream.print(line);
        }
    }

}
