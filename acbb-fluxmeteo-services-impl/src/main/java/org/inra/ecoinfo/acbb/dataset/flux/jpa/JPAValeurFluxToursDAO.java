/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.ValeurFluxTours;

/**
 * The Class JPAValeurFluxToursDAO.
 */
public class JPAValeurFluxToursDAO extends AbstractJPADAO<ValeurFluxTours> {

}
