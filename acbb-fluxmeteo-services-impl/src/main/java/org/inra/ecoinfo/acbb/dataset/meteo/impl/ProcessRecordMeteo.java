package org.inra.ecoinfo.acbb.dataset.meteo.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.meteo.IMesureMeteoDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.ISequenceMeteoDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Transient;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.Map.Entry;

/**
 * process the record of flux files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordMeteo.
 */
@SuppressWarnings("rawtypes")
public class ProcessRecordMeteo extends AbstractProcessRecord {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesure meteo dao {@link IMesureMeteoDAO}.
     */
    @Transient
    IMesureMeteoDAO<? extends MesureMeteo> mesureMeteoDAO;
    @Transient
    ISequenceMeteoDAO<? extends SequenceMeteo> sequenceMeteoDAO;

    /**
     * Instantiates a new process record flux.
     */
    public ProcessRecordMeteo() {
        super();
    }

    private void buildLines(VersionFile versionFile, final ErrorsReport errorsReport,
                            final Map<LocalDate, List<MeteoLineRecord>> lines,
                            final SortedSet<MeteoLineRecord> ligneEnErreur) throws PersistenceException,
            DateTimeException {
        int i = 0;
        for (Entry<LocalDate, List<MeteoLineRecord>> lineDateEntry : lines.entrySet()) {
            LocalDate date = lineDateEntry.getKey();
            final SequenceMeteo sequence = this.buildSequence(versionFile, date);
            Map<Variable, RealNode> dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
            for (MeteoLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(dvus, line, sequence, ligneEnErreur, errorsReport);
            }
            this.sequenceMeteoDAO.saveOrUpdate(sequence);
            lineDateEntry.getValue().clear();
            if (++i % 50 == 0) {
                this.sequenceMeteoDAO.flush();
                versionFile = versionFileDAO.merge(versionFile);
                dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
            }
        }
    }

    /**
     * Builds the mesure.
     *
     * @param line
     * @param sequenceMeteo
     * @param ligneEnErreur
     * @param errorsReport
     * @throws DateTimeException
     * @link(LineRecord) the line
     * @link(SequenceMeteo) the sequence meteo
     * @link(SortedSet<LineRecord>) the ligne en erreur
     * @link(ErrorsReport) the errors report
     * @link(LineRecord) the line
     * @link(SequenceMeteo) the sequence meteo
     * @link(SortedSet<LineRecord>) the ligne en erreur
     * @link(ErrorsReport) the errors report
     */
    void buildMesure(final Map<Variable, RealNode> dvus, final MeteoLineRecord line, final SequenceMeteo sequenceMeteo,
                     final SortedSet<MeteoLineRecord> ligneEnErreur, final ErrorsReport errorsReport)
            throws DateTimeException {
        LocalTime time = line.getDateTime().toLocalTime();
        MesureMeteo mesureMeteo = new MesureMeteo(sequenceMeteo, time, line.getOriginalLineNumber());
        for (final VariableValue variableValue : line.getVariablesValues()) {
            if (org.apache.commons.lang.StringUtils.isEmpty(variableValue.getValue())) {
                continue;
            }
            final ValeurMeteo valeurMeteo = new ValeurMeteo();
            DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
            RealNode realNode = dvus.get(dvu.getVariable());
            valeurMeteo.setRealNode(realNode);
            if (org.apache.commons.lang.StringUtils.isEmpty(variableValue.getValue())) {
                valeurMeteo.setValue(null);
            } else {

                final Float value = Float.parseFloat(variableValue.getValue());
                valeurMeteo.setValue(value);
            }
            valeurMeteo.setMesureMeteo(mesureMeteo);
            mesureMeteo.getValeursMeteo().add(valeurMeteo);
        }
    }

    /**
     * Builds the sequence.
     *
     * @param finalVersionFile
     * @param sequences
     * @param line
     * @return the sequence meteo
     * @throws PersistenceException the persistence exception
     * @throws DateTimeException
     * @link(VersionFile) the version
     * @link(Map<Date,SequenceMeteo>) the sequences
     * @link(LineRecord) the line
     * @link(VersionFile) the version
     * @link(Map<Date,SequenceMeteo>) the sequences
     * @link(LineRecord) the line
     */
    SequenceMeteo buildSequence(final VersionFile versionFile, LocalDate date)
            throws PersistenceException, DateTimeException {
        VersionFile finalVersionFile = this.versionFileDAO.merge(versionFile);
        return new SequenceMeteo(finalVersionFile, date);
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        super.processRecord(parser, versionFile, requestProperties, fileEncoding, datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            final Map<LocalDate, List<MeteoLineRecord>> lines = new HashMap();
            long lineCount = datasetDescriptor.getEnTete();
            this.readLines(parser, dbVariables, lines, lineCount, errorsReport);
            final SortedSet<MeteoLineRecord> ligneEnErreur = new TreeSet();

            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, errorsReport, lines, ligneEnErreur);
            }
            if (!ligneEnErreur.isEmpty()) {
                for (final MeteoLineRecord line : ligneEnErreur) {
                    try {
                        String formatTime;
                        String formatDate;
                        formatTime = DateUtil.getUTCDateTextFromLocalDateTime(line.getDateTime(), DateUtil.HH_MM);
                        formatDate = DateUtil.getUTCDateTextFromLocalDateTime(line.getDateTime(), DateUtil.DD_MM_YYYY);
                        final Object[] doublon = this.mesureMeteoDAO
                                .getLinePublicationNameDoublon(line.getDateTime());
                        errorsReport.addErrorMessage(String.format(RecorderACBB
                                        .getACBBMessage(RecorderACBB.PROPERTY_MSG_DOUBLON_DATE_TIME), line
                                        .getOriginalLineNumber().intValue(), formatDate, formatTime,
                                doublon[0].toString(), ((Number) doublon[1]).intValue()));
                    } catch (final Exception z) {
                        errorsReport.addErrorMessage(String.format(RecorderACBB
                                        .getACBBMessage(RecorderACBB.PROPERTY_MSG_ERROR_INSERTION_MEASURE),
                                line.getOriginalLineNumber().intValue()));
                    }
                }
            }
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private void readLines(CSVParser parser, List<DatatypeVariableUniteACBB> dbVariables,
                           final Map<LocalDate, List<MeteoLineRecord>> lines, long lineCount, ErrorsReport errorsReport)
            throws IOException, DateTimeException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final List<VariableValue> variablesValues = new LinkedList();
            final LocalDateTime datetime;
            int index = 0;
            String dateString = null;
            index++;
            String timeString = null;
            try {
                dateString = cleanerValues.nextToken();
                timeString = cleanerValues.nextToken();
            } catch (EndOfCSVLine ex) {
                errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
            }
            try {
                datetime = RecorderACBB.getDateTimeLocalFromDateStringaAndTimeString(dateString,
                        timeString);
            } catch (BusinessException e) {
                errorsReport.addErrorMessage(e.getMessage(), e);
                continue;
            }
            String value = null;
            final Iterator<DatatypeVariableUniteACBB> it = dbVariables.iterator();
            do {
                try {
                    value = cleanerValues.nextToken();
                } catch (EndOfCSVLine ex) {
                    errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
                }
                variablesValues.add(new VariableValue(it.next(), value));
            } while (cleanerValues.currentTokenIndex() < values.length && it.hasNext());

            final MeteoLineRecord line = new MeteoLineRecord(datetime, variablesValues,
                    lineCount);
            LocalDate date;
            try {
                date = datetime.toLocalDate();
                if (!lines.containsKey(date)) {
                    lines.put(date, new LinkedList<>());
                }
                lines.get(date).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE_TIME),
                        dateString, timeString, DateUtil.DD_MM_YYYY, DateUtil.HH_MM));
            }
        }

    }

    /**
     * Sets the mesure meteo dao.
     *
     * @param mesureMeteoDAO the new mesure meteo dao
     *                       {@link IMesureMeteoDAO} {@link IMesureMeteoDAO} the new mesure meteo dao
     */
    public final void setMesureMeteoDAO(final IMesureMeteoDAO<? extends MesureMeteo> mesureMeteoDAO) {
        this.mesureMeteoDAO = mesureMeteoDAO;
    }

    /**
     * @param sequenceMeteoDAO
     */
    public void setSequenceMeteoDAO(ISequenceMeteoDAO<? extends SequenceMeteo> sequenceMeteoDAO) {
        this.sequenceMeteoDAO = sequenceMeteoDAO;
    }

}
