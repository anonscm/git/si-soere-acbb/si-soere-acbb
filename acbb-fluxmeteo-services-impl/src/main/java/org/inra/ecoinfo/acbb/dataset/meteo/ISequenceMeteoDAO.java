/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The Interface ISequenceMeteoDAO.
 *
 * @param <T> the generic type
 */
public interface ISequenceMeteoDAO<T> extends IDAO<SequenceMeteo> {

    /**
     * @param date
     * @param versionFile
     * @return
     */
    Optional<SequenceMeteo> getSequenceMeteo(LocalDate date, VersionFile versionFile);
}
