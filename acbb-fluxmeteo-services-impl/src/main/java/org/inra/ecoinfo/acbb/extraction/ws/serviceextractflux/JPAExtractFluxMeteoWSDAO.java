package org.inra.ecoinfo.acbb.extraction.ws.serviceextractflux;

//~--- non-JDK imports --------------------------------------------------------
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.synthesis.fluxtours.SynthesisDatatype;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Antoine Schellenberger
 */
public class JPAExtractFluxMeteoWSDAO extends AbstractJPADAO<Object> implements IExtractFluxMeteoWSDAO<Object> {

    private String parcelle;
    protected List<CriteriaQuery<Tuple>> queryTupleList = new LinkedList();
    @PersistenceContext
    private EntityManager em;

    /**
     *
     */
    public JPAExtractFluxMeteoWSDAO() {
    }

    @Override
    @Transactional
    public List<TraitementNode> traitementNodes(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot) {
        String request = "select distinct mindate, maxdate, agr.nom agroecosystem, ndbs.code site,par.nom parcelle, \n"
                + "tra.nom traitement, tra.affichage affichage, tra_id\n"
                + "from fluxtourssynthesisdatatype sd\n"
                + "join composite_node_data_set nds on array_position(regexp_split_to_array(idnodes, ','),nds.branch_node_id::::text)>0\n"
                + "join realnode rnd on rnd.id = nds.realnode\n"
                + "join realnode rnt on rnt.id = rnd.id_parent_node\n"
                + "join realnode rnp on rnp.id = rnt.id_parent_node\n"
                + "join composite_nodeable ndbp ON ndbp.id = rnp.id_nodeable\n"
                + "join realnode rns on rns.id = rnp.id_parent_node\n"
                + "join composite_nodeable ndbs ON ndbs.id = rns.id_nodeable\n"
                + "join site_acbb_sit sit on sit.id = ndbs.id\n"
                + "join agroecosysteme_agr agr using(agr_id)\n"
                + "join parcelle_par par on par.par_id=ndbp.id\n"
                + "join suivi_parcelle_spa spa using(par_id)\n"
                + "join traitement_tra tra using(tra_id)\n";
        String filterRights = "join compositeactivityextraction cae on cae.login in (%s)\n"
                + "join composite_node_data_set ndsv on ndsv.branch_node_id = cae.idnode and ndsv.id_parent_node=nds.branch_node_id";
        String groupsSql = groups.stream()
                .distinct()
                .collect(Collectors.joining("|"));
        if (!isRoot) {
            request = String.format(request, String.format(filterRights, groupsSql));
        }
        return (List<TraitementNode>) entityManager.createNativeQuery(request).getResultStream()
                .map(s -> new TraitementNode(localizationManager, (Object[]) s))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public String extract(ILocalizationManager localizationManager, List<String> groups, Boolean isRoot, String startDate, String endDate, String treatments, String variables, int offset, int limit) {
        StringBuilder sb = new StringBuilder("site;traitement;parcelle;bloc;répétition;date;heure;dioxyde_de_carbone;vapeur_d_eau;stabilite_de_l_atmosphere;flux_du_dioxyde_de_carbone;qc1;chaleur_sensible;qc2;chaleur_latente;qc3;momentum;qc4;vitesse_de_friction\n"
                + ";;;;;;;µmol mol-1;mmol mol-1;nounit;µmol m-2 s-1;;W m-2;;W m-2;;kg m-1s-1;;m s-1\n"
                + ";;;;;;;CO2;H2O;ZL;Fc;;H;;LE;;T;;u*\n");
        String select = "variable::::text variable, site,flus.nomtraitement, flus.affichage, flus.parcelle,\n"
                + "flus.bloc, flus.repetition, flus.date, flus.heure";
        String pagination = Optional.ofNullable(offset).filter(o->o>=0).map(o->"  offset " + o).orElse("") 
                +Optional.ofNullable(limit).filter(l->l>=0).map(l-> " limit " + limit).orElse("") + "\n";
        String request = "select %s \n"
                + "from flus \n"
                + "WHERE (%s or jsonb_path_exists(variable, '$.*.droits ? (@ like_regex  \"%s\")'))\n"
                + Optional.ofNullable(startDate).map(sd -> "and date>= '" + sd + "' \n").orElse("")
                + Optional.ofNullable(endDate).map(sd -> "and date<= '" + sd + "' \n").orElse("")
                + (Strings.isNullOrEmpty(treatments)?"":String.format(" and tra_id in (%s) ",treatments))
                + "  %s \n";
        String groupsSql = groups.stream()
                .distinct()
                .collect(Collectors.joining("|"));
        //String count = String.format(request, "count(*)", isRoot, groupsSql, "");
        request = String.format(request, select, isRoot, groupsSql, pagination);
        //String max = entityManager.createNativeQuery(count).getSingleResult().toString();
        entityManager.createNativeQuery(request).getResultStream()
                .map(s -> {
                    try {
                        return new Line(localizationManager, (Object[]) s, groups, isRoot).toString();
                    } catch (IOException ex) {
                        LoggerFactory.getLogger(JPAExtractFluxMeteoWSDAO.class.getName()).error(null, ex);
                        return ex.getMessage();
                    }
                })
                .forEach(
                        s -> sb.append(s)
                );
        return sb.toString();
    }

    @Override
    @Transactional
    public List<SynthesisDatatype> getSynthesisFluxToursDatatypes() {
        CriteriaQuery<SynthesisDatatype> query = builder.createQuery(SynthesisDatatype.class);
        Root<SynthesisDatatype> sd = query.from(SynthesisDatatype.class);
        return getResultList(query.select(sd));

    }

    class Line {

        private final List<String> valeurs = new ArrayList<>();
        private final List<String> qcs = new ArrayList<>();
        private boolean valid = false;
        String site;
        String traitement;
        String parcelle;
        String bloc;
        String repetition;
        LocalDate date;
        LocalTime heure;
        Map<String, Map<String, String>> variables = new HashMap<>();

        private Line() {
            super();
        }

        protected Line(ILocalizationManager localizationManager, Object[] o, List<String> groups, Boolean root) throws IOException {
            Properties localizedSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
            Properties localizedTreatmentsNames = localizationManager.newProperties("traitement_tra", "nom");
            Properties localizedTreatmentsAffichage = localizationManager.newProperties("traitement_tra", "affichage");
            Properties localizedParcelleNames = localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME);
            this.site = localizedSiteName.getProperty((String) o[1], (String) o[1]);
            this.traitement = String.format("%s (%s)",
                    localizedTreatmentsNames.getProperty((String) o[2], (String) o[2]),
                    localizedTreatmentsAffichage.getProperty((String) o[3], (String) o[3])
            );
            this.parcelle = localizedParcelleNames.getProperty((String) o[4], (String) o[4]);
            this.bloc = (String) o[5];
            this.repetition = (String) o[6];
            this.date = ((Date) o[7]).toLocalDate();
            this.heure = ((Time) o[8]).toLocalTime();
            init((String) o[0], groups, root);
            this.valid = true;
        }

        protected void init(String json, List<String> groups, Boolean root) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = mapper.getFactory();
            JsonParser parser = factory.createParser(json);
            JsonNode jsonTree = mapper.readTree(parser);
            for (Iterator<String> iterator = jsonTree.fieldNames(); iterator.hasNext();) {
                String variable = iterator.next();
                JsonNode valeursTree = jsonTree.get(variable);
                if (!root) {
                    JsonNode droits = valeursTree.get("droits");
                }
                variables.computeIfAbsent(variable, k -> new HashMap<String, String>())
                        .put("qc", valeursTree.get("qc").asText());
                variables.computeIfAbsent(variable, k -> new HashMap<String, String>())
                        .put("valeur", valeursTree.get("valeur").asText());
            }
        }

        @Override
        public String toString() {
            return String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;;%s\n", site, traitement, parcelle, bloc, repetition,
                    DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(heure, DateUtil.HH_MM_SS),
                    variables.getOrDefault("CO2", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("HO", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("ZL", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("FC", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("FC", new HashMap()).getOrDefault("qc", "NA"),
                    variables.getOrDefault("H", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("H", new HashMap()).getOrDefault("qc", "NA"),
                    variables.getOrDefault("LE", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("LE", new HashMap()).getOrDefault("qc", "NA"),
                    variables.getOrDefault("T", new HashMap()).getOrDefault("valeur", "NA"),
                    variables.getOrDefault("T", new HashMap()).getOrDefault("qc", "NA"),
                    variables.getOrDefault("u*", new HashMap()).getOrDefault("valeur", "NA")
            );
        }

    }
}
