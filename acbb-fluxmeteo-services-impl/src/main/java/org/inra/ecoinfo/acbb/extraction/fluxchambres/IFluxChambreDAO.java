/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxchambres;

import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoExtractionDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;

import java.time.LocalDate;
import java.util.List;

/**
 * The Interface IFluxChambreDAO.
 */
public interface IFluxChambreDAO extends IFluxMeteoExtractionDAO<ValeurFluxChambre> {


    /**
     * Extract flux chambre.
     *
     * @param selectedSites the selected sites
     * @param dateDebut     the date debut
     * @param dateFin       the date fin
     * @param user          the value of user
     * @return the java.util.List<org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre>
     */
    List<MesureFluxChambre> extractFluxChambre(List<INodeable> selectedSites, LocalDate dateDebut, LocalDate dateFin, IUser user);

    /**
     * @param selectedSites
     * @param dateDebut
     * @param dateFin
     * @return
     */
    Long sizeFluxChambre(List<INodeable> selectedSites, LocalDate dateDebut, LocalDate dateFin);

}
