/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo.impl;


import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.filenamecheckers.FileNameCheckerddmmyyyyhhmmss;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;

/**
 * The Class FileNameCheckerddmmyyyy.
 */
public class MeteoFileNameCheckerddmmyyyyhhmmss extends FileNameCheckerddmmyyyyhhmmss {

    /**
     * Gets the file path.
     *
     * @param version
     * @return the file path @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo
     * .dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public String getFilePath(final VersionFile version) {
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH,
                ACBBUtils.getSiteFromDataset(version.getDataset()).getPath(),
                IMeteoDatatypeManager.CODE_SHORT_DATATYPE_METEO,
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY),
                version.getVersionNumber());
    }

    /**
     * @param currentSite
     * @param currentDatatype
     * @param datatypeName
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     */
    @Override
    protected void testDatatype(final String currentSite, final String currentDatatype,
                                String datatypeName) throws InvalidFileNameException {
        if (IMeteoDatatypeManager.CODE_SHORT_DATATYPE_METEO.equals(datatypeName)) {
            datatypeName = IMeteoDatatypeManager.CODE_DATATYPE_METEO;
        }
        super.testDatatype(currentSite, currentDatatype, datatypeName);
    }

}
