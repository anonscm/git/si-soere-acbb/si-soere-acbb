/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesFluxMeteo;

/**
 * The Interface IRequestPropertiesMeteo.
 */
public interface IRequestPropertiesMeteo extends IRequestPropertiesFluxMeteo {

}
