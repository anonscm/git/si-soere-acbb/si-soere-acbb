/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.flux.IMesureFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours_;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours_;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * The Class JPAMesureFluxToursDAO.
 */
public class JPAMesureFluxToursDAO extends AbstractJPADAO<MesureFluxTours> implements
        IMesureFluxToursDAO<MesureFluxTours> {

    /**
     * Gets the line publication name doublon.
     *
     * @param date
     * @param heure
     * @return the line publication name doublon @see
     * org.inra.ecoinfo.acbb.dataset.flux.IMesureFluxToursDAO#
     * getLinePublicationNameDoublon(java.time.LocalDateTime, java.time.LocalDateTime)
     * @link(Date) the date
     * @link(Date) the heure
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final LocalDate date, final LocalTime heure) {
        CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
        Root<MesureFluxTours> m = query.from(MesureFluxTours.class);
        Join<MesureFluxTours, SequenceFluxTours> s = m.join(MesureFluxTours_.sequenceFluxTours);
        Path<Long> idsId = s.join(SequenceFluxTours_.version).join(VersionFile_.dataset).get(Dataset_.id);
        query.where(
                builder.equal(s.get(SequenceFluxTours_.dateMesure), date),
                builder.equal(m.get(MesureFluxTours_.heure), heure)
        );
        query.multiselect(idsId, m.get(MesureFluxTours_.ligneFichierEchange));
        return getFirstOrNull(query);
    }

}
