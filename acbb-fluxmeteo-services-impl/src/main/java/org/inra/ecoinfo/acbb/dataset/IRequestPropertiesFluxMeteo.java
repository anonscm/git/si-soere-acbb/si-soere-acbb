/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

/**
 * The Interface IRequestPropertiesFluxMeteo.
 */
public interface IRequestPropertiesFluxMeteo extends IRequestPropertiesACBB {

    /**
     * Date to string.
     *
     * @param date the date
     * @param time the time
     * @return the string
     */
    String dateToStringYYYYMMJJHHMMSS(String date, String time);

}
