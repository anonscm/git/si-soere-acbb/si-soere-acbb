/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;

import java.time.LocalDateTime;

/**
 * The Interface IMesureMeteoDAO.
 *
 * @param <T> the generic type
 */
public interface IMesureMeteoDAO<T> extends IDAO<MesureMeteo> {

    /**
     * Gets the line publication name doublon.
     *
     * @param dateTime
     * @return the line publication name doublon
     */
    Object[] getLinePublicationNameDoublon(LocalDateTime dateTime);
}
