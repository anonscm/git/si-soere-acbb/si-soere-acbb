/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.inra.ecoinfo.acbb.dataset.AbstractFluxMeteoRequestProperties;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.flux.IRequestPropertiesFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class RequestPropertiesTours.
 * <p>
 * the flux implementation of {@link IRequestPropertiesACBB}
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB
 * @see org.inra.ecoinfo.acbb.dataset.flux.IRequestPropertiesFluxTours
 */
public class RequestPropertiesTours extends AbstractFluxMeteoRequestProperties implements
        IRequestPropertiesFluxTours, Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    private ITestDuplicates testDuplicates;

    /**
     * Instantiates a new request properties tours.
     */
    public RequestPropertiesTours() {
        super();
    }

    /**
     * Gets the nom de fichier.
     *
     * @param version
     * @return the nom de fichier @see
     * org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#
     * getNomDeFichier(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public String getNomDeFichier(final VersionFile version) {
        if (version == null) {
            return null;
        }
        final Dataset dataset = version.getDataset();
        if (ACBBUtils.getSiteFromDataset(dataset) == null
                || ACBBUtils.getDatatypeFromDataset(dataset) == null
                || dataset.getDateDebutPeriode() == null || dataset.getDateFinPeriode() == null) {
            return AbstractRequestPropertiesACBB.CST_NEW_LINE;
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier.append(ACBBUtils.getSiteFromDataset(dataset).getPath())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS)
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DateUtil.DD_MM_YYYY))
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DateUtil.DD_MM_YYYY))
                    .append(".")
                    .append(IRequestPropertiesACBB.FORMAT_FILE);
        } catch (final Exception e) {
            return AbstractRequestPropertiesACBB.CST_NEW_LINE;
        }
        return nomFichier.toString();
    }

    /**
     * @return
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return this.testDuplicates;
    }

    /**
     * @param testDuplicates
     */
    public void setTestDuplicates(ITestDuplicates testDuplicates) {
        this.testDuplicates = testDuplicates;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#setDateDeFin(java.time.LocalDateTime)
     */
    @Override
    public void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.MINUTES, 30);
    }

    /**
     * @param sequenceFluxToursDAO
     */
    public void setSequenceFluxToursDAO(
            ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO) {
    }

    /**
     * Test nom fichier.
     *
     * @param nomfichier        the nomfichier
     * @param badsFormatsReport the bads formats report
     */
    public void testNomFichier(final String nomfichier, final BadsFormatsReport badsFormatsReport) {
        final Matcher matches = Pattern.compile(AbstractRequestPropertiesACBB.PATTERN_FILE_NAME)
                .matcher(nomfichier);
        String originalSiteChemin = AbstractRequestPropertiesACBB.CST_NEW_LINE;
        String originalDatatypeFrequence = AbstractRequestPropertiesACBB.CST_NEW_LINE;
        String originalDateDeDebut = AbstractRequestPropertiesACBB.CST_NEW_LINE;
        String originalDateDeFin = AbstractRequestPropertiesACBB.CST_NEW_LINE;
        if (!matches.matches()) {
            badsFormatsReport.addException(new BadExpectedValueException(this
                    .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_BAD_NAME_FILE)));
            return;
        } else {
            originalSiteChemin = matches.group(1);
            originalDatatypeFrequence = matches.group(2);
            originalDateDeDebut = matches.group(3);
            originalDateDeFin = matches.group(4);
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier.append(this.getSite().getPath())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS)
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(getDateDeDebut(), DateUtil.DD_MM_YYYY_FILE))
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(getDateDeFin(), DateUtil.DD_MM_YYYY_FILE))
                    .append(".")
                    .append(IRequestPropertiesACBB.FORMAT_FILE);
            final Matcher matches2 = Pattern.compile(
                    AbstractRequestPropertiesACBB.PATTERN_FILE_NAME).matcher(nomFichier);
            String siteChemin = AbstractRequestPropertiesACBB.CST_NEW_LINE;
            String datatypeFrequence = AbstractRequestPropertiesACBB.CST_NEW_LINE;
            String dateDeDebut = AbstractRequestPropertiesACBB.CST_NEW_LINE;
            String dateDeFin = AbstractRequestPropertiesACBB.CST_NEW_LINE;
            if (!matches2.matches()) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_NAME_FILE)));
            } else {
                siteChemin = matches2.group(1);
                datatypeFrequence = matches2.group(2);
                dateDeDebut = matches2.group(3).replace(AbstractRequestPropertiesACBB.CST_SLASH,
                        AbstractRequestPropertiesACBB.CST_HYPHEN);
                dateDeFin = matches2.group(4).replace(AbstractRequestPropertiesACBB.CST_SLASH,
                        AbstractRequestPropertiesACBB.CST_HYPHEN);
            }
            if (!originalSiteChemin.equals(siteChemin)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_SITE)));
            }
            if (!originalDatatypeFrequence.equals(datatypeFrequence)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_DATATYPE)));
            }
            if (!originalDateDeDebut.equals(dateDeDebut)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_BEGIN_DATE)));
            }
            if (!originalDateDeFin.equals(dateDeFin)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_END_DATE)));
            }
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new BadExpectedValueException(this
                    .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_BAD_NAME_FILE)));
        }
    }
}
