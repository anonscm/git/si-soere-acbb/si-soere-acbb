/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.ws.serviceextractflux;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Locale;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
@XmlRootElement(name = "traitement")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
class TraitementNode {
    LocalDateTime mindate;
    LocalDateTime maxdate;
    String agroecosystem;
    String site;
    String parcelle;
    String traitement;
    String affichage;
    Long tra_id;

    public TraitementNode(ILocalizationManager localizationManager, Object[] s) {
        this.mindate = ((Timestamp)s[0]).toLocalDateTime();
        this.maxdate = ((Timestamp)s[1]).toLocalDateTime();
        this.agroecosystem = (String) s[2];
        this.site = (String) s[3];
        this.parcelle = (String) s[4];
        this.traitement = (String) s[5];
        this.affichage = (String) s[6];
        this.tra_id = ((BigInteger) s[7]).longValue();
    }

    public LocalDateTime getMindate() {
        return mindate;
    }

    public String getMindateString() {
        return DateUtil.getUTCDateTextFromLocalDateTime(mindate, DateUtil.DD_MM_YYYY_HH_MM_SS);
    }

    public void setMindate(LocalDateTime mindate) {
        this.mindate = mindate;
    }

    public LocalDateTime getMaxdate() {
        return maxdate;
    }

    public String getMaxdateString() {
        return DateUtil.getUTCDateTextFromLocalDateTime(maxdate, DateUtil.DD_MM_YYYY_HH_MM_SS);
    }

    public void setMaxdate(LocalDateTime maxdate) {
        this.maxdate = maxdate;
    }

    public String getAgroecosystem() {
        return agroecosystem;
    }

    public void setAgroecosystem(String agroecosystem) {
        this.agroecosystem = agroecosystem;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getParcelle() {
        return parcelle;
    }

    public void setParcelle(String parcelle) {
        this.parcelle = parcelle;
    }

    public String getTraitement() {
        return traitement;
    }

    public void setTraitement(String traitement) {
        this.traitement = traitement;
    }

    public String getAffichage() {
        return affichage;
    }

    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

    public Long getTra_id() {
        return tra_id;
    }

    public void setTra_id(Long tra_id) {
        this.tra_id = tra_id;
    }
}
