package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.acbb.dataset.VariableValue;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The Class LineRecord.
 * <p>
 * record one line of flux data
 */
class FluxLineRecord implements Comparable<FluxLineRecord> {

    /**
     * The date @link(Date).
     */
    LocalDateTime dateTime;

    /**
     * The original line number <long>.
     */
    Long originalLineNumber;

    /**
     * The variables values @link(List<VariableValue>).
     */
    List<VariableValue> variablesValues;

    /**
     * Instantiates a new line record.
     *
     * @param processRecordFlux TODO
     */
    FluxLineRecord() {

    }

    /**
     * Instantiates a new line record.
     *
     * @param processRecordFlux
     * @param date               the date
     * @param time               the time
     * @param variablesValues    the variables values
     * @param originalLineNumber the original line number
     * @param processRecordFlux  TODO
     */
    FluxLineRecord(final LocalDateTime datetime, final List<VariableValue> variablesValues, final Long originalLineNumber) {
        super();
        this.dateTime = datetime;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int
     * @link(LineRecord) the o
     * @link(LineRecord) the o
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final FluxLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     * Copy.
     *
     * @param line the line
     */
    public void copy(final FluxLineRecord line) {
        this.dateTime = line.getDateTime();
        this.variablesValues = line.getVariablesValues();
        this.originalLineNumber = line.getOriginalLineNumber();
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDateTime() {
        return this.dateTime;
    }

    /**
     * Gets the original line number.
     *
     * @return the original line number
     */
    public Long getOriginalLineNumber() {
        return this.originalLineNumber;
    }

    /**
     * Gets the variables values.
     *
     * @return the variables values
     */
    public List<VariableValue> getVariablesValues() {
        return this.variablesValues;
    }

    /**
     * Sets the variables values.
     *
     * @param variablesValues the new variables values
     */
    public final void setVariablesValues(final List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
