/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl;

import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.IDateFormParameter;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Class FluxMeteoParameterVO.
 */
public class FluxMeteoParameterVO extends DefaultParameter implements IParameter {

    /**
     * The Constant FLUXCHAMBRE.
     */
    public static final String FLUXCHAMBRE = IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES;

    /**
     * The Constant FLUXTOURS.
     */
    public static final String FLUXTOURS = IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS;

    /**
     * The Constant METEO.
     */
    public static final String METEO = IMeteoDatatypeManager.CODE_DATATYPE_METEO;
    /**
     * The Constant FLUX_METEO_EXTRACTION_TYPE_CODE @link(String).
     */
    static final String FLUX_METEO_EXTRACTION_TYPE_CODE = "flux_gazeux_et_meteorologie";

    /**
     * The selected sites @link(List<SiteACBB>).
     */
    List<SiteACBB> selectedSites = new LinkedList();

    /**
     * The commentaires @link(String).
     */
    String commentaires;

    /**
     * The dates form1 param vo @link(DatesForm1ParamVO).
     */
    IDateFormParameter datesForm1ParamVO;

    /**
     * The affichage @link(int).
     */
    int affichage;

    /**
     * Instantiates a new flux meteo parameter vo.
     */
    public FluxMeteoParameterVO() {
    }

    /**
     * Instantiates a new flux meteo parameter vo.
     *
     * @param metadatasMap the metadatas map
     */
    public FluxMeteoParameterVO(final Map<String, Object> metadatasMap) {
        this.setParameters(metadatasMap);
        this.setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public int getAffichage() {
        return this.affichage;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public final void setAffichage(final int affichage) {
        this.affichage = affichage;
    }

    /**
     * Gets the commentaires.
     *
     * @return the commentaires
     */
    public String getCommentaires() {
        return this.commentaires;
    }

    /**
     * Sets the commentaires.
     *
     * @param commentaires the new commentaires
     */
    public final void setCommentaires(final String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     * Gets the dates form1 param vo.
     *
     * @return the dates form1 param vo
     */
    public DatesFormParamVO getDatesForm1ParamVO() {
        return (DatesFormParamVO) this.datesForm1ParamVO;
    }

    /**
     * Sets the dates form1 param vo.
     *
     * @param datesForm1ParamVO the new dates form1 param vo
     */
    public final void setDatesForm1ParamVO(final DatesFormParamVO datesForm1ParamVO) {
        this.datesForm1ParamVO = datesForm1ParamVO;
    }

    /**
     * Gets the extraction type code.
     *
     * @return the extraction type code
     * @see org.inra.ecoinfo.extraction.IParameter#getExtractionTypeCode()
     */
    @Override
    public String getExtractionTypeCode() {
        return FluxMeteoParameterVO.FLUX_METEO_EXTRACTION_TYPE_CODE;
    }

    /**
     * Gets the selected sites.
     *
     * @return the selected sites
     */
    public List<SiteACBB> getSelectedSites() {
        return this.selectedSites;
    }

    /**
     * Sets the selected sites.
     *
     * @param selectedSites the new selected sites
     */
    public final void setSelectedSites(final List<SiteACBB> selectedSites) {
        this.selectedSites = selectedSites == null ? new LinkedList<>() : selectedSites;
    }
}
