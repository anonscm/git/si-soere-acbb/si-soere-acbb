package org.inra.ecoinfo.acbb.dataset.fluxchambres.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesFluxMeteo;
import org.inra.ecoinfo.acbb.dataset.ITestValues;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Map;

/**
 * The Class TestValuesFlux.
 * <p>
 * implementation of {@link ITestValues}
 *
 * @author Tcherniatinsky Philippe
 * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues
 * @see org.inra.ecoinfo.acbb.dataset.ITestValues
 */
public class TestValuesChambres extends GenericTestValues {
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new test values flux.
     */
    public TestValuesChambres() {
        super();
    }

    /**
     * Check date type value.
     *
     * @param values
     * @param badsFormatsReport
     * @param lineNumber            <long> the line number
     * @param index
     * @param value
     * @param column
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @return the date
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#checkDateTypeValue(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, long, int, java.lang.String,
     * org.inra.ecoinfo.dataset.Column, java.util.Map,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB)
     */
    @Override
    protected LocalDate checkDateTypeValue(final String[] values,
                                           final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                           final String value, final Column column,
                                           final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                           final DatasetDescriptorACBB datasetDescriptor,
                                           final IRequestPropertiesACBB requestPropertiesACBB) {
        final LocalDate date = super.checkDateTypeValue(values, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        if (date == null) {
            return null;
        }
        try {
            String dateToString = ((IRequestPropertiesFluxMeteo) requestPropertiesACBB)
                    .dateToStringYYYYMMJJHHMMSS(values[index], "00:00:00");
            requestPropertiesACBB.addDate(dateToString);
        } catch (final BadExpectedValueException e) {
            badsFormatsReport.addException(new BadExpectedValueException(e.getMessage()));
        } catch (final DateTimeException e) {
            badsFormatsReport
                    .addException(new BadExpectedValueException(String.format(
                            RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE),
                            value, lineNumber, index + 1, column.getName(),
                            requestPropertiesACBB.getDateFormat())));
        }
        return date;
    }

    /**
     * Test values.
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#testValues(long,
     *                           com.Ostermiller.util.CSVParser,
     *                           org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *                           org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *                           org.inra.ecoinfo.dataset.BadsFormatsReport,
     *                           org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB, java.lang.String)
     * @link(long) the startline
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(String) the datatype name
     */
    @Override
    public void testValues(final long startline, final CSVParser parser,
                           final VersionFile versionFile, final IRequestPropertiesACBB requestProperties,
                           final String encoding, final BadsFormatsReport badsFormatsReport,
                           final DatasetDescriptorACBB datasetDescriptor, final String datatypeName)
            throws BusinessException {
        super.testValues(startline, parser, versionFile, requestProperties, encoding,
                badsFormatsReport, datasetDescriptor, datatypeName);
    }

}
