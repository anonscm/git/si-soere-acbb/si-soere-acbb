/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.jobs.StatusBar;

/**
 * The Class FluxMeteoOutputsBuildersResolver.
 */
public class FluxMeteoOutputsBuildersResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {

    /**
     * The Constant EXTRACTION @link(String).
     */
    static final String EXTRACTION = "extraction";

    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     * The Constant SEPARATOR_TEXT.
     */
    static final String SEPARATOR_TEXT = "_";

    /**
     * The Constant EXTENSION_ZIP.
     */
    static final String EXTENSION_ZIP = ".zip";

    /**
     * The flux chambre output builder.
     */
    IOutputBuilder fluxChambreOutputBuilder;

    /**
     * The flux tours output builder.
     */
    IOutputBuilder fluxToursOutputBuilder;

    /**
     * The meteo output builder.
     */
    IOutputBuilder meteoOutputBuilder;

    /**
     * The request reminder output builder.
     */
    IOutputBuilder requestReminderOutputBuilder;

    /**
     * Adds the output builder by condition.
     *
     * @param condition the condition
     * @param outputBuilder the output builder
     * @param outputsBuilders the outputs builders
     */
    protected void addOutputBuilderByCondition(final Boolean condition,
            final IOutputBuilder outputBuilder, final List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     * .extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            parameters.getParameters().put(FluxMeteoExtractor.CST_RESULTS, parameters.getResults());
            final List<IOutputBuilder> roBuildZipOutputStream = this
                    .resolveOutputsBuilders(parameters.getParameters());
            NoExtractionResultException noDataToExtract;
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                noDataToExtract = null;
                StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
                statusBar.setProgress(50);
                int progressToAdd = 50/roBuildZipOutputStream.size();
                for (final IOutputBuilder iOutputBuilder : roBuildZipOutputStream) {
                    try {
                        iOutputBuilder.buildOutput(parameters);
                        statusBar.setProgress(statusBar.getProgress()+progressToAdd);
                    } catch (final NoExtractionResultException e) {
                        noDataToExtract = e;
                    }
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
        return rObuildZipOutputStream;
    }

    /**
     * Resolve outputs builders.
     *
     * @param metadatasMap
     * @return the list @see
     * org.inra.ecoinfo.extraction.IOutputsBuildersResolver#resolveOutputsBuilders
     * (java.util.Map)
     * @link(Map<String,Object>) the metadatas map
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(final Map<String, Object> metadatasMap) {
        final List<IOutputBuilder> outputsBuilders = new LinkedList();
        final Map<String, Map<String, List>> results = (Map<String, Map<String, List>>) metadatasMap
                .get(FluxMeteoExtractor.CST_RESULTS);
        this.addOutputBuilderByCondition(
                results.get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE) != null,
                this.fluxChambreOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_TOURS_CODE) != null,
                this.fluxToursOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE) != null,
                this.meteoOutputBuilder, outputsBuilders);
        outputsBuilders.add(this.requestReminderOutputBuilder);
        return outputsBuilders;
    }

    /**
     * Sets the flux chambre output builder.
     *
     * @param fluxChambreOutputBuilder the new flux chambre output builder
     */
    public final void setFluxChambreOutputBuilder(final IOutputBuilder fluxChambreOutputBuilder) {
        this.fluxChambreOutputBuilder = fluxChambreOutputBuilder;
    }

    /**
     * Sets the flux tours output builder.
     *
     * @param fluxToursOutputBuilder the new flux tours output builder
     */
    public final void setFluxToursOutputBuilder(final IOutputBuilder fluxToursOutputBuilder) {
        this.fluxToursOutputBuilder = fluxToursOutputBuilder;
    }

    /**
     * Sets the meteo output builder.
     *
     * @param meteoOutputBuilder the new meteo output builder
     */
    public final void setMeteoOutputBuilder(final IOutputBuilder meteoOutputBuilder) {
        this.meteoOutputBuilder = meteoOutputBuilder;
    }

    /**
     * Sets the request reminder output builder.
     *
     * @param requestReminderOutputBuilder the new request reminder output
     * builder
     */
    public final void setRequestReminderOutputBuilder(
            final IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
