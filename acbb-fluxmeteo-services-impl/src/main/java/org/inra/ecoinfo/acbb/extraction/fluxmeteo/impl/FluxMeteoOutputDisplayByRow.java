/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FluxMeteoOutputDisplayByRow.
 */
public class FluxMeteoOutputDisplayByRow extends FluxMeteoOutputsBuildersResolver {

    /**
     * The flux chambre build output by row @link(IOutputBuilder).
     */
    IOutputBuilder fluxChambreBuildOutputByRow;

    /**
     * The flux tours build output by row @link(IOutputBuilder).
     */
    IOutputBuilder fluxToursBuildOutputByRow;

    /**
     * The meteo build output by row @link(IOutputBuilder).
     */
    IOutputBuilder meteoBuildOutputByRow;

    /**
     * Instantiates a new flux meteo output display by row.
     */
    public FluxMeteoOutputDisplayByRow() {
        super();
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @link(IParameter)
     * @see org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl. FluxMeteoOutputsBuildersResolver
     * #buildOutput(org.inra.ecoinfo.extraction.IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) {
        try {
            super.setFluxChambreOutputBuilder(this.fluxChambreBuildOutputByRow);
            super.setFluxToursOutputBuilder(this.fluxToursBuildOutputByRow);
            super.setMeteoOutputBuilder(this.meteoBuildOutputByRow);
            return super.buildOutput(parameters);
        } catch (BusinessException e) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("error while building output", e);
            return null;
        }
    }

    /**
     * Sets the flux chambre build output by row.
     *
     * @param fluxChambreBuildOutputByRow the new flux chambre build output by
     *                                    row
     */
    public void setFluxChambreBuildOutputByRow(
            final IOutputBuilder fluxChambreBuildOutputByRow) {
        this.fluxChambreBuildOutputByRow = fluxChambreBuildOutputByRow;
    }

    /**
     * Sets the flux tours build output by row.
     *
     * @param fluxToursBuildOutputByRow the new flux tours build output by row
     */
    public void setFluxToursBuildOutputByRow(final IOutputBuilder fluxToursBuildOutputByRow) {
        this.fluxToursBuildOutputByRow = fluxToursBuildOutputByRow;
    }

    /**
     * Sets the meteo build output by row.
     *
     * @param meteoBuildOutputByRow the new meteo build output by row
     */
    public void setMeteoBuildOutputByRow(final IOutputBuilder meteoBuildOutputByRow) {
        this.meteoBuildOutputByRow = meteoBuildOutputByRow;
    }
}
