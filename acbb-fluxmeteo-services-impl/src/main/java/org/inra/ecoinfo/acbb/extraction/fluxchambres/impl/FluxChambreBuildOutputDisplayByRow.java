package org.inra.ecoinfo.acbb.extraction.fluxchambres.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoExtractor;
import org.inra.ecoinfo.acbb.extraction.impl.ComparatorVariable;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;

/**
 * The Class FluxChambreBuildOutputDisplayByRow.
 */
public class FluxChambreBuildOutputDisplayByRow extends AbstractOutputBuilder {

    /**
     * The Constant HEADER_RAW_DATA.
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.fluxchambres.messages";
    /**
     * The msg missing variable in references datas @link(String).
     */
    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";
    /**
     * The Constant PATTERN_WORD @link(String).
     */
    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";
    /**
     * The Constant PATTERB_CSV_6_FIELD @link(String).
     */
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s";
    /**
     * The Constant CST_NEW_LINE @link(String).
     */
    static final String CST_NEW_LINE = "\n";
    /**
     * The Constant CST_CVS_5_EMPTY_FIELD @link(String).
     */
    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";
    /**
     * The Constant CST_0 @link(String).
     */
    static final String CST_0 = "0";
    private static final int TEN = 10;
    /**
     * The comparator.
     */
    final ComparatorVariable comparator = new ComparatorVariable();
    /**
     * The variable dao.
     */
    IVariableDAO variableDAO;
    /**
     * The datatype unite variable acbbdao.
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;

    /**
     * Builds the body.
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder #buildBody(java
     *                           .lang.String, java.util.Map, java.util.Map)
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final List<SiteACBB> selectedSites = (List<SiteACBB>) requestMetadatasMap
                .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList());
        final List<Variable> selectedFluxChambreVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES), new ArrayList());
        final List<MesureFluxChambre> mesuresFluxChambres = resultsDatasMap
                .getOrDefault(FluxChambreExtractor.MAP_INDEX_FLUXCHAMBRE, new ArrayList());
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES);
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<Parcelle, Map<LocalDateTime, MesureFluxChambre>>> mesuresFluxChambresMap = new TreeMap();
        try {
            this.buildmap(mesuresFluxChambres, mesuresFluxChambresMap);
        } catch (DateTimeException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedSites, selectedFluxChambreVariables, outputPrintStreamMap, mesuresFluxChambresMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @return the string
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder #buildHeader(java
     *                           .util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Properties propertiesVariableName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        final ErrorsReport errorsReport = new ErrorsReport();
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .get(Variable.class.getSimpleName().toLowerCase().concat(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES));
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        try {
            stringBuilder1.append(String.format(
                    FluxChambreBuildOutputDisplayByRow.PATTERN_WORD,
                    this.getLocalizationManager().getMessage(
                            FluxChambreBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                            FluxChambreBuildOutputDisplayByRow.HEADER_RAW_DATA)));
            stringBuilder2.append(FluxChambreBuildOutputDisplayByRow.CST_CVS_6_EMPTY_FIELD);
            stringBuilder3.append(FluxChambreBuildOutputDisplayByRow.CST_CVS_6_EMPTY_FIELD);
            for (final Variable variableVO : selectedVariables) {
                final VariableACBB variable = (VariableACBB) this.variableDAO.getByCode(variableVO
                        .getCode()).orElse(null);
                if (variable == null) {
                    errorsReport
                            .addErrorMessage(String
                                    .format(this
                                                    .getLocalizationManager()
                                                    .getMessage(
                                                            FluxChambreBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                                                            FluxChambreBuildOutputDisplayByRow.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                            variableVO.getCode()));
                    continue;
                }
                String localizedVariableName = propertiesVariableName
                        .getProperty(variable.getName(), variable.getName());
                if (Strings.isNullOrEmpty(localizedVariableName)) {
                    localizedVariableName = variable.getName();
                }
                Optional<Unite> unite = this.datatypeVariableUniteACBBDAO
                        .getUnite(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES,
                                variable.getCode());
                stringBuilder1.append(String
                        .format(ACBBMessages.PATTERN_1_FIELD,
                                localizedVariableName));
                stringBuilder2.append(String.format(
                        ACBBMessages.PATTERN_1_FIELD, unite.map(u -> u.getCode()).orElseGet(String::new)));
                stringBuilder3.append(String.format(
                        ACBBMessages.PATTERN_1_FIELD,
                        variable.getAffichage()));
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder1.append(FluxChambreBuildOutputDisplayByRow.CST_NEW_LINE)
                .append(stringBuilder2).append(FluxChambreBuildOutputDisplayByRow.CST_NEW_LINE)
                .append(stringBuilder3).toString();
    }

    private void buildmap(
            final List<MesureFluxChambre> mesuresFluxChambres,
            final SortedMap<Long, SortedMap<Parcelle, Map<LocalDateTime, MesureFluxChambre>>> mesuresFluxChambresMap)
            throws DateTimeException {
        Iterator<MesureFluxChambre> itMesure = mesuresFluxChambres
                .iterator();
        while (itMesure.hasNext()) {
            MesureFluxChambre mesureFluxChambre = itMesure
                    .next();
            Parcelle parcelle = mesureFluxChambre.getSuiviParcelle().getParcelle();
            Long siteId = parcelle.getSite().getId();
            mesuresFluxChambresMap
                    .computeIfAbsent(siteId, k -> new TreeMap<>())
                    .computeIfAbsent(parcelle, k -> new TreeMap<>())
                    .put(mesureFluxChambre.getDate().atStartOfDay(), mesureFluxChambre);
            itMesure.remove();
        }
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception
     * @link(IParameter) the parameters
     * @see org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     * .extraction.IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE)
                .get(FluxChambreExtractor.MAP_INDEX_FLUXCHAMBRE) == null
                || ((DefaultParameter) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE)
                .get(FluxChambreExtractor.MAP_INDEX_FLUXCHAMBRE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters,
                        FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE));
        return null;
    }

    /**
     * Fill line.
     *
     * @param rawDataPrintStream           the raw data print stream
     * @param selectedFluxChambreVariables the selected flux chambre variables
     * @param site                         the site
     * @param bloc                         the bloc
     * @param repetition
     * @param parcelle                     the parcelle
     * @param traitement                   the traitement
     * @param date                         the date
     * @param chambre                      the chambre
     * @param valeursMesuresFluxChambre    the valeurs mesures flux chambre
     */
    protected void fillLine(final PrintStream rawDataPrintStream,
                            final List<Variable> selectedFluxChambreVariables, final String site,
                            final String bloc, final String repetition, final String parcelle,
                            final String traitement, final String date, final String chambre,
                            final List<ValeurFluxChambre> valeursMesuresFluxChambre) {
        final String line = String.format(FluxChambreBuildOutputDisplayByRow.PATTERB_CSV_7_FIELD,
                site, bloc, repetition, parcelle, traitement, date, chambre);
        rawDataPrintStream.print(line);

        for (final Variable variable : selectedFluxChambreVariables) {
            rawDataPrintStream.print(";");
            String displayValue = null;
            if (valeursMesuresFluxChambre != null) {
                for (final ValeurFluxChambre ValeurFluxChambre : valeursMesuresFluxChambre) {
                    if (getVariableFromValeur(ValeurFluxChambre).getId().equals(variable.getId())) {
                        displayValue = ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(ValeurFluxChambre.getValeur());
                        break;
                    }
                }
            }
            if (displayValue != null) {
                rawDataPrintStream.print(displayValue);
            } else {
                rawDataPrintStream.print(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE);
            }
        }
    }

    private VariableACBB getVariableFromValeur(ValeurFluxChambre valeur) {
        return (VariableACBB) ((DatatypeVariableUniteACBB) valeur.getRealNode().getNodeable()).getVariable();
    }

    /**
     * Format.
     *
     * @param nombre
     * @param nombreDeChiffresSignificatifs the nombre de chiffres significatifs
     * @return the string
     */
    protected String format(final float nombre, final int nombreDeChiffresSignificatifs) {
        float localNombre = nombre;
        if (Float.floatToRawIntBits(localNombre) == 0) {
            return FluxChambreBuildOutputDisplayByRow.CST_0;
        }
        int exposant = 0;
        final float arrondi = (float) Math.pow(10, nombreDeChiffresSignificatifs - 1);
        while (Math.abs(localNombre) <= arrondi) {
            localNombre *= FluxChambreBuildOutputDisplayByRow.TEN;
            exposant++;
        }
        return new StringBuffer().append(
                Math.round(localNombre)
                        / Math.pow(FluxChambreBuildOutputDisplayByRow.TEN, exposant)).toString();
    }

    private void readMap(
            final List<SiteACBB> selectedSites,
            final List<Variable> selectedFluxChambreVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<Parcelle, Map<LocalDateTime, MesureFluxChambre>>> mesuresFluxChambresMap) {
        try {
            String currentSite;
            String currentBloc;
            String currentRepetition;
            String currentParcelle;
            String currentTraitement;
            String currentChambre;
            Properties traitementNameProperties = localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_NAME);
            Properties siteNameProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
            Properties parcelleNameProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME);
            for (final SiteACBB site : selectedSites) {
                final SortedMap<Parcelle, Map<LocalDateTime, MesureFluxChambre>> mesureFluxChambresMapBySite = mesuresFluxChambresMap.get(site.getId());
                if (mesureFluxChambresMapBySite == null) {
                    continue;
                }
                Iterator<Entry<Parcelle, Map<LocalDateTime, MesureFluxChambre>>> itParcelle = mesureFluxChambresMapBySite.entrySet().iterator();
                while (itParcelle.hasNext()) {
                    Entry<Parcelle, Map<LocalDateTime, MesureFluxChambre>> parcelleEntry = itParcelle.next();
                    Parcelle parcelle = parcelleEntry.getKey();
                    Map<LocalDateTime, MesureFluxChambre> mesureFluxChambresMap = parcelleEntry.getValue();
                    if (mesureFluxChambresMap != null) {
                        final PrintStream rawDataPrintStream = outputPrintStreamMap.get(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES);
                        for (Iterator<Entry<LocalDateTime, MesureFluxChambre>> iterator = mesureFluxChambresMap.entrySet().iterator(); iterator.hasNext(); ) {
                            Entry<LocalDateTime, MesureFluxChambre> dateEntry = iterator.next();
                            LocalDateTime date = dateEntry.getKey();
                            MesureFluxChambre mesureFluxChambre = dateEntry.getValue();
                            currentSite = siteNameProperties.getProperty(parcelle.getSite().getName(), parcelle.getSite().getName());
                            currentBloc = parcelle.getBloc() != null ? parcelle.getBloc().getBlocName()
                                    : org.apache.commons.lang.StringUtils.EMPTY;
                            currentRepetition = parcelle.getBloc() != null ? parcelle.getBloc()
                                    .getRepetitionName()
                                    : org.apache.commons.lang.StringUtils.EMPTY;
                            currentParcelle = parcelleNameProperties.getProperty(parcelle.getName(), parcelle.getName());
                            currentTraitement = traitementNameProperties.getProperty(mesureFluxChambre.getSuiviParcelle().getTraitement().getNom(), mesureFluxChambre.getSuiviParcelle().getTraitement().getNom());
                            currentChambre = String.format(
                                    FluxChambreBuildOutputDisplayByRow.PATTERN_WORD,
                                    mesureFluxChambre.getNbrChambre());
                            this.fillLine(
                                    rawDataPrintStream,
                                    selectedFluxChambreVariables,
                                    currentSite,
                                    currentBloc,
                                    currentRepetition,
                                    currentParcelle,
                                    currentTraitement,
                                    DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                                    currentChambre,
                                    mesureFluxChambre.getValeurs());
                            rawDataPrintStream.println();
                        }
                    }
                    itParcelle.remove();
                }
            }
        } catch (final DateTimeException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteDAO the new datatype unite variable acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteDAO;
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }
}
