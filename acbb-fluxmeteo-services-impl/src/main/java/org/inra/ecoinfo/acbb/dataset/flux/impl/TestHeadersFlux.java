package org.inra.ecoinfo.acbb.dataset.flux.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.flux.IFluxToursDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.flux.IRequestPropertiesFluxTours;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.IOException;

/**
 * The Class TestHeadersFlux.
 *
 * @author Tcherniatinsky Philippe
 */
public class TestHeadersFlux extends GenericTestHeader {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new test headers flux.
     */
    public TestHeadersFlux() {
        super();
    }

    /**
     * Test headers.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return the long
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.dataset.ITestHeaders#testHeaders(com.Ostermiller
     *                           .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *                           org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *                           org.inra.ecoinfo.dataset.BadsFormatsReport,
     *                           org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final String encoding,
                            final BadsFormatsReport badsFormatsReport, final DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException {
        super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport,
                datasetDescriptor);
        final IRequestPropertiesFluxTours requestPropertiesFluxTours = (IRequestPropertiesFluxTours) requestProperties;
        requestPropertiesFluxTours.initDate();
        long lineNumber = 0;
        try {
            lineNumber = this.readSiteAndParcelle(versionFile, badsFormatsReport, parser,
                    lineNumber, requestProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    IFluxToursDatatypeManager.CODE_DATATYPE_FLUX_TOURS);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, requestProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, requestProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 1);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, requestProperties);
            lineNumber = this.testUnites(parser.getLine(), lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 2);
        } catch (final IOException e) {
            this.getLogger().debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }

    /**
     * Test unites.
     *
     * @param line
     * @param lineNumber <long> the line number
     * @return the long
     * @link(String[]) the line
     * @link(String[]) the line
     */
    long testUnites(final String[] line, final long lineNumber) {
        return lineNumber + 1;
    }

}
