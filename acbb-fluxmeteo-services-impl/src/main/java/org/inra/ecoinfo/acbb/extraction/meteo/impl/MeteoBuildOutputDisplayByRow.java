/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.meteo.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorBuilderACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.SequenceMeteo;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoExtractor;
import org.inra.ecoinfo.acbb.extraction.impl.ComparatorVariable;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.VariableDescriptor;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.Map.Entry;

/**
 * The Class MeteoBuildOutputDisplayByRow.
 */
public class MeteoBuildOutputDisplayByRow extends AbstractOutputBuilder {

    /**
     * The Constant HEADER_RAW_DATA.
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.meteo.messages";
    /**
     * The msg missing variable in references datas @link(String).
     */
    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_CST_UNAVAILABLE_VARIABLE";
    /**
     * The Constant CST_NEW_LINE @link(String).
     */
    static final String CST_NEW_LINE = "\n";
    /**
     * The Constant REGEX_WORD @link(String).
     */
    static final String REGEX_WORD = "[^;]";
    /**
     * The Constant PATTERN_WORD @link(String).
     */
    static final String PATTERN_WORD = "%s";
    /**
     * The Constant CST_0 @link(String).
     */
    static final String CST_0 = "0";
    /**
     * The Constant DATASET_DESCRIPTOR_XML.
     */
    static final String DATASET_DESCRIPTOR_XML = "org/inra/ecoinfo/acbb/dataset/meteo/impl/dataset-descriptor.xml";
    private static final int TEN = 10;
    /**
     * The org.inra.ecoinfo.acbb.dataset descriptor @link(DatasetDescriptor).
     */
    static DatasetDescriptor datasetDescriptor;
    /**
     * The comparator {@link Comparator}.
     */
    final ComparatorVariable comparator = new ComparatorVariable();
    /**
     * The variable dao {@link IVariableACBBDAO}.
     */
    IVariableACBBDAO variableDAO;

    /**
     * The datatype unite variable acbbdao {@link IDatatypeVariableUniteACBBDAO}
     * .
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     * The variables descriptor {@link Map}.
     */
    Map<String, VariableDescriptor> variablesDescriptor = new HashMap();

    /**
     * Instantiates a new meteo build output display by row.
     */
    public MeteoBuildOutputDisplayByRow() {
        super();
        try {
            if (MeteoBuildOutputDisplayByRow.datasetDescriptor == null) {
                MeteoBuildOutputDisplayByRow.setDatasetDescriptor(DatasetDescriptorBuilderACBB
                        .buildDescriptorACBB(this
                                .getClass()
                                .getClassLoader()
                                .getResourceAsStream(
                                        MeteoBuildOutputDisplayByRow.DATASET_DESCRIPTOR_XML)));
                for (final Column column : MeteoBuildOutputDisplayByRow.datasetDescriptor
                        .getColumns()) {
                    if (null != column.getFlagType()) {
                        switch (column.getFlagType()) {
                            case RecorderACBB.PROPERTY_CST_VARIABLE_TYPE:
                            case RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE:
                                this.variablesDescriptor.put(column.getName(),
                                        new VariableDescriptor(column.getName()));
                                break;
                            case RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE:
                                this.variablesDescriptor.get(column.getRefVariableName())
                                        .setHasQualityClass(true);
                                break;
                        }
                    }
                }
            }
        } catch (final IOException | SAXException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset descriptor.
     *
     * @param datasetDescriptor the new org.inra.ecoinfo.acbb.dataset descriptor
     * @link(DatasetDescriptor) {@link DatasetDescriptor} the new org.inra.ecoinfo.acbb.dataset
     * descriptor
     */
    static void setDatasetDescriptor(final DatasetDescriptor datasetDescriptor) {
        MeteoBuildOutputDisplayByRow.datasetDescriptor = datasetDescriptor;
    }

    /**
     * Builds the body.
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        LocalDate startDate = DateUtil.MIN_LOCAL_DATE;
        LocalDate endDate = DateUtil.MAX_LOCAL_DATE;
        final DatesFormParamVO dateFormParam = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        try {
            startDate = dateFormParam.getDateStart();
            endDate = dateFormParam.getDateEnd();
        } catch (final DateTimeException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
        final List<Variable> selectedMeteosVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(IMeteoDatatypeManager.CODE_DATATYPE_METEO), new ArrayList());
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(IMeteoDatatypeManager.CODE_DATATYPE_METEO);
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            outputPrintStreamMap.get(datatypeNameEntry.getKey()).println(headers);
        }
        final PrintStream rawDataPrintStream = outputPrintStreamMap
                .get(IMeteoDatatypeManager.CODE_DATATYPE_METEO);
        final OutputHelper outputHelper = new OutputHelper(startDate, endDate, rawDataPrintStream,
                selectedMeteosVariables);
        final List<MesureMeteo> mesuresMeteo = resultsDatasMap.get(MeteoExtractor.MAP_INDEX_METEO);
        try {
            outputHelper.buildBody(mesuresMeteo);
        } catch (DateTimeException e) {
            AbstractOutputBuilder.LOGGER.error("error parsing date \n{}", e.getMessage());
        }
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     * @return the string
     * @throws BusinessException the business exception
     * @link(Map<String,Object>) the request metadatas map @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder #buildHeader(java
     * .util.Map)
     */
    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @return the string
     * @throws BusinessException the business exception
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Properties propertiesVariableName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        final ErrorsReport errorsReport = new ErrorsReport();
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(IMeteoDatatypeManager.CODE_DATATYPE_METEO), new ArrayList());
        final List<String> selectedVariablesAffichage = new LinkedList();
        for (final Variable variable : selectedVariables) {
            selectedVariablesAffichage.add(variable.getAffichage());
        }
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        try {
            stringBuilder1.append(String.format(MeteoBuildOutputDisplayByRow.PATTERN_WORD,
                    RecorderACBB.getACBBMessageWithBundle(
                            MeteoBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                            MeteoBuildOutputDisplayByRow.HEADER_RAW_DATA)));
            stringBuilder2.append(String.format(
                    MeteoBuildOutputDisplayByRow.PATTERN_WORD,
                    RecorderACBB.getACBBMessageWithBundle(
                            MeteoBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                            MeteoBuildOutputDisplayByRow.HEADER_RAW_DATA).replaceAll(
                            MeteoBuildOutputDisplayByRow.REGEX_WORD,
                            org.apache.commons.lang.StringUtils.EMPTY)));
            stringBuilder3.append(String.format(
                    MeteoBuildOutputDisplayByRow.PATTERN_WORD,
                    RecorderACBB.getACBBMessageWithBundle(
                            MeteoBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                            MeteoBuildOutputDisplayByRow.HEADER_RAW_DATA).replaceAll(
                            MeteoBuildOutputDisplayByRow.REGEX_WORD,
                            org.apache.commons.lang.StringUtils.EMPTY)));
            final Iterator<Column> it = MeteoBuildOutputDisplayByRow.datasetDescriptor.getColumns()
                    .iterator();
            Column column;
            VariableACBB variable;
            while (it.hasNext()) {
                column = it.next();
                if (column.isFlag()
                        && RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())) {
                    variable = (VariableACBB) this.variableDAO.getByAffichage(column.getName()).orElse(null);
                    if (variable == null) {
                        errorsReport
                                .addErrorMessage(String.format(
                                        RecorderACBB
                                                .getACBBMessageWithBundle(
                                                        MeteoBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                                                        MeteoBuildOutputDisplayByRow.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                        selectedVariables));
                        continue;
                    }
                    String localizedVariableName = propertiesVariableName.getProperty(variable.getName(), variable.getName());
                    if (Strings.isNullOrEmpty(localizedVariableName)) {
                        localizedVariableName = variable.getName();
                    }
                    Optional<Unite> unite = this.datatypeVariableUniteACBBDAO.getUnite(
                            IMeteoDatatypeManager.CODE_DATATYPE_METEO, variable.getCode());
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder1.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD, localizedVariableName));
                        stringBuilder2.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD, unite.map(u -> u.getCode()).orElseGet(String::new)));
                        stringBuilder3
                                .append(String.format(ACBBMessages.PATTERN_1_FIELD,
                                        variable.getAffichage()));
                    }
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder1.append(MeteoBuildOutputDisplayByRow.CST_NEW_LINE)
                .append(stringBuilder2).append(MeteoBuildOutputDisplayByRow.CST_NEW_LINE)
                .append(stringBuilder3).toString();
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @link(IParameter) the parameters
     * @throws FileNotFoundException the file not found exception
     * @throws BusinessException the business exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws NoExtractionResultException the no extract data found exception
     * @link(IParameter) the parameters
     * @see org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     *      .extraction.IParameter)
     */
    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE)
                .get(MeteoExtractor.MAP_INDEX_METEO) == null
                || ((DefaultParameter) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE)
                .get(MeteoExtractor.MAP_INDEX_METEO).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE));
        return null;
    }

    /**
     * Format.
     *
     * @param nombre
     * @param nombreDeChiffresSignificatifs the nombre de chiffres significatifs
     * @return the string
     */
    protected String format(final float nombre, final int nombreDeChiffresSignificatifs) {
        float finalNombre = nombre;
        if (Float.floatToRawIntBits(finalNombre) == 0) {
            return MeteoBuildOutputDisplayByRow.CST_0;
        }
        int exposant = 0;
        final float arrondi = (float) Math.pow(10, nombreDeChiffresSignificatifs - 1);
        while (Math.abs(finalNombre) <= arrondi) {
            finalNombre *= MeteoBuildOutputDisplayByRow.TEN;
            exposant++;
        }
        return new StringBuffer().append(
                Math.round(finalNombre) / Math.pow(MeteoBuildOutputDisplayByRow.TEN, exposant))
                .toString();
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public final void setVariableDAO(final IVariableACBBDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     * The Class OutputHelper.
     */
    class OutputHelper {

        /**
         * The Constant PATTERN_CSV_3_FIELDS @link(String).
         */
        static final String PATTERN_CSV_3_FIELDS = "%s;%s;%s";
        /**
         * The versions @link(List<VersionFile>).
         */
        final List<VersionFile> versions = new LinkedList();
        /**
         * The raw data print stream @link(PrintStream).
         */
        final PrintStream rawDataPrintStream;
        /**
         * The start date @link(Date).
         */
        final LocalDateTime localStartDate, localEndDate;
        /**
         * The variables affichage @link(List<String>).
         */
        final List<String> variablesAffichage = new LinkedList();
        /**
         * The sorted mesures @link(Map<String,Map<Date,MesureMeteo>>).
         */
        SortedMap<String, SortedMap<LocalDateTime, MesureMeteo>> sortedMesures = new TreeMap();
        LocalDateTime beginDateLocal, endDateLocal, dateMesure;
        /**
         * The site @link(String).
         */
        String site;

        /**
         * Instantiates a new output helper.
         *
         * @param utcStartDate
         * @param localEndDate
         * @param rawDataPrintStream
         * @param selectedMeteoVariables
         * @link(Date) the start date
         * @link(Date) the end date
         * @link(PrintStream) the raw data print stream
         * @link(List<Variable>) the selected meteo variables {@link Date} the start date
         * {@link Date} the end date {@link PrintStream} the raw data print
         * stream {@link List} the selected meteo variables
         */
        OutputHelper(final LocalDate localStartDate, final LocalDate localEndDate,
                     final PrintStream rawDataPrintStream, final List<Variable> selectedMeteoVariables) {
            super();
            this.localStartDate = localStartDate.atStartOfDay();
            this.localEndDate = localEndDate.atStartOfDay();
            this.rawDataPrintStream = rawDataPrintStream;
            for (final Column column : MeteoBuildOutputDisplayByRow.datasetDescriptor.getColumns()) {
                if (RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())) {
                    for (final Variable variable : selectedMeteoVariables) {
                        if (variable.getAffichage().equals(column.getName())) {
                            this.variablesAffichage.add(variable.getAffichage());
                        }
                    }
                }
            }
        }

        /**
         * Adds the mesure.
         *
         * @param mesure
         * @throws DateTimeException
         * @link(MesureMeteo) the mesure {@link MesureMeteo} the mesure
         */
        public void addMesure(final MesureMeteo mesure) throws DateTimeException {
            final SequenceMeteo sequence = mesure.getSequenceMeteo();
            final VersionFile version = sequence.getVersion();
            this.site = ACBBUtils.getSiteFromDataset(version.getDataset()).getName();
            try {
                this.dateMesure = DateUtil.getLocalDateTimeFromLocalDateAndLocaltime(sequence.getDateMesure(), mesure.getHeure());
            } catch (final DateTimeException e) {
                AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
            }
            if (!this.versions.contains(version)) {
                beginDateLocal = version.getDataset().getDateDebutPeriode();
                endDateLocal = version.getDataset().getDateFinPeriode();
                this.beginDateLocal = localStartDate.isAfter(this.beginDateLocal) ? localStartDate : beginDateLocal;
                if (!localEndDate.isBefore(this.endDateLocal)) {
                    this.endDateLocal = this.endDateLocal.plus(1, ChronoUnit.DAYS);
                } else {
                    this.endDateLocal = this.endDateLocal.plus(1, ChronoUnit.MILLIS);
                }
                while (this.beginDateLocal.isBefore(this.endDateLocal)) {
                    this.sortedMesures
                            .computeIfAbsent(site, t -> new TreeMap<>())
                            .put(beginDateLocal, null);
                    this.beginDateLocal = this.beginDateLocal.plus(30, ChronoUnit.MINUTES);
                }
                this.versions.add(version);
            }
            this.sortedMesures
                    .computeIfAbsent(site, t -> new TreeMap<>())
                    .put(dateMesure, mesure);
        }

        /**
         * Adds the mesure line.
         *
         * @param mesure
         * @link(MesureMeteo) the mesure
         * @link(MesureMeteo) the mesure
         */
        void addMesureLine(final MesureMeteo mesure) {
            List<ValeurMeteo> valeursMeteo = mesure.getValeursMeteo();
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            while (itVariablesAffichage.hasNext()) {
                variableAffichage = itVariablesAffichage.next();
                final Iterator<ValeurMeteo> itValeursMeteo = valeursMeteo.iterator();
                ValeurMeteo valeur = null;
                while (itValeursMeteo.hasNext()) {
                    ValeurMeteo next = itValeursMeteo.next();
                    if (variableAffichage.equals(getVariableFromValeur(next).getAffichage())) {
                        valeur = next;
                        break;
                    }
                }
                printValue(Optional.ofNullable(valeur));
            }
        }

        private VariableACBB getVariableFromValeur(ValeurMeteo valeur) {
            return (VariableACBB) ((DatatypeVariableUniteACBB) valeur.getRealNode().getNodeable()).getVariable();
        }

        private void printValue(Optional<ValeurMeteo> valeurMeteo) throws NumberFormatException {
            this.rawDataPrintStream.print(String.format(ACBBMessages.PATTERN_1_FIELD, ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(
                    valeurMeteo
                            .map(value -> value.getValue())
                            .orElse((float) ACBBUtils.CST_INVALID_EMPTY_MEASURE))));
        }

        private void printNAValue() {
            this.rawDataPrintStream.print(String.format(ACBBMessages.PATTERN_1_FIELD,
                    this.getACBBMessage(ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE)));
        }

        /**
         * Adds the na line.
         */
        void addNALine() {
            this.variablesAffichage.forEach((_item) -> {
                printNAValue();
            });
        }

        /**
         * Builds the body.
         *
         * @param mesures
         * @throws DateTimeException
         * @link(List<MesureMeteo>) the mesures {@link MesureMeteo} the mesures
         */
        public void buildBody(final List<MesureMeteo> mesures) throws DateTimeException {
            for (final MesureMeteo mesure : mesures) {
                this.addMesure(mesure);
            }
            LocalDateTime dateTime;
            Iterator<Entry<String, SortedMap<LocalDateTime, MesureMeteo>>> itSite = this.sortedMesures.entrySet().iterator();
            Properties siteNameProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
            while (itSite.hasNext()) {
                Entry<String, SortedMap<LocalDateTime, MesureMeteo>> siteEntry = itSite.next();
                this.site = siteEntry.getKey();
                for (final Entry<LocalDateTime, MesureMeteo> dateEntry : siteEntry.getValue().entrySet()) {
                    dateTime = dateEntry.getKey();
                    String dateTodateString = DateUtil.getUTCDateTextFromLocalDateTime(dateTime, DateUtil.DD_MM_YYYY);
                    String dateToTimeString = DateUtil.getUTCDateTextFromLocalDateTime(dateTime, DateUtil.HH_MM);
                    if (dateTime.getMinute() == 0 && dateTime.getHour() == 0) {
                        dateTime = dateTime.minus(1, ChronoUnit.DAYS);
                        dateTodateString = DateUtil.getUTCDateTextFromLocalDateTime(dateTime, DateUtil.DD_MM_YYYY);
                        dateToTimeString = "24:00";
                    }
                    this.rawDataPrintStream.print(String.format(OutputHelper.PATTERN_CSV_3_FIELDS,
                            siteNameProperties.getProperty(site, site), dateTodateString, dateToTimeString));
                    if (dateEntry.getValue() == null) {
                        this.addNALine();
                    } else {
                        this.addMesureLine(dateEntry.getValue());
                    }
                    this.rawDataPrintStream.println();
                }
                itSite.remove();
            }
        }

        /**
         * Gets the aCBB message.
         *
         * @param key
         * @return the aCBB message {@link String}
         * @link(String) the key
         * @link(String) the key
         */
        String getACBBMessage(final String key) {
            return RecorderACBB
                    .getACBBMessageWithBundle(RecorderACBB.ACBB_DATASET_BUNDLE_NAME, key);
        }
    }

}
