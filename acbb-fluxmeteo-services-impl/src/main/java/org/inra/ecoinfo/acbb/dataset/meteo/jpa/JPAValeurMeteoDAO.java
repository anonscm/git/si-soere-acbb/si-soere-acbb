/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.ValeurMeteo;

/**
 * The Class JPAValeurMeteoDAO.
 */
public class JPAValeurMeteoDAO extends AbstractJPADAO<ValeurMeteo> {

}
