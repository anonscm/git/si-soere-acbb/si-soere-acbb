/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.jpa;


import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationFluxToursDAO.
 */
public class JPAPublicationFluxToursDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        deleteValeurs(version);
        deleteMesures(version);
        deleteSequence(version);
    }

    private void deleteValeurs(final VersionFile version) {
        CriteriaDelete<ValeurFluxTours> deleteValeur = builder.createCriteriaDelete(ValeurFluxTours.class);
        Root<ValeurFluxTours> v = deleteValeur.from(ValeurFluxTours.class);
        Subquery<MesureFluxTours> subqueryMesure = deleteValeur.subquery(MesureFluxTours.class);
        Root<MesureFluxTours> m = subqueryMesure.from(MesureFluxTours.class);
        Subquery<SequenceFluxTours> subquerySequence = subqueryMesure.subquery(SequenceFluxTours.class);
        Root<SequenceFluxTours> s = subquerySequence.from(SequenceFluxTours.class);
        Path<VersionFile> ver = s.get(SequenceFluxTours_.version);
        subquerySequence
                .select(s)
                .where(builder.equal(ver, version));
        Path<SequenceFluxTours> sDb = m.get(MesureFluxTours_.sequenceFluxTours);
        subqueryMesure
                .select(m)
                .where(sDb.in(subquerySequence));
        Path<MesureFluxTours> mDb = v.get(ValeurFluxTours_.mesureFluxTours);
        deleteValeur
                .where(mDb.in(subqueryMesure));
        delete(deleteValeur);
        flush();
    }

    private void deleteMesures(final VersionFile version) {
        CriteriaDelete<MesureFluxTours> deleteMesure = builder.createCriteriaDelete(MesureFluxTours.class);
        Root<MesureFluxTours> m = deleteMesure.from(MesureFluxTours.class);
        Subquery<SequenceFluxTours> subquerySequence = deleteMesure.subquery(SequenceFluxTours.class);
        Root<SequenceFluxTours> s = subquerySequence.from(SequenceFluxTours.class);
        Path<VersionFile> ver = s.get(SequenceFluxTours_.version);
        subquerySequence
                .select(s)
                .where(builder.equal(ver, version));
        Path<SequenceFluxTours> sDb = m.get(MesureFluxTours_.sequenceFluxTours);
        deleteMesure
                .where(sDb.in(subquerySequence));
        delete(deleteMesure);
        flush();
    }

    private void deleteSequence(final VersionFile version) {
        CriteriaDelete<SequenceFluxTours> deleteSequence = builder.createCriteriaDelete(SequenceFluxTours.class);
        Root<SequenceFluxTours> s = deleteSequence.from(SequenceFluxTours.class);
        Path<VersionFile> ver = s.get(SequenceFluxTours_.version);
        deleteSequence
                .where(builder.equal(ver, version));
        delete(deleteSequence);
        flush();
    }

}
