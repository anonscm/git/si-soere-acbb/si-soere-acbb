package org.inra.ecoinfo.acbb.dataset.flux.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * The Class TestDuplicateFlux.
 * <p>
 * implementation for flux of {@link AbstractTestDuplicate}
 * <p>
 * test the existence of duplicates in flux files
 *
 * @author Tcherniatinsky Philippe
 */
public class TestDuplicateFlux extends AbstractTestDuplicate {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.flux.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicateFlux.class
            .getName());
    /**
     * The date time line.
     *
     * @link(SortedMap<String,SortedMap<String,SortedSet<Long>>>).
     */
    final SortedMap<String, SortedSet<Long>> dateTimeLine;
    private final ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO;

    /**
     * Instantiates a new test duplicate flux.
     *
     * @param sequenceFluxToursDAO
     */
    public TestDuplicateFlux(ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO) {
        this.sequenceFluxToursDAO = sequenceFluxToursDAO;
        this.dateTimeLine = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param date
     * @param time
     * @param lineNumber long the line number {@link String} the date string {@link String} the time string
     * @link(String) the date
     * @link(String) the time
     */
    void addLine(final String datetime, final String date, final String time, final long lineNumber) {
        if (this.dateTimeLine.containsKey(datetime)) {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB
                            .getACBBMessageWithBundle(TestDuplicateFlux.BUNDLE_SOURCE_PATH,
                                    ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE), lineNumber, date, time,
                    this.dateTimeLine.get(datetime).first().intValue()));
            this.dateTimeLine.get(datetime).add(lineNumber);
        } else {
            final SortedSet<Long> setLine = new TreeSet();
            setLine.add(lineNumber);
            this.dateTimeLine.put(datetime, setLine);
        }
    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @param lineNumber  long the line number {@link String[]} the values
     * @link(String[]) the values
     */
    @Override
    public void addLine(final String[] values, final long lineNumber, String[] dates,
                        VersionFile versionFile) {
        String date = values[0];
        String time = values[1];
        String localValue = getLocalValue(date, time);
        this.addLine(localValue, date, time, lineNumber);
        if (dates != null) {
            this.testLineForDuplicatesDateInDB(dates, date, time, lineNumber, versionFile);
        }
    }

    /**
     * @param dateDBString
     * @param timeDBString
     * @param lineNumber
     * @param versionFile
     */
    @Override
    protected void testLineForDuplicatesLineinDb(String dateDBString, String timeDBString,
                                                 long lineNumber, VersionFile versionFile) {
        LocalDate date;
        LocalTime time;
        try {
            date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDBString);
            timeDBString = (timeDBString + ":00").substring(0, DateUtil.HH_MM_SS.length());
            time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, timeDBString);
            Optional<SequenceFluxTours> sequenceFluxTours = this.sequenceFluxToursDAO.getSequence(date, versionFile);
            if (sequenceFluxTours.isPresent()) {
                for (MesureFluxTours mesureFluxTours : sequenceFluxTours.get().getMesuresFluxTours()) {
                    if (mesureFluxTours.getHeure().equals(time)) {
                        this.errorsReport.addErrorMessage(String.format(RecorderACBB
                                        .getACBBMessageWithBundle(TestDuplicateFlux.BUNDLE_SOURCE_PATH,
                                                ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE_IN_FILE),
                                lineNumber, dateDBString, timeDBString, mesureFluxTours
                                        .getLigneFichierEchange(), sequenceFluxTours.get().getVersion()
                                        .getDataset().buildDownloadFilename(this.configuration)));
                    }
                }
            }
        } catch (DateTimeException e) {
            AbstractTestDuplicate.LOGGER.info("can't test line", e);
        }
    }
}
