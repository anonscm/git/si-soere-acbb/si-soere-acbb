package org.inra.ecoinfo.acbb.dataset.flux.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesFluxMeteo;
import org.inra.ecoinfo.acbb.dataset.ITestValues;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;

/**
 * The Class TestValuesFlux.
 * <p>
 * implementation of {@link ITestValues}
 *
 * @author Tcherniatinsky Philippe
 * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues
 * @see org.inra.ecoinfo.acbb.dataset.ITestValues
 */
public class TestValuesFlux extends GenericTestValues {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new test values flux.
     */
    public TestValuesFlux() {
        super();
    }

    /**
     * Check date type value.
     *
     * @param values
     * @param badsFormatsReport
     * @param lineNumber            <long> the line number
     * @param index
     * @param value
     * @param column
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @return the date
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#checkDateTypeValue(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, long, int, java.lang.String,
     * org.inra.ecoinfo.dataset.Column, java.util.Map,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB)
     */
    @Override
    protected LocalDate checkDateTypeValue(final String[] values,
                                           final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                           final String value, final Column column,
                                           final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                           final DatasetDescriptorACBB datasetDescriptor,
                                           final IRequestPropertiesACBB requestPropertiesACBB) {
        return null;
    }

    /**
     * Check other type value.
     *
     * @param values
     * @param badsFormatsReport
     * @param lineNumber            <long> the line number
     * @param index
     * @param value
     * @param column
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#checkOtherTypeValue(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, long, int, java.lang.String,
     * org.inra.ecoinfo.dataset.Column, java.util.Map)
     */
    @Override
    protected void checkOtherTypeValue(final String[] values,
                                       final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                       final String value, final Column column,
                                       final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                       final DatasetDescriptorACBB datasetDescriptor,
                                       final IRequestPropertiesACBB requestPropertiesACBB) {
        if (column.isFlag()
                && RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE.equals(column.getFlagType())) {
            Integer qualityClass;
            try {
                qualityClass = Strings.isNullOrEmpty(value) ? null : Integer.parseInt(value);
                if (qualityClass != null && qualityClass != 0 && qualityClass != 1
                        && qualityClass != 2) {
                    badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_QUALITY_CLASS),
                            lineNumber, index + 1)));
                }

            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_QUALITY_CLASS),
                        lineNumber, index + 1)));
            }
        }

    }

    /**
     * Check time type value.
     *
     * @param values
     * @param badsFormatsReport
     * @param lineNumber            <long> the line number
     * @param index
     * @param value
     * @param column
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @return the date
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#checkTimeTypeValue(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, long, int, java.lang.String,
     * org.inra.ecoinfo.dataset.Column, java.util.Map)
     */
    @Override
    protected LocalTime checkTimeTypeValue(final String[] values,
                                           final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                           final String value, final Column column,
                                           final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                           final DatasetDescriptorACBB datasetDescriptor,
                                           final IRequestPropertiesACBB requestPropertiesACBB) {
        final int indexPrec = index - 1;
        String dateString = values[indexPrec];
        final LocalDate date = super.checkDateTypeValue(values, badsFormatsReport, lineNumber, indexPrec,
                dateString, datasetDescriptor.getColumns().get(indexPrec), variablesTypesDonnees,
                datasetDescriptor, requestPropertiesACBB);
        if (!value.matches("^([0-1][0-9]|2[0-3]):[0|3]0:?0{0,2}$|24:00(:00)?")) {
            badsFormatsReport.addException(new NullValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_TIME), value,
                    lineNumber, index + 1, column.getName(), "HH:00[:00] ou HH:30[:00]")));
        }
        final LocalTime time = super.checkTimeTypeValue(null, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        if (time == null) {
            return null;
        } else if (date == null) {
            return time;
        }
        try {
            dateString = ((IRequestPropertiesFluxMeteo) requestPropertiesACBB).dateToStringYYYYMMJJHHMMSS(
                    dateString, value);
            requestPropertiesACBB.addDate(dateString);
        } catch (final BadExpectedValueException | DateTimeException e) {
            badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_INTERVAL_DATE_TIME),
                    values[indexPrec], value, lineNumber)));
        }
        return time;
    }

    /**
     * Test values.
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#testValues(long,
     *                           com.Ostermiller.util.CSVParser,
     *                           org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *                           org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *                           org.inra.ecoinfo.dataset.BadsFormatsReport,
     *                           org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB, java.lang.String)
     * @link(long) the startline
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(String) the datatype name
     */
    @Override
    public void testValues(final long startline, final CSVParser parser,
                           final VersionFile versionFile, final IRequestPropertiesACBB requestProperties,
                           final String encoding, final BadsFormatsReport badsFormatsReport,
                           final DatasetDescriptorACBB datasetDescriptor, final String datatypeName)
            throws BusinessException {
        super.testValues(startline, parser, versionFile, requestProperties, encoding,
                badsFormatsReport, datasetDescriptor, datatypeName);
    }
}
