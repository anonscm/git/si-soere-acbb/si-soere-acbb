package org.inra.ecoinfo.acbb.extraction.meteo.impl;

import org.inra.ecoinfo.acbb.dataset.meteo.IMeteoDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoExtractor;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.acbb.extraction.meteo.IMeteoDAO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class MeteoExtractor.
 */
public class MeteoExtractor extends AbstractExtractor {

    /**
     * The Constant METEO.
     */
    public static final String METEO = "meteo";

    /**
     * The Constant MAP_INDEX_METEO.
     */
    protected static final String MAP_INDEX_METEO = "meteo";

    /**
     * The Constant MAP_INDEX_0.
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     * The meteo dao.
     */
    private IMeteoDAO meteoDAO;

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#extract(org.inra.ecoinfo
     *                           .extraction.IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        if (((FluxMeteoParameterVO) parameters).getResults().get(
                FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE) == null
                || ((FluxMeteoParameterVO) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE).isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(
                    FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE, resultsDatasMap);
        } else {
            ((FluxMeteoParameterVO) parameters)
                    .getResults()
                    .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE)
                    .put(MeteoExtractor.METEO,
                            resultsDatasMap.get(MeteoExtractor.MAP_INDEX_METEO));
        }
        ((FluxMeteoParameterVO) parameters)
                .getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_METEO_CODE)
                .put(MeteoExtractor.MAP_INDEX_0,
                        resultsDatasMap.get(MeteoExtractor.MAP_INDEX_METEO));
    }

    /**
     * Extract datas.
     *
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#extractDatas(java.
     *                           util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final DatesFormParamVO datesForm1ParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<INodeable> selectedSites = ((List<SiteACBB>) requestMetadatasMap
                    .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList())).stream().collect(Collectors.toList());
            final LocalDate dateStart = datesForm1ParamVO.getDateStart();
            final LocalDate dateEnd = datesForm1ParamVO.getDateEnd();
            final List<MesureMeteo> mesuresMeteo = this.meteoDAO.extractMeteo(selectedSites,
                    dateStart, dateEnd, policyManager.getCurrentUser());
            if (mesuresMeteo == null || mesuresMeteo.isEmpty()) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
            extractedDatasMap.put(MeteoExtractor.MAP_INDEX_METEO, mesuresMeteo);
        } catch (final DateTimeException | BusinessException | PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    /**
     * Prepare request metadatas.
     *
     * @param requestMetadatasMap
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     *                           (java.util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
    }

    /**
     * Sets the meteo dao.
     *
     * @param meteoDAO the new meteo dao
     */
    public final void setMeteoDAO(final IMeteoDAO meteoDAO) {
        this.meteoDAO = meteoDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
    }

    /**
     * Sort selected variables.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort(
                (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(
                        IMeteoDatatypeManager.CODE_DATATYPE_METEO), new ArrayList()), new Comparator<Variable>() {

                    @Override
                    public int compare(final Variable o1, final Variable o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });

    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesForm1ParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<INodeable> selectedSites = ((List<SiteACBB>) requestMetadatasMap
                    .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList())).stream().collect(Collectors.toList());
            final LocalDate dateStart = datesForm1ParamVO.getDateStart();
            final LocalDate dateEnd = datesForm1ParamVO.getDateEnd();
            if (selectedSites.isEmpty()) {
                return 0L;
            }
            return meteoDAO.sizeMeteo(selectedSites, dateStart, dateEnd);
        } catch (DateTimeException ex) {
            return -1l;
        }
    }

}
