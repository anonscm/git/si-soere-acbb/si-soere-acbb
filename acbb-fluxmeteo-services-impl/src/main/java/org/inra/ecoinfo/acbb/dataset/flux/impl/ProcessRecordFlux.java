package org.inra.ecoinfo.acbb.dataset.flux.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.flux.IMesureFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.ISequenceFluxToursDAO;
import org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.SequenceFluxTours;
import org.inra.ecoinfo.acbb.dataset.flux.entity.ValeurFluxTours;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.VariableDescriptor;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Transient;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;

/**
 * process the record of flux files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
@SuppressWarnings("rawtypes")
public class ProcessRecordFlux extends AbstractProcessRecord {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesure flux tours dao.
     */
    @Transient
    IMesureFluxToursDAO<MesureFluxTours> mesureFluxToursDAO;
    @Transient
    ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO;

    /**
     * Instantiates a new process record flux.
     */
    public ProcessRecordFlux() {
        super();
    }

    private void buildLines(VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final ErrorsReport errorsReport,
                            final Map<LocalDate, List<FluxLineRecord>> lines,
                            final SortedSet<FluxLineRecord> ligneEnErreur) throws PersistenceException,
            DateTimeException {
        int i = 0;
        Iterator<Entry<LocalDate, List<FluxLineRecord>>> iterator = lines.entrySet().iterator();
        Map<Variable, RealNode> dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
        while (iterator.hasNext()) {
            Entry<LocalDate, List<FluxLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            final SequenceFluxTours sequence = this.buildSequence(versionFile, date, requestProperties);
            for (FluxLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, sequence, ligneEnErreur, errorsReport, requestProperties, dvus);
            }

            this.sequenceFluxToursDAO.saveOrUpdate(sequence);
            if (++i % 50 == 0) {
                datatypeVariableUniteACBBDAO.flush();
                versionFile = versionFileDAO.merge(versionFile);
                dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
            }
            iterator.remove();
        }
    }

    /**
     * Builds the mesure.
     *
     * @param line
     * @param sequenceFluxTours
     * @param ligneEnErreur
     * @param errorsReport
     * @param requestProperties
     * @throws DateTimeException
     * @link(LineRecord) the line
     * @link(SequenceFluxTours) the sequence flux tours
     * @link(SortedSet<LineRecord>) the ligne en erreur
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesACBB) the request properties
     * @link(LineRecord) the line
     * @link(SequenceFluxTours) the sequence flux tours
     * @link(SortedSet<LineRecord>) the ligne en erreur
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesACBB) the request properties {@link FluxLineRecord} the line
     * {@link SequenceFluxTours} the sequence flux tours
     * {@link SortedSet} of {@link FluxLineRecord} the lignes in error
     * {@link ErrorsReport} the errors report
     * {@link IRequestPropertiesACBB} the request properties
     */
    void buildMesure(final FluxLineRecord line, final SequenceFluxTours sequenceFluxTours,
                     final SortedSet<FluxLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
                     final IRequestPropertiesACBB requestProperties, Map<Variable, RealNode> dbRealNodesVariable) {
        final Long originalLineNumber = line.getOriginalLineNumber();
        final MesureFluxTours mesureFluxTours = new MesureFluxTours(sequenceFluxTours, line.getDateTime().toLocalTime(), originalLineNumber);
        mesureFluxTours.setLigneFichierEchange(originalLineNumber);
        for (final VariableValue variableValue : line.getVariablesValues()) {
            final String valueString = variableValue.getValue();
            if (org.apache.commons.lang.StringUtils.isEmpty(valueString)) {
                continue;
            }
            final ValeurFluxTours valeurFluxTours = new ValeurFluxTours();
            DatatypeVariableUniteACBB dvu = variableValue.getDatatypeVariableUnite();
            RealNode realNode = dbRealNodesVariable.get(dvu.getVariable());
            valeurFluxTours.setRealNode(realNode);
            if (variableValue.isQualityClass()) {
                valeurFluxTours.setQualityFlag(variableValue.getQualityClass());
            }
            final Float value = Float.parseFloat(valueString);
            valeurFluxTours.setValue(value);
            valeurFluxTours.setMesureFluxTours(mesureFluxTours);
            mesureFluxTours.getValeursFluxTours().add(valeurFluxTours);
        }
    }

    /**
     * Builds the sequence.
     *
     * @param localVersion
     * @param sequences
     * @param line
     * @param requestPropertiesFluxTours
     * @return the sequence flux tours
     * @throws PersistenceException the persistence exception {@link VersionFile} the version {@link Map} of
     *                              {@link SequenceFluxTours} {@link FluxLineRecord} the line
     *                              {@link IRequestPropertiesACBB} the request properties flux tours
     * @throws DateTimeException
     * @link(VersionFile) the version
     * @link(Map<Date,SequenceFluxTours>) the sequences
     * @link(LineRecord) the line
     * @link(IRequestPropertiesACBB) the request properties flux tours
     * @link(VersionFile) the version
     * @link(Map<Date,SequenceFluxTours>) the sequences
     * @link(LineRecord) the line
     * @link(IRequestPropertiesACBB) the request properties flux tours
     */
    SequenceFluxTours buildSequence(final VersionFile versionFile, final LocalDate date,
                                    final IRequestPropertiesACBB requestPropertiesFluxTours) throws PersistenceException {
        Parcelle parcelle = this.parcelleDAO.merge(requestPropertiesFluxTours.getParcelle());
        SequenceFluxTours sequenceFluxTours = new SequenceFluxTours(versionFile, date, parcelle);
        return sequenceFluxTours;
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful @see
     * java.lang.Object#equals(java.lang.Object)
     * @link(Object) the obj
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        super.processRecord(parser, versionFile, requestProperties, fileEncoding, datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            final Map<LocalDate, List<FluxLineRecord>> lines = new HashMap();
            final Map<String, VariableDescriptor> variablesDescriptor = new HashMap();
            this.updateColumns(datasetDescriptor, variablesDescriptor);
            final long lineCount = datasetDescriptor.getEnTete();
            this.readLines(parser, dbVariables, lines, variablesDescriptor, lineCount, errorsReport);
            final SortedSet<FluxLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, requestProperties, errorsReport, lines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private long readLines(final CSVParser parser, final List<DatatypeVariableUniteACBB> dbVariables,
                           final Map<LocalDate, List<FluxLineRecord>> lines,
                           final Map<String, VariableDescriptor> variablesDescriptor, long lineCount,
                           ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final List<VariableValue> variablesValues = new LinkedList();
            final LocalDateTime datetime;
            String dateString = null;
            String timeString = null;
            int index = 0;
            try {
                dateString = cleanerValues.nextToken();
                index++;
                timeString = cleanerValues.nextToken();
            } catch (EndOfCSVLine ex) {
                errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
            }
            try {
                datetime = RecorderACBB.getDateTimeLocalFromDateStringaAndTimeString(dateString,
                        timeString);
            } catch (BusinessException e) {
                errorsReport.addErrorMessage(e.getMessage(), e);
                continue;
            }
            final Iterator<DatatypeVariableUniteACBB> variableIterator = dbVariables.iterator();
            DatatypeVariableUniteACBB dbVariable;
            VariableValue variableValue = null;
            while (variableIterator.hasNext() && cleanerValues.currentTokenIndex() < values.length) {
                index++;
                dbVariable = variableIterator.next();
                try {
                    variableValue = new VariableValue(dbVariable, cleanerValues.nextToken());
                } catch (EndOfCSVLine ex) {
                    errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
                    continue;
                }
                if (variablesDescriptor.get(dbVariable.getVariable().getAffichage()).hasQualityClass()) {
                    index++;
                    String value = null;
                    try {
                        value = cleanerValues.currentTokenIndex() == values.length ? org.apache.commons.lang.StringUtils.EMPTY
                                : cleanerValues.nextToken();
                    } catch (EndOfCSVLine ex) {
                        errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
                        continue;
                    }
                    variableValue.setQualityClass(Strings.isNullOrEmpty(value) ? null : Integer
                            .parseInt(value));
                }
                variablesValues.add(variableValue);
            }

            final FluxLineRecord line = new FluxLineRecord(datetime, variablesValues,
                    lineCount);
            try {
                lines
                        .computeIfAbsent(datetime.toLocalDate(), k -> new LinkedList())
                        .add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE_TIME), lineCount,
                        dateString, timeString, DateUtil.DD_MM_YYYY, DateUtil.HH_MM));
            }
        }
        return lineCount;
    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            logger.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    /**
     * Sets the mesure flux tours dao.
     *
     * @param mesureFluxToursDAO the new mesure flux tours dao
     *                           {@link IMesureFluxToursDAO} the new mesure flux tours dao
     */
    public final void setMesureFluxToursDAO(
            final IMesureFluxToursDAO<MesureFluxTours> mesureFluxToursDAO) {
        this.mesureFluxToursDAO = mesureFluxToursDAO;
    }

    /**
     * @param sequenceFluxToursDAO
     */
    public void setSequenceFluxToursDAO(
            ISequenceFluxToursDAO<SequenceFluxTours> sequenceFluxToursDAO) {
        this.sequenceFluxToursDAO = sequenceFluxToursDAO;
    }

    private void updateColumns(final DatasetDescriptorACBB datasetDescriptor,
                               final Map<String, VariableDescriptor> variablesDescriptor) {
        for (final Column column : datasetDescriptor.getColumns()) {
            if (null != column.getFlagType()) {
                switch (column.getFlagType()) {
                    case RecorderACBB.PROPERTY_CST_VARIABLE_TYPE:
                    case RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE:
                        variablesDescriptor.put(column.getName(),
                                new VariableDescriptor(column.getName()));
                        break;
                    case RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE:
                        variablesDescriptor.get(column.getRefVariableName()).setHasQualityClass(
                                true);
                        break;
                    default:
                }
            }
        }
    }

}
