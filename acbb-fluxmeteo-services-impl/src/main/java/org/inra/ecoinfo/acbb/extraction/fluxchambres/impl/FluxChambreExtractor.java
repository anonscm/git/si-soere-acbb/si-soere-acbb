/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxchambres.impl;

import org.inra.ecoinfo.acbb.dataset.fluxchambres.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.MesureFluxChambre;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.fluxchambres.IFluxChambreDAO;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoExtractor;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class FluxChambreExtractor.
 */
public class FluxChambreExtractor extends AbstractExtractor {

    /**
     * The Constant FLUX_CHAMBRE.
     */
    public static final String FLUX_CHAMBRE = "flux_chambres";

    /**
     * The Constant MAP_INDEX_FLUXCHAMBRE.
     */
    protected static final String MAP_INDEX_FLUXCHAMBRE = "fluxChambre";

    /**
     * The Constant MAP_INDEX_0.
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     * The flux chambre dao.
     */
    private IFluxChambreDAO fluxChambreDAO;

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#extract(org.inra.ecoinfo
     *                           .extraction.IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        if (((FluxMeteoParameterVO) parameters).getResults().get(
                FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE) == null
                || ((FluxMeteoParameterVO) parameters).getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE).isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(
                    FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE, resultsDatasMap);
        } else {
            ((FluxMeteoParameterVO) parameters)
                    .getResults()
                    .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE)
                    .put(FluxChambreExtractor.FLUX_CHAMBRE,
                            resultsDatasMap.get(FluxChambreExtractor.MAP_INDEX_FLUXCHAMBRE));
        }
        ((FluxMeteoParameterVO) parameters)
                .getResults()
                .get(FluxMeteoExtractor.CST_RESULT_EXTRACTION_CHAMBRE_CODE)
                .put(FluxChambreExtractor.MAP_INDEX_0,
                        resultsDatasMap.get(FluxChambreExtractor.MAP_INDEX_FLUXCHAMBRE));
    }

    /**
     * Extract datas.
     *
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#extractDatas(java.
     *                           util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final DatesFormParamVO datesForm1ParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<INodeable> selectedSites = ((List<SiteACBB>) requestMetadatasMap
                    .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList())).stream().collect(Collectors.toList());
            final LocalDate dateDebut = datesForm1ParamVO.getDateStart();
            final LocalDate dateFin = datesForm1ParamVO.getDateEnd();
            final List<MesureFluxChambre> mesuresFluxChambre = this.fluxChambreDAO
                    .extractFluxChambre(selectedSites, dateDebut, dateFin, policyManager.getCurrentUser());
            if (mesuresFluxChambre == null || mesuresFluxChambre.isEmpty()) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        NoExtractionResultException.ERROR));
            }
            extractedDatasMap.put(FluxChambreExtractor.MAP_INDEX_FLUXCHAMBRE, mesuresFluxChambre);
        } catch (final DateTimeException | BusinessException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    /**
     * Prepare request metadatas.
     *
     * @param requestMetadatasMap
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     *                           (java.util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }

    /**
     * @param datatypeVariableUniteACBBDAO the datatypeVariableUniteACBBDAO to
     *                                     set
     */
    public void setDatatypeVariableUniteACBBDAO(
            IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
    }

    /**
     * Sets the flux chambre dao.
     *
     * @param fluxChambreDAO the new flux chambre dao
     */
    public void setFluxChambreDAO(final IFluxChambreDAO fluxChambreDAO) {
        this.fluxChambreDAO = fluxChambreDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
    }

    /**
     * Sort selected variables.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort(
                (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(
                        IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES), new ArrayList()), new Comparator<Variable>() {

                    @Override
                    public int compare(final Variable o1, final Variable o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });

    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesForm1ParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<INodeable> selectedSites = ((List<SiteACBB>) requestMetadatasMap
                    .getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList())).stream().collect(Collectors.toList());
            final LocalDate dateDebut = datesForm1ParamVO.getDateStart();
            final LocalDate dateFin = datesForm1ParamVO.getDateEnd();
            if (selectedSites.isEmpty()) {
                return 0L;
            }
            return fluxChambreDAO.sizeFluxChambre(selectedSites, dateDebut, dateFin);
        } catch (DateTimeException ex) {
            return -1l;
        }
    }
}
