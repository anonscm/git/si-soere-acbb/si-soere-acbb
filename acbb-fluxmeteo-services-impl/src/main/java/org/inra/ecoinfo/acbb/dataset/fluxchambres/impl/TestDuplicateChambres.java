/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * The Class {@link TestDuplicateChambres}.
 *
 * implementation for chambers of {@link AbstractTestDuplicate}
 *
 * test the existence of duplicates in flux chambers files
 *
 * @author Tcherniatinsky Philippe
 */
public class TestDuplicateChambres extends AbstractTestDuplicate {
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.fluxchambres.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicateChambres.class
            .getName());
    /**
     * The date time line.
     *
     * @link(SortedMap<String,SortedMap<String,SortedSet<Long>>>).
     */
    final SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateChambres() {
        this.dateTimeLine = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param date
     * @link(String) the date
     * @param time
     * @link(String) the time
     * @param lineNumber
     *            long the line number {@link String} the date string {@link String} the time string
     */
    void addLine(final String date, final String time, final long lineNumber) {
        if (!this.dateTimeLine.containsKey(date)) {
            final SortedMap<String, SortedSet<Long>> timeMap = new TreeMap();
            this.dateTimeLine.put(date, timeMap);
        }
        if (this.dateTimeLine.get(date).containsKey(time)) {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateChambres.BUNDLE_SOURCE_PATH,
                    ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE), lineNumber, date, time,
                    this.dateTimeLine.get(date).get(time).first().intValue()));
        } else {
            final SortedSet<Long> setLine = new TreeSet();
            setLine.add(lineNumber);
            this.dateTimeLine.get(date).put(time, setLine);
        }
        this.dateTimeLine.get(date).get(time).add(lineNumber);
    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber
     *            long the line number {@link String[]} the values
     */
    @Override
    public void addLine(final String[] values, final long lineNumber, String[] dates,
                        VersionFile versionFile) {
        this.addLine(values[0], values[1], lineNumber);
    }
}
