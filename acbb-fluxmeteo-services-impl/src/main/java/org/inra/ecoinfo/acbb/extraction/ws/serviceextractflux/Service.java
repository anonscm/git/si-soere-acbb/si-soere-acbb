package org.inra.ecoinfo.acbb.extraction.ws.serviceextractflux;

//~--- non-JDK imports --------------------------------------------------------
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.streamers.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Antoine Schellenberger
 */
public class Service extends AbstractService implements IService {

    public static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
    public static final String LOGGER_ERROR = "Method %s -> %s";
    private IPolicyManager policyManager;
    private IExtractFluxMeteoWSDAO extractFluxMeteoWSDAO;
    private ILocalizationManager localizationManager;
    private INotificationsManager notificationsManager;
    private IVariableACBBDAO variableACBBDAO;

    @GET
    @Produces(MediaType.APPLICATION_JSON)    
    @Path("traitement")
    public Response getTraitementsList(@Context HttpServletResponse response) throws BusinessException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        Utilisateur currentUser;
        try {
            currentUser = (Utilisateur) policyManager.getCurrentUser();
            List<String> groups = currentUser.getAllGroups().stream()
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
            List<TraitementNode> traitementNodes = extractFluxMeteoWSDAO.traitementNodes(localizationManager, groups, Boolean.TRUE);
            String jsonString = new ObjectMapper().writeValueAsString(traitementNodes);
            return Response.ok().entity(jsonString).build();
        } catch (Exception ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()).concat("\n").concat(ex.getMessage()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        }
    }

    @GET
    @Produces("text/csv")
    public Response extractFluxMeteo(
            @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @QueryParam("startDate") String startDate,
            @QueryParam("endDate") String endDate,
            @QueryParam("treatment") String treatments,
            @QueryParam("variables") String variables,
            @DefaultValue("0") @QueryParam("offset") Integer offset,
            @DefaultValue("48") @QueryParam("limit") Integer limit,
            InputStream requestBody) throws BusinessException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        Utilisateur currentUser;
        //List<SynthesisDatatype> synthesisFluxToursDatatypes = extractFluxMeteoWSDAO.getSynthesisFluxToursDatatypes();
        try {
            currentUser = (Utilisateur) policyManager.getCurrentUser();
            List<String> groups = currentUser.getAllGroups().stream()
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
            Optional<List<Optional<Variable>>> dbVariables = Optional.ofNullable(variables).
                    map(vs -> Stream.of(vs.split(","))
                    .map(affichage -> variableACBBDAO.getByAffichage(affichage))
                    .collect(Collectors.toList()));

            String extract = extractFluxMeteoWSDAO.extract(localizationManager, groups, currentUser.getIsRoot(), startDate, endDate, treatments, variables, offset, limit);
            return Response.ok().entity(extract).build();
//            String message = String.format("%s %S (%s)", currentUser.getPrenom(), currentUser.getNom(), currentUser.getLogin());
//            return Response.ok().entity(message).build();
        } catch (Exception ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()).concat("\n").concat(ex.getMessage()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        }

    }

    @GET
    @Produces("text/csv")
    @Path("extract")
    public Response extractAllFluxMeteo(
            @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @QueryParam("startDate") String startDate,
            @QueryParam("endDate") String endDate,
            @QueryParam("treatment") String treatments,
            @QueryParam("variables") String variables,
            InputStream requestBody) throws BusinessException {
        return extractFluxMeteo(request, response, startDate, endDate, treatments, variables, -1,-1, requestBody);
    }

    private Optional<Notification> getLastNotification(Utilisateur user) {
        return notificationsManager.getAllNotifications()
                .stream()
                .filter(n -> n.getUtilisateur().equals(user))
                .findFirst();
    }

    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    public void setExtractFluxMeteoWSDAO(IExtractFluxMeteoWSDAO extractFluxMeteoWSDAO) {
        this.extractFluxMeteoWSDAO = extractFluxMeteoWSDAO;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setVariableACBBDAO(IVariableACBBDAO variableACBBDAO) {
        this.variableACBBDAO = variableACBBDAO;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
