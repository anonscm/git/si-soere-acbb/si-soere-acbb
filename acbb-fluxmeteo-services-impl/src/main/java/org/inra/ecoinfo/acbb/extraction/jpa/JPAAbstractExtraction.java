/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jpa;

import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoExtractionDAO;

/**
 * @param <T>
 * @author tcherniatinskyextends
 */
public abstract class JPAAbstractExtraction<T> extends AbstractACBBExtractionJPA<T> implements IFluxMeteoExtractionDAO<T> {

    private Class synthesisValueClass;
    private boolean isParcelleDatatype;

    private JPAAbstractExtraction() {
    }

    /**
     * @param synthesisValueClass
     * @param isParcelleDatatype
     */
    public JPAAbstractExtraction(Class synthesisValueClass, boolean isParcelleDatatype) {
        this.synthesisValueClass = synthesisValueClass;
        this.isParcelleDatatype = isParcelleDatatype;
    }
}
