/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.modalite;

import org.inra.ecoinfo.IDAO;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IModaliteDAO.
 */
public interface IModaliteDAO extends IDAO<Modalite> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<Modalite> getAll();

    /**
     * Gets the by code.
     *
     * @param modaliteCode the modalite code
     * @return the by code
     */
    Optional<Modalite> getByCode(String modaliteCode);

    /**
     * @param modaliteCodeOrName
     * @return
     */
    Optional<Modalite> getByCodeOrName(String modaliteCodeOrName);

    /**
     * Gets the by n key.
     *
     * @param variableCode the variable code
     * @param modaliteCode the modalite code
     * @return the by n key
     */
    Optional<Modalite> getByNKey(String variableCode, String modaliteCode);

    /**
     * @return
     */
    List<Rotation> getRotations();
}
