/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.utils.Column;

import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Class RequestPropertiesITK.
 */
public abstract class RequestPropertiesITK extends AbstractRequestPropertiesACBB implements
        IRequestPropertiesITK, Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The value columns.
     */
    @Transient
    Map<Integer, Column> valueColumns = new TreeMap();

    /**
     * Instantiates a new request properties itk.
     */
    public RequestPropertiesITK() {
        super();
    }

    /**
     * @param dateDeFin1
     * @param fieldStep
     * @param step
     * @param dateFormatForCompare
     */
    @Override
    public final void buildTree(final LocalDateTime dateDeFin1, final ChronoUnit fieldStep, final int step,
                                String dateFormatForCompare) {
        // nothing
    }

    /**
     * @return
     */
    @Override
    public Map<Integer, Column> getValueColumns() {
        return this.valueColumns;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#setDateDeFin
     * (java.time.LocalDateTime)
     */
    @Override
    public final void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.DAYS, 1);
    }
}
