/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeProductionDAO extends AbstractJPAMethodeDAO<MethodeProduction> implements
        IMethodeProductionDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeProduction> getMethodClass() {
        return MethodeProduction.class;
    }
}
