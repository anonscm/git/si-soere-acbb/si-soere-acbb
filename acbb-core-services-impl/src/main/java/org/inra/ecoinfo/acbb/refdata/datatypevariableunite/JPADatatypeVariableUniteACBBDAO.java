/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.datatypevariableunite;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB_;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.unite.Unite_;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.refdata.variable.Variable_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The Class JPADatatypeVariableUniteACBBDAO.
 */
public class JPADatatypeVariableUniteACBBDAO extends AbstractJPADAO<DatatypeVariableUniteACBB> implements
        IDatatypeVariableUniteACBBDAO {

    IMgaRecorder mgaRecorder;

    /**
     * @param mgaRecorder
     */
    public void setMgaRecorder(IMgaRecorder mgaRecorder) {
        this.mgaRecorder = mgaRecorder;
    }

    /**
     * Gets the all datatype unite variable acbb.
     *
     * @return the all datatype unite variable acbb
     * @see org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO#getAllDatatypeVariableUniteACBB()
     */
    @Override
    public List<DatatypeVariableUniteACBB> getAllDatatypeVariableUniteACBB() {

        CriteriaQuery<DatatypeVariableUniteACBB> query = builder.createQuery(DatatypeVariableUniteACBB.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        query.select(dvu);
        return getResultList(query);
    }

    /**
     * Gets the all variable type donnees by data type map by variable
     * affichage.
     *
     * @param code
     * @return the all variable type donnees by data type map by variable
     * affichage
     * @link(String) the name
     * @link(String) the name
     * @see org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO#getAllVariableTypeDonneesByDataType(java.lang.String)
     */
    @Override
    public Map<String, DatatypeVariableUniteACBB> getAllVariableTypeDonneesByDataTypeMapByVariableAffichage(
            final String code) {
        return getMapForDatatypeCodeByFunctionKey(code, v -> v.getVariable().getAffichage());
    }

    private Map<String, DatatypeVariableUniteACBB> getMapForDatatypeCodeByFunctionKey(final String code, Function<DatatypeVariableUniteACBB, String> mapKey) {

        CriteriaQuery<DatatypeVariableUniteACBB> query = builder.createQuery(DatatypeVariableUniteACBB.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        query.where(
                builder.equal(dvu.join(DatatypeVariableUniteACBB_.datatype).get(Nodeable_.code), code)
        );
        query.select(dvu);
        return getResultList(query).stream()
                .collect(Collectors.toMap(mapKey, v -> v));
    }

    /**
     * Gets the all variable type donnees by data type map by variable code.
     *
     * @param code
     * @return the all variable type donnees by data type map by variable code
     * @link(String) the name
     * @link(String) the name
     * @see org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO#getAllVariableTypeDonneesByDataTypeMapByVariableCode(java.lang.String)
     */
    @Override
    public Map<String, DatatypeVariableUniteACBB> getAllVariableTypeDonneesByDataTypeMapByVariableCode(
            final String code) {
        return getMapForDatatypeCodeByFunctionKey(code, v -> v.getVariable().getCode());
    }

    /**
     * Gets the by datatype and variable.
     *
     * @param datatypeCode
     * @param variable
     * @return the by datatype and variable
     * @link(String) the datatype
     * @link(VariableACBB) the variable
     * @link(String) the datatype
     * @link(VariableACBB) the variable
     * @see org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO#getByDatatypeAndVariable(java.lang.String,
     * org.inra.ecoinfo.acbb.refdata.variable.VariableACBB)
     */
    @Override
    public Optional<DatatypeVariableUniteACBB> getByDatatypeAndVariable(final String datatypeCode,
                                                                        final VariableACBB variable) {

        CriteriaQuery<DatatypeVariableUniteACBB> query = builder.createQuery(DatatypeVariableUniteACBB.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<DatatypeVariableUniteACBB, DataType> dty = dvu.join(DatatypeVariableUniteACBB_.datatype);
        Join<DatatypeVariableUniteACBB, Variable> var = dvu.join(DatatypeVariableUniteACBB_.variable);
        query.where(
                builder.equal(dty.get(Nodeable_.code), datatypeCode),
                builder.equal(var, variable)
        );
        query.select(dvu);
        return getOptional(query);
    }

    /**
     * Gets the name variable for datatype name.
     *
     * @param datatypeName
     * @return the name variable for datatype name
     * @link(String) the datatype name
     * @link(String) the datatype name
     * @see org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO#getNameVariableForDatatypeName(java.lang.String)
     */
    @Override
    public Optional<String> getNameVariableForDatatypeName(final String datatypeName) {

        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        final Join<DatatypeVariableUniteACBB, Variable> variable = dvu.join(DatatypeVariableUniteACBB_.variable);
        query.where(builder.equal(dvu.join(DatatypeVariableUniteACBB_.datatype).get(Nodeable_.code), datatypeName),
                builder.equal(variable.get(Nodeable_.code), datatypeName)
        );
        query.select(variable.get(VariableACBB_.affichage));
        return getOptional(query);
    }

    /**
     * Gets the by datatype.
     *
     * @param datatypeCode the datatype code
     * @return the by datatype
     * @see org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO#getByDatatype(java.lang.String)
     */
    @Override
    public List<DatatypeVariableUniteACBB> getByDatatype(final String datatypeCode) {
        CriteriaQuery<DatatypeVariableUniteACBB> query = builder.createQuery(DatatypeVariableUniteACBB.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        query.where(
                builder.equal(dvu.join(DatatypeVariableUniteACBB_.datatype).get(Nodeable_.code), datatypeCode)
        );
        query.orderBy(builder.asc(dvu.join(DatatypeVariableUniteACBB_.variable).get(Variable_.code)));
        query.select(dvu);
        return getResultList(query);
    }

    /**
     * Gets the by n key.
     *
     * @param datatypeCode the datatype code
     * @param variableCode the variable code
     * @param uniteCode    the unite code
     * @return the by n key
     * @see org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO#getByNKey(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public Optional<DatatypeVariableUniteACBB> getByNKey(final String datatypeCode, final String variableCode, final String uniteCode) {
        CriteriaQuery<DatatypeVariableUniteACBB> query = builder.createQuery(DatatypeVariableUniteACBB.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        final Join<DatatypeVariableUniteACBB, Variable> variable = dvu.join(DatatypeVariableUniteACBB_.variable);
        query.where(builder.equal(dvu.join(DatatypeVariableUniteACBB_.datatype).get(Nodeable_.code), datatypeCode),
                builder.equal(variable.get(Nodeable_.code), variableCode),
                builder.equal(dvu.join(DatatypeVariableUniteACBB_.unite).get(Unite_.code), uniteCode)
        );
        query.orderBy(builder.asc(variable.get(Variable_.code)));
        query.select(dvu);
        return getOptional(query);
    }

    /**
     * Gets the unite.
     *
     * @param datatypeCode
     * @param variableCode
     * @return the unite
     * @see org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO#getUnite(java.lang.String,
     * java.lang.String)
     */
    @Override
    public Optional<Unite> getUnite(final String datatypeCode, final String variableCode) {
        CriteriaQuery<Unite> query = builder.createQuery(Unite.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        final Join<DatatypeVariableUniteACBB, Variable> variable = dvu.join(DatatypeVariableUniteACBB_.variable);
        query.where(builder.equal(dvu.join(DatatypeVariableUniteACBB_.datatype).get(Nodeable_.code), datatypeCode),
                builder.equal(variable.get(Nodeable_.code), variableCode)
        );
        query.orderBy(builder.asc(variable.get(Variable_.code)));
        query.select(dvu.get(DatatypeVariableUniteACBB_.unite));
        return getOptional(query);
    }

    /**
     * @param realNode
     * @return
     */
    @Override
    public RealNode mergeRealNode(RealNode realNode) {
        return entityManager.merge(realNode);
    }

    /**
     * @param realNode
     * @return
     */
    @Override
    public Map<Variable, RealNode> getRealNodesVariables(RealNode realNode) {

        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        query.where(
                builder.equal(rn.join(RealNode_.parent), realNode)
        );
        query.select(rn);
        return getResultAsStream(query)
                .collect(Collectors.toMap(c -> ((DatatypeVariableUniteACBB) c.getNodeable()).getVariable(), c -> c));
    }

    /**
     * @param dvu
     * @throws PersistenceException
     */
    @Override
    public void delete(DatatypeVariableUniteACBB dvu) throws PersistenceException {
        try {
            CriteriaQuery<INode> query = builder.createQuery(INode.class);
            Root<NodeDataSet> node = query.from(NodeDataSet.class);
            query.select(node);
            List<INode> nodes = getResultAsStream(query)
                    .filter(
                            n -> n.getRealNode().getNodeable().getCode().equals(dvu.getCode())
                    )
                    .collect(Collectors.toList());
            CriteriaDelete<NodeDataSet> nodedatasetCriteriaDelete = builder.createCriteriaDelete(NodeDataSet.class);
            Root<NodeDataSet> nds = nodedatasetCriteriaDelete.from(NodeDataSet.class);
            nodedatasetCriteriaDelete.where(nds.in(nodes));
            if (delete(nodedatasetCriteriaDelete) > 0) {
                List<RealNode> realNodes = nodes.stream().map(n -> n.getRealNode()).collect(Collectors.toList());
                CriteriaDelete<RealNode> realnodesCriteriaDelete = builder.createCriteriaDelete(RealNode.class);
                Root<RealNode> rn = realnodesCriteriaDelete.from(RealNode.class);
                realnodesCriteriaDelete.where(rn.in(realNodes));
                if (delete(realnodesCriteriaDelete) > 0) {
                    remove(dvu);
                }
            }
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
