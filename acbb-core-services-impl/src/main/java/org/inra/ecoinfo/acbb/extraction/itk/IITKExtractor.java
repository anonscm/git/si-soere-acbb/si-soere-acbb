package org.inra.ecoinfo.acbb.extraction.itk;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author ptcherniati
 */
public interface IITKExtractor {

    /**
     * @param parameters
     * @throws BusinessException
     */
    void extract(IParameter parameters) throws BusinessException;

    /**
     * @return
     */
    String getExtractCode();

    long getExtractionSize(IParameter parameters);

}
