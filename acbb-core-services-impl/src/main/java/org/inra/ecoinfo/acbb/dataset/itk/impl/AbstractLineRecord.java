package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class AbstractLineRecord.
 *
 * @param <T>
 * @param <L>
 */
public abstract class AbstractLineRecord<T extends AbstractLineRecord, L extends ListeACBB> implements Comparable<T>,
        ILineRecord<T, L> {

    /**
     * The variables values @link(List<VariableValue>).
     */
    protected List<ACBBVariableValue<L>> variablesValues = new LinkedList();
    Parcelle parcelle;
    TraitementProgramme traitementProgramme;
    int version;
    LocalDate dateDebutDetraitementSurParcelle;
    /**
     * The date @link(Date).
     */
    LocalDate date;
    /**
     * The original line number <long>.
     */
    long originalLineNumber;
    /**
     * The observation @link(String).
     */
    String observation = org.apache.commons.lang.StringUtils.EMPTY;
    /**
     * The rotation number @link(int).
     */
    int rotationNumber = 0;
    /**
     * The annee number @link(int).
     */
    int anneeNumber = 0;
    /**
     * The periode number @link(int).
     */
    int periodeNumber = 0;
    /**
     * The tempo @link(Tempo).
     */
    Tempo tempo;
    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    SuiviParcelle suiviParcelle;
    VersionDeTraitement versionDeTraitement;

    /**
     *
     */
    protected AbstractLineRecord() {
        super();
    }

    /**
     * Instantiates a new abstract line record.
     *
     * @param date
     * @param originalLineNumber <long> the original line number
     * @param observation
     * @param rotationNumber     <int> the rotation number
     * @param anneeNumber        <int> the annee number
     * @param periodeNumber      <int> the periode number
     * @param variablesValues
     * @param suiviParcelle
     * @link(Date) the date
     * @link(String) the observation
     * @link(List<VariableValue>) the variables values
     * @link(SuiviParcelle) the suivi parcelle {@link Date} the date {@link String} the observation
     * {@link VariableValue} the variables values {@link SuiviParcelle} the
     * suivi parcelle
     */
    AbstractLineRecord(final LocalDate date, final long originalLineNumber, final String observation,
                       final int rotationNumber, final int anneeNumber, final int periodeNumber,
                       final List<ACBBVariableValue<L>> variablesValues,
                       final SuiviParcelle suiviParcelle) {
        super();
        this.date = date;
        this.originalLineNumber = originalLineNumber;
        this.observation = observation;
        this.rotationNumber = rotationNumber;
        this.anneeNumber = anneeNumber;
        this.periodeNumber = periodeNumber;
        this.variablesValues = variablesValues;
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Instantiates a new abstract line record.
     *
     * @param originalLineNumber long the original line number
     */
    protected AbstractLineRecord(final long originalLineNumber) {
        super();
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * @param originalLineNumber
     * @param suiviParcelle2
     */
    protected AbstractLineRecord(final long originalLineNumber, final SuiviParcelle suiviParcelle2) {
        super();
        this.originalLineNumber = originalLineNumber;
        this.suiviParcelle = suiviParcelle2;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int
     * @link(AbstractLineRecord) the o
     * @link(AbstractLineRecord) the o
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final T o) {
        if (this.originalLineNumber == o.getOriginalLineNumber()) {
            return 0;
        }
        return this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
    }

    /**
     * Copy.
     *
     * @param line
     * @link(AbstractLineRecord) the line {@link AbstractLineRecord} the line
     */
    public void copy(final T line) {
        this.date = line.getDate();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.observation = line.getObservation();
        this.rotationNumber = line.getRotationNumber();
        this.anneeNumber = line.getAnneeNumber();
        this.periodeNumber = line.getPeriodeNumber();
        this.suiviParcelle = line.getSuiviParcelle();
        this.versionDeTraitement = line.getVersionDeTraitement();
        this.variablesValues = line.getVariablesValues();
        this.traitementProgramme = line.getTraitementProgramme();
        this.parcelle = line.getParcelle();
        this.version = line.getVersion();
        this.tempo = line.getTempo();
        this.dateDebutDetraitementSurParcelle = line.getDateDebutDetraitementSurParcelle();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final T other = (T) obj;
        return this.originalLineNumber == other.originalLineNumber;
    }

    /**
     * Gets the annee number.
     *
     * @return the annee number
     */
    @Override
    public int getAnneeNumber() {
        return this.anneeNumber;
    }

    /**
     * Sets the annee number.
     *
     * @param anneeNumber int the new annee number
     */
    public final void setAnneeNumber(final int anneeNumber) {
        this.anneeNumber = anneeNumber;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    @Override
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date @link(Date) {@link Date} the new date
     */
    public final void setDate(final LocalDate date) {
        this.date = date;
    }

    /**
     * @return the dateDebutDetraitementSurParcelle
     */
    public LocalDate getDateDebutDetraitementSurParcelle() {
        return this.dateDebutDetraitementSurParcelle;
    }

    /**
     * @param dateDebutDetraitementSurParcelle the
     *                                         dateDebutDetraitementSurParcelle to set
     */
    public void setDateDebutDetraitementSurParcelle(final LocalDate dateDebutDetraitementSurParcelle) {
        this.dateDebutDetraitementSurParcelle = dateDebutDetraitementSurParcelle;
    }

    /**
     * Gets the observation.
     *
     * @return the observation
     */
    @Override
    public String getObservation() {
        return this.observation;
    }

    /**
     * Sets the observation.
     *
     * @param observation the new observation @link(String) {@link String} the
     *                    new observation
     */
    public final void setObservation(final String observation) {
        this.observation = observation;
    }

    /**
     * Gets the original line number.
     *
     * @return the original line number
     */
    @Override
    public long getOriginalLineNumber() {
        return this.originalLineNumber;
    }

    /**
     * Sets the original line number.
     *
     * @param originalLineNumber long the new original line number
     */
    public final void setOriginalLineNumber(final long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * @return the parcelle
     */
    @Override
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    /**
     * @param parcelle the parcelle to set
     */
    public void setParcelle(final Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * Gets the periode number.
     *
     * @return the periode number
     */
    @Override
    public int getPeriodeNumber() {
        return this.periodeNumber;
    }

    /**
     * Sets the periode number.
     *
     * @param periodeNumber int the new periode number
     */
    public final void setPeriodeNumber(final int periodeNumber) {
        this.periodeNumber = periodeNumber;
    }

    /**
     * Gets the rotation number.
     *
     * @return the rotation number
     */
    @Override
    public int getRotationNumber() {
        return this.rotationNumber;
    }

    /**
     * Sets the rotation number.
     *
     * @param rotationNumber the new rotation number int
     */
    public final void setRotationNumber(final int rotationNumber) {
        this.rotationNumber = rotationNumber;
    }

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    @Override
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle @link(SuiviParcelle)
     *                      {@link SuiviParcelle} the new suivi parcelle
     */
    public void setSuiviParcelle(final SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Gets the tempo @link(Tempo).
     *
     * @return the tempo
     */
    @Override
    public Tempo getTempo() {
        return this.tempo;
    }

    /**
     * Sets the tempo @link(Tempo).
     *
     * @param tempo the tempo to set
     */
    @Override
    public void setTempo(final Tempo tempo) {
        this.tempo = tempo;
    }

    /**
     * @return the traitementProgramme
     */
    public TraitementProgramme getTraitementProgramme() {
        return this.traitementProgramme;
    }

    /**
     * @param traitementProgramme the traitementProgramme to set
     */
    public final void setTraitementProgramme(final TraitementProgramme traitementProgramme) {
        this.traitementProgramme = traitementProgramme;
    }

    /**
     * Gets the variables values.
     *
     * @return the variables values
     */
    @Override
    public List<ACBBVariableValue<L>> getVariablesValues() {
        return this.variablesValues;
    }

    /**
     * Sets the variables values.
     *
     * @param variablesValues the new variables values
     * @link(List<VariableValue>) {@link VariableValue} the new variables values
     */
    public final void setVariablesValues(
            final List<ACBBVariableValue<L>> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * @param version the version to set
     */
    public final void setVersion(final int version) {
        this.version = version;
    }

    /**
     * @return the versionDeTraitement
     */
    public VersionDeTraitement getVersionDeTraitement() {
        return this.versionDeTraitement;
    }

    /**
     * @param versionDeTraitement
     */
    public void setVersionDeTraitement(final VersionDeTraitement versionDeTraitement) {
        this.versionDeTraitement = versionDeTraitement;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.originalLineNumber ^ this.originalLineNumber >>> 32);
        return hash;
    }
}
