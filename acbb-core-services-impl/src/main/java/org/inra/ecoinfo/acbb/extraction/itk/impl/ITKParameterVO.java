/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKParameter;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;
import java.util.Map;

/**
 * The Class ITKParameterVO.
 */
public class ITKParameterVO extends DefaultParameter implements IITKParameter {
    /**
     * The Constant ITK_EXTRACTION_TYPE_CODE @link(String).
     */
    static final String ITK_EXTRACTION_TYPE_CODE = "pratiques_de_gestion";

    /**
     * The commentaires @link(String).
     */
    String commentaires;

    /**
     * The affichage @link(int).
     */
    int affichage;

    /**
     * Instantiates a new iTK parameter vo.
     */
    public ITKParameterVO() {
    }

    /**
     * Instantiates a new iTK parameter vo.
     *
     * @param metadatasMap
     * @link(Map<String,Object>) the metadatas map
     */
    public ITKParameterVO(final Map<String, Object> metadatasMap) {
        this.setParameters(metadatasMap);
        this.setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    /**
     * Gets the affichage @link(int).
     *
     * @return the affichage @link(int)
     */
    @Override
    public int getAffichage() {
        return this.affichage;
    }

    /**
     * Sets the affichage @link(int).
     *
     * @param affichage the new affichage @link(int)
     */
    @Override
    public final void setAffichage(final int affichage) {
        this.affichage = affichage;
    }

    /**
     * Gets the commentaires @link(String).
     *
     * @return the commentaires @link(String)
     */
    @Override
    public String getCommentaire() {
        return this.commentaires;
    }

    /**
     * Sets the commentaires @link(String).
     *
     * @param commentaires the new commentaires @link(String)
     */
    @Override
    public final void setCommentaire(final String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     * @return
     */
    public DatesFormParamVO getDatesYearsContinuousFormParamVO() {
        return (DatesFormParamVO) this.getParameters().get(DatesFormParamVO.class.getSimpleName());
    }

    /**
     * @return @see org.inra.ecoinfo.extraction.IParameter#getExtractionTypeCode()
     */
    @Override
    public String getExtractionTypeCode() {
        return ITKParameterVO.ITK_EXTRACTION_TYPE_CODE;
    }

    /**
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<IntervalDate> getIntervalsDates() {
        return (List<IntervalDate>) this.getParameters().get(IntervalDate.class.getSimpleName());
    }

    /**
     * @return
     */
    public List<TraitementProgramme> getSelectedsTraitements() {
        return (List<TraitementProgramme>) this.getParameters().get(TraitementProgramme.class.getSimpleName());
    }

    /**
     * @param listTraitement
     */
    @Override
    public void setSelectedsTraitements(List<TraitementProgramme> listTraitement) {
        this.getParameters().put(TraitementProgramme.class.getSimpleName(), listTraitement);
    }

    /**
     * @return the selectedTraitementProgrammes
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TraitementProgramme> getSelectedTraitementProgrammes() {
        return (List<TraitementProgramme>) this.getParameters().get(
                TraitementProgramme.class.getSimpleName());
    }

    /**
     * @param intervalDates
     */
    @Override
    public void setIntervalsDate(List<IntervalDate> intervalDates) {
        this.getParameters().put(IntervalDate.class.getSimpleName(), intervalDates);
    }
}
