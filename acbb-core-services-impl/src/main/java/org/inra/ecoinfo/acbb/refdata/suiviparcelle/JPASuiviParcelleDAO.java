/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.suiviparcelle;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.*;

/**
 * The Class JPASuiviParcelleDAO.
 */
public class JPASuiviParcelleDAO extends AbstractJPADAO<SuiviParcelle> implements ISuiviParcelleDAO {

    /**
     * @return
     */
    @Override
    public List<SuiviParcelle> getAll() {
        CriteriaQuery<SuiviParcelle> query = builder.createQuery(SuiviParcelle.class);
        Root<SuiviParcelle> sp = query.from(SuiviParcelle.class);
        query
                .select(sp)
                .orderBy(
                        builder.asc(sp.join(SuiviParcelle_.parcelle).join(Parcelle_.site).get(Nodeable_.code)),
                        builder.asc(builder.function("to_number", Integer.class, sp.join(SuiviParcelle_.parcelle).get(Parcelle_.nom), builder.parameter(String.class, "format")))
                );

        return entityManager.createQuery(query).setParameter("format", "9999").getResultList();
    }

    /**
     * Gets the by n key.
     *
     * @param parcelle
     * @param traitement
     * @param dateDebutTraitement
     * @return the by n key
     * @link(Parcelle) the parcelle
     * @link(TraitementProgramme) the traitement
     * @link(Date) the date debut traitement
     * @link(Parcelle) the parcelle
     * @link(TraitementProgramme) the traitement
     * @link(Date) the date debut traitement
     * @see org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO#getByNKey(org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle,
     * org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme, java.time.LocalDateTime)
     */
    @Override
    public Optional<SuiviParcelle> getByNKey(final Parcelle parcelle, final TraitementProgramme traitement,
                                             final LocalDate dateDebutTraitement) {
        CriteriaQuery<SuiviParcelle> query = builder.createQuery(SuiviParcelle.class);
        Root<SuiviParcelle> sp = query.from(SuiviParcelle.class);
        query.where(
                builder.equal(sp.join(SuiviParcelle_.parcelle), parcelle),
                builder.equal(sp.join(SuiviParcelle_.traitement), traitement)
        );
        query.select(sp);
        return getOptional(query);
    }

    /**
     * @return
     */
    @Override
    public Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> getSortedSuivisParcelles() {
        Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> sortedSuivisParcelles = new HashMap();
        List<SuiviParcelle> suivisParcelles;
        suivisParcelles = this.getAll(SuiviParcelle.class);
        suivisParcelles.stream()
                .forEach(sp -> sortedSuivisParcelles
                        .computeIfAbsent(sp.getParcelle(), k -> new TreeMap<>())
                        .computeIfAbsent(sp.getDateDebutTraitement(), kk -> sp)
                );
        return sortedSuivisParcelles;
    }

    /**
     * @param parcelle
     * @param date
     * @return
     */
    @Override
    public Optional<SuiviParcelle> retrieveSuiviParcelle(Parcelle parcelle, LocalDate date) {
        CriteriaQuery<SuiviParcelle> query = builder.createQuery(SuiviParcelle.class);
        Root<SuiviParcelle> sp = query.from(SuiviParcelle.class);
        query.where(
                builder.equal(sp.join(SuiviParcelle_.parcelle), parcelle),
                builder.lessThanOrEqualTo(sp.get(SuiviParcelle_.dateDebutTraitement), date)
        );
        query.select(sp);
        query.orderBy(builder.desc(sp.get(SuiviParcelle_.dateDebutTraitement)));
        return getOptional(query);
    }
}
