package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseExtractor;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.util.*;

/**
 * @param <T>
 * @author vkoyao
 */
public abstract class AbstractBiomasseExtractor<T extends AbstractBiomasse> extends AbstractExtractor implements
        IBiomasseExtractor {

    private static final String CST_EXTRACT = "EXTRACT_";
    private IBiomasseExtractionDAO<T> biomasseExtractionDAO;
    private String resultCode = org.apache.commons.lang.StringUtils.EMPTY;
    private String extractCode = org.apache.commons.lang.StringUtils.EMPTY;

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
            final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                    .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
            final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
            if (selectedTraitementProgrammes.isEmpty() || selectedVariables.isEmpty()) {
                return 0L;
            }
            return biomasseExtractionDAO.getSize(selectedTraitementProgrammes, selectedVariables, intervalsDates, null);
        } catch (DateTimeException ex) {
            return -1l;
        }

    }

    /**
     * @param parameters
     * @throws BusinessException
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void extract(final IParameter parameters) throws BusinessException {
        this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        if (((BiomasseParameterVO) parameters).getResults().get(this.getResultCode()) == null
                || ((BiomasseParameterVO) parameters).getResults().get(this.getResultCode())
                .isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(this.getResultCode(),
                    resultsDatasMap);
        } else {
            ((BiomasseParameterVO) parameters).getResults().get(this.getResultCode())
                    .put(this.getExtractCode(), resultsDatasMap.get(this.getExtractCode()));
        }
        ((BiomasseParameterVO) parameters)
                .getResults()
                .get(this.getResultCode())
                .put(BiomasseExtractor.MAP_INDEX_0, resultsDatasMap.get(this.getExtractCode()));
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
        final List<T> mesures = this.biomasseExtractionDAO.extractBiomasseValues(
                selectedTraitementProgrammes, selectedVariables, intervalsDates, policyManager.getCurrentUser());
        if (mesures == null || mesures.isEmpty()) {
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(this.getExtractCode(), mesures);
        return extractedDatasMap;
    }

    /**
     * @return
     */
    @Override
    public String getExtractCode() {
        return this.extractCode;
    }

    /**
     * @param extractCode the extractCode to set
     */
    public void setExtractCode(String extractCode) {
        this.extractCode = extractCode;
        this.resultCode = new StringBuffer(AbstractBiomasseExtractor.CST_EXTRACT).append(
                extractCode).toString();
    }

    /**
     * @return
     */
    protected String getResultCode() {
        return this.resultCode;
    }

    /**
     * @param requestMetadatasMap
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     * (java.util.Map)
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }

    /**
     * @param BiomasseExtractionDAO the BiomasseExtractionDAO to set
     */
    public void setBiomasseExtractionDAO(IBiomasseExtractionDAO<T> BiomasseExtractionDAO) {
        this.biomasseExtractionDAO = BiomasseExtractionDAO;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
    }

    /**
     * Sort selected variables.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort(
                (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(
                        this.getExtractCode()), new ArrayList()), new Comparator<Variable>() {

                    @Override
                    public int compare(final Variable o1, final Variable o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });
    }
}
