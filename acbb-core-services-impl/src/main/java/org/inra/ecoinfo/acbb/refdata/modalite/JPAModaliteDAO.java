/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.modalite;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.variablededescription.VariableDescription_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAModaliteDAO.
 */
public class JPAModaliteDAO extends AbstractJPADAO<Modalite> implements IModaliteDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO#getAll()
     */
    @Override
    public List<Modalite> getAll() {
        return getAll(Modalite.class);
    }

    /**
     * Gets the by code.
     *
     * @param modaliteCode
     * @return the by code @see
     * org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO#getByCode(java.lang
     * .String)
     * @link(String) the modalite code
     */
    @Override
    public Optional<Modalite> getByCode(final String modaliteCode) {
        CriteriaQuery<Modalite> query = builder.createQuery(Modalite.class);
        Root<Modalite> mod = query.from(Modalite.class);
        query.where(
                builder.equal(mod.get(Modalite_.code), modaliteCode)
        );
        query.select(mod);
        return getOptional(query);
    }

    /**
     * @param modaliteCodeOrName
     * @return
     */
    @Override
    public Optional<Modalite> getByCodeOrName(final String modaliteCodeOrName) {
        CriteriaQuery<Modalite> query = builder.createQuery(Modalite.class);
        Root<Modalite> mod = query.from(Modalite.class);
        query.where(
                builder.or(
                        builder.and(
                                builder.like(mod.get(Modalite_.code), "U%"),
                                builder.equal(builder.lower(mod.get(Modalite_.code)), modaliteCodeOrName.toLowerCase())
                        ),
                        builder.and(
                                builder.equal(builder.lower(mod.get(Modalite_.nom)), modaliteCodeOrName.toLowerCase())
                        )
                )
        );
        query.select(mod);
        return getOptional(query);
    }

    /**
     * Gets the by n key.
     *
     * @param variableCode
     * @param code
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO#getByNKey(java.lang
     * .String, java.lang.String)
     * @link(String) the variable code
     * @link(String) the code
     */
    @Override
    public Optional<Modalite> getByNKey(final String variableCode, final String code) {
        CriteriaQuery<Modalite> query = builder.createQuery(Modalite.class);
        Root<Modalite> mod = query.from(Modalite.class);
        query.where(
                builder.equal(mod.join(Modalite_.variableDescription).get(VariableDescription_.code), variableCode),
                builder.equal(mod.get(Modalite_.code), code)
        );
        query.select(mod);
        return getOptional(query);
    }

    /**
     * @return
     */
    @Override
    public List<Rotation> getRotations() {
        CriteriaQuery<Rotation> query = builder.createQuery(Rotation.class);
        Root<Rotation> rot = query.from(Rotation.class);
        query.select(rot);
        return getResultList(query);
    }
}
