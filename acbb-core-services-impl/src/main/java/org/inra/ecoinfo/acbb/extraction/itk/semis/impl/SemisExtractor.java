package org.inra.ecoinfo.acbb.extraction.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKExtractor;

/**
 * @author ptcherniati
 */
public class SemisExtractor extends AbstractITKExtractor<MesureSemis> {

}
