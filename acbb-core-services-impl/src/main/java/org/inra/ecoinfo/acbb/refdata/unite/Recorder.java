/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.unite;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends org.inra.ecoinfo.refdata.unite.Recorder {


    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * @return
     */
    @Override
    protected List<Unite> getAllElements() {
        List<Unite> all = super.getAllElements();
        Collections.sort(all, new ComparatorUnite());
        return all;
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        Unite.NAME_ENTITY_JPA);
                lineNumber++;
                // On parcourt chaque colonne d'une ligne
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final Unite unite = new Unite(code, nom);
                final Unite dbUnite = this.uniteDAO.getByCode(code)
                        .orElse(null);
                // Enregistre une unité uniquement si elle n'existe pas en BD ou
                // bien si elle est considérée comme une mise à jour
                if (dbUnite == null) {
                    this.uniteDAO.saveOrUpdate(unite);
                } else {
                    dbUnite.setName(nom);
                }
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private final static class ComparatorUnite implements Comparator<Unite> {

        @Override
        public int compare(Unite o1, Unite o2) {
            if (o2 == null) {
                return 1;
            }
            return o1.getCode().compareTo(o2.getCode());
        }
    }

}
