/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.agroecosysteme;

import org.inra.ecoinfo.IDAO;

import java.util.Optional;

/**
 * The Interface IAgroEcosystemeDAO.
 */
public interface IAgroEcosystemeDAO extends IDAO<AgroEcosysteme> {

    /**
     * Gets the by code.
     *
     * @param codeAgroEcosysteme the code agro ecosysteme
     * @return the by code
     */
    Optional<AgroEcosysteme> getByCode(String codeAgroEcosysteme);

}
