package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.IFertilisantDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertType;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildTravailDusolHelper.
 */
public class BuildFertilisantHelper extends AbstractBuildHelper<LineRecord> {

    /**
     * The travaux du sol.
     */
    Map<Parcelle, Map<LocalDate, Map<Integer, Map<FertType, Fertilisant>>>> fertilisants = new HashMap();

    IFertilisantDAO fertilisantDAO;

    /**
     * Builds the.
     *
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() {
        for (final LineRecord line : this.lines) {
            final Fertilisant fertilisant = this.getOrCreateFertilisant(line);
            if (fertilisant == null) {
                break;
            }
            try {
                for (ACBBVariableValue<? extends IValeurQualitative> variableValue : line
                        .getVariablesValues()) {

                    RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
                    ValeurFertilisant valeur;
                    if (variableValue.isValeurQualitative()) {
                        valeur = new ValeurFertilisant(variableValue.getValeurQualitative(),
                                realNode, fertilisant);
                    } else {
                        valeur = new ValeurFertilisant(Float.parseFloat(variableValue.getValue()),
                                realNode, fertilisant);
                    }
                    fertilisant.getValeursFertilisant().add(valeur);
                }
                this.fertilisantDAO.saveOrUpdate(fertilisant);
            } catch (final PersistenceException e) {
                LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("mesureTravailDuSolDAO.saveOrUpdate", e);
            }
        }
    }

    /**
     * Instantiates a new builds the fertilisant helper.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines {@link AbstractLineRecord}
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(IRequestPropertiesITK) the request properties itk
     */
    public void build(final VersionFile version, final List<LineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK) {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    Fertilisant getOrCreateFertilisant(final LineRecord line) {
        return fertilisants
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getRang(), k -> new HashMap<>())
                .computeIfAbsent(line.getFertType(), k -> createNewFertilisant(line));
    }

    /**
     * @param fertilisantDAO
     */
    public void setFertilisantDAO(IFertilisantDAO fertilisantDAO) {
        this.fertilisantDAO = fertilisantDAO;
    }

    private Fertilisant createNewFertilisant(LineRecord line) {
        Fertilisant fertilisant = new Fertilisant(this.version, line.getObservation(), line.getDate(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber());
        fertilisant.setSuiviParcelle(line.getSuiviParcelle());
        fertilisant.setRang(line.getRang());
        fertilisant.setFertType(line.getFertType());
        return fertilisant;
    }
}
