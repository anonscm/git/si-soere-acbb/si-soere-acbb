/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestHeaders;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 *
 *
 *
 * generic implementation of {@link ITestHeaders}.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * @author Tcherniatinsky Philippe
 */
public class GenericTestHeader implements ITestHeaders {

    /**
     *
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     *
     * The LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericTestHeader.class);
    /**
     *
     * The suivi parcelle dao @link(ISuiviParcelleDAO).
     */
    @Transient
    protected ISuiviParcelleDAO suiviParcelleDAO;
    /**
     *
     * The version de traitement dao @link(IVersionDeTraitementDAO).
     */
    @Transient
    protected IVersionDeTraitementDAO versionDeTraitementDAO;

    /**
     *
     * The datatype unite variable acbbdao.
     */
    @Transient
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;

    /**
     *
     * The traitement dao @link(ITraitementDAO).
     */
    @Transient
    ITraitementDAO traitementDAO;

    /**
     *
     * The parcelle dao @link(IParcelleDAO).
     */
    @Transient
    IParcelleDAO parcelleDAO;

    /**
     *
     * The site dao @link(ISiteACBBDAO).
     */
    @Transient
    ISiteACBBDAO siteDAO;

    /**
     *
     * The localization manager @link(ILocalizationManager).
     */
    @Transient
    ILocalizationManager localizationManager;

    @Transient
    IDatasetConfiguration datasetConfiguration;

    /**
     *
     * Instantiates a new generic test header.
     */
    public GenericTestHeader() {

        super();

    }

    private String buildDownloadFilename(VersionFile version) {

        return version.getDataset().buildDownloadFilename(this.datasetConfiguration);

    }

    /**
     *
     *
     *
     * test if a line is empty.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param values
     *
     * @link(String[]) the values
     *
     *
     *
     * @return true if the line is empty {@link String[]}
     *
     *
     *
     *         the values of the line
     *
     * @link(String[]) the values
     */
    public Boolean estLigneVide(String[] values) {

        for (final String string : values) {

            if (!string.equals(org.apache.commons.lang.StringUtils.EMPTY)) {

                return false;

            }

        }

        return true;

    }

    /**
     *
     * @return the LOGGER
     */
    protected Logger getLogger() {

        return LOGGER;

    }

    /**
     *
     *
     *
     * find the plot in database.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param site
     *
     * @link(SiteACBB) the site
     *
     * @param parcelleCode
     *
     * @link(String) the parcelle code
     *
     *
     *
     * @return {@link Parcelle} the parcelle
     *
     * @link(SiteACBB) the site
     *
     * @link(String) the parcelle code
     */
    protected Optional<Parcelle> getParcelle(final SiteACBB site, final String parcelleCode) {
        if (site == null || parcelleCode == null) {
            return Optional.empty();
        }
        final String codeParcelle = Utils.createCodeFromString(parcelleCode);
        return this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(codeParcelle, site));

    }

    /**
     * get the site in database.
     *
     * @param siteName
     *
     * @link(String) the site name
     *
     *
     *
     * @return {@link SiteACBB} the site
     *
     * @link(String) the site name
     */
    protected Optional<Site> getSite(final String siteName) {
        return Optional.ofNullable(siteName)
                .map(s -> Utils.createCodeFromString(s))
                .map(code -> this.siteDAO.getByPath(code))
                .orElseGet(Optional::empty);
    }

    /**
     *
     *
     *
     * find an experiment treatment from database.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param site
     *
     * @link(SiteACBB) the site
     *
     * @param traitementCode
     *
     * @link(String) the traitement code
     *
     *
     *
     * @return {@link TraitementProgramme} the traitement
     *
     * @link(SiteACBB) the site
     *
     * @link(String) the traitement code
     *
     * @link(Date) the date debut traitment
     */
    protected Optional<TraitementProgramme> getTraitement(final SiteACBB site, final String traitementCode) {
        return Optional.ofNullable(site)
                .map(s -> s.getId())
                .map(id -> this.traitementDAO.getByNKey(site.getId(), Utils.createCodeFromString(traitementCode)))
                .orElseGet(Optional::empty);
    }

    /**
     *
     *
     *
     * jump lines from parser.
     *
     *
     *
     * Be carefull, if line are empty they're jumped too!
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param parser
     *
     * the parser
     * @param lineNumber
     *
     * @param numberOfJumpedLines
     *
     * int
     *
     *
     *
     * number of line to jump
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     * Signals that an I/O exception has occurred.
     */
    protected long jumpLines(final CSVParser parser, final long lineNumber,
                             final int numberOfJumpedLines) throws IOException {

        long finalLineNumber = lineNumber;

        for (int j = 0; j < numberOfJumpedLines; j++) {
            int line = Math.max(0, parser.getLastLineNumber());
            parser.getLine();
            finalLineNumber += parser.getLastLineNumber() - line;
        }
        return finalLineNumber;

    }

    /**
     *
     * Read begin and end dates.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param version
     *
     * @link(VersionFile) the version
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     * @link(CSVParser) the parser
     *
     * @param lineNumber
     *
     * @link(long) the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred.
     */
    protected long readBeginAndEndDates(final VersionFile version,
                                        final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                                        final long lineNumber, final IRequestPropertiesACBB requestProperties)
            throws IOException {

        long returnLineNumber = this.readBeginDate(version, badsFormatsReport, parser, lineNumber,
                requestProperties);
        returnLineNumber = this.readEndDate(version, badsFormatsReport, parser, returnLineNumber,
                requestProperties);
        this.testIntervalDate(badsFormatsReport, returnLineNumber, requestProperties);
        if (requestProperties.getDateDeDebut() != null && requestProperties.getDateDeFin() != null) {
            this.testAreConsistentDates(badsFormatsReport, version, requestProperties, returnLineNumber);
        }
        return returnLineNumber;
    }

    /**
     *
     * reads lines header beginDate on line lineNumber.
     *
     * @param versionFile
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link IRequestPropertiesACBB} the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readBeginDate(final VersionFile versionFile,
                                 final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                                 final long lineNumber, final IRequestPropertiesACBB requestProperties)
            throws IOException {

        long returnLineNumber = lineNumber;

        try {

            String[] line = parser.getLine();

            final String date = line[1];

            String time = line[2];

            if (Strings.isNullOrEmpty(time)) {

                time = "00:00:00";

            }

            returnLineNumber++;

            if (!date.matches(RecorderACBB.PATTERN_DATE)
                    || !time.matches(RecorderACBB.PATTERN_TIME_SECONDE)) {

                if (!date.matches(RecorderACBB.PATTERN_DATE)) {

                    badsFormatsReport.addException(new BadExpectedValueException(String.format(
                            RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_BEGIN_DATE),
                            date, returnLineNumber, 1, RecorderACBB.PROPERTY_CST_FRENCH_DATE)));

                    requestProperties.setDateDeDebut(null);

                }

                if (!time.matches(RecorderACBB.PATTERN_TIME_SECONDE)) {

                    badsFormatsReport.addException(new BadExpectedValueException(String.format(
                            RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_BEGIN_TIME),
                            time, returnLineNumber, 2, RecorderACBB.PROPERTY_CST_FRENCH_TIME)));

                    requestProperties.setDateDeDebut(null);

                }

            } else {

                try {

                    requestProperties.setDateDeDebut(RecorderACBB.getDateTimeLocalFromDateStringaAndTimeString(date, time));

                } catch (final BusinessException e) {

                    badsFormatsReport.addException(new BadExpectedValueException(e.getMessage()));

                    requestProperties.setDateDeDebut(null);

                }

            }

        } catch (final ArrayIndexOutOfBoundsException e) {

            returnLineNumber++;

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_BEGIN_DATE),
                    returnLineNumber, 1)));

        }

        return returnLineNumber;

    }

    /**
     *
     *
     *
     * read a comment on line lineNumber.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param parser
     *
     * the parser
     * @param lineNumber
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link IRequestPropertiesACBB} the
     *             request properties
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readCommentaire(final CSVParser parser, final long lineNumber,
                                   final IRequestPropertiesACBB requestProperties) throws IOException {

        long finalLineNumber = lineNumber;

        String[] values;

        values = parser.getLine();

        finalLineNumber++;

        try {

            requestProperties.setCommentaire(values[1]);

        } catch (final Exception e) {

            requestProperties.setCommentaire(org.apache.commons.lang.StringUtils.EMPTY);

        }

        return finalLineNumber;

    }

    /**
     *
     *
     *
     * read a the datatype on line lineNumber.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     * @param lineNumber
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param datatypeName
     *
     * @link(String) the datatype name
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link String} the datatype name
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(String) the datatype name
     */
    protected long readDatatype(final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                                final long lineNumber, final String datatypeName) throws IOException {

        long finalLineNumber = lineNumber;

        String[] values;

        values = parser.getLine();

        finalLineNumber++;

        String dataType = null;

        try {

            if (values != null && values.length > 1) {

                dataType = Utils.createCodeFromString(values[1]);

            } else {

                throw new IndexOutOfBoundsException();

            }

            if (dataType == null || !dataType.equalsIgnoreCase(datatypeName)) {

                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATATYPE),
                        finalLineNumber, 2, datatypeName)));

            }

        } catch (final IndexOutOfBoundsException e1) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_DATATYPE),
                    finalLineNumber, 2, datatypeName)));

        }

        return finalLineNumber;

    }

    /**
     *
     *
     *
     * read an empty line.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     * @param lineNumber
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report
     *
     * @link(BadsFormatsReport) the bads formats report
     */
    protected long readEmptyLine(final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                                 final long lineNumber) throws IOException {

        long finalLineNumber = lineNumber;
        String[] values;
        values = parser.getLine();
        finalLineNumber++;
        if (!this.estLigneVide(values)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_EMPTY_LINE),
                    finalLineNumber)));
        }
        return finalLineNumber;

    }

    /**
     *
     *
     *
     * reads lines header endDate on line lineNumber.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param versionFile
     * @param badsFormatsReport
     * @param lineNumber
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link IRequestPropertiesACBB} the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readEndDate(final VersionFile versionFile,
                               final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                               final long lineNumber, final IRequestPropertiesACBB requestProperties)
            throws IOException {

        long finalLineNumber = lineNumber;

        try {

            String[] line = parser.getLine();

            final String date = line[1];

            String time = line[2];

            if (Strings.isNullOrEmpty(time)) {

                time = "00:00:00";

            }

            finalLineNumber++;

            if (!date.matches(RecorderACBB.PATTERN_DATE)
                    || !time.matches(RecorderACBB.PATTERN_TIME_SECONDE)) {

                if (!date.matches(RecorderACBB.PATTERN_DATE)) {

                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderACBB
                                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_END_DATE),
                                            date, finalLineNumber, 1,
                                            RecorderACBB.PROPERTY_CST_FRENCH_DATE)));

                    requestProperties.setDateDeFin(null);

                }

                if (!time.matches(RecorderACBB.PATTERN_TIME_SECONDE)) {

                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderACBB
                                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_END_TIME),
                                            time, finalLineNumber, 1,
                                            RecorderACBB.PROPERTY_CST_FRENCH_TIME)));

                    requestProperties.setDateDeFin(null);

                }

            } else {

                try {

                    requestProperties.setDateDeFin(RecorderACBB.getDateTimeLocalFromDateStringaAndTimeString(date, time));

                } catch (final BusinessException e) {

                    badsFormatsReport.addException(new BadExpectedValueException(e.getMessage()));

                    requestProperties.setDateDeFin(null);

                }

            }

        } catch (final ArrayIndexOutOfBoundsException e) {

            finalLineNumber++;

            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_END_DATE),
                    finalLineNumber, 1)));

        }

        return finalLineNumber;

    }

    /**
     *
     *
     *
     * read the columnNames line.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     * @param lineNumber
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param datasetDescriptor
     *
     * @link(DatasetDescriptor) the dataset descriptor
     *
     * @param requestPropertiesACBB
     *
     * @link(IRequestPropertiesACBB) the request properties acbb
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link DatasetDescriptorACBB} the dataset descriptor
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(DatasetDescriptor) the dataset descriptor
     */
    protected long readLineHeader(final BadsFormatsReport badsFormatsReport,
                                  final CSVParser parser, final long lineNumber,
                                  final DatasetDescriptor datasetDescriptor,
                                  final IRequestPropertiesACBB requestPropertiesACBB) throws IOException {

        final long finalLineNumber = lineNumber + 1;

        String[] values;

        int index;

        values = parser.getLine();

        String value;

        for (index = 0; index < values.length; index++) {

            if (index > datasetDescriptor.getColumns().size() - 1) {

                break;

            }

            value = values[index].trim();

            final Column column = datasetDescriptor.getColumns().get(index);

            final String columnName = column.getName();

            if (!columnName.trim().equalsIgnoreCase(value)) {

                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_COLUMN_HEADER),
                        finalLineNumber, index + 1, value, columnName.trim().toLowerCase())));

            }

        }

        return finalLineNumber;

    }

    /**
     *
     * Read site.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param finalLineNumber
     *
     *            <long> the line number
     *
     * @param values
     *
     * @link(String[]) the values
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws BusinessException
     *
     *             the business exception
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(String[]) the values
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    long readSite(final BadsFormatsReport badsFormatsReport, final long lineNumber,
                  final String[] values, final IRequestPropertiesACBB requestProperties)
            throws BusinessException {
        long finalLineNumber = lineNumber;
        if (values == null) {
            throw new BusinessException(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_EMPTY_FILE));
        }
        finalLineNumber++;
        String siteName = "";
        try {
            siteName = values[1];
            requestProperties.setSite((SiteACBB) this.getSite(siteName).orElseThrow(() -> new PersistenceException("bad site")));
        } catch (final PersistenceException | ArrayIndexOutOfBoundsException e) {
            if (e instanceof ArrayIndexOutOfBoundsException) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_SITE), finalLineNumber, 2)));

            } else if ("bad site".equals(e.getMessage()) || e instanceof ArrayIndexOutOfBoundsException) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_SITE),
                        siteName, finalLineNumber, 2)));

            } else {

                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_SITE),
                        finalLineNumber, 2)));
            }

        }
        return finalLineNumber;
    }

    /**
     *
     *
     *
     * read a site on line lineNumber.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param version
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred.
     *
     * @throws BusinessException
     *
     *             the business exception {@link BadFormatException} the bads formats report
     *             {@link IRequestPropertiesACBB} the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readSite(final VersionFile version, final BadsFormatsReport badsFormatsReport,
                            final CSVParser parser, final long lineNumber,
                            final IRequestPropertiesACBB requestProperties) throws IOException, BusinessException {

        final String[] values = parser.getLine();
        long line = this.readSite(badsFormatsReport, lineNumber, values, requestProperties);
        this.testIsConsistentSite(version, badsFormatsReport, lineNumber, requestProperties);
        return line;

    }

    /**
     *
     *
     *
     * read on line lineNumber a site in column 2 and a parcelle on column 3.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param version
     *
     *
     *
     * @param badsFormatsReport
     * @param lineNumber
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred.
     *
     * @throws BusinessException
     *
     *             the business exception {@link BadsFormatsReport} the bads formats report
     *             {@link IRequestPropertiesACBB} the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readSiteAndParcelle(VersionFile version,
                                       final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                                       final long lineNumber, final IRequestPropertiesACBB requestProperties)
            throws IOException, BusinessException {

        long finalLineNumber = lineNumber;
        final String[] values = parser.getLine();
        final long returnLineNumber = this.readSite(badsFormatsReport, finalLineNumber, values,
                requestProperties);
        if (requestProperties.getSite() == null) {

            finalLineNumber++;

            return finalLineNumber;

        }
        try {
            final String parcelleCode = values[2];
            requestProperties.setParcelle(this.getParcelle(requestProperties.getSite(),
                    parcelleCode).orElseThrow(() -> new PersistenceException("bad parcelle")));
            if (requestProperties.getParcelle() == null) {

                final String message = String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_PLOT),
                        parcelleCode, returnLineNumber, 3, requestProperties.getSite().getName());

                final BadExpectedValueException badFormatException = new BadExpectedValueException(
                        message);

                badsFormatsReport.addException(badFormatException);

            }
        } catch (final PersistenceException | ArrayIndexOutOfBoundsException e) {

            final String message = String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_PLOT),
                    returnLineNumber, 3);

            final BadExpectedValueException badFormatException = new BadExpectedValueException(
                    message);

            badsFormatsReport.addException(badFormatException);

        }
        if (requestProperties.getSite() != null && requestProperties.getTraitement() != null) {
            try {
                final LocalDate dateDebutTraitementProgramme = requestProperties.getTraitement()
                        .getDateDebutTraitement();
                requestProperties.setSuiviParcelle(this.suiviParcelleDAO.getByNKey(
                        requestProperties.getParcelle(), requestProperties.getTraitement(),
                        dateDebutTraitementProgramme).orElseThrow(() -> new PersistenceException("bad suivi parcelle")));
            } catch (PersistenceException e) {

                badsFormatsReport
                        .addException(new BadExpectedValueException(
                                String.format(
                                        RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER),
                                        returnLineNumber, requestProperties.getParcelle().getName(),
                                        requestProperties.getTraitement().getAffichage())));

            }
        }
        this.testIsConsistentSite(version, badsFormatsReport, returnLineNumber, requestProperties);
        this.testIsConsistentParcelle(version, badsFormatsReport, returnLineNumber,
                requestProperties);
        return returnLineNumber;

    }

    /**
     *
     *
     *
     * read a traitment on line lineNumber.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestPropertiesACBB
     *
     * @link(IRequestPropertiesACBB) the request properties acbb
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link IRequestPropertiesACBB} the request properties acbb
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties acbb
     */
    protected long readTraitement(final BadsFormatsReport badsFormatsReport,
                                  final CSVParser parser, final long lineNumber,
                                  final IRequestPropertiesACBB requestPropertiesACBB) throws IOException {

        String[] values;
        values = parser.getLine();
        return this.readTraitement(badsFormatsReport, lineNumber, values, requestPropertiesACBB);

    }

    /**
     *
     * Read traitement.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param finalLineNumber
     *
     *            <long> the line number
     *
     * @param values
     *
     * @link(String[]) the values
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(String[]) the values
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    long readTraitement(final BadsFormatsReport badsFormatsReport, final long lineNumber,
                        final String[] values, final IRequestPropertiesACBB requestProperties) {
        long finalLineNumber = lineNumber;
        finalLineNumber++;
        if (requestProperties.getSite() == null) {
            return finalLineNumber;
        }
        String traitementCode = null;
        try {
            traitementCode = values[1];
        } catch (final ArrayIndexOutOfBoundsException e) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_TREATMENT),
                    finalLineNumber, 2)));
            return finalLineNumber;
        }
        String dateDebutTraitementString = org.apache.commons.lang.StringUtils.EMPTY;
        try {
            LocalDate dateDebutTraitement;
            dateDebutTraitementString = values[2];
            if (Strings.isNullOrEmpty(dateDebutTraitementString)) {
                throw new IndexOutOfBoundsException();
            }
            dateDebutTraitement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDebutTraitementString);
            requestProperties.setDateDebutTraitement(dateDebutTraitement);
            requestProperties.setTraitement(this.getTraitement(requestProperties.getSite(),
                    traitementCode).orElseThrow(() -> new PersistenceException("bad treatment")));
        } catch (final PersistenceException | IndexOutOfBoundsException e) {
            if (e instanceof IndexOutOfBoundsException) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_BEGIN_TREATMENT_DATE), finalLineNumber, 2, requestProperties.getSite().getName())));

            } else if ("bad treatment".equals(e.getMessage())) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_TRAITEMENT),
                        traitementCode, finalLineNumber, 2, requestProperties.getSite().getName())));

            } else {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_BEGIN_TREATMENT_DATE),
                        finalLineNumber, 3)));
            }

        } catch (final DateTimeException e) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_BEGIN_TREATMENT_DATE),
                    dateDebutTraitementString, finalLineNumber, 3, DateUtil.DD_MM_YYYY)));

        }
        return finalLineNumber;
    }

    /**
     *
     *
     *
     * read a traitement on line lineNumber and find suiviParcelle (you must
     * read site and parcelle before).
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link IRequestPropertiesACBB} the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readTraitementAndSuiviParcelle(final BadsFormatsReport badsFormatsReport,
                                                  final CSVParser parser, final long lineNumber,
                                                  final IRequestPropertiesACBB requestProperties) throws IOException {

        final long returnLineNumber = this.readTraitement(badsFormatsReport, parser, lineNumber,
                requestProperties);
        this.updateSuiviParcelle(badsFormatsReport, returnLineNumber, requestProperties);
        return returnLineNumber;

    }

    /**
     *
     * <read traitement on line lineNumber column 2 and the version of the
     * treatment on column 3.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param parser
     *
     *            the parser
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws IOException
     *
     *             Signals that an I/O exception has occurred. {@link BadsFormatsReport} the bads
     *             formats report {@link IRequestPropertiesACBB} the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties
     */
    protected long readTraitementVersionAndDateDebutTraitement(
            final BadsFormatsReport badsFormatsReport, final CSVParser parser,
            final long lineNumber, final IRequestPropertiesACBB requestProperties)
            throws IOException {

        String[] values;
        values = parser.getLine();
        if (requestProperties.getSite() == null) {
            return lineNumber + 1;
        }
        final long returnLineNumber = this.readTraitement(badsFormatsReport, lineNumber, values,
                requestProperties);
        if (requestProperties.getTraitement() == null) {

            return returnLineNumber;

        }
        try {
            VersionDeTraitement versionDeTraitement = null;
            final String versionNumber = values[3];
            requestProperties.setVersion(Integer.parseInt(versionNumber));
            if (requestProperties.getSite() != null && requestProperties.getTraitement() != null
                    && requestProperties.getVersion() > 0) {
                versionDeTraitement = this.versionDeTraitementDAO.getByNKey(
                        requestProperties.getSite().getCode(),
                        requestProperties.getTraitement().getCode(),
                        requestProperties.getVersion()
                ).orElseThrow(() -> new PersistenceException("bad version de traitement"));
                requestProperties.setVersionDeTraitement(versionDeTraitement);
            }
            if (versionDeTraitement == null) {

                badsFormatsReport
                        .addException(new BadExpectedValueException(
                                String.format(
                                        RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_SITE_TREATMENT_VERSION),
                                        requestProperties.getTraitement().getAffichage(),
                                        returnLineNumber, 2, requestProperties.getSite().getName(),
                                        requestProperties.getVersion())));

            }
            if (requestProperties.getParcelle() != null
                    && requestProperties.getTraitement() != null) {
                this.updateSuiviParcelle(badsFormatsReport, returnLineNumber, requestProperties);
            }
        } catch (NumberFormatException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_VERSION_TRAITEMENT), requestProperties.getVersion(),
                    returnLineNumber, 2, requestProperties.getSite().getCode(), requestProperties.getTraitement().getCode())));

        } catch (IndexOutOfBoundsException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_VERSION_TRAITEMENT), requestProperties.getVersion(),
                    returnLineNumber, 2, requestProperties.getSite().getCode(), requestProperties.getTraitement().getCode())));

        } catch (final PersistenceException e) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_VERSION_TREATMENT),
                    returnLineNumber, 2)));

        }
        return returnLineNumber;

    }

    /**
     *
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {

        this.datasetConfiguration = datasetConfiguration;

    }

    /**
     *
     * Sets the datatype unite variable acbbdao.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param datatypeVariableUniteACBBDAO
     *
     * the new datatype unite variable acbbdao
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {

        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;

    }

    /**
     *
     * Sets the localization manager.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param localizationManager
     *
     * the new localization manager {@link ILocalizationManager} the new
     * localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {

        this.localizationManager = localizationManager;

    }

    /**
     *
     * Sets the parcelle dao.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param parcelleDAO
     *
     * the new parcelle dao {@link IParcelleDAO} the new parcelle dao
     */
    public final void setParcelleDAO(final IParcelleDAO parcelleDAO) {

        this.parcelleDAO = parcelleDAO;

    }

    /**
     *
     * Sets the site dao.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param siteDAO
     *
     * the new site dao {@link ISiteDAO} the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteDAO) {

        this.siteDAO = siteDAO;

    }

    /**
     *
     * Sets the suivi parcelle dao.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param suiviParcelleDAO
     *
     * the new suivi parcelle dao {@link ISuiviParcelleDAO} the new suivi
     * parcelle dao
     */
    public final void setSuiviParcelleDAO(final ISuiviParcelleDAO suiviParcelleDAO) {

        this.suiviParcelleDAO = suiviParcelleDAO;

    }

    /**
     *
     * Sets the traitement dao.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param traitementDAO
     *
     * the new traitement dao {@link ITraitementDAO} the new traitement dao
     */
    public final void setTraitementDAO(final ITraitementDAO traitementDAO) {

        this.traitementDAO = traitementDAO;

    }

    /**
     *
     * Sets the version de traitement dao.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param versionDeTraitementDAO
     *
     * the new version de traitement dao {@link IVersionDeTraitementDAO} the new
     * version de traitement dao
     */
    public final void setVersionDeTraitementDAO(final IVersionDeTraitementDAO versionDeTraitementDAO) {

        this.versionDeTraitementDAO = versionDeTraitementDAO;

    }

    void testAreConsistentDates(BadsFormatsReport badsFormatsReport, VersionFile version,
                                IRequestPropertiesACBB requestProperties, final long lineNumber) {
        LocalDateTime dateDeDebutLocale = null, dateDeFinLocale = null;
        dateDeDebutLocale = requestProperties.getDateDeDebut();
        dateDeFinLocale = requestProperties.getDateDeFin();
        if (dateDeDebutLocale == null
                || !version.getDataset().getDateDebutPeriode().equals(dateDeDebutLocale)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_IMPROPER_BEGIN_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(dateDeDebutLocale, DateUtil.DD_MM_YYYY_HH_MM),
                    lineNumber, 1, this.buildDownloadFilename(version))));
        }
        if (dateDeFinLocale == null
                || !version.getDataset().getDateFinPeriode().equals(dateDeFinLocale)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_IMPROPER_END_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(dateDeFinLocale, DateUtil.DD_MM_YYYY_HH_MM),
                    lineNumber, 2, this.buildDownloadFilename(version))));
        }
    }

    /**
     *
     * Test headers.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param parser
     *
     * the parser
     *
     * @param versionFile
     *
     * @link(VersionFile) the version file
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     * @param encoding
     *
     *            the encoding
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param datasetDescriptor
     *
     * @link(DatasetDescriptorACBB) the dataset descriptor
     *
     *
     *
     * @return the long
     *
     *
     *
     * @throws BusinessException
     *
     *             the business exception {@link VersionFile} the version file
     *             {@link IRequestPropertiesACBB} the request properties {@link BadsFormatsReport}
     *             the bads formats report {@link DatasetDescriptorACBB} the dataset descriptor
     *
     * @link(VersionFile) the version file
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(DatasetDescriptorACBB) the dataset descriptor
     *
     * @see org.inra.ecoinfo.acbb.dataset.ITestHeaders#testHeaders(com.Ostermiller.util.CSVParser,
     *      org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *      org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String)
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final String encoding,
                            final BadsFormatsReport badsFormatsReport, final DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException {

        try {
            this.updateMinMaxDatatypeVariableUnite(new CSVParser(new InputStreamReader(
                            new ByteArrayInputStream(versionFile.getData()), encoding), ITestHeaders.SEPARATOR),
                    datasetDescriptor);

        } catch (final BadDelimiterException | UnsupportedEncodingException e) {

            badsFormatsReport.addException(e);

        }
        return 0;

    }

    /**
     *
     *
     *
     * test if the begin date is before the end date
     *
     *
     *
     * you must read begin and end dates before.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties {@link BadsFormatsReport} the bads
     *                               formats report {@link IRequestPropertiesACBB} the session
     *                               properties
     */
    protected void testIntervalDate(final BadsFormatsReport badsFormatsReport,
                                    final long lineNumber, final IRequestPropertiesACBB requestProperties) {

        if (requestProperties.getDateDeDebut() != null
                && requestProperties.getDateDeFin() != null
                && requestProperties.getDateDeDebut().isAfter(requestProperties.getDateDeFin())) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_INTERVAL_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(requestProperties.getDateDeDebut(), requestProperties.getDateFormat()),
                    lineNumber - 1, 1,
                    DateUtil.getUTCDateTextFromLocalDateTime(requestProperties.getDateDeFin(), requestProperties.getDateFormat()),
                    lineNumber, 1)));

        }

    }

    private void testIsConsistentParcelle(VersionFile version, BadsFormatsReport badsFormatsReport,
                                          long lineNumber, IRequestPropertiesACBB requestProperties) {

        if (requestProperties.getParcelle() == null) {

            return;

        }
        RealNode node = version.getDataset().getRealNode();
        Parcelle parcelle = null;

        parcelle = (Parcelle) node.getNodeByNodeableTypeResource(Parcelle.class).getNodeable();
        if (!requestProperties.getParcelle().equals(parcelle)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_IMPROPER_PLOT),
                    this.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                            requestProperties.getParcelle().getName(),
                            requestProperties.getParcelle().getName()), lineNumber, 2, version
                            .getDataset().buildDownloadFilename(this.datasetConfiguration))));
        }

    }

    private void testIsConsistentSite(VersionFile version, BadsFormatsReport badsFormatsReport,
                                      long lineNumber, IRequestPropertiesACBB requestProperties) {
        if (requestProperties.getSite() == null) {
            return;
        }
        RealNode node = version.getDataset().getRealNode();
        SiteACBB site = null;
        site = ((SiteACBB) node.getNodeByNodeableTypeResource(SiteACBB.class).getNodeable());
        if (!site.equals(requestProperties.getSite())) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_IMPROPER_SITE),
                    this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                            requestProperties.getSite().getName(),
                            requestProperties.getSite().getName()), lineNumber, 1, version
                            .getDataset().buildDownloadFilename(this.datasetConfiguration))));
        }

    }

    /**
     *
     *
     *
     * read the min and max header lines.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param parser
     *
     * @link(CSVParser) the parser
     *
     * @param datasetDescriptor
     *
     * @link(DatasetDescriptorACBB) the dataset descriptor
     *
     * @link(CSVParser) the parser
     *
     * @link(DatasetDescriptorACBB) the dataset descriptor {@link DatasetDescriptorACBB}
     */
    void updateMinMaxDatatypeVariableUnite(final CSVParser parser,
                                           final DatasetDescriptorACBB datasetDescriptor) {

        String[] headersValues;
        String[] minValues;
        String[] maxValues;
        try {

            final String[][] allValues = parser.getAllValues();

            headersValues = datasetDescriptor.getHeadersLine() - 1 > 0 ? allValues[datasetDescriptor
                    .getHeadersLine() - 1] : null;

            minValues = datasetDescriptor.getMinLine() - 1 > 0 ? allValues[datasetDescriptor
                    .getMinLine() - 1] : null;

            maxValues = datasetDescriptor.getMaxLine() - 1 > 0 ? allValues[datasetDescriptor
                    .getMaxLine() - 1] : null;

        } catch (final IOException e) {
            LOGGER.info("can't parse all values");
            return;

        }
        if (headersValues == null) {

            return;

        }
        for (int i = 0; i < headersValues.length; i++) {
            try {
                final String header = headersValues[i];

                if (minValues != null && i < minValues.length) {

                    final Float min = Float.valueOf(minValues[i]);

                    datasetDescriptor.getMins().put(header, min);

                }

                if (maxValues != null && i < maxValues.length) {

                    final Float max = Float.valueOf(maxValues[i]);

                    datasetDescriptor.getMaxs().put(header, max);

                }
            } catch (final NumberFormatException e) {
                LOGGER.info("can't parse min/max value");
            }
        }
        for (final DatatypeVariableUniteACBB datatypeVariableUniteACBB : this.datatypeVariableUniteACBBDAO
                .getAllVariableTypeDonneesByDataTypeMapByVariableCode(datasetDescriptor.getName())
                .values()) {
            final Float dbMin = datatypeVariableUniteACBB.getValeurMin() == null ? Float.NEGATIVE_INFINITY
                    : datatypeVariableUniteACBB.getValeurMin();
            final Float dbMax = datatypeVariableUniteACBB.getValeurMax() == null ? Float.POSITIVE_INFINITY
                    : datatypeVariableUniteACBB.getValeurMax();
            final Float writeMin = datasetDescriptor.getMins().get(
                    datatypeVariableUniteACBB.getVariable().getAffichage());
            Float writeMax = datasetDescriptor.getMaxs().get(
                    datatypeVariableUniteACBB.getVariable().getAffichage());
            this.updateMinMaxWrite(writeMin, dbMin, dbMax, writeMax, datasetDescriptor,
                    datatypeVariableUniteACBB);
            writeMax = datasetDescriptor.getMaxs().get(
                    datatypeVariableUniteACBB.getVariable().getAffichage());
        }

    }

    /**
     *
     * @param writeMin
     * @param dbMin
     * @param dbMax
     * @param writeMax
     * @param datasetDescriptor
     * @param datatypeVariableUniteACBB
     */
    protected void updateMinMaxWrite(final Float writeMin, final Float dbMin, final Float dbMax,
                                     final Float writeMax, final DatasetDescriptorACBB datasetDescriptor,
                                     final DatatypeVariableUniteACBB datatypeVariableUniteACBB) {

        this.updatWriteMin(writeMin, dbMin, dbMax, writeMax, datasetDescriptor,
                datatypeVariableUniteACBB);
        this.updateWriteMax(writeMax, dbMax, dbMin, writeMin, datasetDescriptor,
                datatypeVariableUniteACBB);

    }

    /**
     *
     *
     *
     * find the suiviParcelle
     *
     *
     *
     * you must read treatment and plot before.
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * @param badsFormatsReport
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @param lineNumber
     *
     *            int the line number
     *
     * @param requestProperties
     *
     * @link(IRequestPropertiesACBB) the request properties
     *
     * @link(BadsFormatsReport) the bads formats report
     *
     * @link(IRequestPropertiesACBB) the request properties {@link BadsFormatsReport} the bads
     *                               formats report {@link IRequestPropertiesACBB} the session
     *                               properties
     */
    protected void updateSuiviParcelle(final BadsFormatsReport badsFormatsReport,
                                       final long lineNumber, final IRequestPropertiesACBB requestProperties) {

        if (requestProperties.getSite() != null && requestProperties.getTraitement() != null) {

            try {
                final LocalDate dateDebutTraitement = requestProperties.getDateDebutTraitement();
                requestProperties.setSuiviParcelle(this.suiviParcelleDAO.getByNKey(
                        requestProperties.getParcelle(), requestProperties.getTraitement(),
                        dateDebutTraitement).orElseThrow(() -> new PersistenceException("bad suivi parcelle")));
            } catch (final PersistenceException e) {

                badsFormatsReport
                        .addException(new BadExpectedValueException(
                                String.format(
                                        RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER),
                                        lineNumber, requestProperties.getParcelle().getName(),
                                        requestProperties.getTraitement().getAffichage())));

            }

        }

    }

    /**
     *
     * @param writeMax
     * @param dbMax
     * @param dbMin
     * @param writeMin
     * @param datasetDescriptor
     * @param datatypeVariableUniteACBB
     *
     * when the wwriteMin is between the dbMin and the dbMax theb it becomes the
     * new max
     */
    protected void updateWriteMax(final Float writeMax, final Float dbMax, final Float dbMin,
                                  final Float writeMin, final DatasetDescriptorACBB datasetDescriptor,
                                  final DatatypeVariableUniteACBB datatypeVariableUniteACBB) {
        float newMax = dbMax;
        if (writeMax != null && writeMax < dbMax && writeMax > dbMin && writeMin < dbMax) {
            newMax = writeMax;
        }
        datasetDescriptor.getMaxs().put(datatypeVariableUniteACBB.getVariable().getAffichage(),
                newMax);
    }

    /**
     *
     * @param writeMin
     * @param dbMin
     * @param dbMax
     * @param writeMax
     * @param datasetDescriptor
     * @param datatypeVariableUniteACBB
     *
     *
     * when the wwriteMax is between the dbMin and the dbMax theb it becomes the
     * new max
     */
    protected void updatWriteMin(final Float writeMin, final Float dbMin, final Float dbMax,
                                 final Float writeMax, final DatasetDescriptorACBB datasetDescriptor,
                                 final DatatypeVariableUniteACBB datatypeVariableUniteACBB) {
        float newMin = dbMin;
        if (writeMin != null && !(writeMin > dbMin && writeMin < dbMax && writeMin < writeMax)) {
            newMin = datasetDescriptor.getMins().get(
                    datatypeVariableUniteACBB.getVariable().getAffichage());
        }
        datasetDescriptor.getMins().put(datatypeVariableUniteACBB.getVariable().getAffichage(),
                newMin);
    }

}
