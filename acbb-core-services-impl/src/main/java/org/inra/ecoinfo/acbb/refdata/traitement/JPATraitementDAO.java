/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.traitement;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.refdata.site.Site;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPATraitementDAO.
 */
public class JPATraitementDAO extends AbstractJPADAO<TraitementProgramme> implements ITraitementDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO#getAll()
     */
    @Override
    public List<TraitementProgramme> getAll() {
        return getAll(TraitementProgramme.class);
    }

    /**
     * Gets the by n key.
     *
     * @param siteId
     * @param code
     * @return the by n key
     * @link(Long) the site id
     * @link(String) the code
     */
    @Override
    public Optional<TraitementProgramme> getByNKey(final Long siteId, final String code) {
        CriteriaQuery<TraitementProgramme> query = builder.createQuery(TraitementProgramme.class);
        Root<TraitementProgramme> tra = query.from(TraitementProgramme.class);
        query.where(
                builder.equal(tra.get(TraitementProgramme_.code), code),
                builder.equal(tra.join(TraitementProgramme_.site).get(Nodeable_.id), siteId)
        );
        query.select(tra);
        return getOptional(query);
    }

    /**
     * Gets the by site.
     *
     * @param site
     * @return the by site @see
     * org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO#getBySite(org
     * .inra.ecoinfo.dataset.refdata.site.Site)
     * @link(Site) the site
     */
    @Override
    public List<TraitementProgramme> getBySite(final Site site) {
        CriteriaQuery<TraitementProgramme> query = builder.createQuery(TraitementProgramme.class);
        Root<TraitementProgramme> tra = query.from(TraitementProgramme.class);
        query.where(
                builder.equal(tra.join(TraitementProgramme_.site), site)
        );
        query.select(tra);
        return getResultList(query);
    }

}
