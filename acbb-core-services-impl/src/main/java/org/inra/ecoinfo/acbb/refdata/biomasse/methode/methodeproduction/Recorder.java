package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * The Class Recorder.
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractMethodeRecorder<MethodeProduction> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.messages";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    /**
     * The massevegclass possibles @link(Map<String,List<String>>).
     */
    Map<String, List<String>> methodeProductionPossibles;
    private IMethodeProductionDAO methodeProductionDAO;

    /**
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String numberIdString = tokenizerValues.nextToken();
                ErrorsReport errorsReport = new ErrorsReport();
                Optional<MethodeProduction> methodeProduction = this.getByNumberId(numberIdString, errorsReport);
                if (methodeProduction.isPresent()) {
                    this.methodeProductionDAO.saveOrUpdate(methodeProduction.get());
                    this.methodeProductionDAO.remove(methodeProduction.get());
                }
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param allMethods
     * @return
     */
    @Override
    protected Set<MethodeProduction> filter(Set<AbstractMethode> allMethods) {
        Set<MethodeProduction> methodes = new HashSet();
        for (AbstractMethode methode : allMethods) {
            if (methode instanceof MethodeProduction) {
                methodes.add((MethodeProduction) methode);
            }
        }
        return methodes;
    }

    /**
     * @return
     */
    @Override
    protected List<MethodeProduction> getAllElements() {
        List<MethodeProduction> all = this.methodeProductionDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeDefinitionName() {
        return AbstractMethode.ATTRIBUTE_JPA_DEFINITION;
    }

    // SETTERS DAO

    /**
     * @return
     */
    @Override
    protected String getMethodeEntityName() {
        return MethodeProduction.NAME_ENTITY_JPA;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeLibelleName() {
        return AbstractMethode.ATTRIBUTE_JPA_LIBELLE;
    }

    /**
     * @param methodeProduction
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final MethodeProduction methodeProduction) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = super
                .getNewLineModelGridMetadata(methodeProduction);
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<MethodeProduction> initModelGridMetadata() {
        this.initProperties();
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(
                        values, MethodeProduction.NAME_ENTITY_JPA);
                final Long numberId = this.getNumberId(errorsReport, lineNumber, tokenizerValues);
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String definition = this.getDefinition(errorsReport, lineNumber,
                        tokenizerValues);
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMethodeProduction(errorsReport, numberId, libelle, definition);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMethodeProduction(
            AbstractCSVMetadataRecorder.ErrorsReport errorsReport, Long numberId,
            String libelle, String definition) {
        MethodeProduction methodeProductionDb = this.methodeProductionDAO.getByNKey(numberId).orElse(null);
        if (methodeProductionDb == null) {

            methodeProductionDb = new MethodeProduction(definition, libelle, numberId);

        } else {

            methodeProductionDb.update(definition);
        }
        try {
            this.methodeProductionDAO.saveOrUpdate(methodeProductionDb);
        } catch (PersistenceException e) {

            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);

        }
    }

    /**
     * @param methodeProductionDAO
     */
    public void setMethodeProductionDAO(IMethodeProductionDAO methodeProductionDAO) {
        this.methodeProductionDAO = methodeProductionDAO;
    }

}
