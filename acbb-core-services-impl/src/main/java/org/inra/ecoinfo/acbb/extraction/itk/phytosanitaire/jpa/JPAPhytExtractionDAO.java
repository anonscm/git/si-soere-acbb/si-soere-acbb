/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.phytosanitaire.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire_;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.phytosanitaire.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
@SuppressWarnings("unchecked")
public class JPAPhytExtractionDAO extends AbstractJPAITKDAO<Phytosanitaire, Phytosanitaire, ValeurPhytosanitaire> {

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<Phytosanitaire> getMesureITKClass() {
        return Phytosanitaire.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurPhytosanitaire> getValeurITKClass() {
        return ValeurPhytosanitaire.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, Phytosanitaire> getMesureAttribute() {
        return ValeurPhytosanitaire_.phytosanitaire;
    }
}
