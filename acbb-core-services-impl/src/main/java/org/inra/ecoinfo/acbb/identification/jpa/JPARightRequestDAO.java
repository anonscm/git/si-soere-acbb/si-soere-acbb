/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.identification.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.identification.entity.RightsRequest;
import org.inra.ecoinfo.acbb.identification.entity.RightsRequest_;
import org.inra.ecoinfo.identification.IRightRequestDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author ptcherniati
 */
public class JPARightRequestDAO extends AbstractJPADAO<RightsRequest> implements IRightRequestDAO<RightsRequest> {
    /**
     * @param utilisateur
     * @return
     */
    @Override
    public List<RightsRequest> getByUser(Utilisateur utilisateur) {

        CriteriaQuery<RightsRequest> query = builder.createQuery(RightsRequest.class);
        Root<RightsRequest> rightRequest = query.from(RightsRequest.class);
        query.where(builder.equal(rightRequest.join(RightsRequest_.utilisateur), utilisateur));
        query.select(rightRequest);
        return getResultList(query);
    }

    /**
     * @return
     */
    @Override
    public List<RightsRequest> getAll() {
        return getAll(RightsRequest.class);
    }

}
