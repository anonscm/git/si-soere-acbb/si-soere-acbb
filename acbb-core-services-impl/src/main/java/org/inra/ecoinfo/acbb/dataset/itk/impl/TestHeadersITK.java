package org.inra.ecoinfo.acbb.dataset.itk.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class TestHeadersITK.
 *
 * @author Tcherniatinsky Philippe
 */
public class TestHeadersITK extends GenericTestHeader {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The datatype name @link(String).
     */
    String datatypeName;

    /**
     * Instantiates a new test headers ITK.
     */
    public TestHeadersITK() {
        super();
    }

    /**
     * @param badsFormatsReport
     * @param lineNumber
     * @param requestPropertiesITK
     * @param values
     * @param cleanerValues
     * @param columnToParse
     * @return
     */
    protected int getColumns(final BadsFormatsReport badsFormatsReport, final long lineNumber,
                             final IRequestPropertiesITK requestPropertiesITK, String[] values,
                             final CleanerValues cleanerValues, final List<Column> columnToParse) {
        int index;
        String value;
        for (index = 0; index < values.length; index++) {
            value = null;
            try {
                value = cleanerValues.nextToken();
            } catch (EndOfCSVLine ex) {
                badsFormatsReport.addException(ex.setMessage(lineNumber, index));
                continue;
            }
            Column columnTarget = null;
            for (final Column column : columnToParse) {
                if (column.getName().equalsIgnoreCase(value)) {
                    columnTarget = column;
                    break;
                }
            }
            if (columnTarget == null) {
                badsFormatsReport
                        .addException(new BadExpectedValueException(
                                String.format(
                                        RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_GENERIC_COLUMN_HEADER),
                                        lineNumber, value, index + 1)));
            } else {
                requestPropertiesITK.getValueColumns().put(index, columnTarget);
                // columnToParse.remove(columnTarget);
            }
        }
        return index;
    }

    /**
     * @param badsFormatsReport
     * @param parser
     * @param datasetDescriptor
     * @param lineNumber
     * @param requestPropertiesACBB
     * @return @throws java.io.IOException
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader#readLineHeader(org.inra.ecoinfo.utils.exceptions.BadsFormatsReport,
     * com.Ostermiller.util.CSVParser, long, org.inra.ecoinfo.utils.DatasetDescriptor,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB)
     */
    @Override
    protected long readLineHeader(final BadsFormatsReport badsFormatsReport,
                                  final CSVParser parser, final long lineNumber,
                                  final DatasetDescriptor datasetDescriptor,
                                  final IRequestPropertiesACBB requestPropertiesACBB) throws IOException {
        String[] values;
        values = parser.getLine();
        final CleanerValues cleanerValues = new CleanerValues(values);
        final List<Column> columnToParse = new LinkedList<>(datasetDescriptor.getColumns());
        this.getColumns(badsFormatsReport, lineNumber,
                (IRequestPropertiesITK) requestPropertiesACBB, values, cleanerValues, columnToParse);
        return lineNumber + 1;
    }

    /**
     * Sets the datatype name.
     *
     * @param datatypeName the new datatype name
     */
    public final void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     * Test headers.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return the long
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.ITestHeaders#testHeaders(com.Ostermiller .util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.utils.exceptions.BadsFormatsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final String encoding,
                            final BadsFormatsReport badsFormatsReport, final DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException {
        long lineNumber = 0;
        try {
            super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport,
                    datasetDescriptor);
            lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber,
                    requestProperties);
            lineNumber = this
                    .readDatatype(badsFormatsReport, parser, lineNumber, this.datatypeName);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, requestProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, requestProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 1);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, requestProperties);
            lineNumber = this.jumpLines(parser, lineNumber, 3);
        } catch (final IOException e) {
            this.getLogger().debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }
}
