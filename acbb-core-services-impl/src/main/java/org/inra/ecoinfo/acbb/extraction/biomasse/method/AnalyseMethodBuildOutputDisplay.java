/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class AnalyseMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "BiomasseAnalyseMethode";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return AnalyseMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
