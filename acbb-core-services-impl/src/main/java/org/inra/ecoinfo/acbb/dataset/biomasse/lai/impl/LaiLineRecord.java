package org.inra.ecoinfo.acbb.dataset.biomasse.lai.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.LaiNature;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class LaiLineRecord extends AbstractLineRecord<LaiLineRecord, VariableValueLai> {
    LaiNature nature = null;

    /**
     * Instantiates a new aI line record.
     *
     * @param lineCount
     * @link{long the line count
     */
    public LaiLineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * @return
     */
    public LaiNature getNature() {
        return nature;
    }

    /**
     * @param nature
     */
    public void setNature(LaiNature nature) {
        this.nature = nature;
    }
}
