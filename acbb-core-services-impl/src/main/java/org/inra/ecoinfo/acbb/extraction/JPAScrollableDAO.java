package org.inra.ecoinfo.acbb.extraction;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.IDAO;

import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

/**
 * @param <T>
 * @author ptcherniati
 */
public class JPAScrollableDAO<T> extends AbstractJPADAO<T> implements IDAO<T> {

    int page;

    PaginableResultIterator<T> getIterator(Query query, boolean isNative) {
        return new PaginableResultIterator(query, this.page);
    }

    /**
     * @param query
     * @param pageLength
     * @return
     */
    public PaginableResultIterator<T> getPaginableResultIterator(Query query, int pageLength) {
        return new PaginableResultIterator(query, pageLength);
    }
}

class PaginableResultIterator<T> implements Iterator<T> {

    int pageLength;
    Query query;
    List<T> pageListe;
    Iterator<T> iterator;
    boolean lastList = false;
    int page;

    PaginableResultIterator(Query query, int pageLength) {
        this.pageLength = pageLength;
        this.query = query;
    }

    private void getPage() {
        this.query.setFirstResult(this.pageLength * this.page++);
        this.query.setMaxResults(this.pageLength);
        this.pageListe = this.query.getResultList();
        this.lastList = this.pageListe.size() < this.pageLength;
        this.iterator = this.pageListe.iterator();
    }

    @Override
    public boolean hasNext() {
        if (this.pageListe == null) {
            this.getPage();
        }
        if (!this.iterator.hasNext() && this.lastList) {
            return false;
        } else if (!this.iterator.hasNext()) {
            this.getPage();
        }
        return this.iterator.hasNext();
    }

    @Override
    public T next() {
        final T next = this.iterator.next();
        this.iterator.remove();
        return next;
    }

    @Override
    public void remove() {
        this.iterator.remove();
    }

}
