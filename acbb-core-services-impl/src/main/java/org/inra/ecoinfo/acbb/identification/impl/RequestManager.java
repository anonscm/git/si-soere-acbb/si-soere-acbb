/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.identification.impl;

import org.inra.ecoinfo.acbb.identification.entity.RightsRequest;
import org.inra.ecoinfo.identification.IRequestManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.impl.AbstractRequestManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * @author ptcherniati
 */
public class RequestManager extends AbstractRequestManager<RightsRequest> implements IRequestManager<RightsRequest> {

    /**
     * The Constant PROPERTY_MSG_RIGHT_DEMAND.
     */
    public static final String PROPERTY_MSG_RIGHT_DEMAND = "PROPERTY_MSG_RIGHT_DEMAND";

    /**
     *
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.messages";

    /**
     *
     */
    public static final String LOCAL_BUNDLE_NAME = "org.inra.ecoinfo.acbb.identification.message";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY = "PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY";

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(RequestManager.class);

    /**
     * @param admin
     * @param utilisateur
     * @param rightRequest
     * @return
     */
    @Override
    public String getMailMessageForRequest(Utilisateur admin, Utilisateur utilisateur, RightsRequest rightRequest) {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(LOCAL_BUNDLE_NAME, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
        String message = resourceBundle.getString(PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT);
        message = String.format(message,
                admin.getPrenom(), admin.getNom(),
                utilisateur.getLogin(), rightRequest.toString(),
                rightRequest.getTitle(), rightRequest.getObjectives(),
                rightRequest.getAdditionalsInformations(),
                utilisateur.getEmail());
        return message;

    }

    /**
     * @param rightRequest
     * @param utilisateur
     */
    @Override
    protected void setRightRequestUser(RightsRequest rightRequest, Utilisateur utilisateur) {
        rightRequest.setUser(utilisateur);
    }

    /**
     * @param rightRequest
     * @return
     */
    @Override
    protected Optional<Utilisateur> getUtilisateurForRequest(RightsRequest rightRequest) {
        return Optional.ofNullable(rightRequest).map(rr -> rr.getUser());
    }

}
