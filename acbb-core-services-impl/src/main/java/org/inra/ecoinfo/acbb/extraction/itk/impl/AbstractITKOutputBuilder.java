package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorBuilderACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.impl.ComparatorVariable;
import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.VariableDescriptor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

/**
 * @param <T>
 * @param <V>
 * @param <M>
 * @author ptcherniati
 */
public abstract class AbstractITKOutputBuilder<T extends AbstractIntervention, V extends ValeurIntervention, M extends IMesureITK<T, V>>
        extends AbstractOutputBuilder {

    /**
     *
     */
    public static final String DATASET_DESCRIPTOR_PATH_PATTERN = "org/inra/ecoinfo/acbb/dataset/%s";
    /**
     * The Constant HEADER_RAW_DATA @link(String).
     */
    protected static final String PROPERTY_HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.messages";
    /**
     * The Constant MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS @link(String).
     */
    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNKNOWN_VARIABLE";
    /**
     * The Constant PATTERN_END_LINE @link(String).
     */
    static final String PATTERN_END_LINE = "\n";
    /**
     * The Constant REGEX_CSV_FIELD @link(String).
     */
    static final String REGEX_CSV_FIELD = "[^;]";
    /**
     * The Constant DATASET_DESCRIPTOR_XML @link(String).
     */
    static final String DATASET_DESCRIPTOR_XML = "../../../dataset-descriptor.xml";
    private static final String CST_EXTRACT = "EXTRACT_";
    /**
     * The comparator @link(ComparatorVariable).
     */
    final ComparatorVariable comparator = new ComparatorVariable();
    /**
     * The variable dao @link(IVariableACBBDAO).
     */
    IVariableACBBDAO variableDAO;
    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     * The variables descriptor @link(Map<String,VariableDescriptor>).
     */
    Map<String, VariableDescriptor> variablesDescriptor = new HashMap();
    /**
     * The dataset descriptor @link(DatasetDescriptor).
     */
    private DatasetDescriptor datasetDescriptor;
    private String resultCode = org.apache.commons.lang.StringUtils.EMPTY;
    private String extractCode = org.apache.commons.lang.StringUtils.EMPTY;
    private ISuiviParcelleDAO suiviParcelleDAO;

    /**
     * @param datasetDescriptorPath
     */
    public AbstractITKOutputBuilder(String datasetDescriptorPath) {
        super();
        try {
            if (this.datasetDescriptor == null) {
                this.setDatasetDescriptor(this.getDatasetDescriptor(datasetDescriptorPath));
                for (final Column column : this.datasetDescriptor.getColumns()) {
                    if (null != column.getFlagType()) {
                        switch (column.getFlagType()) {
                            case RecorderACBB.PROPERTY_CST_VARIABLE_TYPE:
                            case RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE:
                                this.variablesDescriptor.put(column.getName(),
                                        new VariableDescriptor(column.getName()));
                                break;
                            case RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE:
                                this.variablesDescriptor.get(column.getRefVariableName())
                                        .setHasQualityClass(true);
                                break;
                        }
                    }
                }
            }
        } catch (final IOException | SAXException | URISyntaxException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * @param headers
     * @param requestMetadatasMap
     * @param resultsDatasMap
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String,
     * java.util.Map, java.util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(final String headers,
            final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        final List<Variable> selectedWsolVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts = (Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>>) requestMetadatasMap
                .getOrDefault(Parcelle.class.getSimpleName(), new HashMap());
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(this.getExtractCode());
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            outputPrintStreamMap.get(datatypeNameEntry.getKey()).println(headers);
        }
        final PrintStream rawDataPrintStream = outputPrintStreamMap.get(this.getExtractCode());
        final OutputHelper outputHelper = this.getOutPutHelper(datesYearsContinuousFormParamVO,
                selectedWsolVariables, availablesCouverts, rawDataPrintStream);
        final List<M> mesure = resultsDatasMap.get(AbstractOutputBuilder.MAP_INDEX_0);
        outputHelper.buildBody(mesure);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Properties propertiesVariableName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        final ErrorsReport errorsReport = new ErrorsReport();
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final List<String> selectedVariablesAffichage = new LinkedList();
        for (final Variable variable : selectedVariables) {
            selectedVariablesAffichage.add(variable.getAffichage());
        }
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        try {
            stringBuilder1.append(this.getLocalizationManager().getMessage(
                    this.getBundleSourcePath(), AbstractITKOutputBuilder.PROPERTY_HEADER_RAW_DATA));
            stringBuilder2.append(this
                    .getLocalizationManager()
                    .getMessage(this.getBundleSourcePath(),
                            AbstractITKOutputBuilder.PROPERTY_HEADER_RAW_DATA)
                    .replaceAll(AbstractITKOutputBuilder.REGEX_CSV_FIELD,
                            org.apache.commons.lang.StringUtils.EMPTY));
            stringBuilder3.append(this
                    .getLocalizationManager()
                    .getMessage(this.getBundleSourcePath(),
                            AbstractITKOutputBuilder.PROPERTY_HEADER_RAW_DATA)
                    .replaceAll(AbstractITKOutputBuilder.REGEX_CSV_FIELD,
                            org.apache.commons.lang.StringUtils.EMPTY));
            final Iterator<Column> it = this.datasetDescriptor.getColumns().iterator();
            Column column;
            VariableACBB variable;
            while (it.hasNext()) {
                column = it.next();
                if (column.isFlag()
                        && (RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())
                        || RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE.equals(column
                                .getFlagType()) || RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE
                                .equals(column.getFlagType()))) {
                    variable = (VariableACBB) this.variableDAO.getByAffichage(column.getName()).orElse(null);
                    if (variable == null) {
                        errorsReport
                                .addErrorMessage(String
                                        .format(this
                                                .getLocalizationManager()
                                                .getMessage(
                                                        AbstractITKOutputBuilder.BUNDLE_SOURCE_PATH,
                                                        AbstractITKOutputBuilder.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                                selectedVariables));
                    }
                    String localizedVariableName = propertiesVariableName.getProperty(
                            variable.getName(), variable.getName());
                    final Unite unite = this.datatypeVariableUniteACBBDAO.getUnite(
                            this.getExtractCode(), variable.getCode())
                            .orElseThrow(() -> new BusinessException("bad unit"));
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder1.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD, localizedVariableName));
                    }
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder2.append(String.format(
                                ACBBMessages.PATTERN_1_FIELD, unite.getCode()));
                    }
                    if (selectedVariablesAffichage.contains(variable.getAffichage())) {
                        stringBuilder3
                                .append(String.format(ACBBMessages.PATTERN_1_FIELD,
                                        variable.getAffichage()));
                    }
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder1.append(AbstractITKOutputBuilder.PATTERN_END_LINE)
                .append(stringBuilder2).append(AbstractITKOutputBuilder.PATTERN_END_LINE)
                .append(stringBuilder3).toString();
    }

    /**
     * @param parameters
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        if (!((DefaultParameter) parameters).getResults().containsKey(this.getResultCode())
                || ((DefaultParameter) parameters).getResults().get(this.getResultCode())
                        .get(this.getExtractCode()) == null
                || ((DefaultParameter) parameters).getResults().get(this.getResultCode())
                        .get(this.getExtractCode()).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, this.getResultCode()));
        return null;
    }

    /**
     * @return
     */
    protected abstract String getBundleSourcePath();

    /**
     * @return
     */
    protected String getExtractCode() {
        return this.extractCode;
    }

    /**
     * @param extractCode the extractCode to set
     */
    public void setExtractCode(String extractCode) {
        this.extractCode = extractCode;
        this.resultCode = new StringBuffer(AbstractITKOutputBuilder.CST_EXTRACT)
                .append(extractCode).toString();
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    protected OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedWsolVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream) {
        return new OutputHelper(rawDataPrintStream,
                selectedWsolVariables);
    }

    /**
     * @return
     */
    protected String getResultCode() {
        return this.resultCode;
    }

    /**
     * @param parcelle
     * @param date
     * @return
     */
    public Optional<SuiviParcelle> getSuiviParcelle(Parcelle parcelle, LocalDate date) {
        return this.suiviParcelleDAO.retrieveSuiviParcelle(parcelle, date);
    }

    /**
     * Sets the dataset descriptor {@link DatasetDescriptor}.
     *
     * @param datasetDescriptor the new dataset descriptor
     * {@link DatasetDescriptor}
     */
    void setDatasetDescriptor(final DatasetDescriptor datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteDAO the new datatype unite variable acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteDAO;
    }

    /**
     * @param suiviParcelleDAO
     */
    public void setSuiviParcelleDAO(ISuiviParcelleDAO suiviParcelleDAO) {
        this.suiviParcelleDAO = suiviParcelleDAO;
    }

    /**
     * Sets the variable dao {@link IVariableACBBDAO}.
     *
     * @param variableDAO the new variable dao {@link IVariableACBBDAO}
     */
    public void setVariableDAO(final IVariableACBBDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    private DatasetDescriptorACBB getDatasetDescriptor(String datasetDescriptorPath)
            throws IOException, SAXException, URISyntaxException {
        final String path = String.format(
                AbstractITKOutputBuilder.DATASET_DESCRIPTOR_PATH_PATTERN,
                datasetDescriptorPath);
        return DatasetDescriptorBuilderACBB.buildDescriptorACBB(this.getClass().getClassLoader()
                .getResourceAsStream(path));
    }

    /**
     * The Class OutputHelper.
     */
    public class OutputHelper {

        /**
         * The Constant PATTERN_5_FIELD @link(String).
         */
        static final String PATTERN_11_FIELD = "%s;%s;%s;%s;%s;%s(%s);%s;%s;%s;%s;%s";
        /**
         * The raw data print stream @link(PrintStream).
         */
        final protected PrintStream rawDataPrintStream;
        /**
         * The variables affichage @link(List).
         */
        protected final List<String> variablesAffichage = new LinkedList();
        /**
         * The date @link(Date).
         */
//        protected Date date;
        /**
         *
         */
//        protected String parcelleName;
        /**
         *
         */
//        protected String siteName;
        /**
         *
         */
//        protected String treatmentName;
        /**
         *
         */
        protected final Properties propertiesCouvertName = localizationManager
                .newProperties(
                        ValeurQualitative.NAME_ENTITY_JPA,
                        ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        /**
         *
         */
        protected final Properties propertiesParcelleName = localizationManager
                .newProperties(
                        Nodeable.getLocalisationEntite(Parcelle.class),
                        Nodeable.ENTITE_COLUMN_NAME);
        /**
         *
         */
        protected final Properties propertiesSiteName = localizationManager
                .newProperties(
                        Nodeable.getLocalisationEntite(SiteACBB.class),
                        Nodeable.ENTITE_COLUMN_NAME);
        /**
         *
         */
        protected final Properties propertiesTreatmentAffichage = localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA,
                TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE);

        /**
         *
         */
        protected final Properties propertiesTreatmentName = localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA,
                TraitementProgramme.ATTRIBUTE_JPA_NAME);
        /**
         *
         */
        protected final Properties propertiesValeursQualitative = localizationManager
                .newProperties(
                        ValeurQualitative.NAME_ENTITY_JPA,
                        ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        final SortedMap<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, List<M>>>>> sortedValeurs = new TreeMap();

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         */
        public OutputHelper(final PrintStream rawDataPrintStream, final List<Variable> selectedVariables) {
            super();
            this.rawDataPrintStream = rawDataPrintStream;
            for (final Column column : datasetDescriptor.getColumns()) {
                if (RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())
                        || RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE.equals(column
                                .getFlagType())
                        || RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE.equals(column
                                .getFlagType())) {
                    for (final Variable variable : selectedVariables) {
                        if (variable.getAffichage().equals(column.getName())) {
                            this.variablesAffichage.add(variable.getAffichage());
                        }
                    }
                }
            }
        }

        /**
         * @param mesure
         */
        public void addMesure(final M mesure) {
            final T sequence = mesure.getSequence();
            sequence.getVersion();
            SiteACBB site = sequence.getSuiviParcelle().getParcelle().getSite();
            TraitementProgramme treatment = sequence.getSuiviParcelle().getTraitement();
            Parcelle parcelle = sequence.getSuiviParcelle().getParcelle();
            sortedValeurs
                    .computeIfAbsent(site, k -> new TreeMap<>())
                    .computeIfAbsent(treatment, k -> new TreeMap<>())
                    .computeIfAbsent(parcelle, k -> new TreeMap<>())
                    .computeIfAbsent(sequence.getDate(), k -> new LinkedList())
                    .add(mesure);
        }

        /**
         * @param mesure
         */
        protected void addMesureLine(final M mesure) {
            this.printLineGeneric(mesure);
            List<ValeurIntervention> valeurs = new LinkedList();
            valeurs.addAll(mesure.getValeurs());
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            Properties propertiesValeurQualitatives = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, ValeurQualitative.ATTRIBUTE_JPA_VALUE);
            while (itVariablesAffichage.hasNext()) {
                String value = RecorderACBB.PROPERTY_CST_NOT_AVALAIBALE_FIELD;
                variableAffichage = itVariablesAffichage.next();
                for (ValeurIntervention valeurIntervention : new LinkedList<>(valeurs)) {
                    if (variableAffichage.equals(((DatatypeVariableUniteACBB) valeurIntervention.getRealNode().getNodeable())
                            .getVariable().getAffichage())) {
                        final String valeurToString = Utils.createCodeFromString(valeurIntervention.getValeurToString());
                        value = String.format(ACBBMessages.PATTERN_1_FIELD, propertiesValeurQualitatives.getProperty(valeurToString, valeurToString));
                        valeurs.remove(valeurIntervention);
                        break;
                    }
                }
                this.rawDataPrintStream.print(value);
            }
        }

        /**
         * Builds the body.
         *
         * @param mesures
         * @link(List<MesureFluxTours>) the mesures
         */
        public void buildBody(final List<M> mesures) {
            mesures.stream()
                    .sorted((a, b) -> a.getSequence().compareTo(b.getSequence()))
                    .forEach(mesure -> addMesure(mesure));
            for (Iterator<Entry<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, List<M>>>>>> siteIterator = sortedValeurs.entrySet().iterator(); siteIterator.hasNext();) {
                for (Iterator<Entry<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, List<M>>>>> treatmentIterator = siteIterator.next().getValue().entrySet().iterator(); treatmentIterator.hasNext();) {
                    for (Iterator<Entry<Parcelle, SortedMap<LocalDate, List<M>>>> parcelleIterator = treatmentIterator.next().getValue().entrySet().iterator(); parcelleIterator.hasNext();) {
                        for (Iterator<Entry<LocalDate, List<M>>> dateIterator = parcelleIterator.next().getValue().entrySet().iterator(); dateIterator.hasNext();) {
                            for (Iterator<M> mesureIterator = dateIterator.next().getValue().iterator(); mesureIterator.hasNext();) {
                                M mesure = mesureIterator.next();
                                this.addMesureLine(mesure);
                                this.rawDataPrintStream.println();
                                mesureIterator.remove();
                            }
                            dateIterator.remove();
                        }
                        parcelleIterator.remove();
                    }
                    treatmentIterator.remove();
                }
                siteIterator.remove();
            }
        }

        /**
         * Prints the line generic.
         *
         * @param mesure
         */
        protected void printLineGeneric(M mesure) {
            final SuiviParcelle suiviParcelle = mesure.getSequence().getSuiviParcelle();
            final Parcelle parcelle = suiviParcelle.getParcelle();
            TraitementProgramme treatment = suiviParcelle.getTraitement();
            String line;
            final Bloc bloc = parcelle.getBloc();
            final String blocName = parcelle.getBloc() != null ? bloc == null ? StringUtils.EMPTY : bloc.getBlocName()
                    : StringUtils.EMPTY;
            final String repetitionName = parcelle.getBloc() != null ? parcelle.getBloc()
                    .getRepetitionName() : StringUtils.EMPTY;
            final Integer rotation = mesure.getSequence().getVersionTraitementRealiseeNumber();
            final Integer annee = mesure.getSequence().getAnneeRealiseeNumber();
            final Integer periode = mesure.getSequence().getPeriodeRealiseeNumber();
            line = String.format(OutputHelper.PATTERN_11_FIELD,
                    this.propertiesSiteName.getProperty(parcelle.getSite().getName(), parcelle.getSite().getName()),
                    this.propertiesParcelleName.getProperty(parcelle.getName(), parcelle.getName()),
                    blocName,
                    repetitionName,
                    DateUtil.getUTCDateTextFromLocalDateTime(mesure.getSequence().getDate(), DateUtil.DD_MM_YYYY),
                    this.propertiesTreatmentName.getProperty(treatment.getNom(), treatment.getNom()),
                    this.propertiesTreatmentAffichage.getProperty(treatment.getAffichage(), treatment.getAffichage()),
                    DateUtil.getUTCDateTextFromLocalDateTime(suiviParcelle.getDateDebutTraitement(), DateUtil.DD_MM_YYYY),
                    mesure.getSequence().getObservation(),
                    rotation <= 0 ? org.apache.commons.lang.StringUtils.EMPTY : rotation,
                    annee < 0 ? org.apache.commons.lang.StringUtils.EMPTY : annee,
                    periode < 0 ? org.apache.commons.lang.StringUtils.EMPTY : periode);
            this.rawDataPrintStream.print(line);
        }
    }

}
