/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.lai.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractVariableValueBiomasse;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai.MethodeLAI;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

/**
 * @author ptcherniati
 */
public class VariableValueLai extends AbstractVariableValueBiomasse {
    BiomasseEntreeSortie biomasseEntreeSortie;

    /**
     * @param measureNumber
     * @param standardDeviation
     * @param mediane
     * @param qualityIndex
     * @param dvu
     * @param value
     * @param methodeLAI
     * @param biomasseEntreeSortie
     */
    public VariableValueLai(int measureNumber, float standardDeviation, Float mediane,
                            int qualityIndex, DatatypeVariableUniteACBB dvu, String value, MethodeLAI methodeLAI,
                            BiomasseEntreeSortie biomasseEntreeSortie) {
        super(dvu, value, methodeLAI, qualityIndex, measureNumber, 0, qualityIndex);
        this.biomasseEntreeSortie = biomasseEntreeSortie;
        setMediane(mediane);
        setStandardDeviation(standardDeviation);
    }

    /**
     * @return
     */
    public BiomasseEntreeSortie getBiomasseEntreeSortie() {
        return biomasseEntreeSortie;
    }

    /**
     * @param biomasseEntreeSortie
     */
    public void setBiomasseEntreeSortie(BiomasseEntreeSortie biomasseEntreeSortie) {
        this.biomasseEntreeSortie = biomasseEntreeSortie;
    }
}
