package org.inra.ecoinfo.acbb.dataset.itk.impl;


import org.inra.ecoinfo.acbb.dataset.impl.DeleteRecord;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author ptcherniati
 */
public class DefaultITKDeleteRecord extends DeleteRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private ITempoDAO tempoDAO;

    /**
     *
     */
    public DefaultITKDeleteRecord() {
        super();
    }

    /**
     * @param versionFile
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(VersionFile versionFile) throws BusinessException {
        super.deleteRecord(versionFile);
        try {
            this.tempoDAO.deleteUnusedTempos();
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param tempoDAO
     */
    public void setTempoDAO(ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

}
