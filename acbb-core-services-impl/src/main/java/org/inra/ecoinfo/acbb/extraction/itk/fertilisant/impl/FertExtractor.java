package org.inra.ecoinfo.acbb.extraction.itk.fertilisant.impl;

import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKExtractor;

/**
 * @author ptcherniati
 */
public class FertExtractor extends AbstractITKExtractor<Fertilisant> {

}
