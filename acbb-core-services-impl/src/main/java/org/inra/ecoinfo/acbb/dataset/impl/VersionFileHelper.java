/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import org.inra.ecoinfo.acbb.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.DefaultVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;

import java.util.LinkedList;
import java.util.List;

/**
 * The Class VersionFileHelper.
 * <p>
 * used to build a file name from a {@link VersionFile}
 */
public class VersionFileHelper extends DefaultVersionFileHelper {
    private List<String> scheduleDatatypes = new LinkedList<>();

    /**
     * @param scheduleDatatypes
     */
    public void setScheduleDatatypes(List<String> scheduleDatatypes) {
        this.scheduleDatatypes = scheduleDatatypes;
    }

    /**
     * Builds the download filename.
     *
     * @param versionFile
     * @return the string
     * @link(VersionFile) the version file
     * @link(VersionFile) the version file
     * @see org.inra.ecoinfo.dataset.DefaultVersionFileHelper#buildDownloadFilename(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String buildDownloadFilename(final VersionFile versionFile) {
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier
                    .append(ACBBUtils.getSiteFromDataset(versionFile.getDataset())
                            .getPath()
                            .replaceAll(
                                    IVersionFileHelper.FILE_SEPARATOR,
                                    AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR_IN_FILE_NAME))
                    .append(DefaultVersionFileHelper.UNDERSCORE);
            nomFichier.append(ACBBUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode())
                    .append(DefaultVersionFileHelper.UNDERSCORE);
            if (ACBBUtils.getParcelleFromDataset(versionFile.getDataset()) != null) {
                nomFichier.append(ACBBUtils.getParcelleFromDataset(versionFile.getDataset()).getCode()).append(
                        DefaultVersionFileHelper.UNDERSCORE);
            }
            final String dateFormat;
            if (scheduleDatatypes.contains(ACBBUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode())) {
                dateFormat = RecorderACBB.DD_MM_YYYY_HHMMSS_FILE;
            } else {
                dateFormat = DateUtil.DD_MM_YYYY_FILE;
            }
            nomFichier.append(DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateDebutPeriode(), dateFormat))
                    .append(DefaultVersionFileHelper.UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateFinPeriode(), dateFormat))
                    .append(IVersionFileHelper.EXTENSION_FILE_CSV);
        } catch (final Exception e) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        return nomFichier.toString();
    }

}
