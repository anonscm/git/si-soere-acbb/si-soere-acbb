/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.ValeurAI;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.ValeurAI_;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationAIDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        CriteriaDelete<ValeurAI> deleteQueryValeur = builder.createCriteriaDelete(ValeurAI.class);
        Root<ValeurAI> valeur = deleteQueryValeur.from(ValeurAI.class);
        Subquery<AI> subquery = deleteQueryValeur.subquery(AI.class);
        Root<AI> itk = subquery.from(AI.class);
        subquery.select(itk)
                .where(builder.equal(itk.get(AbstractIntervention_.version), version));
        deleteQueryValeur.where(valeur.get(ValeurAI_.ai).in(subquery));
        entityManager.createQuery(deleteQueryValeur).executeUpdate();
        CriteriaDelete<AI> deleteQueryAI = builder.createCriteriaDelete(AI.class);
        itk = deleteQueryAI.from(AI.class);
        deleteQueryAI
                .where(builder.equal(itk.get(AbstractIntervention_.version), version));
        entityManager.createQuery(deleteQueryAI).executeUpdate();
    }

}
