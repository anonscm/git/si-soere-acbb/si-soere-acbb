/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class RecFauBuildOutputDisplay extends
        AbstractITKOutputBuilder<RecFau, ValeurRecFau, RecFau> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(RecFauBuildOutputDisplay.class
            .getName());

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public RecFauBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return RecFauBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedWsolVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream) {
        return new RecFauOutputHelper(rawDataPrintStream,
                selectedWsolVariables);
    }

    class RecFauOutputHelper extends OutputHelper {

        private static final String PATTERN_1_FIELD = ";%d";
        final SortedMap<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<Integer, RecFau>>>>> sortedRecFauValeurs = new TreeMap();

        RecFauOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedWSolVariables) {
            super(rawDataPrintStream, selectedWSolVariables);
        }

        @Override
        public void addMesure(RecFau mesure) {
            final RecFau sequence = mesure;
            Parcelle parcelle = sequence.getSuiviParcelle().getParcelle();
            TraitementProgramme treatment = sequence.getSuiviParcelle().getTraitement();
            SiteACBB site = sequence.getSuiviParcelle().getParcelle().getSite();
            Integer passage = sequence.getPassage();
            if (!this.sortedRecFauValeurs.containsKey(site)) {
                this.sortedRecFauValeurs.put(site, new TreeMap());
            }
            if (!this.sortedRecFauValeurs.get(site).containsKey(treatment)) {
                this.sortedRecFauValeurs.get(site).put(treatment, new TreeMap());
            }
            if (!this.sortedRecFauValeurs.get(site).get(treatment).containsKey(parcelle)) {
                this.sortedRecFauValeurs.get(site).get(treatment).put(parcelle, new TreeMap());
            }
            if (!this.sortedRecFauValeurs.get(site).get(treatment).get(parcelle).containsKey(sequence.getDate())) {
                this.sortedRecFauValeurs.get(site).get(treatment).get(parcelle).put(sequence.getDate(), new TreeMap());
            }
            this.sortedRecFauValeurs.get(site).get(treatment).get(parcelle).get(sequence.getDate()).put(passage, mesure);
        }

        @Override
        public void buildBody(List<RecFau> mesures) {
            for (final RecFau valeur : mesures) {
                this.addMesure(valeur);
            }
            for (final Entry<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<Integer, RecFau>>>>> siteEntry : this.sortedRecFauValeurs.entrySet()) {
                for (final Entry<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<Integer, RecFau>>>> treatmentEntry : siteEntry.getValue().entrySet()) {
                    for (final Entry<Parcelle, SortedMap<LocalDate, SortedMap<Integer, RecFau>>> parcelleEntry : treatmentEntry.getValue().entrySet()) {
                        for (final Entry<LocalDate, SortedMap<Integer, RecFau>> dateEntry : parcelleEntry.getValue().entrySet()) {
                            for (Entry<Integer, RecFau> passageEntry : dateEntry.getValue().entrySet()) {
                                this.addMesureLine(passageEntry.getValue());
                                this.rawDataPrintStream.println();
                            }
                        }
                    }
                }
            }
        }

        @Override
        protected void printLineGeneric(RecFau mesure) {
            super.printLineGeneric(mesure);
            String line = String.format(RecFauOutputHelper.PATTERN_1_FIELD, mesure.getPassage());
            this.rawDataPrintStream.print(line);
        }
    }
}
