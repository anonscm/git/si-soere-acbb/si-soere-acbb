/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.utils.Column;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

// TODO: Auto-generated Javadoc

/**
 * The Class DatasetDescriptorBuilderACBB.
 * <p>
 * build {@link DatasetDescriptorACBB}
 *
 * @author Philippe TCHERNIATINSKY
 */
public class DatasetDescriptorBuilderACBB {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetDescriptorBuilderACBB.class
            .getName());

    /**
     *
     */
    public DatasetDescriptorBuilderACBB() {
        super();
    }

    /**
     * Builds the descriptor acbb.
     *
     * @param datasetDescriptorFile
     * @return the dataset descriptor acbb
     * @throws IOException  Signals that an I/O exception has occurred.
     * @throws SAXException the sAX exception {@link File} the dataset descriptor file
     * @link(File) the dataset descriptor file
     */
    public static final DatasetDescriptorACBB buildDescriptorACBB(final File datasetDescriptorFile)
            throws IOException, SAXException {
        DatasetDescriptorACBB datasetDescriptorACBB;
        final Digester digester = DatasetDescriptorBuilderACBB.buildDigester();

        datasetDescriptorACBB = (DatasetDescriptorACBB) digester.parse(datasetDescriptorFile);

        return datasetDescriptorACBB;
    }

    /**
     * @param datasetDescriptorStream
     * @return
     * @throws IOException
     * @throws SAXException
     */
    public static final DatasetDescriptorACBB buildDescriptorACBB(
            final InputStream datasetDescriptorStream) throws IOException, SAXException {
        DatasetDescriptorACBB datasetDescriptorACBB;
        final Digester digester = DatasetDescriptorBuilderACBB.buildDigester();

        datasetDescriptorACBB = (DatasetDescriptorACBB) digester.parse(datasetDescriptorStream);

        return datasetDescriptorACBB;
    }

    /**
     * @return
     */
    public static Digester buildDigester() {
        final Digester digester = new Digester();
        digester.setNamespaceAware(true);
        digester.setUseContextClassLoader(true);
        digester.addObjectCreate("dataset-descriptor", DatasetDescriptorACBB.class);
        DatasetDescriptorBuilderACBB.setEnTete(digester);
        DatasetDescriptorBuilderACBB.setName(digester);
        DatasetDescriptorBuilderACBB.setColumns(digester);
        DatasetDescriptorBuilderACBB.setSites(digester);
        DatasetDescriptorBuilderACBB.setVariableName(digester);
        DatasetDescriptorBuilderACBB.setUndefinedColumn(digester);
        return digester;
    }

    /**
     * Sets the {@link Column} of the {@link DatasetDescriptorACBB}.
     *
     * @param digester the new columns {@link Digester} the new columns
     */
    public static final void setColumns(final Digester digester) {
        digester.addObjectCreate("dataset-descriptor/column", Column.class);
        digester.addSetNext("dataset-descriptor/column", "addColumn");
        digester.addCallMethod("dataset-descriptor/column/name", "setName", 0);
        digester.addCallMethod("dataset-descriptor/column/not-null", "setNullable", 0);
        digester.addCallMethod("dataset-descriptor/column/value-type", "setValueType", 0);
        digester.addCallMethod("dataset-descriptor/column/variable", "setVariable", 0);
        digester.addCallMethod("dataset-descriptor/column/flag", "setFlag", 0);
        digester.addCallMethod("dataset-descriptor/column/flag-type", "setFlagType", 0);
        digester.addCallMethod("dataset-descriptor/column/ref-variable-name", "setRefVariableName",
                0);
        digester.addCallMethod("dataset-descriptor/column/format-type", "setFormatType", 0);

    }

    /**
     * Sets the header of the {@link DatasetDescriptorACBB}.
     *
     * @param digester the new en tete {@link Digester} the new en tete
     */
    public static final void setEnTete(final Digester digester) {
        digester.addCallMethod("dataset-descriptor/en-tete", "setEnTete", 0);
        digester.addCallMethod("dataset-descriptor/headersLine", "setHeadersLine", 0);
        digester.addCallMethod("dataset-descriptor/minLine", "setMinLine", 0);
        digester.addCallMethod("dataset-descriptor/maxLine", "setMaxLine", 0);
    }

    /**
     * Sets the name.
     *
     * @param digester the new name {@link Digester} the new name
     */
    public static final void setName(final Digester digester) {
        digester.addCallMethod("dataset-descriptor/name", "setName", 0);
    }

    /**
     * Sets the sites of the {@link DatasetDescriptorACBB}.
     *
     * @param digester the new sites {@link Digester} the new sites
     */
    public static void setSites(final Digester digester) {
        digester.addCallMethod("dataset-descriptor/sites", "setSites", 0);
    }

    /**
     * Sets the undefined column.
     *
     * @param digester the new undefined column
     */
    static void setUndefinedColumn(final Digester digester) {
        digester.addCallMethod("dataset-descriptor/undefined-column", "setUndefinedColumn", 0);
    }

    /**
     * Sets the variable name.
     *
     * @param digester the new variable name
     */
    static void setVariableName(final Digester digester) {
        digester.addCallMethod("dataset-descriptor/variableName", "setVariableName", 0);
    }

    /**
     * Skip header flux.
     *
     * @param bufferedReader
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(BufferedReader) the buffered reader
     * @link(BufferedReader) the buffered reader
     */
    static void skipHeaderFlux(final BufferedReader bufferedReader) throws IOException {
        for (int i = 0; i < 3; i++) {
            bufferedReader.readLine();
        }

    }

}
