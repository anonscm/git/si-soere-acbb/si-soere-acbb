/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.autreintervention.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.ValeurAI;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.ValeurAI_;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.autreintervention.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
@SuppressWarnings("unchecked")
public class JPAAIExtractionDAO extends AbstractJPAITKDAO<AI, AI, ValeurAI> {

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<AI> getMesureITKClass() {
        return AI.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurAI> getValeurITKClass() {
        return ValeurAI.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, AI> getMesureAttribute() {
        return ValeurAI_.ai;
    }
}
