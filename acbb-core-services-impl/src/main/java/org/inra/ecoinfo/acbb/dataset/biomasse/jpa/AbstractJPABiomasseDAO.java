package org.inra.ecoinfo.acbb.dataset.biomasse.jpa;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse_;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse_;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.jpa.AbstractACBBExtractionJPA;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * @param <T>
 * @param <V>
 * @author ptcherniati
 */
public abstract class AbstractJPABiomasseDAO<T extends AbstractBiomasse, V extends ValeurBiomasse> extends AbstractACBBExtractionJPA<T>
        implements IBiomasseExtractionDAO<T> {

    /**
     * @param selectedTraitementProgrammes
     * @param selectedVariables
     * @param intervals
     * @param user
     * @return
     */
    @Override
    public List<T> extractBiomasseValues(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user) {
        CriteriaQuery criteria = getQuery(intervals, user, selectedVariables, selectedTraitementProgrammes, false);
        return getResultList(criteria);
    }

    @Override
    public Long getSize(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user) {
        CriteriaQuery<Long> criteria = getQuery(intervals, user, selectedVariables, selectedTraitementProgrammes, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;
    }

    private CriteriaQuery getQuery(List<IntervalDate> intervals, IUser user, List<Variable> selectedVariables, List<TraitementProgramme> selectedTraitementProgrammes, boolean isCount) throws DateTimeParseException {
        CriteriaQuery criteria;
        if (isCount) {
            criteria = builder.createQuery(Long.class);
        } else {
            criteria = builder.createQuery(getBiomassClass());
        }
        Root<V> v = criteria.from(getValeurBiomassClass());
        Root<DatatypeVariableUniteACBB> dvu = criteria.from(DatatypeVariableUniteACBB.class);
        Join<V, T> s = v.join(getMesureAttribute());
        Join<V, RealNode> rn = v.join(ValeurBiomasse_.realNode);
        Join<RealNode, Nodeable> dvuNodeable = rn.join(RealNode_.nodeable);
        List<Predicate> and = new LinkedList();
        List<Predicate> or = new LinkedList();
        Root<NodeDataSet> nds = null;
        for (IntervalDate intervalDate : intervals) {
            List<Predicate> andForDate = new LinkedList();
            LocalDate minDate, maxDate;
            minDate = intervalDate.getBeginDate().toLocalDate();
            maxDate = intervalDate.getEndDate().toLocalDate();
            andForDate.add(builder.between(s.get(AbstractBiomasse_.date), minDate, maxDate));
            if (!(isCount || user.getIsRoot())) {
                nds = nds == null ? criteria.from(NodeDataSet.class) : nds;
                andForDate.add(builder.equal(nds.join(NodeDataSet_.realNode), rn));
                addRestrictiveRequestOnRoles(user, criteria, andForDate, builder, nds, s.get(AbstractBiomasse_.date));
            }
            or.add(builder.and(andForDate.toArray(new Predicate[andForDate.size()])));
        }
        and.add(builder.or(or.toArray(new Predicate[or.size()])));
        and.add(dvu.join(DatatypeVariableUniteACBB_.variable).in(selectedVariables));
        and.add(builder.equal(dvu, dvuNodeable));
        and.add(s.join(AbstractBiomasse_.suiviParcelle).join(SuiviParcelle_.traitement).in(selectedTraitementProgrammes));
        criteria.where(builder.and(and.toArray(new Predicate[and.size()])));
        criteria.distinct(true);
        if (isCount) {
            criteria.select(builder.countDistinct(s));
        } else {
            criteria.select(s);
        }
        return criteria;
    }

    /**
     * @return
     */
    abstract protected Class<V> getValeurBiomassClass();

    /**
     * @return
     */
    abstract protected Class<T> getBiomassClass();

    /**
     * @return
     */
    abstract protected SingularAttribute<V, T> getMesureAttribute();

    /**
     * @return
     */
    abstract protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass();

    @Override
    protected boolean isParcelleDatatype() {
        return false;
    }

}
