/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisAttrDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemisAttr;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * The Class JPAValeurSemisAttrDAO.
 */
public class JPAValeurSemisAttrDAO extends AbstractJPADAO<ValeurSemisAttr> implements
        IValeurSemisAttrDAO {
    /**
     * The Constant SQL_GET_DELETE_VALEURS_SEMIS_ATTR @link(String).
     */
    static final String SQL_GET_DELETE_VALEURS_SEMIS_ATTR = "delete from ValeurSemisAttr vsa where vsa in (:vsa)";

    /**
     * @param valeurSemisAttrsToDelete
     * @see org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisAttrDAO#deleteAll(java.util.List)
     */
    @Override
    public void deleteAll(List<ValeurSemisAttr> valeurSemisAttrsToDelete) {
        CriteriaDelete<ValeurSemisAttr> delete = builder.createCriteriaDelete(ValeurSemisAttr.class);
        Root<ValeurSemisAttr> valeur = delete.from(ValeurSemisAttr.class);
        delete
                .where(valeur.in(valeurSemisAttrsToDelete));

        delete(delete);
    }
}
