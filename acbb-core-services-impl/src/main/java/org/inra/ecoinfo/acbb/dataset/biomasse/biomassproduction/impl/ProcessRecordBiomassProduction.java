package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractProcessRecordBiomasse;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.IMasseVegClassDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.IMethodeAnalyseDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.MethodeAnalyse;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.IMethodeProductionDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.MethodeProduction;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * @author vivianne
 */
public class ProcessRecordBiomassProduction extends AbstractProcessRecordBiomasse {


    /**
     *
     */
    static final public String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl.messages";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_MASS_VEG_CLASS = "PROPERTY_MSG_BAD_MASS_VEG_CLASS";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_NUMBER_FOR_METHOD = "PROPERTY_MSG_BAD_NUMBER_FOR_METHOD";

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_METHOD = "PROPERTY_MSG_BAD_METHOD";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant SEMIS_BUNDLE_NAME.
     */
    private IMasseVegClassDAO masseVegClassDAO;
    private IMethodeProductionDAO methodeProductionDAO;
    private IMethodeAnalyseDAO methodeAnalyseDAO;

    /**
     *
     */
    public ProcessRecordBiomassProduction() {
        super();
        logger = LoggerFactory.getLogger(getClass());
    }

    /**
     * @param version
     * @param errorsReport
     * @param lines
     * @param requestPropertiesBiomasse
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractProcessRecordBiomasse#buildNewLines(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.util.List, org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse)
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    protected void buildNewLines(
            VersionFile version,
            @SuppressWarnings("rawtypes") List<? extends AbstractLineRecord> lines,
            ErrorsReport errorsReport, IRequestPropertiesBiomasse requestPropertiesBiomasse)
            throws PersistenceException {
        final BuildBiomassProductionHelper instance = ApplicationContextHolder.getContext()
                .getBean(BuildBiomassProductionHelper.class);
        instance.build(version, (List<BiomassProductionLineRecord>) lines, errorsReport,
                requestPropertiesBiomasse);
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param fileEncoding
     * @param requestProperties
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile localVersionFile = versionFile;
        super.processRecord(parser, localVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesBiomasse) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            localVersionFile = this.versionFileDAO.merge(localVersionFile);
            final List<BiomassProductionLineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, intervalDate, (IRequestPropertiesBiomasse) requestProperties,
                    datasetDescriptor, errorsReport, lineCount, dbVariables, lines);
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(localVersionFile, lines, errorsReport,
                        (IRequestPropertiesBiomasse) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            /**
             * e.printStackTrace();
             */
            throw new BusinessException(e);

        }
    }

    /**
     * @param cleanerValues
     * @param line
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param datasetDescriptor
     * @param requestProperties
     * @param values
     * @return
     */
    protected int readDateDebutrepousse(CleanerValues cleanerValues,
                                        BiomassProductionLineRecord line, ErrorsReport errorsReport, long lineCount, int index,
                                        DatasetDescriptorACBB datasetDescriptor, IRequestPropertiesACBB requestProperties,
                                        String[] values) {
        if (Strings.isNullOrEmpty(values[index])) {
            try {
                cleanerValues.nextToken();
            } catch (EndOfCSVLine ex) {
                errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
            }
            return index + 1;
        }
        LocalDate dateDebutRepousse = this.getDate(errorsReport, lineCount, index, cleanerValues,
                datasetDescriptor);
        line.setDateDebutRepousse(dateDebutRepousse);
        return index + 1;
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param requestProperties
     * @param datasetDescriptor
     * @param errorsReport
     * @param lineCount
     * @param dbVariables
     * @param lines
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{IRequestPropertiesACBB the request properties
     * @link{DatasetDescriptorACBB the dataset descriptor
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{java.util.List<VariableACBB> the db variables
     * @link{java.util.List<AILineRecord> the lines
     */
    long readLines(final CSVParser parser, IntervalDate intervalDate, final IRequestPropertiesBiomasse requestProperties,
                   final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                   long lineCount, final List<DatatypeVariableUniteACBB> dbVariables,
                   final List<BiomassProductionLineRecord> lines) throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            int index = 0;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final BiomassProductionLineRecord lineRecord = new BiomassProductionLineRecord(
                    localLineCount);
            index = this.readGenericColumns(errorsReport, localLineCount, cleanerValues,
                    lineRecord, datasetDescriptor, requestProperties);
            index = this.readMasseVegClass(cleanerValues, lineRecord, errorsReport, localLineCount,
                    index);
            index = this.readNatureCouvert(cleanerValues, lineRecord, errorsReport, localLineCount,
                    index);
            index = this.readDateAndSetVersionTraitement(intervalDate, lineRecord, errorsReport, localLineCount,
                    index, cleanerValues, datasetDescriptor, requestProperties);
            index = this.readDateDebutrepousse(cleanerValues, lineRecord, errorsReport,
                    localLineCount, index, datasetDescriptor, requestProperties, values);
            while (index < values.length) {
                index = this.readVariableValueBiomassProduction(lineRecord, errorsReport,
                        localLineCount, index, cleanerValues, datasetDescriptor, requestProperties,
                        dbVariables);
            }
            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }

    /**
     * @param cleanerValues
     * @param line
     * @param errorsReport
     * @param lineCount
     * @param index
     * @return
     */
    protected int readMasseVegClass(CleanerValues cleanerValues, BiomassProductionLineRecord line,
                                    ErrorsReport errorsReport, long lineCount, int index) {
        String massevegString = null;
        try {
            massevegString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        MasseVegClass masseVegClass = this.masseVegClassDAO.getByNKey(massevegString).orElse(null);
        if (masseVegClass == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordBiomassProduction.BUNDLE_NAME,
                    ProcessRecordBiomassProduction.PROPERTY_MSG_BAD_MASS_VEG_CLASS), lineCount,
                    index + 1, massevegString));

        } else {
            line.setMasseVegClass(masseVegClass);
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readMethod(final ErrorsReport errorsReport, final long lineCount, int index,
                             final CleanerValues cleanerValues, final VariableValueBiomassProduction variableValue) {
        String methodString = null;
        try {
            methodString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            Long methodNumber = Long.parseLong(methodString);
            MethodeAnalyse methodeAnalyse = this.methodeAnalyseDAO.getByNumberId(methodNumber).orElse(null);
            MethodeProduction methodeProduction = this.methodeProductionDAO.getByNumberId(methodNumber).orElse(null);
            if (methodeAnalyse == null && methodeProduction == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        ProcessRecordBiomassProduction.BUNDLE_NAME,
                        ProcessRecordBiomassProduction.PROPERTY_MSG_BAD_METHOD), lineCount,
                        index + 1, methodString));
            } else {
                AbstractMethode methode = methodeAnalyse == null ? methodeProduction : methodeAnalyse;
                variableValue.setMethode(methode);
            }
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordBiomassProduction.BUNDLE_NAME,
                    ProcessRecordBiomassProduction.PROPERTY_MSG_BAD_NUMBER_FOR_METHOD), lineCount,
                    index + 1, methodString));
        }
        return index + 1;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     * @param dbVariables
     * @return
     */
    protected int readVariableValueBiomassProduction(final BiomassProductionLineRecord lineRecord,
                                                     final ErrorsReport errorsReport, final long lineCount, int index,
                                                     final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                                                     final IRequestPropertiesBiomasse requestPropertiesBiomasse,
                                                     List<DatatypeVariableUniteACBB> dbVariables) {
        int localIndex = index;
        DatatypeVariableUniteACBB dvu = dbVariables.get(index);
        VariableValueBiomassProduction variableValue = new VariableValueBiomassProduction(
                localIndex, 0F, null, 0, null, dvu, null);
        localIndex = this.readValue(cleanerValues, variableValue, errorsReport, lineCount, localIndex);
        if (Strings.isNullOrEmpty(variableValue.getValue())) {
            return localIndex + 4;
        }
        localIndex = this.readMeasureNumber(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readStandardDeviation(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readMethod(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readQualityIndex(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        lineRecord.getVariablesValues().add(variableValue);
        return localIndex;
    }

    /**
     * @param masseVegClassDAO
     */
    public void setMasseVegClassDAO(IMasseVegClassDAO masseVegClassDAO) {
        this.masseVegClassDAO = masseVegClassDAO;
    }

    /**
     * @param methodeAnalyseDAO
     */
    public void setMethodeAnalyseDAO(IMethodeAnalyseDAO methodeAnalyseDAO) {
        this.methodeAnalyseDAO = methodeAnalyseDAO;
    }

    /**
     * @param methodeProductionDAO
     */
    public void setMethodeProductionDAO(IMethodeProductionDAO methodeProductionDAO) {
        this.methodeProductionDAO = methodeProductionDAO;
    }

}
