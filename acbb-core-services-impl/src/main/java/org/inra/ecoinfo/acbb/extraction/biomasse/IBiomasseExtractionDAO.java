package org.inra.ecoinfo.acbb.extraction.biomasse;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * @author vkoyao
 */

/**
 * The Interface IBiomasseExtractionDAO.
 *
 * @param <T> the generic type
 */
public interface IBiomasseExtractionDAO<T extends AbstractBiomasse> extends IDAO<T> {

    /**
     * Extract Biomasse mesures.
     *
     * @param selectedTraitementProgrammes
     * @param user
     * @link(List<TraitementProgramme>) the selected traitement programmes
     * @param selectedVariables
     * @link(List<Variable>) the selected variables
     * @param intervalsDates
     * @link(List<IntervalDate>) the intervals dates
     * @return the list
     */
    List<T> extractBiomasseValues(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervalsDates, IUser user);

    Long getSize(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user);

    /**
     * Gets the availables variables by sites.
     *
     * @param linkedList
     * @param intervals
     * @param user
     * @link(List<SiteACBB>) the linked list
     * @return the availables variables by sites
     */
    List<NodeDataSet> getAvailableVariableByTraitement(List<TraitementProgramme> linkedList, List<IntervalDate> intervals, IUser user);

    /**
     * Gets the available traitements.
     *
     * @param intervalsDate
     * @param user
     * @link(List<IntervalDate>) the intervals date
     * @return the available traitements
     * @throws SecurityException
     */
    List<TraitementProgramme> getAvailablesTraitementsForPeriode(List<IntervalDate> intervalsDate, IUser user);

}
