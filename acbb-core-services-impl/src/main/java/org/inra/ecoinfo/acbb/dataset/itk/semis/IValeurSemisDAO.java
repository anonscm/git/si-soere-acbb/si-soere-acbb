/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemis;

/**
 * The Interface IValeurSemisDAO.
 */
public interface IValeurSemisDAO extends IDAO<ValeurSemis> {
}
