/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.site;

import org.inra.ecoinfo.refdata.site.ISiteDAO;

import java.util.List;
import java.util.Optional;

/**
 * The Interface ISiteACBBDAO.
 */
public interface ISiteACBBDAO extends ISiteDAO {

    /**
     * Gets the all sites acbb.
     *
     * @return the all sites acbb
     */
    List<SiteACBB> getAllSitesACBB();

    /**
     * Gets the by agro id.
     *
     * @param id the id
     * @return the by agro id
     */
    List<SiteACBB> getByAgroId(Long id);

    /**
     * Gets the by short name.
     *
     * @param code the code
     * @return the by short name
     */
    Optional<SiteACBB> getByShortName(String code);
}
