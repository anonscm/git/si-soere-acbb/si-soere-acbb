package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;

import java.util.Optional;

/**
 * The Interface IInterventionDAO.
 *
 * @param <T> the generic type
 */
public interface IInterventionForNumDAO<T extends AbstractIntervention> extends IInterventionDAO<T> {

    /**
     * @param interventionType
     * @return
     */
    boolean hasType(InterventionType interventionType);

    /**
     * @param parcelle
     * @param num
     * @param rotationNumber
     * @param anneeNumber
     * @param valeur
     * @return
     */
    Optional<T> getinterventionForNumYearAndType(Parcelle parcelle, int num,
                                                 Integer rotationNumber, Integer anneeNumber, String valeur);

}
