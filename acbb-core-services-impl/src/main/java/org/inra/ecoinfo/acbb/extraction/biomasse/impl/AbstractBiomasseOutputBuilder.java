package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.ComparatorWithFirstStringThenAlpha;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.IMesureBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;
import org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorBuilderACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKDatasetManager;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.VariableDescriptor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

/**
 * @param <T>
 * @param <V>
 * @param <M>
 * @author vkoyao
 */
public abstract class AbstractBiomasseOutputBuilder<T extends AbstractBiomasse, V extends ValeurBiomasse, M extends IMesureBiomasse<T, V>>
        extends AbstractOutputBuilder {

    /**
     *
     */
    public static final String DATASET_DESCRIPTOR_PATH_PATTERN = "org/inra/ecoinfo/acbb/dataset/%s";

    /**
     *
     */
    public static final String COLUMN_VALUE = "valeur";

    /**
     *
     */
    public static final String COLUMN_AVERAGE = "moyenne";

    /**
     *
     */
    public static final String COLUMN_MEDIAN = "mediane";

    /**
     *
     */
    public static final String COLUMN_STANDARD_DEVIATION = "et";
    /**
     *
     */
    public static final String PATTERN_CSV_VARIABLE_FIELD = ";%s(%s)";
    /**
     * The Constant HEADER_RAW_DATA @link(String).
     */
    protected static final String PROPERTY_HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";
    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_COLUMN_NAME = "PROPERTY_HEADER_RAW_DATA_COLUMN_NAME";
    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.acbb.dataset.biomasse.impl.messages";
    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION = "PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION";
    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION_DATE = "PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION_DATE";
    /**
     *
     */
    protected static final String PROPERTY_HEADER_RAW_DATA_COLUMN_NAME_FOR_OBSERVATION_DATE = "PROPERTY_HEADER_RAW_DATA_COLUMN_NAME_FOR_OBSERVATION_DATE";
    /*
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.biomasse.messages";
    /**
     * The Constant MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS @link(String).
     */
    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNKNOWN_VARIABLE";
    /**
     * The Constant PATTERN_END_LINE @link(String).
     */
    static final String PATTERN_END_LINE = "\n";
    /**
     * The Constant REGEX_CSV_FIELD @link(String).
     */
    static final String REGEX_CSV_FIELD = "[^;]";
    /**
     * The Constant DATASET_DESCRIPTOR_XML @link(String).
     */
    static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";
    private static final String CST_EXTRACT = "EXTRACT_";
    private final List<String> variableColumnTypes = Arrays.asList(AbstractBiomasseOutputBuilder.COLUMN_VALUE,
            AbstractBiomasseOutputBuilder.COLUMN_AVERAGE,
            AbstractBiomasseOutputBuilder.COLUMN_MEDIAN,
            AbstractBiomasseOutputBuilder.COLUMN_STANDARD_DEVIATION);
    /**
     *
     */
    protected IListeVegetationDAO listeBioassedAO;
    /**
     *
     */
    protected IITKDatasetManager itkDatasetManager;
    /**
     * The variable dao @link(IVariableACBBDAO).
     */
    protected IVariableACBBDAO variableDAO;
    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     * The variables descriptor @link(Map<String,VariableDescriptor>).
     */
    Map<String, VariableDescriptor> variablesDescriptor = new HashMap();
    private Map<String, String> columnPatterns = null;
    /**
     * The dataset descriptor @link(DatasetDescriptor).
     */
    private DatasetDescriptor datasetDescriptor;
    private String resultCode = org.apache.commons.lang.StringUtils.EMPTY;
    private String extractCode = org.apache.commons.lang.StringUtils.EMPTY;
    private ISuiviParcelleDAO suiviParcelleDAO;

    /**
     * @param datasetDescriptorPath
     */
    public AbstractBiomasseOutputBuilder(String datasetDescriptorPath) {
        super();
        try {
            if (this.datasetDescriptor == null) {
                this.setDatasetDescriptor(this.getDatasetDescriptor(datasetDescriptorPath));
                for (final Column column : this.datasetDescriptor.getColumns()) {
                    if (null != column.getFlagType()) {
                        switch (column.getFlagType()) {
                            case RecorderACBB.PROPERTY_CST_VARIABLE_TYPE:
                            case RecorderACBB.PROPERTY_CST_GAP_FIELD_TYPE:
                                this.variablesDescriptor.put(column.getName(),
                                        new VariableDescriptor(column.getName()));
                                break;
                            case RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE:
                                this.variablesDescriptor.get(column.getRefVariableName())
                                        .setHasQualityClass(true);
                                break;
                        }
                    }
                }
            }
        } catch (final IOException | SAXException | URISyntaxException ex) {
            AbstractOutputBuilder.LOGGER.debug(ex.getMessage(), ex);
        }
    }

    /**
     * @return
     */
    protected abstract String getFirstVariable();

    private void addColumnName(Variable variable, String columnType,
                               List<ListeVegetation> valueColumns, StringBuilder stringBuilder1,
                               StringBuilder stringBuilder2, StringBuilder stringBuilder3,
                               Properties propertiesVariableName, Properties propertiesUniteName,
                               Properties value) throws BusinessException {
        String localizedVariableName = propertiesVariableName.getProperty(variable.getName(),
                variable.getName());
        final Unite unite = this.datatypeVariableUniteACBBDAO.getUnite(this.getExtractCode(),
                variable.getCode())
                .orElseThrow(() -> new BusinessException("bas unit"));
        for (ListeVegetation valueColumn : valueColumns) {
            if (Utils.createCodeFromString(valueColumn.getCode()).endsWith(Utils.createCodeFromString(columnType))) {
                String columnDefinition = value.getProperty(valueColumn.getValeur(), valueColumn.getValeur());
                String columnName = String.format("%s_%s", variable.getAffichage(), columnType);
                String uniteName = org.apache.commons.lang.StringUtils.EMPTY;
                if (this.variableColumnTypes.contains(columnType)) {
                    uniteName = propertiesUniteName.getProperty(unite.getName(), unite.getName());
                }
                stringBuilder1.append(String.format(
                        AbstractBiomasseOutputBuilder.PATTERN_CSV_VARIABLE_FIELD,
                        localizedVariableName, columnDefinition));
                stringBuilder2.append(String.format(
                        ACBBMessages.PATTERN_1_FIELD, uniteName));
                stringBuilder3.append(String.format(
                        ACBBMessages.PATTERN_1_FIELD, columnName));
                return;
            }
        }
    }

    /**
     * @param stringBuilder1
     * @param stringBuilder2
     * @param stringBuilder3
     */
    protected void addGenericColumns(final StringBuilder stringBuilder1,
                                     final StringBuilder stringBuilder2, final StringBuilder stringBuilder3) {
        stringBuilder1.append(this.getLocalizationManager().getMessage(
                AbstractBiomasseOutputBuilder.BUNDLE_PATH,
                AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA));
        stringBuilder2.append(this
                .getLocalizationManager()
                .getMessage(AbstractBiomasseOutputBuilder.BUNDLE_PATH,
                        AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA)
                .replaceAll(AbstractBiomasseOutputBuilder.REGEX_CSV_FIELD,
                        org.apache.commons.lang.StringUtils.EMPTY));
        stringBuilder3.append(this.getLocalizationManager().getMessage(
                AbstractBiomasseOutputBuilder.BUNDLE_PATH,
                AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA_COLUMN_NAME));
    }

    /**
     * @param stringBuilder1
     * @param stringBuilder2
     * @param stringBuilder3
     */
    protected void addObservation(final StringBuilder stringBuilder1,
                                  final StringBuilder stringBuilder2, final StringBuilder stringBuilder3) {
        stringBuilder1.append(this.getLocalizationManager().getMessage(
                AbstractBiomasseOutputBuilder.BUNDLE_PATH,
                AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION));
        stringBuilder2.append(";");
        stringBuilder3.append(this.getLocalizationManager().getMessage(
                AbstractBiomasseOutputBuilder.BUNDLE_PATH,
                AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA_FOR_OBSERVATION));
    }

    /**
     * @param stringBuilder1
     * @param stringBuilder2
     * @param stringBuilder3
     */
    protected void addSpecificColumns(StringBuilder stringBuilder1, StringBuilder stringBuilder2,
                                      StringBuilder stringBuilder3) {
        stringBuilder1.append(this.getLocalizationManager().getMessage(this.getBundleSourcePath(),
                AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA));
        stringBuilder2.append(this
                .getLocalizationManager()
                .getMessage(this.getBundleSourcePath(),
                        AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA)
                .replaceAll(AbstractBiomasseOutputBuilder.REGEX_CSV_FIELD,
                        org.apache.commons.lang.StringUtils.EMPTY));
        stringBuilder3.append(this.getLocalizationManager().getMessage(this.getBundleSourcePath(),
                AbstractBiomasseOutputBuilder.PROPERTY_HEADER_RAW_DATA_COLUMN_NAME));
    }

    /**
     * @param headers
     * @param requestMetadatasMap
     * @param resultsDatasMap
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String,
     * java.util.Map, java.util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                .get(DatesFormParamVO.class.getSimpleName());
        final List<Variable> selectedVegetalVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts = (Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>>) requestMetadatasMap
                .getOrDefault(Parcelle.class.getSimpleName(), new HashMap());
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(this.getExtractCode());
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            outputPrintStreamMap.get(datatypeNameEntry.getKey()).println(headers);
        }
        final PrintStream rawDataPrintStream = outputPrintStreamMap.get(this.getExtractCode());
        final OutputHelper outputHelper = this.getOutPutHelper(datesYearsContinuousFormParamVO,
                selectedVegetalVariables, availablesCouverts, rawDataPrintStream);
        final List<M> mesureHauteurVegetal = resultsDatasMap.get(AbstractOutputBuilder.MAP_INDEX_0);
        outputHelper.buildBody(mesureHauteurVegetal, requestMetadatasMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Properties propertiesVariableName = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(VariableACBB.class),
                Nodeable.ENTITE_COLUMN_NAME);
        final Properties propertiesUniteName = this.localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME);
        final Properties propertiesValeurQualitativeValue = this.localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        final ErrorsReport errorsReport = new ErrorsReport();
        final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
        final SortedMap<String, Variable> selectedVariablesAffichage = new TreeMap(new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
        for (final Variable variable : selectedVariables) {
            selectedVariablesAffichage.put(variable.getAffichage(), variable);
        }
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        List<ListeVegetation> valueColumns = this.getValueColumns();
        try {
            this.addGenericColumns(stringBuilder1, stringBuilder2, stringBuilder3);
            this.addSpecificColumns(stringBuilder1, stringBuilder2, stringBuilder3);
            for (Entry<String, Variable> variableEntry : selectedVariablesAffichage.entrySet()) {
                if (variableEntry.getValue() == null) {
                    errorsReport
                            .addErrorMessage(String
                                    .format(this
                                                    .getLocalizationManager()
                                                    .getMessage(
                                                            AbstractBiomasseOutputBuilder.BUNDLE_SOURCE_PATH,
                                                            AbstractBiomasseOutputBuilder.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                            selectedVariables));
                    continue;
                }
                for (Entry<String, String> patternEntry : this.columnPatterns.entrySet()) {
                    String columnType = patternEntry.getKey();
                    this.addColumnName(variableEntry.getValue(), columnType, valueColumns, stringBuilder1,
                            stringBuilder2, stringBuilder3, propertiesVariableName,
                            propertiesUniteName, propertiesValeurQualitativeValue);
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
            this.addObservation(stringBuilder1, stringBuilder2, stringBuilder3);
        } catch (final PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder1.append(AbstractBiomasseOutputBuilder.PATTERN_END_LINE)
                .append(stringBuilder2).append(AbstractBiomasseOutputBuilder.PATTERN_END_LINE)
                .append(stringBuilder3).toString();
    }

    /**
     * @param parameters
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        if (!((DefaultParameter) parameters).getResults().containsKey(this.getResultCode())
                || ((DefaultParameter) parameters).getResults().get(this.getResultCode())
                .get(this.getExtractCode()) == null
                || ((DefaultParameter) parameters).getResults().get(this.getResultCode())
                .get(this.getExtractCode()).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, this.getResultCode()));
        return null;
    }

    /**
     * @return
     */
    protected abstract String getBundleSourcePath();

    /**
     * @return
     */
    public Map<String, String> getColumnPatterns() {
        return this.columnPatterns;
    }

    /**
     * @param columnPatterns
     */
    public void setColumnPatterns(Map<String, String> columnPatterns) {
        this.columnPatterns = columnPatterns;
    }

    private DatasetDescriptorACBB getDatasetDescriptor(String datasetDescriptorPath)
            throws IOException, SAXException, URISyntaxException {
        final String path = String.format(
                AbstractBiomasseOutputBuilder.DATASET_DESCRIPTOR_PATH_PATTERN,
                datasetDescriptorPath);
        return DatasetDescriptorBuilderACBB.buildDescriptorACBB(this.getClass().getClassLoader().getResourceAsStream(path));
    }

    /**
     * @return
     */
    protected String getExtractCode() {
        return this.extractCode;
    }

    /**
     * @param extractCode the extractCode to set
     */
    public void setExtractCode(String extractCode) {
        this.extractCode = extractCode;
        this.resultCode = new StringBuffer(AbstractBiomasseOutputBuilder.CST_EXTRACT).append(
                extractCode).toString();
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    protected abstract OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedWsolVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream);

    /**
     * @return
     */
    protected String getResultCode() {
        return this.resultCode;
    }

    /**
     * @param parcelle
     * @param date
     * @return
     */
    public Optional<SuiviParcelle> getSuiviParcelle(Parcelle parcelle, LocalDate date) {
        return this.suiviParcelleDAO.retrieveSuiviParcelle(parcelle, date);
    }

    /**
     * @return
     */
    protected List<ListeVegetation> getValueColumns() {
        return this.listeBioassedAO.getValueColumns();
    }

    /**
     * Sets the dataset descriptor {@link DatasetDescriptor}.
     *
     * @param datasetDescriptor the new dataset descriptor
     *                          {@link DatasetDescriptor}
     */
    void setDatasetDescriptor(final DatasetDescriptor datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteDAO the new datatype unite variable acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteDAO;
    }

    /**
     * @param itkDatasetManager
     */
    public void setItkDatasetManager(IITKDatasetManager itkDatasetManager) {
        this.itkDatasetManager = itkDatasetManager;
    }

    /**
     * @param listeBioassedAO
     */
    public void setListeVegetationDAO(IListeVegetationDAO listeBioassedAO) {
        this.listeBioassedAO = listeBioassedAO;
    }

    /**
     * @param suiviParcelleDAO
     */
    public void setSuiviParcelleDAO(ISuiviParcelleDAO suiviParcelleDAO) {
        this.suiviParcelleDAO = suiviParcelleDAO;
    }

    /**
     * Sets the variable dao {@link IVariableACBBDAO}.
     *
     * @param variableDAO the new variable dao {@link IVariableACBBDAO}
     */
    public void setVariableDAO(final IVariableACBBDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     * The Class OutputHelper.
     */
    public abstract class OutputHelper {

        /**
         *
         */
        protected static final String PATTERN_DATE_YYYY = "YYYY";
        /**
         * The Constant PATTERN_5_FIELD @link(String).
         */
        static final String PATTERN_11_FIELD = "%s;%s(%s);%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         * The raw data print stream @link(PrintStream).
         */
        final protected PrintStream rawDataPrintStream;
        /**
         * The variables affichage @link(List).
         */
        protected final List<String> variablesAffichage = new LinkedList();

        /**
         *
         */
        protected final Properties propertiesCouvertName = localizationManager.newProperties(
                ValeurQualitative.NAME_ENTITY_JPA,
                ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        /**
         *
         */
        protected final Properties propertiesParcelleName = localizationManager.newProperties(
                Nodeable.getLocalisationEntite(Parcelle.class),
                Nodeable.ENTITE_COLUMN_NAME);
        /**
         *
         */
        protected final Properties propertiesSiteName = localizationManager.newProperties(
                Nodeable.getLocalisationEntite(SiteACBB.class),
                Nodeable.ENTITE_COLUMN_NAME);
        /**
         *
         */
        protected final Properties propertiesTreatmentName = localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA,
                TraitementProgramme.ATTRIBUTE_JPA_NAME);

        /**
         *
         */
        protected final Properties propertiesTreatmentAffichage = localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA,
                TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE);
        /**
         *
         */
        protected final Properties propertiesValeursQualitative = localizationManager.newProperties(
                ValeurQualitative.NAME_ENTITY_JPA,
                ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        final SortedMap<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, List<M>>>>> sortedValeurs = new TreeMap();
        Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts;

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         * @param availablesCouverts
         */
        public OutputHelper(final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts, final PrintStream rawDataPrintStream, final List<Variable> selectedVariables) {
            super();
            this.availablesCouverts = availablesCouverts;
            this.rawDataPrintStream = rawDataPrintStream;
            for (final Variable variable : selectedVariables) {
                this.variablesAffichage.add(variable.getAffichage());
            }
        }

        /**
         * @param mesure
         */
        public void addMesure(final M mesure) {
            final T sequence = mesure.getSequence();
            sequence.getVersion();
            final SiteACBB site = sequence.getSuiviParcelle().getParcelle().getSite();
            final TraitementProgramme treatment = sequence.getSuiviParcelle().getTraitement();
            final Parcelle parcelle = sequence.getSuiviParcelle().getParcelle();
            sortedValeurs
                    .computeIfAbsent(site, k -> new TreeMap<>())
                    .computeIfAbsent(treatment, k -> new TreeMap<>())
                    .computeIfAbsent(parcelle, k -> new TreeMap<>())
                    .computeIfAbsent(sequence.getDate(), k -> new LinkedList())
                    .add(mesure);
        }

        /**
         * Builds the body.
         *
         * @param mesures
         * @param requestMap
         * @link(List<MesureFluxTours>) the mesures
         */
        public void buildBody(final List<M> mesures, Map<String, Object> requestMap) {
            for (final M mesure : mesures) {
                this.addMesure(mesure);
            }
            for (Iterator<Entry<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, List<M>>>>>> siteIterator = sortedValeurs.entrySet().iterator(); siteIterator.hasNext();) {
                for (Iterator<Entry<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, List<M>>>>> treatmentIterator = siteIterator.next().getValue().entrySet().iterator(); treatmentIterator.hasNext();) {
                    for (Iterator<Entry<Parcelle, SortedMap<LocalDate, List<M>>>> parcelleIterator = treatmentIterator.next().getValue().entrySet().iterator(); parcelleIterator.hasNext();) {
                        for (Iterator<Entry<LocalDate, List<M>>> dateIterator = parcelleIterator.next().getValue().entrySet().iterator(); dateIterator.hasNext();) {
                            for (Iterator<M> mesureIterator = dateIterator.next().getValue().iterator(); mesureIterator.hasNext();) {
                                M mesure = mesureIterator.next();
                                this.addMesureLine(mesure, requestMap);
                                this.rawDataPrintStream.println();
                                mesureIterator.remove();
                            }
                            dateIterator.remove();
                        }
                        parcelleIterator.remove();
                    }
                    treatmentIterator.remove();
                }
                siteIterator.remove();
            }
        }

        /**
         * @return
         */
        public Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> getAvailablesCouverts() {
            return this.availablesCouverts;
        }

        /**
         * @param mesure
         * @param requestMap
         */
        protected void addMesureLine(final M mesure, Map<String, Object> requestMap) {
            this.printLineGeneric(mesure);
            this.printLineSpecific(mesure);
            List<ValeurBiomasse> valeurs = new LinkedList();
            valeurs.addAll(mesure.getValeurs());
            Collections.sort(this.variablesAffichage, new ComparatorWithFirstStringThenAlpha(getFirstVariable()));
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            while (itVariablesAffichage.hasNext()) {
                variableAffichage = itVariablesAffichage.next();
                boolean isPrinted = false;
                for (ValeurBiomasse valeurBiomasse : new LinkedList<>(valeurs)) {
                    if (variableAffichage.equals(((DatatypeVariableUniteACBB) valeurBiomasse.getRealNode().getNodeable())
                            .getVariable().getAffichage())) {
                        this.addVariablesCulumns((V) valeurBiomasse);
                        this.addMethode(valeurBiomasse, requestMap);
                        valeurs.remove(valeurBiomasse);
                        isPrinted = true;
                        break;
                    }
                }
                if (!isPrinted) {
                    this.addVariablesCulumnsForNullVariable();
                }
            }
            this.printObservation(mesure);
        }

        /**
         * @param valeurBiomasse
         * @param requestMap
         */
        protected void addMethode(ValeurBiomasse valeurBiomasse, Map<String, Object> requestMap) {
            Set<AbstractMethode> methods = (Set<AbstractMethode>) requestMap
                    .get(AbstractMethode.class.getSimpleName());
            if (methods == null) {
                requestMap.put(AbstractMethode.class.getSimpleName(),
                        new HashSet<AbstractMethode>());
                methods = (Set<AbstractMethode>) requestMap.get(AbstractMethode.class
                        .getSimpleName());
            }
            if (valeurBiomasse.getMethode() != null) {
                methods.add(valeurBiomasse.getMethode());
            }
        }

        /**
         * @param valeurBiomasse
         */
        protected abstract void addVariablesCulumns(V valeurBiomasse);

        /**
         *
         */
        protected abstract void addVariablesCulumnsForNullVariable();

        /**
         * Prints the line generic.
         *
         * @param mesure
         */
        protected void printLineGeneric(M mesure) {
            final SuiviParcelle suiviParcelle = mesure.getSequence().getSuiviParcelle();
            final Parcelle parcelle = suiviParcelle.getParcelle();
            TraitementProgramme treatment = suiviParcelle.getTraitement();
            String line;
            final AbstractIntervention intervention = mesure.getSequence().getIntervention();
            final InterventionType interventionType = mesure.getSequence().getInterventionType();
            final Integer rotation = mesure.getSequence().getVersionTraitementRealiseeNumber();
            final Integer annee = mesure.getSequence().getAnneeRealiseeNumber();
            final Integer periode = mesure.getSequence().getPeriodeRealiseeNumber();
            final Bloc bloc = parcelle.getBloc();
            String numIntervention = org.apache.commons.lang.StringUtils.EMPTY;
            if (intervention instanceof Paturage) {
                numIntervention = getPatPassageFromPaturage((Paturage) intervention);
            } else if (intervention instanceof RecFau) {
                numIntervention = getNumFromRecFau((RecFau) intervention);
            }
            line = String.format(OutputHelper.PATTERN_11_FIELD,
                    this.propertiesSiteName.getProperty(parcelle.getSite().getName(), parcelle.getSite().getName()),
                    this.propertiesTreatmentName.getProperty(treatment.getNom(), treatment.getNom()),
                    this.propertiesTreatmentAffichage.getProperty(treatment.getAffichage(), treatment.getAffichage()),
                    this.propertiesParcelleName.getProperty(parcelle.getName(), parcelle.getName()),
                    bloc == null ? StringUtils.EMPTY : bloc.getBlocName(),
                    bloc == null ? StringUtils.EMPTY : bloc.getRepetitionName(),
                    rotation <= 0 ? org.apache.commons.lang.StringUtils.EMPTY : rotation,
                    annee < 0 ? org.apache.commons.lang.StringUtils.EMPTY : annee,
                    periode < 0 ? org.apache.commons.lang.StringUtils.EMPTY : periode,
                    interventionType == null ? org.apache.commons.lang.StringUtils.EMPTY : this.propertiesValeursQualitative.getProperty(
                            interventionType.getValeur(), interventionType.getValeur()),
                    intervention == null ? org.apache.commons.lang.StringUtils.EMPTY
                            : DateUtil.getUTCDateTextFromLocalDateTime(intervention.getDate(), DateUtil.DD_MM_YYYY),
                    numIntervention);
            this.rawDataPrintStream.print(line);
        }

        /**
         * @param mesure
         */
        abstract protected void printLineSpecific(M mesure);

        /**
         * @param mesure
         * @return
         */
        protected PrintStream printObservation(final M mesure) {
            return this.rawDataPrintStream.printf(ACBBMessages.PATTERN_1_FIELD,
                    mesure.getSequence().getObservation());
        }

        /**
         * @param value
         */
        protected void printVariableColumnForStringValue(String value) {
            value = Strings.isNullOrEmpty(value) ? RecorderACBB.PROPERTY_CST_NOT_AVALAIBALE_FIELD
                    : String.format(ACBBMessages.PATTERN_1_FIELD, value);
            this.rawDataPrintStream.print(value);
        }

        private String getPatPassageFromPaturage(Paturage paturage) {
            for (ValeurPaturage valeur : paturage.getValeurs()) {
                if ("pat_passage".equals(((DatatypeVariableUniteACBB) valeur.getRealNode().getNodeable()).getVariable().getAffichage())) {
                    int passage = valeur.getValeur().intValue();
                    return passage <= 0 ? org.apache.commons.lang.StringUtils.EMPTY : Integer.toString(passage);
                }
            }
            return org.apache.commons.lang.StringUtils.EMPTY;
        }

        private String getNumFromRecFau(RecFau recFau) {
            int passage = recFau.getPassage();
            return passage <= 0 ? org.apache.commons.lang.StringUtils.EMPTY : Integer.toString(passage);
        }

        /**
         * @param mesure
         * @return
         */
        protected String getCouvert(AbstractBiomasse mesure) {
            final LocalDate dateForCover = mesure.getIntervention() == null ? mesure.getDate() : mesure
                    .getIntervention().getDate();
            final VegetalPeriode couvert = itkDatasetManager.getCouvertVegetal(
                    mesure.getSuiviParcelle().getParcelle(),
                    dateForCover,
                    this.getAvailablesCouverts());
            return this.propertiesValeursQualitative.getProperty(
                    couvert.getCouvert(mesure.getNatureCouvert()),
                    couvert.getCouvert(mesure.getNatureCouvert())
            );
        }

        /**
         * @param mesure
         * @return
         */
        protected String getAnnee(AbstractBiomasse mesure) {
            return DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDate(), OutputHelper.PATTERN_DATE_YYYY);
        }

        /**
         * @param mesure
         * @return
         */
        protected String getDateMesure(AbstractBiomasse mesure) {
            return DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDate(), DateUtil.DD_MM_YYYY);
        }
    }
}
