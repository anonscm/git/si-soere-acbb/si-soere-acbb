/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.versiontraitement;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAVersionDeTraitementDAO.
 */
public class JPAVersionDeTraitementDAO extends AbstractJPADAO<VersionDeTraitement> implements
        IVersionDeTraitementDAO {
    /**
     * @return
     */
    @Override
    public List<VersionDeTraitement> getAll() {
        return this.getAll(VersionDeTraitement.class);
    }

    /**
     * Gets the by n key.
     *
     * @param siteCode
     * @param codeTraitement
     * @param version
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO
     * #getByNKey(java.lang.String, java.lang.String, int)
     * @link(String) the site code
     * @link(String) the code traitement
     * @link(int) the version
     */
    @Override
    public Optional<VersionDeTraitement> getByNKey(final String siteCode, final String codeTraitement,
                                                   final int version) {
        CriteriaQuery<VersionDeTraitement> query = builder.createQuery(VersionDeTraitement.class);
        Root<VersionDeTraitement> vdt = query.from(VersionDeTraitement.class);
        Join<VersionDeTraitement, TraitementProgramme> tp = vdt.join(VersionDeTraitement_.traitementProgramme);
        Join<TraitementProgramme, SiteACBB> site = tp.join(TraitementProgramme_.site);
        query
                .select(vdt)
                .where(
                        builder.equal(tp.get(TraitementProgramme_.code), codeTraitement),
                        builder.equal(vdt.get(VersionDeTraitement_.version), version),
                        builder.equal(site.get(SiteACBB_.code), siteCode)
                );
        return getOptional(query);
    }

    /**
     * Gets the last version.
     *
     * @param traitement
     * @return the last version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO
     *                              #getLastVersion
     *                              (org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme)
     * @link(TraitementProgramme) the traitement
     */
    @Override
    public int getLastVersion(final TraitementProgramme traitement) throws PersistenceException {
        CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
        Root<VersionDeTraitement> vdt = query.from(VersionDeTraitement.class);
        query
                .select(builder.max(vdt.get(VersionDeTraitement_.version)))
                .where(
                        builder.equal(vdt.join(VersionDeTraitement_.traitementProgramme), traitement)
                );
        return getResultAsStream(query)
                .filter(t -> t != null)
                .findFirst()
                .orElse(0);
    }

    /**
     * Gets the previous version.
     *
     * @param traitement
     * @param version
     * @return the previous version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO
     *                              #getpreviousVersion
     *                              (org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme, int)
     * @link(TraitementProgramme) the traitement
     * @link(int) the version
     */
    @Override
    public int getpreviousVersion(final TraitementProgramme traitement, final int version)
            throws PersistenceException {
        CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
        Root<VersionDeTraitement> vdt = query.from(VersionDeTraitement.class);
        query
                .select(builder.max(vdt.get(VersionDeTraitement_.version)))
                .where(
                        builder.equal(vdt.join(VersionDeTraitement_.traitementProgramme), traitement),
                        builder.lt(vdt.get(VersionDeTraitement_.version), version)
                );
        return getResultAsStream(query)
                .filter(t -> t != null)
                .findFirst()
                .orElse(0);
    }

    /**
     * Gets the previous version traitement.
     *
     * @param traitement
     * @param version
     * @return the previous version traitement
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @link(TraitementProgramme) the traitement
     * @link(int) the version
     */
    @Override
    public Optional<VersionDeTraitement> getpreviousVersionTraitement(final TraitementProgramme traitement,
                                                                      final int version) throws PersistenceException {
        return this.getByNKey(traitement.getSite().getCode(), traitement.getCode(), version);
    }

    /**
     * @param traitement
     * @param date
     * @return
     */
    @Override
    public Optional<VersionDeTraitement> retrieveVersiontraitement(TraitementProgramme traitement, LocalDate date) {
        CriteriaQuery<VersionDeTraitement> query = builder.createQuery(VersionDeTraitement.class);
        Root<VersionDeTraitement> vdt = query.from(VersionDeTraitement.class);
        Subquery<LocalDate> subquery = query.subquery(LocalDate.class);
        Root<VersionDeTraitement> vdt2 = subquery.from(VersionDeTraitement.class);
        subquery
                .select(builder.greatest(vdt2.get(VersionDeTraitement_.dateDebutVersionTraitement)))
                .where(builder.equal(vdt2.join(VersionDeTraitement_.traitementProgramme), traitement));
        query
                .select(vdt)
                .where(
                        builder.equal(subquery, vdt.get(VersionDeTraitement_.dateDebutVersionTraitement)),
                        builder.equal(vdt.join(VersionDeTraitement_.traitementProgramme), traitement)
                );
        return getOptional(query);
    }
}
