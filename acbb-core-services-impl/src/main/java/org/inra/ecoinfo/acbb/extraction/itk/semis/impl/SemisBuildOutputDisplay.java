/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemis;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class SemisBuildOutputDisplay extends
        AbstractITKOutputBuilder<Semis, ValeurSemis, MesureSemis> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.semis.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(SemisBuildOutputDisplay.class
            .getName());

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public SemisBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return SemisBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedSemisVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedSemisVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream) {
        return new SemisOutputHelper(rawDataPrintStream,
                selectedSemisVariables);
    }

    class SemisOutputHelper extends OutputHelper {

        final SortedMap<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<ListeItineraire, MesureSemis>>>>> sortedValeursSemis = new TreeMap();

        SemisOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedSemisVariables) {
            super(rawDataPrintStream, selectedSemisVariables);
        }

        @Override
        public void addMesure(MesureSemis mesure) {
            final Semis sequence = mesure.getSequence();
            SiteACBB site = sequence.getSuiviParcelle().getParcelle().getSite();
            Parcelle parcelle = sequence.getSuiviParcelle().getParcelle();
            TraitementProgramme treatment = sequence.getSuiviParcelle().getTraitement();
            ListeItineraire variete = mesure.getVariete();
            sortedValeursSemis
                    .computeIfAbsent(site, k -> new TreeMap<>())
                    .computeIfAbsent(treatment, k -> new TreeMap<>())
                    .computeIfAbsent(parcelle, k -> new TreeMap<>())
                    .computeIfAbsent(sequence.getDate(), k -> new TreeMap<>())
                    .put(variete, mesure);
        }

        /**
         * Adds the mesure line.
         *
         * @param valeurSemis
         * @link(MesureFluxTours) the mesure
         */
        @Override
        protected void addMesureLine(final MesureSemis mesureSemis) {
            this.printLineGeneric(mesureSemis);
            List<ValeurIntervention> valeurs = new LinkedList();
            valeurs.addAll(((Semis) mesureSemis.getSemis()).getAttributs());
            valeurs.addAll(mesureSemis.getvaleursSemis());
            final Iterator<String> itVariablesAffichage = this.variablesAffichage.iterator();
            String variableAffichage;
            Properties propertiesValeurQualitatives = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, ValeurQualitative.ATTRIBUTE_JPA_VALUE);
            while (itVariablesAffichage.hasNext()) {
                String value = RecorderACBB.PROPERTY_CST_NOT_AVALAIBALE_FIELD;
                variableAffichage = itVariablesAffichage.next();
                for (ValeurIntervention valeurIntervention : new LinkedList<>(valeurs)) {
                    if (variableAffichage.equals(((DatatypeVariableUniteACBB) valeurIntervention.getRealNode().getNodeable())
                            .getVariable().getAffichage())) {
                        final String valeurToString = Utils.createCodeFromString(valeurIntervention.getValeurToString());
                        value = String.format(ACBBMessages.PATTERN_1_FIELD, propertiesValeurQualitatives.getProperty(valeurToString, valeurToString));
                        valeurs.remove(valeurIntervention);
                        break;
                    }
                }
                this.rawDataPrintStream.print(value);
            }
        }

        @Override
        public void buildBody(List<MesureSemis> mesures) {
            mesures.stream()
                    .sorted((a, b) -> a.getSequence().compareTo(b.getSequence()))
                    .forEach(mesure -> addMesure(mesure));
            for (final Entry<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<ListeItineraire, MesureSemis>>>>> siteEntry : this.sortedValeursSemis.entrySet()) {
                for (final Entry<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<ListeItineraire, MesureSemis>>>> treatmentEntry : siteEntry.getValue().entrySet()) {
                    for (final Entry<Parcelle, SortedMap<LocalDate, SortedMap<ListeItineraire, MesureSemis>>> parcelleEntry : treatmentEntry.getValue().entrySet()) {
                        for (final Entry<LocalDate, SortedMap<ListeItineraire, MesureSemis>> dateEntry : parcelleEntry.getValue().entrySet()) {
                            for (Entry<ListeItineraire, MesureSemis> varieteEntry : dateEntry.getValue().entrySet()) {
                                this.addMesureLine(varieteEntry.getValue());
                                this.rawDataPrintStream.println();
                            }
                        }
                    }
                }
            }
        }
    }
}
