/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.lai;

import org.inra.ecoinfo.acbb.dataset.biomasse.IBiomasseDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The Interface IAIDAO.
 */
public interface ILaiDAO extends IBiomasseDAO<Lai> {

    /**
     * Gets the by n key.
     *
     * @param version
     * @param date
     * @return the by n key
     * @link{VersionFile the version
     * @link{Date the date
     */
    Optional<Lai> getByNKey(VersionFile version, LocalDate date);
}
