package org.inra.ecoinfo.acbb.refdata.listesacbb;

import org.inra.ecoinfo.IDAO;

import java.util.List;
import java.util.Optional;

/**
 * @param <T>
 * @author ptcherniati
 */
public interface IListeACBBDAO<T extends ListeACBB> extends IDAO<T> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<T> getAll();

    /**
     * Gets the by code.
     *
     * @param <U>
     * @param code the code
     * @return the by code
     */
    <U extends T> List<U> getByName(String code);

    /**
     * @param <U>
     * @param nom
     * @param valeur
     * @return
     */
    <U extends T> Optional<U> getByNKey(String nom, String valeur);

}
