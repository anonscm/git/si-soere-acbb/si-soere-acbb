/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IValeurRecFauDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau;

/**
 * The Class JPAValeurSemisDAO.
 */
public class JPAValeurRecFauDAO extends AbstractJPADAO<ValeurRecFau> implements IValeurRecFauDAO {

}
