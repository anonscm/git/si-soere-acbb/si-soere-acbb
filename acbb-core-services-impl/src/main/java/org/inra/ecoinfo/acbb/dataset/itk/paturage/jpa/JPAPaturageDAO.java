package org.inra.ecoinfo.acbb.dataset.itk.paturage.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage_;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage_;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPAPaturageDAO extends JPAInterventionDAO<Paturage> implements IPaturageDAO {

    private static final String QUALITATIVE_FIELD_PAT_PASSAGE = "pat_passage";

    /**
     * Gets the by n key.v;
     *
     * @param version
     * @param dateDebut
     * @param dateFin
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO#getByNKey(org.inra.
     * ecoinfo.dataset.versioning.entity.VersionFile, int,
     * java.time.LocalDateTime)
     * @link(VersionFile)
     * @link(int)
     * @link(Date) the date
     */
    @Override
    public Optional<Paturage> getByNKey(final VersionFile version, final LocalDate dateDebut, final LocalDate dateFin) {

        CriteriaQuery<Paturage> query = builder.createQuery(Paturage.class);
        Root<Paturage> pat = query.from(Paturage.class);
        query.where(
                builder.equal(pat.get(Paturage_.version), version),
                builder.equal(pat.get(Paturage_.date), dateDebut),
                builder.equal(pat.get(Paturage_.dateFin), dateFin)
        );
        query.select(pat);
        return getOptional(query);
    }

    /**
     * @param parcelle
     * @param num
     * @param rotationNumber
     * @param anneeNumber
     * @param type
     * @return
     */
    @Override
    public Optional<Paturage> getinterventionForNumYearAndType(Parcelle parcelle, int num, Integer rotationNumber,
                                                               Integer anneeNumber, String type) {

        CriteriaQuery<Paturage> query = builder.createQuery(Paturage.class);
        Root<ValeurPaturage> valeur = query.from(ValeurPaturage.class);
        Join<RealNode, Nodeable> nodeable = valeur.join(ValeurPaturage_.realNode).join(RealNode_.nodeable);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<ValeurPaturage, Paturage> paturage = valeur.join(ValeurPaturage_.paturage);
        query.where(
                builder.equal(paturage.get(Paturage_.versionTraitementRealiseeNumber), Math.max(0, rotationNumber)),
                builder.equal(paturage.get(Paturage_.anneeRealiseeNumber), anneeNumber),
                builder.equal(paturage.join(Paturage_.suiviParcelle).join(SuiviParcelle_.parcelle), parcelle),
                builder.equal(nodeable, dvu),
                builder.equal(dvu.join(DatatypeVariableUniteACBB_.variable).get(VariableACBB_.affichage), QUALITATIVE_FIELD_PAT_PASSAGE),
                builder.equal(valeur.get(ValeurPaturage_.valeur), num)
        );
        query.select(paturage);
        return getOptional(query);
    }

    /**
     * @param interventionType
     * @return
     */
    @Override
    public boolean hasType(InterventionType interventionType) {
        return Paturage.PATURAGE_DESCRIMINATOR.equals(interventionType.getValeur());
    }

    @Override
    protected Class<Paturage> getInterventionClass() {
        return Paturage.class;
    }
}
