package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.IHauteurVegetalDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.ValeurHauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildHauteurVegetalHelper extends AbstractBuildHelper<HauteurVegetalLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.impl.messages";

    Map<Parcelle, Map<LocalDate, Map<Long, HauteurVegetal>>> vegetalHeights = new HashMap();

    IHauteurVegetalDAO hauteurVegetalDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursHauteurVegetal(final HauteurVegetalLineRecord line, final HauteurVegetal hauteurvegetal) {
        for (final VariableValueHauteurVegetal variableValue : line.getVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurHauteurVegetal valeur;
            valeur = new ValeurHauteurVegetal(Float.parseFloat(
                    variableValue.getValue()),
                    realNode,
                    variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(),
                    hauteurvegetal,
                    variableValue.getQualityClass(), variableValue.getMethode(),
                    variableValue.getMediane());
            hauteurvegetal.getValeursvaleursHH().add(valeur);
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    protected void build() throws PersistenceException {
        for (final HauteurVegetalLineRecord line : this.lines) {
            final HauteurVegetal hauteurvegetal = this.getOrCreateHauteurVegetal(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursHauteurVegetal(line, hauteurvegetal);
            this.hauteurVegetalDAO.saveOrUpdate(hauteurvegetal);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesBiomasse
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesBiomasse the request properties Biomasse
     */
    public void build(final VersionFile version, final List<HauteurVegetalLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesBiomasse requestPropertiesBiomasse) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesBiomasse);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    HauteurVegetal createNewHauteurVegetal(final HauteurVegetalLineRecord line) {
        HauteurVegetal hauteurvegetal = null;
        hauteurvegetal = new HauteurVegetal(
                this.version, line.getNatureCouvert(),
                line.getNature(),
                line.getObservation(),
                line.getDate(),
                line.getSerie(),
                line.getRotationNumber(),
                line.getAnneeNumber(),
                line.getPeriodeNumber(),
                line.getSuiviParcelle(),
                line.getTypeIntervention(),
                line.getIntervention(),
                line.getEntreeSortie());
        try {
            this.hauteurVegetalDAO.saveOrUpdate(hauteurvegetal);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return hauteurvegetal;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    HauteurVegetal getOrCreateHauteurVegetal(final HauteurVegetalLineRecord line) {
        return vegetalHeights
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getNature().getId(), k -> createNewHauteurVegetal(line));
    }

    /**
     * @param hauteurVegetalDAO the hauteurVegetalDAO to set
     */
    public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurVegetalDAO) {
        this.hauteurVegetalDAO = hauteurVegetalDAO;
    }
}
