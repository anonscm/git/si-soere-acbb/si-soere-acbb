/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import java.util.Optional;

/**
 * The Interface IVersionDeTraitementRealiseeDAO.
 */
public interface IVersionDeTraitementRealiseeDAO extends IDAO<VersionTraitementRealisee> {

    /**
     * Gets the by n key.
     *
     * @param versionDeTraitement the version de traitement
     * @param parcelle
     * @param rotation            the rotation
     * @return the by n key
     * @link{Parcelle the parcelle
     */
    Optional<VersionTraitementRealisee> getByNKey(VersionDeTraitement versionDeTraitement,
                                                  final Parcelle parcelle, int rotation);
}
