/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import java.util.Locale;

/**
 * @author ptcherniati
 */
public class AbstractPeriodFactory {

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    protected IPolicyManager policyManager;

    /**
     * @param valeur
     * @return
     */
    public String getInternationalizedValeurQualitative(final String valeur) {
        return this.localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA,
                IValeurQualitative.ATTRIBUTE_JPA_VALUE,
                new Locale(this.policyManager.getCurrentUser().getLanguage())).getProperty(valeur,
                valeur);
    }

    /**
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

}
