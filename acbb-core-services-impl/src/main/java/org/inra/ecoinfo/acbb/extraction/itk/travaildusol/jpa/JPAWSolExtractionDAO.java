/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.travaildusol.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.*;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.travaildusol.SynthesisValue;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
@SuppressWarnings("unchecked")
public class JPAWSolExtractionDAO extends AbstractJPAITKDAO<TravailDuSol, MesureTravailDuSol, ValeurTravailDuSol> {

    /**
     * @param v
     * @return
     */
    @Override
    protected Join<?, TravailDuSol> getSequencePath(Root<ValeurTravailDuSol> v) {
        return v.join(ValeurTravailDuSol_.mesureTravailDuSol).join(MesureTravailDuSol_.travailDuSol);
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<MesureTravailDuSol> getMesureITKClass() {
        return MesureTravailDuSol.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurTravailDuSol> getValeurITKClass() {
        return ValeurTravailDuSol.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, MesureTravailDuSol> getMesureAttribute() {
        return ValeurTravailDuSol_.mesureTravailDuSol;
    }
}
