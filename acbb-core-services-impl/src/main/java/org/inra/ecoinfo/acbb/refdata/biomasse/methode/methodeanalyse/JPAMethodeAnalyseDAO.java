/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPAMethodeAnalyseDAO.
 */
public class JPAMethodeAnalyseDAO extends AbstractJPAMethodeDAO<MethodeAnalyse> implements
        IMethodeAnalyseDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeAnalyse> getMethodClass() {
        return MethodeAnalyse.class;
    }

}
