package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.TestHeadersITK;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class TestHeadersSemis.
 */
public class TestHeadersFert extends TestHeadersITK {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * @param badsFormatsReport
     * @param parser
     * @param datasetDescriptor
     * @param lineNumber
     * @param requestPropertiesACBB
     * @return @throws java.io.IOException
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader#readLineHeader(org.inra.ecoinfo.utils.exceptions.BadsFormatsReport,
     * com.Ostermiller.util.CSVParser, long, org.inra.ecoinfo.utils.DatasetDescriptor,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB)
     */
    @Override
    protected long readLineHeader(final BadsFormatsReport badsFormatsReport,
                                  final CSVParser parser, final long lineNumber,
                                  final DatasetDescriptor datasetDescriptor,
                                  final IRequestPropertiesACBB requestPropertiesACBB) throws IOException {
        String[] values;
        int index;
        values = parser.getLine();
        final CleanerValues cleanerValues = new CleanerValues(values);

        String value;
        final List<Column> columnToParse = new LinkedList<>(datasetDescriptor.getColumns());
        for (index = 0; index < values.length; index++) {
            value = null;
            try {
                value = cleanerValues.nextToken();
            } catch (EndOfCSVLine ex) {
                badsFormatsReport.addException(ex.setMessage(lineNumber, index));
                continue;
            }
            Column columnTarget = null;
            for (final Column column : columnToParse) {
                if (column.getName().equalsIgnoreCase(value)) {
                    columnTarget = column;
                    break;
                }
            }
            if (columnTarget == null) {
                Pattern pattern = Pattern.compile("fert_([a-z]*)");
                Matcher match = pattern.matcher(value);
                if (match.matches()) {
                    columnTarget = new Column();
                    columnTarget.setFieldName(value);
                    columnTarget.setFlag(true);
                    columnTarget.setFlagType(RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE);
                    columnTarget.setValueType(RecorderACBB.PROPERTY_CST_FLOAT_TYPE);
                    columnTarget.setVariable(true);
                } else {
                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderACBB
                                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_GENERIC_COLUMN_HEADER),
                                            lineNumber, value, index + 1)));
                }
            }
            ((IRequestPropertiesITK) requestPropertiesACBB).getValueColumns().put(index,
                    columnTarget);
            columnToParse.remove(columnTarget);
        }
        return lineNumber + 1;
    }
}
