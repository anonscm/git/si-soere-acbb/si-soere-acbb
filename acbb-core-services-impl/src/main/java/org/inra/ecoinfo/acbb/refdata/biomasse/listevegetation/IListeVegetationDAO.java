package org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation;

import org.inra.ecoinfo.acbb.refdata.listesacbb.IListeACBBDAO;

import java.util.List;

/**
 * @author ptcherniati
 */
public interface IListeVegetationDAO extends IListeACBBDAO<ListeVegetation> {

    /**
     * @return
     */
    List<ListeVegetation> getValueColumns();
}
