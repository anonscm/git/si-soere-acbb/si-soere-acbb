package org.inra.ecoinfo.acbb.extraction.itk.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention_;
import org.inra.ecoinfo.acbb.extraction.itk.IInterventionExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.jpa.AbstractACBBExtractionJPA;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjuster;
import java.util.LinkedList;
import java.util.List;

/**
 * @param <T>
 * @param <M>
 * @param <V>
 * @author ptcherniati
 */
public abstract class AbstractJPAITKDAO<T extends AbstractIntervention, M extends IMesureITK<T, V>, V extends ValeurIntervention> extends AbstractACBBExtractionJPA<M> implements
        IInterventionExtractionDAO<M> {

    /**
     * @param selectedTraitementProgrammes
     * @param selectedVariables
     * @param intervals
     * @param user
     * @return
     */
    @Override
    public List<M> extractITKMesures(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user) {

        CriteriaQuery query = getQuery(intervals, user, selectedVariables, selectedTraitementProgrammes, false);
        return getResultList(query);
    }

    private CriteriaQuery getQuery(List<IntervalDate> intervals, IUser user, List<Variable> selectedVariables, List<TraitementProgramme> selectedTraitementProgrammes, boolean isCount) throws DateTimeParseException {
        CriteriaQuery query;
        if (isCount) {
            query = builder.createQuery(Long.class);
        } else {
            query = builder.createQuery(getMesureITKClass());
        }
        Root<V> v = query.from(getValeurITKClass());
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<V, M> m = v.join((SingularAttribute<ValeurIntervention, M>) getMesureAttribute());
        Join<?, T> s = getSequencePath(v);
        s.join(AbstractIntervention_.suiviParcelle);
        Join<V, RealNode> rn = v.join(ValeurIntervention_.realNode);
        Path<Nodeable> dvuNodeable = v.join(ValeurIntervention_.realNode).join(RealNode_.nodeable);
        List<Predicate> and = new LinkedList();
        List<Predicate> or = new LinkedList();
        Root<NodeDataSet> nds = null;
        for (IntervalDate intervalDate : intervals) {
            List<Predicate> andForDate = new LinkedList();
            LocalDate minDate, maxDate;
            minDate = intervalDate.getBeginDate().toLocalDate();
            maxDate = intervalDate.getEndDate().toLocalDate();
            andForDate.add(builder.between(s.get(AbstractIntervention_.date), minDate, maxDate));
            if (!(isCount || user.getIsRoot())) {
                nds = nds == null ? query.from(NodeDataSet.class) : nds;
                andForDate.add(builder.equal(nds.join(NodeDataSet_.realNode), rn));
                addRestrictiveRequestOnRoles(user, query, andForDate, builder, nds, s.get(AbstractIntervention_.date));
            }
            or.add(builder.and(andForDate.toArray(new Predicate[andForDate.size()])));
        }
        and.add(builder.or(or.toArray(new Predicate[or.size()])));
        and.add(dvu.join(DatatypeVariableUniteACBB_.variable).in(selectedVariables));
        and.add(builder.equal(dvu, dvuNodeable));
        and.add(s.join(AbstractIntervention_.suiviParcelle).join(SuiviParcelle_.traitement).in(selectedTraitementProgrammes));
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        query.distinct(true);
        if (isCount) {
            query.select(builder.count(m));
        } else {
            query.select(m);
        }
        return query;
    }

    public Long getSize(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user) {
        CriteriaQuery<Long> criteria = getQuery(intervals, user, selectedVariables, selectedTraitementProgrammes, true);
        Long rows = entityManager.createQuery(criteria).getSingleResult();
        return rows == null ? 0l : rows * 1_500;
    }

    /**
     * @return
     */
    abstract protected Class<M> getMesureITKClass();

    /**
     * @return
     */
    abstract protected Class<V> getValeurITKClass();

    /**
     * @return
     */
    abstract protected SingularAttribute<?, M> getMesureAttribute();

    /**
     * @param v
     * @return
     */
    protected Join<?, T> getSequencePath(Root<V> v) {
        Join<V, M> mesureSequence = v.join((SingularAttribute<ValeurIntervention, M>) getMesureAttribute());
        return (Join<V, T>) mesureSequence;
    }

    /**
     * @param user
     * @param criteria
     * @param predicatesAnd
     * @param builder
     * @param vns
     * @param dateMesure
     */
    @SuppressWarnings("unchecked")
    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.login), user.getLogin()));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

    /**
     * @return
     */
    abstract protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass();

    @Override
    protected boolean isParcelleDatatype() {
        return false;
    }

}
