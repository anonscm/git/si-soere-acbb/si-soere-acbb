/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractVariableValueBiomasse;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal.MethodeHauteurVegetal;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

/**
 * @author ptcherniati
 */
public class VariableValueHauteurVegetal extends AbstractVariableValueBiomasse {

    BiomasseEntreeSortie biomasseEntreeSortie;

    /**
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     * @param dvu
     * @param value
     * @param methodeHauteurVegetal
     * @param biomasseEntreeSortie
     */
    public VariableValueHauteurVegetal(int measureNumber, float standardDeviation, int qualityIndex, DatatypeVariableUniteACBB dvu, String value, MethodeHauteurVegetal methodeHauteurVegetal, BiomasseEntreeSortie biomasseEntreeSortie) {
        super(dvu, value, methodeHauteurVegetal, qualityIndex, measureNumber, 0, qualityIndex);
        this.biomasseEntreeSortie = biomasseEntreeSortie;
        this.setStandardDeviation(standardDeviation);
    }

    /**
     * @return
     */
    public BiomasseEntreeSortie getBiomasseEntreeSortie() {
        return this.biomasseEntreeSortie;
    }

    /**
     * @param biomasseEntreeSortie
     */
    public void setBiomasseEntreeSortie(BiomasseEntreeSortie biomasseEntreeSortie) {
        this.biomasseEntreeSortie = biomasseEntreeSortie;
    }
}
