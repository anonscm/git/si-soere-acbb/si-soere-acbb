/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeHauteurVegetalDAO extends IMethodeDAO<MethodeHauteurVegetal> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeHauteurVegetal> getAll();
}
