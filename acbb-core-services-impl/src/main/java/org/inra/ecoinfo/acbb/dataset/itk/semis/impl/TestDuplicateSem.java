/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class TestDuplicateSem.
 */
public class TestDuplicateSem extends AbstractTestDuplicate {

    /**
     * The Constant ACBB_DATASET_SEM_BUNDLE_NAME @link(String).
     */
    public static final String ACBB_DATASET_SEM_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE @link(String).
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The Constant PROPERTY_MSG_MISMACH_COUVERT_VEGETAL @link(String).
     */
    static final String PROPERTY_MSG_MISMACH_COUVERT_VEGETAL = "PROPERTY_MSG_MISMACH_COUVERT_VEGETAL";

    /**
     * The line @link(SortedMap<String,Long>).
     */
    final SortedMap<String, Long> line;

    /**
     * The line couvert @link(SortedMap<String,String>).
     */
    final SortedMap<String, String> lineCouvert;

    /**
     * Instantiates a new test duplicate sem.
     */
    public TestDuplicateSem() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param parcelle
     * @link(String) the parcelle
     * @param date
     * @link(String) the date
     * @param couvertVegetal
     * @link(String) the couvert vegetal
     * @param espece
     * @link(String) the espece
     * @param variete
     * @link(String) the variete
     * @param lineNumber
     * @link(long) the line number
     */
    protected void addLine(final String parcelle, final String date, final String couvertVegetal,
                           final String espece, final String variete, final long lineNumber) {
        final String key = this.getKey(parcelle, date, espece, variete);
        final String keyCouvert = this.getKey(parcelle, date);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keyCouvert)) {
                this.lineCouvert.put(keyCouvert, couvertVegetal);
            } else if (!this.lineCouvert.get(keyCouvert).equals(couvertVegetal)) {
                this.errorsReport.addErrorMessage(String.format(RecorderACBB
                                .getACBBMessageWithBundle(TestDuplicateSem.ACBB_DATASET_SEM_BUNDLE_NAME,
                                        TestDuplicateSem.PROPERTY_MSG_MISMACH_COUVERT_VEGETAL), lineNumber,
                        parcelle, date, this.lineCouvert.get(keyCouvert), couvertVegetal));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateSem.ACBB_DATASET_SEM_BUNDLE_NAME,
                    TestDuplicateSem.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle, date,
                    espece, variete, this.line.get(key)));
        }

    }

    /**
     * @param values
     * @param versionFile
     * @param lineNumber
     * @param dates
     * @see
     * org.inra.ecoinfo.acbb.dataset.ITestDuplicates#addLine(java.lang.String[],
     * long)
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[8], values[10], values[11], lineNumber);
    }
}
