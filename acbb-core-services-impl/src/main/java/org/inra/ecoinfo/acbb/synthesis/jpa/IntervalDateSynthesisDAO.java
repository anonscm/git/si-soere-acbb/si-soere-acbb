/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype_;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

/**
 * @author tcherniatinsky
 */
public class IntervalDateSynthesisDAO extends AbstractJPADAO<Object> implements IIntervalDateSynthesisDAO {

    public Optional<IntervalDate> getIntervalDateAvailable(List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypesClasses) {
        LocalDateTime[] minMax = new LocalDateTime[2];
        synthesisDatatypesClasses.stream()
                .map(this::getMinMax)
                .filter(prdct -> prdct != null && prdct[0] != null && prdct[1] != null)
                .forEach(ldts -> {
                    minMax[0] = minMax[0] == null || ldts[0] == null ? ldts[0] : (minMax[0].isBefore(ldts[0]) ? minMax[0] : ldts[0]);
                    minMax[1] = minMax[1] == null || ldts[1] == null ? ldts[1] : (minMax[1].isBefore(ldts[1]) ? minMax[1] : ldts[1]);
                });
        try {
            if (minMax == null || minMax[0] == null && minMax[1] == null) {
                return Optional.empty();
            }
            return Optional.of(new IntervalDate(minMax[0], minMax[1], DateUtil.DD_MM_YYYY));
        } catch (BadExpectedValueException | DateTimeParseException e) {
            return Optional.empty();
        }
    }

    protected LocalDateTime[] getMinMax(Class<? extends GenericSynthesisDatatype> synthesisDatatypeClass) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<? extends GenericSynthesisDatatype> sd = query.from(synthesisDatatypeClass);
        query
                .multiselect(sd.get(GenericSynthesisDatatype_.minDate), sd.get(GenericSynthesisDatatype_.maxDate));
        Tuple result = getFirstOrNull(query);
        if (result == null) {
            return new LocalDateTime[]{null, null};
        }
        try {

            return new LocalDateTime[]{result.get(0, LocalDateTime.class), result.get(1, LocalDateTime.class)};
        } catch (Exception e) {
            return null;
        }
    }
}
