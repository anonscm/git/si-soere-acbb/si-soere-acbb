/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseExtractor;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * The Class BiomasseExtractor.
 */
public class BiomasseExtractor extends MO implements IExtractor {

    /**
     * The Constant MAP_INDEX_0.
     */
    public static final String MAP_INDEX_0 = "0";
    // extraction
    /**
     * The Constant CST_RESULTS.
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.biomasse.messages";

    /**
     * The Constant MSG_EXTRACTION_ABORTED @link(String).
     */
    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     * The Constant PROPERTY_MSG_BADS_RIGHTS @link(String).
     */
    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";
    /**
     * autre type de donnée biomasse
     */
    @SuppressWarnings("rawtypes")
    protected IBiomasseExtractionDAO biomasseExtractionDAO;
    /**
     *
     */
    protected IITKExtractionDAO itkExtractionDAO;
    IBiomasseExtractor hauteurVegetalExtractor;

    // TODO
    IBiomasseExtractor biomasseProducionExtractor;
    IBiomasseExtractor laiExtractor;

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IExtractor#extract(org.inra.ecoinfo.extraction
     *                           .IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        statusBar.setProgress(0);
        int extractionResult = 3;
        this.extractCouverts((BiomasseParameterVO) parameters);
        extractionResult = tryExtract(parameters, laiExtractor, extractionResult);
        statusBar.setProgress(17);
        extractionResult = tryExtract(parameters, hauteurVegetalExtractor, extractionResult);
        statusBar.setProgress(34);
        extractionResult = tryExtract(parameters, biomasseProducionExtractor, extractionResult);
        statusBar.setProgress(50);
        // TODO
        /**
         * autre type de données biomasse
         */
        if (extractionResult == 0) {
            final String message = this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT);
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    BiomasseExtractor.BUNDLE_SOURCE_PATH,
                    BiomasseExtractor.MSG_EXTRACTION_ABORTED)), Notification.ERROR,
                    message, (Utilisateur) this.policyManager.getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH, message));
        }
    }

    private void extractCouverts(BiomasseParameterVO parameter) {
        parameter.getParameters().put(Parcelle.class.getSimpleName(),
                this.itkExtractionDAO.extractCouverts(parameter.getSelectedTraitementProgrammes()));
    }

    /**
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = hauteurVegetalExtractor.getExtractionSize(parameters);
        size += biomasseProducionExtractor.getExtractionSize(parameters);
        size += laiExtractor.getExtractionSize(parameters);
        return size;
    }

    /**
     * @param biomasseExtractionDAO the biomasseExtractionDAO to set
     */
    public void setBiomasseExtractionDAO(IBiomasseExtractionDAO biomasseExtractionDAO) {
        this.biomasseExtractionDAO = biomasseExtractionDAO;
    }

    /**
     * @param biomasseProducionExtractor
     */
    public void setBiomasseProducionExtractor(IBiomasseExtractor biomasseProducionExtractor) {
        this.biomasseProducionExtractor = biomasseProducionExtractor;
    }

    /**
     * Sets the extraction.
     *
     * @param extraction the new extraction
     * @see org.inra.ecoinfo.extraction.IExtractor#setExtraction(org.inra.ecoinfo
     * .config.Extraction)
     */
    @Override
    public final void setExtraction(final Extraction extraction) {

    }

    /**
     * @param hauteurVegetalExtractor the hauteurVegetalExtractor to set
     */
    public void setHauteurVegetalExtractor(IBiomasseExtractor hauteurVegetalExtractor) {
        this.hauteurVegetalExtractor = hauteurVegetalExtractor;
    }

    /**
     * @param itkExtractionDAO the itkExtractionDAO to set
     */
    public void setItkExtractionDAO(IITKExtractionDAO itkExtractionDAO) {
        this.itkExtractionDAO = itkExtractionDAO;
    }

    /**
     * @param laiExtractor
     */
    public void setLaiExtractor(IBiomasseExtractor laiExtractor) {
        this.laiExtractor = laiExtractor;
    }

    private int tryExtract(final IParameter parameters, IBiomasseExtractor extractor, int extractionResult) throws BusinessException {
        final Collection variables = (Collection) parameters.getParameters().get(
                Variable.class.getSimpleName().toLowerCase().concat(extractor.getExtractCode()));
        try {
            if (!CollectionUtils.isEmpty(variables)) {
                extractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e instanceof NoExtractionResultException) {
                return --extractionResult;
            } else {
                throw e;
            }
        }
        return extractionResult;
    }

}
