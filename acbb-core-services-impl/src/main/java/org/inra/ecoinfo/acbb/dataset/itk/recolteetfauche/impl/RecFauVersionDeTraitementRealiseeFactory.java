package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultVersionDeTraitementRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * A factory for creating SemisVersionDeTraitementRealisee objects.
 */
public class RecFauVersionDeTraitementRealiseeFactory extends
        DefaultVersionDeTraitementRealiseeFactory<RecFauLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl.messages";
    private static final String PROPERTY_CST_NO_SEED_FOR_CROP = "PROPERTY_CST_NO_SEED_FOR_CROP";

    /**
     * Gets the new rotation.
     *
     * @param lineNumber
     * @param line
     * @return the new rotation
     * @link(int) the line number
     * @link(LineRecord) the line
     */
    VersionTraitementRealisee getNewRotation(final RecFauLineRecord line) {
        final VersionTraitementRealisee versionTraitementRealisee = new VersionTraitementRealisee(
                0, line.getTempo().getVersionDeTraitement(), line.getTempo().getParcelle());
        return versionTraitementRealisee;
    }

    /**
     * @param line
     * @param infoReport
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.IversionDeTraitementRealiseeFactory#getOrCreateVersiontraitementRealise(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public VersionTraitementRealisee getOrCreateVersiontraitementRealise(
            final RecFauLineRecord line, final ErrorsReport errorsReport,
            final InfoReport infoReport) {
        final Tempo tempo = line.getTempo();
        assert tempo.getVersionDeTraitement() != null : "la version de traitement ne doit pas être nulle";
        VersionTraitementRealisee versionDeTraitementRealisee = tempo
                .getVersionTraitementRealisee();
        final RotationDescriptor rotation = this.getRotationDescriptor(line);
        line.setRotationDescriptor(rotation);
        if (line.isRotation() && versionDeTraitementRealisee == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    RecFauVersionDeTraitementRealiseeFactory.BUNDLE_NAME,
                    RecFauVersionDeTraitementRealiseeFactory.PROPERTY_CST_NO_SEED_FOR_CROP), line
                    .getOriginalLineNumber()));
        } else if (versionDeTraitementRealisee == null) {
            versionDeTraitementRealisee = this.getNewRotation(line);
        }
        try {
            if (versionDeTraitementRealisee != null) {
                this.versionDeTraitementRealiseeDAO.saveOrUpdate(versionDeTraitementRealisee);
            }
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        tempo.setVersionTraitementRealisee(versionDeTraitementRealisee);
        return versionDeTraitementRealisee;
    }

    /**
     * Gets the semis rotation descriptor.
     *
     * @param line
     * @return the rotation descriptor
     * @link(LineRecord) the line
     */
    RotationDescriptor getRotationDescriptor(final RecFauLineRecord line) {
        return new RotationDescriptor(line, line.getTempo().getVersionDeTraitement());
    }

    /**
     * @param anneeRealisee
     * @param line
     * @param errorsReport
     * @throws PersistenceException
     */
    public void updateAnneeRealisee(final AnneeRealisee anneeRealisee, final RecFauLineRecord line,
                                    final ErrorsReport errorsReport) throws PersistenceException {
        ((RecFauAnneeRealiseeFactory) this.anneeRealiseeFactory).updateAnneeRealisee(anneeRealisee,
                line, errorsReport);
    }
}
