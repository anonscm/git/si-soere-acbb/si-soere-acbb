/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.jpa;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.IBiomassProductionDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction_;
import org.inra.ecoinfo.acbb.dataset.biomasse.jpa.JPABiomasseDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPABiomassProductionDAO extends JPABiomasseDAO<BiomassProduction> implements
        IBiomassProductionDAO {

    /**
     * @param version
     * @param date
     * @return
     */
    @Override
    public Optional<BiomassProduction> getByNKey(final VersionFile version, final LocalDate date) {

        CriteriaQuery<BiomassProduction> query = builder.createQuery(BiomassProduction.class);
        Root<BiomassProduction> bp = query.from(BiomassProduction.class);
        query.where(
                builder.equal(bp.get(BiomassProduction_.version), version),
                builder.equal(bp.get(BiomassProduction_.date), date)
        );
        query.select(bp);
        return getOptional(query);
    }
}
