/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.itk.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.utils.DateUtil;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author ptcherniati
 */
public class VegetalPeriode implements Comparable<VegetalPeriode> {

    public static final String BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.itk.messages";
    public static final String PROPERTY_MSG_NO_DEFINED_COUVERT = "PROPERTY_MSG_NO_DEFINED_COUVERT";
    public static final String PROPERTY_MSG_NO_DEFINED_SOWING_DATE = "PROPERTY_MSG_NO_DEFINED_SOWING_DATE";
    public static final String PROPERTY_MSG_NO_DEFINED_HARVEST_DATE = "PROPERTY_MSG_NO_DEFINED_HARVEST_DATE";
    Parcelle plot;
    TraitementProgramme treatment;
    LocalDate beginDate = DateUtil.MIN_LOCAL_DATE;
    LocalDate endDate = DateUtil.MAX_LOCAL_DATE;
    String plantCover;
    /**
     *
     */
    public VegetalPeriode() {
    }

    /**
     * @param result
     */
    public VegetalPeriode(Object[] result) {
        if (result == null) {
            result = new Object[0];
        }
        this.plot = (Parcelle) (result.length > 1 ? result[0] : new Parcelle());
        this.treatment = (TraitementProgramme) (result.length > 2 ? result[1]
                : new TraitementProgramme());
        this.beginDate = (LocalDate) (result.length > 3 ? result[2] : DateUtil.MIN_LOCAL_DATE);
        this.endDate = (LocalDate) (result.length > 4 ? result[3] : DateUtil.MAX_LOCAL_DATE);
        this.plantCover = (String) (result.length > 5 ? result[4] : org.apache.commons.lang.StringUtils.EMPTY);
    }

    private static String getDefaultCover(String defaultCover) {
        if (Strings.isNullOrEmpty(defaultCover)) {
            return RecorderACBB.getACBBMessageWithBundle(
                    VegetalPeriode.BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH,
                    VegetalPeriode.PROPERTY_MSG_NO_DEFINED_COUVERT);

        } else {
            return defaultCover;
        }
    }

    @Override
    public int compareTo(VegetalPeriode o) {
        final int plotCompare = o.plot.compareTo(this.plot);
        final int treatmentCompare = o.treatment.compareTo(this.treatment);
        if (treatmentCompare != 0) {
            return treatmentCompare;
        } else if (plotCompare != 0) {
            return plotCompare;
        } else {
            return o.beginDate.compareTo(this.beginDate);
        }
    }

    /**
     * @return
     */
    public LocalDate getBeginDate() {
        return this.beginDate == null ? DateUtil.MIN_LOCAL_DATE : this.beginDate;
    }

    /**
     * @return the couvert
     */
    public String getCouvert() {
        return getCouvert(null);
    }

    /**
     * @param defaultCover
     * @return the couvert
     */
    public String getCouvert(String defaultCover) {
        return Strings.isNullOrEmpty(this.plantCover) ? getDefaultCover(defaultCover) : this.plantCover;
    }


    /**
     * @return the dateDebut
     */
    public String getDateDebut() {
        if (null == this.beginDate) {
            return "--/--/----";
        }
        return DateUtil.MIN_LOCAL_DATE.equals(this.beginDate) ? RecorderACBB
                .getACBBMessageWithBundle(VegetalPeriode.BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH,
                        VegetalPeriode.PROPERTY_MSG_NO_DEFINED_SOWING_DATE) :

                DateUtil.getUTCDateTextFromLocalDateTime(beginDate, DateUtil.DD_MM_YYYY);
    }

    /**
     * @return the dateFin
     */
    public String getDateFin() {
        if (null == this.endDate) {
            return "--/--/----";
        }
        return DateUtil.MAX_LOCAL_DATE.equals(this.endDate) ? RecorderACBB
                .getACBBMessageWithBundle(VegetalPeriode.BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH,
                        VegetalPeriode.PROPERTY_MSG_NO_DEFINED_HARVEST_DATE) :
                DateUtil.getUTCDateTextFromLocalDateTime(endDate, DateUtil.DD_MM_YYYY);
    }

    /**
     * @return
     */
    public LocalDate getEndDate() {
        return this.endDate == null ? DateUtil.MAX_LOCAL_DATE : this.endDate;
    }

    /**
     * @return
     */
    public Parcelle getPlot() {
        return this.plot;
    }

    /**
     * @return
     */
    public TraitementProgramme getTreatment() {
        return this.treatment;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VegetalPeriode other = (VegetalPeriode) obj;
        if (!Objects.equals(this.plot, other.plot)) {
            return false;
        }
        if (!Objects.equals(this.treatment, other.treatment)) {
            return false;
        }
        if (!Objects.equals(this.beginDate, other.beginDate)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return Objects.equals(this.plantCover, other.plantCover);
    }

}
