package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.WsolTypeObjectif;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.WsolTypeOutil;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;

import java.util.List;

/**
 * The Class LineRecord for wsol.
 */
public class LineRecord extends AbstractLineRecord<LineRecord, ListeItineraire> {

    /**
     * The rang <int>.
     */
    int rang;

    /**
     * The numero passage <int>.
     */
    int numeroPassage;

    /**
     * Instantiates a new line record.
     *
     * @param lineCount <long> the line count
     * @link(SuiviParcelle) the suivi parcelle {@link SuiviParcelle} the suivi parcelle
     */
    LineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * Copy.
     *
     * @param line
     * @link(AbstractLineRecord) the line
     * @link(AbstractLineRecord) the line
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.AbstractLineRecord
     * #copy(org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord)
     */
    @Override
    public void copy(final LineRecord line) {
        super.copy(line);
        this.rang = line.getRang();
        this.numeroPassage = line.getNumeroPassage();
    }

    /**
     * Gets the numero passage.
     *
     * @return the numero passage
     */
    public int getNumeroPassage() {
        return this.numeroPassage;
    }

    /**
     * Sets the numero passage.
     *
     * @param numeroPassage the new numero passage
     */
    public final void setNumeroPassage(final int numeroPassage) {
        this.numeroPassage = numeroPassage;
    }

    /**
     * Gets the rang.
     *
     * @return the rang
     */
    public int getRang() {
        return this.rang;
    }

    /**
     * Sets the rang.
     *
     * @param rang int the new rang
     */
    public final void setRang(final int rang) {
        this.rang = rang;
    }

    /**
     * Update.
     *
     * @param rang2           int the rang2
     * @param numero          int the numero
     * @param variablesValues
     * @link(WsolTypeOutil) the typeoutil2
     * @link(List<WsolTypeObjectif>) the objectifs2
     * @link(List<VariableValue>) the variables values {@link WsolTypeOutil} the typeoutil2
     * {@link List<WsolTypeObjectif>} the objectifs2 {@link List
     * <VariableValue>} the variables values
     */
    public void update(final int rang2, final int numero,
                       final List<ACBBVariableValue<ListeItineraire>> variablesValues) {
        this.rang = rang2;
        this.numeroPassage = numero;
        this.setVariablesValues(variablesValues);
    }

}
