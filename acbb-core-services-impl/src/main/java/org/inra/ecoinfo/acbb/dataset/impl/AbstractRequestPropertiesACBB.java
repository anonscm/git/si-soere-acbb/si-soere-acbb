/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * abstract implementation of {@link IRequestPropertiesACBB}.
 *
 * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB The Class
 * AbstractRequestPropertiesACBB.
 */
public abstract class AbstractRequestPropertiesACBB implements IRequestPropertiesACBB {

    /**
     * The Constant CST_HYPHEN.
     */
    protected static final String CST_HYPHEN = "-";

    /**
     * The Constant CST_SLASH.
     */
    protected static final String CST_SLASH = "/";

    /**
     * The Constant REGEX_END_WORD.
     */
    protected static final String REGEX_END_WORD = "$";

    /**
     * The Constant CST_UNDERSCORE.
     */
    protected static final String CST_UNDERSCORE = "_";

    /**
     * The Constant CST_NEW_LINE.
     */
    protected static final String CST_NEW_LINE = "\n";

    /**
     * The Constant CST_DOT.
     */
    protected static final String CST_DOT = ".";

    /**
     * The Constant PATTERN_FILE_NAME.
     */
    protected static final String PATTERN_FILE_NAME = new StringBuffer("^(.*)")
            .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
            .append("(.*")
            .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
            .append(".*)")
            .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
            .append("(.*)")
            .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
            .append("(.*)")
            .append(".")
            .append(IRequestPropertiesACBB.FORMAT_FILE)
            .append(AbstractRequestPropertiesACBB.REGEX_END_WORD)
            .toString();
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant INCLUSIVE_ADD_SORTED_MAP @link(String).
     */
    static final String INCLUSIVE_ADD_SORTED_MAP = "\0";

    /**
     * The localization manager.
     */
    ILocalizationManager localizationManager;

    /**
     * The site.
     */
    SiteACBB site = null;

    /**
     * The parcelle.
     */
    Parcelle parcelle = null;

    /**
     * The traitement.
     */
    TraitementProgramme traitement = null;

    /**
     * The suivi parcelle.
     */
    SuiviParcelle suiviParcelle = null;

    /**
     * The date de debut.
     */
    LocalDateTime dateDeDebut;

    /**
     * The date of the treatment start on plot.
     */
    LocalDate dateDebutTraitement;

    /**
     * The date de fin.
     */
    LocalDateTime dateDeFin;

    /**
     * The commentaire.
     */
    String commentaire;

    /**
     * The version.
     */
    int version;

    /**
     * The version de traitement.
     */
    VersionDeTraitement versionDeTraitement;

    /**
     * The date format.
     */
    String dateFormat = DateUtil.DD_MM_YYYY;

    /**
     * The dates.
     */
    volatile SortedMap<String, Boolean> dates = new TreeMap();

    /**
     * The doublons line.
     */
    ITestDuplicates doublonsLine;

    IDatasetConfiguration datasetConfiguration;

    /**
     * Adds the date.
     *
     * @param stringDate
     * @throws BadExpectedValueException the bad expected value exception
     * @throws DateTimeException         the parse exception
     * @link(String) the string date
     * @link(String) the string date
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#addDate(java.lang.String)
     */
    @Override
    public void addDate(final String stringDate) throws BadExpectedValueException, DateTimeException {
        try {
            if (this.dates != null && stringDate != null) {
                this.dates.put(stringDate, true);
            }
        } catch (final Exception e) {
            String message = org.apache.commons.lang.StringUtils.EMPTY;
            String formatDateDebut;
            String formatDateFin;
            formatDateDebut = DateUtil.getUTCDateTextFromLocalDateTime(this.dateDeDebut, DateUtil.DD_MM_YYYY_HH_MM);
            formatDateFin = DateUtil.getUTCDateTextFromLocalDateTime(this.dateDeFin, DateUtil.DD_MM_YYYY_HH_MM);
            message = String.format(
                    this.getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_DATE_OFF_LIMITS), stringDate,
                    formatDateDebut, formatDateFin);
            throw new BadExpectedValueException(message, e);
        }
    }

    /**
     * @param dateFin
     * @param chronoUnit
     * @param step
     * @param dateFormatForCompare
     */
    protected void buildTree(final LocalDateTime dateFin, final ChronoUnit chronoUnit, final int step,
                             String dateFormatForCompare) {
        if (this.dateDeDebut != null && dateFin != null && !dateDeDebut.isAfter(dateFin)) {
            LocalDateTime localDateTimeDebut = dateDeDebut;
            LocalDateTime localDateTimeFin = dateFin.plus(step, chronoUnit);
            LocalDateTime currentDate = localDateTimeDebut;
            this.dates = this.dates.subMap(
                    DateUtil.getUTCDateTextFromLocalDateTime(localDateTimeDebut, dateFormatForCompare),
                    DateUtil.getUTCDateTextFromLocalDateTime(localDateTimeFin, dateFormatForCompare)
                            + AbstractRequestPropertiesACBB.INCLUSIVE_ADD_SORTED_MAP);
            int a = 0;
            while (currentDate.isBefore(localDateTimeFin)) {
                String utcDateTextFromLocalDate = DateUtil.getUTCDateTextFromLocalDateTime(currentDate, dateFormatForCompare);
                if (!this.dates.containsKey(utcDateTextFromLocalDate)) {
                    this.dates.put(utcDateTextFromLocalDate, Boolean.FALSE);
                } else {
                }
                currentDate = currentDate.plus(step, chronoUnit);
                a++;
            }
        } else {
            this.initDate();
        }
    }

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getCommentaire()
     */
    @Override
    public String getCommentaire() {
        return this.commentaire;
    }

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setCommentaire(java.lang.String)
     */
    @Override
    public final void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;

    }

    /**
     * Gets the date of the treatment start on plot.
     *
     * @return the date of the treatment start on plot
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getDateDebutTraitement()
     */
    @Override
    public LocalDate getDateDebutTraitement() {
        return this.dateDebutTraitement;
    }

    /**
     * Sets the date of the treatment start on plot.
     *
     * @param dateDebutTraitement the new date of the treatment start on plot
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setDateDebutTraitement(java.time.LocalDateTime)
     */
    @Override
    public final void setDateDebutTraitement(final LocalDate dateDebutTraitement) {
        this.dateDebutTraitement = dateDebutTraitement;
    }

    /**
     * Gets the date de debut.
     *
     * @return the date de debut
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getDateDeDebut()
     */
    @Override
    public LocalDateTime getDateDeDebut() {
        return this.dateDeDebut;
    }

    /**
     * Sets the date de debut.
     *
     * @param dateDeDebut the new date de debut
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setDateDeDebut(java.time.LocalDateTime)
     */
    @Override
    public void setDateDeDebut(final LocalDateTime dateDeDebut) {
        this.dateDeDebut = dateDeDebut;
    }

    /**
     * Gets the date de fin.
     *
     * @return the date de fin
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getDateDeFin()
     */
    @Override
    public LocalDateTime getDateDeFin() {
        return this.dateDeFin;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setDateDeFin(java.time.LocalDateTime)
     */
    @Override
    public void setDateDeFin(final LocalDateTime dateDeFin) {
        this.dateDeFin = dateDeFin;
    }

    /**
     * Gets the date format.
     *
     * @return the date format
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getDateFormat()
     */
    @Override
    public String getDateFormat() {
        return this.dateFormat;
    }

    /**
     * Sets the date format.
     *
     * @param dateFormat the new date format
     * @see
     */
    @Override
    public final void setDateFormat(final String dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * Gets the dates.
     *
     * @return the dates
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getDates()
     */
    @Override
    public SortedMap<String, Boolean> getDates() {
        return this.dates;
    }

    /**
     * Sets the dates.
     *
     * @param dates the new dates
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setDates(java.util.SortedMap)
     */
    @Override
    public final void setDates(final SortedMap<String, Boolean> dates) {
        this.dates = dates;
    }

    /**
     * Gets the doublons line.
     *
     * @return the doublons line
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getDoublonsLine()
     */
    @Override
    public ITestDuplicates getDoublonsLine() {
        return this.doublonsLine;
    }

    /**
     * Sets the doublons line.
     *
     * @param doublonsLine the new doublons line
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setDoublonsLine(org.inra.ecoinfo.acbb.dataset.ITestDuplicates)
     */
    @Override
    public final void setDoublonsLine(final ITestDuplicates doublonsLine) {
        this.doublonsLine = doublonsLine;
    }

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getLocalizationManager()
     */
    @Override
    public ILocalizationManager getLocalizationManager() {
        return this.localizationManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * @param version
     * @return
     */
    @Override
    public String getNomDeFichier(VersionFile version) {
        return version == null ? org.apache.commons.lang.StringUtils.EMPTY : version.getDataset().buildDownloadFilename(
                this.datasetConfiguration);
    }

    /**
     * Gets the parcelle.
     *
     * @return the parcelle
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getParcelle()
     */
    @Override
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    /**
     * Sets the parcelle.
     *
     * @param parcelle the new parcelle
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setParcelle(org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle)
     */
    @Override
    public final void setParcelle(final Parcelle parcelle) {
        this.parcelle = parcelle;

    }

    /**
     * Gets the site.
     *
     * @return the site
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getSite()
     */
    @Override
    public SiteACBB getSite() {
        return this.site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setSite(org.inra.ecoinfo.acbb.refdata.site.SiteACBB)
     */
    @Override
    public final void setSite(final SiteACBB site) {
        this.site = site;
    }

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getSuiviParcelle()
     */
    @Override
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setSuiviParcelle(org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle)
     */
    @Override
    public final void setSuiviParcelle(final SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Gets the traitement.
     *
     * @return the traitement
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getTraitement()
     */
    @Override
    public TraitementProgramme getTraitement() {
        return this.traitement;
    }

    /**
     * Sets the traitement.
     *
     * @param traitement the new traitement
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setTraitement(org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme)
     */
    @Override
    public final void setTraitement(final TraitementProgramme traitement) {
        this.traitement = traitement;

    }

    /**
     * Gets the version.
     *
     * @return the version
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getVersion()
     */
    @Override
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setVersion(int)
     */
    @Override
    public final void setVersion(final int version) {
        this.version = version;
    }

    /**
     * Gets the version de traitement.
     *
     * @return the version de traitement
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getVersionDeTraitement()
     */
    @Override
    public VersionDeTraitement getVersionDeTraitement() {
        return this.versionDeTraitement;
    }

    /**
     * Sets the version de traitement.
     *
     * @param versionDeTraitement the new version de traitement
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setVersionDeTraitement(org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement)
     */
    @Override
    public final void setVersionDeTraitement(final VersionDeTraitement versionDeTraitement) {
        this.versionDeTraitement = versionDeTraitement;
    }

    /**
     * Inits the date.
     *
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#initDate()
     */
    @Override
    public void initDate() {
        this.dates = new TreeMap();

    }

    /**
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin
     * @param chronoUnit
     * @param step
     * @link(Date)
     * @link(int) the field step
     * @link(int) the step
     * @link(Date) the date de fin
     * @link(int) the field step
     * @link(int) the step
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#setDateDeFin(java.time.LocalDateTime, int,
     * int)
     */
    @Override
    public final void setDateDeFin(final LocalDateTime dateDeFin, final ChronoUnit chronoUnit, final int step) {
        this.dateDeFin = dateDeFin;
        this.buildTree(dateDeFin, chronoUnit, step, RecorderACBB.YYYYMMJJHHMMSS);
    }

    /**
     * Test dates.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @link(BadsFormatsReport) the bads formats report
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#testDates(org.inra.ecoinfo.utils.exceptions.BadsFormatsReport)
     */
    @Override
    public void testDates(final BadsFormatsReport badsFormatsReport) {
        if (this.dateDeDebut == null || this.dateDeFin == null) {
            return;
        }
        if (this.dates == null || this.dates.size() < 1) {
            badsFormatsReport.addException(new BadExpectedValueException(this
                    .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_UNDEFINED_PERIOD)));
        }
    }

    /**
     * Test non missing dates.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @link(BadsFormatsReport) the bads formats report
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#testNonMissingDates(org.inra.ecoinfo.utils.exceptions.BadsFormatsReport)
     */
    @Override
    public void testNonMissingDates(final BadsFormatsReport badsFormatsReport) {
        if (this.dates == null || this.dates.isEmpty()) {
            return;
        }
        for (final Map.Entry<String, Boolean> dateEntry : this.dates.entrySet()) {
            if (!dateEntry.getValue()) {
                try {
                    String formatDate;
                    String formatDatedebut;
                    String formatDateFin;
                    formatDate = DateUtil.getUTCDateTextFromLocalDateTime(DateUtil.readLocalDateFromText(RecorderACBB.YYYYMMJJHHMMSS, dateEntry.getKey()), RecorderACBB.DD_MM_YYYY_HHMMSS_DISPLAY);
                    formatDatedebut = DateUtil.getUTCDateTextFromLocalDateTime(dateDeDebut, DateUtil.DD_MM_YYYY);
                    formatDateFin = DateUtil.getUTCDateTextFromLocalDateTime(dateDeFin, DateUtil.DD_MM_YYYY);
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(
                            RecorderACBB.getACBBMessageWithBundle(
                                    IRequestPropertiesACBB.BUNDLE_NAME,
                                    IRequestPropertiesACBB.PROPERTY_MSG_MISSING_DATE), formatDate,
                            formatDatedebut, formatDateFin)));
                } catch (final DateTimeException e) {
                    LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("can't parse date");
                }
            }
        }
    }

    /**
     * Date to string.
     *
     * @param dateString
     * @param timeString
     * @return the string
     * @link(String)
     * @link(String) the time
     * @link(String) the date
     * @link(String) the time
     * @see org.inra.ecoinfo.acbb.dataset.flux.IRequestPropertiesFluxTours#dateToString(java.lang.String,
     * java.lang.String)
     */

    public String dateToStringYYYYMMJJHHMMSS(final String dateString, String timeString) {
        if (Strings.isNullOrEmpty(dateString)) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        if (Strings.isNullOrEmpty(timeString)) {
            timeString = "00:00:00";
        }
        timeString = (timeString + ":00").substring(0, DateUtil.HH_MM_SS.length());
        LocalDateTime date = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM_SS, String.format("%s %s", dateString, timeString));
        return DateUtil.getUTCDateTextFromLocalDateTime(date, RecorderACBB.YYYYMMJJHHMMSS);
    }

}
