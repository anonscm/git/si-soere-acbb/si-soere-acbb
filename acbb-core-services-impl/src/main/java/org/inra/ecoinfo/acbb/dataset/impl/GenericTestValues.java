/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.ITestValues;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.exceptions.EmptyFileException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;

/**
 * The Class GenericTestValues.
 *
 * ana generic implementation of {@link ITestValues}
 *
 * @author Tcherniatinsky Philippe
 */
public class GenericTestValues implements ITestValues {

    /**
     * The LOGGER.
     */
    static protected final Logger LOGGER = LoggerFactory.getLogger(GenericTestValues.class);
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;

    /**
     * Instantiates a new generic test values.
     */
    public GenericTestValues() {
        super();
    }

    /**
     *
     * @param column
     * @param variablesTypesDonnees
     * @param floatValue
     * @return
     * @throws NumberFormatException
     */
    protected boolean canTest(final Column column,
                              final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                              final Float floatValue) throws NumberFormatException {
        boolean canTest = RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType());
        canTest = canTest && variablesTypesDonnees.containsKey(column.getName());
        canTest = canTest
                && ACBBUtils.CST_INVALID_BAD_MEASURE != floatValue.intValue();
        return canTest;
    }

    /**
     * Check date type value.
     *
     * @param values
     * @link(String[]) the values
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index int the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param requestPropertiesACBB
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @return the date {@link String[]} the values {@link BadsFormatsReport} the bads formats
     *         report {@link String} the value {@link Column} the column
     *         {@link DatatypeVariableUniteACBB} the variables types donnees
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     */
    protected LocalDate checkDateTypeValue(final String[] values,
                                           final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                           final String value, final Column column,
                                           final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                           final DatasetDescriptorACBB datasetDescriptor,
                                           final IRequestPropertiesACBB requestPropertiesACBB) {
        String dateFormat = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType();
        try {
            final LocalDate date = DateUtil.readLocalDateFromText(dateFormat, value);
            if (!value.equals(DateUtil.getUTCDateTextFromLocalDateTime(date, dateFormat))) {
                badsFormatsReport.addException(new NullValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE), value,
                        lineNumber, index + 1, column.getName(), dateFormat)));
                return null;
            }
            return date;
        } catch (final DateTimeException e) {
            if (!column.isInull() && !value.isEmpty()) {
                badsFormatsReport.addException(new NullValueException(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE), value,
                        lineNumber, index + 1, column.getName(), dateFormat)));
                return null;
            } else {
                return null;
            }
        }
    }

    /**
     * Check float type value.
     *
     * @param values
     * @link(String[]) the values
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index int the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param requestPropertiesACBB
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @return the float {@link String[]} the values {@link BadsFormatsReport} the bads formats
     *         report {@link String} the value {@link Column} the column
     *         {@link DatatypeVariableUniteACBB} the variables types donnees
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     */
    protected Float checkFloatTypeValue(final String[] values,
                                        final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                        final String value, final Column column,
                                        final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                        final DatasetDescriptorACBB datasetDescriptor,
                                        final IRequestPropertiesACBB requestPropertiesACBB) {
        Float floatValue = null;
        try {
            floatValue = Float.parseFloat(value);
            this.checkIntervalValue(badsFormatsReport, lineNumber, index, column,
                    variablesTypesDonnees, datasetDescriptor, floatValue);
        } catch (final NumberFormatException e) {
            badsFormatsReport.addException(new BadValueTypeException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_FLOAT_VALUE),
                    lineNumber, index + 1, column.getName(), value)));
        }
        return floatValue;
    }

    /**
     * Check integer type value.
     *
     * @param values
     * @link(String[]) the values
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param requestPropertiesACBB
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @return the int {@link String[]} the values {@link BadsFormatsReport} the bads formats report
     *         {@link String} the value {@link Column} the column {@link DatatypeVariableUniteACBB}
     *         the variables types donnees
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     */
    protected int checkIntegerTypeValue(final String[] values,
                                        final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                        final String value, final Column column,
                                        final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                        final DatasetDescriptorACBB datasetDescriptor,
                                        final IRequestPropertiesACBB requestPropertiesACBB) {
        try {
            final int intValue = Integer.parseInt(value);
            this.checkIntervalValue(badsFormatsReport, lineNumber, index, column,
                    variablesTypesDonnees, datasetDescriptor, (float) intValue);
            return intValue;
        } catch (final NumberFormatException e) {
            badsFormatsReport.addException(new BadValueTypeException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_INT_VALUE),
                    lineNumber, index + 1, column.getName(), value)));
            return ACBBUtils.CST_INVALID_BAD_MEASURE;
        }

    }

    /**
     * Check interval value.
     *
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber long the line number
     * @param index
     * @link(int) the index
     * @param column
     * @link(Column) the column
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param floatValue
     * @link(Float) the float value
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(Float) the float value
     */
    protected void checkIntervalValue(final BadsFormatsReport badsFormatsReport,
                                      final long lineNumber, final int index, final Column column,
                                      final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                      DatasetDescriptorACBB datasetDescriptor, final Float floatValue) {
        if (ACBBUtils.CST_INVALID_BAD_MEASURE == floatValue.intValue()) {
            return;
        }
        Float valeurMin = datasetDescriptor.getMins().get(column.getName());
        Float valeurMax = datasetDescriptor.getMaxs().get(column.getName());
        if (valeurMin != null && valeurMax != null) {
            this.test(valeurMin, valeurMax, floatValue, badsFormatsReport, lineNumber, index,
                    column);
        } else if (!this.canTest(column, variablesTypesDonnees, floatValue)) {
            final DatatypeVariableUniteACBB variableTypeDeDonnee = variablesTypesDonnees.get(column
                    .getName());
            valeurMin = valeurMin == null
                    ? (variableTypeDeDonnee == null || variableTypeDeDonnee.getValeurMin() == null ? Float.NEGATIVE_INFINITY : variableTypeDeDonnee.getValeurMin())
                    : valeurMin;
            valeurMax = valeurMax == null
                    ? (variableTypeDeDonnee == null || variableTypeDeDonnee.getValeurMax() == null ? Float.POSITIVE_INFINITY : variableTypeDeDonnee.getValeurMax())
                    : valeurMax;
            this.test(valeurMin, valeurMax, floatValue, badsFormatsReport, lineNumber, index,
                    column);
        }
    }

    /**
     * Check other type value.
     *
     * @param values
     * @link(String[]) the values
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param requestPropertiesACBB
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb {@link String []} the values
     *                               {@link BadsFormatsReport} the bads formats report
     *                               {@link String} the value {@link Column} the column
     *                               {@link DatatypeVariableUniteACBB} the variables types donnees
     */
    protected void checkOtherTypeValue(final String[] values,
                                       final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                       final String value, final Column column,
                                       final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                       final DatasetDescriptorACBB datasetDescriptor,
                                       final IRequestPropertiesACBB requestPropertiesACBB) {

    }

    /**
     * Check time type value.
     *
     * @param values
     * @param value
     * @link(String[]) the values
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param requestPropertiesACBB
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @return the date {@link String[]} the values {@link BadsFormatsReport} the bads formats
     *         report {@link String} the value {@link Column} the column
     *         {@link DatatypeVariableUniteACBB} the variables types donnees
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     */
    protected LocalTime checkTimeTypeValue(final String[] values,
                                           final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                           final String value, final Column column,
                                           final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                           final DatasetDescriptorACBB datasetDescriptor,
                                           final IRequestPropertiesACBB requestPropertiesACBB) {
        String localValue = value;
        String dateFormat = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType();
        try {
            LocalTime time;
            localValue = String.format("%-" + dateFormat.length() + "."
                    + dateFormat.length() + "s", value + ":00");
            if (localValue.startsWith("24:00")) {
                localValue = localValue.replaceFirst("24:00", "00:00");
            }
            time = DateUtil.readLocalTimeFromText(dateFormat, localValue);
            if (!localValue.equals(DateUtil.getUTCDateTextFromLocalDateTime(time, dateFormat))) {
                badsFormatsReport
                        .addException(new NullValueException(
                                String.format(RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_TIME),
                                        localValue, lineNumber, index + 1, column.getName(),
                                        dateFormat)));
            }
            return time;
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new NullValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_TIME),
                    localValue, lineNumber, index + 1, column.getName(), dateFormat)));
            return null;
        }
    }

    /**
     * Check value.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index int the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @param values
     * @link(String[]) the values
     * @param variablesTypesDonnees
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param requestPropertiesACBB
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(String[]) the values
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb {@link BadsFormatsReport} the bads
     *                               formats report {@link String} the value {@link Column} the
     *                               column {@link String[]} the values
     *                               {@link DatatypeVariableUniteACBB} the variables types donnees
     */
    protected void checkValue(final BadsFormatsReport badsFormatsReport, final long lineNumber,
                              final int index, final String value, final Column column, final String[] values,
                              final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                              final DatasetDescriptorACBB datasetDescriptor,
                              final IRequestPropertiesACBB requestPropertiesACBB) {
        String cleanValue = value;
        if (!column.isNullable() && org.apache.commons.lang.StringUtils.isEmpty(cleanValue)) {
            final String message = String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_VALUE),
                    lineNumber, index + 1, column.getName());
            final NullValueException badFormatException = new NullValueException(message);
            badsFormatsReport.addException(badFormatException);
        }
        if (cleanValue != null) {
            cleanValue = this.cleanValue(cleanValue);
        }
        final String valueType = column.getValueType();
        if (RecorderACBB.PROPERTY_CST_FLOAT_TYPE.equals(valueType) && cleanValue.length() > 0) {
            this.checkFloatTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                    column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        } else if (RecorderACBB.PROPERTY_CST_INTEGER_TYPE.equals(valueType)
                && cleanValue.length() > 0) {
            this.checkIntegerTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                    column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        } else if (column.isFlag()
                && RecorderACBB.PROPERTY_CST_DATE_TYPE.equals(column.getFlagType())) {
            this.checkDateTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                    column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        } else if (column.isFlag()
                && RecorderACBB.PROPERTY_CST_TIME_TYPE.equals(column.getFlagType())) {
            this.checkTimeTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                    column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        } else {
            this.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                    column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        }
    }

    /**
     * Clean value.
     *
     * @param value
     * @link(String) the value
     * @return the string {@link String} the value
     * @link(String) the value
     */
    protected String cleanValue(final String value) {
        String returnValue = value;
        returnValue = returnValue.replaceAll(RecorderACBB.CST_COMMA, RecorderACBB.CST_DOT);
        returnValue = returnValue.replaceAll(RecorderACBB.CST_SPACE, org.apache.commons.lang.StringUtils.EMPTY);
        return returnValue;
    }

    /**
     *
     * @param intervalDate
     * @return
     */
    protected String[] getIntervalDateFromVersion(IntervalDate intervalDate) {
        if (intervalDate == null) {
            return null;
        }
        String[] beginDate = intervalDate.getBeginDateToString().split(";");
        String[] endDate = intervalDate.getEndDateToString().split(";");
        String[] dates = new String[4];
        dates[0] = beginDate[0];
        dates[1] = beginDate[1];
        dates[2] = endDate[0];
        dates[3] = endDate[1];
        return dates;
    }

    /**
     *
     * @param versionFile
     * @return
     */
    protected IntervalDate getIntervalDateFromVersion(VersionFile versionFile) {
        if (versionFile == null) {
            return null;
        }
        try {
            IntervalDate intervalDate = new IntervalDate(versionFile.getDataset()
                    .getDateDebutPeriode(), versionFile.getDataset().getDateFinPeriode(),
                    RecorderACBB.DD_MM_YYYY_HHMMSS_DOUBLONS_FILE);
            return intervalDate;
        } catch (BadExpectedValueException e) {
            LOGGER.info("pas de version", e);
            return null;
        }
    }

    private ITestDuplicates getTestDuplicates(IRequestPropertiesACBB requestPropertiesACBB) {
        return requestPropertiesACBB.getTestDuplicates();
    }

    /**
     * Sets the datatypeVariableUniteACBBDao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     * acbbdao {@link IDatatypeVariableUniteACBBDAO} the new datatype unite
     * variable acbbdao
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    private void test(Float valeurMin, Float valeurMax, Float floatValue,
                      BadsFormatsReport badsFormatsReport, long lineNumber, int index, Column column) {
        final boolean isBadMaxValue = valeurMax != null && floatValue > valeurMax;
        final boolean isBadMinValue = valeurMin != null && floatValue < valeurMin;
        final boolean isOutOfRangeValue = isBadMinValue || isBadMaxValue;
        if (isOutOfRangeValue) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE),
                    lineNumber, index + 1, column.getName(), floatValue, valeurMin, valeurMax)));
        }

    }

    /**
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException
     */
    protected void testDuplicatesLineWithpublishedVersion(final long startline,
                                                          final CSVParser parser, final VersionFile versionFile,
                                                          final IRequestPropertiesACBB requestProperties, final String encoding,
                                                          final BadsFormatsReport badsFormatsReport,
                                                          final DatasetDescriptorACBB datasetDescriptor, final String datatypeName)
            throws BusinessException {
    }

    /**
     * Test values.
     *
     * @param startline long the startline
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile) the version file
     * @param requestProperties
     * @link(IRequestPropertiesACBB) the request properties
     * @param encoding
     * @link(String) the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param datatypeName
     * @link(String) the datatype name
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(String) the datatype name
     * @see org.inra.ecoinfo.acbb.dataset.ITestValues#testValues(long,
     *      com.Ostermiller.util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *      org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *      org.inra.ecoinfo.utils.exceptions.BadsFormatsReport)
     */
    @Override
    public void testValues(final long startline, final CSVParser parser,
                           final VersionFile versionFile, final IRequestPropertiesACBB requestProperties,
                           final String encoding, final BadsFormatsReport badsFormatsReport,
                           final DatasetDescriptorACBB datasetDescriptor, final String datatypeName)
            throws BusinessException {
        long lineNumber = startline;
        final long headerCountLine = lineNumber;
        final Map<String, DatatypeVariableUniteACBB> VariableNodeable = this.datatypeVariableUniteACBBDAO
                .getAllVariableTypeDonneesByDataTypeMapByVariableAffichage(datatypeName);
        String[] values;
        // On parcourt chaque ligne du fichier
        ITestDuplicates testDuplicates = this.getTestDuplicates(requestProperties);
        String[] dates = this.getIntervalDateFromVersion(this.getIntervalDateFromVersion(versionFile));
        try {
            while ((values = parser.getLine()) != null) {
                int index = 0;
                lineNumber++;
                if (testDuplicates != null) {
                    testDuplicates.addLine(values, lineNumber, dates, versionFile);
                }
                // On parcourt chaque colonne d'une ligne
                for (String value = values[index]; index < values.length; index++) {
                    if (index > datasetDescriptor.getColumns().size() - 1) {
                        break;
                    }
                    value = values[index];
                    final Column column = datasetDescriptor.getColumns().get(index);
                    this.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                            VariableNodeable, datasetDescriptor, requestProperties);
                }
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }
        if (lineNumber == headerCountLine) {
            badsFormatsReport.addException(new EmptyFileException(RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_NO_DATA)));
        }
        if (testDuplicates != null && testDuplicates.hasError()) {
            testDuplicates.addErrors(badsFormatsReport);
        }
    }

}
