/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IMesureSemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;

/**
 * The Class JPAMesureSemisDAO.
 */
public class JPAMesureSemisDAO extends AbstractJPADAO<MesureSemis> implements IMesureSemisDAO {

}
