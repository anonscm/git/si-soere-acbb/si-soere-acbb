package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisVariete;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * The Class ProcessRecordSemis.
 */
public class ProcessRecordSemis extends AbstractProcessRecordITK {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant SEMIS_BUNDLE_NAME @link(String).
     */
    static final String SEMIS_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.messages";
    /**
     * The Constant PROPERTY_MSG_INVALID_COUVERT_VEGETAL @link(String).
     */
    static final String PROPERTY_MSG_INVALID_COUVERT_VEGETAL = "PROPERTY_MSG_INVALID_COUVERT_VEGETAL";
    /**
     * The Constant PROPERTY_MSG_INVALID_NAMM @link(String).
     */
    static final String PROPERTY_MSG_INVALID_NAMM = "PROPERTY_MSG_INVALID_NAMM";
    /**
     * The Constant PROPERTY_MSG_INVALID_VARIETE @link(String).
     */
    static final String PROPERTY_MSG_INVALID_VARIETE = "PROPERTY_MSG_INVALID_VARIETE";
    /**
     * The Constant PROPERTY_MSG_INVALID_SPECIES @link(String).
     */
    static final String PROPERTY_MSG_INVALID_SPECIES = "PROPERTY_MSG_INVALID_SPECIES";
    /**
     * The Constant PROPERTY_MSG_INVALID_SEM_OBJECTIF @link(String).
     */
    static final String PROPERTY_MSG_INVALID_SEM_OBJECTIF = "PROPERTY_MSG_INVALID_SEM_OBJECTIF";
    /**
     * The Constant PROPERTY_MSG_UNFOUND_VARIETE @link(String).
     */
    private static final String PROPERTY_MSG_UNFOUND_VARIETE = "PROPERTY_MSG_UNFOUND_VARIETE";

    /**
     * @param version
     * @param lines
     * @param requestPropertiesITK
     * @param errorsReport
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK#buildNewLines(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.util.List, org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK)
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    protected void buildNewLines(final VersionFile version,
                                 final List<? extends AbstractLineRecord> lines, final ErrorsReport errorsReport,
                                 final IRequestPropertiesITK requestPropertiesITK) throws PersistenceException {
        final BuildSemisHelper instance = ApplicationContextHolder.getContext().getBean(
                BuildSemisHelper.class);
        instance.build(version, (List<LineRecord>) lines, errorsReport, requestPropertiesITK);
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param fileEncoding
     * @param requestProperties
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile finalVersionFile = versionFile;
        super.processRecord(parser, finalVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesITK) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            finalVersionFile = this.versionFileDAO.merge(finalVersionFile);
            final List<LineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, requestProperties, datasetDescriptor, errorsReport,
                    lineCount, dbVariables, lines, intervalDate);
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(finalVersionFile, lines, errorsReport,
                        (IRequestPropertiesITK) requestProperties);
            }
            this.notifyInfoMessages(errorsReport);
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param requestProperties
     * @param datasetDescriptor
     * @param errorsReport
     * @param lineCount
     * @param dbVariables
     * @param lines
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(CSVParser) the parser
     * @link(IRequestPropertiesACBB) the request properties
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(ErrorsReport) the errors report
     * @link(long) the line count
     * @link(List<VariableACBB>) the db variables
     * @link(List<LineRecord>) the lines
     */
    long readLines(final CSVParser parser, final IRequestPropertiesACBB requestProperties,
                   final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                   final long lineCount, final List<DatatypeVariableUniteACBB> dbVariables, final List<LineRecord> lines, IntervalDate intervalDate)
            throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final LineRecord lineRecord = new LineRecord(localLineCount);
            int genericIndex = this.getGenericColumns(errorsReport, localLineCount, cleanerValues,
                    lineRecord, datasetDescriptor, (IRequestPropertiesITK) requestProperties, intervalDate);
            for (final Entry<Integer, Column> entry : ((IRequestPropertiesITK) requestProperties)
                    .getValueColumns().entrySet()) {
                if (entry.getKey() >= values.length) {
                    break;
                }
                final String value = values[entry.getKey()];
                if (entry.getKey() < genericIndex || Strings.isNullOrEmpty(value)) {
                    continue;
                }
                ACBBVariableValue<ListeItineraire> variableValue = null;
                final DatatypeVariableUniteACBB dvu = dbVariables.get(entry.getKey());
                if (RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE.equals(entry.getValue()
                        .getFlagType())
                        && SemisVariete.SEMIS_VARIETE.equals(entry.getValue().getName())) {
                    if (lineRecord.getEspece() == null) {
                        continue;
                    }
                    String name = lineRecord.getEspece().getValeur();
                    ListeItineraire valeurQualitative = null;
                    try {
                        valeurQualitative = this.getListeItineraireDAO().getByNKey(name, Utils.createCodeFromString(value))
                                .orElseThrow(() -> new PersistenceException(
                                        ProcessRecordSemis.PROPERTY_MSG_UNFOUND_VARIETE));
                        variableValue = new ACBBVariableValue<>(dvu, valeurQualitative);
                    } catch (final PersistenceException e) {
                        errorsReport.addErrorMessage(String.format(RecorderACBB
                                        .getACBBMessageWithBundle(ProcessRecordSemis.SEMIS_BUNDLE_NAME,
                                                ProcessRecordSemis.PROPERTY_MSG_UNFOUND_VARIETE),
                                lineCount + 1, entry.getKey() + 1, value, entry.getValue()
                                        .getName()));
                    }
                    variableValue = new ACBBVariableValue<>(dvu, valeurQualitative);
                } else {
                    variableValue = this.getVariableValue(errorsReport, lineCount, entry.getKey(),
                            dvu, entry.getValue(), value);
                }
                if (entry.getKey() < datasetDescriptor.getUndefinedColumn()
                        && variableValue != null) {
                    lineRecord.getAttributesVariablesValues().add(variableValue);
                } else if (variableValue != null) {
                    lineRecord.getVariablesValues().add(variableValue);
                }
            }
            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }
}
