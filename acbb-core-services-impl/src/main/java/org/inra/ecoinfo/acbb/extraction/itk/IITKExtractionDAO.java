package org.inra.ecoinfo.acbb.extraction.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * The Interface ISemisExtractionDAO.
 */
public interface IITKExtractionDAO extends IDAO<AbstractIntervention> {

    /**
     * @param selectedTraitementProgrammes
     * @return
     */
    Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> extractCouverts(
            List<TraitementProgramme> selectedTraitementProgrammes);
}
