/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemisAttr;

import java.util.List;

/**
 * The Interface IValeurSemisAttrDAO.
 */
public interface IValeurSemisAttrDAO extends IDAO<ValeurSemisAttr> {

    /**
     * Delete all.
     *
     * @param valeurSemisAttrsToDelete
     * @link(List<ValeurSemisAttr>) the valeur semis attrs to delete
     */
    void deleteAll(List<ValeurSemisAttr> valeurSemisAttrsToDelete);
}
