package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertType;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * process the record of tillage files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
public class ProcessRecordFert extends AbstractProcessRecordITK {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant CST_BEAN_SESSION_PROPERTIES_TRAVAIL_DU_SOL @link(String).
     */
    static final String FERT_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.fertilisant.messages";

    /**
     * The Constant PROPERTY_MSG_INVALID_FERT_TYPE @link(String).
     */
    static final String PROPERTY_MSG_INVALID_FERT_TYPE = "PROPERTY_MSG_INVALID_FERT_TYPE";

    private IListeItineraireDAO listeItineraireDAO;

    /**
     * Instantiates a new process record wsol.
     */
    public ProcessRecordFert() {
        super();
    }

    /**
     * Builds the new line.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesITK
     * @link(VersionFile) the version
     * @link(List<? extends AbstractLineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(VersionFile) the version
     * @link(List<? extends AbstractLineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder#buildNewLine(org
     * .inra.ecoinfo.dataset.versioning.entity.VersionFile, java.util.LinkedList,
     * org.inra.ecoinfo.acbb.dataset.impl.AbstractACBBRecorder.ErrorsReport)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void buildNewLines(final VersionFile version,
                                 final List<? extends AbstractLineRecord> lines, final ErrorsReport errorsReport,
                                 final IRequestPropertiesITK requestPropertiesITK) {
        final BuildFertilisantHelper instance = ApplicationContextHolder.getContext().getBean(
                BuildFertilisantHelper.class);
        instance.build(version, (List<LineRecord>) lines, errorsReport, requestPropertiesITK);
    }

    /**
     * Builds the variables header and skip header.
     *
     * @param parser
     * @param requestPropertiesFert
     * @param datasetDescriptor
     * @return the list
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException                                         Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{DatasetDescriptorACBB the dataset descriptor
     */
    protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(final CSVParser parser,
                                                                                final RequestPropertiesFert requestPropertiesFert,
                                                                                DatasetDescriptorACBB datasetDescriptor) throws BusinessException, IOException {
        List<DatatypeVariableUniteACBB> dvus = new LinkedList();
        Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites = this.datatypeVariableUniteACBBDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode(this.datatypeName);
        for (final Entry<Integer, Column> colonneEntry : requestPropertiesFert.getValueColumns()
                .entrySet()) {
            Column colonne = colonneEntry.getValue();
            if (RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE.equals(colonne.getFlagType())
                    || RecorderACBB.PROPERTY_CST_DATE_TYPE.equals(colonne.getFlagType())
                    || RecorderACBB.PROPERTY_CST_TIME_TYPE.equals(colonne.getFlagType())) {
                continue;
            }
            if (colonne.isFlag()
                    && (colonne.getFlagType().equals(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE)
                    || colonne.getFlagType().equals(
                    RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE)
                    || colonne.getFlagType().equals(
                    RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE) || colonne
                    .getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE))) {
                final VariableACBB variable = (VariableACBB) this.variableDAO
                        .getByAffichage(colonne.getName())
                        .orElseThrow(() -> new BusinessException("bad variable"));
                dvus.add(dbDatatypeVariableUnites.get(variable.getCode()));
            }
        }
        for (int i = 0; i < datasetDescriptor.getEnTete(); i++) {
            parser.getLine();
        }
        return dvus;
    }

    private FertType getFertType(ErrorsReport errorsReport, long lineCount, int columnIndex,
                                 CleanerValues cleanerValues) {
        FertType fertType = null;
        String fertTypeName = null;
        try {
            fertTypeName = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, columnIndex).getMessage());
        }
        fertType = (FertType) this.listeItineraireDAO.getByNKey(FertType.FERT_TYPE, fertTypeName)
                .orElse(
                        this.listeItineraireDAO.getByNKey(FertType.FERT_TYPE, Utils.createCodeFromString(fertTypeName))
                                .orElse(null)
                );
        if (fertType == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordFert.FERT_BUNDLE_NAME,
                    ProcessRecordFert.PROPERTY_MSG_INVALID_FERT_TYPE), lineCount, columnIndex + 1,
                    fertTypeName));
        }
        return fertType;
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser)
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile finalVersionFile = versionFile;
        super.processRecord(parser, finalVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            String[] values = null;
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    (RequestPropertiesFert) requestProperties, datasetDescriptor);
            finalVersionFile = this.versionFileDAO.merge(finalVersionFile);
            final List<LineRecord> lines = new LinkedList();
            lineCount = datasetDescriptor.getEnTete();
            while ((values = parser.getLine()) != null) {
                lineCount++;
                final List<ACBBVariableValue<ListeItineraire>> variablesValues = new LinkedList();
                final CleanerValues cleanerValues = new CleanerValues(values);
                final LineRecord lineRecord = new LineRecord(lineCount);
                int columnIndex = this.getGenericColumns(errorsReport, lineCount, cleanerValues,
                        lineRecord, datasetDescriptor, (IRequestPropertiesITK) requestProperties, intervalDate);
                final int rang = this.getInt(errorsReport, lineCount, columnIndex++, cleanerValues,
                        datasetDescriptor);
                FertType fertType = this.getFertType(errorsReport, lineCount, columnIndex++,
                        cleanerValues);
                final Iterator<DatatypeVariableUniteACBB> variableIterator = dbVariables.iterator();
                while (variableIterator.hasNext()) {
                    ACBBVariableValue<ListeItineraire> variableValue = null;
                    final DatatypeVariableUniteACBB dbVariable = variableIterator.next();
                    if (cleanerValues.currentTokenIndex() >= values.length) {
                        break;
                    }
                    final String value = cleanerValues.nextToken();
                    if (Strings.isNullOrEmpty(value)) {
                        continue;
                    }
                    variableValue = this.getVariableValue(
                            errorsReport,
                            lineCount,
                            cleanerValues.currentTokenIndex() - 1,
                            dbVariable,
                            ((IRequestPropertiesITK) requestProperties).getValueColumns().get(
                                    cleanerValues.currentTokenIndex() - 1), value);
                    if (variableValue == null) {
                        continue;
                    }
                    // new ACBBVariableValue<ListeItineraire>(dbVariable, value);
                    if (Strings.isNullOrEmpty(variableValue.getValue())
                            && variableValue.getValeurQualitative() == null
                            && variableValue.getValeursQualitatives() == null) {
                        continue;
                    }
                    variablesValues.add(variableValue);
                }
                lineRecord.update(rang, fertType, variablesValues);
                lines.add(lineRecord);
            }
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(finalVersionFile, lines, errorsReport,
                        (IRequestPropertiesITK) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * @param listeItineraireDAO
     */
    @Override
    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }
}
