package org.inra.ecoinfo.acbb.extraction.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKExtractor;

/**
 * @author ptcherniati
 */
public class RecFauExtractor extends AbstractITKExtractor<RecFau> {

}
