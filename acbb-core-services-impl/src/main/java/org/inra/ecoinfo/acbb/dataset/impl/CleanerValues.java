package org.inra.ecoinfo.acbb.dataset.impl;

import java.util.Arrays;

/**
 * The Class CleanerValues.
 */
public class CleanerValues {

    /**
     * The value index @link(int).
     */
    int valueIndex = 0;

    /**
     * The values @link(String[]).
     */
    String[] values;

    /**
     * Instantiates a new cleaner values.
     *
     * @param val
     * @link(String[]) the val
     * @link(String[]) the val
     */
    public CleanerValues(final String[] val) {
        if (val == null) {
            this.values = new String[0];
        } else {
            this.values = Arrays.copyOf(val, val.length);
        }

    }

    /**
     * Current token.
     *
     * @return the string
     */
    public String currentToken() throws EndOfCSVLine {
        if (this.valueIndex >= this.values.length) {
            throw new EndOfCSVLine();
        }
        final String value = this.values[this.valueIndex];
        return value == null ? null : value.toLowerCase().trim();
    }

    /**
     * Current token index.
     *
     * @return the int
     */
    public int currentTokenIndex() {
        return this.valueIndex;
    }

    /**
     * Next token.
     *
     * @return the string
     */
    public String nextToken() throws EndOfCSVLine {
        if (this.valueIndex >= this.values.length) {
            throw new EndOfCSVLine();
        }
        final String value = this.values[this.valueIndex++];
        return value == null ? null : value.toLowerCase().trim();
    }
}
