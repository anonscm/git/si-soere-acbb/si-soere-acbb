package org.inra.ecoinfo.acbb.utils;

/**
 * The Class VariableDescriptor.
 */
public class VariableDescriptor {

    /**
     * The name @link(String).
     */
    String name;

    /**
     * The has quality class @link(boolean).
     */
    boolean hasQualityClass;

    /**
     * The quality class name @link(String).
     */
    String qualityClassName;

    /**
     * Instantiates a new variable descriptor.
     *
     * @param name the name
     */
    public VariableDescriptor(final String name) {
        super();
        this.name = name;
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful @see java.lang.Object#equals(java.lang.Object)
     * @link(Object) the obj
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final VariableDescriptor other = (VariableDescriptor) obj;
        if (this.hasQualityClass != other.hasQualityClass) {
            return false;
        }
        if (this.name == null) {
            return other.name == null;
        } else return this.name.equals(other.name);
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the quality class name.
     *
     * @return the quality class name
     */
    public String getQualityClassName() {
        return this.qualityClassName;
    }

    /**
     * Sets the quality class name.
     *
     * @param qualityClassName the new quality class name
     */
    public void setQualityClassName(final String qualityClassName) {
        this.qualityClassName = qualityClassName;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.hasQualityClass ? 1_231 : 1_237);
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        return result;
    }

    /**
     * Checks for quality class.
     *
     * @return true, if successful
     */
    public boolean hasQualityClass() {
        return this.hasQualityClass;
    }

    /**
     * Sets the checks for quality class.
     *
     * @param hasQualityClass the new checks for quality class
     */
    public void setHasQualityClass(final boolean hasQualityClass) {
        this.hasQualityClass = hasQualityClass;
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VariableDescriptor [name=" + this.name + ", hasQualityClass="
                + this.hasQualityClass + "]";
    }

}
