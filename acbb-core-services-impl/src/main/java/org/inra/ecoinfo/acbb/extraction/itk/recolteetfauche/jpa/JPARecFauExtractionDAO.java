/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.recolteetfauche.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau_;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.recolteetfauche.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
@SuppressWarnings("unchecked")
public class JPARecFauExtractionDAO extends AbstractJPAITKDAO<RecFau, RecFau, ValeurRecFau> {

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<RecFau> getMesureITKClass() {
        return RecFau.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurRecFau> getValeurITKClass() {
        return ValeurRecFau.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, RecFau> getMesureAttribute() {
        return ValeurRecFau_.recFau;
    }
}
