/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.hauteurvegetal.jpa;


import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.ValeurHauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.ValeurHauteurVegetal_;
import org.inra.ecoinfo.acbb.dataset.biomasse.jpa.AbstractJPABiomasseDAO;
import org.inra.ecoinfo.acbb.synthesis.hauteurvegetal.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPAHauteurVegetalExtractionDAO extends
        AbstractJPABiomasseDAO<HauteurVegetal, ValeurHauteurVegetal> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurHauteurVegetal> getValeurBiomassClass() {
        return ValeurHauteurVegetal.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurHauteurVegetal, HauteurVegetal> getMesureAttribute() {
        return ValeurHauteurVegetal_.hauteurVegetal;
    }

    /**
     * @return
     */
    @Override
    protected Class<HauteurVegetal> getBiomassClass() {
        return HauteurVegetal.class;
    }
}
