/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire_;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire_;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.synthesis.phytosanitaire.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.phytosanitaire.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author tcherniatinsky
 */
public class PhytosanitaireSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurPhytosanitaire> v = query.from(ValeurPhytosanitaire.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<ValeurPhytosanitaire, Phytosanitaire> s = v.join(ValeurPhytosanitaire_.phytosanitaire);
        Join<ValeurPhytosanitaire, ListeACBB> vq = v.join(ValeurIntervention_.valeurQualitative, JoinType.LEFT);
        final Join<ValeurPhytosanitaire, RealNode> varRn = v.join(ValeurIntervention_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Path<String> parcelleCode = s.join(AbstractIntervention_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        final Path<Float> valeur = v.get(ValeurPhytosanitaire_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(Phytosanitaire_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        final Path<String> vqValue = vq.get(ListeACBB_.valeur);
        Path<Long> idNode = node.get(NodeDataSet_.id);

        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                dateMesure,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                vqValue,
                                builder.avg(valeur),
                                idNode
                        )
                )
                .distinct(true)
                .where(
                        builder.equal(node.get(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.or(
                                        builder.isNull(valeur),
                                        builder.gt(valeur, -9999)
                                )
                        )
                )
                .groupBy(
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        vqValue,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure),
                        builder.asc(vqValue)
                );
        return getResultAsStream(query);
    }

}
