/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction;

import com.google.common.base.Strings;
import java.io.File;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IMotivation;
import org.inra.ecoinfo.extraction.IMotivationFormatter;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractRequestReminder extends AbstractOutputBuilder implements IMotivationFormatter {

    /**
     * The Constant BUNDLE_REMINDER_SOURCE_PATH @link(String).
     */
    protected static final String BUNDLE_REMINDER_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.reminder.messages";
    /**
     * The Constant CST_REQUEST_REMINDER @link(String).
     */
    protected static final String CST_REQUEST_REMINDER = "request_reminder";
    /**
     * The Constant PROPERTY_MSG_HEADER @link(String).
     */
    protected static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";
    protected static final String PROPERTY_MSG_EXTRACTION_DATE = "PROPERTY_MSG_EXTRACTION_DATE";
    protected static final String PROPERTY_MSG_WARNING_INCOMPLETE_DATA = "PROPERTY_MSG_WARNING_INCOMPLETE_DATA";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DATES @link(String).
     */
    protected static final String PROPERTY_MSG_SELECTED_DATES = "PROPERTY_MSG_SELECTED_DATES";
    /**
     * The Constant PROPERTY_MSG_SELECTED_TREATMENT @link(String).
     */
    protected static final String PROPERTY_MSG_SELECTED_TREATMENT = "PROPERTY_MSG_SELECTED_TREATMENT";
    protected static final String PROPERTY_MSG_SELECTED_SITES = "PROPERTY_MSG_SELECTED_SITES";
    protected static final String PATTERN_STRING_SITES_SUMMARY = "\t%s - %s";
    protected static final String PROPERTY_MSG_SELECTED_FILECOMPS = "PROPERTY_MSG_SELECTED_FILECOMPS";
    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLE @link(String).
     */
    protected static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DISPLAY @link(String).
     */
    protected static final String PROPERTY_MSG_SELECTED_DISPLAY = "PROPERTY_MSG_SELECTED_DISPLAY";
    /**
     * The Constant PROPERTY_MSG_COMMENT @link(String).
     */
    protected static final String PROPERTY_MSG_COMMENT = "PROPERTY_MSG_COMMENT";
    protected static final String PROPERTY_MSG_NO_COMMENT = "PROPERTY_MSG_NO_COMMENT";

    public String getCommentaire(final Map<String, Object> requestMetadatasMap) {
        final String commentaire = (String) requestMetadatasMap.get(PROPERTY_MSG_COMMENT);
        MDC.put("commentaire", commentaire);
        return commentaire;
    }

    public Set<FileComp> getFileComp(final Map<String, Object> requestMetadatasMap) {
        final Set<FileComp> filecomps = (Set<FileComp>) requestMetadatasMap
                .getOrDefault(FileComp.class.getSimpleName(), new TreeSet<>());
        MDC.put("filecomps", filecomps.stream()
                .map(f -> String.format("%s-%s:%s", f.getFileType().getCode(), f.getFileName(), f.getDescription()))
                .collect(Collectors.joining(",")));
        return filecomps;
    }

    public List<TraitementProgramme> getTreatments(final Map<String, Object> requestMetadatasMap) {
        final List<TraitementProgramme> treatments = (List<TraitementProgramme>) requestMetadatasMap
                .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
        Set<String> sites = new TreeSet<>();
        MDC.put("traitements", treatments.stream()
                .peek(
                        t -> sites.add(t.getSite().getName())
                )
                .map(t -> String.format("%s:%s (%s)", t.getSite().getCode(), t.getCode(), t.getAffichage()))
                .collect(Collectors.joining(",")));
        if (!CollectionUtils.isEmpty(sites)) {
            MDC.put("sites", sites.stream().collect(Collectors.joining(",")));
        }
        return treatments;
    }

    public List<SiteACBB> getSites(final Map<String, Object> requestMetadatasMap) {
        final List<SiteACBB> sites = (List<SiteACBB>) requestMetadatasMap.getOrDefault(SiteACBB.class.getSimpleName(), new ArrayList());
        if (!CollectionUtils.isEmpty(sites)) {
            MDC.put("sites", sites.stream()
                    .map(s -> s.getCode())
                    .collect(Collectors.joining(", ")));
        }
        return sites;
    }

    public DatesFormParamVO getDates(final Map<String, Object> requestMetadatasMap) {
        final DatesFormParamVO date = (DatesFormParamVO) requestMetadatasMap.get(DatesFormParamVO.class.getSimpleName());
        MDC.put("startDate", DateUtil.getUTCDateTextFromLocalDateTime(date.getDateStart(), DateUtil.DD_MM_YYYY));
        MDC.put("endDate", DateUtil.getUTCDateTextFromLocalDateTime(date.getDateEnd(), DateUtil.DD_MM_YYYY));
        return date;
    }

    public Map<String, List<Variable>> getVariables(final Map<String, Object> requestMetadatasMap, String[] datatypeCodes, String[] datatypeLabels) {
        Map<String, List<Variable>> variables = new HashMap<>();
        for (int i = 0; i < datatypeCodes.length; i++) {
            String datatypeCode = datatypeCodes[i];
            String datatypeLabel = datatypeLabels[i];
            Stream.of(datatypeCode.split(":"))
                    .forEach(dc -> variables.put(
                    datatypeLabel,
                    (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(dc), new ArrayList())
            ));
            ;
        }
        MDC.put("variables", variables.entrySet().stream()
                .filter(e -> !e.getValue().isEmpty())
                .map(
                        s -> String.format("%s:( %s)", RecorderACBB.getACBBMessageWithBundle(getLocalBundle(), s.getKey()), s.getValue().stream().map(v -> v.getAffichage()).collect(Collectors.joining(", "))))
                .collect(Collectors.joining(", ")).replaceAll("::", ":")
        );
        return variables;
    }

    protected void buildOutputData(
            final Map<String, Object> requestMetadatasMap,
            final PrintStream outputStream,
            final String[] datatypesLabels,
            final String[] datatypesCodes) {
        buildOutputHeader(outputStream);
        this.outputPrintDates(outputStream, getDates(requestMetadatasMap));
        this.outputPrintTraitements(outputStream, getTreatments(requestMetadatasMap));
        this.outputPrintSites(getSites(requestMetadatasMap), outputStream);
        outputStream.println(RecorderACBB.getACBBMessageWithBundle(
                BUNDLE_REMINDER_SOURCE_PATH,
                PROPERTY_MSG_SELECTED_VARIABLES));
        getVariables(requestMetadatasMap, datatypesCodes, datatypesLabels)
                .forEach((k, v) -> this.outputPrintVariables(outputStream, k, v));
        this.outputPrintFileComp(outputStream, getFileComp(requestMetadatasMap));
        this.outputPrintCommentaire(outputStream, getCommentaire(requestMetadatasMap));
    }

    protected abstract void buildOutputData(final Map<String, Object> requestMetadatasMap,
            final Map<String, PrintStream> outputPrintStreamMap);

    /**
     * Builds the body.
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder #buildBody(java
     * .lang.String, java.util.Map, java.util.Map)
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings(value = "rawtypes")
    @Override
    protected Map<String, File> buildBody(final String headers, final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(CST_REQUEST_REMINDER);
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames, AbstractOutputBuilder.EXTENSION_TXT);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        for (final Map.Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            outputPrintStreamMap.get(datatypeNameEntry.getKey()).println(headers);
        }
        this.buildOutputData(requestMetadatasMap, outputPrintStreamMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @return the string
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder #buildHeader(java
     * .util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap) throws BusinessException {
        // TODO(ptcherniati) Auto-generated method stub
        return null;
    }

    protected void buildOutputHeader(final PrintStream outputSteam) {
        outputSteam.println(RecorderACBB.getACBBMessageWithBundle(
                BUNDLE_REMINDER_SOURCE_PATH,
                PROPERTY_MSG_HEADER));
        outputSteam.println();
        outputSteam.printf(RecorderACBB.getACBBMessageWithBundle(
                BUNDLE_REMINDER_SOURCE_PATH,
                PROPERTY_MSG_EXTRACTION_DATE));
        outputSteam.println(DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY_HH_MM_SS));
        outputSteam.println();
        if (!policyManager.getCurrentUser().getIsRoot()) {
            outputSteam.printf(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_REMINDER_SOURCE_PATH,
                    PROPERTY_MSG_WARNING_INCOMPLETE_DATA));
            outputSteam.println();
            outputSteam.println();
        }
    }

    /**
     * @param parameters
     * @return
     * @throws BusinessException
     * @throws NoExtractionResultException
     */
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        parameters.getParameters().put(PROPERTY_MSG_COMMENT, parameters.getCommentaire());
        final Map<String, List> results = parameters.getResults().get(CST_REQUEST_REMINDER);
        parameters.getParameters().put(IParameter.class.getSimpleName(), parameters);
        ((DefaultParameter) parameters).getFilesMaps().add(this.buildBody(StringUtils.EMPTY, results, parameters.getParameters()));
        return null;
    }

    /**
     * Builds the outputs files.
     *
     * @param filesNames
     * @param suffix
     * @return the map
     * @link(Set<String>) the files names
     * @link(String) the suffix
     */
    @Override
    protected Map<String, File> buildOutputsFiles(final Set<String> filesNames, final String suffix) {
        final Map<String, File> filesMaps = new HashMap();
        final String extractionPath = this.configuration.getRepositoryURI().concat(AbstractOutputBuilder.FILE_SEPARATOR).concat(AbstractOutputBuilder.EXTRACTION).concat(AbstractOutputBuilder.FILE_SEPARATOR);
        for (final String filename : filesNames) {
            final String fileRandomSuffix = Long.toString(Instant.now().toEpochMilli()).concat("-").concat(Double.toString(Math.random() * 1_000_000).substring(0, 6));
            final File resultFile = new File(extractionPath.concat(filename).concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(suffix).concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(fileRandomSuffix).concat(AbstractOutputBuilder.EXTENSION_TXT));
            filesMaps.put(filename, resultFile);
        }
        return filesMaps;
    }

    /**
     * Output print commentaire.
     *
     * @param outputSteam
     * @param commentaire
     * @link(PrintStream) the output steam
     * @link(String) the commentaire
     * @link(PrintStream) the output steam
     * @link(String) the commentaire
     */
    protected void outputPrintCommentaire(final PrintStream outputSteam, final String commentaire) {
        outputSteam.println(RecorderACBB.getACBBMessageWithBundle(BUNDLE_REMINDER_SOURCE_PATH, PROPERTY_MSG_COMMENT));
        outputSteam.printf("\t%s", Optional.ofNullable(commentaire).orElse(RecorderACBB.getACBBMessageWithBundle(BUNDLE_REMINDER_SOURCE_PATH, PROPERTY_MSG_NO_COMMENT)));
    }

    /**
     * Output print dates.
     *
     * @param outputSteam
     * @param datesYearsContinuousFormParamVO
     * @link(PrintStream) the output steam
     * @link(DatesYearsContinuousFormParamVO) the dates years continuous form param vo
     * @link(PrintStream) the output steam
     * @link(DatesYearsContinuousFormParamVO) the dates years continuous form param vo
     */
    protected void outputPrintDates(final PrintStream outputSteam, final DatesFormParamVO datesYearsContinuousFormParamVO) {
        outputSteam.println(RecorderACBB.getACBBMessageWithBundle(BUNDLE_REMINDER_SOURCE_PATH, PROPERTY_MSG_SELECTED_DATES));
        try {
            MDC.put("startDate", datesYearsContinuousFormParamVO.getPeriods().get(0).get("start"));
            MDC.put("endDate", datesYearsContinuousFormParamVO.getPeriods().get(0).get("end"));
            datesYearsContinuousFormParamVO.buildSummary(outputSteam);
        } catch (final DateTimeException e) {
            outputSteam.println("MSG_DISPLAY_ERROR");
        }
        outputSteam.println();
        outputSteam.println();
    }

    /**
     * Output print traitements.
     *
     * @param outputSteam
     * @param selectedsTraitements
     * @link(PrintStream) the output steam
     * @link(List<TraitementProgramme>) the selecteds traitements
     * @link(PrintStream) the output steam
     * @link(List<TraitementProgramme>) the selecteds traitements
     */
    protected void outputPrintTraitements(final PrintStream outputSteam, final List<TraitementProgramme> selectedsTraitements) {
        if (CollectionUtils.isEmpty(selectedsTraitements)) {
            return;
        }
        outputSteam.println(RecorderACBB.getACBBMessageWithBundle(BUNDLE_REMINDER_SOURCE_PATH, PROPERTY_MSG_SELECTED_TREATMENT));
        Properties agroecosystemeProperties = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties siteProperties = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        final Properties propertiesAffichage = this.localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE);
        final Properties propertiesNom = this.localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_NAME);
        String localizedAgroecosysteme;
        String localizedSite;
        String localizedAffichage;
        String localizedNom;
        for (final TraitementProgramme traitement : selectedsTraitements) {
            localizedAgroecosysteme = agroecosystemeProperties.getProperty(traitement.getSite().getAgroEcosysteme().getName(), traitement.getSite().getAgroEcosysteme().getName());
            localizedSite = siteProperties.getProperty(traitement.getSite().getName(), traitement.getSite().getName());
            localizedAffichage = propertiesAffichage.getProperty(traitement.getAffichage(), traitement.getAffichage());
            localizedNom = propertiesNom.getProperty(traitement.getNom(), traitement.getNom());
            outputSteam.printf("\t%s - %s : %s (%s)", localizedAgroecosysteme, localizedSite, localizedAffichage, localizedNom);
            outputSteam.println();
        }
        outputSteam.println();
        outputSteam.println();
    }

    protected void outputPrintSites(final List<SiteACBB> list, final PrintStream reminderPrintStream) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                BUNDLE_REMINDER_SOURCE_PATH,
                PROPERTY_MSG_SELECTED_SITES));
        Properties agroecosystemeProperties = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties siteProperties = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        String localizedAgroecosysteme, localizedSite;
        for (final SiteACBB site : list) {
            localizedAgroecosysteme = agroecosystemeProperties.getProperty(site.getAgroEcosysteme().getName(), site.getAgroEcosysteme().getName());
            localizedSite = siteProperties.getProperty(site.getName(), site.getName());
            localizedAgroecosysteme = agroecosystemeProperties.getProperty(site.getAgroEcosysteme().getName(), site.getAgroEcosysteme().getName());
            localizedSite = siteProperties.getProperty(site.getName(), site.getName());
            reminderPrintStream.println(String.format(PATTERN_STRING_SITES_SUMMARY, localizedAgroecosysteme, localizedSite));
        }
        reminderPrintStream.println();
    }

    protected void outputPrintFileComp(final PrintStream outputSteam, final Set<FileComp> selectedFileComp) {
        if (CollectionUtils.isEmpty(selectedFileComp)) {
            return;
        }
        Properties filetype = this.localizationManager.newProperties(FileType.NAME_ENTITY_JPA, FileType.NAME_ATTRIBUTS_NAME);
        Properties description = this.localizationManager.newProperties(FileComp.NAME_ENTITY_JPA, FileComp.ENTITY_DESCRIPTION_COLUM_NAME);
        localizationManager.getMessage(BUNDLE_REMINDER_SOURCE_PATH, EXTRACTION);
        outputSteam.println(RecorderACBB.getACBBMessageWithBundle(BUNDLE_REMINDER_SOURCE_PATH, PROPERTY_MSG_SELECTED_FILECOMPS));
        selectedFileComp.stream()
                .forEach((filecomp) -> {
                    outputSteam.printf("\t%s > %s (%s)",
                            filetype.getOrDefault(filecomp.getFileType().getDescription(), filecomp.getFileType().getDescription()),
                            filecomp.getFileName(),
                            description.getOrDefault(filecomp.getDescription(), filecomp.getDescription())
                    );
                    outputSteam.println();

                });
        outputSteam.println();
    }

    /**
     * Output print variables.
     *
     * @param outputSteam
     * @param BiomasseParameter .getVariablesSemis()
     * @link(PrintStream) the output steam
     * @link(List<VariableACBB>) the variables
     * @link(PrintStream) the output steam
     * @link(List<VariableACBB>) the variables
     */
    protected void outputPrintVariables(final PrintStream outputSteam, String title, final List<Variable> selectedVariables) {
        if (CollectionUtils.isEmpty(selectedVariables)) {
            return;
        }
        final Properties propertiesNom = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        outputSteam.println("\t" + RecorderACBB.getACBBMessageWithBundle(getLocalBundle(), title));
        outputSteam.println();
        selectedVariables.stream().sorted((o1, o2) -> {
            return o1.getAffichage().toLowerCase().compareTo(o2.getAffichage().toLowerCase());
        }).forEach((variable) -> {
            String localizedNom = propertiesNom.getProperty(variable.getName());
            outputSteam.printf("\t\t%s (%s)", variable.getAffichage(), Strings.isNullOrEmpty(localizedNom) ? variable.getName() : localizedNom);
            outputSteam.println();
        });
        // TODO
        /**
         * autre datatype biomasse
         */
        outputSteam.println();
        outputSteam.println();
    }

    @Override
    public void formatMotivation(IParameter parameters, IMotivation motivation, ILocalizationManager localizationManager, Utilisateur user) {

        MDC.put("date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY_HH_MM_SS));
        MDC.put("login", user.getLogin());
        MDC.put("name", user.getNom());
        MDC.put("prenom", user.getPrenom());
        MDC.put("email", user.getEmail());
        MDC.put("motivation", motivation.getMotivation());
        MDC.put("extractionType", parameters.getExtractionTypeCode());
    }

    protected abstract String getLocalBundle();

}
