/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.autreintervention;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;

/**
 * The Interface IAIDAO.
 */
public interface IAIDAO extends IInterventionDAO<AI> {
}
