/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variable;

import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.JPAVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.refdata.variable.Variable_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAVariableACBBDAO.
 */
public class JPAVariableACBBDAO extends JPAVariableDAO implements IVariableACBBDAO {

    /**
     * Gets the all variable acbb.
     *
     * @param class1
     * @return the all variable acbb @see
     * org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO#getAllVariableACBB
     * (java.lang.Class)
     * @link(Class<Variable>) the class1
     */
    @Override
    public List<VariableACBB> getAllVariableACBB(final Class<Variable> class1) {
        CriteriaQuery<VariableACBB> query = builder.createQuery(VariableACBB.class);
        Root<VariableACBB> var = query.from(VariableACBB.class);
        query.select(var);
        return getResultList(query);
    }

    /**
     * Gets the by affichage.
     *
     * @param affichage
     * @return the by affichage
     * @link(String) the affichage
     */
    @Override
    public Optional<Variable> getByAffichage(final String affichage) {
        CriteriaQuery<Variable> query = builder.createQuery(Variable.class);
        Root<VariableACBB> var = query.from(VariableACBB.class);
        query.where(
                builder.equal(var.get(Variable_.affichage), affichage)
        );
        query.select(var);
        return getOptional(query);
    }

    /**
     * @param affichage
     * @param datatypeCode
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Optional<Variable> getByAffichageAndDatatypeCode(final String affichage, String datatypeCode) {
        CriteriaQuery<Variable> query = builder.createQuery(Variable.class);
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        Join<DatatypeVariableUniteACBB, Variable> var = dvu.join(DatatypeVariableUniteACBB_.variable);
        Join<DatatypeVariableUniteACBB, DataType> dty = dvu.join(DatatypeVariableUniteACBB_.datatype);
        query.where(
                builder.equal(var.get(Variable_.affichage), affichage),
                builder.equal(dty.get(Nodeable_.code), datatypeCode)
        );
        query.select(var);
        return getOptional(query);

    }
}
