/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne;

/**
 * The Interface IValeurFneDAO.
 */
public interface IValeurFneDAO extends IDAO<ValeurFne> {
}
