/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.IRequestPropertiesBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.RequestPropertiesBiomasse;

import java.io.Serializable;

/**
 * The Class RequestPropertiesAI.
 * <p>
 * record the general information of the file
 */
public class RequestPropertiesBiomassProduction extends RequestPropertiesBiomasse implements
        IRequestPropertiesBiomassProduction, Serializable {
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new request properties ai.
     */
    public RequestPropertiesBiomassProduction() {
        super();
    }

    /**
     * @return @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getTestDuplicates()
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateBiomassProduction();
    }
}
