/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.MesureTravailDuSol;

/**
 * The Interface IMesureTravailDuSolDAO.
 */
public interface IMesureTravailDuSolDAO extends IDAO<MesureTravailDuSol> {
}
