/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.bloc;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPABlocDAO.
 */
public class JPABlocDAO extends AbstractJPADAO<Bloc> implements IBlocDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.bloc.IBlocDAO#getAll()
     */
    @Override
    public List<Bloc> getAll() {
        return getAll(Bloc.class);
    }

    /**
     * Gets the by n key.
     *
     * @param siteId
     * @param code
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.refdata.bloc.IBlocDAO#getByNKey(java.lang.Long,
     * java.lang.String)
     * @link(Long) the site id
     * @link(String) the code
     */
    @Override
    public Optional<Bloc> getByNKey(final Long siteId, final String code) {

        CriteriaQuery<Bloc> query = builder.createQuery(Bloc.class);
        Root<Bloc> bloc = query.from(Bloc.class);
        query.where(
                builder.equal(bloc.get(Bloc_.code), code),
                builder.equal(bloc.join(Bloc_.site).get(Nodeable_.id), siteId)
        );
        query.select(bloc);
        return getOptional(query);

    }

    /**
     * Gets the by site.
     *
     * @param site
     * @return the by site @see
     * org.inra.ecoinfo.acbb.refdata.bloc.IBlocDAO#getBySite(org.inra.ecoinfo
     * .acbb.refdata.site.SiteACBB)
     * @link(SiteACBB) the site
     */
    @Override
    public List<Bloc> getBySite(final SiteACBB site) {

        CriteriaQuery<Bloc> query = builder.createQuery(Bloc.class);
        Root<Bloc> bloc = query.from(Bloc.class);
        query.where(
                builder.equal(bloc.join(Bloc_.site), site)
        );
        query.select(bloc);
        return getResultList(query);
    }

    /**
     * Gets the names blocs for site.
     *
     * @param site
     * @return the names blocs for site @see
     * org.inra.ecoinfo.acbb.refdata.bloc.IBlocDAO#getNamesBlocsForSite(org.
     * inra.ecoinfo.acbb.refdata.site.SiteACBB)
     * @link(SiteACBB) the site
     */
    @Override
    public List<String> getNamesBlocsForSite(final SiteACBB site) {

        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<Bloc> bloc = query.from(Bloc.class);
        query.where(
                builder.equal(bloc.join(Bloc_.site), site)
        );
        query.select(bloc.get(Bloc_.nom));
        return getResultList(query);
    }
}
