package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultPeriodeRealiseeFactory;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.Optional;

/**
 * A factory for creating SemisPeriodeRealisee objects.
 */
public class PaturagePeriodeRealiseeFactory extends
        DefaultPeriodeRealiseeFactory<PaturageLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.paturage.impl.messages";

    /**
     * The Constant PROPERTY_MSG_ERROR_EXISTING_PERIODE @link(String).
     */
    static final String PROPERTY_MSG_ERROR_EXISTING_PERIODE = "PROPERTY_MSG_ERROR_EXISTING_PERIODE";

    Optional<PeriodeRealisee> createNewPeriodeRealisee(final PaturageLineRecord line) {
        PeriodeRealisee periodeRealisee = new PeriodeRealisee(line.getPeriodeNumber(), line
                .getTempo().getAnneeRealisee());
        return Optional.of(periodeRealisee);
    }

    /**
     * @param line
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultPeriodeRealiseeFactory#getOrCreatePeriodeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport)
     */
    @Override
    public Optional<PeriodeRealisee> getOrCreatePeriodeRealisee(final PaturageLineRecord line,
                                                                final ErrorsReport errorsReport) {
        if (line.getPeriodeNumber() < 1) {
            return Optional.empty();
        }
        Optional<PeriodeRealisee> periodeRealisee = this.periodeRealiseeDAO.getByNKey(line.getTempo()
                .getAnneeRealisee(), line.getPeriodeNumber());
        if (!periodeRealisee.isPresent()) {
            periodeRealisee = this.createNewPeriodeRealisee(line);
        }
        this.updatePeriodeRealisee(periodeRealisee.get(), line);
        try {
            this.periodeRealiseeDAO.saveOrUpdate(periodeRealisee.get());
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return periodeRealisee;
    }

    void updatePeriodeRealisee(final PeriodeRealisee periodeRealisee, final PaturageLineRecord line) {
        periodeRealisee.setDateDeDebut(line.getDate());
        periodeRealisee.setDateDeFin(line.getEndDate());
    }

}
