/*
 *
 */
package org.inra.ecoinfo.acbb.utils.filenamecheckers;

import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class AbstractACBBFileNameChecker.
 */
public abstract class AbstractACBBFileNameChecker extends AbstractFileNameChecker {

    /**
     * The Constant PATTERN @link(String).
     */
    protected static final String ACBB_PATTERN = "^(%s|.*?)_(%s|.*?)_([^_]*)_([^_]*)\\.csv$";

    /**
     * The LOGGER @link(Logger).
     */
    static final Logger LOGGER = LoggerFactory.getLogger(AbstractACBBFileNameChecker.class);
    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;

    /**
     * Instantiates a new abstract acbb file name checker.
     */
    AbstractACBBFileNameChecker() {
        super();
    }

    /**
     * Checks if is valid file name.
     *
     * @param fileName
     * @param version
     * @return true, if is valid file name
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(String) the file name
     * @link(VersionFile) the version
     */
    @Override
    public boolean isValidFileName(final String fileName, final VersionFile version)
            throws InvalidFileNameException {
        String finalFileName = fileName;
        finalFileName = this.cleanFileName(finalFileName);
        Dataset dataset = version.getDataset();
        final String currentSite = ACBBUtils.getSiteFromDataset(dataset).getCode();
        final String currentDatatype = ACBBUtils.getDatatypeFromDataset(dataset).getCode();
        final Matcher splitFilename = Pattern.compile(
                String.format(ACBB_PATTERN, currentSite, currentDatatype))
                .matcher(finalFileName);
        this.testPath(currentSite, currentDatatype, splitFilename);
        final String siteName = Utils.createCodeFromString(splitFilename.group(1));
        this.testSite(version, currentSite, currentDatatype, siteName);
        final String datatypeName = Utils.createCodeFromString(splitFilename.group(2));
        this.testDatatype(currentSite, currentDatatype, datatypeName);
        this.testDates(version, currentSite, currentDatatype, splitFilename);
        return true;
    }

    /**
     * Re write current site path.
     *
     * @param currentSite
     * @return the string @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker
     * #reWriteCurrentSitePath(java.lang.String)
     * @link(String) the current site
     */
    @Override
    protected String reWriteCurrentSitePath(final String currentSite) {
        return currentSite;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    /**
     * Test datatype.
     *
     * @param currentSite
     * @param currentDatatype
     * @param datatypeName
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(String)
     */
    @Override
    protected void testDatatype(final String currentSite, final String currentDatatype,
                                final String datatypeName) throws InvalidFileNameException {
        if ((!currentDatatype.equals(Utils.createCodeFromString(datatypeName)) || this.datatypeDAO.getByCode(Utils.createCodeFromString(datatypeName)) == null)
                && !org.apache.commons.lang.StringUtils.EMPTY.equals(datatypeName)
                && !datatypeName.equalsIgnoreCase(this.datatypeVariableUniteACBBDAO.getNameVariableForDatatypeName(
                currentDatatype)
                .orElseThrow(() -> new InvalidFileNameException(String.format(
                        AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype,
                        this.getDatePattern(), this.getDatePattern()))))) {
            throw new InvalidFileNameException(String.format(
                    AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype,
                    this.getDatePattern(), this.getDatePattern()));
        }
    }

    /**
     * Test site.
     *
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param siteName
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(VersionFile) the version
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(String)
     */
    @Override
    protected void testSite(final VersionFile version, final String currentSite,
                            final String currentDatatype, final String siteName) throws InvalidFileNameException {
        SiteACBB siteACBB = (SiteACBB) this.siteDAO.getByPath(Utils.createCodeFromString(siteName))
                .orElseGet(() -> ((ISiteACBBDAO) this.siteDAO).getByShortName(Utils.createCodeFromString(siteName))
                        .orElse(null));
        final String sitePath = ACBBUtils.getSiteFromDataset(version.getDataset()).getPath();
        if (siteACBB == null || !sitePath.equals(siteACBB.getPath())) {
            throw new InvalidFileNameException(String.format(
                    AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype,
                    this.getDatePattern(), this.getDatePattern()));
        }
    }
}
