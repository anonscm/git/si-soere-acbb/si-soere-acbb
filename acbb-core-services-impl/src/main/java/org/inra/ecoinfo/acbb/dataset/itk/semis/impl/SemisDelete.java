package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.IDeleteRecord;
import org.inra.ecoinfo.acbb.dataset.impl.DeleteRecord;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IMesureSemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisAttrDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemisAttr;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * The Class SemisDelete.
 */
public class SemisDelete extends DeleteRecord implements IDeleteRecord {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.impl.messages";

    /**
     * The Constant PROPERTY_MSG_BINDED_DELETE @link(String).
     */
    private static final String PROPERTY_MSG_BINDED_DELETE = "PROPERTY_MSG_BINDED_DELETE";

    /**
     * The tempo dao @link(ITempoDAO).
     */
    private ITempoDAO tempoDAO;

    /**
     * The semis dao @link(ISemisDAO).
     */
    private ISemisDAO semisDAO;

    /**
     * The mesure semis dao @link(IMesureSemisDAO).
     */
    private IMesureSemisDAO mesureSemisDAO;

    /**
     * The valeur semis attr dao @link(IValeurSemisAttrDAO).
     */
    private IValeurSemisAttrDAO valeurSemisAttrDAO;

    /**
     * <p>
     * try to delete semis
     * </p>
     * <p>
     * first find if there's outBindedsInterventions
     * </p>
     * <p>
     * if there are delete those which are from and then throw an info message
     * </p>
     * <p>
     * else delete semis
     * </p>
     *
     * @param versionFile
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.DeleteRecord#deleteRecord(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecord(final VersionFile versionFile) throws BusinessException {

        VersionFile finalVersionFile = versionFile;
        final List<Tempo> temposToDelete = new LinkedList();
        List<Semis> semisToDelete;
        try {
            finalVersionFile = this.getVersionFileDAO().merge(finalVersionFile);
            semisToDelete = this.semisDAO.getInterventionForVersion(versionFile);
            final Set<AbstractIntervention> outBindedsInterventions = this
                    .findAndRemoveTemposMesuresAndValeursToDelete(temposToDelete, semisToDelete);
            if (!outBindedsInterventions.isEmpty()) {
                final String message = this.sendInfosMessage(finalVersionFile,
                        outBindedsInterventions);
                throw new BusinessException(message);
            }
            this.deleteSemis(semisToDelete, finalVersionFile);
        } catch (final PersistenceException e) {
            this.getLogger().debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Delete semis.
     * <p>
     * before deleting semis delete all attached ValeurSemisAttr
     * </P>
     * <p>
     * after deletion delete all unused tempo
     * </p>
     *
     * @param semisToDelete
     * @param version
     * @throws PersistenceException the persistence exception
     * @link(List<Semis>) the semis to delete
     * @link(VersionFile) the version
     */
    private void deleteSemis(final List<Semis> semisToDelete, VersionFile version)
            throws PersistenceException {
        for (final Semis semis : semisToDelete) {
            final List<ValeurSemisAttr> attributs = new LinkedList<>(semis.getAttributs());
            for (ValeurSemisAttr attribut : attributs) {
                semis.getAttributs().remove(attribut);
                attribut.setSemis(null);
                this.valeurSemisAttrDAO.remove(attribut);
            }
        }
        this.getPublicationDAO().removeVersion(version);
        this.tempoDAO.deleteUnusedTempos();
    }

    /**
     * Find and remove tempos mesures and valeurs to delete.
     *
     * @param temposToDelete
     * @param semisToDelete
     * @return the sets the
     * @throws PersistenceException the persistence exception
     * @link(List<Tempo>) the tempos to delete
     * @link(List<Semis>) the semis to delete
     */
    private Set<AbstractIntervention> findAndRemoveTemposMesuresAndValeursToDelete(
            final List<Tempo> temposToDelete, final List<Semis> semisToDelete)
            throws PersistenceException {
        final Set<AbstractIntervention> outBindedsInterventions = new TreeSet();
        for (final Semis semis : semisToDelete) {
            final Tempo tempoToDelete = semis.getTempo();
            final Set<AbstractIntervention> bindedITK = tempoToDelete.getBindedsITK();
            if (semisToDelete.containsAll(bindedITK)) {
                temposToDelete.add(tempoToDelete);
                this.removeMesurestoDelete(semis.getMesuresSemis());
                semis.getMesuresSemis().clear();
            } else {
                return bindedITK;
                /*
                 * for (final AbstractIntervention intervention : bindedITK) {
                 * outBindedsInterventions.add(intervention); if (bindedITK instanceof Semis &&
                 * !semisToDelete.contains(intervention)) { if (((AbstractIntervention)
                 * bindedITK).getPeriodeRealiseeNumber() == 0) {
                 * removeMesurestoDelete(semis.getMesuresSemis()); } else {
                 * outBindedsInterventions.add(intervention); } } else if (!(intervention instanceof
                 * Semis)) { outBindedsInterventions.add(intervention); } }
                 */
            }
            semis.setTempo(null);
            this.semisDAO.saveOrUpdate(semis);
        }
        for (final Tempo tempo : temposToDelete) {
            this.tempoDAO.remove(tempo);
        }
        return outBindedsInterventions;
    }

    /**
     * Removes the mesuresto delete.
     *
     * @param mesuresToDelete
     * @throws PersistenceException the persistence exception
     * @link(List<MesureSemis>) the mesures to delete
     */
    private void removeMesurestoDelete(final List<MesureSemis> mesuresToDelete)
            throws PersistenceException {
        for (final MesureSemis mesureSemis : mesuresToDelete) {
            mesureSemis.getvaleursSemis().clear();
            this.mesureSemisDAO.saveOrUpdate(mesureSemis);
        }
    }

    /**
     * Send infos message.
     *
     * @param versionFile
     * @param outBindedsInterventions
     * @return the string
     * @link(VersionFile) the version file
     * @link(Set<Intervention>) the out bindeds interventions
     */
    private String sendInfosMessage(final VersionFile versionFile,
                                    final Set<AbstractIntervention> outBindedsInterventions) {
        final String messages = ACBBUtils.getBindedsMessages(outBindedsInterventions);
        return String.format(RecorderACBB.getACBBMessageWithBundle(SemisDelete.BUNDLE_NAME,
                SemisDelete.PROPERTY_MSG_BINDED_DELETE), versionFile.getVersionNumber(),
                versionFile.getDataset().buildDownloadFilename(this.datasetConfiguration), messages);
    }

    /**
     * Sets the annee realisee dao.
     *
     * @param anneeRealiseeDAO the new annee realisee dao
     */
    public void setAnneeRealiseeDAO(IAnneeRealiseeDAO anneeRealiseeDAO) {
    }

    /**
     * Sets the mesure semis dao.
     *
     * @param mesureSemisDAO the new mesure semis dao
     */
    public void setMesureSemisDAO(final IMesureSemisDAO mesureSemisDAO) {
        this.mesureSemisDAO = mesureSemisDAO;
    }

    /**
     * Sets the periode realisee dao.
     *
     * @param periodeRealiseeDAO the new periode realisee dao
     */
    public void setPeriodeRealiseeDAO(IPeriodeRealiseeDAO periodeRealiseeDAO) {
    }

    /**
     * Sets the semis dao.
     *
     * @param semisDAO the new semis dao
     */
    public void setSemisDAO(final ISemisDAO semisDAO) {
        this.semisDAO = semisDAO;
    }

    /**
     * Sets the tempo dao.
     *
     * @param tempoDAO the new tempo dao
     */
    public void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * Sets the valeur semis attr dao.
     *
     * @param valeurSemisAttrDAO the new valeur semis attr dao
     */
    public void setValeurSemisAttrDAO(final IValeurSemisAttrDAO valeurSemisAttrDAO) {
        this.valeurSemisAttrDAO = valeurSemisAttrDAO;
    }

    /**
     * Sets the version de traitement realisee dao.
     *
     * @param versionDeTraitementRealiseeDAO the new version de traitement
     *                                       realisee dao
     */
    public void setVersionDeTraitementRealiseeDAO(
            IVersionDeTraitementRealiseeDAO versionDeTraitementRealiseeDAO) {
    }

}
