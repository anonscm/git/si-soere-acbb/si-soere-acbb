/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class TestDuplicateWsol.
 *
 * implementation for Tillage of {@link ITestDuplicates}
 *
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux
 * files
 */
public class TestDuplicateFert extends AbstractTestDuplicate {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String ACBB_DATASET_WSOL_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.travaildusol.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    final SortedMap<String, Long> line;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateFert() {
        this.line = new TreeMap();
    }

    /**
     *
     * @param parcelle
     * @param date
     * @param rang
     * @param fertType
     * @param lineNumber
     */
    protected void addLine(final String parcelle, final String date, final String rang,
                           final String fertType, final long lineNumber) {
        final String key = this.getKey(parcelle, date, rang, fertType);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateFert.ACBB_DATASET_WSOL_BUNDLE_NAME,
                    ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle, date, rang,
                    fertType, this.line.get(key)));
        }

    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[6], values[7], lineNumber + 1);
    }
}
