/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.IListeACBBDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * The Class Recorder.
 * <p>
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractCSVMetadataRecorder<MasseVegClass> {

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_SITE_LIST = "PROPERTY_MSG_INVALID_SITE_LIST";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.messages";
    private static final String PROPERTY_MSG_MISSING_UNITE = "PROPERTY_MSG_MISSING_UNITE";
    private static final String PROPERTY_MSG_MISSING_PARTIE_PLANTE = "PROPERTY_MSG_MISSING_PARTIE_PLANTE";
    private static final String PROPERTY_MSG_MISSING_CATEGORIE_COUVERT = "PROPERTY_MSG_MISSING_CATEGORIE_COUVERT";
    private static final String PROPERTY_MSG_MISSING_COUVERT_VEGETAL = "PROPERTY_MSG_MISSING_COUVERT_VEGETAL";
    private static final String PROPERTY_MSG_MISSING_AERIEN_SOUTERRAIN = "PROPERTY_MSG_MISSING_AERIEN_SOUTERRAIN";
    private static final String PROPERTY_MSG_MISSING_TYPE_MASSE_VEGETALE = "PROPERTY_MSG_MISSING_TYPE_MASSE_VEGETALE";
    private static final String PROPERTY_MSG_MISSING_DEVENIR = "PROPERTY_MSG_MISSING_DEVENIR";
    private static final String PROPERTY_MSG_EMPTY_LABEL = "PROPERTY_MSG_EMPTY_LABEL";
    private static final String CST_VARIABLE_BIOMASS = "masse_vegetale";
    private static final String CST_DATATYPE_BIOMASS = "biomasse_production_teneur";
    private IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    private IListeVegetationDAO listeVegetationDAO;
    private IMasseVegClassDAO masseVegClassDAO;

    private Properties propertiesDefinitionEN;

    private String[] listeCategorieCouverts;

    private String[] listeTypesUtilisationPossibles;

    private String[] listeDevenirPossibles;

    private String[] listeTypesCouvertsPossibles;

    private String[] listeAerienSouterrainPossibles;

    private String[] listePartiesPlantePossibles;

    private String[] listeUnitesPossibles;

    /**
     * Delete record.
     * <p>
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder
     *                           #deleteRecord(com. Ostermiller.util.CSVParser, java.io.File,
     *                           java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     * <p>
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String libelle = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(libelle);
                MasseVegClass masseVegetal = this.masseVegClassDAO.getByNKey(code)
                        .orElseThrow(PersistenceException::new); // ligne
                // précedente
                this.masseVegClassDAO.remove(masseVegetal);// suppression
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private MassVegClassAerienSouterrain getAerienSouterrainDb(ErrorsReport errorsReport,
                                                               int lineNumber, TokenizerValues tokenizerValues) throws BusinessException {
        String aerienSouterrain = tokenizerValues.nextToken();
        MassVegClassAerienSouterrain aerienSouterrainDb = (MassVegClassAerienSouterrain) this.listeVegetationDAO.getByNKey(MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN, Utils.createCodeFromString(aerienSouterrain)).orElse(null);
        if (aerienSouterrainDb == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_AERIEN_SOUTERRAIN),
                    lineNumber, tokenizerValues.currentTokenIndex(), aerienSouterrain));
        }
        return aerienSouterrainDb;
    }

    /**
     * Gets the all elements.
     * <p>
     *
     * @return the all elements
     * <p>
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<MasseVegClass> getAllElements() {
        List<MasseVegClass> all = this.masseVegClassDAO.getAll();
        Collections.sort(all);
        return all;
    }

    private String[] getArrayStringFromListeVegetation(List<? extends ListeVegetation> liste) {
        String[] returnArray = new String[liste.size()];
        for (int i = 0; i < liste.size(); i++) {
            returnArray[i] = liste.get(i).getValeur();
        }
        return returnArray;
    }

    private MassVegClassCategorieCouvert getCategorieCouvertDb(ErrorsReport errorsReport,
                                                               int lineNumber, TokenizerValues tokenizerValues) throws BusinessException {
        String categorieCouvert = tokenizerValues.nextToken();
        MassVegClassCategorieCouvert categorieCouvertDb = (MassVegClassCategorieCouvert) this.listeVegetationDAO.getByNKey(MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT, Utils.createCodeFromString(categorieCouvert)).orElse(null);
        if (categorieCouvertDb == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_CATEGORIE_COUVERT),
                    lineNumber, tokenizerValues.currentTokenIndex(), categorieCouvert));
        }
        return categorieCouvertDb;
    }

    private MassVegClassCouvertVegetalType getCouvertVegetalDb(ErrorsReport errorsReport,
                                                               int lineNumber, TokenizerValues tokenizerValues) throws BusinessException {
        String couvertVegetal = tokenizerValues.nextToken();
        MassVegClassCouvertVegetalType couvertVegetalDb = (MassVegClassCouvertVegetalType) this.listeVegetationDAO.getByNKey(
                MassVegClassCouvertVegetalType.MAV_CLASS_COUVERT_VEGETAL, Utils.createCodeFromString(couvertVegetal)).orElse(null);
        if (couvertVegetalDb == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_COUVERT_VEGETAL),
                    lineNumber, tokenizerValues.currentTokenIndex(), couvertVegetal));
        }
        return couvertVegetalDb;
    }

    private DatatypeVariableUniteACBB getdatatypeVariableUniteDb(ErrorsReport errorsReport,
                                                                 int lineNumber, TokenizerValues tokenizerValues) throws BusinessException {
        DatatypeVariableUniteACBB dvuDB = null;
        String unitName = tokenizerValues.nextToken();
        try {
            dvuDB = this.datatypeVariableUniteACBBDAO.getByNKey(
                    Recorder.CST_DATATYPE_BIOMASS, Recorder.CST_VARIABLE_BIOMASS, unitName)
                    .orElseThrow(PersistenceException::new);
        } catch (PersistenceException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_UNITE), lineNumber,
                    tokenizerValues.currentTokenIndex(), unitName));
        }
        return dvuDB;
    }

    private MassVegClassDevenir getDevenirDb(ErrorsReport errorsReport, int lineNumber,
                                             TokenizerValues tokenizerValues) throws BusinessException {
        String devenir = tokenizerValues.nextToken();
        MassVegClassDevenir devenirDb = (MassVegClassDevenir) this.listeVegetationDAO.getByNKey(MassVegClassDevenir.MAV_CLASS_DEVENIR, Utils.createCodeFromString(devenir)).orElse(null);
        if (devenirDb == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_DEVENIR),
                    lineNumber, tokenizerValues.currentTokenIndex(), devenir));
        }
        return devenirDb;
    }

    private String getLibelle(ErrorsReport errorsReport, int lineNumber,
                              TokenizerValues tokenizerValues) throws BusinessException {
        String libelle = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(libelle)) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_EMPTY_LABEL), lineNumber,
                    tokenizerValues.currentTokenIndex()));
        }
        return libelle;
    }

    private String[] getListeAerienSouterrainPossibles() {
        List<MassVegClassDevenir> aerienSouterrain = this.listeVegetationDAO
                .getByName(MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN);
        return this.getArrayStringFromListeVegetation(aerienSouterrain);
    }

    private String[] getListeCategorieCouvertsPossibles() {
        List<MassVegClassCategorieCouvert> categorieCouvertsPossible = this.listeVegetationDAO
                .getByName(MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT);
        return this.getArrayStringFromListeVegetation(categorieCouvertsPossible);
    }

    private String[] getListeCouvertsVegetalPossibles() {
        List<MassVegClassCouvertVegetalType> typescouverts = this.listeVegetationDAO
                .getByName(MassVegClassCouvertVegetalType.MAV_CLASS_COUVERT_VEGETAL);
        return this.getArrayStringFromListeVegetation(typescouverts);
    }

    private String[] getListeDevenirPossibles() {
        List<MassVegClassDevenir> devenirs = this.listeVegetationDAO
                .getByName(MassVegClassDevenir.MAV_CLASS_DEVENIR);
        return this.getArrayStringFromListeVegetation(devenirs);
    }

    private String[] getListePartiesPlantePossibles() {
        List<MassVegClassPartieVegetaleType> partiesPlantePossible = this.listeVegetationDAO
                .getByName(MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE);
        return this.getArrayStringFromListeVegetation(partiesPlantePossible);
    }

    private String[] getListeTypesUtilisationPossibles() {
        List<MassVegClassTypeUtilisation> typesUtilisation = this.listeVegetationDAO
                .getByName(MassVegClassTypeUtilisation.MAV_CLASS_TYPE_UTILISATION_TYPE);
        return this.getArrayStringFromListeVegetation(typesUtilisation);
    }

    private String[] getListeUnitesPossibles() {
        List<DatatypeVariableUniteACBB> datatypeVariableUniteACBBDb;
        datatypeVariableUniteACBBDb = this.datatypeVariableUniteACBBDAO
                .getAllDatatypeVariableUniteACBB();
        List<String> variablesDb = new LinkedList();
        for (DatatypeVariableUniteACBB dvu : datatypeVariableUniteACBBDb) {
            if (Recorder.CST_DATATYPE_BIOMASS.equals(dvu.getDatatype().getCode())
                    && Recorder.CST_VARIABLE_BIOMASS.equals(dvu.getVariable().getCode())) {
                variablesDb.add(dvu.getUnite().getCode());
            }
        }
        return variablesDb.toArray(new String[]{});

    }

    /**
     * Gets the new line model grid metadata.
     * <p>
     *
     * @param masseVegetale
     * @return the new line model grid metadata
     * <p>
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(Bloc) the bloc
     * <p>
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final MasseVegClass masseVegetale)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(masseVegetale != null ? masseVegetale.getLibelle()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getCategorieCouvert().getValeur(),
                        this.listeCategorieCouverts, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getTypeMasseVegetale().getValeur(),
                        this.listeTypesUtilisationPossibles, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getAerien().getValeur(),
                        this.listeAerienSouterrainPossibles, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getPartiesPlante().getValeur(),
                        this.listePartiesPlantePossibles, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getDevenir().getValeur(),
                        this.listeDevenirPossibles, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(masseVegetale != null ? masseVegetale.getDefinition()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(masseVegetale != null ? this.propertiesDefinitionEN
                        .getProperty(masseVegetale.getDefinition())
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getCouvertVegetal().getValeur(),
                        this.listeTypesCouvertsPossibles, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        masseVegetale == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : masseVegetale.getDatatypeVariableUnite().getUnite().getCode(),
                        this.listeUnitesPossibles, null, false, false, true));
        return lineModelGridMetadata;
    }

    @SuppressWarnings({})
    private MassVegClassPartieVegetaleType getPartiePlanteDb(ErrorsReport errorsReport,
                                                             int lineNumber, TokenizerValues tokenizerValues) throws BusinessException {
        String partiePlante = tokenizerValues.nextToken();
        MassVegClassPartieVegetaleType partiePlanteDb = (MassVegClassPartieVegetaleType) this.listeVegetationDAO.getByNKey(MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE, Utils.createCodeFromString(partiePlante)).orElse(null);
        if (partiePlanteDb == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_MISSING_PARTIE_PLANTE),
                    lineNumber, tokenizerValues.currentTokenIndex(), partiePlante));
        }
        return partiePlanteDb;
    }

    private MassVegClassTypeMasseVegetale getTypeDonneeDb(ErrorsReport errorsReport,
                                                          int lineNumber, TokenizerValues tokenizerValues) throws BusinessException {
        String typeMasseVegetale = tokenizerValues.nextToken();
        MassVegClassTypeMasseVegetale typeMasseVegetaleDb = (MassVegClassTypeMasseVegetale) this.listeVegetationDAO.getByNKey(MassVegClassTypeMasseVegetale.MAV_CLASS_TYPE_MASSE_VEGETALE, Utils.createCodeFromString(typeMasseVegetale)).orElse(null);
        if (typeMasseVegetaleDb == null) {
            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessageWithBundle(Recorder.BUNDLE_SOURCE_PATH,
                            Recorder.PROPERTY_MSG_MISSING_TYPE_MASSE_VEGETALE), lineNumber,
                    tokenizerValues.currentTokenIndex(), typeMasseVegetale));
        }
        return typeMasseVegetaleDb;
    }

    /**
     * Inits the model grid metadata.
     * <p>
     *
     * @return the model grid metadata
     * <p>
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<MasseVegClass> initModelGridMetadata() {
        this.propertiesDefinitionEN = this.localizationManager.newProperties(
                MasseVegClass.NAME_ENTITY_JPA, MasseVegClass.ATTRIBUTE_JPA_DEFINITION,
                Locale.ENGLISH);
        this.listeTypesUtilisationPossibles = this.getListeTypesUtilisationPossibles();
        this.listeAerienSouterrainPossibles = this.getListeAerienSouterrainPossibles();
        this.listeCategorieCouverts = this.getListeCategorieCouvertsPossibles();
        this.listeDevenirPossibles = this.getListeDevenirPossibles();
        this.listeUnitesPossibles = this.getListeUnitesPossibles();
        this.listePartiesPlantePossibles = this.getListePartiesPlantePossibles();
        this.listeTypesCouvertsPossibles = getListeCouvertsVegetalPossibles();
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     * <p>
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     *                 <p>
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MasseVegClass.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String code = Utils.createCodeFromString(libelle);
                final MassVegClassCategorieCouvert categorieCouvert = this.getCategorieCouvertDb(errorsReport, lineNumber, tokenizerValues);
                final MassVegClassTypeMasseVegetale typeDonnee = this.getTypeDonneeDb(errorsReport, lineNumber, tokenizerValues);
                final MassVegClassAerienSouterrain aerien = this.getAerienSouterrainDb(errorsReport, lineNumber, tokenizerValues);
                final MassVegClassPartieVegetaleType partiePlante = this.getPartiePlanteDb(errorsReport, lineNumber, tokenizerValues);
                final MassVegClassDevenir devenir = this.getDevenirDb(errorsReport, lineNumber, tokenizerValues);
                final String definition = tokenizerValues.nextToken();
                final MassVegClassCouvertVegetalType couvertVegetal = this.getCouvertVegetalDb(errorsReport, lineNumber, tokenizerValues);
                final DatatypeVariableUniteACBB datatypeVariableUniteACBB = this.getdatatypeVariableUniteDb(errorsReport, lineNumber, tokenizerValues);
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMasseVegetalClassification(errorsReport, code, libelle,
                            categorieCouvert, typeDonnee, aerien, partiePlante, devenir,
                            definition, couvertVegetal, datatypeVariableUniteACBB);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMasseVegetalClassification(ErrorsReport errorsReport, String code,
                                                        String libelle, MassVegClassCategorieCouvert categorieCouvert,
                                                        MassVegClassTypeMasseVegetale typeDonnee, MassVegClassAerienSouterrain aerien,
                                                        MassVegClassPartieVegetaleType partiePlante, MassVegClassDevenir devenir,
                                                        String definition, MassVegClassCouvertVegetalType couvertVegetal,
                                                        DatatypeVariableUniteACBB datatypeVariableUniteACBB) {
        MasseVegClass masseVegClassDb = this.masseVegClassDAO.getByNKey(code).orElse(null);
        if (masseVegClassDb == null) {
            masseVegClassDb = new MasseVegClass(libelle, code, categorieCouvert, typeDonnee,
                    aerien, partiePlante, devenir, couvertVegetal, definition,
                    datatypeVariableUniteACBB);

        } else {
            masseVegClassDb.update(libelle, code, categorieCouvert, typeDonnee, aerien,
                    partiePlante, devenir, couvertVegetal, definition, datatypeVariableUniteACBB);
        }
        try {
            this.masseVegClassDAO.saveOrUpdate(masseVegClassDb);
        } catch (PersistenceException e) {
            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);
        }
    }

    /**
     * @param datatypeVariableUniteACBBDAO
     */
    public void setDatatypeVariableUniteACBBDAO(
            IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    /**
     * @param listeVegetationDAO
     */
    public void setListeVegetationDAO(IListeVegetationDAO listeVegetationDAO) {
        this.listeVegetationDAO = listeVegetationDAO;
    }

    /**
     * @param listeItineraireDAO
     */
    public void setListeItineraireDAO(IListeACBBDAO listeItineraireDAO) {
    }

    /**
     * @param masseVegClassDAO
     */
    public void setMasseVegClassDAO(IMasseVegClassDAO masseVegClassDAO) {
        this.masseVegClassDAO = masseVegClassDAO;
    }

}
