/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.AbstractJPADAO;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAMasseVegClassDAO.
 */

/**
 * @author koyao
 */
public class JPAMasseVegClassDAO extends AbstractJPADAO<MasseVegClass> implements IMasseVegClassDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.bloc.IBlocDAO#getAll()
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    public List<MasseVegClass> getAll() {
        return getAll(MasseVegClass.class);
    }

    /**
     * Gets the by n key.
     *
     * @param code
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.refdata.massevegclass.IMasseVegClassDAO#getByNKey
     * (java.lang.Long, java.lang.String)
     * @link(Long) the site id
     * @link(String) the code
     */
    @Override
    public Optional<MasseVegClass> getByNKey(final String code) {

        CriteriaQuery<MasseVegClass> query = builder.createQuery(MasseVegClass.class);
        Root<MasseVegClass> mv = query.from(MasseVegClass.class);
        query.where(
                builder.equal(mv.get(MasseVegClass_.code), code)
        );
        query.select(mv);
        return getOptional(query);
    }
}
