/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse;

import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractVariableValueBiomasse;

/**
 * @param <U>
 * @author tcherniatinsky
 */
public interface ILineRecordBiomasse<U extends AbstractVariableValueBiomasse> extends ILineRecord<U> {

}
