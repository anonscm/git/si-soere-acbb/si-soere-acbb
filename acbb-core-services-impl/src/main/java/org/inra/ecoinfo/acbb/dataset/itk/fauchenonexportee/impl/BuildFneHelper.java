package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.IFneDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.IValeurFneDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildTravailDusolHelper.
 */
public class BuildFneHelper extends AbstractBuildHelper<FneLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl.messages";

    /**
     * The semis_.
     */
    Map<Parcelle, Map<LocalDate, Fne>> fnes = new HashMap();

    IFneDAO fneDAO;

    IValeurFneDAO valeurfneDAO;

    /**
     * The tempo dao @link(ITempoDAO).
     */
    ITempoDAO tempoDAO;

    /**
     * Adds the mesures semis.
     *
     * @param line
     * @param semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    void addValeursRecFau(final FneLineRecord line, final Fne fne) {
        for (final ACBBVariableValue<? extends IValeurQualitative> variableValue : line
                .getVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurFne valeur;
            if (variableValue.isValeurQualitative()) {
                valeur = new ValeurFne(variableValue.getValeurQualitative(), null, fne);
            } else {
                valeur = new ValeurFne(Float.parseFloat(variableValue.getValue()),
                        realNode, fne);
            }
            fne.getValeursfne().add(valeur);
        }
    }

    /**
     * Builds the datas and record it to database.
     *
     * @throws PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() throws PersistenceException {
        for (final FneLineRecord line : this.lines) {
            final Fne fne = this.getOrCreateFne(line);
            this.fneDAO.saveOrUpdate(fne);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursRecFau(line, fne);
            this.fneDAO.saveOrUpdate(fne);
        }
    }

    /**
     * Instantiates a new builds semis helper.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines @{List
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @throws PersistenceException
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(RequestPropertiesSemis) the request properties semis
     */
    public void build(final VersionFile version, final List<FneLineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK)
            throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    /**
     * Creates the new semis.
     *
     * @param line
     * @return the semis
     * @link(LineRecord) the line
     */
    Fne createNewFne(final FneLineRecord line) {
        Fne fne = null;
        final VersionDeTraitement versionTraitement = line.getVersionDeTraitement();
        final Parcelle parcelle = line.getParcelle();
        final Tempo tempo = this.getTempo(line, versionTraitement, parcelle);
        if (tempo.isInErrors()) {
            return fne;
        }
        fne = new Fne(this.version, line.getObservation(), line.getDate(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber(), tempo);
        fne.setSuiviParcelle(line.getSuiviParcelle());
        fne.setRang(line.getRang());
        try {
            this.fneDAO.saveOrUpdate(fne);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return fne;
    }

    /**
     * Gets the or create semis.
     *
     * @param line
     * @return the or create semis
     * @link(LineRecord) the line
     * @link(LineRecord) the line
     */
    Fne getOrCreateFne(final FneLineRecord line) {
        return fnes
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> createNewFne(line));
    }

    /**
     * @param fneDAO
     */
    public final void setFneDAO(final IFneDAO fneDAO) {
        this.fneDAO = fneDAO;
    }

    /**
     * @param tempoDAO the tempoDAO to set
     */
    public final void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * @param valeurFneDAO
     */
    public final void setValeurFneDAO(final IValeurFneDAO valeurFneDAO) {
        this.valeurfneDAO = valeurFneDAO;
    }

}
