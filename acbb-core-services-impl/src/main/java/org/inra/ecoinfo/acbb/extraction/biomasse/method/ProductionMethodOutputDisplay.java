/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class ProductionMethodOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "ProductionMethode";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return ProductionMethodOutputDisplay.CST_METHOD_NAME;
    }

}
