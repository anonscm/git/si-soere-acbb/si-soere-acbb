/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.IFertilisantDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPAFertilisantDAO extends JPAInterventionDAO<Fertilisant> implements IFertilisantDAO {

    @Override
    protected Class<Fertilisant> getInterventionClass() {
        return Fertilisant.class;
    }
}
