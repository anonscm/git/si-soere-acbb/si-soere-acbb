/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.listeitineraire;

import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.PaturageTypeAnimaux;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class JPAListeItineraireDAO.
 */
@SuppressWarnings("unchecked")
public class JPAValeurQualitativeDAO extends
        org.inra.ecoinfo.refdata.valeurqualitative.JPAValeurQualitativeDAO {

    static final String QUERY_GET_BY_CODE = "select distinct * "
            + "from ("
            + "     select vq_id,code,nom,valeur "
            + "     from valeur_qualitative "
            + "     where code!='" + PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX
            + "' union "
            + "     select vq.vq_id, cnd.code code, var.nom, vq.valeur "
            + "     from valeur_qualitative vq inner "
            + "     join variable var on vq.code=lower(var.affichage) "
            + "     inner join  composite_nodeable cnd  on var.var_id=cnd.id"
            + "     where vq.code!='" + PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX
            + "' union "
            + "     select vq.vq_id, 'sem_variete' code, 'variété semée' nom,vq.valeur "
            + "     from valeur_qualitative vq "
            + "     where nom in ("
            + "         select valeur "
            + "         from valeur_qualitative esp "
            + "         where code='sem_espece'"
            + "     ) "
            + " union "
            + "     select vq.vq_id, 'variete_semee', 'variété semée',vq.valeur "
            + "     from valeur_qualitative vq "
            + "     where nom in ("
            + "         select valeur "
            + "         from valeur_qualitative esp "
            + "         where code='sem_espece'"
            + "     ) "
            + " union "
            + "     select vq.vq_id, 'type_d_animaux', 'Types d''animaux',vq.valeur "
            + "     from valeur_qualitative vq "
            + "     where nom in ("
            + "         select valeur "
            + "         from valeur_qualitative esp"
            + "         where code='pat_type_animaux'"
            + "     ) "
            + " union "
            + "     select pphyt_id, 'produit_phytosanitaire' code, 'Produit phytosanitaire' nom, pphyt.nom nom "
            + "     from produit_phytosanitaire_pphyt pphyt"
            + ") valeur_qualitative "
            + "where code=:code "
            + "order by code";
    private static final Logger LOGGER = LoggerFactory.getLogger(JPAValeurQualitativeDAO.class
            .getName());

    /**
     * @param <T>
     * @param name
     * @return
     * @see org.inra.ecoinfo.refdata.valeurqualitative.JPAValeurQualitativeDAO#getByName(java.lang.String)
     */
    @Override
    public <T extends IValeurQualitative> List<T> getByCode(String name) {
        final Query query = this.entityManager
                .createNativeQuery(JPAValeurQualitativeDAO.QUERY_GET_BY_CODE);
        query.setParameter("code", name);
        List<Object[]> listesItineraire;
        try {
            listesItineraire = query.getResultList();
        } catch (final NoResultException e) {
            listesItineraire = new LinkedList();
        }
        List<T> returnList = new LinkedList();
        for (Object[] t : listesItineraire) {
            returnList.add((T) new ValeurQualitative((String) t[2], (String) t[3]));
        }
        return returnList;
    }
}
