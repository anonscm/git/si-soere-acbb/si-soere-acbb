/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.*;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractGenericTestFormat.
 * <p>
 * an abstract implementation of {@link ITestFormat}
 *
 * @see org.inra.ecoinfo.acbb.dataset.ITestFormat
 */
public class GenericTestFormat implements ITestFormat {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    static final Logger LOGGER = LoggerFactory.getLogger(GenericTestFormat.class);

    /**
     * The test headers @link(ITestHeaders).
     */
    ITestHeaders testHeaders;

    /**
     * The test values @link(ITestValues).
     */
    ITestValues testValues;

    /**
     * The datatype name @link(String).
     */
    String datatypeName;


    /**
     * Instantiates a new abstract generic test format.
     */
    public GenericTestFormat() {
        super();
    }

    /**
     * Gets the datatype name.
     *
     * @return {@link String} the datatype name
     */
    protected String getDatatypeName() {
        return this.datatypeName;
    }

    /**
     * @param datatypeName
     * @see org.inra.ecoinfo.acbb.dataset.ITestFormat#setDatatypeName(java.lang.String)
     */
    @Override
    public final void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     * Sets the test headers.
     *
     * @param testHeaders the new test headers
     * @see org.inra.ecoinfo.acbb.dataset.ITestFormat#setTestHeaders(org.inra.ecoinfo.acbb.dataset.ITestHeaders)
     */
    @Override
    public final void setTestHeaders(final ITestHeaders testHeaders) {
        this.testHeaders = testHeaders;
    }

    /**
     * Sets the test values.
     *
     * @param testValues the new test values
     * @see org.inra.ecoinfo.acbb.dataset.ITestFormat#setTestValues(org.inra.ecoinfo.acbb.dataset.ITestValues)
     */
    @Override
    public final void setTestValues(final ITestValues testValues) {
        this.testValues = testValues;
    }

    /**
     * Test format.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param datasetDescriptor
     * @throws BadFormatException the bad format exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.ITestFormat#testFormat(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * impl.DatasetDescriptorACBB)
     */
    @Override
    public void testFormat(final CSVParser parser, final VersionFile versionFile,
                           final IRequestPropertiesACBB requestProperties, final String encoding,
                           final DatasetDescriptorACBB datasetDescriptor) throws BadFormatException {
        final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(
                RecorderACBB.getACBBMessage(ITestFormat.PROPERTY_MSG_ERROR_BAD_FORMAT));
        LOGGER.debug(RecorderACBB.getACBBMessage(ITestFormat.PROPERTY_MSG_CHECKING_FORMAT_FILE));
        long lineNumber = -1;
        try {
            lineNumber = this.testHeaders.testHeaders(parser, versionFile, requestProperties,
                    encoding, badsFormatsReport, datasetDescriptor);
            if (datasetDescriptor.getEnTete() != lineNumber) {
                final String message = String.format(
                        RecorderACBB.getACBBMessage(ITestFormat.PROPERTY_MSG_BAD_HEADER_SIZE),
                        lineNumber, datasetDescriptor.getEnTete());
                LOGGER.debug(message);
                throw new BusinessException(message);
            }
            this.testValues.testValues(lineNumber, parser, versionFile, requestProperties,
                    encoding, badsFormatsReport, datasetDescriptor, this.getDatatypeName());
        } catch (final BusinessException e) {
            badsFormatsReport.addException(e);
            throw new BadFormatException(badsFormatsReport);
        }
        if (badsFormatsReport.hasErrors()) {
            LOGGER.debug(badsFormatsReport.getMessages());
            throw new BadFormatException(badsFormatsReport);
        }
    }
}
