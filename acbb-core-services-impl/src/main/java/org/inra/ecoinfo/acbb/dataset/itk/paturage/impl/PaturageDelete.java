package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.IDeleteRecord;
import org.inra.ecoinfo.acbb.dataset.impl.DeleteRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IValeurPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author ptcherniati
 */
public class PaturageDelete extends DeleteRecord implements IDeleteRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private IPaturageDAO paturageDAO;
    private ITempoDAO tempoDAO;
    private IValeurPaturageDAO valeurPaturageDAO;
    private IPeriodeRealiseeDAO periodeRealiseeDAO;

    private void deletePaturages(List<Paturage> paturagesToDelete) {
        this.paturageDAO.deleteAll(paturagesToDelete);
    }

    /**
     * Delete record.
     *
     * @param versionFile
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(VersionFile) the version file
     * @see org.inra.ecoinfo.acbb.dataset.IDeleteRecord#deleteRecord(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @see org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion((org
     * .inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecord(final VersionFile versionFile) throws BusinessException {
        List<Paturage> paturagesToDelete;
        try {
            paturagesToDelete = this.paturageDAO.getInterventionForVersion(versionFile);
            this.findAndRemoveTemposAndValeursToDelete(paturagesToDelete);
            this.deletePaturages(paturagesToDelete);
            this.tempoDAO.deleteUnusedTempos();
        } catch (final PersistenceException e) {
            this.getLogger().debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void findAndRemoveTemposAndValeursToDelete(List<Paturage> paturagesToDelete)
            throws PersistenceException {
        for (Paturage paturage : paturagesToDelete) {
            Tempo tempoToDelete = paturage.getTempo();
            Set<AbstractIntervention> bindedItk = tempoToDelete.getBindedsITK();
            for (ValeurPaturage valeur : new LinkedList<>(paturage.getValeursPaturage())) {
                valeur.getPaturage().getValeursPaturage().remove(valeur);
                this.valeurPaturageDAO.remove(valeur);
            }
            paturage.setTempo(null);
            this.paturageDAO.remove(paturage);
            boolean hasSemisBinded = false;
            for (AbstractIntervention intervention : bindedItk) {
                if (Semis.SEMIS_DESCRIMINATOR.equals(intervention.getType())) {
                    hasSemisBinded = true;
                }
            }
            if (!hasSemisBinded) {
                this.tempoDAO.remove(tempoToDelete);
            } else {
                this.periodeRealiseeDAO.remove(tempoToDelete.getPeriodeRealisee());
            }
        }
    }

    /**
     * @param paturageDAO the paturageDAO to set
     */
    public void setPaturageDAO(IPaturageDAO paturageDAO) {
        this.paturageDAO = paturageDAO;
    }

    /**
     * @param periodeRealiseeDAO the periodeRealiseeDAO to set
     */
    public void setPeriodeRealiseeDAO(IPeriodeRealiseeDAO periodeRealiseeDAO) {
        this.periodeRealiseeDAO = periodeRealiseeDAO;
    }

    /**
     * @param tempoDAO the tempoDAO to set
     */
    public void setTempoDAO(ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * @param valeurPaturageDAO the valeurPaturageDAO to set
     */
    public void setValeurPaturageDAO(IValeurPaturageDAO valeurPaturageDAO) {
        this.valeurPaturageDAO = valeurPaturageDAO;
    }
}
