/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPAMethodeLAIDAO.
 */
public class JPAMethodeLAIDAO extends AbstractJPAMethodeDAO<MethodeLAI> implements IMethodeLAIDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeLAI> getMethodClass() {
        return MethodeLAI.class;
    }
}
