package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.IDeleteRecord;
import org.inra.ecoinfo.acbb.dataset.impl.DeleteRecord;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IRecFauDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IValeurRecFauDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author ptcherniati
 */
public class RecFauDelete extends DeleteRecord implements IDeleteRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ITempoDAO tempoDAO;
    private IRecFauDAO recFauDAO;
    private IValeurRecFauDAO valeurRecFauDAO;

    private void deleteRecFaus(List<RecFau> recFausToDelete) {
        this.recFauDAO.deleteAll(recFausToDelete);
    }

    /**
     * Delete record.
     *
     * @param versionFile
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(VersionFile) the version file
     * @see org.inra.ecoinfo.acbb.dataset.IDeleteRecord#deleteRecord(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @see org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion((org
     * .inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecord(final VersionFile versionFile) throws BusinessException {
        VersionFile finalVersionFile = versionFile;
        List<Tempo> temposToDelete = new LinkedList();
        List<RecFau> recFauToDelete;
        try {
            finalVersionFile = this.getVersionFileDAO().merge(finalVersionFile);
            recFauToDelete = this.recFauDAO.getInterventionForVersion(versionFile);
            this.findAndRemoveTemposAndValeursToDelete(temposToDelete, recFauToDelete);
            this.deleteRecFaus(recFauToDelete);
            this.getPublicationDAO().removeVersion(finalVersionFile);
            this.tempoDAO.deleteUnusedTempos();
        } catch (final PersistenceException e) {
            this.getLogger().debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void findAndRemoveTemposAndValeursToDelete(List<Tempo> temposToDelete,
                                                       List<RecFau> recFauToDelete) throws PersistenceException {
        for (RecFau recfau : recFauToDelete) {
            Tempo tempoToDelete = recfau.getTempo();
            Set<AbstractIntervention> bindedITK = tempoToDelete.getBindedsITK();
            if (recFauToDelete.containsAll(bindedITK)) {
                temposToDelete.add(tempoToDelete);
            }
            for (ValeurRecFau valeur : new LinkedList<>(recfau.getValeursRecFau())) {
                valeur.getRecFau().getValeursRecFau().remove(valeur);
                this.valeurRecFauDAO.remove(valeur);
            }
            recfau.setTempo(null);
            this.recFauDAO.remove(recfau);
        }
    }

    /**
     * @param recFauDAO the recFauDAO to set
     */
    public void setRecFauDAO(IRecFauDAO recFauDAO) {
        this.recFauDAO = recFauDAO;
    }

    /**
     * @param tempoDAO the tempoDAO to set
     */
    public void setTempoDAO(ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * @param valeurRecFauDAO the valeurRecFauDAO to set
     */
    public void setValeurRecFauDAO(IValeurRecFauDAO valeurRecFauDAO) {
        this.valeurRecFauDAO = valeurRecFauDAO;
    }
}
