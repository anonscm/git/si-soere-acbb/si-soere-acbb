package org.inra.ecoinfo.acbb.refdata.datatypevariableunite;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The Interface IDatatypeVariableUniteACBBDAO.
 * <p>
 * DAO for {@link DatatypeVariableUniteACBB}
 */
public interface IDatatypeVariableUniteACBBDAO extends IDAO<DatatypeVariableUniteACBB> {

    /**
     * Gets the all {@link DatatypeVariableUniteACBB}.
     *
     * @return the all datatype unite variable acbb
     */
    List<DatatypeVariableUniteACBB> getAllDatatypeVariableUniteACBB();

    /**
     * Gets the all {@link DatatypeVariableUniteACBB} by datatypeName map by
     * variable affichage.
     *
     * @param datatypeName the datatype name
     * @return the all {@link DatatypeVariableUniteACBB} for datatypeName map by
     * variable affichage
     */
    Map<String, DatatypeVariableUniteACBB> getAllVariableTypeDonneesByDataTypeMapByVariableAffichage(
            String datatypeName);

    /**
     * Gets the all {@link DatatypeVariableUniteACBB} by data type map by
     * variable code.
     *
     * @param datatypeName the datatypeName
     * @return the all {@link DatatypeVariableUniteACBB} for datatypeName map by
     * variable code
     */
    Map<String, DatatypeVariableUniteACBB> getAllVariableTypeDonneesByDataTypeMapByVariableCode(
            String datatypeName);

    /**
     * Gets {@link DatatypeVariableUniteACBB} by datatype and variable.
     *
     * @param datatype
     * @param variable
     * @return the {@link DatatypeVariableUniteACBB} by datatype and variable {@link String} the
     * datatype {@link VariableACBB} the variable
     * @link(String) the datatype
     * @link(VariableACBB) the variable
     */
    Optional<DatatypeVariableUniteACBB> getByDatatypeAndVariable(String datatype, VariableACBB variable);

    /**
     * Gets the name of the variable for datatype name.
     *
     * @param datatypeName
     * @return the name variable for datatype name {@link String} the datatype name
     * @link(String) the datatype name
     */
    Optional<String> getNameVariableForDatatypeName(String datatypeName);

    /**
     * Gets the by datatype.
     *
     * @param datatypeCode the datatype code
     * @return the by datatype
     * @link(String) the datatype code
     */
    List<DatatypeVariableUniteACBB> getByDatatype(String datatypeCode);

    /**
     * Gets the by n key.
     *
     * @param datatypeCode the datatype code
     * @param variableCode the variable code
     * @param uniteCode    the unite code
     * @return the by n key
     * @link(String) the datatype code
     * @link(String) the variable code
     * @link(String) the unite code
     */
    Optional<DatatypeVariableUniteACBB> getByNKey(String datatypeCode, String variableCode, String uniteCode);

    /**
     * Gets the unite.
     *
     * @param datatype the datatype
     * @param variable the variable
     * @return the unite
     * @link(String) the datatype
     * @link(String) the variable
     */
    Optional<Unite> getUnite(String datatype, String variable);

    /**
     * @param realNode
     * @return
     */
    RealNode mergeRealNode(RealNode realNode);

    /**
     * For a deposit node realnode, return all the children realNode variables
     *
     * @param realNode
     * @return
     */
    Map<Variable, RealNode> getRealNodesVariables(RealNode realNode);

    /**
     * @param dvu
     * @throws PersistenceException
     */
    void delete(DatatypeVariableUniteACBB dvu) throws PersistenceException;
}
