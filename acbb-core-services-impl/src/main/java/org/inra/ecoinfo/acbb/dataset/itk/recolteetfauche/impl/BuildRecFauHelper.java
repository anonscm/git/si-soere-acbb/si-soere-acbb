package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IRecFauDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IValeurRecFauDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildTravailDusolHelper.
 */
public class BuildRecFauHelper extends AbstractBuildHelper<RecFauLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl.messages";

    /**
     * The semis_.
     */
    Map<Parcelle, Map<LocalDate, Map<Integer, RecFau>>> recfaus = new HashMap();

    IRecFauDAO recfauDAO;

    IValeurRecFauDAO valeurRecFauDAO;

    /**
     * The tempo dao @link(ITempoDAO).
     */
    ITempoDAO tempoDAO;

    /**
     * Adds the mesures semis.
     *
     * @param line
     * @param semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    void addValeursRecFau(final RecFauLineRecord line, final RecFau recfau) {
        for (final ACBBVariableValue<ListeItineraire> variableValue : line.getVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurRecFau valeur;
            if (variableValue.isValeurQualitative()) {
                valeur = new ValeurRecFau(variableValue.getValeurQualitative(),
                        realNode, recfau);
            } else {
                valeur = new ValeurRecFau(Float.parseFloat(variableValue.getValue()),
                        realNode, recfau);
            }
            recfau.getValeursRecFau().add(valeur);
        }
    }

    /**
     * Builds the datas and record it to database.
     *
     * @throws PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() throws PersistenceException {
        for (final RecFauLineRecord line : this.lines) {
            final RecFau recfau = this.getOrCreateRecFaus(line);
            if (recfau != null) {
                this.recfauDAO.saveOrUpdate(recfau);
            }
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursRecFau(line, recfau);
            this.recfauDAO.saveOrUpdate(recfau);
        }
    }

    /**
     * Instantiates a new builds semis helper.
     *
     * @param version              the version {@link Version
     * @param lines@param          errorsReport
     *                             the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @throws PersistenceException
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(RequestPropertiesSemis) the request properties semis
     */
    public void build(final VersionFile version, final List<RecFauLineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK)
            throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    /**
     * Creates the new semis.
     *
     * @param line
     * @return the semis
     * @link(LineRecord) the line
     */
    RecFau createNewRecFau(final RecFauLineRecord line) {
        RecFau recfau = null;
        final VersionDeTraitement versionTraitement = line.getVersionDeTraitement();
        final Parcelle parcelle = line.getParcelle();
        final Tempo tempo = this.getTempo(line, versionTraitement, parcelle);
        if (tempo.isInErrors()) {
            return recfau;
        }
        recfau = new RecFau(this.version, line.getObservation(), line.getDate(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber(), line.getRecFauPassage(), tempo);
        recfau.setSuiviParcelle(line.getSuiviParcelle());
        try {
            this.recfauDAO.saveOrUpdate(recfau);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return recfau;

    }

    /**
     * Gets the or create semis.
     *
     * @param line
     * @return the or create semis
     * @link(LineRecord) the line
     * @link(LineRecord) the line
     */
    RecFau getOrCreateRecFaus(final RecFauLineRecord line) {
        VersionDeTraitement versionDeTraitement = null;
        try {
            versionDeTraitement = this.versionDeTraitementDAO.merge(line.getVersionDeTraitement());
            line.setVersionDeTraitement(versionDeTraitement);
        } catch (final PersistenceException e) {
            LOGGER.error("can't getOrCreateFne");
        }
        return recfaus
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getRecFauPassage(), k -> createNewRecFau(line));
    }

    /**
     * @param recfauDAO the recfauDAO to set
     */
    public final void setRecFauDAO(final IRecFauDAO recfauDAO) {
        this.recfauDAO = recfauDAO;
    }

    /**
     * @param tempoDAO the tempoDAO to set
     */
    public final void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * @param valeurRecFauDAO the valeurRecFauDAO to set
     */
    public final void setValeurRecFauDAO(final IValeurRecFauDAO valeurRecFauDAO) {
        this.valeurRecFauDAO = valeurRecFauDAO;
    }

}
