/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.parcelle;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.dataset.IRecorderDAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<Parcelle> {

    /**
     * The LOGGER.
     */
    static final protected Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.parcelle.messages";
    /**
     * The Constant PROPERTY_MSG_BAD_DATE_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_BAD_DATE_FORMAT = "PROPERTY_MSG_BAD_DATE_FORMAT";
    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    /**
     * The blocs possibles @link(Map<String,String[]>).
     */
    final Map<String, String[]> blocsPossibles = new ConcurrentHashMap();
    /**
     * The repetition possibles @link(Map<String,String[]>).
     */
    final Map<String, String[]> repetitionPossibles = new ConcurrentHashMap();
    /**
     * The site dao.
     */
    protected ISiteACBBDAO siteDAO;
    /**
     * The parcelle dao.
     */
    protected IParcelleDAO parcelleDAO;
    /**
     * The bloc dao.
     */
    protected IBlocDAO blocDAO;
    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomFR;
    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;
    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesDescriptionEN;
    /**
     * The sites @link(String[]).
     */
    String[] sites;
    /**
     * The blocs and repetition possibles.
     */
    List<Map<String, String[]>> blocsAndRepetitionPossibles = new LinkedList();
    /**
     * The recorder dao @link(IRecorderDAO).
     */
    IRecorderDAO recorderDAO;

    /**
     * Instantiates a new recorder.
     */
    public Recorder() {
        super();
        this.blocsAndRepetitionPossibles.add(this.blocsPossibles);
        this.blocsAndRepetitionPossibles.add(this.repetitionPossibles);
    }

    /**
     * Builds the string date creation.
     *
     * @param parcelle
     * @return the string
     * @link(Parcelle) the parcelle
     * @link(Parcelle) the parcelle
     */
    String buildStringDateCreation(final Parcelle parcelle) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (parcelle.getDateCreation() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(parcelle.getDateCreation(), this.datasetDescriptor.getColumns().get(7).getFormatType());
        }
        return dateDebut;
    }

    /**
     * Creates the or update parcelle.
     *
     * @param nomParcelle
     * @param surface
     * @param dateCreation
     * @param commentaire
     * @param dbSite
     * @param dbBloc
     * @param dbParcelle
     * @throws PersistenceException the persistence exception
     * @link(String) the nom parcelle
     * @link(Float) the surface
     * @link(Date) the date creation
     * @link(String) the commentaire
     * @link(SiteACBB) the db site
     * @link(Bloc) the db bloc
     * @link(Parcelle) the db parcelle
     * @link(String) the nom parcelle
     * @link(Float) the surface
     * @link(Date) the date creation
     * @link(String) the commentaire
     * @link(SiteACBB) the db site
     * @link(Bloc) the db bloc
     * @link(Parcelle) the db parcelle
     */
    void createOrUpdateParcelle(final String nomParcelle, final Float surface,
                                final LocalDate dateCreation, final String commentaire, final SiteACBB dbSite,
                                final Bloc dbBloc, final Parcelle dbParcelle) throws PersistenceException {
        if (dbParcelle == null) {
            final Parcelle parcelle = new Parcelle(nomParcelle, surface, dateCreation, commentaire);
            parcelle.setSite(dbSite);
            parcelle.setBloc(dbBloc);
            this.parcelleDAO.saveOrUpdate(parcelle);
            parcelleDAO.flush();
        } else {
            dbParcelle.setSurface(surface);
            dbParcelle.setCommentaire(commentaire);
            dbParcelle.setDateCreation(dateCreation);
            this.parcelleDAO.saveOrUpdate(dbParcelle);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                final String nomParcelle = tokenizerValues.nextToken();
                final SiteACBB site = (SiteACBB) this.siteDAO.getByPath(codeSite)
                        .orElseThrow(() -> new BusinessException("bad site"));
                this.mgaServiceBuilder.getRecorder().remove(this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(nomParcelle, site))
                        .orElseThrow(() -> new BusinessException("bad parcelle")));
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Parcelle> getAllElements() {
        List<Parcelle> all = this.parcelleDAO.getAll(Parcelle.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the names blocs and repetition possibles by site.
     *
     * @return the names blocs and repetition possibles by site
     * @throws PersistenceException the persistence exception
     */
    List<Map<String, String[]>> getNamesBlocsAndRepetitionPossiblesBySite()
            throws PersistenceException {
        this.blocsAndRepetitionPossibles.get(0).clear();
        this.blocsAndRepetitionPossibles.get(1).clear();
        final List<SiteACBB> localSites = this.siteDAO.getAllSitesACBB();
        String key;
        for (final SiteACBB site : localSites) {
            final List<Bloc> blocs = this.blocDAO.getBySite(site);
            if (!blocs.isEmpty()) {
                final List<String> namesBlocsPossiblesForSite = new LinkedList();
                final Map<String, List<String>> namesRepetitonsPossiblesForSite = new HashMap();
                for (final Bloc bloc : blocs) {
                    if (bloc.getParent() == null) {
                        namesBlocsPossiblesForSite.add(bloc.getNom());
                        key = site.getName()
                                .concat(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR)
                                .concat(bloc.getNom());
                        this.initList(key, namesRepetitonsPossiblesForSite);
                    } else {
                        key = site.getName()
                                .concat(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR)
                                .concat(bloc.getParent().getNom());
                        this.initList(key, namesRepetitonsPossiblesForSite);
                        namesRepetitonsPossiblesForSite.get(key).add(bloc.getNom());
                    }
                }
                this.blocsAndRepetitionPossibles.get(0).put(site.getName(),
                        namesBlocsPossiblesForSite.toArray(new String[]{}));
                for (final Entry<String, List<String>> entry : namesRepetitonsPossiblesForSite
                        .entrySet()) {
                    this.blocsAndRepetitionPossibles.get(1).put(entry.getKey(),
                            entry.getValue().toArray(new String[]{}));
                }
            } else {
                this.blocsAndRepetitionPossibles.get(0).put(site.getName(),
                        new String[]{AbstractCSVMetadataRecorder.EMPTY_STRING});
                this.blocsAndRepetitionPossibles.get(1).put(
                        site.getName()
                                .concat(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR)
                                .concat(AbstractCSVMetadataRecorder.EMPTY_STRING),
                        new String[]{AbstractCSVMetadataRecorder.EMPTY_STRING});
            }
        }
        return this.blocsAndRepetitionPossibles;
    }

    /**
     * Gets the names sites possibles.
     *
     * @return the names sites possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesSitesPossibles() throws PersistenceException {
        final List<SiteACBB> dateDebutTraitementProgramme = this.siteDAO.getAllSitesACBB();
        final String[] namesSitesPossibles = new String[dateDebutTraitementProgramme.size()];
        int index = 0;
        for (final Site site : dateDebutTraitementProgramme) {
            namesSitesPossibles[index++] = site.getName();
        }
        return namesSitesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param parcelle
     * @return the new line model grid metadata
     * @link(Parcelle) the parcelle
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Parcelle parcelle) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata columnModelGridMetadataSite = new ColumnModelGridMetadata(
                parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : parcelle.getSite()
                        .getName(), this.sites, null, true, false, true);
        final ColumnModelGridMetadata columnModelGridMetadataBloc = new ColumnModelGridMetadata(
                parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : parcelle.getBloc() != null ? parcelle.getBloc().getParent() != null ? parcelle
                        .getBloc().getParent().getNom()
                        : parcelle.getBloc().getNom()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                this.blocsAndRepetitionPossibles.get(0), null, true, false, false);
        final ColumnModelGridMetadata columnModelGridMetadataRepetition = new ColumnModelGridMetadata(
                AbstractCSVMetadataRecorder.EMPTY_STRING, this.blocsAndRepetitionPossibles.get(1),
                null, true, false, false);
        final List<ColumnModelGridMetadata> refsSite = new LinkedList();
        refsSite.add(columnModelGridMetadataBloc);
        columnModelGridMetadataSite.setRefs(refsSite);
        columnModelGridMetadataBloc
                .setValue(parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : parcelle
                        .getBloc() != null ? parcelle.getBloc().getParent() != null ? parcelle
                        .getBloc().getParent().getNom() : parcelle.getBloc().getNom()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING);
        final List<ColumnModelGridMetadata> refsBloc = new LinkedList();
        refsBloc.add(columnModelGridMetadataRepetition);
        columnModelGridMetadataBloc.setRefs(refsBloc);
        columnModelGridMetadataRepetition
                .setValue(parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : parcelle
                        .getBloc() != null ? parcelle.getBloc().getParent() != null ? parcelle
                        .getBloc().getNom() : org.apache.commons.lang.StringUtils.EMPTY : org.apache.commons.lang.StringUtils.EMPTY);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataBloc);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataRepetition);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : parcelle
                                .getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomFR.getProperty(parcelle.getName(), parcelle.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.getProperty(parcelle.getName(), parcelle.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : parcelle
                                .getSurface(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this
                                .buildStringDateCreation(parcelle),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : parcelle
                                .getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        parcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesDescriptionEN.getProperty(parcelle.getCommentaire(), parcelle.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the list.
     *
     * @param key
     * @param namesRepetitonsPossiblesForSite
     * @link(String) the key
     * @link(Map<String,List<String>>) the names repetitons possibles for site
     * @link(String) the key
     * @link(Map<String,List<String>>) the names repetitons possibles for site
     */
    void initList(final String key, final Map<String, List<String>> namesRepetitonsPossiblesForSite) {
        if (namesRepetitonsPossiblesForSite.get(key) == null) {
            namesRepetitonsPossiblesForSite.put(key, new LinkedList<>());
            namesRepetitonsPossiblesForSite.get(key).add(AbstractCSVMetadataRecorder.EMPTY_STRING);
        }
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Parcelle> initModelGridMetadata() {
        try {
            this.sites = this.getNamesSitesPossibles();
            this.blocsAndRepetitionPossibles = this.getNamesBlocsAndRepetitionPossiblesBySite();

            this.propertiesNomFR = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRENCH);

            this.propertiesNomEN = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
            this.propertiesDescriptionEN = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Parcelle.ATTRIBUTE_JPA_DESCRIPTION, Locale.ENGLISH);
            return super.initModelGridMetadata();
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist parcelle.
     *
     * @param errorsReport
     * @param codeSite
     * @param codeBloc
     * @param codeRepetition
     * @param nomParcelle
     * @param surface
     * @param dateCreation
     * @param commentaire
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the code bloc
     * @link(String) the code repetition
     * @link(String) the nom parcelle
     * @link(Float) the surface
     * @link(Date) the date creation
     * @link(String) the commentaire
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the code bloc
     * @link(String) the code repetition
     * @link(String) the nom parcelle
     * @link(Float) the surface
     * @link(Date) the date creation
     * @link(String) the commentaire
     */
    void persistParcelle(final ErrorsReport errorsReport, final String codeSite,
                         final String codeBloc, final String codeRepetition, final String nomParcelle,
                         final Float surface, final LocalDate dateCreation, final String commentaire)
            throws BusinessException {
        final SiteACBB dbSite = this.retrieveDBSite(errorsReport, codeSite);
        Bloc dbBloc = null;
        if (dbSite != null) {
            if (codeBloc != null) {
                if (codeRepetition != null) {
                    dbBloc = this.retrieveDBBloc(errorsReport,
                            codeBloc.concat(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR)
                                    .concat(codeRepetition), dbSite.getId()).orElse(null);
                } else {
                    dbBloc = this.retrieveDBBloc(errorsReport, codeBloc, dbSite.getId()).orElse(null);
                }
            }
            final Parcelle dbParcelle = this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(nomParcelle, dbSite))
                    .orElse(null);
            try {
                this.createOrUpdateParcelle(nomParcelle, surface, dateCreation, commentaire, dbSite,
                        dbBloc, dbParcelle);
            } catch (PersistenceException ex) {
                throw new BusinessException("can't save parcelle", ex);
            }
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(Parcelle.class));
                // On parcourt chaque colonne d'une ligne
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeBloc = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeRepetition = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                final String nomParcelle = tokenizerValues.nextToken();
                final Float surface = tokenizerValues.nextTokenFloat();
                final String dateCreationString = tokenizerValues.nextToken();
                try {
                    final LocalDate dateCreation = dateCreationString != null
                            && dateCreationString.length() > 0
                            ? DateUtil.readLocalDateFromText(this.datasetDescriptor.getColumns().get(7).getFormatType(), dateCreationString)
                            : null;
                    final String commentaire = tokenizerValues.nextToken();
                    this.persistParcelle(errorsReport, codeSite, codeBloc, codeRepetition,
                            nomParcelle, surface, dateCreation, commentaire);
                } catch (final DateTimeException e) {
                    LOGGER.debug(e.getMessage(), e);
                    errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                            Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_DATE_FORMAT),
                            dateCreationString, this.datasetDescriptor.getColumns().get(5)
                                    .getFormatType()));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db bloc.
     *
     * @param errorsReport
     * @param codeBloc
     * @param siteId       <long> the site id
     * @return the bloc
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code bloc
     * @link(ErrorsReport) the errors report
     * @link(String) the code bloc
     */
    Optional<Bloc> retrieveDBBloc(final ErrorsReport errorsReport, final String codeBloc, final Long siteId) {
        return this.blocDAO.getByNKey(siteId, codeBloc);
    }

    /**
     * Retrieve db site.
     *
     * @param errorsReport
     * @param codeSite
     * @return the site acbb
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     */
    SiteACBB retrieveDBSite(final ErrorsReport errorsReport, final String codeSite) {
        final SiteACBB dbSite = (SiteACBB) this.siteDAO.getByPath(codeSite).orElse(null);
        if (dbSite == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), codeSite));
        }
        return dbSite;
    }

    /**
     * Sets the bloc dao.
     *
     * @param blocDAO the new bloc dao
     */
    public final void setBlocDAO(final IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    /**
     * Sets the parcelle dao.
     *
     * @param parcelleDAO the new parcelle dao
     */
    public final void setParcelleDAO(final IParcelleDAO parcelleDAO) {
        this.parcelleDAO = parcelleDAO;
    }

    /**
     * @param recorderDAO the recorderDAO to set
     */
    public final void setRecorderDAO(final IRecorderDAO recorderDAO) {
        this.recorderDAO = recorderDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

}
