package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.IAIDAO;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.ValeurAI;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.semis.impl.LineRecord;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildAIHelper.
 * <p>
 * record the content of a {@link LineRecord} into database
 */
public class BuildAIHelper extends AbstractBuildHelper<AILineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.autreintervention.impl.messages";

    /**
     * The ais.
     */
    Map<Parcelle, Map<LocalDate, AI>> ais = new HashMap();

    /**
     * The ai dao.
     */
    IAIDAO aiDao;

    /**
     * The tempo dao.
     */
    ITempoDAO tempoDAO;

    /**
     * Adds the valeurs ai.
     *
     * @param line
     * @param ai
     * @link{AILineRecord the line
     * @link{AI the ai
     */
    void addValeursAI(final AILineRecord line, final AI ai) {
        for (final ACBBVariableValue<ListeItineraire> variableValue : line.getACBBVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurAI valeur;
            if (variableValue.isValeurQualitative()) {
                valeur = new ValeurAI(variableValue.getValeurQualitative(),
                        realNode, ai);
            } else {
                valeur = new ValeurAI(Float.parseFloat(variableValue.getValue()),
                        realNode, ai);
            }
            ai.getValeursAI().add(valeur);
        }
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() throws PersistenceException {
        for (final AILineRecord line : this.lines) {
            final AI ai = this.getOrCreateAI(line);
            this.aiDao.saveOrUpdate(ai);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursAI(line, ai);
            this.aiDao.saveOrUpdate(ai);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesITK
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesITK the request properties itk
     */
    public void build(final VersionFile version, final List<AILineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK)
            throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    AI createNewAI(final AILineRecord line) {
        final Parcelle parcelle = line.getParcelle();
        final Tempo tempo = this.getTempo(line, line.getVersionDeTraitement(), parcelle);
        AI ai = null;
        if (tempo.isInErrors()) {
            return ai;
        }
        ai = new AI(this.version, line.getObservation(), line.getDate(), line.getRotationNumber(),
                line.getAnneeNumber(), line.getPeriodeNumber(),
                tempo);
        ai.setSuiviParcelle(line.getSuiviParcelle());
        try {
            this.aiDao.saveOrUpdate(ai);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return ai;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    AI getOrCreateAI(final AILineRecord line) {
        return ais
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> createNewAI(line));
    }

    /**
     * Sets the ai dao.
     *
     * @param aiDao the new ai dao
     */
    public void setAiDAO(IAIDAO aiDao) {
        this.aiDao = aiDao;
    }

    /**
     * Sets the tempo dao.
     *
     * @param tempoDAO the new tempo dao
     */
    public final void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

}
