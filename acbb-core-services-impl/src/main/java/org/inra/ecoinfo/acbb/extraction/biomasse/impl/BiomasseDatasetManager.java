/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseDatasetManager;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.*;

/**
 * The Class FluxMeteoDatasetManager.
 */
public class BiomasseDatasetManager extends DefaultDatasetManager implements
        IBiomasseDatasetManager {

    /**
     *
     */
    public static final String LAI_DATATYPE = "indice_de_surface_foliaire";
    /**
     *
     */
    public static final String HAUTEUR_HERBE_DATATYPE = "hauteur_vegetal";
    /**
     *
     */
    public static final String BIOMASSE_PRODUCTION_DATATYPE = "biomasse_production_teneur";
    static final String SITE_LAI_PRIVILEGE_PATTERN = "E->(%s:site,*,indice_de_surface_foliaire:datatype)";
    static final String VARIABLE_LAI_PRIVILEGE_PATTERN = "E->(%s:site,*,indice_de_surface_foliaire:datatype,%s:variable";
    static final String SITE_HAUTEUR_HERBE_PRIVILEGE_PATTERN = "E->(%s:site,*,hauteur_vegetal:datatype)";
    static final String VARIABLE_HAUTEUR_HERBE_PRIVILEGE_PATTERN = "E->(%s:site,*,hauteur_vegetal:datatype,%s:variable";
    static final String SITE_BIOMASSE_PRODUCTION_PRIVILEGE_PATTERN = "E->(%s:site,*,biomasse_production_teneur:datatype)";
    static final String VARIABLE_BIOMASSE_PRODUCTION_PRIVILEGE_PATTERN = "E->(%s:site,*,biomasse_production_teneur:datatype,%s:variable";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("rawtypes")
    IBiomasseExtractionDAO biomasseExtractionDAO;

    Map<String, IBiomasseExtractionDAO> extractionsDAO = new HashMap();
    Map<String, String> variablePatterns = new HashMap();

    /**
     * The security context @link(IPolicyManager). IPolicyManager policyManager;
     */
    public BiomasseDatasetManager() {
        super();
        this.variablePatterns.put(BiomasseDatasetManager.LAI_DATATYPE,
                BiomasseDatasetManager.VARIABLE_LAI_PRIVILEGE_PATTERN);
        this.variablePatterns.put(BiomasseDatasetManager.HAUTEUR_HERBE_DATATYPE,
                BiomasseDatasetManager.VARIABLE_HAUTEUR_HERBE_PRIVILEGE_PATTERN);
        this.variablePatterns.put(BiomasseDatasetManager.BIOMASSE_PRODUCTION_DATATYPE,
                BiomasseDatasetManager.VARIABLE_BIOMASSE_PRODUCTION_PRIVILEGE_PATTERN);

    }

    /**
     * @param intervalsDate
     * @return
     */
    @Override
    public Collection<? extends TraitementProgramme> getAvailableTraitements(
            List<IntervalDate> intervalsDate) {
        final Set<TraitementProgramme> availablesTraitements = new HashSet();
        for (Map.Entry<String, IBiomasseExtractionDAO> entry : extractionsDAO.entrySet()) {
            IBiomasseExtractionDAO interventionDAO = entry.getValue();
            availablesTraitements.addAll(interventionDAO.getAvailablesTraitementsForPeriode(intervalsDate, policyManager.getCurrentUser()));
        }
        return availablesTraitements;
    }

    /**
     * @param biomasseProductionExtractionDAO
     */
    public void setBiomasseProductionExtractionDAO(
            IBiomasseExtractionDAO<BiomassProduction> biomasseProductionExtractionDAO) {
        this.extractionsDAO.put(BiomasseDatasetManager.BIOMASSE_PRODUCTION_DATATYPE,
                biomasseProductionExtractionDAO);
    }

    /**
     * @param hauteurVegetalExtractionDAO
     */
    public void setHauteurVegetalExtractionDAO(
            IBiomasseExtractionDAO<HauteurVegetal> hauteurVegetalExtractionDAO) {
        this.extractionsDAO.put(BiomasseDatasetManager.HAUTEUR_HERBE_DATATYPE,
                hauteurVegetalExtractionDAO);
    }

    /**
     * @param laiExtractionDAO
     */
    public void setLaiExtractionDAO(IBiomasseExtractionDAO<Lai> laiExtractionDAO) {
        this.extractionsDAO.put(BiomasseDatasetManager.LAI_DATATYPE, laiExtractionDAO);
    }

    /**
     * @param treatment
     * @param variable
     * @param returnVariables
     * @return
     */
    protected boolean testAndAddVariable(final TraitementProgramme treatment,
                                         final VariableACBB variable, List<VariableACBB> returnVariables) {
        String expression = String.format(BiomasseDatasetManager.VARIABLE_HAUTEUR_HERBE_PRIVILEGE_PATTERN, treatment.getSite().getCode(), variable.getCode());
        if (this.policyManager.isRoot()
                || this.policyManager.checkActivity(policyManager.getCurrentUser(), AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS, Activities.extraction.name(), expression)) {
            if (!returnVariables.contains(variable)) {
                returnVariables.add(variable);
            }
            return true;
        }
        return false;
    }

    /**
     * @param linkedList
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByTreatmentAndDatesInterval(List<TraitementProgramme> linkedList, List<IntervalDate> intervals) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        for (Map.Entry<String, IBiomasseExtractionDAO> entry : extractionsDAO.entrySet()) {
            String datatype = entry.getKey();
            IBiomasseExtractionDAO interventionDAO = entry.getValue();
            variablesByDatatype.put(datatype, interventionDAO.getAvailableVariableByTraitement(linkedList, intervals, policyManager.getCurrentUser()));
        }
        return variablesByDatatype;

    }

}
