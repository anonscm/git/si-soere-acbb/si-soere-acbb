/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.paturage.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage_;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.paturage.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPAPaturageExtractionDAO extends AbstractJPAITKDAO<Paturage, Paturage, ValeurPaturage> {

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<Paturage> getMesureITKClass() {
        return Paturage.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurPaturage> getValeurITKClass() {
        return ValeurPaturage.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, Paturage> getMesureAttribute() {
        return ValeurPaturage_.paturage;
    }
}
