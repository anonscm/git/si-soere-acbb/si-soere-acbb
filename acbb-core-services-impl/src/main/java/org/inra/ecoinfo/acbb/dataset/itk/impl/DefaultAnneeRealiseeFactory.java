package org.inra.ecoinfo.acbb.dataset.itk.impl;


import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;

import javax.persistence.Transient;

/**
 * A factory for creating DefaultAnneeRealisee objects.
 *
 * @param <T> the generic line
 * @author philippe Tcherniatinsky
 */
public class DefaultAnneeRealiseeFactory<T extends ILineRecord> extends AbstractPeriodFactory
        implements IAnneeRealiseeFactory<T> {

    /**
     * The annee realisee dao @link(IAnneeRealiseeDAO).
     */
    @Transient
    protected IAnneeRealiseeDAO anneeRealiseeDAO;

    /**
     * The periode realisee factory @link(IPeriodeRealiseeFactory).
     */
    @Transient
    protected IPeriodeRealiseeFactory<T> periodeRealiseeFactory;

    /**
     * <p>
     * return both
     * </p>
     * <ul>
     * <li>the AnneeRealisee in tempo if exists</li>
     * <li>the Anneerealisee in Db for the VersionRealisee and the Annee Number</li>
     * </ul>
     *
     * @param line
     * @param infoReport
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeFactory#getOrCreateAnneeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public AnneeRealisee getOrCreateAnneeRealisee(final T line, final ErrorsReport errorsReport,
                                                  final InfoReport infoReport) {
        return line.getTempo().getAnneeRealisee() == null ?
                this.anneeRealiseeDAO.getByNKey(line.getTempo().getVersionTraitementRealisee(), line.getAnneeNumber()).orElse(null) :
                line.getTempo().getAnneeRealisee();
    }

    /**
     * return IperiodeRealiseefactory
     *
     * @return
     */
    @Override
    public IPeriodeRealiseeFactory<T> getPeriodeRealiseeFactory() {
        return this.periodeRealiseeFactory;
    }

    /**
     * Sets the periode realisee factory.
     *
     * @param periodeRealiseeFactory the periodeRealiseeFactory to set
     */
    public final void setPeriodeRealiseeFactory(
            final IPeriodeRealiseeFactory<T> periodeRealiseeFactory) {
        this.periodeRealiseeFactory = periodeRealiseeFactory;
    }

    /**
     * Sets the annee realisee dao.
     *
     * @param anneeRealiseeDAO the anneeRealiseeDAO to set
     */
    public final void setAnneeRealiseeDAO(final IAnneeRealiseeDAO anneeRealiseeDAO) {
        this.anneeRealiseeDAO = anneeRealiseeDAO;
    }

}
