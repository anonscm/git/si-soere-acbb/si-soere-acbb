package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.IAIDAO;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;

/**
 * The Class JPASemisDAO.
 */
public class JPAAIDAO extends JPAInterventionDAO<AI> implements IAIDAO {

    @Override
    protected Class<AI> getInterventionClass() {
        return AI.class;
    }
}
