/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.impl;

import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;

/**
 * @author tcherniatinsky
 */
class ACBBLocalizedFormatterForVariableNameGetDescription implements ILocalizedFormatter<DatatypeVariableUniteACBB> {

    ILocalizationManager localizationManager;

    public ACBBLocalizedFormatterForVariableNameGetDescription() {
    }

    @Override
    public String format(DatatypeVariableUniteACBB nodeable, Locale locale, Object... arguments) {
        final Properties propertiesVariablesDefinitions = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), "definition", locale);
        return Optional.ofNullable(nodeable).map(nodeabledvu -> StringUtils.isEmpty(nodeabledvu.getVariable().getDefinition()) ? nodeabledvu.getVariable().getName() : nodeabledvu.getVariable().getDefinition())
                .map(definition -> propertiesVariablesDefinitions.getProperty(definition, definition))
                .map(definition -> String.format("<i>%s</i>", definition))
                .orElse("Error while retrieving variable definition)");
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
