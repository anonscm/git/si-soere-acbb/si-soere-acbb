/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipOutputStream;

/**
 * The Class BiomasseOutputsBuildersResolver.
 */
@SuppressWarnings({})
public abstract class BiomasseOutputsBuildersResolver extends AbstractOutputBuilder implements
        IOutputsBuildersResolver {

    /**
     * The Constant PATTERN_HEADER @link(String).
     */
    static final String PATTERN_HEADER = "%s;%s;%s(%s)";
    /**
     * The Constant EXTRACTION @link(String).
     */
    static final String EXTRACTION = "extraction";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant SEPARATOR_TEXT.
     */
    static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant EXTENSION_ZIP.
     */
    static final String EXTENSION_ZIP = ".zip";
    /**
     * The Constant FILENAME_REMINDER.
     */
    static final String FILENAME_REMINDER = "recapitulatif_extraction";
    /**
     * The Constant EXTENSION_CSV.
     */
    static final String EXTENSION_CSV = ".csv";
    /**
     * The Constant EXTENSION_TXT.
     */
    static final String EXTENSION_TXT = ".txt";
    /**
     * The Constant MAP_INDEX_0.
     */
    static final String MAP_INDEX_0 = "0";
    /**
     * The Constant REQUEST_REMINDER @link(String).
     */
    static final String REQUEST_REMINDER = "RequestReminder";
    /**
     * The LOGGER.
     */
    static final Logger LOGGER = LoggerFactory.getLogger(BiomasseDatasetManager.class);
    private static final int KO = 1_024;

    /**
     *
     */
    public BiomasseOutputsBuildersResolver() {
        super();
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     */
    protected void buidOutputFile(final IParameter parameters) throws BusinessException {
        final Set<String> datatypeNames = new HashSet();
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames, SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     * .extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        NoExtractionResultException noDataToExtract = null;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            parameters.getParameters().put(BiomasseExtractor.CST_RESULTS, parameters.getResults());
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
        } catch (FileNotFoundException e1) {
            throw new BusinessException("FileNotFoundException", e1);
        } catch (IOException e) {
            throw new BusinessException("IOException", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }

    /**
     * The Class BuildOutputHelper.
     */
    protected static class BuildOutputHelper {

        /**
         * The parameter.
         */
        protected BiomasseParameterVO parameter;
        /**
         * The print stream.
         */
        protected PrintStream printStream;

        /**
         * Instantiates a new builds the output helper.
         *
         * @param parameter the parameter
         * @param printStream the print stream
         * @throws NoExtractionResultException the no extract data found
         * exception
         */
        BuildOutputHelper(final BiomasseParameterVO parameter, final PrintStream printStream)
                throws NoExtractionResultException {
            super();
            this.parameter = parameter;
            this.printStream = printStream;
        }
    }
}
