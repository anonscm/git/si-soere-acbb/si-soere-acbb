package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction;

import org.inra.ecoinfo.acbb.dataset.biomasse.IBiomasseDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface IBiomassProductionDAO extends IBiomasseDAO<BiomassProduction> {

    /**
     * @param version
     * @param date
     * @return
     */
    Optional<BiomassProduction> getByNKey(VersionFile version, LocalDate date);
}
