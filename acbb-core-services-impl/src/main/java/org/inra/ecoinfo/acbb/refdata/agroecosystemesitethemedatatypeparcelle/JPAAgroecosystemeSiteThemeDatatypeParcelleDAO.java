/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.acbb.refdata.agroecosystemesitethemedatatypeparcelle;

import com.google.common.base.Strings;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Class JPAAgroecosystemeSiteThemeDatatypeParcelleDAO.
 *
 * @author Philippe TCHERNIATINSKY
 */
public class JPAAgroecosystemeSiteThemeDatatypeParcelleDAO extends AbstractJPADAO<INode>
        implements IAgroecosystemeSiteThemeDatatypeParcelleDAO {

    /**
     * The LOGGER.
     */
    static final protected Logger LOGGER = LoggerFactory.getLogger(JPAAgroecosystemeSiteThemeDatatypeParcelleDAO.class);

    @Override
    public List<INode> getByParcellePathSiteThemeCodeAndDatatypeCode(String pathSite, String themeCode, String datatypeCode, String parcelle) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<NodeDataSet> dtyNode = query.from(NodeDataSet.class);
        List<Predicate> and = new LinkedList();
        Path<Nodeable> dty = dtyNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        and.add(builder.equal(dty.get(Nodeable_.code), datatypeCode));
        Join<NodeDataSet, AbstractBranchNode> theNode = dtyNode.join(NodeDataSet_.parent, JoinType.LEFT);
        Path<Nodeable> the = theNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        and.add(builder.equal(the.get(Nodeable_.code), themeCode));
        Join<AbstractBranchNode, AbstractBranchNode> parNode = null, sitNode;
        Path<Nodeable> par, sit;
        if (Strings.isNullOrEmpty(parcelle)) {
            sitNode = theNode.join(NodeDataSet_.parent, JoinType.LEFT);
            sit = sitNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        } else {
            parNode = theNode.join(NodeDataSet_.parent, JoinType.LEFT);
            par = parNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
            and.add(builder.equal(par.get(Nodeable_.code), parcelle));
            sitNode = parNode.join(NodeDataSet_.parent, JoinType.LEFT);
            sit = sitNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        }
        and.add(builder.equal(sit.get(Nodeable_.code), pathSite));
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        query.multiselect(
                dtyNode,
                theNode,
                sitNode,
                parNode
        );
        Tuple result = getFirstOrNull(query);
        return result == null
                ? null
                : result.getElements().stream()
                .map(t -> (INode) result.get(t))
                .collect(Collectors.toList());
    }

    /**
     * @return
     */
    @Override
    public List<RealNode> loadRealDatatypeNodes() {

        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        Join<RealNode, Nodeable> rnNodeable = rn.join(RealNode_.nodeable);
        query
                .select(rn)
                .where(builder.equal(rnNodeable.type(), builder.literal(DataType.class)))
                .orderBy(builder.asc(rn.get(RealNode_.path)));
        return getResultList(query);
    }

    /**
     * @return
     */
    @Override
    public List<String> getPathes() {

        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<RealNode> rn = query.from(RealNode.class);
        query.select(rn.get(RealNode_.path));
        return getResultList(query);
    }

    @Override
    public void remove(INode object) throws PersistenceException {
        super.remove(object);
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        query.where(builder.equal(rn.join(RealNode_.parent), rn));
        query.select(rn);
        if (object.getRealNode() != null && getResultList(query).isEmpty()) {
            CriteriaDelete<RealNode> delete = builder.createCriteriaDelete(RealNode.class);
            rn = delete.from(RealNode.class);
            delete.where(builder.equal(rn, object));
            delete(delete);
            flush();
        }
    }
}
