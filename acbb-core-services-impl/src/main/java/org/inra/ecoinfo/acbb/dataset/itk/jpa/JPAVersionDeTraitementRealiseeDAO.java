/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.jpa;


import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee_;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPAVersionDeTraitementRealiseeDAO.
 */
public class JPAVersionDeTraitementRealiseeDAO extends AbstractJPADAO<VersionTraitementRealisee>
        implements IVersionDeTraitementRealiseeDAO {
    /**
     * Gets the by n key.
     *
     * @param versionDeTraitement
     * @param parcelle
     * @param rotation
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO#getByNKey
     * (org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement, int)
     * @link(VersionDeTraitement) the version de traitement
     * @link(int) the rotation
     */
    @Override
    public Optional<VersionTraitementRealisee> getByNKey(final VersionDeTraitement versionDeTraitement,
                                                         final Parcelle parcelle, final int rotation) {
        CriteriaQuery<VersionTraitementRealisee> query = builder.createQuery(VersionTraitementRealisee.class);
        Root<VersionTraitementRealisee> vtr = query.from(VersionTraitementRealisee.class);
        query
                .select(vtr)
                .where(
                        builder.equal(vtr.join(VersionTraitementRealisee_.versionDeTraitement), versionDeTraitement),
                        builder.equal(vtr.join(VersionTraitementRealisee_.parcelle), parcelle),
                        builder.equal(vtr.get(VersionTraitementRealisee_.numero), rotation)
                );
        return getOptional(query);
    }
}
