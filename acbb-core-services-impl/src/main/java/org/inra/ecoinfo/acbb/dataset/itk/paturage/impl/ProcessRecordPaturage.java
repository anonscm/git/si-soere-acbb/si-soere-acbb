package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.PaturageTypeAnimaux;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.*;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * process the record of tillage files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
public class ProcessRecordPaturage extends AbstractProcessRecordITK {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant SEMIS_BUNDLE_NAME @link(String).
     */
    static final String PATURAGE_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.paturage.messages";
    private static final String PROPERTY_MSG_UNFOUND_ANIMALS_TYPE = "PROPERTY_MSG_UNFOUND_ANIMALS_TYPE";

    /**
     * Builds the new line.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesITK
     * @throws PersistenceException
     * @link(VersionFile) the version
     * @link(List<? extends AbstractLineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(VersionFile) the version
     * @link(List<? extends AbstractLineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void buildNewLines(final VersionFile version,
                                 final List<? extends AbstractLineRecord> lines, final ErrorsReport errorsReport,
                                 final IRequestPropertiesITK requestPropertiesITK) throws PersistenceException {
        final BuildPaturageHelper instance = ApplicationContextHolder.getContext().getBean(
                BuildPaturageHelper.class);
        instance.build(version, (List<PaturageLineRecord>) lines, errorsReport,
                requestPropertiesITK);
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param cleanerValues
     * @param lineRecord
     * @param datasetDescriptorACBB
     * @param requestPropertiesITK
     * @param intervalDate
     * @return
     */
    @Override
    protected int getGenericColumns(final ErrorsReport errorsReport, final long lineCount,
                                    final CleanerValues cleanerValues, final AbstractLineRecord lineRecord,
                                    final DatasetDescriptorACBB datasetDescriptorACBB,
                                    final IRequestPropertiesITK requestPropertiesITK, IntervalDate intervalDate) {
        int index = 0;
        index = this.getParcelle(lineRecord, errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB, requestPropertiesITK);
        if (lineRecord.getParcelle() == null) {
            return ++index;
        }
        final LocalDate measureDate = this.getDate(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB);
        if (!intervalDate.isInto(measureDate.atStartOfDay())) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_DATE_FOR_INTERVAL),
                    lineCount,
                    index,
                    DateUtil.getUTCDateTextFromLocalDateTime(measureDate, DateUtil.DD_MM_YYYY),
                    intervalDate.getBeginDateToString(),
                    intervalDate.getEndDateToString()));
        }
        lineRecord.setDate(measureDate);
        ((PaturageLineRecord) lineRecord).setEndDate(this.getDate(errorsReport, lineCount, index++,
                cleanerValues, datasetDescriptorACBB));
        lineRecord.setObservation(this.getObservation(errorsReport, lineCount, index, cleanerValues));
        this.updateVersionDeTraitement(lineRecord, errorsReport, lineCount, index,
                datasetDescriptorACBB, requestPropertiesITK);
        index++;
        final int rotationNumber = this.getInt(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB);
        lineRecord.setRotationNumber(rotationNumber <= 0 ? 0 : rotationNumber);
        lineRecord.setAnneeNumber(this.getInt(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB));
        lineRecord.setPeriodeNumber(this.getInt(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB));
        return index;
    }

    /**
     * @param errorsReport
     * @param lineNumber
     * @param columnIndex
     * @param dvu
     * @param column
     * @param value
     * @param requestProperties
     * @param lineRecord
     * @return
     */
    protected ACBBVariableValue<ListeItineraire> getVariableValue(ErrorsReport errorsReport,
                                                                  Long lineNumber, int columnIndex, DatatypeVariableUniteACBB dvu, Column column,
                                                                  String value, IRequestPropertiesACBB requestProperties, PaturageLineRecord lineRecord) {
        ACBBVariableValue<ListeItineraire> variableValue = null;
        if (RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE.equals(column.getFlagType())
                && PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX.equals(column.getName())) {
            TraitementProgramme traitementProgramme = requestProperties.getTraitement();
            LocalDate date = lineRecord.getDate();
            VersionDeTraitement versionDeTraitement = this.versionDeTraitementDAO
                    .retrieveVersiontraitement(traitementProgramme, date).orElse(null);
            try {
                final List<PaturageTypeAnimaux> typesAnimaux = this.getListeItineraireDAO().getAllForClass(PaturageTypeAnimaux.class);
                ListeItineraire valeurQualitative = null;
                if (versionDeTraitement == null) {
                    for (PaturageTypeAnimaux paturageTypeAnimaux : typesAnimaux) {
                        if (paturageTypeAnimaux.getValeur().equals(Utils.createCodeFromString(value))) {
                            valeurQualitative = paturageTypeAnimaux;
                            break;
                        }
                    }
                } else {
                    String ta = null;
                    for (Modalite modalite : versionDeTraitement.getModalites()) {
                        if (modalite.getCode().startsWith("ta")) {
                            ta = modalite.getCode();
                            break;
                        }
                    }
                    if (ta == null) {
                        throw new PersistenceException(String.format(RecorderACBB
                                        .getACBBMessageWithBundle(
                                                ProcessRecordPaturage.PATURAGE_BUNDLE_NAME,
                                                ProcessRecordPaturage.PROPERTY_MSG_UNFOUND_ANIMALS_TYPE),
                                lineNumber, value, ta, traitementProgramme.getCode(),
                                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY)
                        ));
                    }
                    valeurQualitative = this.getListeItineraireDAO().getByNKey(ta, value).orElse(null);
                }
                if (valeurQualitative == null) {
                    throw new PersistenceException(
                            AbstractProcessRecord.PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE);
                }
                variableValue = new ACBBVariableValue<>(dvu, valeurQualitative);
            } catch (final PersistenceException e) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        AbstractProcessRecord.BUNDLE_PATH,
                        AbstractProcessRecord.PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE), lineNumber,
                        columnIndex + 1, value, column.getName()));
            }
        } else {
            variableValue = super.getVariableValue(errorsReport, lineNumber, columnIndex,
                    dvu, column, value);
        }
        return variableValue;
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser)
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile localVersionFile = versionFile;
        super.processRecord(parser, localVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesITK) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            localVersionFile = this.versionFileDAO.merge(localVersionFile);
            final List<PaturageLineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, requestProperties, datasetDescriptor, errorsReport,
                    lineCount, dbVariables, lines, intervalDate);
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(localVersionFile, lines, errorsReport,
                        (IRequestPropertiesITK) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param requestProperties
     * @param datasetDescriptor
     * @param errorsReport
     * @param localLineCount
     * @param dbVariables
     * @param lines
     * @param dbVariables2
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(CSVParser) the parser
     * @link(IRequestPropertiesACBB) the request properties
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(ErrorsReport) the errors report
     * @link(long) the line count
     * @link(List<VariableACBB>) the db variables
     * @link(List<LineRecord>) the lines
     */
    long readLines(final CSVParser parser, final IRequestPropertiesACBB requestProperties,
                   final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                   long lineCount, final List<DatatypeVariableUniteACBB> dbVariables,
                   final List<PaturageLineRecord> lines, IntervalDate intervalDate) throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final PaturageLineRecord lineRecord = new PaturageLineRecord(localLineCount);
            final int genericIndex = this.getGenericColumns(errorsReport, localLineCount,
                    cleanerValues, lineRecord, datasetDescriptor,
                    (IRequestPropertiesITK) requestProperties, intervalDate);
            for (final Entry<Integer, Column> entry : ((RequestPropertiesPaturage) requestProperties)
                    .getValueColumns().entrySet()) {
                if (entry.getKey() >= values.length) {
                    break;
                }
                final String value = values[entry.getKey()];
                if (entry.getKey() < genericIndex || Strings.isNullOrEmpty(value)) {
                    continue;
                }
                ACBBVariableValue<ListeItineraire> variableValue = null;
                final DatatypeVariableUniteACBB dvu = dbVariables.get(entry.getKey());
                variableValue = this.getVariableValue(errorsReport, lineCount, entry.getKey(),
                        dvu, entry.getValue(), value, requestProperties, lineRecord);
                lineRecord.getACBBVariablesValues().add(variableValue);
            }

            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }
}
