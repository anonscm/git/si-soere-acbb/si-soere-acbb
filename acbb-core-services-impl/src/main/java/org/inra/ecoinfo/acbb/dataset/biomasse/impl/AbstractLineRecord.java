package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.ILineRecordBiomasse;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class AbstractLineRecord.
 *
 * @param <T>
 * @param <U>
 */
public abstract class AbstractLineRecord<T extends AbstractLineRecord<T, U>, U extends AbstractVariableValueBiomasse> implements Comparable<T>, ILineRecordBiomasse<U> {

    Parcelle parcelle;

    /**
     * The observation @link(String).
     */
    String observation = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * The rotation number @link(int).
     */
    Integer rotationNumber = 0;

    /**
     * The annee number @link(int).
     */
    Integer anneeNumber = 0;

    /**
     * The periode number @link(int).
     */
    Integer periodeNumber = 0;

    InterventionType typeIntervention;
    AbstractIntervention intervention;

    /**
     * The date @link(Date).
     */
    LocalDate date;

    /**
     * The original line number <long>.
     */
    long originalLineNumber;
    TraitementProgramme traitementProgramme;
    VersionDeTraitement versionDeTraitement;
    LocalDate dateDebuttraitementSurParcelle;
    String serie;

    /**
     * The variables values @link(List<VariableValue>).
     */
    List<U> variablesValues = new LinkedList();

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    SuiviParcelle suiviParcelle;
    String natureCouvert;

    /**
     * @param originalLineNumber
     */
    public AbstractLineRecord(long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int
     * @link(AbstractLineRecord) the o
     * @link(AbstractLineRecord) the o
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final T o) {
        if (Float.floatToRawIntBits(this.originalLineNumber) == o.getOriginalLineNumber()) {
            return 0;
        }
        return this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
    }

    /**
     * Copy.
     *
     * @param line
     * @link(AbstractLineRecord) the line {@link AbstractLineRecord} the line
     */
    public void copy(final T line) {
        this.originalLineNumber = line.getOriginalLineNumber();
        this.observation = line.getObservation();
        this.rotationNumber = line.getRotationNumber();
        this.anneeNumber = line.getAnneeNumber();
        this.periodeNumber = line.getPeriodeNumber();
        this.typeIntervention = line.getTypeIntervention();
        this.intervention = line.getIntervention();
        this.suiviParcelle = line.getSuiviParcelle();
        this.versionDeTraitement = line.getVersionDeTraitement();
        this.dateDebuttraitementSurParcelle = line.getDateDebuttraitementSurParcelle();
        this.variablesValues = line.getVariablesValues();
        this.date = line.getDate();
        this.serie = line.getSerie();
        this.traitementProgramme = line.getTraitementProgramme();
        this.parcelle = line.getParcelle();
        this.natureCouvert = line.natureCouvert;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final AbstractLineRecord<T, U> other = (AbstractLineRecord<T, U>) obj;
        return this.originalLineNumber == other.originalLineNumber;
    }

    /**
     * @return
     */
    @Override
    public Integer getAnneeNumber() {
        return this.anneeNumber;
    }

    /**
     * @param anneeNumber
     */
    public void setAnneeNumber(Integer anneeNumber) {
        this.anneeNumber = anneeNumber;
    }

    /**
     * @return
     */
    @Override
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return
     */
    public LocalDate getDateDebuttraitementSurParcelle() {
        return this.dateDebuttraitementSurParcelle;
    }

    /**
     * @param dateDebuttraitementSurParcelle
     */
    public void setDateDebuttraitementSurParcelle(LocalDate dateDebuttraitementSurParcelle) {
        this.dateDebuttraitementSurParcelle = dateDebuttraitementSurParcelle;
    }

    /**
     * @return
     */
    @Override
    public AbstractIntervention getIntervention() {
        return this.intervention;
    }

    /**
     * @param intervention
     */
    public void setIntervention(AbstractIntervention intervention) {
        this.intervention = intervention;
    }

    /**
     * @return
     */
    @Override
    public String getObservation() {
        return this.observation;
    }

    /**
     * @param observation
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * @return
     */
    public String getSerie() {
        return serie;
    }

    /**
     * @param serie
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     * @return
     */
    @Override
    public long getOriginalLineNumber() {
        return this.originalLineNumber;
    }

    /**
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * @return
     */
    @Override
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    void setParcelle(Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * @return
     */
    public String getNatureCouvert() {
        return this.natureCouvert;
    }

    /**
     * @param natureCouvert
     */
    public void setNatureCouvert(String natureCouvert) {
        this.natureCouvert = natureCouvert;
    }

    /**
     * @return
     */
    @Override
    public Integer getPeriodeNumber() {
        return this.periodeNumber;
    }

    /**
     * @param periodeNumber
     */
    public void setPeriodeNumber(Integer periodeNumber) {
        this.periodeNumber = periodeNumber;
    }

    /**
     * @return
     */
    @Override
    public Integer getRotationNumber() {
        return this.rotationNumber;
    }

    /**
     * @param rotationNumber
     */
    public void setRotationNumber(Integer rotationNumber) {
        this.rotationNumber = rotationNumber;
    }

    /**
     * @return
     */
    @Override
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * @param suiviParcelle
     */
    public void setSuiviParcelle(SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * @return
     */
    public TraitementProgramme getTraitementProgramme() {
        return this.traitementProgramme;
    }

    /**
     * @param traitementProgramme
     */
    public void setTraitementProgramme(TraitementProgramme traitementProgramme) {
        this.traitementProgramme = traitementProgramme;
    }

    /**
     * @return
     */
    @Override
    public InterventionType getTypeIntervention() {
        return this.typeIntervention;
    }

    /**
     * @param typeIntervention
     */
    public void setTypeIntervention(InterventionType typeIntervention) {
        this.typeIntervention = typeIntervention;
    }

    /**
     * @return
     */
    @Override
    public List<U> getVariablesValues() {
        return this.variablesValues;
    }

    /**
     * @param variablesValues
     */
    public void setVariablesValues(List<U> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     * @return
     */
    @Override
    public VersionDeTraitement getVersionDeTraitement() {
        return this.versionDeTraitement;
    }

    /**
     * @param versionDeTraitement
     */
    public void setVersionDeTraitement(VersionDeTraitement versionDeTraitement) {
        this.versionDeTraitement = versionDeTraitement;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.originalLineNumber ^ this.originalLineNumber >>> 32);
        return hash;
    }

}
