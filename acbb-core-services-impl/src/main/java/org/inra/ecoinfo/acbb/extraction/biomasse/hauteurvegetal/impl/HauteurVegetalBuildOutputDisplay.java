package org.inra.ecoinfo.acbb.extraction.biomasse.hauteurvegetal.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.ValeurHauteurVegetal;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.AbstractBiomasseOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.refdata.variable.Variable;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class HauteurVegetalBuildOutputDisplay extends
        AbstractBiomasseOutputBuilder<HauteurVegetal, ValeurHauteurVegetal, HauteurVegetal> {

    /**
     *
     */
    public static final String PATTERN_6_FIELD = ";%s;%s;%s;%s;%s;%s";
    /**
     *
     */
    protected static final String FIRST_VARIABLE = "hav";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.messages";
    static final String PROPERTY_HEADER_RAW_DATA_FOR_METHOD_HAV = "PROPERTY_HEADER_RAW_DATA_FOR_METHOD_HAV";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public HauteurVegetalBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return HauteurVegetalBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedWsolVariables,
            Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            PrintStream rawDataPrintStream) {
        return new HauteurVegetalOutputHelper(availablesCouverts, rawDataPrintStream,
                selectedWsolVariables);
    }

    /**
     *
     */
    public class HauteurVegetalOutputHelper extends OutputHelper {

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         * @param availablesCouverts
         */
        public HauteurVegetalOutputHelper(final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
                                          PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(availablesCouverts, rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurBiomasse
         */
        @Override
        protected void addVariablesCulumns(ValeurHauteurVegetal valeurBiomasse) {
            String line = String.format(HauteurVegetalBuildOutputDisplay.PATTERN_6_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getStandardDeviation()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getMediane()),
                    valeurBiomasse.getMethode().getNumberId(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesCulumnsForNullVariable() {
            this.rawDataPrintStream.print(HauteurVegetalBuildOutputDisplay.PATTERN_6_FIELD
                    .replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param mesure
         */
        @Override
        protected void printLineSpecific(HauteurVegetal mesure) {
            String line;
            String nature = mesure.getHauteurVegetalNature().getValeur();
            nature = propertiesValeursQualitative.getProperty(nature, nature);
            String es = mesure.getBiomasseEntreeSortie() == null ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : mesure.getBiomasseEntreeSortie().getValeur();
            es = propertiesValeursQualitative.getProperty(es, es);
            line = String.format(HauteurVegetalBuildOutputDisplay.PATTERN_6_FIELD,
                    nature,
                    getCouvert(mesure),
                    getAnnee(mesure),
                    mesure.getSerie(),
                    getDateMesure(mesure),
                    es);
            this.rawDataPrintStream.print(line);
        }
    }
}
