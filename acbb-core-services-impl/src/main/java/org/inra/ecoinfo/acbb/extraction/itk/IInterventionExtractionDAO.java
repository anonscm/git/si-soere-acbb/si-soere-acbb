package org.inra.ecoinfo.acbb.extraction.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * The Interface IInterventionExtractionDAO.
 *
 * @param <T> the generic type
 */
public interface IInterventionExtractionDAO<T> extends IDAO<T> {

    /**
     * Extract itk mesures.
     *
     * @param selectedTraitementProgrammes
     * @param user
     * @param selectedVariables
     * @param intervalsDates
     * @return the list
     * @link(List<TraitementProgramme>) the selected traitement programmes
     * @link(List<Variable>) the selected variables
     * @link(List<IntervalDate>) the intervals dates
     */
    List<T> extractITKMesures(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervalsDates, IUser user);

    /**
     * Gets the availables variables by sites.
     *
     * @param linkedList
     * @param intervals
     * @param user
     * @return the availables variables by sites
     * @link(List<SiteACBB>) the linked list
     */
    List<NodeDataSet> getAvailableVariableByTraitement(List<TraitementProgramme> linkedList, List<IntervalDate> intervals, IUser user);

    /**
     * Gets the available traitements.
     *
     * @param intervalsDate
     * @param user
     * @return the available traitements
     * @throws SecurityException
     * @link(List<IntervalDate>) the intervals date
     */
    List<TraitementProgramme> getAvailablesTraitementsForPeriode(List<IntervalDate> intervalsDate, IUser user);

    Long getSize(List<TraitementProgramme> selectedTraitementProgrammes, List<Variable> selectedVariables, List<IntervalDate> intervals, IUser user);

}
