package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionForNumDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IRecFauDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public abstract class AbstractProcessRecordBiomasse extends
        AbstractProcessRecord<ListeVegetation, IListeVegetationDAO> {

    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_STANDARD_DEVIATION = "PROPERTY_MSG_BAD_STANDARD_DEVIATION";
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_MEASURE_NUMBER = "PROPERTY_MSG_BAD_MEASURE_NUMBER";
    /**
     *
     */
    static final public String PROPERTY_MSG_BAD_QUALITY_INDEX = "PROPERTY_MSG_BAD_QUALITY_INDEX";
    static final String PROPERTY_MSG_MISSING_PLOT_TRACKING = "PROPERTY_MSG_MISSING_PLOT_TRACKING";
    static final String PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT = "PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT";
    static final String PROPERTY_MSG_IGNORING_TYPE_INTERVENTION = "PROPERTY_MSG_IGNORING_TYPE_INTERVENTION";
    static final String PROPERTY_MSG_BAD_NUM_OR_DATE = "PROPERTY_MSG_BAD_NUM_OR_DATE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM = "PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_MISSING_DATE = "PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_MISSING_DATE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_BAD_TYPE = "PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_BAD_TYPE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_FOR_DATE = "PROPERTY_MSG_BAD_INTERVENTION_FOR_DATE";
    static final String PROPERTY_MSG_BAD_INTERVENTION_DATE = "PROPERTY_MSG_BAD_INTERVENTION_DATE";
    static final String PROPERTY_MSG_BAD_BIOMASSE_MEDIANE = "PROPERTY_MSG_BAD_BIOMASSE_MEDIANE";
    static final String PROPERTY_MSG_BAD_DATE_FOR_INTERVAL = "PROPERTY_MSG_BAD_DATE_FOR_INTERVAL";
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.impl.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    private static final String PROPERTY_MSG_INVALID_INTERVENTION_TYPE = "PROPERTY_MSG_INVALID_INTERVENTION_TYPE";
    private static final String REGEX_INTEGER = "^[0-9]+$";
    private static final String REGEX_DATE = "(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\\d\\d$";
    private final List<IInterventionForNumDAO> interventionForNumDAOs = new LinkedList();
    private IInterventionDAO interventionDAO;
    private IListeItineraireDAO listeItineraireDAO;

    /**
     * Instantiates a new abstract process record biomasse.
     */
    public AbstractProcessRecordBiomasse() {
        super();
    }

    private IInterventionForNumDAO testTypeIntervention(InterventionType interventionType) {
        for (IInterventionForNumDAO interventionDAO : interventionForNumDAOs) {
            if (interventionDAO.hasType(interventionType)) {
                return interventionDAO;
            }
        }
        return null;
    }

    /**
     * Builds the new line.
     *
     * @param version                   the version {@link VersionFile}
     * @param lines                     the lines
     * @param errorsReport              the errors report {@link ErrorsReport}
     * @param requestPropertiesBiomasse
     * @throws PersistenceException
     * @link(IRequestPropertiesBiomasse) the request properties biomasse
     * @link(IRequestPropertiesBiomasse) the request properties biomasse
     */
    protected abstract void buildNewLines(VersionFile version,
                                          List<? extends AbstractLineRecord> lines, ErrorsReport errorsReport,
                                          IRequestPropertiesBiomasse requestPropertiesBiomasse) throws PersistenceException;

    /**
     * @return
     */
    public IListeVegetationDAO getListeVegetationDAO() {
        return this.listeACBBDAO;
    }

    /**
     * @param listeACBBDAO
     */
    public void setListeVegetationDAO(IListeVegetationDAO listeACBBDAO) {
        super.setListeACBBDAO(listeACBBDAO);
    }

    /**
     * @param colonneEntry
     * @return
     * @throws PersistenceException
     */
    protected Optional<VariableACBB> getVariable(final Entry<Integer, Column> colonneEntry)
            throws PersistenceException {
        if (Strings.isNullOrEmpty(colonneEntry.getValue().getRefVariableName())) {
            return this.variableDAO
                    .getByAffichage(colonneEntry.getValue().getName())
                    .map(v -> (VariableACBB) v);
        } else {
            return this.variableDAO.getByCode(Utils
                    .createCodeFromString(colonneEntry.getValue().getRefVariableName()))
                    .map(v -> (VariableACBB) v);
        }
    }

    /**
     * @param intervalDate
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     * @return
     */
    protected int readDateAndSetVersionTraitement(final IntervalDate intervalDate, final AbstractLineRecord lineRecord,
                                                  final ErrorsReport errorsReport, final long lineCount, int index,
                                                  final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                                                  final IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        final LocalDate measureDate = this.getDate(errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB);
        if (!intervalDate.isInto(measureDate.atStartOfDay())) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_DATE_FOR_INTERVAL),
                    lineCount,
                    index,
                    DateUtil.getUTCDateTextFromLocalDateTime(measureDate, DateUtil.DD_MM_YYYY),
                    intervalDate.getBeginDateToString(),
                    intervalDate.getEndDateToString()));
            return index + 1;
        }
        lineRecord.setDate(measureDate);
        if (lineRecord.getDate() != null) {
            this.updateVersionDeTraitement(lineRecord, errorsReport, lineCount, index,
                    datasetDescriptorACBB, requestPropertiesBiomasse);
        }
        return index + 1;
    }

    /**
     * Gets the generic columns.
     *
     * @param errorsReport              the errors report {@link ErrorsReport}
     * @param lineCount                 the line count long
     * @param cleanerValues             the cleaner values {@link CleanerValues}
     * @param lineRecord                the line record {@link AbstractLineRecord}
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     * @return the generic columns
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(IRequestPropertiesBiomasse) the request properties biomasse
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(IRequestPropertiesBiomasse) the request properties biomasse
     */
    protected int readGenericColumns(final ErrorsReport errorsReport, final long lineCount,
                                     final CleanerValues cleanerValues, final AbstractLineRecord lineRecord,
                                     final DatasetDescriptorACBB datasetDescriptorACBB,
                                     final IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        int index = 0;
        index = this.readParcelle(lineRecord, errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB, requestPropertiesBiomasse);
        if (lineRecord.getParcelle() == null) {
            return index + 7;
        }
        index = this.readObservation(errorsReport, cleanerValues, lineRecord, index);
        index = this.readRotationAnneePeriode(cleanerValues, errorsReport, lineRecord, lineCount,
                index, datasetDescriptorACBB);
        index = this.readIntervention(cleanerValues, errorsReport, lineRecord, lineCount, index,
                datasetDescriptorACBB, requestPropertiesBiomasse);
        index = this.readSerie(cleanerValues, errorsReport, lineRecord, index);
        return index;
    }

    /**
     * @param cleanerValues
     * @param errorsReport
     * @param line
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     * @return
     */
    protected int readIntervention(CleanerValues cleanerValues, ErrorsReport errorsReport,
                                   AbstractLineRecord line, long lineCount, int index,
                                   DatasetDescriptorACBB datasetDescriptorACBB,
                                   IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        int localIndex = index;
        localIndex = this.readTypeIntervention(line, errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB, requestPropertiesBiomasse);
        String interventionDateOrNum = null;
        try {
            interventionDateOrNum = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        localIndex++;
        if (line.getTypeIntervention() == null) {
            if (!Strings.isNullOrEmpty(interventionDateOrNum)) {
                errorsReport.addInfoMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        AbstractProcessRecordBiomasse.BUNDLE_NAME,
                        AbstractProcessRecordBiomasse.PROPERTY_MSG_IGNORING_TYPE_INTERVENTION),
                        lineCount, index + 1, datasetDescriptorACBB.getColumnName(index + 1),
                        datasetDescriptorACBB.getColumnName(index)));
            }
            return localIndex;
        }
        if (interventionDateOrNum.matches(AbstractProcessRecordBiomasse.REGEX_INTEGER)) {
            this.readInterventionForNum(interventionDateOrNum, errorsReport, line, lineCount,
                    localIndex, datasetDescriptorACBB, requestPropertiesBiomasse);
        } else if (interventionDateOrNum.matches(AbstractProcessRecordBiomasse.REGEX_DATE)) {
            this.readInterventionForDate(interventionDateOrNum, errorsReport, line, lineCount,
                    localIndex, datasetDescriptorACBB, requestPropertiesBiomasse);
        } else {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_NUM_OR_DATE), lineCount,
                    localIndex, datasetDescriptorACBB.getColumnName(localIndex - 1), DateUtil.DD_MM_YYYY));
        }
        return localIndex;
    }

    /**
     * @param interventionString
     * @param errorsReport
     * @param line
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     */
    protected void readInterventionForDate(String interventionString, ErrorsReport errorsReport,
                                           AbstractLineRecord line, long lineCount, int index,
                                           DatasetDescriptorACBB datasetDescriptorACBB,
                                           IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        InterventionType interventionType = line.getTypeIntervention();
        LocalDate date = null;
        try {
            date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, interventionString);
        } catch (DateTimeException ex) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_INTERVENTION_DATE), lineCount,
                    index, DateUtil.DD_MM_YYYY));
            line.setIntervention(null);
            return;
        }
        AbstractIntervention intervention = (AbstractIntervention) this.interventionDAO.getinterventionForDateAndType(
                line.getParcelle(),
                date,
                line.getAnneeNumber(),
                interventionType.getValeur())
                .orElse(null);
        if (intervention == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_INTERVENTION_FOR_DATE),
                    lineCount, index, line.getTypeIntervention().getValeur(), line.getParcelle()
                            .getName(), interventionString));
            line.setIntervention(null);

        } else {
            line.setIntervention(intervention);
        }
    }

    /**
     * @param interventionString
     * @param errorsReport
     * @param line
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     */
    protected void readInterventionForNum(String interventionString, ErrorsReport errorsReport,
                                          AbstractLineRecord line, long lineCount, int index,
                                          DatasetDescriptorACBB datasetDescriptorACBB,
                                          IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        InterventionType interventionType = line.getTypeIntervention();
        int num = Integer.parseInt(interventionString);
        if (line.getAnneeNumber() == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_MISSING_DATE),
                    lineCount, index, interventionString));
            line.setIntervention(null);
            return;
        }
        IInterventionForNumDAO interventionForNumDAO = testTypeIntervention(interventionType);
        if (interventionForNumDAO == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM_BAD_TYPE),
                    lineCount, index, interventionType.getValeur()));
            line.setIntervention(null);
            return;

        }
        AbstractIntervention intervention = (AbstractIntervention) interventionForNumDAO.getinterventionForNumYearAndType(
                line.getParcelle(), num, line.getRotationNumber(), line.getAnneeNumber(), interventionType.getValeur())
                .orElse(null);
        if (intervention == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_BAD_INTERVENTION_FOR_NUM),
                    lineCount, index, line.getTypeIntervention().getValeur(), line.getParcelle()
                            .getName(), line.getAnneeNumber(), interventionString));
            line.setIntervention(null);

        } else {
            line.setIntervention(intervention);
        }
    }

    @Override
    protected Optional<VariableACBB> readVariableFromColumn(Column column) {
        return this.variableDAO.getByAffichage(column.getName().replaceAll("_.*", ""))
                .map(v -> (VariableACBB) v);
    }

    /**
     * Gets the observation.
     *
     * @param cleanerValues
     * @param line
     * @param index
     * @return the observation {@link CleanerValues} the cleaner values
     * @link(CleanerValues) the cleaner values
     */
    protected int readObservation(final ErrorsReport errorReport, CleanerValues cleanerValues, AbstractLineRecord line,
                                  int index) {
        try {
            line.setObservation(cleanerValues.nextToken());
        } catch (EndOfCSVLine ex) {
            errorReport.addErrorMessage(ex.setMessage(line.originalLineNumber, index).getMessage());
        }
        return index + 1;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestProperties
     * @return
     */
    protected int readParcelle(final AbstractLineRecord lineRecord,
                               final ErrorsReport errorsReport, final long lineCount, final int index,
                               final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                               final IRequestPropertiesBiomasse requestProperties) {
        int returnIndex = index;
        Parcelle parcelle = this.readDbParcelle(errorsReport, lineCount, returnIndex++,
                cleanerValues, datasetDescriptorACBB, requestProperties);
        lineRecord.setParcelle(parcelle);
        return returnIndex;
    }

    /**
     * @param cleanerValues
     * @param errorsReport
     * @param lineRecord
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @return
     */
    protected int readRotationAnneePeriode(final CleanerValues cleanerValues,
                                           final ErrorsReport errorsReport, final AbstractLineRecord lineRecord,
                                           final long lineCount, int index, final DatasetDescriptorACBB datasetDescriptorACBB) {
        int localIndex = index;
        lineRecord.setRotationNumber(this.getInt(errorsReport, lineCount, localIndex++,
                cleanerValues, datasetDescriptorACBB));
        lineRecord.setAnneeNumber(this.getInt(errorsReport, lineCount, localIndex++, cleanerValues,
                datasetDescriptorACBB));
        lineRecord.setPeriodeNumber(this.getInt(errorsReport, lineCount, localIndex++,
                cleanerValues, datasetDescriptorACBB));
        return localIndex;
    }

    /**
     * @param cleanerValues
     * @param lineRecord
     * @param index
     * @return
     */
    protected int readSerie(final CleanerValues cleanerValues, ErrorsReport errorsReport, final AbstractLineRecord lineRecord, int index) {
        int localIndex = index + 1;
        try {
            lineRecord.setSerie(cleanerValues.nextToken());
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineRecord.originalLineNumber, index).getMessage());
        }
        return localIndex;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     * @return
     */
    protected int readTypeIntervention(AbstractLineRecord lineRecord, ErrorsReport errorsReport,
                                       long lineCount, int index, CleanerValues cleanerValues,
                                       DatasetDescriptorACBB datasetDescriptorACBB,
                                       IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        int localIndex = index + 1;
        String interventionTypeName = null;
        try {
            interventionTypeName = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(interventionTypeName)) {
            return localIndex;
        }
        InterventionType interventionType = (InterventionType) this.listeItineraireDAO.getByNKey(
                InterventionType.INTERVENTION_TYPE, interventionTypeName).orElse(null);
        if (interventionType == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_INVALID_INTERVENTION_TYPE),
                    lineCount, index, interventionTypeName));
        } else {
            lineRecord.setTypeIntervention(interventionType);
        }
        return localIndex;
    }

    /**
     * @param interventionDAO
     */
    public void setInterventionDAO(IInterventionDAO interventionDAO) {
        this.interventionDAO = interventionDAO;
    }

    /**
     * @param listeItineraireDAO
     */
    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @param requestPropertiesBiomasse
     */
    protected void updateVersionDeTraitement(AbstractLineRecord lineRecord,
                                             ErrorsReport errorsReport, long lineCount, int index,
                                             DatasetDescriptorACBB datasetDescriptorACBB,
                                             IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        SuiviParcelle suiviParcelle;
        LocalDate localeDate = lineRecord.getDate();
        suiviParcelle = this.suiviParcelleDAO.retrieveSuiviParcelle(lineRecord.getParcelle(), localeDate).orElse(null);
        if (suiviParcelle == null) {
            String localizedParcelleName = this.localizationManager.newProperties(
                    Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getName(), lineRecord.getParcelle().getName());
            String localizedSiteName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getSite().getName(),
                    lineRecord.getParcelle().getSite().getName());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_MISSING_PLOT_TRACKING),
                    DateUtil.getUTCDateTextFromLocalDateTime(localeDate, DateUtil.DD_MM_YYYY),
                    localizedParcelleName,
                    localizedSiteName));
            return;
        }
        lineRecord.setSuiviParcelle(suiviParcelle);
        lineRecord.setTraitementProgramme(suiviParcelle.getTraitement());
        VersionDeTraitement versionDeTraitement = this.versionDeTraitementDAO
                .retrieveVersiontraitement(lineRecord.getTraitementProgramme(), localeDate).orElse(null);
        if (versionDeTraitement == null) {
            String localizedTreatmentAffichage = this.localizationManager.newProperties(
                    TraitementProgramme.NAME_ENTITY_JPA,
                    TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE).getProperty(
                    lineRecord.getTraitementProgramme().getAffichage(),
                    lineRecord.getTraitementProgramme().getAffichage());
            String localizedSiteName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getSite().getName(),
                    lineRecord.getParcelle().getSite().getName());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordBiomasse.BUNDLE_NAME,
                    AbstractProcessRecordBiomasse.PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT),
                    DateUtil.getUTCDateTextFromLocalDateTime(localeDate, DateUtil.DD_MM_YYYY),
                    lineRecord.getTraitementProgramme().getNom(),
                    localizedTreatmentAffichage,
                    localizedSiteName));
            return;
        }
        lineRecord.setVersionDeTraitement(versionDeTraitement);
        lineRecord.setDateDebuttraitementSurParcelle(suiviParcelle.getDateDebutTraitement());
    }

    /**
     * @param cleanerValues
     * @param line
     * @param errorsReport
     * @param lineCount
     * @param index
     * @return
     */
    protected int readNatureCouvert(CleanerValues cleanerValues, AbstractLineRecord line, ErrorsReport errorsReport, long lineCount, int index) {
        String natureCouvert = null;
        try {
            natureCouvert = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        line.setNatureCouvert(natureCouvert);
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readMediane(ErrorsReport errorsReport, long lineCount, int index, final CleanerValues cleanerValues, final ACBBVariableValue<IValeurQualitative> variableValue) {
        String valueString = null;
        try {
            valueString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(valueString)) {
            valueString = org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
        }
        Float value = null;
        try {
            value = Float.parseFloat(valueString);
            variableValue.setMediane(value);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_BIOMASSE_MEDIANE), lineCount, index + 1,
                    valueString));
        }
        return index + 1;
    }

    /**
     * @param paturageDAO
     */
    public void setPaturageDAO(IPaturageDAO paturageDAO) {
        interventionForNumDAOs.add(paturageDAO);
    }

    /**
     * @param recFauDAO
     */
    public void setRecFauDAO(IRecFauDAO recFauDAO) {
        interventionForNumDAOs.add(recFauDAO);
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readMeasureNumber(final ErrorsReport errorsReport, final long lineCount,
                                    int index, final CleanerValues cleanerValues,
                                    final AbstractVariableValueBiomasse variableValue) {
        String measureNumberString = null;
        try {
            measureNumberString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(measureNumberString)) {
            measureNumberString = "1";
        }
        try {
            int measureNumber = Integer.parseInt(measureNumberString);
            if (measureNumber < 0) {
                throw new NumberFormatException("negative number");
            }
            variableValue.setMeasureNumber(measureNumber);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_MEASURE_NUMBER), lineCount,
                    index + 1, measureNumberString));
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readQualityIndex(final ErrorsReport errorsReport, final long lineCount,
                                   int index, final CleanerValues cleanerValues,
                                   final AbstractVariableValueBiomasse variableValue) {
        String qualityIndexString = null;
        try {
            qualityIndexString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            if (Strings.isNullOrEmpty(qualityIndexString)) {
                qualityIndexString = org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
            }
            int qualityClass = Integer.parseInt(qualityIndexString);
            if (qualityClass != org.inra.ecoinfo.acbb.utils.ACBBUtils.CST_INVALID_EMPTY_MEASURE && qualityClass < 0) {
                throw new NumberFormatException("negative number");
            }
            variableValue.setQualityClass(qualityClass);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_QUALITY_INDEX), lineCount,
                    index + 1, qualityIndexString));
        }
        return index + 1;
    }

    /**
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param variableValue
     * @return
     */
    protected int readStandardDeviation(final ErrorsReport errorsReport, final long lineCount,
                                        int index, final CleanerValues cleanerValues,
                                        final AbstractVariableValueBiomasse variableValue) {
        String standardDeviationString = null;
        try {
            standardDeviationString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(standardDeviationString)) {
            standardDeviationString = org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_EMPTY_MEASURE;
        }
        try {
            float standardDeviation = Float.parseFloat(standardDeviationString);
            variableValue.setStandardDeviation(standardDeviation);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_STANDARD_DEVIATION), lineCount,
                    index + 1, standardDeviationString));
        }
        return index + 1;
    }

    /**
     * @param cleanerValues
     * @param variableValue
     * @param index
     * @return
     */
    protected int readValue(final CleanerValues cleanerValues,
                            final AbstractVariableValueBiomasse variableValue, ErrorsReport errorsReport, long lineCount, int index) {
        String value = null;
        try {
            value = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        variableValue.setValue(value);
        return index + 1;
    }
}
