package org.inra.ecoinfo.acbb.dataset.itk.jpa;

import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @param <T>
 * @author ptcherniati
 */
public class JPAInterventionDAO<T extends AbstractIntervention> extends AbstractJPADAO<T> implements
        IInterventionDAO<T> {

    /**
     *
     */
    public JPAInterventionDAO() {
        super();
    }

    /**
     * @param interventionToDelete
     */
    @Override
    public void deleteAll(List<T> interventionToDelete) {
        if (CollectionUtils.isEmpty(interventionToDelete)) {
            return;
        }

        CriteriaDelete<AbstractIntervention> delete = builder.createCriteriaDelete(AbstractIntervention.class);
        Root<AbstractIntervention> intervention = delete.from(AbstractIntervention.class);
        delete.where(intervention.in(interventionToDelete));
        delete(delete);
    }

    /**
     * @param parcelle
     * @param localDate
     * @param anneeNumber
     * @param type
     * @return
     */
    @Override
    public Optional<AbstractIntervention> getinterventionForDateAndType(Parcelle parcelle, LocalDate localDate,
                                                                        Integer anneeNumber, String type) {

        CriteriaQuery<AbstractIntervention> query = builder.createQuery(AbstractIntervention.class);
        Root<AbstractIntervention> intervention = query.from(AbstractIntervention.class);
        query.where(
                builder.equal(intervention.get(AbstractIntervention_.type), type),
                builder.equal(intervention.get(AbstractIntervention_.date), localDate),
                builder.equal(intervention.join(AbstractIntervention_.suiviParcelle).join(SuiviParcelle_.parcelle), parcelle)
        );
        query.select(intervention);
        return getOptional(query);
    }

    /**
     * @param version
     * @return
     */
    @Override
    public List<T> getInterventionForVersion(VersionFile version) {

        CriteriaQuery<T> query = builder.createQuery((Class<T>) AbstractIntervention.class);
        Root<T> intervention = query.from((Class<T>) AbstractIntervention.class);
        query.where(
                builder.equal(intervention.get(AbstractIntervention_.version), version)
        );
        query.select(intervention);
        return getResultList(query);
    }

    /**
     * @param version
     * @param date
     * @return
     */
    @Override
    public Optional<T> getByNKey(final VersionFile version, final LocalDate date) {

        CriteriaQuery<T> query = builder.createQuery(getInterventionClass());
        Root<T> t = query.from(getInterventionClass());
        query.where(
                builder.equal(t.get(AbstractIntervention_.version), version),
                builder.equal(t.get(AbstractIntervention_.date), date)
        );
        query.select(t);
        return getOptional(query);
    }

    /**
     * @return
     */
    protected Class<T> getInterventionClass() {
        return null;
    }
}
