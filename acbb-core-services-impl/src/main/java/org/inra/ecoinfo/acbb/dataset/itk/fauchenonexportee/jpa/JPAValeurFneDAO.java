/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.IValeurFneDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne;

/**
 * The Class JPAValeurSemisDAO.
 */
public class JPAValeurFneDAO extends AbstractJPADAO<ValeurFne> implements IValeurFneDAO {

}
