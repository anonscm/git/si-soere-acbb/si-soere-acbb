/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.modalite;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.variablededescription.IVariableDeDescriptionDAO;
import org.inra.ecoinfo.acbb.refdata.variablededescription.VariableDescription;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<Modalite> {

    /**
     * The LOGGER.
     */
    static final protected Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.modalite.messages";
    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_VARIABLE_DE_FORCAGE_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_VARIABLE_DE_FORCAGE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_VARIABLE_DE_FORCAGE_NOT_FOUND_IN_DB";
    static final String PROPERTY_MSG_ERROR_INVALID_COVER_IN_NAME = "PROPERTY_MSG_ERROR_INVALID_COVER_IN_NAME";
    static final String PROPERTY_MSG_MISSING_COVERS_IN_NAME = "PROPERTY_MSG_MISSING_COVERs_IN_NAME";
    /**
     * The modalite dao.
     */
    @Transient
    protected IModaliteDAO modaliteDAO;

    /**
     * The variable de description dao.
     */
    @Transient
    protected IVariableDeDescriptionDAO variableDeDescriptionDAO;

    /**
     * The IListeItineraireDAO.
     */
    @Transient
    protected IListeItineraireDAO listeItineraireDAO;

    /**
     * The variables de forcage possibles @link(String[]).
     */
    String[] variablesDeForcagePossibles;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * The properties commentaire en @link(Properties).
     */
    Properties propertiesCommentaireEN;

    /**
     * Instantiates a new recorder.
     */
    public Recorder() {
        super();
    }

    /**
     * Creates the or update modalite.
     *
     * @param codeVariabl
     * @param codeModalite
     * @param nom
     * @param description
     * @param finalDbModalite
     * @param dbVariable
     * @throws PersistenceException the persistence exception
     * @link(String) the code variabl
     * @link(String) the code modalite
     * @link(String) the nom
     * @link(String) the description
     * @link(Modalite) the db modalite
     * @link(VariableDescription) the db variable
     * @link(String) the code variabl
     * @link(String) the code modalite
     * @link(String) the nom
     * @link(String) the description
     * @link(Modalite) the db modalite
     * @link(VariableDescription) the db variable
     */
    void createOrUpdateModalite(final String codeVariabl, final String codeModalite,
                                final String nom, final String description, final Modalite dbModalite,
                                final VariableDescription dbVariable) throws PersistenceException {
        Modalite finalDbModalite = dbModalite;
        if (finalDbModalite == null) {
            final Modalite modalite = new Modalite(nom, codeModalite, description);
            dbVariable.getModalites().add(modalite);
            modalite.setVariableDescription(dbVariable);
            this.modaliteDAO.saveOrUpdate(modalite);
            finalDbModalite = modalite;
        } else {
            finalDbModalite.setNom(nom);
            finalDbModalite.setDescription(description);
            if (!dbVariable.getModalites().contains(finalDbModalite)) {
                dbVariable.getModalites().add(finalDbModalite);
            }
            finalDbModalite.setVariableDescription(dbVariable);
            this.modaliteDAO.saveOrUpdate(finalDbModalite);
        }
    }

    private void createOrUpdateRotation(String codeModalite, String nom, String description,
                                        Modalite dbModalite, VariableDescription dbVariable, ErrorsReport errorsReport)
            throws PersistenceException {
        assert null == dbModalite || dbModalite instanceof Rotation : "Modalite must be instance of rotation";
        Modalite finalDbModalite = dbModalite;
        Rotation rotation;
        if (finalDbModalite == null) {
            rotation = new Rotation(nom, codeModalite, description);
            dbVariable.getModalites().add(rotation);
            rotation.setVariableDescription(dbVariable);
            this.updateCouvertVegetal(rotation, errorsReport);
            this.modaliteDAO.saveOrUpdate(rotation);
        } else {
            rotation = (Rotation) finalDbModalite;
            finalDbModalite.setNom(nom);
            finalDbModalite.setDescription(description);
            if (!dbVariable.getModalites().contains(finalDbModalite)) {
                dbVariable.getModalites().add(finalDbModalite);
            }
            finalDbModalite.setVariableDescription(dbVariable);
            this.updateCouvertVegetal(rotation, errorsReport);
            this.modaliteDAO.saveOrUpdate(finalDbModalite);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeVariable = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeModalite = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.modaliteDAO.remove(this.modaliteDAO.getByNKey(codeVariable, codeModalite)
                        .orElseThrow(() -> new BusinessException("bad modality")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Modalite> getAllElements() {
        List<Modalite> all = this.modaliteDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the names variables possibles.
     *
     * @return the names variables possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesVariablesPossibles() throws PersistenceException {
        final List<VariableDescription> variables = this.variableDeDescriptionDAO
                .getAll(VariableDescription.class);
        final String[] namesVariablesPossibles = new String[variables.size()];
        int index = 0;
        for (final VariableDescription variableDescription : variables) {
            namesVariablesPossibles[index++] = variableDescription.getNom();
        }
        return namesVariablesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param modalite
     * @return the new line model grid metadata
     * @link(Modalite) the modalite
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Modalite modalite) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        modalite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : modalite
                                .getVariableDescription().getNom(),
                        this.variablesDeForcagePossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        modalite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : modalite
                                .getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        modalite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : modalite
                                .getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        modalite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.getProperty(modalite.getNom(),
                                modalite.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        modalite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : modalite
                                .getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        modalite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesCommentaireEN.getProperty(
                                modalite.getDescription(), modalite.getDescription()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Modalite> initModelGridMetadata() {
        try {
            this.variablesDeForcagePossibles = this.getNamesVariablesPossibles();
            this.propertiesCommentaireEN = this.localizationManager.newProperties(
                    Modalite.NAME_ENTITY_JPA, Modalite.ATTRIBUTE_JPA_DESCRIPTION, Locale.ENGLISH);
            this.propertiesNomEN = this.localizationManager.newProperties(Modalite.NAME_ENTITY_JPA,
                    Modalite.ATTRIBUTE_JPA_NOM, Locale.ENGLISH);

        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist modalite.
     *
     * @param errorsReport
     * @param codeVariable
     * @param codeModalite
     * @param nom
     * @param description
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code variable
     * @link(String) the code modalite
     * @link(String) the nom
     * @link(String) the description
     * @link(ErrorsReport) the errors report
     * @link(String) the code variable
     * @link(String) the code modalite
     * @link(String) the nom
     * @link(String) the description
     */
    void persistModalite(final ErrorsReport errorsReport, final String codeVariable,
                         final String codeModalite, final String nom, final String description)
            throws PersistenceException {
        final VariableDescription dbVariable = this.retrieveDBVariable(errorsReport, codeVariable);
        if (dbVariable == null) {
            return;
        }
        final Modalite dbModalite = this.modaliteDAO.getByNKey(codeVariable, codeModalite).orElse(null);
        if ("rotation".equals(dbVariable.getCode())) {
            this.createOrUpdateRotation(codeModalite, nom, description, dbModalite, dbVariable,
                    errorsReport);
        } else {
            this.createOrUpdateModalite(codeVariable, codeModalite, nom, description, dbModalite,
                    dbVariable);
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Modalite.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String codeVariable = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeModalite = tokenizerValues.nextToken();
                final String nomModalite = tokenizerValues.nextToken();
                final String descriptionModalite = tokenizerValues.nextToken();
                this.persistModalite(errorsReport, codeVariable, codeModalite, nomModalite,
                        descriptionModalite);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db variable.
     *
     * @param errorsReport
     * @param codeVariable
     * @return the variable description
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code variable
     * @link(ErrorsReport) the errors report
     * @link(String) the code variable
     */
    VariableDescription retrieveDBVariable(final ErrorsReport errorsReport,
                                           final String codeVariable) throws PersistenceException {
        final VariableDescription dbVariable = this.variableDeDescriptionDAO
                .getByCode(codeVariable).orElse(null);
        if (dbVariable == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_VARIABLE_DE_FORCAGE_NOT_FOUND_IN_DB),
                    codeVariable));
        }
        return dbVariable;
    }

    /**
     * Sets the IListeItineraireDAO.
     *
     * @param listeItineraireDAO the listeItineraireDAO to set
     */
    public final void setListeItineraireDAO(final IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    /**
     * Sets the modalite dao.
     *
     * @param modaliteDAO the new modalite dao
     */
    public final void setModaliteDAO(final IModaliteDAO modaliteDAO) {
        this.modaliteDAO = modaliteDAO;
    }

    /**
     * Sets the variable de description dao.
     *
     * @param variableDeDescriptionDAO the new variable de description dao
     */
    public final void setVariableDeDescriptionDAO(
            final IVariableDeDescriptionDAO variableDeDescriptionDAO) {
        this.variableDeDescriptionDAO = variableDeDescriptionDAO;
    }

    private void updateCouvertVegetal(Rotation rotation, ErrorsReport errorsReport) {
        String message = org.apache.commons.lang.StringUtils.EMPTY;
        final String[] couverts = rotation.getNom().split("\\s*,\\s*");
        if (couverts.length < 1) {
            message = RecorderACBB.getACBBMessageWithBundle(Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_MISSING_COVERS_IN_NAME);
            errorsReport.addErrorMessage(message);
            return;
        }
        rotation.getCouvertsVegetal().clear();
        rotation.setRepeatedCouvert(org.apache.commons.lang.StringUtils.EMPTY);
        rotation.setInfiniteRepeatedCouvert(Boolean.FALSE);
        int index = 1;
        for (final String couvertString : couverts) {
            try {
                final Matcher matches = Pattern.compile(Rotation.PATTERN_COUVERT).matcher(
                        couvertString);
                if (matches.matches()) {
                    final String coverName = matches.group(1);
                    final boolean isInfiniteRepeteadCover = !Strings
                            .isNullOrEmpty(matches.group(2));
                    final String nbCover = matches.group(3);
                    final ListeItineraire couvert = this.listeItineraireDAO.getByNKey(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL, Utils.createCodeFromString(coverName)).orElse(null);
                    if (couvert == null) {
                        message = String.format(RecorderACBB.getACBBMessageWithBundle(
                                Recorder.BUNDLE_SOURCE_PATH,
                                Recorder.PROPERTY_MSG_ERROR_INVALID_COVER_IN_NAME), coverName,
                                couvertString);
                        errorsReport.addErrorMessage(message);
                        continue;
                    }
                    final int repetitionOfCover = Strings.isNullOrEmpty(nbCover) ? 1 : Integer
                            .parseInt(nbCover);
                    index = rotation.updateRepeatedCouverts(isInfiniteRepeteadCover,
                            repetitionOfCover, (SemisCouvertVegetal) couvert, index);
                } else {
                    message = String.format(RecorderACBB.getACBBMessageWithBundle(
                            Recorder.BUNDLE_SOURCE_PATH,
                            Recorder.PROPERTY_MSG_ERROR_INVALID_COVER_IN_NAME), couvertString,
                            couvertString);
                    errorsReport.addErrorMessage(message);
                }
            } catch (final NumberFormatException e) {
                index++;
            }
        }
    }

}
