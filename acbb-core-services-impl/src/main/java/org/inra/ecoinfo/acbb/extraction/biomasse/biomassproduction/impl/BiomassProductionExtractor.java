package org.inra.ecoinfo.acbb.extraction.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.AbstractBiomasseExtractor;

/**
 * @author ptcherniati
 */
public class BiomassProductionExtractor extends AbstractBiomasseExtractor<BiomassProduction> {

}
