package org.inra.ecoinfo.acbb.extraction.itk.autreintervention.impl;

import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKExtractor;

/**
 * @author ptcherniati
 */
public class AIExtractor extends AbstractITKExtractor<AI> {

}
