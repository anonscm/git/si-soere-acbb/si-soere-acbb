/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationTravailDuSolDAO.
 */
public class JPAPublicationFertilisantDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {
    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeur(version);
        deleteBiomasse(version);
    }

    private void deleteValeur(VersionFile versionFile) {
        CriteriaDelete<ValeurFertilisant> deletevaleur = builder.createCriteriaDelete(ValeurFertilisant.class);
        Root<ValeurFertilisant> v = deletevaleur.from(ValeurFertilisant.class);
        Subquery<Fertilisant> subqueryFertilisant = deletevaleur.subquery(Fertilisant.class);
        Root<Fertilisant> intervention = subqueryFertilisant.from(Fertilisant.class);
        Path<VersionFile> version = intervention.get(AbstractIntervention_.version);
        subqueryFertilisant
                .select(intervention)
                .where(builder.equal(version, versionFile));
        deletevaleur
                .where(v.get(ValeurFertilisant_.fertilisant).in(subqueryFertilisant));
        delete(deletevaleur);
    }

    private void deleteBiomasse(VersionFile versionFile) {
        CriteriaDelete<Fertilisant> deleteBiomasse = builder.createCriteriaDelete(Fertilisant.class);
        Root<Fertilisant> intervention = deleteBiomasse.from(Fertilisant.class);
        Path<VersionFile> version = intervention.get(AbstractIntervention_.version);
        deleteBiomasse
                .where(builder.equal(version, versionFile));
        delete(deleteBiomasse);
    }

}
