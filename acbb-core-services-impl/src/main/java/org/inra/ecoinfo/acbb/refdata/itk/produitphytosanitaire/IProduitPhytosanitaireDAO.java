/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import org.inra.ecoinfo.IDAO;

import java.util.Optional;

/**
 * The Interface IProduitPhytosanitaireDAO.
 */
public interface IProduitPhytosanitaireDAO extends IDAO<ProduitPhytosanitaire> {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     */
    Optional<ProduitPhytosanitaire> getByCode(String code);
}
