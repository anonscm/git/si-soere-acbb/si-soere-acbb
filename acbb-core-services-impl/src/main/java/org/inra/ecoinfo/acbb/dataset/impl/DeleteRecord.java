package org.inra.ecoinfo.acbb.dataset.impl;

import org.inra.ecoinfo.acbb.dataset.IDeleteRecord;
import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;

/**
 * The Class DeleteRecord.
 * <p>
 * generic implementation of {@link IDeleteRecord} deleting files
 *
 * @author Tcherniatinsky Philippe
 * @see org.inra.ecoinfo.acbb.dataset.IDeleteRecord
 */
public class DeleteRecord implements IDeleteRecord {
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    static private final Logger LOGGER = LoggerFactory.getLogger(RecorderACBB.class);
    /**
     *
     */
    protected IDatasetConfiguration datasetConfiguration;
    /**
     * The version file dao @link(IVersionFileDAO). {@link IVersionFileDAO} The version file dao.
     */
    @Transient
    private IVersionFileDAO versionFileDAO;
    /**
     * The publication dao @link(ILocalPublicationDAO). {@link ILocalPublicationDAO} publication
     * dao.
     */
    @Transient
    private ILocalPublicationDAO publicationDAO;

    /**
     * Instantiates a new delete record.
     */
    public DeleteRecord() {
    }

    /**
     * Delete record.
     *
     * @param versionFile
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(VersionFile) the version file
     * @see org.inra.ecoinfo.acbb.dataset.IDeleteRecord#deleteRecord(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @see org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion((org
     * .inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecord(final VersionFile versionFile) throws BusinessException {
        VersionFile finalVersionFile = versionFile;
        try {
            finalVersionFile = this.getVersionFileDAO().merge(finalVersionFile);
            this.getPublicationDAO().removeVersion(finalVersionFile);
        } catch (final PersistenceException e) {
            this.getLogger().debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return
     */
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * @return
     */
    public ILocalPublicationDAO getPublicationDAO() {
        return this.publicationDAO;
    }

    /**
     * Sets the publication dao.
     *
     * @param publicationDAO the new publication dao @link(ILocalPublicationDAO) {@link ILocalPublicationDAO}
     *                       the new publication dao
     */
    public final void setPublicationDAO(final ILocalPublicationDAO publicationDAO) {
        this.publicationDAO = publicationDAO;
    }

    /**
     * @return
     */
    public IVersionFileDAO getVersionFileDAO() {
        return this.versionFileDAO;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao @link(IVersionFileDAO) {@link IVersionFileDAO} the new
     *                       version file dao
     */
    public final void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

}
