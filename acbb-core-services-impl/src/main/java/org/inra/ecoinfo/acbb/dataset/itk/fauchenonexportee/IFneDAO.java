/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;

/**
 * The Interface IFneDAO.
 */
public interface IFneDAO extends IInterventionDAO<Fne> {
}
