/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.fertilisant.impl;

import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertType;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class FertBuildOutputDisplay extends
        AbstractITKOutputBuilder<Fertilisant, ValeurFertilisant, Fertilisant> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.fertilisant.messages";
    static final String PATTERN_11_FIELD = "%s;%s;%s;%s;%s;%s(%s);%s;%s;%s;%s;%s;%s;%s";
    private static final Logger LOGGER = LoggerFactory.getLogger(FertBuildOutputDisplay.class
            .getName());

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public FertBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return FertBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedFertilisantVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedFertilisantVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream) {
        return new FertilisantOutputHelper(rawDataPrintStream,
                selectedFertilisantVariables);
    }

    class FertilisantOutputHelper extends OutputHelper {

        protected final Properties propertiesFertType = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, ValeurQualitative.ATTRIBUTE_JPA_VALUE);

        FertilisantOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedFertilisantVariables) {
            super(rawDataPrintStream,
                    selectedFertilisantVariables);
        }

        /**
         * @see org.inra.ecoinfo.acbb.extraction.pratiquesdegestion.impl.AbstractITKOutputBuilder.OutputHelper#printLineGeneric(org.inra.ecoinfo.acbb.dataset.itk.impl.IMesureITK)
         */
        @Override
        protected void printLineGeneric(Fertilisant mesure) {
            final SuiviParcelle suiviParcelle = mesure.getSequence().getSuiviParcelle();
            final Parcelle parcelle = suiviParcelle.getParcelle();
            int rang = mesure.getRang();
            FertType fertType = mesure.getFertType();
            TraitementProgramme treatment = suiviParcelle.getTraitement();
            String line;
            final String blocName = parcelle.getBloc() != null ? parcelle.getBloc().getBlocName()
                    : StringUtils.EMPTY;
            final String repetitionName = parcelle.getBloc() != null ? parcelle.getBloc()
                    .getRepetitionName() : StringUtils.EMPTY;
            line = String.format(FertBuildOutputDisplay.PATTERN_11_FIELD,
                    this.propertiesSiteName.getProperty(parcelle.getSite().getName(), parcelle.getSite().getName()),
                    this.propertiesParcelleName.getProperty(parcelle.getName(), parcelle.getName()),
                    blocName,
                    repetitionName,
                    DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDate(), DateUtil.DD_MM_YYYY),
                    this.propertiesTreatmentName.getProperty(treatment.getNom(), treatment.getNom()),
                    this.propertiesTreatmentAffichage.getProperty(treatment.getAffichage(), treatment.getAffichage()),
                    DateUtil.getUTCDateTextFromLocalDateTime(suiviParcelle.getDateDebutTraitement(), DateUtil.DD_MM_YYYY),
                    mesure.getSequence().getObservation(),
                    mesure.getSequence().getVersionTraitementRealiseeNumber(),
                    mesure.getSequence().getAnneeRealiseeNumber(),
                    mesure.getSequence().getPeriodeRealiseeNumber(),
                    rang,
                    this.propertiesFertType.getProperty(Utils.createCodeFromString(fertType.getValeur()), fertType.getValeur())
            );
            this.rawDataPrintStream.print(line);
        }
    }
}
