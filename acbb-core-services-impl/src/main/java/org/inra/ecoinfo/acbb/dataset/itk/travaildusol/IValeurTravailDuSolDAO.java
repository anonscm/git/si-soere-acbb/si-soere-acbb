/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.ValeurTravailDuSol;

/**
 * The Interface IValeurTravailDuSolDAO.
 */
public interface IValeurTravailDuSolDAO extends IDAO<ValeurTravailDuSol> {
}
