package org.inra.ecoinfo.acbb.dataset.itk.semis.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPASemisDAO extends JPAInterventionDAO<Semis> implements ISemisDAO {

    /**
     * @param version
     * @param parcelle
     * @param date
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO#getByNKey(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * int, java.time.LocalDateTime)
     */
    @Override
    public Optional<AbstractIntervention> getByNKey(final VersionFile version, final SuiviParcelle parcelle, final LocalDate date) {

        CriteriaQuery<AbstractIntervention> query = builder.createQuery(AbstractIntervention.class);
        Root<Semis> semis = query.from(Semis.class);
        query.where(
                builder.equal(semis.join(Semis_.version), version),
                builder.equal(semis.join(Semis_.suiviParcelle).join(SuiviParcelle_.parcelle), parcelle),
                builder.equal(semis.get(Semis_.date), date)
        );
        query.select(semis);
        return getOptional(query);
    }

    @Override
    protected Class<Semis> getInterventionClass() {
        return Semis.class;
    }
}
