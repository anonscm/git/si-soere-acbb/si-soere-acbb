/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.travaildusol.impl;

import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.TravailDuSol;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class WSolBuildOutputDisplay extends
        AbstractITKOutputBuilder<TravailDuSol, ValeurTravailDuSol, MesureTravailDuSol> {

    static final String PATTERN_2_FIELD = ";%d;%d";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.travaildusol.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(WSolBuildOutputDisplay.class
            .getName());

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public WSolBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return WSolBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedWsolVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream) {
        return new WSolOutputHelper(rawDataPrintStream,
                selectedWsolVariables);
    }

    class WSolOutputHelper extends OutputHelper {

        final SortedMap<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<Integer, SortedMap<Integer, MesureTravailDuSol>>>>>> sortedWsolValeurs = new TreeMap();
        protected Integer ordre;
        protected Integer intervention;

        WSolOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedWSolVariables) {
            super(rawDataPrintStream, selectedWSolVariables);
        }

        @Override
        public void addMesure(MesureTravailDuSol mesure) {
            final TravailDuSol sequence = mesure.getSequence();
            SiteACBB site = sequence.getSuiviParcelle().getParcelle().getSite();
            TraitementProgramme treatment = sequence.getSuiviParcelle().getTraitement();
            Parcelle parcelle = sequence.getSuiviParcelle().getParcelle();
            this.intervention = sequence.getRang();
            this.ordre = mesure.getNumero();
            if (!this.sortedWsolValeurs.containsKey(site)) {
                this.sortedWsolValeurs.put(site, new TreeMap());
            }
            if (!this.sortedWsolValeurs.get(site).containsKey(treatment)) {
                this.sortedWsolValeurs.get(site).put(treatment, new TreeMap());
            }
            if (!this.sortedWsolValeurs.get(site).get(treatment).containsKey(parcelle)) {
                this.sortedWsolValeurs.get(site).get(treatment).put(parcelle, new TreeMap());
            }
            if (!this.sortedWsolValeurs.get(site).get(treatment).get(parcelle).containsKey(sequence.getDate())) {
                this.sortedWsolValeurs.get(site).get(treatment).get(parcelle).put(sequence.getDate(), new TreeMap());
            }
            if (!this.sortedWsolValeurs.get(site).get(treatment).get(parcelle).get(sequence.getDate()).containsKey(this.intervention)) {
                this.sortedWsolValeurs.get(site).get(treatment).get(parcelle).get(sequence.getDate()).put(this.intervention, new TreeMap());
            }
            this.sortedWsolValeurs.get(site).get(treatment).get(parcelle).get(sequence.getDate()).get(this.intervention).put(this.ordre, mesure);
        }

        @Override
        public void buildBody(List<MesureTravailDuSol> mesures) {
            for (final MesureTravailDuSol valeur : mesures) {
                this.addMesure(valeur);
            }
            for (final Entry<SiteACBB, SortedMap<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<Integer, SortedMap<Integer, MesureTravailDuSol>>>>>> siteEntry : this.sortedWsolValeurs.entrySet()) {
                for (final Entry<TraitementProgramme, SortedMap<Parcelle, SortedMap<LocalDate, SortedMap<Integer, SortedMap<Integer, MesureTravailDuSol>>>>> treatmentEntry : siteEntry.getValue().entrySet()) {
                    for (final Entry<Parcelle, SortedMap<LocalDate, SortedMap<Integer, SortedMap<Integer, MesureTravailDuSol>>>> parcelleEntry : treatmentEntry.getValue().entrySet()) {
                        for (final Entry<LocalDate, SortedMap<Integer, SortedMap<Integer, MesureTravailDuSol>>> dateEntry : parcelleEntry.getValue().entrySet()) {
                            for (Entry<Integer, SortedMap<Integer, MesureTravailDuSol>> interventionEntry : dateEntry.getValue().entrySet()) {
                                for (Entry<Integer, MesureTravailDuSol> ordreEntry : interventionEntry
                                        .getValue().entrySet()) {
                                    this.addMesureLine(ordreEntry.getValue());
                                    this.rawDataPrintStream.println();
                                }
                            }
                        }
                    }
                }
            }
        }

        @Override
        protected void printLineGeneric(MesureTravailDuSol mesure) {
            super.printLineGeneric(mesure);
            String line = String.format(WSolBuildOutputDisplay.PATTERN_2_FIELD, mesure
                    .getTravailDuSol().getRang(), mesure.getNumero());
            this.rawDataPrintStream.print(line);
        }
    }
}
