/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.fauchenonexportee.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne_;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.fauchenonexportee.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
@SuppressWarnings("unchecked")
public class JPAFneExtractionDAO extends AbstractJPAITKDAO<Fne, Fne, ValeurFne> {

    /**
     * @return
     */
    @Override
    protected Class<Fne> getMesureITKClass() {
        return Fne.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurFne> getValeurITKClass() {
        return ValeurFne.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, Fne> getMesureAttribute() {
        return ValeurFne_.intervention;
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }
}
