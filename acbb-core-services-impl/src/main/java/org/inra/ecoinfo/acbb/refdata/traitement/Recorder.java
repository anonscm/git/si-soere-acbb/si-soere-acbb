/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.traitement;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<TraitementProgramme> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.traitement.messages";

    /**
     * The Constant PROPERTY_MSG_BAD_DATE_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_BAD_DATE_FORMAT = "PROPERTY_MSG_BAD_DATE_FORMAT";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * The site dao.
     */
    ISiteACBBDAO siteDAO;

    /**
     * The traitement dao.
     */
    ITraitementDAO traitementDAO;

    /**
     * The sites possibles @link(String[]).
     */
    String[] sitesPossibles;

    /**
     * The properties affichage en @link(Properties).
     */
    Properties propertiesAffichageEN;

    /**
     * The properties description en @link(Properties).
     */
    Properties propertiesDescriptionEN;

    /**
     * The properties duree en @link(Properties).
     */
    Properties propertiesDureeEN;

    /**
     * Builds the string date debut traitement.
     *
     * @param traitement
     * @return the string
     * @link(TraitementProgramme) the traitement
     * @link(TraitementProgramme) the traitement
     */
    String buildStringDateDebutTraitement(final TraitementProgramme traitement) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (traitement.getDateDebutTraitement() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(traitement.getDateDebutTraitement(), this.datasetDescriptor.getColumns().get(10).getFormatType());
        }
        return dateDebut;
    }

    /**
     * Builds the string date fin traitement.
     *
     * @param traitement
     * @return the string
     * @link(TraitementProgramme) the traitement
     * @link(TraitementProgramme) the traitement
     */
    String buildStringDateFinTraitement(final TraitementProgramme traitement) {
        String dateFin = org.apache.commons.lang.StringUtils.EMPTY;
        if (traitement.getDateFinTraitement() != null) {
            dateFin = DateUtil.getUTCDateTextFromLocalDateTime(traitement.getDateFinTraitement(), this.datasetDescriptor.getColumns().get(11).getFormatType());
        }
        return dateFin;
    }

    /**
     * Creates the or update traitement.
     *
     * @param nomTraitement
     * @param affichage
     * @param description
     * @param duree
     * @param dateDebut
     * @param dateFin
     * @param dbSite
     * @param dbTraitement
     * @throws PersistenceException the persistence exception
     * @link(String) the nom traitement
     * @link(String) the affichage
     * @link(String) the description
     * @link(String) the duree
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(String) the nom traitement
     * @link(String) the affichage
     * @link(String) the description
     * @link(String) the duree
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     */
    void createOrUpdateTraitement(final String nomTraitement, final String affichage,
                                  final String description, final String duree, final LocalDate dateDebut, final LocalDate dateFin,
                                  final SiteACBB dbSite, final TraitementProgramme dbTraitement)
            throws PersistenceException {
        if (dbTraitement == null) {
            final TraitementProgramme traitement = new TraitementProgramme(nomTraitement,
                    affichage, description, duree, dateDebut, dateFin);
            traitement.setSite(dbSite);
            this.traitementDAO.saveOrUpdate(traitement);
        } else {
            dbTraitement.setAffichage(affichage);
            dbTraitement.setDescription(description);
            dbTraitement.setDuree(duree);
            dbTraitement.setDateDebutTraitement(dateDebut);
            dbTraitement.setDateFinTraitement(dateFin);
            this.traitementDAO.saveOrUpdate(dbTraitement);
        }

    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeTraitement = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                this.traitementDAO.remove(this.traitementDAO.getByNKey(
                        this.siteDAO.getByPath(codeSite).orElseThrow(() -> new BusinessException("bad site")).getId(), codeTraitement)
                        .orElseThrow(() -> new PersistenceException("bad Treatment")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<TraitementProgramme> getAllElements() {
        List<TraitementProgramme> all = this.traitementDAO.getAll(TraitementProgramme.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the names sites possibles.
     *
     * @return the names sites possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesSitesPossibles() throws PersistenceException {
        final List<SiteACBB> sites = this.siteDAO.getAllSitesACBB();
        final String[] namesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (final Site site : sites) {
            namesSitesPossibles[index++] = site.getName();
        }
        return namesSitesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param traitement
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(TraitementProgramme) the traitement
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final TraitementProgramme traitement)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : traitement
                                .getSite().getName(), this.sitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : traitement
                                .getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomFR.get(traitement.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.get(traitement.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : traitement
                                .getAffichage(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesAffichageEN.get(traitement.getAffichage()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : traitement
                                .getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesDescriptionEN.get(traitement.getDescription()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : traitement
                                .getDuree(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesDureeEN.get(traitement.getDuree()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this
                                .buildStringDateDebutTraitement(traitement),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        traitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this
                                .buildStringDateFinTraitement(traitement),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<TraitementProgramme> initModelGridMetadata() {
        this.propertiesNomFR = this.localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_NAME, Locale.FRENCH);

        this.propertiesNomEN = this.localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_NAME, Locale.ENGLISH);
        this.propertiesAffichageEN = this.localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE, Locale.ENGLISH);
        this.propertiesDescriptionEN = this.localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_DESCRIPTION, Locale.ENGLISH);
        this.propertiesDureeEN = this.localizationManager.newProperties(
                TraitementProgramme.NAME_ENTITY_JPA, TraitementProgramme.ATTRIBUTE_JPA_DUREE, Locale.ENGLISH);
        try {
            this.sitesPossibles = this.getNamesSitesPossibles();
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist traitement.
     *
     * @param errorsReport
     * @param codeSite
     * @param nomTraitement
     * @param affichage
     * @param description
     * @param duree
     * @param dateDebut
     * @param dateFin
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the nom traitement
     * @link(String) the affichage
     * @link(String) the description
     * @link(String) the duree
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the nom traitement
     * @link(String) the affichage
     * @link(String) the description
     * @link(String) the duree
     * @link(Date) the date debut
     * @link(Date) the date fin
     */
    void persistTraitement(final ErrorsReport errorsReport, final String codeSite,
                           final String nomTraitement, final String affichage, final String description,
                           final String duree, final LocalDate dateDebut, final LocalDate dateFin)
            throws PersistenceException {
        final SiteACBB dbSite = this.retrieveDBSite(errorsReport, codeSite);
        if (dbSite != null) {
            final TraitementProgramme dbTraitement = this.traitementDAO.getByNKey(dbSite.getId(),
                    Utils.createCodeFromString(nomTraitement))
                    .orElse(null);
            this.createOrUpdateTraitement(nomTraitement, affichage, description, duree, dateDebut,
                    dateFin, dbSite, dbTraitement);
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        TraitementProgramme.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String nomTraitement = tokenizerValues.nextToken();
                final String affichage = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                final String duree = tokenizerValues.nextToken();
                final String dateDebutString = tokenizerValues.nextToken();
                final String dateFinString = tokenizerValues.nextToken();
                try {
                    final LocalDate dateDebut = dateDebutString != null && dateDebutString.length() > 0
                            ? DateUtil.readLocalDateFromText(this.datasetDescriptor.getColumns().get(10).getFormatType(), dateDebutString)
                            : null;
                    try {
                        final LocalDate dateFin = dateFinString != null && dateFinString.length() > 0
                                ? DateUtil.readLocalDateFromText(this.datasetDescriptor.getColumns().get(11).getFormatType(), dateFinString)
                                : null;
                        this.persistTraitement(errorsReport, codeSite, nomTraitement, affichage,
                                description, duree, dateDebut, dateFin);
                    } catch (final DateTimeException e) {
                        LOGGER.debug(e.getMessage(), e);
                        errorsReport.addErrorMessage(String.format(this.localizationManager
                                        .getMessage(Recorder.BUNDLE_SOURCE_PATH,
                                                Recorder.PROPERTY_MSG_BAD_DATE_FORMAT), dateFinString,
                                this.datasetDescriptor.getColumns().get(10).getFormatType()));
                    }
                } catch (final DateTimeException e) {
                    LOGGER.debug(e.getMessage(), e);
                    errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                            Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_DATE_FORMAT),
                            dateDebutString, this.datasetDescriptor.getColumns().get(11)
                                    .getFormatType()));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db site.
     *
     * @param errorsReport
     * @param codeSite
     * @return the site acbb
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     */
    SiteACBB retrieveDBSite(final ErrorsReport errorsReport, final String codeSite)
            throws PersistenceException {
        final SiteACBB dbSite = (SiteACBB) this.siteDAO.getByPath(codeSite).orElse(null);
        if (dbSite == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), codeSite));
        }
        return dbSite;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Sets the traitement dao.
     *
     * @param traitementDAO the new traitement dao
     */
    public final void setTraitementDAO(final ITraitementDAO traitementDAO) {
        this.traitementDAO = traitementDAO;
    }

}
