/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.methode.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode_;
import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * @param <T>
 * @author ptcherniati
 */
abstract public class AbstractJPAMethodeDAO<T extends AbstractMethode> extends AbstractJPADAO<T>
        implements IMethodeDAO<T> {

    /**
     * @return
     */
    @Override
    public List<T> getAll() {

        CriteriaQuery<T> query = builder.createQuery(getMethodClass());
        Root<T> method = query.from(getMethodClass());
        query.select(method);
        return getResultList(query);
    }

    /**
     * @param numberId
     * @return
     */
    @Override
    public Optional<T> getByNumberId(Long numberId) {
        return getMethodeByNumberId(numberId);
    }

    /**
     * @param numberId
     * @return
     */
    @Override
    public Optional<T> getMethodeByNumberId(Long numberId) {
        CriteriaQuery<T> query = builder.createQuery(getMethodClass());
        Root<T> method = query.from(getMethodClass());
        query.where(builder.equal(method.get(AbstractMethode_.numberId), numberId));
        query.select(method);
        return getOptional(query);
    }

    /**
     * @param number_id
     * @return
     */
    @Override
    public Optional<T> getByNKey(final Long number_id) {

        return getByNumberId(number_id);
    }

}
