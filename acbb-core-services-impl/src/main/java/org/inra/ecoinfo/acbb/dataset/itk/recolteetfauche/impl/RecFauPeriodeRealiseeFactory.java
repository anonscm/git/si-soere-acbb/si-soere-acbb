package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultPeriodeRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFauRepousse;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.Optional;

/**
 * A factory for creating SemisPeriodeRealisee objects.
 */
public class RecFauPeriodeRealiseeFactory extends DefaultPeriodeRealiseeFactory<RecFauLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl.messages";

    /**
     * The Constant PROPERTY_MSG_ERROR_EXISTING_PERIODE @link(String).
     */
    static final String PROPERTY_MSG_ERROR_EXISTING_PERIODE = "PROPERTY_MSG_ERROR_EXISTING_PERIODE";

    Optional<PeriodeRealisee> createNewPeriodeRealisee(final RecFauLineRecord line) {
        return Optional.of(new PeriodeRealisee(line.getPeriodeNumber(), line.getTempo().getAnneeRealisee()));
    }

    /**
     * @param line
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultPeriodeRealiseeFactory#getOrCreatePeriodeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport)
     */
    @Override
    public Optional<PeriodeRealisee> getOrCreatePeriodeRealisee(final RecFauLineRecord line, final ErrorsReport errorsReport) {
        if (line.getPeriodeNumber() < 1) {
            return Optional.empty();
        }
        Optional<PeriodeRealisee> periodeRealisee = this.periodeRealiseeDAO.getByNKey(line.getTempo()
                .getAnneeRealisee(), line.getPeriodeNumber());
        if (!periodeRealisee.isPresent()) {
            periodeRealisee = this.createNewPeriodeRealisee(line);
        }
        this.updatePeriodeRealisee(periodeRealisee, line);
        try {
            this.periodeRealiseeDAO.saveOrUpdate(periodeRealisee.orElseThrow(PersistenceException::new));
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return periodeRealisee;
    }

    Optional<PeriodeRealisee> updatePeriodeRealisee(final Optional<PeriodeRealisee> periodeRealisee, final RecFauLineRecord line) {
        if (periodeRealisee.isPresent()) {
            periodeRealisee.get().setDateDeDebut(line.getDate());
            for (ACBBVariableValue<ListeItineraire> variableValue : line.getVariablesValues()) {
                if (variableValue.getValeurQualitative() instanceof RecFauRepousse
                        && Boolean.TRUE.toString().equals(
                        variableValue.getValeurQualitative().getValeur())) {
                    periodeRealisee.get().setDateDeFin(periodeRealisee.get().getAnneeRealisee().getDateDeFin());
                    periodeRealisee.get().setCouvertVegetal(periodeRealisee.get().getAnneeRealisee()
                            .getCouvertVegetal());
                    break;
                }
            }
        }
        return periodeRealisee;
    }

}
