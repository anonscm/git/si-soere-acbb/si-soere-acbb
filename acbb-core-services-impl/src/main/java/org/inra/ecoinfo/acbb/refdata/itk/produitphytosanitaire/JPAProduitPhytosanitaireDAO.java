/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import org.inra.ecoinfo.AbstractJPADAO;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPAProduitPhytosanitaireDAO.
 */
public class JPAProduitPhytosanitaireDAO extends AbstractJPADAO<ProduitPhytosanitaire> implements
        IProduitPhytosanitaireDAO {

    /**
     * Gets the by code.
     *
     * @param code
     * @return the by code
     * @link(String) the code
     */
    @Override
    public Optional<ProduitPhytosanitaire> getByCode(final String code) {
        CriteriaQuery<ProduitPhytosanitaire> query = builder.createQuery(ProduitPhytosanitaire.class);
        Root<ProduitPhytosanitaire> phyt = query.from(ProduitPhytosanitaire.class);
        query.where(
                builder.equal(phyt.get(ProduitPhytosanitaire_.code), code)
        );
        query.select(phyt);
        return getOptional(query);
    }
}
