package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.impl.InfoReport;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The Class AbstractBuildHelper.
 *
 * @param <T> extends {@link AbstractLineRecord} the generic type
 */
public abstract class AbstractBuildHelper<T extends ILineRecord> {

    /**
     *
     */
    static protected final Logger LOGGER = LoggerFactory.getLogger(AbstractBuildHelper.class);
    private final Map<String, NumberFormat> numberformats = new HashMap();
    /**
     * The lines .
     */
    protected List<T> lines;
    /**
     * The errors report {@link ErrorsReport}.
     */
    protected ErrorsReport errorsReport;
    /**
     * The errors report {@link InfoReport}.
     */
    protected InfoReport infoReport = new InfoReport();
    /**
     * The version {@link VersionFile}.
     */
    protected VersionFile version;
    /**
     * The db datatype variable unites
     *
     * @{Map<String, DatatypeVariableUniteACBB>}.
     */
    protected Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites = new HashMap();
    /**
     * The request properties biomasse @link(IRequestPropertiesBiomasse).
     */
    @Transient
    protected IRequestPropertiesBiomasse requestPropertiesBiomasse;
    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    @Transient
    protected IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     *
     */
    protected Map<Variable, RealNode> dvus = new HashMap();
    /**
     * The datatype name @link(String).
     */
    String datatypeName;

    /**
     * Builds the.
     *
     * @throws PersistenceException
     */
    protected abstract void build() throws PersistenceException;

    /**
     * @return the requestPropertiesBiomasse
     */
    @Transient
    protected IRequestPropertiesBiomasse getRequestPropertiesBiomasse() {
        return this.requestPropertiesBiomasse;
    }

    /**
     *
     */
    public final void initNumberFormat() {
        for (String localization : Localization.getLocalisations()) {
            Locale locale = Locale.forLanguageTag(localization);
            numberformats.computeIfAbsent(localization, k -> NumberFormat.getInstance(locale));
        }
    }

    /**
     * Sets the datatype name.
     *
     * @param datatypeName the datatypeName to set
     */
    public final void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the datatypeVariableUniteACBBDAO to
     *                                     set
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    /**
     * Instantiates a new abstract build helper.
     *
     * @param version                   the version {@link VersionFile}
     * @param lines                     the lines {@link List}
     * @param errorsReport              the errors report {@link ErrorsReport}
     * @param requestPropertiesBiomasse
     * @link(IRequestPropertiesBiomasse) the request properties biomasse
     */
    public void update(final VersionFile version, final List<T> lines,
                       final ErrorsReport errorsReport,
                       final IRequestPropertiesBiomasse requestPropertiesBiomasse) {
        this.dbDatatypeVariableUnites = this.datatypeVariableUniteACBBDAO
                .getAllVariableTypeDonneesByDataTypeMapByVariableCode(this.datatypeName);
        this.version = version;
        this.lines = lines;
        this.errorsReport = errorsReport;
        this.requestPropertiesBiomasse = requestPropertiesBiomasse;
        this.dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(version.getDataset().getRealNode());
    }

    /**
     * @param dvu
     * @return
     */
    protected RealNode getRealNodeForSequenceAndVariable(DatatypeVariableUniteACBB dvu) {

        return dvus.get(dvu.getVariable());
    }

}
