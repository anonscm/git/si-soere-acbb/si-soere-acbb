package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertType;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.WsolTypeObjectif;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.WsolTypeOutil;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;

import java.util.List;

/**
 * The Class LineRecord for wsol.
 */
public class LineRecord extends AbstractLineRecord<LineRecord, ListeItineraire> {

    /**
     * The rang <int>.
     */
    int rang;
    private FertType fertType;

    /**
     * Instantiates a new line record.
     *
     * @param lineCount <long> the line count
     * @link(SuiviParcelle) the suivi parcelle {@link SuiviParcelle} the suivi parcelle
     */
    LineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * Copy.
     *
     * @param line
     * @link(AbstractLineRecord) the line
     * @link(AbstractLineRecord) the line
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.AbstractLineRecord
     * #copy(org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord)
     */
    @Override
    public void copy(final LineRecord line) {
        super.copy(line);
        this.rang = line.getRang();
    }

    /**
     * @return
     */
    public FertType getFertType() {
        return this.fertType;
    }

    /**
     * @param fertType
     */
    public void setFertType(FertType fertType) {
        this.fertType = fertType;
    }

    /**
     * Gets the rang.
     *
     * @return the rang
     */
    public int getRang() {
        return this.rang;
    }

    /**
     * Sets the rang.
     *
     * @param rang int the new rang
     */
    public final void setRang(final int rang) {
        this.rang = rang;
    }

    /**
     * Update.
     *
     * @param rang
     * @param fertType
     * @param variablesValues
     * @link(WsolTypeOutil)
     * @link(List<WsolTypeObjectif>) the objectifs2
     * @link(List<VariableValue>) the variables values {@link WsolTypeOutil} the typeoutil2
     * {@link List<WsolTypeObjectif>} the objectifs2 {@link List
     * <VariableValue>} the variables values
     */
    public void update(final int rang, FertType fertType,
                       final List<ACBBVariableValue<ListeItineraire>> variablesValues) {
        this.rang = rang;
        this.fertType = fertType;
        this.setVariablesValues(variablesValues);
    }

}
