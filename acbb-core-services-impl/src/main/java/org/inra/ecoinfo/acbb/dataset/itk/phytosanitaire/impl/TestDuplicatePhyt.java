/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class TestDuplicateWsol.
 *
 * implementation for Tillage of {@link ITestDuplicates}
 *
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux
 * files
 */
public class TestDuplicatePhyt extends AbstractTestDuplicate {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String ACBB_DATASET_WSOL_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.travaildusol.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    final SortedMap<String, Long> line;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicatePhyt() {
        this.line = new TreeMap();
    }

    /**
     *
     * @param parcelle
     * @param date
     * @param rang
     * @param lineNumber
     */
    protected void addLine(final String parcelle, final String date, final String rang,
                           final long lineNumber) {
        final String key = this.getKey(parcelle, date, rang);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicatePhyt.ACBB_DATASET_WSOL_BUNDLE_NAME,
                    ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle, date, rang,
                    this.line.get(key)));
        }

    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[6], lineNumber + 1);
    }
}
