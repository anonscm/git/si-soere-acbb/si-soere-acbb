/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.methode;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import java.util.List;
import java.util.Optional;

/**
 * @param <T>
 * @author koyao
 */
public interface IMethodeDAO<T extends AbstractMethode> extends IDAO<T> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<T> getAll();

    /**
     * @param numberId
     * @return
     */
    Optional<T> getByNumberId(Long numberId);

    /**
     * @return
     */
    Class<T> getMethodClass();

    /**
     * @param <U>
     * @param numberId
     * @return
     */
    <U extends AbstractMethode> Optional<U> getMethodeByNumberId(Long numberId);

    /**
     * Gets the by n key.
     *
     * @param number_id
     * @return the by n key
     */
    Optional<T> getByNKey(final Long number_id);
}
