package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.PrintStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.extraction.AbstractRequestReminder;
import org.inra.ecoinfo.filecomp.entity.FileComp;

/**
 * The Class MPSRequestReminderOutputBuilder.
 */
public class ITKRequestReminderOutputBuilder extends AbstractRequestReminder {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.itk.messages";
    private static final String PROPERTY_MSG_SELECTED_PATURAGE_VARIABLES = "PROPERTY_MSG_SELECTED_PATURAGE_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_WSOL_VARIABLES = "PROPERTY_MSG_SELECTED_WSOL_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_SEMIS_VARIABLES = "PROPERTY_MSG_SELECTED_SEMIS_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_REC_FAU_VARIABLES = "PROPERTY_MSG_SELECTED_REC_FAU_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_AI_VARIABLES = "PROPERTY_MSG_SELECTED_AI_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_FNE_VARIABLES = "PROPERTY_MSG_SELECTED_FNE_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_FERT_VARIABLES = "PROPERTY_MSG_SELECTED_FERT_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_PHYT_VARIABLES = "PROPERTY_MSG_SELECTED_PHYT_VARIABLES";
    private String semisDatatype;

    private String wsolDatatype;

    private String paturageDatatype;

    private String aiDatatype;

    private String fneDatatype;

    private String fertDatatype;

    private String phytDatatype;

    private String recFauDatatype;
    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @return the string
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java
     * .util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        // TODO(ptcherniati) Auto-generated method stub
        return null;
    }

    /**
     * Builds the output data.
     *
     * @param requestMetadatasMap
     * @param outputPrintStreamMap
     * @link(IParameter) the parameters
     * @link(Map<String,PrintStream>) the output print stream map @see
     * org.inra.ecoinfo.acbb.extraction .mps.impl.MPSOutputsBuildersResolver#
     * buildOutputData(org.inra.ecoinfo.extraction.IParameter, java.util.Map)
     */
    protected void buildOutputData(final Map<String, Object> requestMetadatasMap,
            final Map<String, PrintStream> outputPrintStreamMap) {
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            super.buildOutputData(requestMetadatasMap, datatypeNameEntry.getValue(),  
                    Stream.of(PROPERTY_MSG_SELECTED_SEMIS_VARIABLES,PROPERTY_MSG_SELECTED_WSOL_VARIABLES, PROPERTY_MSG_SELECTED_PATURAGE_VARIABLES, PROPERTY_MSG_SELECTED_REC_FAU_VARIABLES, PROPERTY_MSG_SELECTED_AI_VARIABLES, PROPERTY_MSG_SELECTED_FNE_VARIABLES, PROPERTY_MSG_SELECTED_FERT_VARIABLES, PROPERTY_MSG_SELECTED_PHYT_VARIABLES).toArray(String[]::new),
                    Stream.of(semisDatatype,wsolDatatype, paturageDatatype, recFauDatatype, aiDatatype, fneDatatype, fertDatatype, phytDatatype).toArray(String[]::new)
                    );
        }
    }

    /**
     * @param aiDatatype the aiDatatype to set
     */
    public void setAiDatatype(String aiDatatype) {
        this.aiDatatype = aiDatatype;
    }

    /**
     * @param fertDatatype
     */
    public void setFertDatatype(String fertDatatype) {
        this.fertDatatype = fertDatatype;
    }

    /**
     * @param fneDatatype the fneDatatype to set
     */
    public void setFneDatatype(String fneDatatype) {
        this.fneDatatype = fneDatatype;
    }

    /**
     * @param paturageDatatype
     */
    public void setPaturageDatatype(String paturageDatatype) {
        this.paturageDatatype = paturageDatatype;
    }

    /**
     * @param phytDatatype
     */
    public void setPhytDatatype(String phytDatatype) {
        this.phytDatatype = phytDatatype;
    }

    /**
     * @param recFauDatatype
     */
    public void setRecFauDatatype(String recFauDatatype) {
        this.recFauDatatype = recFauDatatype;
    }

    /**
     * @param semisDatatype
     */
    public void setSemisDatatype(String semisDatatype) {
        this.semisDatatype = semisDatatype;
    }

    /**
     * @param wsolDatatype
     */
    public void setWsolDatatype(String wsolDatatype) {
        this.wsolDatatype = wsolDatatype;
    }

    @Override
    protected String getLocalBundle() {
        return BUNDLE_SOURCE_PATH;
    }
        
            
}
