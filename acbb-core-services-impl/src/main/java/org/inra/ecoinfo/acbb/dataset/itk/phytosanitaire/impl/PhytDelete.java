package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.impl;

import org.inra.ecoinfo.acbb.dataset.IDeleteRecord;
import org.inra.ecoinfo.acbb.dataset.impl.DeleteRecord;

/**
 * @author ptcherniati
 */
public class PhytDelete extends DeleteRecord implements IDeleteRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
