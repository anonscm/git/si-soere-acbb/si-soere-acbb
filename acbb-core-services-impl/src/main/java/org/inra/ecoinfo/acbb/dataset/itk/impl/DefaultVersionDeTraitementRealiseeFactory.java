package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.*;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;

import javax.persistence.Transient;
import java.util.Optional;

/**
 * A factory for creating DefaultVersionDeTraitementRealisee.
 *
 * @param <T> the generic line
 * @author philippe Tcherniatinsky
 */
public class DefaultVersionDeTraitementRealiseeFactory<T extends ILineRecord> extends
        AbstractPeriodFactory implements IversionDeTraitementRealiseeFactory<T> {

    /**
     * The version de traitement realisee dao
     *
     * @link(IVersionDeTraitementRealiseeDAO).
     */
    @Transient
    protected IVersionDeTraitementRealiseeDAO versionDeTraitementRealiseeDAO;

    /**
     * The annee realisee factory @link(IAnneeRealiseeFactory
     */
    @Transient
    protected IAnneeRealiseeFactory<T> anneeRealiseeFactory;

    /**
     * The tempo dao @link(ITempoDAO).
     */
    @Transient
    protected ITempoDAO tempoDAO;

    /**
     * <p>
     * create a new Tempo by invoking
     * </p>
     * <ul>
     * <li>the version in Db for VersionDeTraitement, Parcelle and rotation
     * Number</li>
     * <li>if the VersionRealisee exists, the anneeRealisee in versionRealise
     * for the year number</li>
     * <li>if the AnneeRealisee exists, the periodeRealisee in anneeRealise for
     * the periode number</li>
     * </ul>
     *
     * @param versionDeTraitement
     * @param parcelle
     * @param rotationNumber
     * @param anneeNumber
     * @param periodeNumber
     * @return Tempo
     */
    Tempo getNewTempo(final VersionDeTraitement versionDeTraitement, final Parcelle parcelle,
                      final int rotationNumber, final int anneeNumber, final int periodeNumber) {
        final VersionTraitementRealisee versionTraitementRealisee = this.versionDeTraitementRealiseeDAO
                .getByNKey(versionDeTraitement, parcelle, rotationNumber).orElse(null);
        final AnneeRealisee anneeRealisee = versionTraitementRealisee == null ? null
                : versionTraitementRealisee.getAnneesRealisees().get(anneeNumber);
        final PeriodeRealisee periodeRealisee = anneeRealisee == null ? null : anneeRealisee
                .getPeriodesRealisees().get(periodeNumber);
        return new Tempo(versionDeTraitement, parcelle, versionTraitementRealisee, anneeRealisee,
                periodeRealisee);
    }

    /**
     * <p>
     * return both
     * </p>
     * <ul>
     * <li>the VersionRealisee from the tempo if exoists</li>
     * <li>the versionrealisee in DB for the VersionDeTraitement, the Parcele,
     * the rotation number</li>
     * </ul>
     *
     * @param line
     * @param infoReport
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.IversionDeTraitementRealiseeFactory#getOrCreateVersiontraitementRealise(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public VersionTraitementRealisee getOrCreateVersiontraitementRealise(final T line,
                                                                         final ErrorsReport errorsReport, final InfoReport infoReport) {
        return Optional.ofNullable(line)
                .map(l -> l.getTempo())
                .map(t -> t.getVersionTraitementRealisee())
                .orElseGet(() -> this.versionDeTraitementRealiseeDAO.getByNKey(line.getTempo().getVersionDeTraitement(), line.getParcelle(), line.getRotationNumber())
                        .orElse(null));
    }

    /**
     * <p>
     * return both</p< <ul> <li>the tempo in db if exists</li>
     * <li>a nex tempo</li>
     * </ul>
     * <p>
     * then invoke the factories for update vesion, anne and periode in the
     * tempo
     * </p>
     *
     * @param line
     * @param versionDeTraitement
     * @param parcelle
     * @param errorsReport
     * @param infoReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.IversionDeTraitementRealiseeFactory#getTempo(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public final Tempo getTempo(final T line, final VersionDeTraitement versionDeTraitement,
                                final Parcelle parcelle, final ErrorsReport errorsReport, final InfoReport infoReport) {
        Optional<Tempo> tempoOpt = tempoDAO.getByNKey(versionDeTraitement, parcelle,
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber());
        if (tempoOpt.isPresent()) {
            line.setTempo(tempoOpt.get());
            return tempoOpt.get();
        }
        Tempo tempo = this.getNewTempo(versionDeTraitement, parcelle, line.getRotationNumber(),
                line.getAnneeNumber(), line.getPeriodeNumber());
        final int errorsCount = errorsReport.getErrorsMessages().length();
        line.setTempo(tempo);
        final VersionTraitementRealisee versionRealisee = this.getOrCreateVersiontraitementRealise(line, errorsReport, infoReport);
        tempo.setVersionTraitementRealisee(versionRealisee);
        if (errorsCount != errorsReport.getErrorsMessages().length()) {
            tempo.setInErrors(Boolean.TRUE);
            return tempo;
        }
        tempo.setAnneeRealisee(this.anneeRealiseeFactory.getOrCreateAnneeRealisee(line,
                errorsReport, infoReport));
        if (errorsCount != errorsReport.getErrorsMessages().length()) {
            tempo.setInErrors(Boolean.TRUE);
            return tempo;
        }
        tempo.setPeriodeRealisee(this.anneeRealiseeFactory.getPeriodeRealiseeFactory()
                .getOrCreatePeriodeRealisee(line, errorsReport)
                .orElse(null));
        if (errorsCount != errorsReport.getErrorsMessages().length()) {
            tempo.setInErrors(Boolean.TRUE);
            return tempo;
        }
        return tempo;
    }

    /**
     * Sets the annee realisee factory.
     *
     * @param anneeRealiseeFactory the anneeRealiseeFactory to set
     */
    public final void setAnneeRealiseeFactory(final IAnneeRealiseeFactory<T> anneeRealiseeFactory) {
        this.anneeRealiseeFactory = anneeRealiseeFactory;
    }

    /**
     * Sets the tempo dao.
     *
     * @param tempoDAO the tempoDAO to set
     */
    public final void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * Sets the version de traitement realisee dao.
     *
     * @param versionDeTraitementRealiseeDAO the versionDeTraitementRealiseeDAO
     *                                       to set
     */
    public final void setVersionDeTraitementRealiseeDAO(
            final IVersionDeTraitementRealiseeDAO versionDeTraitementRealiseeDAO) {
        this.versionDeTraitementRealiseeDAO = versionDeTraitementRealiseeDAO;
    }

}
