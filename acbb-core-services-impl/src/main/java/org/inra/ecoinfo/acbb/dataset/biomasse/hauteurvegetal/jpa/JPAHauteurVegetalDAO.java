/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.jpa;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction_;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.IHauteurVegetalDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.jpa.JPABiomasseDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPAHauteurVegetalDAO extends JPABiomasseDAO<HauteurVegetal> implements IHauteurVegetalDAO {

    /**
     * @param version
     * @param date
     * @return
     */
    @Override
    public Optional<HauteurVegetal> getByNKey(final VersionFile version, final LocalDate date) {

        CriteriaQuery<HauteurVegetal> query = builder.createQuery(HauteurVegetal.class);
        Root<HauteurVegetal> hv = query.from(HauteurVegetal.class);
        query.where(
                builder.equal(hv.get(BiomassProduction_.version), version),
                builder.equal(hv.get(BiomassProduction_.date), date)
        );
        query.select(hv);
        return getOptional(query);
    }
}
