/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variablededescription;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme_;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement_;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAVariableDeDescriptionDAO.
 */
public class JPAVariableDeDescriptionDAO extends AbstractJPADAO<VariableDescription> implements
        IVariableDeDescriptionDAO {


    /**
     * Gets the by code.
     *
     * @param code
     * @return the by code
     * @link(String) the code
     */
    @Override
    public Optional<VariableDescription> getByCode(final String code) {
        CriteriaQuery<VariableDescription> query = builder.createQuery(VariableDescription.class);
        Root<VariableDescription> variableDescription = query.from(VariableDescription.class);
        query
                .select(variableDescription)
                .where(builder.equal(variableDescription.get(VariableDescription_.code), code));
        return getOptional(query);
    }

    /**
     * Gets the variables de description for traitement.
     *
     * @param traitementProgramme
     * @param version
     * @return the variables de description for traitement
     * @link(TraitementProgramme) the traitement programme
     * @link(int) the version
     */
    @Override
    public List<VariableDescription> getVariablesDeDescriptionForTraitement(
            final TraitementProgramme traitementProgramme, final int version) {
        CriteriaQuery<VariableDescription> query = builder.createQuery(VariableDescription.class);
        Root<TraitementProgramme> tpro = query.from(TraitementProgramme.class);
        ListJoin<TraitementProgramme, VersionDeTraitement> vdt = tpro.join(TraitementProgramme_.versionDeTraitements);
        Join<VersionDeTraitement, TraitementProgramme> tp = vdt.join(VersionDeTraitement_.traitementProgramme);
        SetJoin<VersionDeTraitement, Modalite> modalites = vdt.join(VersionDeTraitement_.modalites);
        Join<Modalite, VariableDescription> vdd = modalites.join(Modalite_.variableDescription);
        query
                .select(vdd)
                .where(
                        builder.equal(tp, traitementProgramme),
                        builder.equal(vdt.get(VersionDeTraitement_.version), version)
                );
        return getResultList(query);
    }
}
