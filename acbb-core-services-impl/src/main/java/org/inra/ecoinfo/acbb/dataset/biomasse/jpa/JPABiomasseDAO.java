package org.inra.ecoinfo.acbb.dataset.biomasse.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.IBiomasseDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;

/**
 * @param <T>
 * @author ptcherniati
 */
public class JPABiomasseDAO<T extends AbstractBiomasse> extends AbstractJPADAO<T> implements
        IBiomasseDAO<T> {

    /**
     *
     **/
    public JPABiomasseDAO() {
        super();
    }
}
