/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.ITravailDuSolDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.TravailDuSol;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.TravailDuSol_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPATravailDuSolDAO extends AbstractJPADAO<TravailDuSol> implements ITravailDuSolDAO {

    /**
     * Gets the by n key.
     *
     * @param version
     * @param rang
     * @param date
     * @return the by n key
     * @link(VersionFile) the version
     * @link(int) the rang
     * @link(Date) the date
     */
    @Override
    public Optional<TravailDuSol> getByNKey(final VersionFile version, final int rang, final LocalDate date) {
        CriteriaQuery<TravailDuSol> query = builder.createQuery(TravailDuSol.class);
        Root<TravailDuSol> wol = query.from(TravailDuSol.class);
        query
                .select(wol)
                .where(
                        builder.equal(wol.get(TravailDuSol_.date), date),
                        builder.equal(wol.join(TravailDuSol_.version), version),
                        builder.equal(wol.get(TravailDuSol_.rang), rang)
                );
        return getOptional(query);
    }
}
