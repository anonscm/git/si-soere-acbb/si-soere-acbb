package org.inra.ecoinfo.acbb.extraction.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.AbstractBiomasseOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class BiomassProductionBuildOutputDisplay
        extends
        AbstractBiomasseOutputBuilder<BiomassProduction, ValeurBiomassProduction, BiomassProduction> {

    /**
     *
     */
    protected static final String FIRST_VARIABLE = "mav";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static protected final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.messages";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public BiomassProductionBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return BiomassProductionBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedWsolVariables,
            Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            PrintStream rawDataPrintStream) {
        return new BiomassProductionOutputHelper(availablesCouverts, rawDataPrintStream, selectedWsolVariables);
    }

    /**
     *
     */
    protected class BiomassProductionOutputHelper extends OutputHelper {

        /**
         *
         */
        protected static final String PATTERN_11_FIELD = ";%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
        /**
         *
         */
        protected static final String PATTERN_5_FIELD = ";%s;%s;%s;%s;%s";

        /**
         *
         */
        protected static final String FIRST_VARIABLE = "mav";

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         * @param availablesCouverts
         */
        protected BiomassProductionOutputHelper(final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
                                                PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(availablesCouverts, rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurBiomasse
         */
        @Override
        protected void addVariablesCulumns(ValeurBiomassProduction valeurBiomasse) {
            String line = String.format(BiomassProductionOutputHelper.PATTERN_5_FIELD,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getStandardDeviation()),
                    valeurBiomasse.getMethode().getNumberId(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesCulumnsForNullVariable() {
            this.rawDataPrintStream.print(BiomassProductionOutputHelper.PATTERN_5_FIELD.replaceAll(
                    "%s", org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param mesure
         */
        @Override
        protected void printLineSpecific(BiomassProduction mesure) {
            String line;
            final MasseVegClass masseVegClass = mesure.getSequence().getMasseVegClass();
            line = String
                    .format(BiomassProductionOutputHelper.PATTERN_11_FIELD,
                            masseVegClass.getLibelle(),
                            this.propertiesValeursQualitative.getProperty(
                                    Utils.createCodeFromString(masseVegClass.getCategorieCouvert().getValeur()),
                                    masseVegClass.getCategorieCouvert().getValeur()),
                            this.propertiesValeursQualitative.getProperty(
                                    Utils.createCodeFromString(masseVegClass.getTypeMasseVegetale().getValeur()),
                                    masseVegClass.getTypeMasseVegetale().getValeur()),
                            this.propertiesValeursQualitative.getProperty(
                                    Utils.createCodeFromString(masseVegClass.getAerien().getValeur()),
                                    masseVegClass.getAerien().getValeur()),
                            this.propertiesValeursQualitative.getProperty(
                                    Utils.createCodeFromString(masseVegClass.getPartiesPlante().getValeur()),
                                    masseVegClass.getPartiesPlante().getValeur()),
                            this.propertiesValeursQualitative.getProperty(
                                    Utils.createCodeFromString(masseVegClass.getDevenir().getValeur()),
                                    masseVegClass.getDevenir().getValeur()),
                            getCouvert(mesure),
                            getAnnee(mesure),
                            mesure.getSerie(),
                            getDateMesure(mesure),
                            mesure.getSequence().getDateDebutRepousse() == null ? org.inra.ecoinfo.acbb.utils.ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE
                                    : DateUtil.getUTCDateTextFromLocalDateTime(mesure.getSequence().getDateDebutRepousse(), DateUtil.DD_MM_YYYY)
                    );
            this.rawDataPrintStream.print(line);
        }
    }
}
