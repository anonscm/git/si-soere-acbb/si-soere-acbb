package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Transient;
import java.util.Optional;

/**
 * A factory for creating a Default PeriodeRealisee objects.
 *
 * @param <T> the generic line
 * @author philippe Tcherniatinsky
 */
public class DefaultPeriodeRealiseeFactory<T extends ILineRecord> extends AbstractPeriodFactory
        implements IPeriodeRealiseeFactory<T> {

    /**
     * The periode realisee dao @link(IPeriodeRealiseeDAO).
     */
    @Transient
    protected IPeriodeRealiseeDAO periodeRealiseeDAO;

    /**
     * <p>
     * Create and persist a new PeriodeRealisee with 0 number and the
     * AnneeRealisee
     * </p>
     *
     * @param anneeRealisee
     * @param errorsReport
     * @see org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeFactory#createDefaultPeriode(org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport)
     */
    @Override
    public void createDefaultPeriode(final AnneeRealisee anneeRealisee,
                                     final ErrorsReport errorsReport) {
        try {
            final PeriodeRealisee defaultPeriode = this.getDefaultPeriode(anneeRealisee);
            this.periodeRealiseeDAO.saveOrUpdate(defaultPeriode);
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
    }

    /**
     * <p>
     * return a new PeriodeRealisee with number 0 and the AnneeRealisee
     * </p>
     *
     * @param anneeRealisee
     * @return a
     * @see org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeFactory#getDefaultPeriode(org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee)
     */
    @Override
    public PeriodeRealisee getDefaultPeriode(final AnneeRealisee anneeRealisee) {
        return new PeriodeRealisee(0, anneeRealisee);
    }

    /**
     * <p>
     * Return both :
     * </p>
     * <ul>
     * <li>the Perioderealisee of the tempo if exixts</li>
     * <li>the PeriodeRealisee in DB for the AnneeRealisee and the Periode
     * Number</li>
     * </ul>
     *
     * @param line
     * @param errorsReport
     * @return PeriodeRealisee
     * @see org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeFactory#getOrCreatePeriodeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport)
     */
    @Override
    public Optional<PeriodeRealisee> getOrCreatePeriodeRealisee(final T line, final ErrorsReport errorsReport) {
        Optional<PeriodeRealisee> periode = Optional.ofNullable(line)
                .map(l -> l.getTempo())
                .map(t -> t.getPeriodeRealisee())
                .map(p -> line.getTempo().getPeriodeRealisee());
        if (!periode.isPresent()) {
            return this.periodeRealiseeDAO.getByNKey(line.getTempo().getAnneeRealisee(), line.getPeriodeNumber());
        }
        return periode;
    }

    /**
     * Sets the periode realisee dao.
     *
     * @param periodeRealiseeDAO the periodeRealiseeDAO to set
     */
    public final void setPeriodeRealiseeDAO(final IPeriodeRealiseeDAO periodeRealiseeDAO) {
        this.periodeRealiseeDAO = periodeRealiseeDAO;
    }

}
