/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variablededescription;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariableDescription> {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    /**
     * The variable de description dao.
     */
    IVariableDeDescriptionDAO variableDeDescriptionDAO;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * The properties description en @link(Properties).
     */
    Properties propertiesDescriptionEN;

    /**
     * Creates the or update variable de description.
     *
     * @param nom
     * @param description
     * @param type
     * @param dbVariableDeDescription
     * @throws PersistenceException  the persistence exception
     * @throws BadValueTypeException the bad value type exception
     * @link(String) the nom
     * @link(String) the description
     * @link(String) the type
     * @link(VariableDescription) the db variable de description
     * @link(String) the nom
     * @link(String) the description
     * @link(String) the type
     * @link(VariableDescription) the db variable de description
     */
    void createOrUpdateVariableDeDescription(final String nom, final String description,
                                             final String type, final VariableDescription dbVariableDeDescription)
            throws PersistenceException, BadValueTypeException {
        if (dbVariableDeDescription == null) {
            final VariableDescription variableDeDescription = new VariableDescription(nom,
                    description, type);
            this.variableDeDescriptionDAO.saveOrUpdate(variableDeDescription);
        } else {
            dbVariableDeDescription.setDescription(description);
            this.variableDeDescriptionDAO.saveOrUpdate(dbVariableDeDescription);
        }

    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            final TokenizerValues tokenizerValues = new TokenizerValues(values);
            while ((values = parser.getLine()) != null) {
                final String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.variableDeDescriptionDAO.remove(this.variableDeDescriptionDAO.getByCode(code).orElseThrow(() -> new BusinessException("bad variable desription")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<VariableDescription> getAllElements() {
        List<VariableDescription> all = this.variableDeDescriptionDAO
                .getAll(VariableDescription.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param variableDeDescription
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(VariableDescription) the variable de description
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final VariableDescription variableDeDescription) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variableDeDescription == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : variableDeDescription.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variableDeDescription == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomFR.getProperty(variableDeDescription.getNom(),
                                variableDeDescription.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variableDeDescription == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.getProperty(variableDeDescription.getNom(),
                                variableDeDescription.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variableDeDescription == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : variableDeDescription.getDescription(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variableDeDescription == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesDescriptionEN.getProperty(
                                variableDeDescription.getDescription(),
                                variableDeDescription.getDescription()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variableDeDescription == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : variableDeDescription.getType(), new String[]{
                        VariableDescription.VARIABLE_DE_FORCAGE,
                        VariableDescription.VARIABLE_DE_CONDUITE}, null, true, false,
                        false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<VariableDescription> initModelGridMetadata() {
        this.propertiesNomFR = this.localizationManager.newProperties(
                VariableDescription.NAME_ENTITY_JPA, VariableDescription.ATTRIBUTE_JPA_NAME,
                Locale.FRENCH);
        this.propertiesNomEN = this.localizationManager.newProperties(
                VariableDescription.NAME_ENTITY_JPA, VariableDescription.ATTRIBUTE_JPA_NAME,
                Locale.ENGLISH);
        this.propertiesDescriptionEN = this.localizationManager.newProperties(
                VariableDescription.NAME_ENTITY_JPA, VariableDescription.ATTRIBUTE_JPA_DESCRIPTION,
                Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist variable.
     *
     * @param errorsReport
     * @param nom
     * @param description
     * @param type
     * @throws PersistenceException  the persistence exception
     * @throws BadValueTypeException the bad value type exception
     * @link(ErrorsReport) the errors report
     * @link(String) the nom
     * @link(String) the description
     * @link(String) the type
     * @link(ErrorsReport) the errors report
     * @link(String) the nom
     * @link(String) the description
     * @link(String) the type
     */
    void persistVariable(final ErrorsReport errorsReport, final String nom,
                         final String description, final String type) throws PersistenceException,
            BadValueTypeException {
        final VariableDescription dbVariableDeDescription = this.retrieveDBVariableDeDescription(
                errorsReport, Utils.createCodeFromString(nom)).orElse(null);
        this.createOrUpdateVariableDeDescription(nom, description, type, dbVariableDeDescription);
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        VariableDescription.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String nom = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                String type;
                type = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.persistVariable(errorsReport, nom, description, type);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException | BadValueTypeException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db variable de description.
     *
     * @param errorsReport
     * @param codeVariable
     * @return the variable description
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code variable
     * @link(ErrorsReport) the errors report
     * @link(String) the code variable
     */
    Optional<VariableDescription> retrieveDBVariableDeDescription(final ErrorsReport errorsReport,
                                                                  final String codeVariable) {
        return this.variableDeDescriptionDAO.getByCode(codeVariable);
    }

    /**
     * Sets the variable de description dao.
     *
     * @param variableDeDescriptionDAO the new variable de description dao
     */
    public final void setVariableDeDescriptionDAO(
            final IVariableDeDescriptionDAO variableDeDescriptionDAO) {
        this.variableDeDescriptionDAO = variableDeDescriptionDAO;
    }
}
