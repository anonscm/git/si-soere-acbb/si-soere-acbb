package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import java.util.List;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class AILineRecord extends AbstractLineRecord<AILineRecord, ListeItineraire> {

    /**
     * Instantiates a new aI line record.
     *
     * @param lineCount
     * @link{long the line count
     */
    public AILineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * Gets the aCBB variables values.
     *
     * @return the aCBB variables values
     */
    public List<ACBBVariableValue<ListeItineraire>> getACBBVariablesValues() {
        return this.variablesValues;
    }

    /**
     * Sets the aCBB variables values.
     *
     * @param variablesValues the new aCBB variables values
     */
    public final void setACBBVariablesValues(
            final List<ACBBVariableValue<ListeItineraire>> variablesValues) {
        this.variablesValues = variablesValues;
    }
}
