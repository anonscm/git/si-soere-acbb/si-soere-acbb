/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.datatypevariableunite;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<DatatypeVariableUniteACBB> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.datatypevariableunite.messages";

    /**
     * The Constant PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE @link(String).
     */
    static final String PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE = "PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE";

    /**
     * The Constant PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE @link(String).
     */
    static final String PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";

    /**
     * The Constant PROPERTY_MSG_UNITE_MISSING_IN_DATABASE @link(String).
     */
    static final String PROPERTY_MSG_UNITE_MISSING_IN_DATABASE = "PROPERTY_MSG_UNITE_MISSING_IN_DATABASE";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The datatype dao.
     */
    IDatatypeDAO datatypeDAO;

    /**
     * The variable acbbdao.
     */
    IVariableACBBDAO variableACBBDAO;

    /**
     * The unite dao.
     */
    IUniteDAO uniteDAO;

    /**
     * The datatype unite variable acbbdao.
     */
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;

    /**
     * The datatypes possibles @link(String[]).
     */
    String[] datatypesPossibles;

    /**
     * The variables possibles @link(String[]).
     */
    String[] variablesPossibles;

    /**
     * The unites possibles @link(String[]).
     */
    String[] unitesPossibles;

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = tokenizerValues.nextToken();
                final String uniteCode = tokenizerValues.nextToken();
                final DatatypeVariableUniteACBB dbDVU = this.datatypeVariableUniteACBBDAO
                        .getByNKey(datatypeCode, variableCode, uniteCode)
                        .orElseThrow(PersistenceException::new);
                datatypeVariableUniteACBBDAO.delete(dbDVU);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<DatatypeVariableUniteACBB> getAllElements() {
        List<DatatypeVariableUniteACBB> all = this.datatypeVariableUniteACBBDAO
                .getAllDatatypeVariableUniteACBB();
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the names datatypes possibles.
     *
     * @return the names datatypes possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesDatatypesPossibles() throws PersistenceException {
        final List<DataType> datatypes = this.datatypeDAO.getAll(DataType.class);
        final String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (final DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        return namesDatatypesPossibles;
    }

    /**
     * Gets the names unites possibles.
     *
     * @return the names unites possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesUnitesPossibles() throws PersistenceException {
        final List<Unite> unites = this.uniteDAO.getAll(Unite.class);
        final String[] getNamesUnitesPossibles = new String[unites.size()];
        int index = 0;
        for (final Unite unite : unites) {
            getNamesUnitesPossibles[index++] = unite.getCode();
        }
        return getNamesUnitesPossibles;
    }

    /**
     * Gets the names variables possibles.
     *
     * @return the names variables possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesVariablesPossibles() throws PersistenceException {
        final List<Variable> variables = this.variableACBBDAO.getAll();
        final String[] namesVariablesPossibles = new String[variables.size()];
        int index = 0;
        for (final Variable variable : variables) {
            namesVariablesPossibles[index++] = variable.getName();
        }
        return namesVariablesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param datatypeVariableUnite
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(DatatypeVariableUniteACBB) the datatype variable unite
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final DatatypeVariableUniteACBB datatypeVariableUnite) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypeVariableUnite.getDatatype().getCode(),
                        this.datatypesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypeVariableUnite.getVariable().getCode(),
                        this.variablesPossibles, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypeVariableUnite.getUnite().getCode(), this.unitesPossibles,
                        null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypeVariableUnite.getValeurMin(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypeVariableUnite.getValeurMax(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<DatatypeVariableUniteACBB> initModelGridMetadata() {
        try {

            this.datatypesPossibles = this.getNamesDatatypesPossibles();
            this.variablesPossibles = this.getNamesVariablesPossibles();
            this.unitesPossibles = this.getNamesUnitesPossibles();
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist datatype variable.
     *
     * @param errorsReport
     * @param datatypeCode
     * @param variableCode
     * @param uniteCode
     * @param minValue
     * @param maxValue
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     * @link(String) the variable code
     * @link(String) the unite code
     * @link(Float) the min value
     * @link(Float) the max value
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     * @link(String) the variable code
     * @link(String) the unite code
     * @link(Float) the min value
     * @link(Float) the max value
     */
    DatatypeVariableUniteACBB persistDatatypeVariable(final ErrorsReport errorsReport, final String datatypeCode,
                                                      final String variableCode, final String uniteCode, final Float minValue,
                                                      final Float maxValue) throws PersistenceException {
        final DataType dbDatatype = this.retrieveDBDatatype(errorsReport, datatypeCode);
        final VariableACBB dbVariable = this.retrieveDBVariable(errorsReport, variableCode);
        final Unite dbUnite = this.retrieveDBUnite(errorsReport, uniteCode);
        DatatypeVariableUniteACBB dbDatatypeVariableUnite = this.datatypeVariableUniteACBBDAO
                .getByNKey(datatypeCode, variableCode, uniteCode).orElse(null);
        if (dbDatatype != null && dbVariable != null && dbUnite != null
                && dbDatatypeVariableUnite == null) {
            dbDatatypeVariableUnite = new DatatypeVariableUniteACBB(
                    dbDatatype, dbUnite, dbVariable, minValue, maxValue);
            this.datatypeVariableUniteACBBDAO.saveOrUpdate(dbDatatypeVariableUnite);
            return dbDatatypeVariableUnite;
        } else if (dbDatatypeVariableUnite != null) {
            dbDatatypeVariableUnite.setValeurMin(minValue);
            dbDatatypeVariableUnite.setValeurMax(maxValue);
            return null;
        }
        return null;
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        Map<DataType, List<DatatypeVariableUniteACBB>> nodeablesDVU = new HashMap();
        try {
            this.skipHeader(parser);
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = tokenizerValues.nextToken();
                final String uniteCode = tokenizerValues.nextToken();
                final Float minValue = tokenizerValues.nextTokenFloat();
                final Float maxValue = tokenizerValues.nextTokenFloat();
                final DatatypeVariableUniteACBB nodeable = persistDatatypeVariable(errorsReport, datatypeCode, variableCode, uniteCode,
                        minValue, maxValue);
                if (nodeable != null) {
                    nodeablesDVU.computeIfAbsent(nodeable.getDatatype(), k -> new LinkedList())
                            .add(nodeable);
                }
                if (parser.getLastLineNumber() % 50 == 0) {
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

            try {

                Map<DataType, List<INode>> datatypeNodesMap = mgaServiceBuilder.loadNodesByTypeResource(WhichTree.TREEDATASET, DataType.class)
                        .filter(n -> nodeablesDVU.containsKey(n.getNodeable()))
                        .collect(Collectors.groupingBy(
                                p -> ((DataType) p.getNodeable()))
                        );
                Map<String, RealNode> realNodes = mgaServiceBuilder.getRecorder().getAllRealNodes()
                        .collect(Collectors.toMap(r -> r.getPath(), k -> k));
                int[] i = new int[]{0};
                for (Iterator<Map.Entry<DataType, List<DatatypeVariableUniteACBB>>> iteratorOnDVU = nodeablesDVU.entrySet().iterator(); iteratorOnDVU.hasNext(); ) {
                    Map.Entry<DataType, List<DatatypeVariableUniteACBB>> dvusEntry = iteratorOnDVU.next();
                    DataType datatype = dvusEntry.getKey();
                    List<DatatypeVariableUniteACBB> dvus = dvusEntry.getValue();
                    List<INode> datatypeNodeList = datatypeNodesMap.get(datatype);
                    if (datatypeNodeList == null) {
                        continue;
                    }
                    for (Iterator<INode> iteratorOnDatatypeNodes = datatypeNodeList.iterator(); iteratorOnDatatypeNodes.hasNext(); ) {
                        INode datatypeNode = iteratorOnDatatypeNodes.next();
                        RealNode parentRn = datatypeNode.getRealNode();
                        for (Iterator<DatatypeVariableUniteACBB> dvuIterator = dvus.iterator(); dvuIterator.hasNext(); ) {
                            DatatypeVariableUniteACBB dvu = dvuIterator.next();
                            String path = String.format("%s%s%s", parentRn.getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = realNodes.get(path);
                            if (rn == null) {
                                rn = new RealNode(parentRn, null, dvu, path);
                                realNodes.put(path, rn);
                                mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            }
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) datatypeNode, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        }
                        iteratorOnDatatypeNodes.remove();
                    }
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                    iteratorOnDVU.remove();
                }
                policyManager.clearTreeFromSession();

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            } catch (final BusinessException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    /**
     * @param datatypeNode
     * @param nv
     * @return
     */
    public RealNode getOrCreateRealNode(INode datatypeNode, DatatypeVariableUnite nv) {
        return policyManager.getMgaServiceBuilder().getRecorder()
                .getRealNodeByNKey(String.format("%s%s%s", datatypeNode.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, nv.getCode()))
                .orElse(new RealNode(datatypeNode.getRealNode(), null, nv, String.format("%s%s%s", datatypeNode.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, nv.getCode())));
    }

    /**
     * Retrieve db datatype.
     *
     * @param errorsReport
     * @param datatypeCode
     * @return the data type
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     */
    DataType retrieveDBDatatype(final ErrorsReport errorsReport, final String datatypeCode)
            throws PersistenceException {
        final DataType datatype = this.datatypeDAO.getByCode(datatypeCode).orElse(null);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager
                    .getMessage(Recorder.BUNDLE_SOURCE_PATH,
                            Recorder.PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE), datatypeCode));
        }
        return datatype;
    }

    /**
     * Retrieve db unite.
     *
     * @param errorsReport
     * @param uniteCode
     * @return the unite
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the unite code
     * @link(ErrorsReport) the errors report
     * @link(String) the unite code
     */
    Unite retrieveDBUnite(final ErrorsReport errorsReport, final String uniteCode)
            throws PersistenceException {
        final Unite unite = this.uniteDAO.getByCode(uniteCode).orElse(null);
        if (unite == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_UNITE_MISSING_IN_DATABASE),
                    uniteCode));
        }
        return unite;
    }

    /**
     * Retrieve db variable.
     *
     * @param errorsReport
     * @param variableCode
     * @return the variable acbb
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the variable code
     * @link(ErrorsReport) the errors report
     * @link(String) the variable code
     */
    VariableACBB retrieveDBVariable(final ErrorsReport errorsReport, final String variableCode)
            throws PersistenceException {
        final VariableACBB variable = (VariableACBB) this.variableACBBDAO.getByCode(variableCode).orElse(null);
        if (variable == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager
                    .getMessage(Recorder.BUNDLE_SOURCE_PATH,
                            Recorder.PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE), variableCode));
        }
        return variable;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public final void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteDAO the new datatype unite variable acbbdao
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteDAO;
    }

    /**
     * Sets the unite dao.
     *
     * @param uniteDAO the new unite dao
     */
    public final void setUniteDAO(final IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     * Sets the variable acbbdao.
     *
     * @param variableDAO the new variable acbbdao
     */
    public final void setVariableACBBDAO(final IVariableACBBDAO variableDAO) {
        this.variableACBBDAO = variableDAO;
    }

}
