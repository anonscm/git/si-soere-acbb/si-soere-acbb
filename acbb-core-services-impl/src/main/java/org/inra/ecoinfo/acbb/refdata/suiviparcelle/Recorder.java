/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.suiviparcelle;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<SuiviParcelle> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.suiviparcelle.messages";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_ERROR_BAD_BEGIN_DATE @link(String).
     */
    static final String PROPERTY_MSG_ERROR_BAD_BEGIN_DATE = "PROPERTY_MSG_ERROR_BAD_BEGIN_DATE";

    /**
     * The Constant PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_START_TREATMENT.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_START_TREATMENT = "PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_END_TREATMENT";

    /**
     * The Constant PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_END_TREATMENT.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_END_TREATMENT = "PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_END_TREATMENT";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The traitement dao {@link ITraitementDAO}.
     */
    ITraitementDAO traitementDAO;

    /**
     * The parcelle dao.
     */
    IParcelleDAO parcelleDAO;

    /**
     * The site dao {@link ISiteACBBDAO}.
     */
    ISiteACBBDAO siteDAO;

    /**
     * The suivi parcelle dao {@link IParcelleDAO}.
     */
    ISuiviParcelleDAO suiviParcelleDAO;

    /**
     * The names sites possibles @link(String[]).
     */
    String[] namesSitesPossibles;

    /**
     * The names traitements possibles @link(Map<String,String[]>).
     */
    Map<String, String[]> namesTraitementsPossibles;

    /**
     * The names parcelles possibles @link(Map<String,String[]>).
     */
    Map<String, String[]> namesParcellesPossibles;

    /**
     * Builds the string date debut traitement.
     *
     * @param suiviParcelle
     * @return the string
     * @link(SuiviParcelle) the suivi parcelle
     * @link(SuiviParcelle) the suivi parcelle
     */
    String buildStringDateDebutTraitement(final SuiviParcelle suiviParcelle) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (suiviParcelle.getDateDebutTraitement() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(suiviParcelle.getDateDebutTraitement(), this.datasetDescriptor.getColumns().get(3).getFormatType());
        }
        return dateDebut;
    }

    /**
     * Creates the or update suivi parcelle.
     *
     * @param dbTraitement
     * @param dbParcelle
     * @param dateDebutTraitment
     * @param dbSuiviParcelle
     * @throws PersistenceException the persistence exception
     * @link(TraitementProgramme) the db traitement
     * @link(Parcelle) the db parcelle
     * @link(Date) the date debut traitment
     * @link(SuiviParcelle) the db suivi parcelle
     * @link(TraitementProgramme) the db traitement
     * @link(Parcelle) the db parcelle
     * @link(Date) the date debut traitment
     * @link(SuiviParcelle) the db suivi parcelle
     * @link(TraitementProgramme) the db traitement
     * @link(Parcelle) the db parcelle
     * @link(SuiviParcelle) the db suivi parcelle
     */
    void createOrUpdateSuiviParcelle(final TraitementProgramme dbTraitement,
                                     final Parcelle dbParcelle, final LocalDate dateDebutTraitment,
                                     final SuiviParcelle dbSuiviParcelle) throws PersistenceException {
        if (dbSuiviParcelle == null) {
            final SuiviParcelle suiviParcelle = new SuiviParcelle(dbTraitement, dbParcelle,
                    dateDebutTraitment);
            this.suiviParcelleDAO.saveOrUpdate(suiviParcelle);
        } else {
            this.suiviParcelleDAO.saveOrUpdate(dbSuiviParcelle);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeTraitement = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                final String codeParcelle = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String dateDebutTraitementString = tokenizerValues.nextToken();
                final SiteACBB dbSite = this.siteDAO.getByPath(codeSite)
                        .map(s -> (SiteACBB) s)
                        .orElseThrow(() -> new BusinessException("bad site"));
                final TraitementProgramme dbTraitementProgramme = this.traitementDAO.getByNKey(
                        dbSite.getId(), codeTraitement)
                        .orElseThrow(() -> new BusinessException("bad treatment"));
                final Parcelle dbParcelle = this.parcelleDAO
                        .getByNKey(Parcelle.getCodeFromNameAndSite(codeParcelle, dbSite))
                        .orElseThrow(() -> new BusinessException("bad plot"));
                final LocalDate dateDebutTraitement;
                dateDebutTraitement = DateUtil.readLocalDateFromText(dateDebutTraitementString, DateUtil.DD_MM_YYYY_FILE);
                this.suiviParcelleDAO.remove(this.suiviParcelleDAO.getByNKey(dbParcelle,
                        dbTraitementProgramme, dateDebutTraitement)
                        .orElseThrow(() -> new BusinessException("bad suivi parcelle")));
            }
        } catch (final IOException | DateTimeException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<SuiviParcelle> getAllElements() {
        List<SuiviParcelle> all = this.suiviParcelleDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the date debut traitement.
     *
     * @param errorsReport
     * @param dateDebutTraitementString
     * @param dbTraitement
     * @return the date debut traitement
     * @link(ErrorsReport) the errors report
     * @link(String) the date debut traitement string
     * @link(TraitementProgramme) the db traitement
     * @link(ErrorsReport) the errors report
     * @link(String) the date debut traitement string
     * @link(TraitementProgramme) the db traitement
     */
    LocalDate getDateDebutTraitement(final ErrorsReport errorsReport,
                                     final String dateDebutTraitementString, final TraitementProgramme dbTraitement) {
        final LocalDate dateDebutTraitement;
        try {
            dateDebutTraitement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDebutTraitementString);
            if (this.testIsValidDateForTreatment(errorsReport, dateDebutTraitement, dbTraitement)) {
                return dateDebutTraitement;
            }
            return null;

        } catch (final DateTimeException e) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_BAD_BEGIN_DATE),
                    dateDebutTraitementString, DateUtil.DD_MM_YYYY));
            return null;
        }

    }

    /**
     * Gets the names parcelles possibles.
     *
     * @return the names parcelles possibles
     */
    Map<String, String[]> getNamesParcellesPossibles() throws PersistenceException {
        final List<SiteACBB> sites = this.parcelleDAO.getAllSitesWithParcelleDefined();
        final Map<String, String[]> parcellesPossibles = new ConcurrentHashMap();
        for (final Site site : sites) {
            final List<Parcelle> parcelles = this.parcelleDAO.getBySite(site);
            if (!parcelles.isEmpty()) {
                final String[] localNamesParcellesPossibles = new String[parcelles.size()];
                int index = 0;
                for (final Parcelle parcelle : parcelles) {
                    localNamesParcellesPossibles[index++] = parcelle.getName();
                }
                parcellesPossibles.put(site.getName(), localNamesParcellesPossibles);
            }
        }
        return parcellesPossibles;
    }

    /**
     * Gets the names sites possibles.
     *
     * @return the names sites possibles
     */
    String[] getNamesSitesPossibles() throws PersistenceException {
        final List<SiteACBB> sites = this.parcelleDAO.getAllSitesWithParcelleDefined();
        final String[] localNamesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (final Site site : sites) {
            localNamesSitesPossibles[index++] = site.getName();
        }
        return localNamesSitesPossibles;
    }

    /**
     * Gets the names traitements possibles.
     *
     * @return the names traitements possibles
     */
    Map<String, String[]> getNamesTraitementsPossibles() throws PersistenceException {
        final List<SiteACBB> localSites = this.siteDAO.getAllSitesACBB();
        final Map<String, String[]> traitementsPossibles = new ConcurrentHashMap();
        for (final Site site : localSites) {
            final List<TraitementProgramme> traitements = this.traitementDAO.getBySite(site);
            final String[] namesTraitementsPossibles = new String[traitements.size()];
            if (!traitements.isEmpty()) {
                int index = 0;
                for (final TraitementProgramme traitement : traitements) {
                    namesTraitementsPossibles[index++] = traitement.getNom();
                }
                traitementsPossibles.put(site.getName(), namesTraitementsPossibles);
            }
            traitementsPossibles.put(site.getName(), namesTraitementsPossibles);
        }
        return traitementsPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param suiviParcelle
     * @return the new line model grid metadata
     * @link(SuiviParcelle) the suivi parcelle
     * @link(SuiviParcelle) the suivi parcelle
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final SuiviParcelle suiviParcelle) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata siteColumn = new ColumnModelGridMetadata(
                suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : suiviParcelle
                        .getParcelle().getSite().getName(), this.namesSitesPossibles, null, true,
                false, true);
        final ColumnModelGridMetadata traitementColumn = new ColumnModelGridMetadata(
                suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : suiviParcelle
                        .getTraitement().getNom(), this.namesTraitementsPossibles, null, true,
                false, true);
        final ColumnModelGridMetadata parcelleColumn = new ColumnModelGridMetadata(
                suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : suiviParcelle
                        .getParcelle().getName(), this.namesParcellesPossibles, null, true, false,
                true);
        final List<ColumnModelGridMetadata> refsSite = new LinkedList();
        refsSite.add(traitementColumn);
        traitementColumn.setValue(suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : suiviParcelle.getTraitement().getNom());
        refsSite.add(parcelleColumn);
        parcelleColumn.setValue(suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : suiviParcelle.getParcelle().getName());
        siteColumn.setRefs(refsSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(siteColumn);
        traitementColumn.setValue(suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : suiviParcelle.getTraitement().getNom());
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(traitementColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(parcelleColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        suiviParcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this
                                .buildStringDateDebutTraitement(suiviParcelle),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, true, false, true));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<SuiviParcelle> initModelGridMetadata() {
        try {
            this.namesSitesPossibles = this.getNamesSitesPossibles();
            this.namesParcellesPossibles = this.getNamesParcellesPossibles();
            this.namesTraitementsPossibles = this.getNamesTraitementsPossibles();
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist suivi parcelle.
     *
     * @param errorsReport
     * @param codeSite
     * @param codeTraitement
     * @param codeParcelle
     * @param dateDebutTraitementString
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the code traitement
     * @link(String) the code parcelle
     * @link(String) the date debut traitement string
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the code traitement
     * @link(String) the code parcelle
     * @link(String) the date debut traitement string
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the code traitement
     * @link(String) the code parcelle
     */
    void persistSuiviParcelle(final ErrorsReport errorsReport, final String codeSite,
                              final String codeTraitement, final String codeParcelle,
                              final String dateDebutTraitementString) throws PersistenceException {
        final SiteACBB dbSite = this.retrieveDBSite(errorsReport, codeSite);
        if (dbSite != null) {
            final TraitementProgramme dbTraitement = this.retrieveDBTraitement(errorsReport,
                    codeTraitement, dbSite);
            final Parcelle dbParcelle = this.retrieveDBParcelle(errorsReport, codeParcelle, dbSite);
            final LocalDate dateDebutTraitement = this.getDateDebutTraitement(errorsReport,
                    dateDebutTraitementString, dbTraitement);
            if (dbTraitement != null && dbParcelle != null && dateDebutTraitement != null) {
                final SuiviParcelle dbSuiviParcelle = this.suiviParcelleDAO.getByNKey(dbParcelle,
                        dbTraitement, dateDebutTraitement).orElse(null);
                this.createOrUpdateSuiviParcelle(dbTraitement, dbParcelle, dateDebutTraitement,
                        dbSuiviParcelle);
            }
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        SuiviParcelle.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeTraitement = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                final String codeParcelle = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String dateDebutTraitementString = tokenizerValues.nextToken();
                this.persistSuiviParcelle(errorsReport, codeSite, codeTraitement, codeParcelle,
                        dateDebutTraitementString);
                if (parser.lastLineNumber() % 50 == 0) {
                    suiviParcelleDAO.flush();
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db parcelle.
     *
     * @param errorsReport
     * @param codeParcelle
     * @param site
     * @return the parcelle
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code parcelle
     * @link(SiteACBB) the site
     * @link(ErrorsReport) the errors report
     * @link(String) the code parcelle
     * @link(SiteACBB) the site
     * @link(ErrorsReport) the errors report
     * @link(String) the code parcelle
     * @link(SiteACBB) the site
     */
    Parcelle retrieveDBParcelle(final ErrorsReport errorsReport, final String codeParcelle,
                                final SiteACBB site) throws PersistenceException {
        final Parcelle dbParcelle = this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(codeParcelle, site)).orElse(null);
        if (dbParcelle == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB), codeParcelle, site
                    .getName()));
        }
        return dbParcelle;
    }

    /**
     * Retrieve db site.
     *
     * @param errorsReport
     * @param codeSite
     * @return the site acbb
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     */
    SiteACBB retrieveDBSite(final ErrorsReport errorsReport, final String codeSite)
            throws PersistenceException {
        final SiteACBB dbSite = this.siteDAO.getByPath(codeSite)
                .map(s -> (SiteACBB) s)
                .orElse(null);
        if (dbSite == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), codeSite));
        }
        return dbSite;
    }

    /**
     * Retrieve db traitement.
     *
     * @param errorsReport
     * @param codeTraitement
     * @param site
     * @return the traitement programme
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code traitement
     * @link(SiteACBB) the site
     * @link(ErrorsReport) the errors report
     * @link(String) the code traitement
     * @link(SiteACBB) the site
     * @link(ErrorsReport) the errors report
     * @link(String) the code traitement
     * @link(SiteACBB) the site
     */
    TraitementProgramme retrieveDBTraitement(final ErrorsReport errorsReport,
                                             final String codeTraitement, final SiteACBB site) throws PersistenceException {
        final TraitementProgramme dbTraitement = this.traitementDAO.getByNKey(site.getId(),
                codeTraitement).orElse(null);
        if (dbTraitement == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB), codeTraitement,
                    site.getName()));
        }
        return dbTraitement;
    }

    /**
     * Sets the parcelle dao.
     *
     * @param parcelleDAO the new parcelle dao
     */
    public final void setParcelleDAO(final IParcelleDAO parcelleDAO) {
        this.parcelleDAO = parcelleDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Sets the suivi parcelle dao.
     *
     * @param suiviParcelleDAO the new suivi parcelle dao
     */
    public final void setSuiviParcelleDAO(final ISuiviParcelleDAO suiviParcelleDAO) {
        this.suiviParcelleDAO = suiviParcelleDAO;
    }

    /**
     * Sets the traitement dao.
     *
     * @param traitementDAO the new traitement dao
     */
    public final void setTraitementDAO(final ITraitementDAO traitementDAO) {
        this.traitementDAO = traitementDAO;
    }

    /**
     * Test is valid date for treatment.
     *
     * @param errorsReport
     * @param dateDebutTraitement
     * @param dbTraitement
     * @return true, if successful
     * @link(AbstractCSVMetadataRecorder<SuiviParcelle>.ErrorsReport) the errors report
     * @link(Date) the date debut traitement
     * @link(TraitementProgramme) the db traitement
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut traitement
     * @link(TraitementProgramme) the db traitement
     */
    boolean testIsValidDateForTreatment(
            final AbstractCSVMetadataRecorder<SuiviParcelle>.ErrorsReport errorsReport,
            final LocalDate dateDebutTraitement, final TraitementProgramme dbTraitement) {
        if (dbTraitement == null || dateDebutTraitement == null) {
            return false;
        }
        boolean valid = true;
        final String expectedDateDebutDeTraitement;
        final String expectedDateFinDeTraitement;
        final String dateDebutTraitementString;
        dateDebutTraitementString = DateUtil.getUTCDateTextFromLocalDateTime(dateDebutTraitement, DateUtil.DD_MM_YYYY_FILE);
        if (dateDebutTraitement.isBefore(dbTraitement.getDateDebutTraitement())) {
            expectedDateDebutDeTraitement = DateUtil.getUTCDateTextFromLocalDateTime(dbTraitement.getDateDebutTraitement(), DateUtil.DD_MM_YYYY_FILE);
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_START_TREATMENT),
                    dateDebutTraitementString, expectedDateDebutDeTraitement));
            valid = false;
        }
        if (dbTraitement.getDateFinTraitement() != null
                && dateDebutTraitement.isAfter(dbTraitement.getDateDebutTraitement())) {
            expectedDateFinDeTraitement = DateUtil.getUTCDateTextFromLocalDateTime(dbTraitement.getDateFinTraitement(), DateUtil.DD_MM_YYYY_FILE);
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_INVALIDE_BEGIN_DATE_WITH_END_TREATMENT),
                    dateDebutTraitementString, expectedDateFinDeTraitement));
            valid = false;
        }
        return valid;
    }
}
