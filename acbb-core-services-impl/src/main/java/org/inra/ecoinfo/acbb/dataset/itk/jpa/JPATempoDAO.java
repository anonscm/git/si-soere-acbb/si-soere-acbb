package org.inra.ecoinfo.acbb.dataset.itk.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.*;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPATempoDAO.
 */
public class JPATempoDAO extends AbstractJPADAO<Tempo> implements ITempoDAO {

    private void deleteUnusedRotations() {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<VersionTraitementRealisee> vdt = query.from(VersionTraitementRealisee.class);
        Subquery<Long> linkedVersion = query.subquery(Long.class);
        Root<Tempo> tempo = linkedVersion.from(Tempo.class);
        linkedVersion.select(tempo.join(Tempo_.versionTraitementRealisee).get(VersionTraitementRealisee_.id));
        query.where(vdt.in(linkedVersion).not());
        final Path<Long> vdtId = vdt.get(VersionTraitementRealisee_.id);
        query.select(vdtId);
        List<Long> unlinkedVersions = getResultList(query);
        if (!unlinkedVersions.isEmpty()) {
            deleteYears(unlinkedVersions);
            CriteriaDelete<VersionTraitementRealisee> delete = builder.createCriteriaDelete(VersionTraitementRealisee.class);
            vdt = delete.from(VersionTraitementRealisee.class);
            delete.where(vdtId.in(unlinkedVersions));
            delete(delete);
        }
    }

    private void deleteYears(List<Long> unlinkedVersions) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<AnneeRealisee> year = query.from(AnneeRealisee.class);
        query.where(year.join(AnneeRealisee_.versionTraitementRealise).get(VersionTraitementRealisee_.id).in(unlinkedVersions));
        query.select(year.get(AnneeRealisee_.id));
        List<Long> unLinkedYear = getResultList(query);
        if (!unLinkedYear.isEmpty()) {
            delete(PeriodeRealisee.class,
                    PeriodeRealisee_.anneeRealisee, AnneeRealisee_.id, unLinkedYear);
            delete(AnneeRealisee.class,
                    AnneeRealisee_.versionTraitementRealise, VersionTraitementRealisee_.id, unlinkedVersions);
        }
    }

    private <T, U> void delete(Class<U> clazz, SingularAttribute<U, T> attribute, SingularAttribute<T, Long> id, List<Long> ids) {
        CriteriaDelete<U> delete = builder.createCriteriaDelete(clazz);
        Root<U> u = delete.from(clazz);
        delete.where(u.get(attribute).get(id).in(ids));
        delete(delete);
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void deleteUnusedTempos() throws PersistenceException {
        flush();
        CriteriaDelete<Tempo> delete = builder.createCriteriaDelete(Tempo.class
        );
        Root<Tempo> tempo = delete.from(Tempo.class
        );
        Subquery<Long> subquery = delete.subquery(Long.class
        );
        Root<AbstractIntervention> intervention = subquery.from(AbstractIntervention.class
        );
        Join<AbstractIntervention, Tempo> linkedTempo = intervention.join(AbstractIntervention_.tempo);
        subquery.where(builder.isNotNull(intervention.get(AbstractIntervention_.tempo)));
        subquery.select(linkedTempo.get(Tempo_.id));
        delete.where(tempo.get(Tempo_.id).in(subquery).not());
        delete(delete);
        this.flush();
        this.deleteUnusedRotations();
    }

    /**
     * @param versionDeTraitement
     * @param anneeNumber
     * @param rotationNumber
     * @param parcelle
     * @param periodeNumber
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO#getByNKey(org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement,
     * int, int, int)
     */
    @Override
    public Optional<Tempo> getByNKey(final VersionDeTraitement versionDeTraitement, final Parcelle parcelle,
                                     final int rotationNumber, final int anneeNumber, final int periodeNumber) {
        CriteriaQuery<Tempo> query = builder.createQuery(Tempo.class);
        Root<Tempo> tempo = query.from(Tempo.class);
        Join<Tempo, VersionDeTraitement> vdt = tempo.join(Tempo_.versionDeTraitement);
        Join<Tempo, Parcelle> par = tempo.join(Tempo_.parcelle);
        Join<Tempo, VersionTraitementRealisee> vtra = tempo.join(Tempo_.versionTraitementRealisee);
        Join<Tempo, AnneeRealisee> ar = tempo.join(Tempo_.anneeRealisee);
        Join<Tempo, PeriodeRealisee> pr = tempo.join(Tempo_.periodeRealisee);
        query.where(
                builder.equal(vdt, versionDeTraitement),
                builder.equal(par, parcelle),
                builder.equal(vtra.get(VersionTraitementRealisee_.numero), rotationNumber),
                builder.equal(ar.get(AnneeRealisee_.annee), anneeNumber),
                builder.equal(pr.get(PeriodeRealisee_.numero), periodeNumber)
        );
        query.select(tempo);
//        getSession().setFlushMode(FlushModeType.COMMIT);
        return getOptional(query);
    }

    /**
     * @param tempo
     * @param intervention
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO#saveOrUpdate(org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo,
     * org.inra.ecoinfo.acbb.dataset.itk.entity.Intervention, boolean, boolean,
     * boolean)
     */
    @Override
    public void saveOrUpdate(final Tempo tempo, final AbstractIntervention intervention)
            throws PersistenceException {
        assert intervention != null : "l'intervention ne doit pas être nulle";
        this.saveOrUpdateGeneric(intervention);
        assert tempo != null : "tempo ne doit pas être null";
        super.saveOrUpdate(tempo);

    }

}
