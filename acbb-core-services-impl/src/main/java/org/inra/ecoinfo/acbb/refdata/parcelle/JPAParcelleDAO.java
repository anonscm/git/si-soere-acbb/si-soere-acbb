/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.parcelle;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.refdata.site.Site;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAParcelleDAO.
 */
public class JPAParcelleDAO extends AbstractJPADAO<Parcelle> implements IParcelleDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO#getAll()
     */
    @Override
    public List<Parcelle> getAll() {
        return getAll(Parcelle.class);
    }

    /**
     * Gets the all sites with parcelle defined.
     *
     * @return the all sites with parcelle defined
     * @see org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO#getAllSitesWithParcelleDefined()
     */
    @Override
    public List<SiteACBB> getAllSitesWithParcelleDefined() {
        CriteriaQuery<SiteACBB> query = builder.createQuery(SiteACBB.class);
        Root<Parcelle> par = query.from(Parcelle.class);
        query.select(par.join(Parcelle_.site));
        return getResultList(query);
    }

    /**
     * Gets the by n key.
     *
     * @param code
     * @return the by n key
     * @link(Long) the site id
     * @link(String) the code
     * @link(Long) the site id
     * @link(String) the code
     * @see org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO#getByNKey(java.lang .Long,
     * java.lang.String)
     */
    @Override
    public Optional<Parcelle> getByNKey(final String code) {
        CriteriaQuery<Parcelle> query = builder.createQuery(Parcelle.class);
        Root<Parcelle> par = query.from(Parcelle.class);
        query.where(builder.equal(par.get(Nodeable_.code), code));
        query.select(par);
        return getOptional(query);
    }

    /**
     * Gets the by site.
     *
     * @param site
     * @return the by site
     * @link(Site) the site
     * @link(Site) the site
     * @see org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO#getBySite(org.inra
     * .ecoinfo.dataset.refdata.site.Site)
     */
    @Override
    public List<Parcelle> getBySite(final Site site) {
        CriteriaQuery<Parcelle> query = builder.createQuery(Parcelle.class);
        Root<Parcelle> par = query.from(Parcelle.class);
        query.where(builder.equal(par.join(Parcelle_.site), site));
        query.select(par);
        return getResultList(query);
    }
}
