package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultAnneeRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Map;
import java.util.Optional;

/**
 * A factory for creating SemisAnneeRealisee objects.
 */
public class SemisAnneeRealiseeFactory extends DefaultAnneeRealiseeFactory<LineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.impl.messages";
    /**
     * The Constant PROPERTY_MSG_INFO_MISMACK_COVER @link(String).
     */
    static final String PROPERTY_MSG_INFO_MISMACK_COVER = "PROPERTY_MSG_INFO_MISMACK_COVER";
    /**
     * The Constant PROPERTY_MSG_ERROR_MISSING_REAL_TREATMENT_VERSION
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_MISSING_REAL_TREATMENT_VERSION = "PROPERTY_MSG_ERROR_MISSING_REAL_TREATMENT_VERSION";
    /**
     * The Constant PROPERTY_MSG_ERROR_EXISTING_YEAR @link(String).
     */
    static final String PROPERTY_MSG_ERROR_EXISTING_YEAR = "PROPERTY_MSG_ERROR_EXISTING_YEAR";
    /**
     * The Constant PROPERTY_MSG_BAD_YEAR @link(String).
     */
    static final String PROPERTY_MSG_BAD_YEAR = "PROPERTY_MSG_BAD_YEAR";
    /**
     * The Constant PROPERTY_MSG_MISSING_MAIN_CROP @link(String).
     */
    static final String PROPERTY_MSG_MISSING_MAIN_CROP = "PROPERTY_MSG_MISSING_MAIN_CROP";
    /**
     * The Constant PROPERTY_MSG_UNEXPECTED_COVER @link(String).
     */
    static final String PROPERTY_MSG_UNEXPECTED_COVER = "PROPERTY_MSG_UNEXPECTED_COVER";
    /**
     * The Constant PROPERTY_MSG_ERROR_UPDATE_PREVIOUS_YEAR @link(String).
     */
    static final String PROPERTY_MSG_ERROR_UPDATE_PREVIOUS_YEAR = "PROPERTY_MSG_ERROR_UPDATE_PREVIOUS_YEAR";
    /**
     * The Constant PROPERTY_MSG_BAD_YEAR_INFINITE @link(String).
     */
    static final String PROPERTY_MSG_BAD_YEAR_INFINITE = "PROPERTY_MSG_BAD_YEAR_INFINITE";
    private static final Logger LOGGER = LoggerFactory.getLogger(SemisAnneeRealiseeFactory.class
            .getName());

    private String addCouvertsForMessage(String bindedsMessages, AnneeRealisee anneeRealisee) {
        StringBuilder localMessage = new StringBuilder(bindedsMessages);
        if (anneeRealisee.getVersionTraitementRealise() == null
                || anneeRealisee.getVersionTraitementRealise().getAnneesRealisees() == null) {
            return bindedsMessages;
        }
        for (Map.Entry<Integer, AnneeRealisee> anneeRealiseeEntry : anneeRealisee
                .getVersionTraitementRealise().getAnneesRealisees().entrySet()) {
            localMessage = localMessage.append("\nannée ").append(anneeRealiseeEntry.getKey())
                    .append(" -> ")
                    .append(anneeRealiseeEntry.getValue().getCouvertVegetal().getValeur());
        }
        return localMessage.toString();
    }

    /**
     * Alert non expected couvert.
     *
     * @param line
     * @param infoReport
     * @link(LineRecord) the line
     * @link(InfoReport) the info report
     */
    void alertNonExpectedCouvert(final LineRecord line, final InfoReport infoReport) {
        infoReport.addInfoMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                SemisAnneeRealiseeFactory.BUNDLE_NAME,
                SemisAnneeRealiseeFactory.PROPERTY_MSG_INFO_MISMACK_COVER), line
                        .getOriginalLineNumber(), this.getInternationalizedValeurQualitative(line
                        .getCouvertVegetal().getCode()), line.getAnneeNumber(), line.getRotationNumber(),
                line.getTreatmentVersion(), line.getTreatmentAffichage(), this
                        .getInternationalizedValeurQualitative(line.getSemisRotationDescriptor()
                                .getExpectedCouvert().getCode())));
    }

    /**
     * @param line
     * @param infoReport
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultAnneeRealiseeFactory#getOrCreateAnneeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public AnneeRealisee getOrCreateAnneeRealisee(final LineRecord line,
                                                  final ErrorsReport errorsReport, final InfoReport infoReport) {
        assert line.getTempo().getVersionTraitementRealisee() != null : "la version de traitement ne doit pas être nulle";
        AnneeRealisee anneeRealisee = line.getTempo().getVersionTraitementRealisee()
                .getAnneesRealisees().get(line.getAnneeNumber());
        final RotationDescriptor rotation = line.getSemisRotationDescriptor();
        if (!line.isGoodYearForRotation()) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisAnneeRealiseeFactory.BUNDLE_NAME,
                    SemisAnneeRealiseeFactory.PROPERTY_MSG_BAD_YEAR), line.getOriginalLineNumber(),
                    line.getAnneeNumber(), line.getRotationNumber(), line.getTreatmentVersion(),
                    line.getTreatmentAffichage(), rotation.getCouverts() != null ? rotation
                            .getCouverts().size() : 1));
            return anneeRealisee;
        } else if (!line.isGoodYearForTemporaryGrassland()) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisAnneeRealiseeFactory.BUNDLE_NAME,
                    SemisAnneeRealiseeFactory.PROPERTY_MSG_BAD_YEAR_INFINITE), line
                    .getOriginalLineNumber(), line.getAnneeNumber(), line.getRotationNumber(), line
                    .getTreatmentVersion(), line.getTreatmentAffichage()));
            return anneeRealisee;
        }
        assert line.isPositiveYear() : "le numéro de l'année doit être positif";
        assert !(line.isRotation()
                && !line.getSemisRotationDescriptor().getRotation().isInfiniteRepeatedCouvert() && line
                .getAnneeNumber() > line.getSemisRotationDescriptor().getCouverts().size()) : "le numéro de l'année doit être inférieure à la durée d'une rotation";
        if (anneeRealisee == null) {
            if (!line.isReplanting()) {
                anneeRealisee = new AnneeRealisee(line.getAnneeNumber(), line.getTempo()
                        .getVersionTraitementRealisee());
                if (line.isRotation() && line.isAnnuelle()) {
                    this.periodeRealiseeFactory.createDefaultPeriode(anneeRealisee, errorsReport);
                }
            } else {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        SemisAnneeRealiseeFactory.BUNDLE_NAME,
                        SemisAnneeRealiseeFactory.PROPERTY_MSG_MISSING_MAIN_CROP), line
                                .getOriginalLineNumber(), line.getAnneeNumber(), line.getRotationNumber(),
                        line.getVersion(), line.getTreatmentVersion()));
                return anneeRealisee;
            }
        } else if (!line.isReplanting() && line.getPeriodeNumber() == 0) {
            String bindedsMessages = ACBBUtils.getBindedsMessages(anneeRealisee.getBindedsITK());
            bindedsMessages = this.addCouvertsForMessage(bindedsMessages, anneeRealisee);
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisAnneeRealiseeFactory.BUNDLE_NAME,
                    SemisAnneeRealiseeFactory.PROPERTY_MSG_ERROR_EXISTING_YEAR), line
                    .getOriginalLineNumber(), line.getAnneeNumber(), line.getRotationNumber(), line
                    .getTreatmentVersion(), line.getTreatmentAffichage(), bindedsMessages));
            return anneeRealisee;
        }
        if (rotation.isPossibleCouvert() && !line.isReplanting()) {
            this.updateAnneeForRotation(line, anneeRealisee, errorsReport, infoReport);
        } else if (!rotation.isPossibleCouvert()) {
            final String couvert = line.getCouvertVegetal().getValeur();
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisAnneeRealiseeFactory.BUNDLE_NAME,
                    SemisAnneeRealiseeFactory.PROPERTY_MSG_UNEXPECTED_COVER), line
                            .getOriginalLineNumber(), this.getInternationalizedValeurQualitative(couvert),
                    line.getAnneeNumber(), line.getRotationNumber(), line.getTreatmentVersion(),
                    line.getTreatmentAffichage(), line.getSemisRotationDescriptor()
                            .getExpectedCouvertList()));
            return anneeRealisee;
        }
        try {
            this.anneeRealiseeDAO.saveOrUpdate(anneeRealisee);
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return anneeRealisee;
    }

    /**
     * Gets the previous rotation.
     *
     * @param versionTraitementRealise
     * @return the previous rotation
     * @link(VersionTraitementRealisee) the version traitement realise
     */
    VersionTraitementRealisee getPreviousRotation(
            final VersionTraitementRealisee versionTraitementRealise) {
        assert versionTraitementRealise.getNumero() > 1 : "la rotation n'est pas la première";
        return versionTraitementRealise.getVersionDeTraitement().getVersionsDeTraitementRealisees()
                .get(versionTraitementRealise.getNumero() - 1);
    }

    /**
     * Gets the previous year.
     *
     * @param anneeRealisee
     * @return the previous year
     * @link(AnneeRealisee) the annee realisee
     */
    Optional<AnneeRealisee> getPreviousYear(final AnneeRealisee anneeRealisee) {
        if (anneeRealisee.getAnnee() > 1) {
            return this.anneeRealiseeDAO.getByNKey(
                    anneeRealisee.getVersionTraitementRealise(), anneeRealisee.getAnnee() - 1);
        } else if (anneeRealisee.getVersionTraitementRealise().getNumero() > 1) {
            final VersionTraitementRealisee previousRotation = this
                    .getPreviousRotation(anneeRealisee.getVersionTraitementRealise());
            return Optional.ofNullable(previousRotation)
                    .map(p -> p.getAnneesRealisees().get(p.getAnneesRealisees().size()));
        }
        return Optional.empty();
    }

    /**
     * Gets the begin date for temporal meadows.
     *
     * @param line
     * @param anneeRealisee
     * @param errorsReport
     * @return the begin date for temporal meadows
     * @link(LineRecord) the line
     * @link(AnneeRealisee) the annee realisee
     * @link(ErrorsReport) the errors report
     */
    void setBeginAndEndDateForTemporalMeadows(final LineRecord line,
                                              final AnneeRealisee anneeRealisee, final ErrorsReport errorsReport) {
        assert !line.isAnnuelle() : "la culture ne doit pas être annuelle";
        final AnneeRealisee previousYear = this.getPreviousYear(anneeRealisee).orElse(null);
        if (previousYear != null && previousYear.getCouvertVegetal().equals(line.getCouvertVegetal())) {
            LocalDate date = previousYear.getDateDeFin();
            LocalDate firstDay = date.plus(3, ChronoUnit.MONTHS).with(TemporalAdjusters.firstDayOfYear());
            LocalDate lastDay = firstDay.with(TemporalAdjusters.firstDayOfNextYear());
            anneeRealisee.setDateDeDebut(firstDay);
            anneeRealisee.setDateDeFin(lastDay);
        } else {
            this.setLastYearDayDateForMeadows(anneeRealisee, line.getDate());
            anneeRealisee.setDateDeDebut(line.getDate());
        }
    }

    /**
     * Gets the begin date.
     *
     * @param line
     * @param anneeRealisee
     * @param errorsReport
     * @link(LineRecord) the line
     * @link(AnneeRealisee) the annee realisee
     * @link(ErrorsReport)
     */
    protected void setBeginDate(final LineRecord line, final AnneeRealisee anneeRealisee,
                                final ErrorsReport errorsReport) {
        if (line.isAnnuelle()) {
            anneeRealisee.setDateDeDebut(line.getDate());
        } else {
            this.setBeginAndEndDateForTemporalMeadows(line, anneeRealisee, errorsReport);
        }
        this.updatePreviousYear(line, anneeRealisee, errorsReport);
    }

    /**
     * First year day date for meadows.
     *
     * @param versionTraitementRealisee
     * @param annee
     * @return the date
     * @link(VersionTraitementRealisee) the version traitement realisee
     * @link(int) the annee
     */
    void setLastYearDayDateForMeadows(final AnneeRealisee anneeRealisee, LocalDate date) {
        LocalDate lastDay = date.plus(1, ChronoUnit.MONTHS).with(TemporalAdjusters.lastDayOfYear());
        anneeRealisee.setDateDeFin(lastDay);
    }

    /**
     * Update annee for rotation.
     *
     * @param line
     * @param anneeRealisee
     * @param errorsReport
     * @param infoReport
     * @link(LineRecord) the line
     * @link(AnneeRealisee) the annee realisee
     * @link(ErrorsReport) the errors report
     * @link(InfoReport) the info report
     */
    void updateAnneeForRotation(final LineRecord line, final AnneeRealisee anneeRealisee,
                                final ErrorsReport errorsReport, final InfoReport infoReport) {
        this.setBeginDate(line, anneeRealisee, errorsReport);
        anneeRealisee.setCouvertVegetal(line.getCouvertVegetal());
    }

    /**
     * Update annee realisee.
     *
     * @param anneeRealisee
     * @throws PersistenceException the persistence exception
     * @link(AnneeRealisee) the annee realisee
     */
    public void updateAnneeRealisee(final AnneeRealisee anneeRealisee) throws PersistenceException {
        this.anneeRealiseeDAO.saveOrUpdate(anneeRealisee);
    }

    /**
     * Update annee realisee.
     *
     * @param anneeRealisee
     * @param line
     * @param errorsReport
     * @throws PersistenceException the persistence exception
     * @link(AnneeRealisee) the annee realisee
     * @link(LineRecord) the line
     * @link(ErrorsReport) the errors report
     */
    public void updateAnneeRealisee(final AnneeRealisee anneeRealisee, final LineRecord line,
                                    final ErrorsReport errorsReport) throws PersistenceException {
        anneeRealisee.setCouvertVegetal(line.getCouvertVegetal());
        this.setBeginDate(line, anneeRealisee, errorsReport);
        this.updateAnneeRealisee(anneeRealisee);
    }

    /**
     * Update end date.
     *
     * @param endDate
     * @param anneeRealisee
     * @link(Date) the end date
     * @link(AnneeRealisee) the annee realisee
     */
    void updateEndDate(final LocalDate endDate, final AnneeRealisee anneeRealisee) {
        if (anneeRealisee != null) {
            anneeRealisee.setDateDeFin(endDate);
        }
    }

    /**
     * Update previous year.
     *
     * @param line
     * @param anneeRealisee
     * @param errorsReport
     * @link(LineRecord) the line
     * @link(AnneeRealisee) the annee realisee
     * @link(ErrorsReport) the errors report
     */
    void updatePreviousYear(final LineRecord line, final AnneeRealisee anneeRealisee,
                            final ErrorsReport errorsReport) {
        AnneeRealisee previousAnneeRealisee = null;
        previousAnneeRealisee = this.getPreviousYear(anneeRealisee).orElse(null);
        this.updateEndDate(anneeRealisee.getDateDeDebut(), previousAnneeRealisee);
        try {
            if (previousAnneeRealisee != null) {
                this.anneeRealiseeDAO.saveOrUpdate(previousAnneeRealisee);
            }
        } catch (final PersistenceException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisAnneeRealiseeFactory.BUNDLE_NAME,
                    SemisAnneeRealiseeFactory.PROPERTY_MSG_ERROR_UPDATE_PREVIOUS_YEAR), line
                    .getOriginalLineNumber(), line.getAnneeNumber(), line.getRotationNumber(), line
                    .getTreatmentVersion(), line.getTreatmentAffichage()));
        }
    }
}
