package org.inra.ecoinfo.acbb.extraction.itk.jpa;

import org.hibernate.query.spi.NativeQueryImplementor;
import org.hibernate.query.spi.QueryProducerImplementor;
import org.hibernate.type.LocalDateType;
import org.hibernate.type.StringType;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.*;

/**
 * @author ptcherniati
 */
public class JPAITKExtractionDAO extends AbstractJPADAO<AbstractIntervention> implements
        IITKExtractionDAO {

    /*
     * return Plot, treatment, sowingDate, harvest date, vegetal cover select {par.*}, {tra.*},
     * datededebut datesemis, datedefin daterecolte, couvert
     */
    static final String SQL_GET_COUVERT = "with 	"
            + "	/*semis*/"
            + "		sem as ("
            + "			select * from semis_sem"
            + "		),"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "	/*couverts végétal des cultures temporaires principales*/ "
            + "		ct as ( "
            + "			select distinct spa_id, tra_id, spa.par_id, preal_id, areal.datededebut, areal.datedefin, vq.valeur as sem_couvert_vegetal"
            + "			from semis_sem "
            + "			join suivi_parcelle_spa spa using(spa_id) "
            + "			join tempo_temp temp using(tempo_id) "
            + "			left join annee_realisee_areal areal using(areal_id)"
            + "			left join valeur_qualitative vq on sem_couvert_vegetal=vq.vq_id"
            + "			order by spa_id, areal.datededebut"
            + "		), "
            + "	/*définition des utilisations pour l'ensemble des parcelles*/"
            + "		selection as ( "
            + "			select distinct vtra_id, tra_id, par_id, mod.code "
            + "			from suivi_parcelle_spa spa "
            + "			join traitement_tra tra using(tra_id) "
            + "			join version_traitement_vtra vtra using(tra_id) "
            + "			join version_traitement_modalite_vtmod using(vtra_id) "
            + "			join modalite_mod mod using(mod_id)"
            + "		), "
            + "	"
            + "	/*définition des prairies permanentes*/ "
            + "		pp as ( "
            + "			select distinct vtra_id, tra_id, par_id "
            + "			from selection "
            + "			where vtra_id not in ("
            + "				select vtra_id "
            + "				from selection "
            + "				where code similar to '(U[0A]|R%)'"
            + "			) "
            + "		), "
            + "	"
            + "	/*définition des sols nus*/ "
            + "		sn as ("
            + "			select distinct vtra_id, tra_id, par_id "
            + "			from selection where code ='U0'"
            + "		), "
            + "		"
            + "	/*définition des abandons*/ "
            + org.apache.commons.lang.StringUtils.EMPTY
            + "		ab as ("
            + "			select distinct vtra_id, tra_id, par_id "
            + "			from selection where code ='UA'"
            + "		), "
            + "	"
            + "	/*récoltes*/ "
            + "		recolte as ( "
            + "			select spa_id, spa.tra_id, treal.par_id, rec_fau.date "
            + "			from recolte_et_fauche_recfau rec_fau"
            + "			join tempo_temp using(tempo_id) "
            + "			join version_traitement_realisee_treal treal using(treal_id) "
            + "			join version_traitement_vtra vtra on vtra.vtra_id = treal.vtra_id "
            + "			join suivi_parcelle_spa spa using(spa_id) "
            + "			where spa.tra_id not in (select tra_id from pp) "
            + "			order by spa_id, date"
            + "		), "
            + org.apache.commons.lang.StringUtils.EMPTY
            + "	/*semis*/ "
            + "		datedebutcouverts as ( "
            + "			select spa_id, spa.tra_id, spa.par_id, datededebut, couvert from("
            + "			"
            + "				/* cas des cultures temporaires principales*/"
            + "				 select spa_id, datededebut, sem_couvert_vegetal as couvert"
            + "				 from ct "
            + "				 "
            + "				 union "
            + "				 "
            + "				/* cas des cultures temporaires secondaires*/"
            + "				 select spa_id , preal.datededebut, vq.valeur as couvert"
            + "				 from ct "
            + "				 join periode_realisee_preal preal using(preal_id) "
            + "				 join valeur_qualitative vq on preal.sem_couvert_vegetal = vq.vq_id "
            + "				 where preal.sem_couvert_vegetal is not null "
            + "				 "
            + "				 union "
            + org.apache.commons.lang.StringUtils.EMPTY
            + "				 /*cas des prairies permanentes*/"
            + "				 select distinct spa_id, tra.datedebuttraitement,'PP' as couvert"
            + "				 from pp "
            + "				 join suivi_parcelle_spa using(tra_id)"
            + "				 join traitement_tra tra using(tra_id) /* la date de debut de prairie est celle de début du traitement */"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "				 union"
            + "				 "
            + "				 /*cas des prairies permanente fauchées*/"
            + "				 select spa_id, date, 'PP' from recolte"
            + "				 where par_id in (select par_id from pp) /*la date de début est la date de fauche*/"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "				 union "
            + org.apache.commons.lang.StringUtils.EMPTY
            + "				 select spa_id, null,'S0' from sn as couvert"
            + "				 join suivi_parcelle_spa using(tra_id) "
            + org.apache.commons.lang.StringUtils.EMPTY
            + "				 union "
            + org.apache.commons.lang.StringUtils.EMPTY
            + "				 select spa_id, null,'SA' from ab as couvert"
            + "				 join suivi_parcelle_spa using(tra_id) ) as req"
            + "				 join suivi_parcelle_spa spa using(spa_id)"
            + "		), "
            + "			"
            + "	/*couverts pour cultures temporaires*/ "
            + "		couverts as ( "
            + "			select spa_id, tra_id, par_id, couvert, datededebut, min(date) datedefin from (		 "
            + "				select distinct spa_id, tra_id, par_id, couvert, datededebut, date  "
            + "				from datedebutcouverts  "
            + "				left join recolte using(spa_id, tra_id, par_id)"
            + "				where datededebut < date and couvert != 'PT') as req1"
            + "			group by spa_id, tra_id, par_id, couvert, datededebut"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "			union"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "			select spa_id, spa.tra_id, spa.par_id, 'PT' couvert, min(datededebut) datededebut, max(datedefin)  datedefin from annee_realisee_areal "
            + "			join tempo_temp using(treal_id)"
            + "			join semis_sem using(tempo_id)"
            + "			join suivi_parcelle_spa spa using(spa_id)"
            + "			left join valeur_qualitative vq on vq_id = sem_couvert_vegetal"
            + "			group by valeur, spa_id, spa.tra_id, spa.par_id"
            + "			having valeur='PT'"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "			union"
            + org.apache.commons.lang.StringUtils.EMPTY
            + "			select debut.spa_id, debut.tra_id, debut.par_id, 'PP' couvert, debut.datededebut, min(fin.datededebut)  datedefin from datedebutcouverts debut"
            + "			left join datedebutcouverts fin on debut.spa_id = fin.spa_id and debut.datededebut < fin.datededebut"
            + "			group by  debut.spa_id, debut.tra_id, debut.par_id, debut.datededebut, debut.couvert"
            + "			having debut.couvert = 'PP'"
            + "	"
            + "		)"
            + "		select {par.*}, {tra.*}, datededebut datesemis, datedefin daterecolte, couvert "
            + "		from couverts  "
            + "		join traitement_tra tra using(tra_id)  "
            + "		join parcelle_par par using(par_id)  "
            + "		join composite_nodeable as par_1_ on par_1_.id=par.id "
            + "		order by par_id, datesemis";
    private static final Logger LOGGER = LoggerFactory.getLogger(JPAITKExtractionDAO.class
            .getName());

    /**
     * @param selectedTraitementProgrammes
     * @return
     */
    @Override
    public Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> extractCouverts(
            List<TraitementProgramme> selectedTraitementProgrammes) {
        NativeQueryImplementor query = ((QueryProducerImplementor) this.entityManager.getDelegate())
                .createSQLQuery(JPAITKExtractionDAO.SQL_GET_COUVERT);
        query
                .addEntity("par", Parcelle.class)
                .addEntity("tra", TraitementProgramme.class)
                .addScalar("datesemis", LocalDateType.INSTANCE)
                .addScalar("daterecolte", LocalDateType.INSTANCE)
                .addScalar("couvert", StringType.INSTANCE);
        @SuppressWarnings("unchecked")
        List<Object[]> results = query.list();
        Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> returnList = new HashMap();
        for (Object[] object : results) {
            Parcelle parcelle = (Parcelle) object[0];
            if (!returnList.containsKey(parcelle)) {
                returnList.put(parcelle, new TreeMap<>());
            }
            LocalDate datedebut = (LocalDate) (object[2] == null ? DateUtil.MIN_LOCAL_DATE : object[2]);
            returnList.get(parcelle).put(datedebut, new VegetalPeriode(object));
        }
        return returnList;
    }
}
