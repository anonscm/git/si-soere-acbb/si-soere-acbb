/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractVariableValueBiomasse;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

/**
 * @author ptcherniati
 */
public class VariableValueBiomassProduction extends AbstractVariableValueBiomasse {

    MasseVegClass masseVegClass;

    /**
     * @param measureNumber
     * @param standardDeviation
     * @param masseVegClass
     * @param qualityIndex
     * @param methode
     * @param dvu
     * @param value
     */
    public VariableValueBiomassProduction(int measureNumber, float standardDeviation,
                                          MasseVegClass masseVegClass, int qualityIndex, AbstractMethode methode,
                                          DatatypeVariableUniteACBB dvu, String value) {
        super(dvu, value, methode, qualityIndex, measureNumber, standardDeviation, qualityIndex);
        this.masseVegClass = masseVegClass;
    }

    /**
     * @return
     */
    public MasseVegClass getMasseVegClass() {
        return this.masseVegClass;
    }

    /**
     * @param masseVegClass
     */
    public void setMasseVegClass(MasseVegClass masseVegClass) {
        this.masseVegClass = masseVegClass;
    }

}
