package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.*;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author tcehrniatinsky The Class LineRecord.
 */
public class LineRecord extends AbstractLineRecord<LineRecord, ListeItineraire> {

    /**
     * The attributes variables values
     *
     * @link(List<ACBBVariableValue<ListeItineraire>>).
     */
    List<ACBBVariableValue<ListeItineraire>> attributesVariablesValues = new LinkedList();
    /**
     * The semis rotation descriptor @link(RotationDescriptor).
     */
    private RotationDescriptor rotationDescriptor;
    /**
     * The couvert vegetal @link(SemisCouvertVegetal).
     */
    private SemisCouvertVegetal couvertVegetal;
    /**
     * The semis objectifs @link(List<SemisObjectif>).
     */
    private List<SemisObjectif> semisObjectifs;
    /**
     * The semis profondeur @link(Float).
     */
    private Float semisProfondeur;
    /**
     * The annuelle @link(Boolean).
     */
    private Boolean annuelle;
    /**
     * The semis espece @link(SemisEspece).
     */
    private SemisEspece semisEspece;
    /**
     * The semis variete @link(ListeItineraire).
     */
    private ListeItineraire semisVariete;

    /**
     * Instantiates a new line record.
     *
     * @param lineCount
     * @link(long) the line count
     */
    public LineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * @param line
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord#copy(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord)
     */
    @Override
    public void copy(final LineRecord line) {
        super.copy(line);
        this.setAttributesVariablesValues(line.getAttributesVariablesValues());

    }

    /**
     * Gets the variables values @link(List,VariableValue).
     *
     * @return the variables values @link(List,VariableValue)
     */
    public List<ACBBVariableValue<ListeItineraire>> getAttributesVariablesValues() {
        return this.attributesVariablesValues;
    }

    /**
     * Sets the variables values @link(List,VariableValue).
     *
     * @param attributesVariablesValues the new variables values
     * @link(List, VariableValue)
     */
    public void setAttributesVariablesValues(
            List<ACBBVariableValue<ListeItineraire>> attributesVariablesValues) {
        this.attributesVariablesValues = attributesVariablesValues;
    }

    /**
     * Gets the couvert vegetal.
     *
     * @return the couvert vegetal
     */
    public SemisCouvertVegetal getCouvertVegetal() {
        if (this.couvertVegetal != null) {
            return this.couvertVegetal;
        }
        for (ACBBVariableValue<ListeItineraire> attribut : this.getAttributesVariablesValues()) {
            if (attribut.isQualitative()
                    && attribut.getValeurQualitative() instanceof SemisCouvertVegetal) {
                this.couvertVegetal = (SemisCouvertVegetal) attribut
                        .getValeurQualitative();
                break;
            }
        }
        return this.couvertVegetal;
    }

    /**
     * Sets the couvert vegetal.
     *
     * @param couvertVegetal the new couvert vegetal
     */
    public final void setCouvertVegetal(final SemisCouvertVegetal couvertVegetal) {
        this.couvertVegetal = couvertVegetal;
    }

    /**
     * Gets the espece.
     *
     * @return the espece
     */
    public SemisEspece getEspece() {
        if (this.semisEspece != null) {
            return this.semisEspece;
        }
        for (ACBBVariableValue<? extends IValeurQualitative> attribut : this.getVariablesValues()) {
            if (attribut.isQualitative() && attribut.getValeurQualitative() instanceof SemisEspece) {
                this.semisEspece = (SemisEspece) attribut.getValeurQualitative();
                break;
            }
        }
        return this.semisEspece;
    }

    /**
     * Gets the objectifs.
     *
     * @return the objectifs
     */
    public List<SemisObjectif> getObjectifs() {
        if (this.semisObjectifs != null) {
            return this.semisObjectifs;
        }
        this.semisObjectifs = new LinkedList();
        for (ACBBVariableValue<ListeItineraire> attribut : this.getAttributesVariablesValues()) {
            if (attribut.isQualitative()
                    && attribut.getValeurQualitative() instanceof SemisObjectif) {
                this.semisObjectifs.add((SemisObjectif) attribut.getValeurQualitative());
            }
        }
        return this.semisObjectifs;
    }

    /**
     * Gets the profondeur.
     *
     * @return the profondeur
     */
    public Float getProfondeur() {
        if (this.semisProfondeur != null) {
            return this.semisProfondeur;
        }
        for (ACBBVariableValue<ListeItineraire> attribut : this.getAttributesVariablesValues()) {
            if (!attribut.isQualitative()
                    && Semis.ATTRIBUTE_JPA_PROFONDEUR.equals(attribut.getDatatypeVariableUnite().getVariable().getCode())) {
                this.semisProfondeur = Float.valueOf(attribut.getValue());
                break;
            }
        }
        return this.semisProfondeur;
    }

    /**
     * Gets the semis rotation descriptor. There's a semis rotation descriptor
     * only if there's a modality of rotation
     *
     * @return the semis rotation descriptor
     */
    public RotationDescriptor getSemisRotationDescriptor() {
        return this.rotationDescriptor;
    }

    /**
     * Gets the treatment affichage.
     *
     * @return the treatment affichage
     */
    public String getTreatmentAffichage() {
        String returnString = org.apache.commons.lang.StringUtils.EMPTY;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null
                && this.getTempo().getVersionDeTraitement().getTraitementProgramme() != null) {
            returnString = this.getTempo().getVersionDeTraitement().getTraitementProgramme()
                    .getAffichage();
        }
        return returnString;
    }

    /**
     * Gets the treatment version.
     *
     * @return the treatment version
     */
    public int getTreatmentVersion() {
        int returnInt = -1;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null) {
            returnInt = this.getTempo().getVersionDeTraitement().getVersion();
        }
        return returnInt;
    }

    /**
     * Gets the variete.
     *
     * @return the variete
     */
    public ListeItineraire getVariete() {
        if (this.semisVariete != null) {
            return this.semisVariete;
        }
        Collection<ListeItineraire> varietes = this.semisEspece.getVarietes().values();
        for (ACBBVariableValue<? extends IValeurQualitative> attribut : this.getVariablesValues()) {
            if (attribut.isQualitative() && varietes.contains(attribut.getValeurQualitative())) {
                this.semisVariete = (ListeItineraire) attribut.getValeurQualitative();
                break;
            }
        }
        return this.semisVariete;
    }

    /**
     * Checks if it's crop is annual.
     * <p>
     * this is an annual sowing an a crop
     * </p>
     *
     * @return true, if it is crop is annual.
     */
    public boolean isAnnualCrop() {
        return this.isRotation() && this.isAnnuelle();
    }

    /**
     * Return the value of the annual column of the line
     *
     * @return the boolean
     */
    public Boolean isAnnuelle() {
        if (this.annuelle != null) {
            return this.annuelle;
        }
        for (ACBBVariableValue<ListeItineraire> attribut : this.getAttributesVariablesValues()) {
            if (attribut.isQualitative()
                    && attribut.getValeurQualitative() instanceof SemisAnnuelle) {
                this.annuelle = Boolean.valueOf(attribut.getValeurQualitative().getValeur());
                break;
            }
        }
        return this.annuelle;
    }

    /**
     * Checks if it's permanent grassland by definition.
     * <p>
     * By definition a permanent grasslanf is not a rotation
     * </p>
     * <p>
     * in a plot there's a permanent grassland if there's no instance of
     * <b>Rotation</B> in the modality of the <b>treatment version</B>.
     *
     * @return true, if it's permanent grassland by definition
     */
    public boolean isDefinedPermanentGrassland() {
        return !this.isRotation();
    }

    /**
     * @return true, if it's first ley year
     * @pre isRotation() && !isAnnuelle() Checks if in the rotation this is the
     * first year of a temporary grassland.
     */
    public boolean isFirstLeyYear() {
        assert this.isRotation() && !this.isAnnuelle() : "this isn't a ley";
        boolean firstLeyYear = false;
        firstLeyYear = !this.getCouvertVegetal().equals(
                this.getSemisRotationDescriptor().getCouverts().get(this.getAnneeNumber() - 1));
        return firstLeyYear;
    }

    /**
     * Checks if it's good year for rotation.
     *
     * @return true, if it's good year for rotation
     */
    public boolean isGoodYearForRotation() {
        final boolean isTemporary = this.isTemporary();
        final boolean isGoodYearForRotation = isTemporary
                && this.getAnneeNumber() <= this.getSemisRotationDescriptor().getCouverts().size();
        return this.isPermanentGrassland() || isGoodYearForRotation;
    }

    /**
     * @return true, if it's good year for permanent grassland
     * @pre isPermanentGrassland() Checks if it's good year for permanent
     * grassland.
     * <p>
     * A good year is positive exept if this is a temporary grassland with
     * infinite</p>
     */
    public boolean isGoodYearForTemporaryGrassland() {
        return this.isRotation() || this.isPositiveYear();
    }

    /**
     * Checks if it's main crop.
     * <p>
     * In a rotation a sowing is a main crop is it's the first year of a non
     * replanting sowing.
     * </p>
     *
     * @return true, if it's main crop
     */
    public boolean isMainCrop() {
        return this.isRotation() && !this.isReplanting() && !this.isFirstLeyYear();
    }

    /**
     * Checks if it's a permanente grassland
     * <p>
     * a permanent grassland is either
     * </p>
     * <ul>
     * <li>a non rotation</li>
     * <li>a rotation with an infinite repeated cover</li>
     * </ul>
     *
     * @return true, if it's a permanente grassland
     */
    public boolean isPermanentGrassland() {
        return this.isDefinedPermanentGrassland()
                || this.getSemisRotationDescriptor().getRotation().isInfiniteRepeatedCouvert();
    }

    /**
     * Checks if the yaer number is positive.
     *
     * @return true, if it's positive year
     */
    public boolean isPositiveYear() {
        return this.getAnneeNumber() > 0;
    }

    /**
     * Checks if sowing is replanting.
     * <p>
     * sowing is replanting in a rotation
     * </p>
     * <ul>
     * <li>if sowing is annual and the period number is 1.</li>
     * <li><b>or </b> if sowing is not the first year of a temporary
     * grassland.</li>
     * </ul>
     *
     * @return true, if it's replanting
     */
    public boolean isReplanting() {
        final boolean isSecondPeriod = this.isAnnuelle() && this.getPeriodeNumber() == 1;
        final boolean isContinuityOfTemporryGrassland = !this.isAnnuelle()
                && !this.isFirstLeyYear();
        return this.isRotation() && (isSecondPeriod || isContinuityOfTemporryGrassland);
    }

    /**
     * Checks if there's a rotation on the plot.<br />
     * Thee's a rotation on the oarcelle only if there's an instance of
     * <b>Rotation</B> in the modality of the <b>treatment version</B>.
     *
     * @return true, if it's rotation
     */
    public boolean isRotation() {
        final RotationDescriptor semisRotationDescriptor = this.getSemisRotationDescriptor();
        return semisRotationDescriptor != null && semisRotationDescriptor.getRotation() != null;
    }

    /**
     * Checks if it's a temporary crop
     * <P>
     * a temporary crop is either
     * </p>
     * <ul>
     * <li>An annual crop in a rotation</li>
     * <li>A temporary grassland in a rotation</Li>
     * </ul>
     *
     * @return true, if it's a temporary crop
     */
    public boolean isTemporary() {
        return this.isAnnualCrop() || this.isTemporaryGrassland();
    }

    /**
     * Checks if it's a temporary grassland.
     * <p>
     * this is in a rotation a non annual crop that has no infinite repeated
     * cover
     * </p>
     *
     * @return true, if it is a temporary grassland.
     */
    public boolean isTemporaryGrassland() {
        return this.isRotation() && !this.isAnnuelle()
                && !this.getSemisRotationDescriptor().getRotation().isInfiniteRepeatedCouvert();
    }

    /**
     * Sets the annuelle.
     *
     * @param annuelle the new annuelle
     */
    public final void setAnnuelle(final boolean annuelle) {
        this.annuelle = annuelle;
    }

    /**
     * Sets the semis rotation descriptor.
     *
     * @param rotation the new semis rotation descriptor
     */
    public final void setRotationDescriptor(final RotationDescriptor rotation) {
        this.rotationDescriptor = rotation;
    }

}
