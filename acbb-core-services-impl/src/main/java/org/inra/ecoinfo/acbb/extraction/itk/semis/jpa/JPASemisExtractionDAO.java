/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.semis.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.*;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.semis.SynthesisValue;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPASemisDAO.
 */
@SuppressWarnings("unchecked")
public class JPASemisExtractionDAO extends AbstractJPAITKDAO<Semis, MesureSemis, ValeurSemis> {

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<MesureSemis> getMesureITKClass() {
        return MesureSemis.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurSemis> getValeurITKClass() {
        return ValeurSemis.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, MesureSemis> getMesureAttribute() {
        return ValeurSemis_.mesureSemis;
    }

    /**
     * @param v
     * @return
     */
    @Override
    protected Join<?, Semis> getSequencePath(Root<ValeurSemis> v) {
        return v.join(ValeurSemis_.mesureSemis).join(MesureSemis_.semis);
    }
}
