/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPAListeItineraireDAO.
 */
@SuppressWarnings("unchecked")
public class JPAListeVegetationDAO extends AbstractJPADAO<ListeVegetation> implements IListeVegetationDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO
     * #getAll()
     */
    @Override
    public List<ListeVegetation> getAll() {
        return getAll(ListeVegetation.class);
    }

    /**
     * Gets the by code.
     *
     * @param name
     * @return the by code @see
     * org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO
     * #getByCode(java.lang.String)
     * @link(String) the code
     */
    @Override
    public List<? extends ListeVegetation> getByName(final String name) {

        CriteriaQuery<ListeVegetation> query = builder.createQuery(ListeVegetation.class);
        Root<ListeVegetation> lv = query.from(ListeVegetation.class);
        query.where(builder.equal(lv.get(ListeVegetation_.code), name));
        query.select(lv);
        return getResultList(query);
    }

    /**
     * @param <U>
     * @param nom
     * @param valeur
     * @return
     */
    @Override
    public <U extends ListeVegetation> Optional<U> getByNKey(String nom, String valeur) {
        CriteriaQuery<U> query = builder.createQuery((Class<U>) ListeVegetation.class);
        Root<U> lv = query.from((Class<U>) ListeVegetation.class);
        query.where(
                builder.equal(lv.get(ListeACBB_.code), nom),
                builder.equal(lv.get(ListeACBB_.valeur), valeur)
        );
        query.select(lv);
        return getOptional(query);
    }

    /**
     * Gets the from sql code.
     *
     * @param sqlCode
     * @return the from sql code
     * @link(String) the sql code
     * @link(String) the sql code
     */
    <T extends ListeVegetation> List<T> getFromSQLCode(final String sqlCode) {
        List<T> returnList = new LinkedList();
        returnList = this.entityManager.createQuery(sqlCode).getResultList();
        return returnList;
    }

    /**
     * @return
     */
    @Override
    public List<ListeVegetation> getValueColumns() {

        CriteriaQuery<ListeVegetation> query = builder.createQuery(ListeVegetation.class);
        Root<ListeVegetation> lv = query.from(ListeVegetation.class);
        query.where(
                builder.like(lv.get(ListeVegetation_.code), "column_%")
        );
        query.select(lv);
        return getResultList(query);
    }
}
