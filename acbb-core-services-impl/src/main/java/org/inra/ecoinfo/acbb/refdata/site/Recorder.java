/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.site;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.IAgroEcosystemeDAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<SiteACBB> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.site.messages";

    /**
     * The Constant PROPERTY_MSG_BAD_DATE_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_BAD_DATE_FORMAT = "PROPERTY_MSG_BAD_DATE_FORMAT";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_AGROECOSYSTEME_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_AGROECOSYSTEME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_AGROECOSYSTEME_NOT_FOUND_IN_DB";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * The site dao.
     */
    ISiteACBBDAO siteDAO;

    /**
     * The agro ecosysteme dao.
     */
    IAgroEcosystemeDAO agroEcosystemeDAO;

    /**
     * The agro ecosystemes possibles @link(String[]).
     */
    String[] agroEcosystemesPossibles;

    /**
     * Builds the string date mise en service.
     *
     * @param siteACBB
     * @return the string
     * @link(SiteACBB) the site acbb
     * @link(SiteACBB) the site acbb
     */
    String buildStringDateMiseEnService(final SiteACBB siteACBB) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (siteACBB != null && siteACBB.getDateMiseEnService() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(siteACBB.getDateMiseEnService(), this.datasetDescriptor.getColumns().get(16).getFormatType());
        }
        return dateDebut;
    }

    /**
     * Creates the or update site.
     *
     * @param siteNom
     * @param ville
     * @param adresse
     * @param coordonnees
     * @param milieu
     * @param climat
     * @param pluviometrieMoyenne
     * @param temperatureMoyenne
     * @param ventDominant
     * @param vitesseMoyenneVent
     * @param typeDeSol
     * @param profondeurMoyenneSol
     * @param altitudeMoyenne
     * @param dateMiseEnService
     * @param dbAgroEcosysteme
     * @param dbSite
     * @throws PersistenceException the persistence exception
     * @link(String) the site nom
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(AgroEcosysteme) the db agro ecosysteme
     * @link(SiteACBB) the db site
     * @link(String) the site nom
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(AgroEcosysteme) the db agro ecosysteme
     * @link(SiteACBB) the db site
     */
    void createOrUpdateSite(final String siteNom, final String ville, final String adresse,
                            final String coordonnees, final String milieu, final String climat,
                            final Float pluviometrieMoyenne, final Float temperatureMoyenne,
                            final String ventDominant, final Float vitesseMoyenneVent, final String typeDeSol,
                            final Float profondeurMoyenneSol, final Float altitudeMoyenne,
                            final LocalDate dateMiseEnService, final AgroEcosysteme dbAgroEcosysteme,
                            final SiteACBB dbSite) throws PersistenceException {

        if (dbSite == null) {
            this.createSite(siteNom, ville, adresse, coordonnees, milieu, climat,
                    pluviometrieMoyenne, temperatureMoyenne, ventDominant, vitesseMoyenneVent,
                    typeDeSol, profondeurMoyenneSol, altitudeMoyenne, dateMiseEnService,
                    dbAgroEcosysteme);
        } else {
            this.updateSite(ville, adresse, coordonnees, milieu, climat, pluviometrieMoyenne,
                    temperatureMoyenne, ventDominant, vitesseMoyenneVent, typeDeSol,
                    profondeurMoyenneSol, altitudeMoyenne, dateMiseEnService, dbAgroEcosysteme,
                    dbSite);
        }
    }

    /**
     * Creates the site.
     *
     * @param siteNom
     * @param ville
     * @param adresse
     * @param coordonnees
     * @param milieu
     * @param climat
     * @param pluviometrieMoyenne
     * @param temperatureMoyenne
     * @param ventDominant
     * @param vitesseMoyenneVent
     * @param typeDeSol
     * @param profondeurMoyenneSol
     * @param altitudeMoyenne
     * @param dateMiseEnService
     * @param dbAgroEcosysteme
     * @throws PersistenceException the persistence exception
     * @link(String) the site nom
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(AgroEcosysteme) the db agro ecosysteme
     * @link(String) the site nom
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(AgroEcosysteme) the db agro ecosysteme
     */
    void createSite(final String siteNom, final String ville, final String adresse,
                    final String coordonnees, final String milieu, final String climat,
                    final Float pluviometrieMoyenne, final Float temperatureMoyenne,
                    final String ventDominant, final Float vitesseMoyenneVent, final String typeDeSol,
                    final Float profondeurMoyenneSol, final Float altitudeMoyenne,
                    final LocalDate dateMiseEnService, final AgroEcosysteme dbAgroEcosysteme)
            throws PersistenceException {
        final SiteACBB siteACBB = new SiteACBB(siteNom, ville, adresse, coordonnees, milieu,
                climat, pluviometrieMoyenne, temperatureMoyenne, ventDominant, vitesseMoyenneVent,
                altitudeMoyenne, dateMiseEnService, typeDeSol, profondeurMoyenneSol,
                dbAgroEcosysteme);
        siteACBB.setAgroEcosysteme(dbAgroEcosysteme);
        this.siteDAO.saveOrUpdate(siteACBB);
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values;
            values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.mgaServiceBuilder.getRecorder().remove(siteDAO.getByPath(code).orElseThrow(() -> new BusinessException("bad site")));
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<SiteACBB> getAllElements() {
        List<SiteACBB> all = this.siteDAO.getAllSitesACBB();
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the names agro ecosystemes possibles.
     *
     * @return the names agro ecosystemes possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesAgroEcosystemesPossibles() throws PersistenceException {
        final List<AgroEcosysteme> agrosEcosystemes = this.agroEcosystemeDAO
                .getAll(AgroEcosysteme.class);
        final String[] namesAgroEcosystemesPossibles = new String[agrosEcosystemes.size()];
        int index = 0;
        for (final AgroEcosysteme agroEcosysteme : agrosEcosystemes) {
            namesAgroEcosystemesPossibles[index++] = agroEcosysteme.getName();
        }
        return namesAgroEcosystemesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param siteACBB
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(SiteACBB) the site acbb
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final SiteACBB siteACBB)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getAgroEcosysteme().getName(), this.agroEcosystemesPossibles, null,
                        true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomFR.getProperty(siteACBB.getName(), siteACBB.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.getProperty(siteACBB.getName(), siteACBB.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getVille(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getAdresse(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getCoordonnees(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getMilieu(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getClimat(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getPluviometrieMoyenne(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getTemperatureMoyenne(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getVentDominant(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getVitesseMoyenne(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getTypeDeSol(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getProfondeurMoyenneSol(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : siteACBB
                                .getAltitudeMoyenne(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        siteACBB == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this
                                .buildStringDateMiseEnService(siteACBB),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<SiteACBB> initModelGridMetadata() {
        try {
            this.agroEcosystemesPossibles = this.getNamesAgroEcosystemesPossibles();

            this.propertiesNomFR = this.localizationManager.newProperties(
                    Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRENCH);

            this.propertiesNomEN = this.localizationManager.newProperties(
                    Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist site.
     *
     * @param errorsReport
     * @param codeZoneEtude
     * @param siteNom
     * @param ville
     * @param adresse
     * @param coordonnees
     * @param milieu
     * @param climat
     * @param pluviometrieMoyenne
     * @param temperatureMoyenne
     * @param ventDominant
     * @param vitesseMoyenneVent
     * @param typeDeSol
     * @param profondeurMoyenneSol
     * @param altitudeMoyenne
     * @param dateMiseEnService
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code zone etude
     * @link(String) the site nom
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(ErrorsReport) the errors report
     * @link(String) the code zone etude
     * @link(String) the site nom
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     */
    void persistSite(final ErrorsReport errorsReport, final String codeZoneEtude,
                     final String siteNom, final String ville, final String adresse,
                     final String coordonnees, final String milieu, final String climat,
                     final Float pluviometrieMoyenne, final Float temperatureMoyenne,
                     final String ventDominant, final Float vitesseMoyenneVent, final String typeDeSol,
                     final Float profondeurMoyenneSol, final Float altitudeMoyenne,
                     final LocalDate dateMiseEnService) throws PersistenceException {
        final AgroEcosysteme dbAgroEcosysteme = this.retrieveDBAgroEcosysteme(errorsReport,
                codeZoneEtude);
        if (dbAgroEcosysteme != null) {
            final SiteACBB dbSite = (SiteACBB) this.siteDAO.getByPath(Utils.createCodeFromString(siteNom)).orElse(null);
            this.createOrUpdateSite(siteNom, ville, adresse, coordonnees, milieu, climat,
                    pluviometrieMoyenne, temperatureMoyenne, ventDominant, vitesseMoyenneVent,
                    typeDeSol, profondeurMoyenneSol, altitudeMoyenne, dateMiseEnService,
                    dbAgroEcosysteme, dbSite);
        }
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(SiteACBB.class));
                // On parcourt chaque colonne d'une ligne
                final String codeAgroEcosysteme = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                final String nomSite = tokenizerValues.nextToken();
                final String ville = tokenizerValues.nextToken();
                final String adresse = tokenizerValues.nextToken();
                final String coordonnees = tokenizerValues.nextToken();
                final String milieu = tokenizerValues.nextToken();
                final String climat = tokenizerValues.nextToken();
                final Float pluviometrieMoyenne = tokenizerValues.nextTokenFloat();
                final Float temperatureMoyenne = tokenizerValues.nextTokenFloat();
                final String ventDominant = tokenizerValues.nextToken();
                final Float vitesseMoyenneVent = tokenizerValues.nextTokenFloat();
                final String typeDeSol = tokenizerValues.nextToken();
                final Float profondeurMoyenneSol = tokenizerValues.nextTokenFloat();
                final Float altitudeMoyenne = tokenizerValues.nextTokenFloat();
                final String stringDateMiseEnService = tokenizerValues.nextToken();
                try {
                    LocalDate dateMiseEnService = null;
                    dateMiseEnService = stringDateMiseEnService != null
                            && !stringDateMiseEnService.isEmpty()
                            ? DateUtil.readLocalDateFromText(this.datasetDescriptor.getColumns().get(16).getFormatType(), stringDateMiseEnService)
                            : null;
                    this.persistSite(errorsReport, codeAgroEcosysteme, nomSite, ville, adresse,
                            coordonnees, milieu, climat, pluviometrieMoyenne, temperatureMoyenne,
                            ventDominant, vitesseMoyenneVent, typeDeSol, profondeurMoyenneSol,
                            altitudeMoyenne, dateMiseEnService);
                } catch (final DateTimeException e) {
                    LOGGER.debug(e.getMessage(), e);
                    errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                            Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_DATE_FORMAT),
                            stringDateMiseEnService, this.datasetDescriptor.getColumns().get(15)
                                    .getFormatType()));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db agro ecosysteme.
     *
     * @param errorsReport
     * @param codeAgroEcosysteme
     * @return the agro ecosysteme
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code agro ecosysteme
     * @link(ErrorsReport) the errors report
     * @link(String) the code agro ecosysteme
     */
    AgroEcosysteme retrieveDBAgroEcosysteme(final ErrorsReport errorsReport,
                                            final String codeAgroEcosysteme) throws PersistenceException {
        final AgroEcosysteme agroEcosysteme = this.agroEcosystemeDAO.getByCode(codeAgroEcosysteme).orElse(null);
        if (agroEcosysteme == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_AGROECOSYSTEME_NOT_FOUND_IN_DB),
                    codeAgroEcosysteme));
        }
        return agroEcosysteme;
    }

    /**
     * Sets the agro ecosysteme dao.
     *
     * @param agroEcosystemeDAO the new agro ecosysteme dao
     */
    public final void setAgroEcosystemeDAO(final IAgroEcosystemeDAO agroEcosystemeDAO) {
        this.agroEcosystemeDAO = agroEcosystemeDAO;
    }

    /**
     * Sets the site acbbdao.
     *
     * @param siteDAO the new site acbbdao
     */
    public final void setSiteACBBDAO(final ISiteACBBDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Update site.
     *
     * @param ville
     * @param adresse
     * @param coordonnees
     * @param milieu
     * @param climat
     * @param pluviometrieMoyenne
     * @param temperatureMoyenne
     * @param ventDominant
     * @param vitesseMoyenneVent
     * @param typeDeSol
     * @param profondeurMoyenneSol
     * @param altitudeMoyenne
     * @param dateMiseEnService
     * @param dbAgroEcosysteme
     * @param dbSite
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(AgroEcosysteme) the db agro ecosysteme
     * @link(SiteACBB) the db site
     * @link(String) the ville
     * @link(String) the adresse
     * @link(String) the coordonnees
     * @link(String) the milieu
     * @link(String) the climat
     * @link(Float) the pluviometrie moyenne
     * @link(Float) the temperature moyenne
     * @link(String) the vent dominant
     * @link(Float) the vitesse moyenne vent
     * @link(String) the type de sol
     * @link(Float) the profondeur moyenne sol
     * @link(Float) the altitude moyenne
     * @link(Date) the date mise en service
     * @link(AgroEcosysteme) the db agro ecosysteme
     * @link(SiteACBB) the db site
     */
    void updateSite(final String ville, final String adresse, final String coordonnees,
                    final String milieu, final String climat, final Float pluviometrieMoyenne,
                    final Float temperatureMoyenne, final String ventDominant,
                    final Float vitesseMoyenneVent, final String typeDeSol,
                    final Float profondeurMoyenneSol, final Float altitudeMoyenne,
                    final LocalDate dateMiseEnService, final AgroEcosysteme dbAgroEcosysteme,
                    final SiteACBB dbSite) {

        dbSite.setVille(ville);
        dbSite.setAdresse(adresse);
        dbSite.setCoordonnees(coordonnees);
        dbSite.setMilieu(milieu);
        dbSite.setClimat(climat);
        dbSite.setPluviometrieMoyenne(pluviometrieMoyenne);
        dbSite.setTemperatureMoyenne(temperatureMoyenne);
        dbSite.setVentDominant(ventDominant);
        dbSite.setVitesseMoyenne(vitesseMoyenneVent);
        dbSite.setTypeDeSol(typeDeSol);
        dbSite.setProfondeurMoyenneSol(profondeurMoyenneSol);
        dbSite.setAltitudeMoyenne(altitudeMoyenne);
        dbSite.setDateMiseEnService(dateMiseEnService);
        dbSite.setAgroEcosysteme(dbAgroEcosysteme);

    }

}
