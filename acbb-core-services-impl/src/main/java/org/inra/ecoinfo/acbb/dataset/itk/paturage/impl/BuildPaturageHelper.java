package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IValeurPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildTravailDusolHelper.
 */
public class BuildPaturageHelper extends AbstractBuildHelper<PaturageLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.paturage.impl.messages";

    /**
     * The semis_.
     */
    Map<Parcelle, Map<LocalDate, Map<LocalDate, Paturage>>> paturages = new HashMap();

    IPaturageDAO paturageDAO;

    IValeurPaturageDAO valeurPaturageDAO;

    /**
     * The tempo dao @link(ITempoDAO).
     */
    ITempoDAO tempoDAO;

    /**
     * Adds the mesures semis.
     *
     * @param line
     * @param semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    void addValeursPaturage(final PaturageLineRecord line, final Paturage paturage) {
        for (final ACBBVariableValue<ListeItineraire> variableValue : line.getACBBVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurPaturage valeur;
            if (variableValue.isValeurQualitative()) {
                valeur = new ValeurPaturage(variableValue.getValeurQualitative(),
                        realNode, paturage);
            } else {
                valeur = new ValeurPaturage(Float.parseFloat(variableValue.getValue()),
                        realNode, paturage);
            }
            paturage.getValeursPaturage().add(valeur);
        }
    }

    /**
     * Builds the datas and record it to database.
     *
     * @throws PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() throws PersistenceException {
        for (final PaturageLineRecord line : this.lines) {
            final Paturage paturage = this.getOrCreatePaturages(line);
            this.paturageDAO.saveOrUpdate(paturage);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursPaturage(line, paturage);
            this.paturageDAO.saveOrUpdate(paturage);
        }
    }

    /**
     * Instantiates a new builds semis helper.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines @{List
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @throws PersistenceException
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(RequestPropertiesSemis) the request properties semis
     */
    public void build(final VersionFile version, final List<PaturageLineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK)
            throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    /**
     * Creates the new semis.
     *
     * @param line
     * @return the semis
     * @link(LineRecord) the line
     */
    Paturage createNewPaturage(final PaturageLineRecord line) {
        Paturage paturage = null;
        final VersionDeTraitement versionTraitement = line.getVersionDeTraitement();
        final Parcelle parcelle = line.getParcelle();
        final Tempo tempo = this.getTempo(line, versionTraitement, parcelle);
        if (tempo.isInErrors()) {
            return paturage;
        }
        paturage = new Paturage(this.version, line.getObservation(), line.getDate(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber(), line.getEndDate(), tempo);
        paturage.setSuiviParcelle(line.getSuiviParcelle());
        try {
            this.paturageDAO.saveOrUpdate(paturage);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return paturage;
    }

    /**
     * Gets the or create semis.
     *
     * @param line
     * @return the or create semis
     * @link(LineRecord) the line
     * @link(LineRecord) the line
     */
    Paturage getOrCreatePaturages(final PaturageLineRecord line) {
        VersionDeTraitement versionDeTraitement = null;
        try {
            versionDeTraitement = this.versionDeTraitementDAO.merge(line.getVersionDeTraitement());
            line.setVersionDeTraitement(versionDeTraitement);
        } catch (final PersistenceException e) {
            LOGGER.error("can't getOrCreateFne");
        }
        return paturages
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getEndDate(), k -> createNewPaturage(line));
    }

    /**
     * @param paturageDAO the paturageDAO to set
     */
    public final void setPaturageDAO(final IPaturageDAO paturageDAO) {
        this.paturageDAO = paturageDAO;
    }

    /**
     * @param tempoDAO the tempoDAO to set
     */
    public final void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * @param valeurPaturageDAO the valeurPaturageDAO to set
     */
    public final void setValeurPaturageDAO(final IValeurPaturageDAO valeurPaturageDAO) {
        this.valeurPaturageDAO = valeurPaturageDAO;
    }

}
