/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal;

import org.inra.ecoinfo.acbb.refdata.methode.jpa.AbstractJPAMethodeDAO;

/**
 * The Class JPABlocDAO.
 */
public class JPAMethodeHauteurVegetalDAO extends AbstractJPAMethodeDAO<MethodeHauteurVegetal> implements
        IMethodeHauteurVegetalDAO {

    /**
     * @return
     */
    @Override
    public Class<MethodeHauteurVegetal> getMethodClass() {
        return MethodeHauteurVegetal.class;
    }
}
