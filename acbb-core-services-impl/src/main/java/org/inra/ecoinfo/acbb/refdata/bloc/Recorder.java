/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.bloc;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Bloc> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.bloc.messages";

    /**
     * The Constant PROPERTY_MSG_BAD_DATE_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_BAD_DATE_FORMAT = "PROPERTY_MSG_BAD_DATE_FORMAT";

    /**
     * The Constant PROPERTY_MSG_ERROR_PARENT_NOT_EXISTING @link(String).
     */
    static final String PROPERTY_MSG_ERROR_PARENT_NOT_EXISTING = "PROPERTY_MSG_ERROR_PARENT_NOT_EXISTING";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The site dao.
     */
    ISiteACBBDAO siteDAO;

    /**
     * The bloc dao.
     */
    IBlocDAO blocDAO;

    /**
     * The sites possibles @link(String[]).
     */
    String[] sitesPossibles;

    /**
     * The blocs possibles @link(Map<String,List<String>>).
     */
    Map<String, List<String>> blocsPossibles;

    /**
     * The properties description en @link(Properties).
     */
    Properties propertiesDescriptionEN;

    /**
     * Builds the string date creation.
     *
     * @param bloc
     * @return the string
     * @link(Bloc) the bloc
     * @link(Bloc) the bloc
     */
    String buildStringDateCreation(final Bloc bloc) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (bloc.getDateCreation() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(bloc.getDateCreation(), this.datasetDescriptor.getColumns().get(3).getFormatType());
        }
        return dateDebut;
    }

    /**
     * Creates the or update bloc.
     *
     * @param nomBloc
     * @param dateCreation
     * @param description
     * @param dbSite
     * @param dbBloc
     * @throws PersistenceException the persistence exception
     * @link(String) the nom bloc
     * @link(Date) the date creation
     * @link(String) the description
     * @link(SiteACBB) the db site
     * @link(Bloc) the db bloc
     * @link(String) the nom bloc
     * @link(Date) the date creation
     * @link(String) the description
     * @link(SiteACBB) the db site
     * @link(Bloc) the db bloc
     */
    void createOrUpdateBloc(final String nomBloc, final LocalDate dateCreation,
                            final String description, final SiteACBB dbSite, final Bloc dbBloc)
            throws PersistenceException {
        if (dbBloc == null) {
            final Bloc bloc = new Bloc(nomBloc, dateCreation, description, null);
            bloc.setSite(dbSite);
            this.blocDAO.saveOrUpdate(bloc);
        } else {
            dbBloc.setDescription(description);
            dbBloc.setDateCreation(dateCreation);
            this.blocDAO.saveOrUpdate(dbBloc);
        }
    }

    /**
     * Creates the or update bloc.
     *
     * @param nomBloc
     * @param dateCreation
     * @param description
     * @param dbSite
     * @param dbBloc
     * @param dbParentBloc
     * @throws PersistenceException the persistence exception
     * @link(String) the nom bloc
     * @link(Date) the date creation
     * @link(String) the description
     * @link(SiteACBB) the db site
     * @link(Bloc) the db bloc
     * @link(Bloc) the db parent bloc
     * @link(String) the nom bloc
     * @link(Date) the date creation
     * @link(String) the description
     * @link(SiteACBB) the db site
     * @link(Bloc) the db bloc
     * @link(Bloc) the db parent bloc
     */
    void createOrUpdateBloc(final String nomBloc, final LocalDate dateCreation,
                            final String description, final SiteACBB dbSite, final Bloc dbBloc,
                            final Bloc dbParentBloc) throws PersistenceException {
        if (dbBloc == null) {
            final Bloc bloc = new Bloc(nomBloc, dateCreation, description, dbParentBloc);
            final String codeParent = Utils.createCodeFromString(dbParentBloc.getNom());
            bloc.setCode(codeParent.concat(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR)
                    .concat(Utils.createCodeFromString(nomBloc)));
            bloc.setSite(dbSite);
            bloc.setParent(dbParentBloc);
            this.blocDAO.saveOrUpdate(bloc);
        } else {
            dbBloc.setDescription(description);
            dbBloc.setDateCreation(dateCreation);
            this.blocDAO.saveOrUpdate(dbBloc);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeBloc = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.blocDAO.remove(this.blocDAO.getByNKey(
                        this.siteDAO.getByPath(codeSite).orElseThrow(PersistenceException::new).getId(), codeBloc).orElseThrow(PersistenceException::new));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Bloc> getAllElements() {
        List<Bloc> all = this.blocDAO.getAll(Bloc.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the names blocs possibles.
     *
     * @return the names blocs possibles
     * @throws PersistenceException the persistence exception
     */
    Map<String, List<String>> getNamesBlocsPossibles() throws PersistenceException {
        final List<SiteACBB> sites = this.siteDAO.getAllSitesACBB();
        final Map<String, List<String>> namesBlocsPossibles = new ConcurrentHashMap();
        for (final SiteACBB site : sites) {
            namesBlocsPossibles.put(site.getName(), this.blocDAO.getNamesBlocsForSite(site));
        }
        return namesBlocsPossibles;

    }

    /**
     * Gets the names sites possibles.
     *
     * @return the names sites possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesSitesPossibles() throws PersistenceException {
        final List<SiteACBB> sites = this.siteDAO.getAllSitesACBB();
        final String[] namesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (final Site site : sites) {
            namesSitesPossibles[index++] = site.getName();
        }
        return namesSitesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param bloc
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(Bloc) the bloc
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Bloc bloc)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata siteColumnModelGridMetadata = new ColumnModelGridMetadata(
                bloc == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : bloc.getSite().getName(),
                this.sitesPossibles, null, true, false, true);
        final ColumnModelGridMetadata codeColumnModelGridMetadata = new ColumnModelGridMetadata(
                bloc == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : bloc.getParent() != null ? bloc.getParent().getNom() : bloc.getNom(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true);
        codeColumnModelGridMetadata.setAutoComplete(this.blocsPossibles,
                siteColumnModelGridMetadata);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(siteColumnModelGridMetadata);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(codeColumnModelGridMetadata);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(bloc != null && bloc.getParent() != null ? bloc
                        .getNom() : org.apache.commons.lang.StringUtils.EMPTY, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(bloc == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : this.buildStringDateCreation(bloc),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(bloc == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : bloc.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(bloc == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : this.propertiesDescriptionEN.get(bloc.getDescription()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Bloc> initModelGridMetadata() {
        this.propertiesDescriptionEN = this.localizationManager.newProperties(Bloc.NAME_ENTITY_JPA,
                "description", Locale.ENGLISH);
        try {
            this.sitesPossibles = this.getNamesSitesPossibles();
            this.blocsPossibles = this.getNamesBlocsPossibles();
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist bloc.
     *
     * @param errorsReport
     * @param codeSite
     * @param nomBloc
     * @param repetition
     * @param dateCreation
     * @param description
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the nom bloc
     * @link(String) the repetition
     * @link(Date) the date creation
     * @link(String) the description
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(String) the nom bloc
     * @link(String) the repetition
     * @link(Date) the date creation
     * @link(String) the description
     */
    void persistBloc(final ErrorsReport errorsReport, final String codeSite, final String nomBloc,
                     final String repetition, final LocalDate dateCreation, final String description)
            throws PersistenceException {
        final SiteACBB dbSite = this.retrieveDBSite(errorsReport, codeSite);
        if (dbSite != null) {
            Bloc dbBloc;
            if (repetition == null) {
                dbBloc = this.blocDAO
                        .getByNKey(dbSite.getId(), Utils.createCodeFromString(nomBloc)).orElse(null);
                this.createOrUpdateBloc(nomBloc, dateCreation, description, dbSite, dbBloc);
            } else {
                final Bloc dbParentBloc = this.blocDAO.getByNKey(dbSite.getId(),
                        Utils.createCodeFromString(nomBloc)).orElse(null);
                if (dbParentBloc != null) {
                    final String codeParent = Utils.createCodeFromString(nomBloc);
                    dbBloc = this.blocDAO.getByNKey(
                            dbSite.getId(),
                            codeParent.concat(
                                    AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR).concat(
                                    Utils.createCodeFromString(repetition))).orElse(null);
                    this.createOrUpdateBloc(repetition, dateCreation, description, dbSite, dbBloc,
                            dbParentBloc);
                } else {
                    errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                            Recorder.BUNDLE_SOURCE_PATH,
                            Recorder.PROPERTY_MSG_ERROR_PARENT_NOT_EXISTING), nomBloc));
                }
            }
        }
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Bloc.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String codeSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String nomBloc = tokenizerValues.nextToken();
                final String repetition = tokenizerValues.nextToken();
                final String dateCreationString = tokenizerValues.nextToken();
                try {
                    final LocalDate dateCreation = dateCreationString != null
                            && dateCreationString.length() > 0 ?
                            DateUtil.readLocalDateFromText(this.datasetDescriptor.getColumns().get(3).getFormatType(), dateCreationString) :
                            null;
                    final String description = tokenizerValues.nextToken();
                    this.persistBloc(errorsReport, codeSite, nomBloc, repetition, dateCreation,
                            description);
                } catch (final DateTimeException e) {
                    errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                            Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_DATE_FORMAT),
                            dateCreationString));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retrieve db site.
     *
     * @param errorsReport
     * @param codeSite
     * @return the site acbb
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     * @link(ErrorsReport) the errors report
     * @link(String) the code site
     */
    SiteACBB retrieveDBSite(final ErrorsReport errorsReport, final String codeSite)
            throws PersistenceException {
        final SiteACBB dbSite = (SiteACBB) this.siteDAO.getByPath(codeSite).orElse(null);
        if (dbSite == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), codeSite));
        }
        return dbSite;
    }

    /**
     * Sets the bloc dao.
     *
     * @param blocDAO the new bloc dao
     */
    public final void setBlocDAO(final IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

}
