package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractor;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.util.Collection;

/**
 * The Class ITKExtractor.
 */
public class ITKExtractor extends MO implements IExtractor {

    /**
     * The Constant MAP_INDEX_0.
     */
    public static final String MAP_INDEX_0 = "0";
    // extraction
    /**
     * The Constant CST_RESULTS.
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.itk.messages";

    /**
     * The Constant MSG_EXTRACTION_ABORTED @link(String).
     */
    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     * The Constant PROPERTY_MSG_BADS_RIGHTS @link(String).
     */
    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";

    IITKExtractor semisExtractor;
    IITKExtractor wsolExtractor;
    IITKExtractor paturageExtractor;
    IITKExtractor recFauExtractor;
    IITKExtractor aiExtractor;
    IITKExtractor fneExtractor;
    IITKExtractor fertilisantExtractor;
    IITKExtractor phytosanitaireExtractor;

    IITKExtractionDAO itkExtractionDAO;

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IExtractor#extract(org.inra.ecoinfo.extraction
     *                           .IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings("unchecked")
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        statusBar.setProgress(0);
        int extractionResult = 8;
        this.extractCouverts((ITKParameterVO) parameters);
        statusBar.setProgress(10);
        extractionResult = tryExtract(parameters, aiExtractor, extractionResult);
        statusBar.setProgress(15);
        extractionResult = tryExtract(parameters, fertilisantExtractor, extractionResult);
        statusBar.setProgress(20);
        extractionResult = tryExtract(parameters, fneExtractor, extractionResult);
        statusBar.setProgress(25);
        extractionResult = tryExtract(parameters, paturageExtractor, extractionResult);
        statusBar.setProgress(30);
        extractionResult = tryExtract(parameters, phytosanitaireExtractor, extractionResult);
        statusBar.setProgress(35);
        extractionResult = tryExtract(parameters, recFauExtractor, extractionResult);
        statusBar.setProgress(40);
        extractionResult = tryExtract(parameters, semisExtractor, extractionResult);
        statusBar.setProgress(45);
        extractionResult = tryExtract(parameters, wsolExtractor, extractionResult);
        statusBar.setProgress(50);
        if (extractionResult == 0) {
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    ITKExtractor.BUNDLE_SOURCE_PATH, this.localizationManager.getMessage(
                            ITKExtractor.BUNDLE_SOURCE_PATH,
                            ITKExtractor.MSG_EXTRACTION_ABORTED))), Notification.ERROR,
                    ITKExtractor.PROPERTY_MSG_BADS_RIGHTS, (Utilisateur) this.policyManager.getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
    }

    private int tryExtract(final IParameter parameters, IITKExtractor extractor, int extractionResult) throws BusinessException {
        final Collection variables = (Collection) parameters.getParameters().get(
                Variable.class.getSimpleName().toLowerCase().concat(extractor.getExtractCode()));
        try {
            if (!CollectionUtils.isEmpty(variables)) {
                extractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                return --extractionResult;
            } else {
                throw e;
            }
        }
        return extractionResult;
    }

    private void extractCouverts(ITKParameterVO parameter) {
        parameter.getParameters().put(Parcelle.class.getSimpleName(),
                this.itkExtractionDAO.extractCouverts(parameter.getSelectedTraitementProgrammes()));
    }

    /**
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = semisExtractor.getExtractionSize(parameters);
        size += wsolExtractor.getExtractionSize(parameters);
        size += paturageExtractor.getExtractionSize(parameters);
        size += recFauExtractor.getExtractionSize(parameters);
        size += aiExtractor.getExtractionSize(parameters);
        size += fneExtractor.getExtractionSize(parameters);
        size += fertilisantExtractor.getExtractionSize(parameters);
        size += phytosanitaireExtractor.getExtractionSize(parameters);
        return size;
    }

    /**
     * @param aiExtractor the aiExtractor to set
     */
    public void setAiExtractor(IITKExtractor aiExtractor) {
        this.aiExtractor = aiExtractor;
    }

    /**
     * Sets the extraction.
     *
     * @param extraction the new extraction
     * @see org.inra.ecoinfo.extraction.IExtractor#setExtraction(org.inra.ecoinfo
     * .config.Extraction)
     */
    @Override
    public final void setExtraction(final Extraction extraction) {

    }

    /**
     * @param fertExtractor the fertExtractor to set
     */
    public void setFertilisantExtractor(IITKExtractor fertExtractor) {
        this.fertilisantExtractor = fertExtractor;
    }

    /**
     * @param fneExtractor the fneExtractor to set
     */
    public void setFneExtractor(IITKExtractor fneExtractor) {
        this.fneExtractor = fneExtractor;
    }

    /**
     * @param itkExtractionDAO the itkExtractionDAO to set
     */
    public void setItkExtractionDAO(IITKExtractionDAO itkExtractionDAO) {
        this.itkExtractionDAO = itkExtractionDAO;
    }

    /**
     * @param paturageExtractor
     */
    public void setPaturageExtractor(IITKExtractor paturageExtractor) {
        this.paturageExtractor = paturageExtractor;
    }

    /**
     * @param phytExtractor the phytExtractor to set
     */
    public void setPhytosanitaireExtractor(IITKExtractor phytExtractor) {
        this.phytosanitaireExtractor = phytExtractor;
    }

    /**
     * @param recFauExtractor
     */
    public void setRecFauExtractor(IITKExtractor recFauExtractor) {
        this.recFauExtractor = recFauExtractor;
    }

    /**
     * @param semisExtractor the semisExtractor to set
     */
    public void setSemisExtractor(IITKExtractor semisExtractor) {
        this.semisExtractor = semisExtractor;
    }

    /**
     * @param wsolExtractor the wsolExtractor to set
     */
    public void setWsolExtractor(IITKExtractor wsolExtractor) {
        this.wsolExtractor = wsolExtractor;
    }
}
