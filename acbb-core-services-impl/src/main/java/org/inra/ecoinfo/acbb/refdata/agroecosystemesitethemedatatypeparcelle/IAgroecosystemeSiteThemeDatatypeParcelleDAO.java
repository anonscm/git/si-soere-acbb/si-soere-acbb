/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.acbb.refdata.agroecosystemesitethemedatatypeparcelle;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import java.util.List;

/**
 * @author Philippe TCHERNIATINSKY
 * <p>
 * The Interface IAgroecosystemeSiteThemeDatatypeParcelleDAO.
 */
public interface IAgroecosystemeSiteThemeDatatypeParcelleDAO extends IDAO<INode> {

    /**
     * Gets the by path agroecosysteme site theme code and datatype code.
     *
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @param parcelleCode
     * @return the by path agroecosysteme site theme code and datatype code
     * @link(String)
     * @link(String) the path site
     * @link(String) the theme code
     * @link(String) the datatype code
     * @link(String) the parcelle code
     */
    List<INode> getByParcellePathSiteThemeCodeAndDatatypeCode(String pathSite, String themeCode, String datatypeCode, String parcelleCode);

    /**
     * @return
     */
    List<RealNode> loadRealDatatypeNodes();

    /**
     * @return
     */
    List<String> getPathes();
}
