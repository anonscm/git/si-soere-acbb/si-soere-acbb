/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.lai.jpa;


import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse_;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.ValeurLai;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.ValeurLai_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationLaiDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {
    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeur(version);
        deleteBiomasse(version);
    }

    private void deleteValeur(VersionFile versionFile) {
        CriteriaDelete<ValeurLai> deletevaleur = builder.createCriteriaDelete(ValeurLai.class);
        Root<ValeurLai> v = deletevaleur.from(ValeurLai.class);
        Subquery<Lai> subqueryLai = deletevaleur.subquery(Lai.class);
        Root<Lai> lai = subqueryLai.from(Lai.class);
        subqueryLai
                .select(lai)
                .where(builder.equal(lai.get(AbstractBiomasse_.version), versionFile));
        Path<Lai> laiDB = v.get(ValeurLai_.lai);
        deletevaleur
                .where(laiDB.in(subqueryLai));
        delete(deletevaleur);
    }

    private void deleteBiomasse(VersionFile versionFile) {
        CriteriaDelete<Lai> deleteBiomasse = builder.createCriteriaDelete(Lai.class);
        Root<Lai> intervention = deleteBiomasse.from(Lai.class);
        deleteBiomasse
                .where(builder.equal(intervention.get(AbstractBiomasse_.version), versionFile));
        delete(deleteBiomasse);
    }

}
