/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;

import java.util.Optional;

/**
 * The Interface IAnneeRealiseeDAO.
 */
public interface IAnneeRealiseeDAO extends IDAO<AnneeRealisee> {

    /**
     * Gets the by n key.
     *
     * @param versionTraitementRealise the version traitement realise
     * @param annee                    the annee
     * @return the by n key
     */
    Optional<AnneeRealisee> getByNKey(VersionTraitementRealisee versionTraitementRealise, int annee);
}
