/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.site;

import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.refdata.site.JPASiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * The Class JPASiteACBBDAO.
 */
public class JPASiteACBBDAO extends JPASiteDAO implements ISiteACBBDAO {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JPASiteACBBDAO.class);

    /**
     * Gets the all sites acbb.
     *
     * @return the all sites acbb
     * @see org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO#getAllSitesACBB()
     */
    @Override
    public List<SiteACBB> getAllSitesACBB() {
        CriteriaQuery<SiteACBB> query = builder.createQuery(SiteACBB.class);
        Root<SiteACBB> sit = query.from(SiteACBB.class);
        query.select(sit);
        return getResultList(query);
    }

    /**
     * Gets the by agro id.
     *
     * @param id
     * @return the by agro id @see
     * org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO#getByAgroId(java.lang
     * .Long)
     * @link(Long) the id
     */
    @Override
    public List<SiteACBB> getByAgroId(final Long id) {
        CriteriaQuery<SiteACBB> query = builder.createQuery(SiteACBB.class);
        Root<SiteACBB> sit = query.from(SiteACBB.class);
        query.where(
                builder.equal(sit.join(SiteACBB_.agroEcosysteme).get(Nodeable_.id), id)
        );
        query.select(sit);
        return getResultList(query);
    }

    /**
     * Gets the by code.
     *
     * @param code
     * @return the by code
     * @link(String) the code
     */
    @Override
    public Optional<Site> getByPath(final String code) {
        if (code == null) {
            return Optional.empty();
        }
        CriteriaQuery<SiteACBB> query = builder.createQuery(SiteACBB.class);
        Root<SiteACBB> sit = query.from(SiteACBB.class);
        query.where(
                builder.equal(sit.get(Nodeable_.code), code)
        );
        query.select(sit);
        return getOptional(query)
                .map(s -> s);
    }

    /**
     * Gets the by short name.
     *
     * @param code
     * @return the by short name
     * @link(String) the code
     */
    @Override
    public Optional<SiteACBB> getByShortName(final String code) {
        if (code == null) {
            return Optional.empty();
        }
        CriteriaQuery<SiteACBB> query = builder.createQuery(SiteACBB.class);
        Root<SiteACBB> sit = query.from(SiteACBB.class);
        query.where(
                builder.equal(builder.substring(sit.get(Nodeable_.code), 1, 3), code)
        );
        query.select(sit);
        return getOptional(query);
    }
}
