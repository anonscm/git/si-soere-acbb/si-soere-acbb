/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Query;

/**
 * The Class JPAPublicationTravailDuSolDAO.
 */
public class JPAPublicationTravailDuSolDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * The Constant QUERY_DELETE_SEQUENCE @link(String).
     */
    static final String QUERY_DELETE_SEQUENCE = "delete from TravailDuSol wsol where wsol.version=:version";

    /**
     * The Constant QUERY_DELETE_SEQUENCE @link(String).
     */
    static final String QUERY_DELETE_VALEUR_VQ = "delete from vwsol_vq where vwsol_id in ("
            + "select vwsol_id from valeur_travail_du_sol_vwsol vwsol where mwsol_id in ("
            + "select mwsol_id from mesure_travail_du_sol_mwsol mwsol where itk_id in ("
            + "select itk_id from travail_du_sol_wsol wsol where ivf_id = :version)))";

    /**
     * The Constant QUERY_DELETE_MEASURE @link(String).
     */
    static final String QUERY_DELETE_MEASURE = "delete from MesureTravailDuSol where  travailDuSol in (select id from TravailDuSol wsol where wsol.version=:version)";

    /**
     * The Constant QUERY_DELETE_VALEUR @link(String).
     */
    static final String QUERY_DELETE_VALEUR = "delete from ValeurTravailDuSol where mesureTravailDuSol in (select id from MesureTravailDuSol mwsol where  travailDuSol in (select id from TravailDuSol wsol where wsol.version=:version))";

    /**
     * Removes the version.
     *
     * @param version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion(org.
     *                              inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        final String requeteDeleteValeurVQ = JPAPublicationTravailDuSolDAO.QUERY_DELETE_VALEUR_VQ;
        Query query = this.entityManager.createNativeQuery(requeteDeleteValeurVQ);
        query.setParameter("version", version);
        query.executeUpdate();
        final String requeteDeleteValeur = JPAPublicationTravailDuSolDAO.QUERY_DELETE_VALEUR;
        query = this.entityManager.createQuery(requeteDeleteValeur);
        query.setParameter("version", version);
        query.executeUpdate();
        final String requeteDeleteMesure = JPAPublicationTravailDuSolDAO.QUERY_DELETE_MEASURE;
        query = this.entityManager.createQuery(requeteDeleteMesure);
        query.setParameter("version", version);
        query.executeUpdate();
        final String requeteDeleteSequence = JPAPublicationTravailDuSolDAO.QUERY_DELETE_SEQUENCE;
        query = this.entityManager.createQuery(requeteDeleteSequence);
        query.setParameter("version", version);
        query.executeUpdate();
    }

}
