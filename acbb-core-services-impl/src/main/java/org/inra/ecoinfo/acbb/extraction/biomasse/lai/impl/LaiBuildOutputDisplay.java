package org.inra.ecoinfo.acbb.extraction.biomasse.lai.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.ValeurLai;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.AbstractBiomasseOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.refdata.variable.Variable;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * @author ptcherniati
 */
public class LaiBuildOutputDisplay extends AbstractBiomasseOutputBuilder<Lai, ValeurLai, Lai> {

    /**
     *
     */
    public static final String PATTERN_7_FIELD = ";%s;%s;%s;%s;%s;%s;%s";
    /**
     *
     */
    public static final String PATTERN_4_FIELD = ";%s;%s;%s;%s";
    /**
     *
     */
    public static final String PATTERN_5_FIELD = ";%s;%s;%s;%s;%s";
    /**
     *
     */
    protected static final String FIRST_VARIABLE = "lai";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.biomasse.lai.messages";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public LaiBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getFirstVariable() {
        return FIRST_VARIABLE;
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return LaiBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            DatesFormParamVO datesYearsContinuousFormParamVO,
            List<Variable> selectedWsolVariables,
            Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            PrintStream rawDataPrintStream) {
        return new LaiOutputHelper(availablesCouverts, rawDataPrintStream,
                selectedWsolVariables);
    }

    /**
     *
     */
    public class LaiOutputHelper extends OutputHelper {

        /**
         * @param rawDataPrintStream
         * @param selectedVariables
         * @param availablesCouverts
         */
        public LaiOutputHelper(final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts, PrintStream rawDataPrintStream, List<Variable> selectedVariables) {
            super(availablesCouverts, rawDataPrintStream, selectedVariables);
        }

        /**
         * @param valeurBiomasse
         */
        @Override
        protected void addVariablesCulumns(ValeurLai valeurBiomasse) {
            String es = valeurBiomasse.getBiomasseEntreeSortie() == null
                    ? org.apache.commons.lang.StringUtils.EMPTY
                    : propertiesValeursQualitative.getProperty(valeurBiomasse.getBiomasseEntreeSortie().getValeur(), valeurBiomasse.getBiomasseEntreeSortie().getValeur());
            es = propertiesValeursQualitative.getProperty(BiomasseEntreeSortie.BIOMASSE_ENTREE_SORTIE, es);
            String line = String.format(PATTERN_7_FIELD,
                    es,
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getValeur()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getMeasureNumber()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getStandardDeviation()),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getMediane()),
                    valeurBiomasse.getMethode().getNumberId(),
                    ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(valeurBiomasse.getQualityIndex()));
            this.rawDataPrintStream.print(line);
        }

        /**
         *
         */
        @Override
        protected void addVariablesCulumnsForNullVariable() {
            this.rawDataPrintStream.print(LaiBuildOutputDisplay.PATTERN_5_FIELD.replaceAll("%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE));
        }

        /**
         * @param mesure
         */
        @Override
        protected void printLineSpecific(Lai mesure) {
            String line;
            String nature = mesure.getLaiNature().getValeur();
            nature = propertiesValeursQualitative.getProperty(nature, nature);
            line = String
                    .format(LaiBuildOutputDisplay.PATTERN_5_FIELD,
                            nature,
                            getCouvert(mesure),
                            getAnnee(mesure),
                            mesure.getSerie(),
                            getDateMesure(mesure));
            this.rawDataPrintStream.print(line);
        }
    }
}
