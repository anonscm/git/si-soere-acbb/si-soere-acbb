/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.methods;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.refdata.IMetadataRecorderDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @param <T>
 * @author ptcherniati
 */
public abstract class AbstractMethodBuildOutputDisplay<T extends AbstractMethodeRecorder> extends AbstractOutputBuilder {

    Logger LOGGER = LoggerFactory.getLogger(AbstractMethodBuildOutputDisplay.class);

    IMetadataRecorderDAO metadataRecorderDAO;
    T recorder;
    String recorderName;

    /**
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings(value = "rawtypes")
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        final Set<String> datatypeNames = new HashSet();
        datatypeNames.add(this.getFileName());
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                AbstractOutputBuilder.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        Set<AbstractMethode> allMethods = (Set<AbstractMethode>) requestMetadatasMap
                .getOrDefault(AbstractMethode.class.getSimpleName(), new HashSet());

        String result = org.apache.commons.lang.StringUtils.EMPTY;
        for (final Map.Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap
                .entrySet()) {
            final PrintStream outputSteam = outputPrintStreamMap.get(datatypeNameEntry.getKey());
            result = this.getRecorder().buildRestrictedModelGrid(allMethods);
            if (result.split("\n").length > 1) {
                outputSteam.println(result);
            }
        }
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        return null;
    }

    /**
     * @param parameters
     * @return
     * @throws BusinessException
     * @throws NoExtractionResultException
     */
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        final Map<String, List> results = parameters.getResults().get(this.getFileName());
        try {
            ((DefaultParameter) parameters).getFilesMaps().add(this.buildBody(org.apache.commons.lang.StringUtils.EMPTY, results, parameters.getParameters()));
        } catch (NoExtractionResultException e) {
            LOGGER.info("no result for method ", e);
        }
        return null;
    }

    /**
     * @return
     */
    protected abstract String getFileName();

    /**
     * @return
     */
    public T getRecorder() {
        if (this.recorder == null) {
            try {
                this.recorder = (T) this.metadataRecorderDAO.getMetadataRecorderByMetadataName(this.recorderName);
            } catch (PersistenceException ex) {
                AbstractOutputBuilder.LOGGER.debug(ex.getMessage(), ex);
            }
        }
        return this.recorder;
    }

    /**
     * @param metadataRecorderDAO
     */
    public void setMetadataRecorderDAO(IMetadataRecorderDAO metadataRecorderDAO) {
        this.metadataRecorderDAO = metadataRecorderDAO;
    }

    /**
     * @param recorderName
     */
    public void setRecorderName(String recorderName) {
        this.recorderName = recorderName;
    }

}
