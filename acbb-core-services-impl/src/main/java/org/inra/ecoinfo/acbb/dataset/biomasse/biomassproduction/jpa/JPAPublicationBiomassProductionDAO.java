/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.jpa;


import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction_;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationBiomassProductionDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {
    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeur(version);
        deleteBiomasse(version);
    }

    private void deleteValeur(VersionFile versionFile) {
        CriteriaDelete<ValeurBiomassProduction> deletevaleur = builder.createCriteriaDelete(ValeurBiomassProduction.class);
        Root<ValeurBiomassProduction> v = deletevaleur.from(ValeurBiomassProduction.class);
        Subquery<BiomassProduction> subqueryBiomassProduction = deletevaleur.subquery(BiomassProduction.class);
        Root<BiomassProduction> biomass = subqueryBiomassProduction.from(BiomassProduction.class);
        subqueryBiomassProduction
                .select(biomass)
                .where(builder.equal(biomass.get(AbstractBiomasse_.version), versionFile));
        Path<BiomassProduction> mesureDB = v.get(ValeurBiomassProduction_.biomassProduction);
        deletevaleur
                .where(mesureDB.in(subqueryBiomassProduction));
        delete(deletevaleur);
    }

    private void deleteBiomasse(VersionFile versionFile) {
        CriteriaDelete<BiomassProduction> deleteBiomasse = builder.createCriteriaDelete(BiomassProduction.class);
        Root<BiomassProduction> mesure = deleteBiomasse.from(BiomassProduction.class);
        deleteBiomasse
                .where(builder.equal(mesure.get(AbstractBiomasse_.version), versionFile));
        delete(deleteBiomasse);
    }
}
