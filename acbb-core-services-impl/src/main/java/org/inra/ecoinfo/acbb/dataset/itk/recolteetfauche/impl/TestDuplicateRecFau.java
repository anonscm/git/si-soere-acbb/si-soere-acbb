/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.*;

/**
 * The Class TestDuplicateWsol.
 *
 * implementation for Tillage of {@link ITestDuplicates}
 *
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux
 * files
 */
public class TestDuplicateRecFau extends AbstractTestDuplicate {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String ACBB_DATASET_RECFAU_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE @link(String).
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The line @link(SortedMap<String,Long>).
     */
    final SortedMap<String, Long> line;

    @Transient
    final PeriodesRecFau periodesPaturage;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateRecFau() {
        this.line = new TreeMap();
        this.periodesPaturage = new PeriodesRecFau();
    }

    /**
     * Adds the line.
     *
     * @param parcelle
     * @param date the date
     * @param lineNumber the line number
     * @param passage
     */
    protected void addLine(final String parcelle, final String date, final String passage,
                           final long lineNumber) {
        final String key = this.getKey(parcelle, date, passage);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateRecFau.ACBB_DATASET_RECFAU_BUNDLE_NAME,
                    TestDuplicateRecFau.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle, date,
                    passage, this.line.get(key)));
        }

    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[6], lineNumber + 1);
    }

    static class PeriodesRecFau implements Serializable {

        private static final long serialVersionUID = 1L;
        @Transient
        final Map<String, List<IntervalDate>> periodes = new HashMap();

        void testInterval(final String parcelle, final IntervalDate intervalDate) {
            for (final IntervalDate interval : this.periodes.get(parcelle)) {
                if (intervalDate.overlaps(interval)) {
                    // TODO(ptcherniati) :
                    // org.inra.ecoinfo.acbb.dataset.itk.paturage.impl/TestDuplicatePaturage.PeriodesPaturage::testInterval()
                    //
                    UncatchedExceptionLogger.logUncatchedException("overlaps non traité",
                            new BusinessException());
                }
            }
        }
    }
}
