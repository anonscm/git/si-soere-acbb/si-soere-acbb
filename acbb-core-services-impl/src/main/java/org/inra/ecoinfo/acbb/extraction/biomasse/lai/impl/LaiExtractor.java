package org.inra.ecoinfo.acbb.extraction.biomasse.lai.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.AbstractBiomasseExtractor;

/**
 * @author ptcherniati
 */
public class LaiExtractor extends AbstractBiomasseExtractor<Lai> {

}
