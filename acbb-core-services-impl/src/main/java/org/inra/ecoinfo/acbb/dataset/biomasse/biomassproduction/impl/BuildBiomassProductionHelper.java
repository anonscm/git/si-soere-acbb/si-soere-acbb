package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.IBiomassProductionDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati, vkoyao
 */
public class BuildBiomassProductionHelper extends AbstractBuildHelper<BiomassProductionLineRecord> {

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl.messages";

    Map<Parcelle, Map<LocalDate, Map<Long, BiomassProduction>>> biomassProductions = new HashMap();
    IBiomassProductionDAO biomassProductionDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursBiomassProduction(final BiomassProductionLineRecord line,
                                     final BiomassProduction biomassProduction) {
        for (final VariableValueBiomassProduction variableValue : line.getVariablesValues()) {

            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurBiomassProduction valeur;
            valeur = new ValeurBiomassProduction(Float.parseFloat(variableValue.getValue()),
                    realNode, variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(), biomassProduction,
                    variableValue.getQualityClass(), variableValue.getMethode());
            biomassProduction.getValeursBiomassProduction().add(valeur);
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    protected void build() throws PersistenceException {
        for (final BiomassProductionLineRecord line : this.lines) {
            final BiomassProduction biomassProduction = this.getOrCreateBiomassProduction(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursBiomassProduction(line, biomassProduction);
            this.biomassProductionDAO.saveOrUpdate(biomassProduction);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesBiomasse
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesBiomasse the request properties Biomasse
     */
    public void build(final VersionFile version, final List<BiomassProductionLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesBiomasse requestPropertiesBiomasse) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesBiomasse);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    BiomassProduction createNewBiomassProduction(final BiomassProductionLineRecord line) {
        BiomassProduction biomassProduction = null;
        biomassProduction = new BiomassProduction(line.getNatureCouvert(), line.getMasseVegClass(),
                line.getDateDebutRepousse(), this.version, BiomassProduction.BIOMASS_PRODUCTION,
                line.getObservation(), line.getDate(), line.getSerie(), line.getRotationNumber(),
                line.getAnneeNumber(), line.getPeriodeNumber(), line.getSuiviParcelle(),
                line.getTypeIntervention(), line.getIntervention());
        try {
            this.biomassProductionDAO.saveOrUpdate(biomassProduction);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return biomassProduction;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    BiomassProduction getOrCreateBiomassProduction(final BiomassProductionLineRecord line) {
        return biomassProductions
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getMasseVegClass().getId(), k -> this.createNewBiomassProduction(line));
    }

    /**
     * @param biomassProductionDAO the Biomass Production DAO to set
     */
    public void setBiomassProductionDAO(IBiomassProductionDAO biomassProductionDAO) {
        this.biomassProductionDAO = biomassProductionDAO;
    }
}
