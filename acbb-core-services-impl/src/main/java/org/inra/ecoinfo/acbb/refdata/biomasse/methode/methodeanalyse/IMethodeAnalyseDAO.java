/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

/**
 * @author koyao
 */
public interface IMethodeAnalyseDAO extends IMethodeDAO<MethodeAnalyse> {
}
