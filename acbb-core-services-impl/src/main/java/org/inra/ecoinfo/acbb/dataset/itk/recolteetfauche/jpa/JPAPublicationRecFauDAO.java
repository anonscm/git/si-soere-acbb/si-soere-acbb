/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.*;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Query;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationRecFauDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    static final String QUERY_DELETE_PERIODS_BINDED = "delete from "
            + PeriodeRealisee.NAME_ENTITY_SEMIS_PREAL
            + " where "
            + AbstractIntervention.ID_JPA
            + " in (select "
            + AbstractIntervention.ID_JPA
            + " from "
            + Semis.NAME_ENTITY_JPA
            + " where "
            + VersionFile.ID_JPA
            + "=:version)";
    static final String QUERY_DELETE_YEARS_BINDED = "delete from "
            + AnneeRealisee.NAME_ENTITY_SEMIS_AREAL
            + " where "
            + AbstractIntervention.ID_JPA
            + " in (select "
            + AbstractIntervention.ID_JPA
            + " from "
            + Semis.NAME_ENTITY_JPA
            + " where "
            + VersionFile.ID_JPA
            + "=:version)";
    static final String QUERY_DELETE_ROTATIONS_BINDED = "delete from "
            + VersionTraitementRealisee.NAME_ENTITY_SEMIS_TREAL
            + " where "
            + AbstractIntervention.ID_JPA
            + " in (select "
            + AbstractIntervention.ID_JPA
            + " from "
            + Semis.NAME_ENTITY_JPA
            + " where "
            + VersionFile.ID_JPA
            + "=:version)";
    static final String QUERY_UPDATE_UNBINDED_TEMPO = "update "
            + Semis.NAME_ENTITY_JPA
            + " set "
            + Tempo.ID_JPA
            + "= null where "
            + AbstractIntervention.ID_JPA
            + " in (select "
            + AbstractIntervention.ID_JPA
            + " from "
            + Semis.NAME_ENTITY_JPA
            + " where "
            + VersionFile.ID_JPA
            + "=:version)";
    static final String QUERY_DELETE_NOT_BINDED_TEMPO = "delete from "
            + Tempo.NAME_ENTITY_JPA
            + " cascade where "
            + PeriodeRealisee.ID_JPA
            + " not in (select "
            + PeriodeRealisee.ID_JPA
            + " from "
            + PeriodeRealisee.NAME_ENTITY_SEMIS_PREAL
            + ") and "
            + AnneeRealisee.ID_JPA
            + " not in (select "
            + AnneeRealisee.ID_JPA
            + " from "
            + AnneeRealisee.NAME_ENTITY_SEMIS_AREAL
            + ") and "
            + VersionTraitementRealisee.ID_JPA
            + " not in (select "
            + VersionTraitementRealisee.ID_JPA
            + " from "
            + VersionTraitementRealisee.NAME_ENTITY_SEMIS_TREAL
            + ")";
    static final String QUERY_DELETE_NOT_BINDED_PERIODS = "delete from "
            + PeriodeRealisee.NAME_ENTITY_JPA
            + " cascade where "
            + PeriodeRealisee.ID_JPA
            + " not in (select "
            + PeriodeRealisee.ID_JPA
            + " from "
            + PeriodeRealisee.NAME_ENTITY_SEMIS_PREAL
            + ")";
    static final String QUERY_DELETE_NOT_BINDED_YEARS = "delete from "
            + AnneeRealisee.NAME_ENTITY_JPA
            + " cascade where "
            + AnneeRealisee.ID_JPA
            + " not in (select "
            + AnneeRealisee.ID_JPA
            + " from "
            + AnneeRealisee.NAME_ENTITY_SEMIS_AREAL
            + ")";
    static final String QUERY_DELETE_NOT_BINDED_ROTATIONS = "delete from "
            + VersionTraitementRealisee.NAME_ENTITY_JPA
            + " cascade where "
            + VersionTraitementRealisee.ID_JPA
            + " not in (select "
            + VersionTraitementRealisee.ID_JPA
            + " from "
            + VersionTraitementRealisee.NAME_ENTITY_SEMIS_TREAL
            + ")";
    /**
     * The Constant QUERY_DELETE_SEQUENCE @link(String).
     */
    static final String QUERY_DELETE_SEQUENCE = "delete from Semis sem where sem.version=:version";
    /**
     * The Constant QUERY_UPDATE_SEM_SEM_DEPTH @link(String).
     */
    static final String QUERY_UPDATE_SEM_SEM_DEPTH = "update valeur_semis_vsema set itk_id=null where itk_id in (select itk_id from semis_sem where ivf_id = :version)";
    /**
     * The Constant QUERY_UPDATE_SEM_SEM_DEPTH @link(String).
     */
    static final String QUERY_DELETE_SEM_SEM_DEPTH = "delete from valeur_semis_vsema where itk_id is null";
    /**
     * The Constant QUERY_DELETE_SEM_SEM_OBJECTIFS @link(String).
     */
    static final String QUERY_DELETE_SEM_SEM_OBJECTIFS = "delete from msem_semobj	 where itk_id in (select itk_id from semis_sem where ivf_id = :version)";
    /**
     * The Constant QUERY_DELETE_MEASURE @link(String).
     */
    static final String QUERY_DELETE_MEASURE = "delete from MesureSemis where  semis in (select id from Semis sem where sem.version=:version)";
    /**
     * The Constant QUERY_DELETE_VALEUR @link(String).
     */
    static final String QUERY_DELETE_VALEUR = "delete from ValeurSemis where mesureSemis in (select id from MesureSemis msem where  semis in (select id from Semis sem where sem.version=:version))";
    private static final String CST_VERSION = "version";

    void removeBindedsRealises(final VersionFile version) {
        final String requeteDeletePeriodBindeds = JPAPublicationRecFauDAO.QUERY_DELETE_PERIODS_BINDED;
        final String requeteDeleteYearsBindeds = JPAPublicationRecFauDAO.QUERY_DELETE_YEARS_BINDED;
        final String requeteDeleteRotationBindeds = JPAPublicationRecFauDAO.QUERY_DELETE_ROTATIONS_BINDED;
        final String requeteUpdateUnbindedTempos = JPAPublicationRecFauDAO.QUERY_UPDATE_UNBINDED_TEMPO;
        final String requeteDeleteNotBindedTempos = JPAPublicationRecFauDAO.QUERY_DELETE_NOT_BINDED_TEMPO;
        final String requeteDeleteNotBindedPeriod = JPAPublicationRecFauDAO.QUERY_DELETE_NOT_BINDED_PERIODS;
        final String requeteDeleteNotBindedsYears = JPAPublicationRecFauDAO.QUERY_DELETE_NOT_BINDED_YEARS;
        final String requeteDeleteNotBindedsRotation = JPAPublicationRecFauDAO.QUERY_DELETE_NOT_BINDED_ROTATIONS;
        Query query;
        query = this.entityManager.createNativeQuery(requeteDeletePeriodBindeds);
        query.setParameter(JPAPublicationRecFauDAO.CST_VERSION, version.getId());
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteDeleteYearsBindeds);
        query.setParameter(JPAPublicationRecFauDAO.CST_VERSION, version.getId());
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteDeleteRotationBindeds);
        query.setParameter(JPAPublicationRecFauDAO.CST_VERSION, version.getId());
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteUpdateUnbindedTempos);
        query.setParameter(JPAPublicationRecFauDAO.CST_VERSION, version.getId());
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteDeleteNotBindedTempos);
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteDeleteNotBindedPeriod);
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteDeleteNotBindedsYears);
        query.executeUpdate();
        query = this.entityManager.createNativeQuery(requeteDeleteNotBindedsRotation);
        query.executeUpdate();
    }

    /**
     * Removes the version.
     *
     * @param version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion(org.
     *                              inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
    }

}
