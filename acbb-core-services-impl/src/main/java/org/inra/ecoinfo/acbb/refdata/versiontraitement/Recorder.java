/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.versiontraitement;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<VersionDeTraitement> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.versiontraitement.messages";

    /**
     * The Constant PROPERTY_MSG_BAD_DATE_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_BAD_DATE_FORMAT = "PROPERTY_MSG_BAD_DATE_FORMAT";

    /**
     * The Constant PROPERTY_MSG_BAD_VERSION_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_BAD_VERSION_FORMAT = "PROPERTY_MSG_BAD_VERSION_FORMAT";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_MODALITE_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_MODALITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_MODALITE_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_INVALID_INTERVAL_DATE @link(String).
     */
    static final String PROPERTY_MSG_INVALID_INTERVAL_DATE = "PROPERTY_MSG_INVALID_INTERVAL_DATE";

    /**
     * The Constant PROPERTY_MSG_BAD_VALUE_OR_VERSION @link(String).
     */
    static final String PROPERTY_MSG_BAD_VALUE_OR_VERSION = "PROPERTY_MSG_BAD_VALUE_OR_VERSION";

    /**
     * The Constant PROPERTY_MSG_BAD_BEGIN_DATE @link(String).
     */
    static final String PROPERTY_MSG_BAD_BEGIN_DATE = "PROPERTY_MSG_BAD_BEGIN_DATE";

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    /**
     * The traitement possible @link(Map<String,String[]>).
     */
    final Map<String, String[]> traitementPossible = new ConcurrentHashMap();
    /**
     * The version de traitement dao.
     */
    IVersionDeTraitementDAO versionDeTraitementDAO;
    /**
     * The modalite dao.
     */
    IModaliteDAO modaliteDAO;
    /**
     * The site acbbdao.
     */
    ISiteACBBDAO siteACBBDAO;
    /**
     * The traitement dao.
     */
    ITraitementDAO traitementDAO;
    /**
     * The properties commentaire en @link(Properties).
     */
    Properties propertiesCommentaireEN;
    /**
     * The sites possibles @link(String[]).
     */
    String[] sitesPossibles;

    /**
     * Compare dates.
     *
     * @param errorsReport
     * @param dateDebut
     * @param dateFin
     * @param dateDebutString
     * @param dateFinString
     * @param dbTraitement
     * @param version
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the date debut string
     * @link(String) the date fin string
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the date debut string
     * @link(String) the date fin string
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     */
    void compareDates(final ErrorsReport errorsReport, final LocalDate dateDebut, final LocalDate dateFin,
                      final String dateDebutString, final String dateFinString,
                      final TraitementProgramme dbTraitement, final int version) {
        if (dateDebut != null && dateFin != null) {
            try {
                IntervalDate intervalDate = new IntervalDate(dateDebut.atStartOfDay(), dateFin.atStartOfDay(), DateUtil.DD_MM_YYYY);
                if (intervalDate == null) {
                    throw new BadExpectedValueException();
                }
            } catch (final BadExpectedValueException e) {
                errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                        Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_INVALID_INTERVAL_DATE),
                        dateDebutString, dateFinString));
            }
            this.updatePreviousVersion(errorsReport, dateDebut, dbTraitement, version);
        }
    }

    /**
     * Creates the or update variable de forcage.
     *
     * @param dbSite
     * @param dbTraitement
     * @param version
     * @param dateDebut
     * @param dateFin
     * @param commentaire
     * @param dbModalites
     * @param dbVersionDeTraitement
     * @throws PersistenceException the persistence exception
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the commentaire
     * @link(List<Modalite>) the db modalites
     * @link(VersionDeTraitement) the db version de traitement
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the commentaire
     * @link(List<Modalite>) the db modalites
     * @link(VersionDeTraitement) the db version de traitement
     */
    void createOrUpdateVariableDeForcage(final SiteACBB dbSite,
                                         final TraitementProgramme dbTraitement, final int version, final LocalDate dateDebut,
                                         final LocalDate dateFin, final String commentaire, final SortedSet<Modalite> dbModalites,
                                         final VersionDeTraitement dbVersionDeTraitement) throws PersistenceException {
        if (dbVersionDeTraitement == null) {
            final VersionDeTraitement versionDeTraitement = new VersionDeTraitement(version,
                    dateDebut, dateFin, commentaire, dbTraitement, dbModalites);
            for (final Modalite modalite : dbModalites) {
                modalite.getVersionDeTraitements().add(versionDeTraitement);
                this.modaliteDAO.saveOrUpdate(modalite);
            }
            this.versionDeTraitementDAO.saveOrUpdate(versionDeTraitement);
            this.versionDeTraitementDAO.flush();
        } else {
            dbVersionDeTraitement.setCommentaire(commentaire);
            dbVersionDeTraitement.setDateDebutVersionTraitement(dateDebut);
            dbVersionDeTraitement.setDateFinVersionTraitement(dateFin);
            dbVersionDeTraitement.setTraitementProgramme(dbTraitement);
            dbVersionDeTraitement.setVersion(version);
            dbVersionDeTraitement.setModalites(dbModalites);
            for (final Modalite modalite : dbModalites) {
                modalite.getVersionDeTraitements().add(dbVersionDeTraitement);
                dbVersionDeTraitement.getModalites().add(modalite);
            }
            this.versionDeTraitementDAO.saveOrUpdate(dbVersionDeTraitement);
            this.versionDeTraitementDAO.flush();
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeTraitement = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                final int version = Integer.parseInt(tokenizerValues.nextToken());
                this.versionDeTraitementDAO.remove(this.versionDeTraitementDAO.getByNKey(siteCode,
                        codeTraitement, version)
                        .orElseThrow(() -> new PersistenceException("bad version de traitement")));
            }
        } catch (final IOException | NumberFormatException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<VersionDeTraitement> getAllElements() {
        List<VersionDeTraitement> all = this.versionDeTraitementDAO
                .getAll(VersionDeTraitement.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the date debut.
     *
     * @param errorsReport
     * @param finalDateDebut
     * @param dateDebutString
     * @return the date debut
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut
     * @link(String) the date debut string
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut
     * @link(String) the date debut string
     */
    LocalDate getDateDebut(final ErrorsReport errorsReport, final LocalDate dateDebut,
                           final String dateDebutString) {
        LocalDate finalDateDebut = dateDebut;
        try {
            finalDateDebut = dateDebutString != null && dateDebutString.length() > 0
                    ? DateUtil.readLocalDateFromText(this.datasetDescriptor.getColumns().get(4).getFormatType(), dateDebutString)
                    : null;
        } catch (final DateTimeException e) {
            LOGGER.debug(e.getMessage(), e);
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_DATE_FORMAT),
                    dateDebutString, this.datasetDescriptor.getColumns().get(4).getFormatType()));
        }
        return finalDateDebut;
    }

    /**
     * Gets the date fin.
     *
     * @param errorsReport
     * @param finalDateFin
     * @param dateFinString
     * @return the date fin
     * @link(ErrorsReport) the errors report
     * @link(Date) the date fin
     * @link(String) the date fin string
     * @link(ErrorsReport) the errors report
     * @link(Date) the date fin
     * @link(String) the date fin string
     */
    LocalDate getDateFin(final ErrorsReport errorsReport, final LocalDate dateFin, final String dateFinString) {
        LocalDate finalDateFin = dateFin;
        if (Strings.isNullOrEmpty(dateFinString)) {
            return null;
        }
        try {
            finalDateFin = Strings.isNullOrEmpty(dateFinString)
                    ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(5).getFormatType(), dateFinString);
        } catch (final DateTimeException e) {
            LOGGER.debug(e.getMessage(), e);
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_DATE_FORMAT),
                    dateFinString, this.datasetDescriptor.getColumns().get(5).getFormatType()));
        }
        return finalDateFin;
    }

    /**
     * Gets the modalites.
     *
     * @param modalitesString
     * @return the modalites
     * @throws Exception the exception
     * @link(String) the modalites string
     * @link(String) the modalites string
     */
    SortedSet<Modalite> getModalites(final String modalitesString) throws BusinessException {
        final String[] modalitesStringList = modalitesString.split(",");
        final SortedSet<Modalite> modalites = new TreeSet();
        for (final String code : modalitesStringList) {
            final Modalite modalite = this.modaliteDAO.getByCode(code).orElse(null);
            if (modalite == null) {
                throw new BusinessException(String.format(this.localizationManager.getMessage(
                        Recorder.BUNDLE_SOURCE_PATH,
                        Recorder.PROPERTY_MSG_ERROR_CODE_MODALITE_NOT_FOUND_IN_DB), code,
                        modalitesString));
            } else modalites.add(modalite);
        }
        return modalites;

    }

    /**
     * Gets the names sites possibles.
     *
     * @return the names sites possibles
     * @throws PersistenceException the persistence exception
     */
    String[] getNamesSitesPossibles() throws PersistenceException {
        final List<SiteACBB> sites = this.siteACBBDAO.getAllSitesACBB();
        final String[] namesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (final Site site : sites) {
            namesSitesPossibles[index++] = site.getName();
        }
        return namesSitesPossibles;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param versionDeTraitement
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(VersionDeTraitement) the version de traitement
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final VersionDeTraitement versionDeTraitement) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata siteColumn = new ColumnModelGridMetadata(
                versionDeTraitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : versionDeTraitement.getTraitementProgramme().getSite().getName(),
                this.sitesPossibles, null, true, false, true);
        final ColumnModelGridMetadata traitementColumn = new ColumnModelGridMetadata(
                versionDeTraitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : versionDeTraitement.getTraitementProgramme().getNom(),
                this.traitementPossible, null, true, false, true);
        final List<ColumnModelGridMetadata> refsSite = new LinkedList();
        refsSite.add(traitementColumn);
        siteColumn.setRefs(refsSite);
        traitementColumn
                .setValue(versionDeTraitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : versionDeTraitement.getTraitementProgramme().getNom());
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(siteColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(traitementColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        versionDeTraitement == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : versionDeTraitement.getVersion(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        final String dateDebut;
        final String dateFin;
        dateDebut = versionDeTraitement == null
                || versionDeTraitement.getDateDebutVersionTraitement() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : DateUtil.getUTCDateTextFromLocalDateTime(versionDeTraitement.getDateDebutVersionTraitement(), DateUtil.DD_MM_YYYY);
        dateFin = versionDeTraitement == null
                || versionDeTraitement.getDateFinVersionTraitement() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : DateUtil.getUTCDateTextFromLocalDateTime(versionDeTraitement.getDateFinVersionTraitement(), DateUtil.DD_MM_YYYY);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dateDebut, ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dateFin, ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, true));
        lineModelGridMetadata
                .getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(
                        versionDeTraitement == null || versionDeTraitement.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : versionDeTraitement.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata
                .getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(
                        versionDeTraitement == null || versionDeTraitement.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesCommentaireEN.getProperty(
                                versionDeTraitement.getCommentaire(),
                                versionDeTraitement.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata
                .getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(
                        versionDeTraitement == null || versionDeTraitement.getModalites().isEmpty() ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.modalitesToString(versionDeTraitement.getModalites()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Gets the version.
     *
     * @param errorsReport
     * @param values
     * @param tokenizerValues
     * @param finalVersion
     * @return the version
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String[]) the values
     * @link(TokenizerValues) the tokenizer values
     * @link(int) the version
     * @link(ErrorsReport) the errors report
     * @link(String[]) the values
     * @link(TokenizerValues) the tokenizer values
     * @link(int) the version
     */
    int getVersion(final ErrorsReport errorsReport, final String[] values,
                   final TokenizerValues tokenizerValues, final int version) throws BusinessException {
        int finalVersion = version;
        try {
            finalVersion = Integer.parseInt(tokenizerValues.nextToken());
        } catch (final NumberFormatException e) {
            LOGGER.debug(e.getMessage(), e);
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_VERSION_FORMAT),
                    values[2]));
        }
        return finalVersion;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<VersionDeTraitement> initModelGridMetadata() {
        try {
            this.sitesPossibles = this.getNamesSitesPossibles();
            this.updateNamesTraitementPossibles();
            this.propertiesCommentaireEN = this.localizationManager.newProperties(
                    VersionDeTraitement.NAME_ENTITY_JPA,
                    VersionDeTraitement.ATTRIBUTE_JPA_COMMENTAIRE, Locale.ENGLISH);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Modalites to string.
     *
     * @param modalites
     * @return the string
     * @link(List<Modalite>) the modalites
     * @link(List<Modalite>) the modalites
     */
    String modalitesToString(final SortedSet<Modalite> modalites) {
        final StringBuffer modalitesToString = new StringBuffer();
        for (final Modalite modalite : modalites) {
            modalitesToString.append(modalitesToString.length() > 0 ? "," : org.apache.commons.lang.StringUtils.EMPTY).append(
                    modalite.getCode());
        }
        return modalitesToString.toString();
    }

    /**
     * Persist version traitement.
     *
     * @param errorsReport
     * @param dbSite
     * @param dbTraitement
     * @param version
     * @param dateDebut
     * @param dateFin
     * @param commentaire
     * @param dbModalites
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the commentaire
     * @link(List<Modalite>) the db modalites
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the commentaire
     * @link(List<Modalite>) the db modalites
     */
    void persistVersionTraitement(final ErrorsReport errorsReport, final SiteACBB dbSite,
                                  final TraitementProgramme dbTraitement, final int version, final LocalDate dateDebut,
                                  final LocalDate dateFin, final String commentaire, final SortedSet<Modalite> dbModalites)
            throws PersistenceException {
        final VersionDeTraitement dbVersionTraitement = this.versionDeTraitementDAO.getByNKey(
                dbSite.getCode(), dbTraitement.getCode(), version)
                .orElse(null);
        this.createOrUpdateVariableDeForcage(dbSite, dbTraitement, version, dateDebut, dateFin,
                commentaire, dbModalites, dbVersionTraitement);
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        VersionDeTraitement.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                SiteACBB dbSite = null;
                TraitementProgramme dbTraitement = null;
                int version = 0;
                LocalDate dateDebut = null;
                LocalDate dateFin = null;
                final String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                dbSite = this.retrieveDBSite(errorsReport, dbSite, siteCode);
                final String traitementProgrammeCode = Utils.createCodeFromString(tokenizerValues
                        .nextToken());
                dbTraitement = this.retrieveDBTraitement(errorsReport, dbSite, dbTraitement,
                        siteCode, traitementProgrammeCode);
                version = this.getVersion(errorsReport, values, tokenizerValues, version);
                this.testVersion(errorsReport, dbTraitement, version, traitementProgrammeCode);
                final String dateDebutString = tokenizerValues.nextToken();
                final String dateFinString = tokenizerValues.nextToken();
                dateDebut = this.getDateDebut(errorsReport, dateDebut, dateDebutString);
                dateFin = this.getDateFin(errorsReport, dateFin, dateFinString);
                this.compareDates(errorsReport, dateDebut, dateFin, dateDebutString, dateFinString,
                        dbTraitement, version);
                final String commentaire = tokenizerValues.nextToken();
                final String modalitesString = tokenizerValues.nextToken();
                this.retrieveDBModalites(errorsReport, dbSite, dbTraitement, version, dateDebut,
                        dateFin, commentaire, modalitesString);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Retireve db modalites.
     *
     * @param errorsReport
     * @param dbSite
     * @param dbTraitement
     * @param version
     * @param finalDbModalites
     * @param dateDebut
     * @param dateFin
     * @param commentaire
     * @param modalitesString
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(List<Modalite>) the db modalites
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the commentaire
     * @link(String) the modalites string
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(List<Modalite>) the db modalites
     * @link(Date) the date debut
     * @link(Date) the date fin
     * @link(String) the commentaire
     * @link(String) the modalites string
     */
    void retrieveDBModalites(final ErrorsReport errorsReport, final SiteACBB dbSite,
                             final TraitementProgramme dbTraitement, final int version, final LocalDate dateDebut,
                             final LocalDate dateFin, final String commentaire, final String modalitesString)
            throws PersistenceException {
        SortedSet<Modalite> finalDbModalites = null;
        try {
            finalDbModalites = this.getModalites(modalitesString);
        } catch (final BusinessException e) {
            LOGGER.debug(e.getMessage(), e);
            errorsReport.addErrorMessage(e.getMessage());
        }
        if (!errorsReport.hasErrors()) {
            this.persistVersionTraitement(errorsReport, dbSite, dbTraitement, version, dateDebut,
                    dateFin, commentaire, finalDbModalites);
        }
    }

    /**
     * Retrieve db site.
     *
     * @param errorsReport
     * @param finalDbSite
     * @param siteCode
     * @return the site acbb
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(String) the site code
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(String) the site code
     */
    SiteACBB retrieveDBSite(final ErrorsReport errorsReport, final SiteACBB dbSite,
                            final String siteCode) {
        Optional<SiteACBB> finalDbSite = this.siteACBBDAO.getByPath(siteCode)
                .map(s -> (SiteACBB) s);

        if (!finalDbSite.isPresent()) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), siteCode));
        }
        return finalDbSite.orElse(null);
    }

    /**
     * Retrieve db traitement.
     *
     * @param errorsReport
     * @param dbSite
     * @param finalDbTraitement
     * @param siteCode
     * @param traitementProgrammeCode
     * @return the traitement programme
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(String) the site code
     * @link(String) the traitement programme code
     * @link(ErrorsReport) the errors report
     * @link(SiteACBB) the db site
     * @link(TraitementProgramme) the db traitement
     * @link(String) the site code
     * @link(String) the traitement programme code
     */
    TraitementProgramme retrieveDBTraitement(final ErrorsReport errorsReport,
                                             final SiteACBB dbSite, final TraitementProgramme dbTraitement, final String siteCode,
                                             final String traitementProgrammeCode) {
        TraitementProgramme finalDbTraitement = dbTraitement;
        if (dbSite != null) {
            finalDbTraitement = this.traitementDAO.getByNKey(dbSite.getId(),
                    traitementProgrammeCode).orElse(null);
            if (finalDbTraitement == null) {
                errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                        Recorder.BUNDLE_SOURCE_PATH,
                        Recorder.PROPERTY_MSG_ERROR_CODE_TRAITEMENT_NOT_FOUND_IN_DB),
                        traitementProgrammeCode, siteCode));
            }
        }
        return finalDbTraitement;
    }

    /**
     * Sets the modalite dao.
     *
     * @param modaliteDAO the new modalite dao
     */
    public final void setModaliteDAO(final IModaliteDAO modaliteDAO) {
        this.modaliteDAO = modaliteDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteACBBDAO the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteACBBDAO) {
        this.siteACBBDAO = siteACBBDAO;
    }

    /**
     * Sets the traitement dao.
     *
     * @param traitementDAO the new traitement dao
     */
    public final void setTraitementDAO(final ITraitementDAO traitementDAO) {
        this.traitementDAO = traitementDAO;
    }

    /**
     * Sets the version de traitement dao.
     *
     * @param versionDeTraitementDAO the new version de traitement dao
     */
    public final void setVersionDeTraitementDAO(final IVersionDeTraitementDAO versionDeTraitementDAO) {
        this.versionDeTraitementDAO = versionDeTraitementDAO;
    }

    /**
     * Test version.
     *
     * @param errorsReport
     * @param dbTraitement
     * @param version
     * @param traitementProgrammeCode
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(String) the traitement programme code
     * @link(ErrorsReport) the errors report
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(String) the traitement programme code
     */
    void testVersion(final ErrorsReport errorsReport, final TraitementProgramme dbTraitement,
                     final int version, final String traitementProgrammeCode) throws PersistenceException {
        final int lastVersion = this.versionDeTraitementDAO.getLastVersion(dbTraitement);
        if (version > lastVersion + 1 || version <= 0) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_VALUE_OR_VERSION),
                    version, lastVersion + 1, traitementProgrammeCode));
        }
    }

    /**
     * Update names traitement possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    void updateNamesTraitementPossibles() throws PersistenceException {
        this.traitementPossible.clear();
        final List<TraitementProgramme> traitementProgrammes = this.traitementDAO.getAll();
        final Map<String, List<String>> traitementsList = new HashMap();
        for (final TraitementProgramme traitementProgramme : traitementProgrammes) {
            if (!traitementsList.containsKey(traitementProgramme.getSite().getName())) {
                traitementsList.put(traitementProgramme.getSite().getName(),
                        new LinkedList());
            }
            traitementsList.get(traitementProgramme.getSite().getName()).add(
                    traitementProgramme.getNom());
        }
        for (final Entry<String, List<String>> traitementEntry : traitementsList.entrySet()) {
            this.traitementPossible.put(traitementEntry.getKey(), traitementEntry.getValue()
                    .toArray(new String[]{}));
        }
    }

    /**
     * Update previous version.
     *
     * @param errorsReport
     * @param dateDebut
     * @param dbTraitement
     * @param version
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     * @link(ErrorsReport) the errors report
     * @link(Date) the date debut
     * @link(TraitementProgramme) the db traitement
     * @link(int) the version
     */
    void updatePreviousVersion(final ErrorsReport errorsReport, final LocalDate dateDebut,
                               final TraitementProgramme dbTraitement, final int version) {
        try {
            final int previousVersion = this.versionDeTraitementDAO.getpreviousVersion(
                    dbTraitement, version);
            if (previousVersion <= 0) {
                return;
            }
            final VersionDeTraitement previousVersionTraitement = this.versionDeTraitementDAO
                    .getpreviousVersionTraitement(dbTraitement, previousVersion)
                    .orElse(null);
            if (previousVersionTraitement.getDateDebutVersionTraitement().isAfter(dateDebut)) {
                String formatDateDebut;
                String formatDateVersion;
                formatDateDebut = DateUtil.getUTCDateTextFromLocalDateTime(dateDebut, DateUtil.DD_MM_YYYY);
                formatDateVersion = DateUtil.getUTCDateTextFromLocalDateTime(previousVersionTraitement.getDateDebutVersionTraitement(), DateUtil.DD_MM_YYYY);
                errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                        Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_BEGIN_DATE),
                        formatDateDebut, formatDateVersion));
            } else {
                previousVersionTraitement.setDateFinVersionTraitement(dateDebut);
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }
}
