/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.lai.impl;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class TestDuplicateAI.
 *
 * test if the file has duplicates lines
 */
public class TestDuplicateLai extends AbstractTestDuplicate {

    /**
     * The Constant ACBB_DATASET_AI_BUNDLE_NAME.
     */
    public static final String ACBB_DATASET_LAI_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.lai.messages";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE.
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The line.
     */
    final SortedMap<String, Long> line;

    /**
     * Instantiates a new test duplicate ai.
     */
    public TestDuplicateLai() {
        this.line = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param parcelle
     * @param nature
     * @link{String the parcelle
     * @param date
     * @link{String the date
     * @param lineNumber
     * @link{long the line number
     */
    protected void addLine(final String parcelle, final String nature, final String date, final long lineNumber) {
        final String key = this.getKey(parcelle, nature, date);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateLai.ACBB_DATASET_LAI_BUNDLE_NAME,
                    TestDuplicateLai.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle, nature, date,
                    this.line.get(key)));
        }

    }

    /**
     * @param values
     * @param versionFile
     * @param lineNumber
     * @param dates
     * @see
     * org.inra.ecoinfo.acbb.dataset.ITestDuplicates#addLine(java.lang.String[],
     * long)
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[8], values[10], lineNumber + 1);
    }
}
