package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;

import java.time.LocalDate;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class BiomassProductionLineRecord extends AbstractLineRecord<BiomassProductionLineRecord, VariableValueBiomassProduction> {
    MasseVegClass masseVegClass;
    LocalDate dateDebutRepousse = null;

    /**
     * @param originalLineNumber
     */
    public BiomassProductionLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }

    /**
     * @return
     */
    public LocalDate getDateDebutRepousse() {
        return this.dateDebutRepousse;
    }

    /**
     * @param dateDebutRepousse
     */
    public void setDateDebutRepousse(LocalDate dateDebutRepousse) {
        this.dateDebutRepousse = dateDebutRepousse;
    }

    /**
     * @return
     */
    public MasseVegClass getMasseVegClass() {
        return this.masseVegClass;
    }

    /**
     * @param masseVegClass
     */
    public void setMasseVegClass(MasseVegClass masseVegClass) {
        this.masseVegClass = masseVegClass;
    }

    /**
     * @param masseVegClass
     * @param dateDebutRepousse
     */
    public void updateBiomassProductionLineRecord(MasseVegClass masseVegClass,
                                                  LocalDate dateDebutRepousse) {
        this.dateDebutRepousse = dateDebutRepousse;
        this.masseVegClass = masseVegClass;
    }

}
