package org.inra.ecoinfo.acbb.dataset.biomasse;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;

/**
 * The Interface IInterventionDAO.
 *
 * @param <T> the generic type
 */
public interface IBiomasseDAO<T extends AbstractBiomasse> extends IDAO<T> {

}
