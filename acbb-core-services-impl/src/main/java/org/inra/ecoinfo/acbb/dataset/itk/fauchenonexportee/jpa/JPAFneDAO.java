package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.IFneDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;

/**
 * The Class JPASemisDAO.
 */
public class JPAFneDAO extends JPAInterventionDAO<Fne> implements IFneDAO {

    @Override
    protected Class<Fne> getInterventionClass() {
        return Fne.class;
    }
}
