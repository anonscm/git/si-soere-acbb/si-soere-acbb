package org.inra.ecoinfo.acbb.dataset.biomasse.lai.jpa;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction_;
import org.inra.ecoinfo.acbb.dataset.biomasse.jpa.JPABiomasseDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.ILaiDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPALaiDAO extends JPABiomasseDAO<Lai> implements ILaiDAO {

    /**
     * Gets the by n key.
     *
     * @param version
     * @param date
     * @return
     * @link(VersionFile)
     * @link(int) the rang
     * @link(Date) the date
     */
    @Override
    public Optional<Lai> getByNKey(final VersionFile version, final LocalDate date) {

        CriteriaQuery<Lai> query = builder.createQuery(Lai.class);
        Root<Lai> hv = query.from(Lai.class);
        query.where(
                builder.equal(hv.get(BiomassProduction_.version), version),
                builder.equal(hv.get(BiomassProduction_.date), date)
        );
        query.select(hv);
        return getOptional(query);
    }
}
