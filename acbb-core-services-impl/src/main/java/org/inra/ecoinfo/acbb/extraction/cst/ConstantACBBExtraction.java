/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.cst;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

// TODO: Auto-generated Javadoc

/**
 * The Class ConstantACBBExtraction.
 */
public final class ConstantACBBExtraction {

    /**
     *
     */
    public static final Logger LOGGER_FOR_EXTRACTION = LoggerFactory
            .getLogger(ConstantACBBExtraction.class);
    public static final String DATATYPE_NAME_INDEX ="DatatypeNames";
    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.extraction.cst";
    /**
     * The Constant RESOURCE_BUNDLE @link(ResourceBundle).
     */
    static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(ConstantACBBExtraction.BUNDLE_NAME);

    /**
     * Instantiates a new constant acbb extraction.
     */
    ConstantACBBExtraction() {
    }

    /**
     * Gets the string.
     *
     * @param key the key
     * @return the string
     */
    public static String getString(final String key) {
        try {
            return ConstantACBBExtraction.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
