/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemis;

/**
 * The Class JPAValeurSemisDAO.
 */
public class JPAValeurSemisDAO extends AbstractJPADAO<ValeurSemis> implements IValeurSemisDAO {

}
