/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.IDAO;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IMasseVegClassDAO.
 */

/**
 * @author koyao
 */
public interface IMasseVegClassDAO extends IDAO<MasseVegClass> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<MasseVegClass> getAll();

    /**
     * Gets the by n key.
     *
     * @param code
     * @return the by n key
     */
    Optional<MasseVegClass> getByNKey(String code);
}
