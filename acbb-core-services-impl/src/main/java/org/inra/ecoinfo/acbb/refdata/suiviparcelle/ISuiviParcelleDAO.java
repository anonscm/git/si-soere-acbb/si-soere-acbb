/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.suiviparcelle;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;

/**
 * The Interface ISuiviParcelleDAO.
 */
public interface ISuiviParcelleDAO extends IDAO<SuiviParcelle> {

    /**
     * @return
     */
    List<SuiviParcelle> getAll();

    /**
     * Gets the by n key.
     *
     * @param parcelle
     * @param traitement
     * @param dateDebutTraitement
     * @return the by n key
     * @link(Parcelle) the parcelle
     * @link(TraitementProgramme) the traitement
     * @link(Date) the date debut traitement
     * @link(Date) the date debut traitement
     */
    Optional<SuiviParcelle> getByNKey(Parcelle parcelle, TraitementProgramme traitement, LocalDate dateDebutTraitement);

    /**
     * @return @throws DateTimeException
     */
    Map<Parcelle, SortedMap<LocalDate, SuiviParcelle>> getSortedSuivisParcelles();

    /**
     * @param parcelle
     * @param date
     * @return
     */
    Optional<SuiviParcelle> retrieveSuiviParcelle(Parcelle parcelle, LocalDate date);
}
