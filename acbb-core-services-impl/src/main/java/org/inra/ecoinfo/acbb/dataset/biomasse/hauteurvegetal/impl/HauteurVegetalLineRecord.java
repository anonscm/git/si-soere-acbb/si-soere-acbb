package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetalNature;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord;

/**
 * The Class AILineRecord.
 * <p>
 * record one line of the file
 */
public class HauteurVegetalLineRecord extends AbstractLineRecord<HauteurVegetalLineRecord, VariableValueHauteurVegetal> {
    HauteurVegetalNature nature = null;
    BiomasseEntreeSortie entreeSortie;

    /**
     * @param originalLineNumber
     */
    public HauteurVegetalLineRecord(long originalLineNumber) {
        super(originalLineNumber);
    }

    /**
     * @return
     */
    public HauteurVegetalNature getNature() {
        return nature;
    }

    /**
     * @param nature
     */
    public void setNature(HauteurVegetalNature nature) {
        this.nature = nature;
    }

    /**
     * @return
     */
    public BiomasseEntreeSortie getEntreeSortie() {
        return entreeSortie;
    }

    /**
     * @param entreeSortie
     */
    public void setEntreeSortie(BiomasseEntreeSortie entreeSortie) {
        this.entreeSortie = entreeSortie;
    }

}
