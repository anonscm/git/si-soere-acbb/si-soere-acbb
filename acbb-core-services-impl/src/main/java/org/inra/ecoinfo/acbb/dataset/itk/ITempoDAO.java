package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.util.Optional;

/**
 * The Interface ITempoDAO.
 */
public interface ITempoDAO extends IDAO<Tempo> {

    /**
     * Delete unused tempos.
     *
     * @throws PersistenceException the persistence exception
     */
    void deleteUnusedTempos() throws PersistenceException;

    /**
     * Gets the by n key.
     *
     * @param versionDeTraitement
     * @param parcelle
     * @param rotationNumber
     * @param anneeNumber
     * @param periodeNumber
     * @return the by n key
     * @link{VersionDeTraitement the version de traitement
     * @link{Parcelle the parcelle
     * @link{int the rotation number
     * @link{int the annee number
     * @link{int the periode number
     * @link(VersionDeTraitement) the version de traitement
     * @link(int) the rotation number
     * @link(int) the annee number
     * @link(int) the periode number
     */
    Optional<Tempo> getByNKey(VersionDeTraitement versionDeTraitement, final Parcelle parcelle, int rotationNumber, int anneeNumber, int periodeNumber);

    /**
     * Save or update.
     *
     * @param tempo
     * @param intervention
     * @throws PersistenceException the persistence exception
     * @link{Tempo the tempo
     * @link{Intervention the intervention
     * @link(Tempo) the tempo
     * @link(Intervention) the binded intervention
     */
    void saveOrUpdate(Tempo tempo, AbstractIntervention intervention) throws PersistenceException;

}
