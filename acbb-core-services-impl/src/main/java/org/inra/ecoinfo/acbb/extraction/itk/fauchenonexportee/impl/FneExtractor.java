package org.inra.ecoinfo.acbb.extraction.itk.fauchenonexportee.impl;

import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKExtractor;

/**
 * @author ptcherniati
 */
public class FneExtractor extends AbstractITKExtractor<Fne> {

}
