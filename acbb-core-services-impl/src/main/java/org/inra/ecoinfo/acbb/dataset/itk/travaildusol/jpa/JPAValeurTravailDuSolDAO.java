/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.IValeurTravailDuSolDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.ValeurTravailDuSol;

/**
 * The Class JPAValeurTravailDuSolDAO.
 */
public class JPAValeurTravailDuSolDAO extends AbstractJPADAO<ValeurTravailDuSol> implements
        IValeurTravailDuSolDAO {

}
