/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;

/**
 * @author ptcherniati
 */
public interface IPhytosanitaireDAO extends IInterventionDAO<Phytosanitaire> {
}
