package org.inra.ecoinfo.acbb.extraction.biomasse.hauteurvegetal.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.AbstractBiomasseExtractor;

/**
 * @author ptcherniati
 */
public class HauteurVegetalExtractor extends AbstractBiomasseExtractor<HauteurVegetal> {

}
