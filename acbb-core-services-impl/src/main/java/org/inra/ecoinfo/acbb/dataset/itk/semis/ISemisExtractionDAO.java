package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * The Interface ISemisExtractionDAO.
 */
public interface ISemisExtractionDAO extends IDAO<Semis> {

    /**
     * @param selectedTraitementProgrammes
     * @param selectedVariables
     * @param intervalsDates
     * @return
     */
    List<MesureSemis> extractSemis(List<TraitementProgramme> selectedTraitementProgrammes,
                                   List<Variable> selectedVariables, List<IntervalDate> intervalsDates);

    /**
     * Gets the availables agro ecosystemes.
     *
     * @return the availables agro ecosystemes
     */
    List<AgroEcosysteme> getAvailablesAgroEcosystemes();

    /**
     * Gets the availables sites by agro id.
     *
     * @param id
     * @return the availables sites by agro id
     * @link(Long) the id
     */
    List<SiteACBB> getAvailablesSitesByAgroId(Long id);

    /**
     * Gets the availables variables by sites.
     *
     * @param linkedList
     * @return the availables variables by sites
     * @link(List<SiteACBB>) the linked list
     */
    List<Variable> getAvailablesVariablesBySites(List<SiteACBB> linkedList);

    /**
     * @param listTraitement
     * @param intervalsDate
     * @return
     */
    List<VariableACBB> getAvailablesVariablesByTraitements(
            List<TraitementProgramme> listTraitement, List<IntervalDate> intervalsDate);

    /**
     * Extractsemis.
     *
     * @param intervalsDate
     * @return the list
     * @link(List<SiteACBB>)
     * @link(List<Variable>)
     * @link(Date)
     * @link(Date) the date fin
     */
    List<TraitementProgramme> getAvailableTraitements(List<IntervalDate> intervalsDate);
}
