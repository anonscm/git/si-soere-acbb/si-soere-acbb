/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.listeitineraire;

import com.google.common.base.Strings;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisEspece;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisVariete;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB_;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class JPAListeItineraireDAO.
 */
public class JPAListeItineraireDAO extends AbstractJPADAO<ListeItineraire> implements
        IListeItineraireDAO {

    static final String PATTERN_COUVERT = "([^0-9>]+)(>?)(\\d*)";

    /**
     * @param espece
     * @see org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO#addVarietes(org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisEspece)
     */
    @Override
    public void addVarietes(final SemisEspece espece) {
        final List<SemisVariete> result = this.getByName(espece.getValeur());
        result.forEach(
                (variete) -> {
                    espece.getVarietes().
                            computeIfAbsent(variete.getValeur().toLowerCase(), k -> variete);
                    variete.setEspece(espece);
                }
        );
    }

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO
     * #getAll()
     */
    @Override
    public List<ListeItineraire> getAll() {
        return getAll(ListeItineraire.class);
    }

    /**
     * Gets the by code.
     *
     * @param <T>
     * @param name
     * @return the by code @see
     * org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO
     * #getByCode(java.lang.String)
     * @link(String) the code
     */
    @Override
    public <T extends ListeItineraire> List<T> getByName(final String name) {
        if (SemisEspece.SEMIS_SPECIES.equals(name)) {
            return (List<T>) getAllForClass(SemisEspece.class);
        }
        CriteriaQuery<T> query = builder.createQuery((Class<T>) ListeItineraire.class);
        Root<T> li = query.from((Class<T>) ListeItineraire.class);
        query.where(
                builder.equal(li.get(ListeItineraire_.code), name)
        );
        query.select(li);
        return getResultList(query);
    }

    @Override
    public <T extends ListeItineraire> List<T> getAllForClass(Class<T> clazz) {
        CriteriaQuery<T> query = builder.createQuery(clazz);
        Root<T> t = query.from(clazz);
        query.select(t);
        return getResultList(query);
    }

    /**
     * Gets the by n key.
     *
     * @param <U>
     * @param nom
     * @param valeur
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO
     * #getByNKey(java.lang.String, java.lang.String)
     * @link(String) the code
     * @link(String) the valeur
     */
    @Override
    public <U extends ListeItineraire> Optional<U> getByNKey(final String nom, final String valeur) {
        CriteriaQuery<U> query = builder.createQuery((Class<U>) ListeItineraire.class);
        Root<U> li = query.from((Class<U>) ListeItineraire.class);
        query.where(
                builder.equal(li.get(ListeACBB_.code), nom),
                builder.equal(li.get(ListeACBB_.valeur), valeur)
        );
        query.select(li);
        final Optional<U> listeItineraire = getOptional(query);
        listeItineraire
                .filter(l -> l instanceof SemisEspece)
                .map(l -> (SemisEspece) l)
                .ifPresent(l -> this.addVarietes(l));
        return listeItineraire;
    }

    /**
     * @param especeValeur
     * @param varieteCode
     * @return
     * @see org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO#getVariete(java.lang.String,
     * java.lang.String)
     */
    @Override
    public Optional<SemisVariete> getVariete(final String especeValeur, final String varieteCode) {
        Optional<ListeItineraire> variete = this.getByNKey(especeValeur, varieteCode);
        if (!variete.isPresent()) {
            return Optional.empty();
        } else {
            Optional<SemisVariete> castVariete = variete
                    .map(v -> (SemisVariete) v);
            this.getByNKey(SemisEspece.SEMIS_SPECIES, castVariete.get().getCode())
                    .map(e -> (SemisEspece) e)
                    .ifPresent(e -> castVariete.get().setEspece(e));
            return castVariete;
        }
    }

    /**
     * Update couvert vegetal.
     *
     * @param rotation
     * @link(Rotation) the rotation
     */
    @Override
    public void updateCouvertVegetalFromRotation(final Rotation rotation) {
        final String[] couverts = rotation.getNom().split("\\s*,\\s*");
        rotation.getCouvertsVegetal().clear();
        rotation.setRepeatedCouvert(org.apache.commons.lang.StringUtils.EMPTY);
        rotation.setInfiniteRepeatedCouvert(Boolean.FALSE);
        int index = 1;
        for (final String couvertString : couverts) {
            try {
                final Matcher matches = Pattern.compile(JPAListeItineraireDAO.PATTERN_COUVERT)
                        .matcher(couvertString);
                if (matches.matches()) {
                    final ListeItineraire couvert = this.getByNKey(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL, matches.group(1).toLowerCase())
                            .orElse(null);
                    final boolean isInfiniteRepeteadCover = Strings.isNullOrEmpty(matches.group(2));
                    final String nbCover = matches.group(3);
                    final int repetitionOfCover = Strings.isNullOrEmpty(nbCover) ? 1 : Integer
                            .parseInt(nbCover);
                    index = rotation.updateRepeatedCouverts(isInfiniteRepeteadCover,
                            repetitionOfCover, (SemisCouvertVegetal) couvert, index);
                }
            } catch (final NumberFormatException e) {
                index++;
            }
        }
        this.entityManager.persist(rotation);
    }
}
