/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.methode;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @param <T>
 * @author ptcherniati
 */
public abstract class AbstractMethodeRecorder<T extends AbstractMethode> extends
        AbstractCSVMetadataRecorder<T> {

    /**
     *
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.messages";

    /**
     *
     */
    public static final String PROPERTY_MSG_BAD_NUMBER_ID = "PROPERTY_MSG_BAD_NUMBER_ID";

    /**
     *
     */
    public static final String PROPERTY_MSG_BAD_NUMBER_ID_WITH_LINE = "PROPERTY_MSG_BAD_NUMBER_ID_WITH_LINE";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_NUMBER_ID = "PROPERTY_MSG_INVALID_NUMBER_ID";

    /**
     *
     */
    public static final String PROPERTY_MSG_MISSING_NUMBER_ID = "PROPERTY_MSG_MISSING_NUMBER_ID";

    /**
     *
     */
    public static final String PROPERTY_MSG_EMPTY_LABEL = "PROPERTY_MSG_EMPTY_LABEL";
    /**
     *
     */
    protected IMethodeDAO methodeDAO;
    private Properties propertiesLibelleEN;
    private Properties propertiesDefinitionEN;

    /**
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            List<T> methodsToDelete = new LinkedList();
            ErrorsReport errorsReport = new ErrorsReport();
            while ((values = parser.getLine()) != null) {
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String numberIdString = tokenizerValues.nextToken();
                methodsToDelete.add(this.getByNumberId(numberIdString, errorsReport)
                        .orElseThrow(() -> new PersistenceException("bad method")));
            }
            if (!(errorsReport.hasErrors() || methodsToDelete.isEmpty())) {
                for (T method : methodsToDelete) {
                    methodeDAO.remove(method);
                }
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param allMethods
     * @return
     * @throws BusinessException
     */
    public String buildRestrictedModelGrid(Set<AbstractMethode> allMethods)
            throws BusinessException {
        Set<T> methods = this.filter(allMethods);
        final ModelGridMetadata<T> modelGridMetadata = this.initModelGridMetadata();
        try {
            for (final T element : methods) {
                final LineModelGridMetadata lineModelGridMetadata = this
                        .getNewLineModelGridMetadata(element);
                modelGridMetadata.getLinesModelGridMetadatas().add(lineModelGridMetadata);
            }
            return modelGridMetadata.formatAsCSV();
        } catch (final IOException e) {
            throw new BusinessException("can't buils model", e);
        }
    }

    /**
     * @param allMethods
     * @return
     */
    protected abstract Set<T> filter(Set<AbstractMethode> allMethods);

    /**
     * @param numberIdString
     * @param errorsReport
     * @return
     */
    protected Optional<T> getByNumberId(String numberIdString, ErrorsReport errorsReport) {
        T methode = null;
        try {
            Long numberId = Long.parseLong(numberIdString);
            return this.methodeDAO.getByNumberId(numberId).map(t -> t);
        } catch (NumberFormatException e) {
            String message = String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractMethodeRecorder.BUNDLE_NAME,
                    AbstractMethodeRecorder.PROPERTY_MSG_BAD_NUMBER_ID), numberIdString);
            errorsReport.addErrorMessage(message);
        }
        return Optional.empty();
    }

    /**
     * @param errorsReport
     * @param lineNumber
     * @param tokenizerValues
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    protected String getDefinition(AbstractCSVMetadataRecorder.ErrorsReport errorsReport,
                                   int lineNumber, AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues)
            throws BusinessException {
        return tokenizerValues.nextToken();
    }

    /**
     * @param errorsReport
     * @param lineNumber
     * @param tokenizerValues
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    protected String getLibelle(AbstractCSVMetadataRecorder.ErrorsReport errorsReport,
                                int lineNumber, AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues)
            throws BusinessException {
        String libelle = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(libelle)) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractMethodeRecorder.BUNDLE_NAME,
                    AbstractMethodeRecorder.PROPERTY_MSG_EMPTY_LABEL), lineNumber, tokenizerValues
                    .currentTokenIndex()));
        }
        return libelle;
    }

    /**
     * @return
     */
    abstract protected String getMethodeDefinitionName();

    /**
     * @return
     */
    abstract protected String getMethodeEntityName();

    /**
     * @return
     */
    abstract protected String getMethodeLibelleName();

    /**
     * @param methode
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final T methode) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode != null ? methode.getNumberId()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode != null ? methode.getLibelle()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        methode != null && methode.getLibelle() != null ? this.propertiesLibelleEN
                                .getProperty(methode.getLibelle(), methode.getLibelle())
                                : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode != null ? methode.getDefinition()
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode != null ? getSanitizedPropertyValue(propertiesDefinitionEN, methode.getDefinition())
                        : AbstractCSVMetadataRecorder.EMPTY_STRING,
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     * @param errorsReport
     * @param lineNumber
     * @param tokenizerValues
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    protected Long getNumberId(AbstractCSVMetadataRecorder.ErrorsReport errorsReport,
                               int lineNumber, AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues)
            throws BusinessException {
        String numberIdString = tokenizerValues.nextToken();
        Long numberid = null;
        if (Strings.isNullOrEmpty(numberIdString)) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractMethodeRecorder.BUNDLE_NAME,
                    AbstractMethodeRecorder.PROPERTY_MSG_MISSING_NUMBER_ID), lineNumber,
                    tokenizerValues.currentTokenIndex()));
            return numberid;
        }
        try {
            numberid = Long.parseLong(numberIdString);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractMethodeRecorder.BUNDLE_NAME,
                    AbstractMethodeRecorder.PROPERTY_MSG_BAD_NUMBER_ID_WITH_LINE), lineNumber,
                    tokenizerValues.currentTokenIndex(), numberIdString));
        }
        return numberid;
    }

    /**
     *
     */
    protected void initProperties() {
        this.propertiesLibelleEN = this.localizationManager.newProperties(
                this.getMethodeEntityName(), this.getMethodeLibelleName(), Locale.ENGLISH);
        this.propertiesDefinitionEN = this.localizationManager.newProperties(
                this.getMethodeEntityName(), this.getMethodeDefinitionName(), Locale.ENGLISH);
    }

    /**
     * @param methodeDAO
     */
    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    /**
     * @param properties
     * @param key
     * @return
     */
    protected String getSanitizedPropertyValue(Properties properties, String key) {
        String localKey = key.replaceAll("^\"", "").replaceAll("\"$", "");
        return getSanitizedString(properties.getProperty(localKey, key));
    }

    /**
     * @param field
     * @return
     */
    protected String getSanitizedString(String field) {
        String localResult = field == null ? "" : field;
        if (localResult.contains("\n")) {
            localResult = String.format("\"%s\"", localResult);
        }
        return localResult;
    }

}
