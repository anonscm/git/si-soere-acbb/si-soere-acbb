/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.listeitineraire;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisEspece;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisVariete;
import org.inra.ecoinfo.acbb.refdata.listesacbb.IListeACBBDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IListeItineraireDAO.
 */
public interface IListeItineraireDAO extends IListeACBBDAO<ListeItineraire> {

    /**
     * Adds the varietes.
     *
     * @param espece
     * @link(SemisEspece)
     */
    void addVarietes(final SemisEspece espece);

    /**
     * Gets the variete.
     *
     * @param especeName
     * @param varieteCode
     * @return the variete
     * @link(String) the espece name
     * @link(String) the variete code
     */
    Optional<SemisVariete> getVariete(final String especeName, String varieteCode);

    /**
     * @param rotation
     */
    void updateCouvertVegetalFromRotation(Rotation rotation);

    /**
     * @param <T>
     * @param clazz
     * @return
     */
    <T extends ListeItineraire> List<T> getAllForClass(Class<T> clazz);

}
