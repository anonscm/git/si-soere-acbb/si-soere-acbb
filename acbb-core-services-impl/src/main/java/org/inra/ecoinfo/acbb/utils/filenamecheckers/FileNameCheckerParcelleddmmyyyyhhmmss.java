/*
 *
 */
package org.inra.ecoinfo.acbb.utils.filenamecheckers;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PersistenceException;
import java.time.DateTimeException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class FileNameCheckerddmmyyyy.
 */
public class FileNameCheckerParcelleddmmyyyyhhmmss extends AbstractACBBFileNameChecker {

    /**
     * The Constant INVALID_FILE_NAME @link(String).
     */
    protected static final String INVALID_FILE_NAME_WITH_PARCELLE = "%s_%s_%s_%s_%s.csv";
    /**
     * The Constant PATTERN @link(String).
     */
    protected static final String PATTERN = "^(%s|.*?)_(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";
    private static final Logger LOG = LoggerFactory.getLogger(FileNameCheckerParcelleddmmyyyyhhmmss.class
            .getName());
    String PATTERN_FILE_NAME_PATH = "%s_%s_%s_%s_%s#V%d#.csv";

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     * @see org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker
     * #getDatePattern()
     */
    @Override
    protected String getDatePattern() {
        return RecorderACBB.DD_MM_YYYY_HHMMSS_FILE;
    }

    /**
     * Gets the file path.
     *
     * @param version
     * @return the file path @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo
     * .dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public String getFilePath(final VersionFile version) {
        Dataset dataset = version.getDataset();
        return String.format(this.PATTERN_FILE_NAME_PATH,
                ACBBUtils.getSiteFromDataset(dataset).getPath(),
                ACBBUtils.getDatatypeFromDataset(dataset).getCode(),
                ACBBUtils.getParcelleFromDataset(dataset).getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), getDatePattern()),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), getDatePattern()),
                version.getVersionNumber());
    }

    /**
     * @param dataset
     * @return
     */
    @Override
    public String getFilePath(final Dataset dataset) {
        return String.format(this.PATTERN_FILE_NAME_PATH,
                ACBBUtils.getSiteFromDataset(dataset).getPath(),
                ACBBUtils.getDatatypeFromDataset(dataset).getCode(),
                ACBBUtils.getParcelleFromDataset(dataset).getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), getDatePattern()),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), getDatePattern()),
                dataset.getVersions().size());
    }

    /**
     * @param fileName
     * @param version
     * @return
     * @throws InvalidFileNameException
     */
    @Override
    public boolean isValidFileName(final String fileName, final VersionFile version)
            throws InvalidFileNameException {
        Dataset dataset = version.getDataset();
        String finalFileName = fileName;
        finalFileName = this.cleanFileName(finalFileName);
        final String currentSite = ACBBUtils.getSiteFromDataset(dataset).getCode();
        final String currentDatatype = ACBBUtils.getDatatypeFromDataset(dataset).getCode();
        final String currentParcelle = Utils.createCodeFromString(ACBBUtils.getParcelleFromDataset(dataset).getName());
        final Matcher splitFilename = Pattern.compile(
                String.format(FileNameCheckerParcelleddmmyyyyhhmmss.PATTERN, currentSite,
                        currentDatatype, currentParcelle)).matcher(finalFileName);
        this.testPath(currentSite, currentDatatype, currentParcelle, splitFilename);
        final String siteName = Utils.createCodeFromString(splitFilename.group(1));
        this.testSite(version, currentSite, currentDatatype, currentParcelle, siteName);
        final String datatypeName = Utils.createCodeFromString(splitFilename.group(2));
        this.testDatatype(currentSite, currentDatatype, currentParcelle, datatypeName);
        final String parcelleName = Utils.createCodeFromString(splitFilename.group(3));
        this.testParcelle(version, currentSite, currentDatatype, currentParcelle, parcelleName);
        this.testDates(version, currentSite, currentDatatype, currentParcelle, splitFilename);
        return true;
    }

    /**
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param datatypeName
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     */
    protected void testDatatype(final String currentSite, final String currentDatatype,
                                final String currentParcelle, String datatypeName) throws InvalidFileNameException {
        try {
            if ((!currentDatatype.equals(Utils.createCodeFromString(datatypeName)) || this.datatypeDAO
                    .getByCode(Utils.createCodeFromString(datatypeName)) == null)
                    && !org.apache.commons.lang.StringUtils.EMPTY.equals(datatypeName)
                    && !datatypeName.equalsIgnoreCase(this.datatypeVariableUniteACBBDAO.getNameVariableForDatatypeName(currentDatatype)
                    .orElseThrow(PersistenceException::new))) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                        currentDatatype, currentParcelle, this.getDatePattern(),
                        this.getDatePattern()));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }

    /**
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     */
    @Override
    protected void testDates(VersionFile version, String currentSite, String currentDatatype,
                             Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = RecorderACBB.getIntervalDateUTCForFile(splitFilename.group(4),
                    splitFilename.group(5));
        } catch (final DateTimeException | BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(
                    AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype,
                    RecorderACBB.DD_MM_YYYY_HHMMSS_FILE, RecorderACBB.DD_MM_YYYY_HHMMSS_FILE));
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }

    /**
     * Test dates.
     *
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param splitFilename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(VersionFile) the version
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(Matcher)
     */
    protected void testDates(final VersionFile version, final String currentSite,
                             final String currentDatatype, String currentParcelle, final Matcher splitFilename)
            throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = RecorderACBB.getIntervalDateUTCForFile(splitFilename.group(4),
                    splitFilename.group(5));
            if (version != null) {
                version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
                version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
            }
        } catch (final DateTimeException | BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, RecorderACBB.DD_MM_YYYY_HHMMSS_FILE,
                    RecorderACBB.DD_MM_YYYY_HHMMSS_FILE));
        }
    }

    /**
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param parcelleName
     * @throws InvalidFileNameException
     */
    protected void testParcelle(final VersionFile version, final String currentSite,
                                final String currentDatatype, final String currentParcelle, String parcelleName)
            throws InvalidFileNameException {
        try {
            if (parcelleName == null || !parcelleName.equals(currentParcelle)) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                        currentDatatype, currentParcelle, this.getDatePattern(),
                        this.getDatePattern()));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }

    /**
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected void testPath(final String currentSite, final String currentDatatype,
                            String currentParcelle, final Matcher splitFilename) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }

    /**
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param siteName
     * @throws InvalidFileNameException
     */
    protected void testSite(final VersionFile version, final String currentSite,
                            final String currentDatatype, final String currentParcelle, String siteName)
            throws InvalidFileNameException {
        Dataset dataset = version.getDataset();
        try {
            SiteACBB siteACBB = (SiteACBB) this.siteDAO.getByPath(Utils
                    .createCodeFromString(siteName))
                    .orElse(null);
            if (siteACBB == null) {
                siteACBB = ((ISiteACBBDAO) this.siteDAO).getByShortName(Utils
                        .createCodeFromString(siteName))
                        .orElse(null);
            }
            final String sitePath = ACBBUtils.getSiteFromDataset(dataset).getPath();
            if (siteACBB == null || !sitePath.equals(siteACBB.getPath())) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                        currentDatatype, currentParcelle, this.getDatePattern(),
                        this.getDatePattern()));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyyhhmmss.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }
}
