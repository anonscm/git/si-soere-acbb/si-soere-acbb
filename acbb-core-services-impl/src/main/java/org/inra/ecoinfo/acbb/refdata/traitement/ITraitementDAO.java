/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.traitement;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.refdata.site.Site;

import java.util.List;
import java.util.Optional;

/**
 * The Interface ITraitementDAO.
 */
public interface ITraitementDAO extends IDAO<TraitementProgramme> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<TraitementProgramme> getAll();

    /**
     * Gets the by n key.
     *
     * @param siteId         the site id
     * @param traitementCode the traitement code
     * @return the by n key
     */
    Optional<TraitementProgramme> getByNKey(Long siteId, String traitementCode);

    /**
     * Gets the by site.
     *
     * @param site the site
     * @return the by site
     */
    List<TraitementProgramme> getBySite(Site site);
}
