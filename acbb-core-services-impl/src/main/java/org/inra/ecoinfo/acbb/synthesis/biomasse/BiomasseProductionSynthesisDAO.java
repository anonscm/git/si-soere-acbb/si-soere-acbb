/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.biomasse;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction_;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse_;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.synthesis.biomasseproduction.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.biomasseproduction.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author tcherniatinsky
 */
public class BiomasseProductionSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurBiomassProduction> v = query.from(ValeurBiomassProduction.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<ValeurBiomassProduction, BiomassProduction> s = v.join(ValeurBiomassProduction_.biomassProduction);
        final Join<ValeurBiomassProduction, RealNode> varRn = v.join(ValeurBiomasse_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Path<String> parcelleCode = s.join(AbstractBiomasse_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurBiomassProduction_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(AbstractBiomasse_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                dateMesure,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                builder.avg(valeur),
                                idNode
                        )
                )
                .where(
                        builder.equal(node.get(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);
    }

}
