/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.agroecosysteme;


import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPAAgroEcosystemeDAO.
 */
public class JPAAgroEcosystemeDAO extends AbstractJPADAO<AgroEcosysteme> implements
        IAgroEcosystemeDAO {

    /**
     * Gets the by code.
     *
     * @param code
     * @return the by code
     * @link(String) the code
     */
    @Override
    public Optional<AgroEcosysteme> getByCode(final String code) {

        CriteriaQuery<AgroEcosysteme> query = builder.createQuery(AgroEcosysteme.class);
        Root<AgroEcosysteme> semis = query.from(AgroEcosysteme.class);
        query.where(
                builder.equal(semis.get(Nodeable_.code), code)
        );
        query.select(semis);
        return getOptional(query);
    }

}
