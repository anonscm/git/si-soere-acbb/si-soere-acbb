/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variablededescription;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IVariableDeDescriptionDAO.
 */
public interface IVariableDeDescriptionDAO extends IDAO<VariableDescription> {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     */
    Optional<VariableDescription> getByCode(String code);

    /**
     * Gets the variables de description for traitement.
     *
     * @param traitementProgramme the traitement programme
     * @param version             the version
     * @return the variables de description for traitement
     */
    List<VariableDescription> getVariablesDeDescriptionForTraitement(TraitementProgramme traitementProgramme, int version);
}
