package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.IRecFauDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau_;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPASemisDAO.
 */
public class JPARecFauDAO extends JPAInterventionDAO<RecFau> implements IRecFauDAO {

    /**
     * @param parcelle
     * @param num
     * @param rotationNumber
     * @param anneeNumber
     * @param type
     * @return
     */
    @Override
    public Optional<RecFau> getinterventionForNumYearAndType(Parcelle parcelle, int num, Integer rotationNumber,
                                                             Integer anneeNumber, String type) {

        CriteriaQuery<RecFau> query = builder.createQuery(RecFau.class);
        Root<RecFau> recFau = query.from(RecFau.class);
        query.where(
                builder.equal(recFau.get(RecFau_.versionTraitementRealiseeNumber), Math.max(0, rotationNumber)),
                builder.equal(recFau.get(RecFau_.anneeRealiseeNumber), anneeNumber),
                builder.equal(recFau.join(RecFau_.suiviParcelle).join(SuiviParcelle_.parcelle), parcelle),
                builder.equal(recFau.get(RecFau_.passage), num)
        );
        query.select(recFau);
        return getOptional(query);
    }

    /**
     * @param interventionType
     * @return
     */
    @Override
    public boolean hasType(InterventionType interventionType) {
        return RecFau.RECOLTE_ET_FAUCHE_DESRIMINATOR.equals(interventionType.getValeur());
    }

    @Override
    protected Class<RecFau> getInterventionClass() {
        return RecFau.class;
    }
}
