package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal;

import org.inra.ecoinfo.acbb.dataset.biomasse.IBiomasseDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface IHauteurVegetalDAO extends IBiomasseDAO<HauteurVegetal> {

    /**
     * @param version
     * @param date
     * @return
     */
    Optional<HauteurVegetal> getByNKey(VersionFile version, LocalDate date);
}
