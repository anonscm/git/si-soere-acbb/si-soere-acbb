/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.jpa;


import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse_;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.ValeurHauteurVegetal;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.ValeurHauteurVegetal_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationHauteurVegetalDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeur(version);
        deleteBiomasse(version);
    }

    private void deleteValeur(VersionFile versionFile) {
        CriteriaDelete<ValeurHauteurVegetal> deletevaleur = builder.createCriteriaDelete(ValeurHauteurVegetal.class);
        Root<ValeurHauteurVegetal> v = deletevaleur.from(ValeurHauteurVegetal.class);
        Subquery<HauteurVegetal> subqueryHauteurVegetal = deletevaleur.subquery(HauteurVegetal.class);
        Root<HauteurVegetal> hauteurVegetal = subqueryHauteurVegetal.from(HauteurVegetal.class);
        subqueryHauteurVegetal
                .select(hauteurVegetal)
                .where(builder.equal(hauteurVegetal.get(AbstractBiomasse_.version), versionFile));
        Path<HauteurVegetal> hauteurVegetalDB = v.get(ValeurHauteurVegetal_.hauteurVegetal);
        deletevaleur
                .where(hauteurVegetalDB.in(subqueryHauteurVegetal));
        delete(deletevaleur);
    }

    private void deleteBiomasse(VersionFile versionFile) {
        CriteriaDelete<HauteurVegetal> deleteBiomasse = builder.createCriteriaDelete(HauteurVegetal.class);
        Root<HauteurVegetal> hauteurVegetal = deleteBiomasse.from(HauteurVegetal.class);
        deleteBiomasse
                .where(builder.equal(hauteurVegetal.get(AbstractBiomasse_.version), versionFile));
        delete(deleteBiomasse);
    }

}
