/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.versiontraitement;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The Interface IVersionDeTraitementDAO.
 */
public interface IVersionDeTraitementDAO extends IDAO<VersionDeTraitement> {

    /**
     * @return
     */
    List<VersionDeTraitement> getAll();

    /**
     * Gets the by n key.
     *
     * @param siteCode       the site code
     * @param codeTraitement the code traitement
     * @param version        the version
     * @return the by n key
     */
    Optional<VersionDeTraitement> getByNKey(String siteCode, String codeTraitement, int version);

    /**
     * Gets the last version.
     *
     * @param traitement the traitement
     * @return the last version
     * @throws PersistenceException the persistence exception
     */
    int getLastVersion(TraitementProgramme traitement) throws PersistenceException;

    /**
     * Gets the previous version.
     *
     * @param traitement the traitement
     * @param version    the version
     * @return the previous version
     * @throws PersistenceException the persistence exception
     */
    int getpreviousVersion(TraitementProgramme traitement, int version) throws PersistenceException;

    /**
     * Gets the previous version traitement.
     *
     * @param traitement the traitement
     * @param version    the version
     * @return the previous version traitement
     * @throws PersistenceException the persistence exception
     */
    Optional<VersionDeTraitement> getpreviousVersionTraitement(TraitementProgramme traitement, int version)
            throws PersistenceException;

    /**
     * @param traitement
     * @param date
     * @return
     */
    Optional<VersionDeTraitement> retrieveVersiontraitement(TraitementProgramme traitement, LocalDate date);
}
