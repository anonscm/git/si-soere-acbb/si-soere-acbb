/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variable;

import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IVariableACBBDAO.
 */
public interface IVariableACBBDAO extends IVariableDAO {

    /**
     * Gets the all variable acbb.
     *
     * @param class1 the class1
     * @return the all variable acbb
     */
    List<VariableACBB> getAllVariableACBB(Class<Variable> class1);

    /**
     * Gets the by affichage.
     *
     * @param affichage the affichage
     * @return the by affichage
     */
    Optional<Variable> getByAffichage(String affichage);

    /**
     * @param affichage
     * @param datatypeCode
     * @return
     */
    Optional<Variable> getByAffichageAndDatatypeCode(final String affichage, String datatypeCode);
}
