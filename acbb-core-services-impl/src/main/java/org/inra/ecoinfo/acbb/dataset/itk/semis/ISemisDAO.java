/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The Interface ISemisDAO.
 */
public interface ISemisDAO extends IInterventionDAO<Semis> {

    /**
     * Gets the by n key.
     *
     * @param version
     * @param suiviParcelle
     * @param date
     * @return the by n key
     * @link(VersionFile)
     * @link(int) the rang
     * @link(Date) the date
     */
    Optional<AbstractIntervention> getByNKey(VersionFile version, SuiviParcelle suiviParcelle, LocalDate date);
}
