/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.parcelle;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.refdata.site.Site;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IParcelleDAO.
 */
public interface IParcelleDAO extends IDAO<Parcelle> {

    /**
     * Gets the all the {@link Parcelle}.
     *
     * @return all plots
     */
    List<Parcelle> getAll();

    /**
     * Gets the all sites with parcelle defined.
     *
     * @return the all {@link SiteACBB} with {@link Parcelle} defined
     */
    List<SiteACBB> getAllSitesWithParcelleDefined();

    /**
     * Gets the {@link Parcelle} by natural key.
     *
     * @param codeParcelle
     * @return the by n key
     * @link(Long) the site id
     * @link(String) the code parcelle
     */
    Optional<Parcelle> getByNKey(String codeParcelle);

    /**
     * Gets the {@link Parcelle} by {@link SiteACBB}.
     *
     * @param site
     * @return the by site {@link SiteACBB} the site
     * @link(Site) the site
     */
    List<Parcelle> getBySite(Site site);
}
