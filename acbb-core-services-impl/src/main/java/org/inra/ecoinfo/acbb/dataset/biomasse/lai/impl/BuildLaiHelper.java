package org.inra.ecoinfo.acbb.dataset.biomasse.lai.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.ILaiDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.ValeurLai;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati
 */
public class BuildLaiHelper extends AbstractBuildHelper<LaiLineRecord> {

    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.lai.impl.messages";

    Map<Parcelle, Map<LocalDate, Map<Long, Lai>>> lais = new HashMap();

    ILaiDAO laiDAO;

    /**
     * @param hauteurvegetalDAO the hauteurvegetalDAO to set
     *                          <p>
     *                          public void setHauteurVegetalDAO(IHauteurVegetalDAO hauteurvegetalDAO) {
     *                          this.hauteurvegetalDAO = hauteurvegetalDAO; }
     */
    void addValeursLai(final LaiLineRecord line, final Lai lai) {
        for (final VariableValueLai variableValue : line.getVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurLai valeur;
            valeur = new ValeurLai(Float.parseFloat(variableValue.getValue()),
                    realNode, variableValue.getMeasureNumber(),
                    variableValue.getStandardDeviation(), lai,
                    variableValue.getQualityClass(), variableValue.getMethode(),
                    variableValue.getBiomasseEntreeSortie(), variableValue.getMediane());
            lai.getValeursLai().add(valeur);
        }
    }

    /**
     * @throws PersistenceException
     */
    @Override
    public void build() throws PersistenceException {
        for (final LaiLineRecord line : this.lines) {
            final Lai lai = this.getOrCreateLai(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addValeursLai(line, lai);
            this.laiDAO.saveOrUpdate(lai);
        }
    }

    /**
     * Builds the entities.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesBiomasse
     * @throws PersistenceException the persistence exception
     * @link{VersionFile the version
     * @link{java.util.List the lines
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesBiomasse the request properties Biomasse
     */
    public void build(final VersionFile version, final List<LaiLineRecord> lines,
                      final ErrorsReport errorsReport,
                      final IRequestPropertiesBiomasse requestPropertiesBiomasse) throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesBiomasse);
        this.build();
    }

    /**
     * Creates the new ai.
     *
     * @param line
     * @return the ai
     * @link{AILineRecord the line
     */
    Lai createNewLai(final LaiLineRecord line) {
        Lai lai = null;
        lai = new Lai(this.version, line.getNatureCouvert(), line.getNature(), line.getObservation(), line.getDate(), line.getSerie(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber(),
                line.getSuiviParcelle(), line.getTypeIntervention(), line.getIntervention());
        try {
            this.laiDAO.saveOrUpdate(lai);
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return lai;
    }

    /**
     * Gets the or create ai.
     *
     * @param line
     * @return the or create ai
     * @link{AILineRecord the line
     */
    Lai getOrCreateLai(final LaiLineRecord line) {
        return lais
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getNature().getId(), k -> createNewLai(line));
    }

    /**
     * @param laiDAO the laiDAO to set
     */
    public void setLaiDAO(ILaiDAO laiDAO) {
        this.laiDAO = laiDAO;
    }
}
