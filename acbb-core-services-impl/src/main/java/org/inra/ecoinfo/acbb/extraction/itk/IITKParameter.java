/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk;

import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * @author ptcherniati
 */
public interface IITKParameter extends IParameter {

    /**
     * @return
     */
    int getAffichage();

    /**
     * @param affichage
     */
    void setAffichage(int affichage);

    /**
     * @return
     */
    List<IntervalDate> getIntervalsDates();

    /**
     * @return
     */
    List<TraitementProgramme> getSelectedTraitementProgrammes();

    /**
     * @param intervalDates
     */
    void setIntervalsDate(List<IntervalDate> intervalDates);

    /**
     * @param listTraitement
     */
    void setSelectedsTraitements(List<TraitementProgramme> listTraitement);
}
