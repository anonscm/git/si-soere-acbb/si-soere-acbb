package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.impl;

import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

/**
 * The Class LineRecord.
 * <p>
 * one line of record
 */
public class FneLineRecord extends AbstractLineRecord<FneLineRecord, ListeItineraire> {

    private Integer rang;

    /**
     * Instantiates a new line record.
     *
     * @param lineCount long the line count
     * @link(SuiviParcelle) the suivi parcelle
     * @link(SuiviParcelle) the suivi parcelle
     */
    public FneLineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * @return the rang
     */
    public Integer getRang() {
        return this.rang;
    }

    /**
     * @param rang
     */
    public void setRang(Integer rang) {
        this.rang = rang;
    }

    /**
     * Gets the treatment affichage.
     *
     * @return the treatment affichage
     */
    public String getTreatmentAffichage() {
        String returnString = org.apache.commons.lang.StringUtils.EMPTY;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null
                && this.getTempo().getVersionDeTraitement().getTraitementProgramme() != null) {
            returnString = this.getTempo().getVersionDeTraitement().getTraitementProgramme()
                    .getAffichage();
        }
        return returnString;
    }

    /**
     * Gets the treatment version.
     *
     * @return the treatment version
     */
    public int getTreatmentVersion() {
        int returnInt = -1;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null) {
            returnInt = this.getTempo().getVersionDeTraitement().getVersion();
        }
        return returnInt;
    }
}
