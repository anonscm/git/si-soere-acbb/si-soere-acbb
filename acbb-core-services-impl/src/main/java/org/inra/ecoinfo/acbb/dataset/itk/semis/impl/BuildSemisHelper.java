package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IMesureSemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisAttrDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.*;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * The Class BuildSemisHelper.
 */
public class BuildSemisHelper extends AbstractBuildHelper<LineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.impl.messages";
    /**
     * The Constant PROPERTY_MSG_ERROR_GENERATE_TEMPORARIES_MEADOWS
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_ERROR_GENERATE_TEMPORARIES_MEADOWS = "PROPERTY_MSG_ERROR_GENERATE_TEMPORARIES_MEADOWS";
    private static final Logger LOG = LoggerFactory.getLogger(BuildSemisHelper.class
            .getName());
    /**
     * The semis s @link(Map<Parcelle,Map<Date,Semis>>).
     */
    Map<Parcelle, Map<LocalDate, Semis>> semisS = new HashMap();

    /**
     * The mesure semis dao @link(IMesureSemisDAO).
     */
    IMesureSemisDAO mesureSemisDAO;

    /**
     * The semis dao @link(ISemisDAO).
     */
    ISemisDAO semisDAO;

    /**
     * The valeur semis attr dao @link(IValeurSemisAttrDAO).
     */
    IValeurSemisAttrDAO valeurSemisAttrDAO;

    /**
     * The tempo dao @link(ITempoDAO).
     */
    ITempoDAO tempoDAO;

    /**
     * Adds the mesures semis.
     *
     * @param line
     * @param semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    void addMesuresSemis(final LineRecord line, final Semis semis) {
        final MesureSemis mesureSemis = this.getOrCreateMesureSemis(line, semis);
        for (final ACBBVariableValue variableValue : line.getVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurSemis valeur;
            if (variableValue.isValeurQualitative()) {
                valeur = new ValeurSemis((ListeItineraire) variableValue.getValeurQualitative(),
                        realNode, mesureSemis);
            } else {
                valeur = new ValeurSemis(Float.valueOf(variableValue.getValue()),
                        realNode, mesureSemis);
            }
            mesureSemis.getvaleursSemis().add(valeur);
        }
    }

    /**
     * Adds the valeur semis.
     *
     * @param mesureSemis
     * @param variableValues
     * @link(MesureSemis) the mesure semis
     * @link(List<VariableValue>) the variable values
     */
    void addValeurSemis(final MesureSemis mesureSemis, final List<VariableValue> variableValues) {
        for (final VariableValue variableValue : variableValues) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            final Float floatvalue = Float.valueOf(variableValue.getValue());
            final ValeurSemis valeurSemis = new ValeurSemis(floatvalue,
                    realNode,
                    mesureSemis);
            mesureSemis.getvaleursSemis().add(valeurSemis);
        }
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() throws PersistenceException {
        for (final LineRecord line : this.lines) {
            Semis semis = this.getOrCreateSemis(line);
            if (this.errorsReport.hasErrors()) {
                LOGGER.debug(this.errorsReport.getErrorsMessages());
                throw new PersistenceException(this.errorsReport.getErrorsMessages());
            }
            this.addMesuresSemis(line, semis);
        }
    }

    /**
     * Builds the.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesITK
     * @throws PersistenceException the persistence exception
     * @link(VersionFile) the version
     * @link(List<LineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     */
    public void build(final VersionFile version, final List<LineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK)
            throws PersistenceException {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    /**
     * Creates the new meadow year.
     *
     * @param annee
     * @param line
     * @param semis
     * @throws PersistenceException the persistence exception
     * @link(int) the annee
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    void createNewMeadowYear(final int annee, final LineRecord line,
                             final AbstractIntervention semis) throws PersistenceException {
        final AnneeRealisee anneeRealisee = new AnneeRealisee(annee, line.getTempo()
                .getVersionTraitementRealisee());
        ((SemisVersionDeTraitementRealiseeFactory) this.versionDeTraitementRealiseeFactory)
                .updateAnneeRealisee(anneeRealisee, line, this.errorsReport);
        line.getTempo().getVersionTraitementRealisee().getAnneesRealisees()
                .put(annee, anneeRealisee);
        ((SemisVersionDeTraitementRealiseeFactory) this.versionDeTraitementRealiseeFactory)
                .updateAnneeRealisee(anneeRealisee, line, this.errorsReport);
        Tempo tempo = new Tempo(line.getVersionDeTraitement(), line.getParcelle(), line.getTempo()
                .getVersionTraitementRealisee(), anneeRealisee, null);
        this.tempoDAO.saveOrUpdate(tempo);
    }

    /**
     * Creates the new semis.
     *
     * @param line
     * @return the semis
     * @link(LineRecord) the line
     */
    Semis createNewSemis(final LineRecord line) {
        Semis semis = null;
        final VersionDeTraitement versionTraitement = line.getVersionDeTraitement();
        final Parcelle parcelle = line.getParcelle();
        final Tempo tempo = this.getTempo(line, versionTraitement, parcelle);
        if (tempo.isInErrors()) {
            return semis;
        }
        semis = new Semis(this.version, line.getObservation(), line.getDate(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber(), tempo);
        semis.setSuiviParcelle(line.getSuiviParcelle());
        for (final ACBBVariableValue<ListeItineraire> variableValue : line
                .getAttributesVariablesValues()) {
            RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
            ValeurSemisAttr valeur;
            if (variableValue.isValeurQualitative()
                    && variableValue.getValeursQualitatives() == null) {
                valeur = new ValeurSemisAttr(variableValue.getValeurQualitative(),
                        realNode, semis);
            } else if (variableValue.isValeurQualitative()) {
                valeur = new ValeurSemisAttr(variableValue.getValeursQualitatives(),
                        realNode, semis);
            } else {
                valeur = new ValeurSemisAttr(Float.valueOf(variableValue.getValue()),
                        realNode, semis);
            }
            valeur.setRealNode(realNode);
            semis.getAttributs().add(valeur);
        }
        try {
            this.semisDAO.saveOrUpdate(semis);
            if (!line.isAnnuelle() && line.getSemisRotationDescriptor().getCouverts().size() > 0) {
                this.generateTemporariesMeadows(line, semis);
            }
        } catch (final PersistenceException e) {
            this.errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return semis;
    }

    /**
     * Generate temporaries meadows.
     *
     * @param line
     * @param semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    void generateTemporariesMeadows(final LineRecord line, final AbstractIntervention semis) {
        int annne = line.getAnneeNumber();
        for (final Entry<Integer, SemisCouvertVegetal> entry : line.getSemisRotationDescriptor()
                .getCouverts().entrySet()) {
            final boolean isGoodCover = entry.getValue().equals(line.getCouvertVegetal());
            if (isGoodCover && annne != line.getAnneeNumber()) {
                try {
                    this.createNewMeadowYear(annne, line, semis);
                } catch (final PersistenceException e) {
                    this.infoReport
                            .addInfoMessage(String.format(
                                    RecorderACBB
                                            .getACBBMessageWithBundle(
                                                    BuildSemisHelper.BUNDLE_NAME,
                                                    BuildSemisHelper.PROPERTY_MSG_ERROR_GENERATE_TEMPORARIES_MEADOWS),
                                    line.getRotationNumber(), line.getTreatmentVersion(), line
                                            .getTreatmentAffichage()));
                }
            }
            if (isGoodCover) {
                annne++;
            }
        }
    }

    /**
     * Gets the or create mesure semis.
     *
     * @param line
     * @param semis
     * @return the or create mesure semis
     * @link(LineRecord) the line
     * @link(Semis) the semis
     */
    MesureSemis getOrCreateMesureSemis(final LineRecord line, final Semis semis) {
        MesureSemis mesureSemis = null;
        for (final MesureSemis mesureSemis2 : semis.getMesuresSemis()) {
            if (mesureSemis2.getEspece().equals(line.getEspece())
                    && mesureSemis2.getVariete().equals(line.getVariete())) {
                mesureSemis = mesureSemis2;
                break;
            }
        }
        if (mesureSemis == null) {
            mesureSemis = new MesureSemis(line.getEspece(), line.getVariete(), semis);
            semis.getMesuresSemis().add(mesureSemis);
        }
        return mesureSemis;
    }

    /**
     * Gets the or create semis.
     *
     * @param line
     * @return the or create semis
     * @link(LineRecord) the line
     */
    Semis getOrCreateSemis(final LineRecord line) {
        VersionDeTraitement versionDeTraitement = null;
        try {
            versionDeTraitement = this.versionDeTraitementDAO.merge(line.getVersionDeTraitement());
            line.setVersionDeTraitement(versionDeTraitement);
        } catch (final PersistenceException e) {
            UncatchedExceptionLogger.logUncatchedException("getOrcreateTravailDuSol", e);
        }
        return semisS
                .computeIfAbsent(line.getParcelle(), k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> createNewSemis(line));
    }

    /**
     * Sets the mesure semis dao @link(IMesureSemisDAO).
     *
     * @param mesureSemisDAO the new mesure semis dao @link(IMesureSemisDAO)
     */
    public final void setMesureSemisDAO(final IMesureSemisDAO mesureSemisDAO) {
        this.mesureSemisDAO = mesureSemisDAO;
    }

    /**
     * Sets the semis dao @link(ISemisDAO).
     *
     * @param semisDAO the new semis dao @link(ISemisDAO)
     */
    public final void setSemisDAO(final ISemisDAO semisDAO) {
        this.semisDAO = semisDAO;
    }

    /**
     * Sets the tempo dao @link(ITempoDAO).
     *
     * @param tempoDAO the new tempo dao @link(ITempoDAO)
     */
    public final void setTempoDAO(final ITempoDAO tempoDAO) {
        this.tempoDAO = tempoDAO;
    }

    /**
     * Sets the valeur semis attr dao @link(IValeurSemisAttrDAO).
     *
     * @param valeurSemisAttrDAO the new valeur semis attr dao
     * @link(IValeurSemisAttrDAO)
     */
    public final void setValeurSemisAttrDAO(final IValeurSemisAttrDAO valeurSemisAttrDAO) {
        this.valeurSemisAttrDAO = valeurSemisAttrDAO;
    }

}
