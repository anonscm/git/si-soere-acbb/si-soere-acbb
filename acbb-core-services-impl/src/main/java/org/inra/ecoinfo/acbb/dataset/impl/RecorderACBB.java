/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.*;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.dataset.AbstractRecorder;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.time.DateTimeException;
import java.time.LocalDateTime;

/**
 * The Class RecorderACBB.
 * <p>
 * This class is used to delete, test, upload and publish files
 *
 * @author Tcherniatinsky Philippe
 */
public class RecorderACBB extends AbstractRecorder implements IRecorderACBB {

    /**
     * The Constant ACBB_DATASET_BUNDLE_NAME.
     */
    public static final String DATASET_DESCRIPTOR_PATH_PATTERN = "org/inra/ecoinfo/acbb/dataset/%s";

    /**
     * The Constant ACBB_DATASET_BUNDLE_NAME.
     */
    public static final String ACBB_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.messages";

    /**
     * The Constant CST_SPACE.
     */
    public static final String CST_SPACE = " ";

    /**
     * The Constant CST_NEW_LINE @link(String).
     */
    public static final String CST_NEW_LINE = "/n";

    /**
     * The Constant CST_DOT.
     */
    public static final String CST_DOT = ".";

    /**
     * The Constant CST_COMMA.
     */
    public static final String CST_COMMA = ",";

    /**
     * The Constant CST_UNDERSCORE.
     */
    public static final String CST_UNDERSCORE = "_";

    // patterns
    /**
     * The Constant PATTERN_DATE.
     */
    public static final String PATTERN_DATE = "^(([0-2][0-9]/(0[0-9]|1[0-2]))|(30/(04|06|09|11))|(3[01]/(01|03|05|07|08|10|12)))/[0-2][0-9]{3}";

    /**
     * The Constant PATTERN_TIME.
     */
    public static final String PATTERN_TIME = "^([0-1][0-9]|2[0-3]):([0-6][0-9])$|24:00";

    /**
     *
     */
    public static final String PATTERN_TIME_SECONDE = "^([0-1][0-9]|2[0-3]):([0-6][0-9]):([0-6][0-9])$|24:00:00";

    /**
     * The Constant PATTERN_VARIABLE_EUROPEAN_NOM.
     */
    public static final String PATTERN_VARIABLE_EUROPEAN_NOM = "^(%s_([0-9]*)_([0-9]*))|(%s)$";

    /**
     * The Constant PROPERTY_CST_FRENCH_DATE.
     */
    public static final String PROPERTY_CST_FRENCH_DATE = DateUtil.DD_MM_YYYY;

    /**
     * The Constant PROPERTY_CST_FRENCH_TIME.
     */
    public static final String PROPERTY_CST_FRENCH_TIME = DateUtil.HH_MM;

    // dataset-descripors_types
    /**
     * The Constant PROPERTY_CST_DATE_TYPE.
     */
    public static final String PROPERTY_CST_DATE_TYPE = "date";

    /**
     * The Constant PROPERTY_CST_INTEGER_TYPE.
     */
    public static final String PROPERTY_CST_INTEGER_TYPE = "integer";

    /**
     * The Constant PROPERTY_CST_FLOAT_TYPE.
     */
    public static final String PROPERTY_CST_FLOAT_TYPE = "float";

    /**
     * The Constant PROPERTY_CST_TIME_TYPE.
     */
    public static final String PROPERTY_CST_TIME_TYPE = "time";

    /**
     * The Constant PROPERTY_CST_QUALITY_CLASS_TYPE.
     */
    public static final String PROPERTY_CST_QUALITY_CLASS_TYPE = "quality class";

    /**
     * The Constant PROPERTY_CST_QUALITY_CLASS_GENERIC.
     */
    public static final String PROPERTY_CST_QUALITY_CLASS_GENERIC = "qc";

    /**
     * The Constant PROPERTY_CST_GAP_FIELD_TYPE.
     */
    public static final String PROPERTY_CST_GAP_FIELD_TYPE = "gap field";

    /**
     * The Constant PROPERTY_CST_VARIABLE_TYPE.
     */
    public static final String PROPERTY_CST_VARIABLE_TYPE = "variable";

    /**
     * The Constant PROPERTY_CST_VARIABLE_TYPE.
     */
    public static final String PROPERTY_CST_VALEUR_QUALITATIVE_TYPE = "valeur_qualitative";

    /**
     * The Constant PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE @link(String).
     */
    public static final String PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE = "liste_valeurs_qualitatives";

    // headers
    /**
     * The Constant PROPERTY_MSG_MISMACH_COLUMN_HEADER.
     */
    public static final String PROPERTY_MSG_MISMACH_COLUMN_HEADER = "PROPERTY_MSG_MISMACH_COLUMN_HEADER";

    // headers
    /**
     * The Constant PROPERTY_MSG_MISMACH_GENERIC_COLUMN_HEADER.
     */
    public static final String PROPERTY_MSG_MISMACH_GENERIC_COLUMN_HEADER = "PROPERTY_MSG_MISMACH_GENERIC_COLUMN_HEADER";

    /**
     * The Constant PROPERTY_MSG_MISMACH_COLUMN_HEADER_EUROPEAN_NOM.
     */
    public static final String PROPERTY_MSG_MISMACH_COLUMN_HEADER_EUROPEAN_NOM = "PROPERTY_MSG_MISMACH_COLUMN_HEADER_EUROPEAN_NOM";

    /**
     * The Constant PROPERTY_MSG_MISSING_EMPTY_LINE.
     */
    public static final String PROPERTY_MSG_MISSING_EMPTY_LINE = "PROPERTY_MSG_MISSING_EMPTY_LINE";

    /**
     * The Constant PROPERTY_MSG_MISSING_SITE.
     */
    public static final String PROPERTY_MSG_MISSING_SITE = "PROPERTY_MSG_MISSING_SITE";

    /**
     * The Constant PROPERTY_MSG_INVALID_SITE.
     */
    public static final String PROPERTY_MSG_INVALID_SITE = "PROPERTY_MSG_INVALID_SITE";

    /**
     * The Constant PROPERTY_MSG_INVALID_DATATYPE.
     */
    public static final String PROPERTY_MSG_INVALID_DATATYPE = "PROPERTY_MSG_INVALID_DATATYPE";

    /**
     * The Constant PROPERTY_MSG_MISSING_DATATYPE.
     */
    public static final String PROPERTY_MSG_MISSING_DATATYPE = "PROPERTY_MSG_MISSING_DATATYPE";

    /**
     * The Constant PROPERTY_MSG_INVALID_SITE_REPOSITORY.
     */
    public static final String PROPERTY_MSG_INVALID_SITE_REPOSITORY = "PROPERTY_MSG_INVALID_SITE_REPOSITORY";

    /**
     * The Constant PROPERTY_MSG_MISSING_PLOT @link(String).
     */
    public static final String PROPERTY_MSG_MISSING_PLOT = "PROPERTY_MSG_MISSING_PLOT";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_PLOT = "PROPERTY_MSG_INVALID_PLOT";

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE.
     */
    public static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The Constant PROPERTY_MSG_MISSING_MONITORING_PLOT.
     */
    public static final String PROPERTY_MSG_MISSING_MONITORING_PLOT = "PROPERTY_MSG_MISSING_MONITORING_PLOT";
    /**
     * The Constant PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER.
     */
    public static final String PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER = "PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER";

    /**
     *
     */
    public static final String PROPERTY_MSG_BAD_MONITORING_PLOT_WITH_LINE_NUMBER = "PROPERTY_MSG_BAD_MONITORING_PLOT_WITH_LINE_NUMBER";

    /**
     * The Constant PROPERTY_MSG_MISSING_TREATMENT.
     */
    public static final String PROPERTY_MSG_MISSING_TREATMENT = "PROPERTY_MSG_MISSING_TREATMENT";

    /**
     * The Constant PROPERTY_MSG_INVALID_TRAITEMENT.
     */
    public static final String PROPERTY_MSG_INVALID_TRAITEMENT = "PROPERTY_MSG_INVALID_TRAITEMENT";

    /**
     * The Constant PROPERTY_MSG_INVALID_TRAITEMENT.
     */
    public static final String PROPERTY_MSG_INVALID_SITE_TREATMENT_VERSION = "PROPERTY_MSG_INVALID_SITE_TREATMENT_VERSION";

    /**
     * The Constant PROPERTY_MSG_MISSING_TREATMENT.
     */
    public static final String PROPERTY_MSG_MISSING_VERSION_TREATMENT = "PROPERTY_MSG_MISSING_TREATMENT";

    /**
     * The Constant PROPERTY_MSG_INVALID_TRAITEMENT.
     */
    public static final String PROPERTY_MSG_INVALID_VERSION_TRAITEMENT = "PROPERTY_MSG_INVALID_VERSION_TRAITEMENT";

    /**
     * The Constant PROPERTY_MSG_MISSING_BEGIN_TREATMENT_DATE.
     */
    public static final String PROPERTY_MSG_MISSING_BEGIN_TREATMENT_DATE = "PROPERTY_MSG_MISSING_BEGIN_TREATMENT_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_BEGIN_TREATMENT_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_BEGIN_TREATMENT_DATE = "PROPERTY_MSG_INVALID_BEGIN_TREATMENT_DATE";

    /**
     * The Constant PROPERTY_MSG_MISSING_END_TREATMENT_DATE.
     */
    public static final String PROPERTY_MSG_MISSING_END_TREATMENT_DATE = "PROPERTY_MSG_MISSING_END_TREATMENT_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_END_TREATMENT_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_END_TREATMENT_DATE = "PROPERTY_MSG_INVALID_END_TREATMENT_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_SPECIES.
     */
    public static final String PROPERTY_MSG_INVALID_SPECIES = "PROPERTY_MSG_INVALID_SPECIES";

    /**
     * The Constant PROPERTY_MSG_MISSING_SPECIES.
     */
    public static final String PROPERTY_MSG_MISSING_SPECIES = "PROPERTY_MSG_MISSING_SPECIES";

    /**
     * The Constant PROPERTY_MSG_MISSING_VERSION.
     */
    public static final String PROPERTY_MSG_MISSING_VERSION = "PROPERTY_MSG_MISSING_VERSION";

    /**
     * The Constant PROPERTY_MSG_MISSING_VERSION_TRAITEMENT.
     */
    public static final String PROPERTY_MSG_MISSING_VERSION_TRAITEMENT = "PROPERTY_MSG_MISSING_VERSION_TRAITEMENT";

    /**
     * The Constant PROPERTY_MSG_INVALID_PLANTING_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_PLANTING_DATE = "PROPERTY_MSG_INVALID_PLANTING_DATE";

    /**
     * The Constant PROPERTY_MSG_MISSING_PLANTING_DATE.
     */
    public static final String PROPERTY_MSG_MISSING_PLANTING_DATE = "PROPERTY_MSG_MISSING_PLANTING_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_DATE_HARVEST.
     */
    public static final String PROPERTY_MSG_INVALID_DATE_HARVEST = "PROPERTY_MSG_INVALID_DATE_HARVEST";

    /**
     * The Constant PROPERTY_MSG_MISSING_BEGIN_DATE.
     */
    public static final String PROPERTY_MSG_MISSING_BEGIN_DATE = "PROPERTY_MSG_MISSING_BEGIN_DATE";

    /**
     * The Constant PROPERTY_MSG_MISSING_END_DATE.
     */
    public static final String PROPERTY_MSG_MISSING_END_DATE = "PROPERTY_MSG_MISSING_END_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_BEGIN_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_BEGIN_DATE = "PROPERTY_MSG_INVALID_BEGIN_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_END_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_END_DATE = "PROPERTY_MSG_INVALID_END_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_INTERVAL_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_INTERVAL_DATE = "PROPERTY_MSG_INVALID_INTERVAL_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_VARIABLE.
     */
    public static final String PROPERTY_MSG_INVALID_VARIABLE = "PROPERTY_MSG_INVALID_VARIABLE";

    /**
     * The Constant PROPERTY_MSG_BAD_MIN_VALUE_FORMAT.
     */
    public static final String PROPERTY_MSG_BAD_MIN_VALUE_FORMAT = "PROPERTY_MSG_BAD_MIN_VALUE_FORMAT";

    /**
     * The Constant PROPERTY_MSG_BAD_MAX_VALUE_FORMAT.
     */
    public static final String PROPERTY_MSG_BAD_MAX_VALUE_FORMAT = "PROPERTY_MSG_BAD_MAX_VALUE_FORMAT";

    // Datas
    /**
     * The Constant PROPERTY_CST_INVALID_BAD_MEASURE.
     */
    public static final String PROPERTY_CST_INVALID_BAD_MEASURE = "-9999";

    /**
     *
     */
    public static final String PROPERTY_CST_NOT_AVALAIBALE_FIELD = ";NA";

    /**
     *
     */
    public static final String PROPERTY_CST_BAD_VALUE_FIELD = ";NV";

    /**
     * The Constant PROPERTY_MSG_NO_DATA.
     */
    public static final String PROPERTY_MSG_NO_DATA = "PROPERTY_MSG_NO_DATA";

    /**
     * The Constant PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE.
     */
    public static final String PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE = "PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE";

    /**
     * The Constant PROPERTY_MSG_INVALID_INT_VALUE.
     */
    public static final String PROPERTY_MSG_INVALID_INT_VALUE = "PROPERTY_MSG_INVALID_INT_VALUE";

    /**
     * The Constant PROPERTY_MSG_INVALID_FLOAT_VALUE.
     */
    public static final String PROPERTY_MSG_INVALID_FLOAT_VALUE = "PROPERTY_MSG_INVALID_FLOAT_VALUE";

    /**
     * The Constant PROPERTY_MSG_INVALID_QUALITY_CLASS.
     */
    public static final String PROPERTY_MSG_INVALID_QUALITY_CLASS = "PROPERTY_MSG_INVALID_QUALITY_CLASS";

    /**
     * The Constant PROPERTY_MSG_INVALID_VALUE_OR_BAD_QUALITY_CLASS.
     */
    public static final String PROPERTY_MSG_INVALID_VALUE_OR_BAD_QUALITY_CLASS = "PROPERTY_MSG_INVALID_VALUE_OR_BAD_QUALITY_CLASS";

    /**
     * The Constant PROPERTY_MSG_MISSING_VALUE.
     */
    public static final String PROPERTY_MSG_MISSING_VALUE = "PROPERTY_MSG_MISSING_VALUE";

    /**
     * The Constant PROPERTY_MSG_INVALID_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_DATE = "PROPERTY_MSG_INVALID_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_TIME.
     */
    public static final String PROPERTY_MSG_INVALID_TIME = "PROPERTY_MSG_INVALID_TIME";

    /**
     * The Constant PROPERTY_MSG_INVALID_INTERVAL_DATE_TIME.
     */
    public static final String PROPERTY_MSG_INVALID_INTERVAL_DATE_TIME = "PROPERTY_MSG_INVALID_INTERVAL_DATE_TIME";
    /**
     * The Constant PROPERTY_MSG_MISSING_UNIT_FOR_FARIABLE_AND_DATATYPE
     *
     * @link(String).
     */
    public static final String PROPERTY_MSG_MISSING_UNIT_FOR_FARIABLE_AND_DATATYPE = "PROPERTY_MSG_MISSING_UNIT_FOR_FARIABLE_AND_DATATYPE";
    /**
     * The Constant PROPERTY_MSG_EMPTY_FILE.
     */
    public static final String PROPERTY_MSG_EMPTY_FILE = "PROPERTY_MSG_EMPTY_FILE";

    /**
     * The Constant PROPERTY_MSG_DOUBLON_DATE.
     */
    public static final String PROPERTY_MSG_DOUBLON_DATE = "PROPERTY_MSG_DOUBLON_DATE";

    /**
     * The Constant PROPERTY_MSG_DOUBLON_DATE_TIME.
     */
    public static final String PROPERTY_MSG_DOUBLON_DATE_TIME = "PROPERTY_MSG_DOUBLON_DATE_TIME";

    /**
     * The Constant PROPERTY_MSG_DOUBLON_DATE_TIME_FOLLOW_VARIABLE_DEPTH.
     */
    public static final String PROPERTY_MSG_DOUBLON_DATE_TIME_FOLLOW_VARIABLE_DEPTH = "PROPERTY_MSG_DOUBLON_DATE_TIME_FOLLOW_VARIABLE_DEPTH";

    /**
     * The Constant PROPERTY_MSG_ERROR_INSERTION_MEASURE.
     */
    public static final String PROPERTY_MSG_ERROR_INSERTION_MEASURE = "PROPERTY_MSG_ERROR_INSERTION_MEASURE";

    /**
     * The Constant PROPERTY_MSG_CHECKING_FORMAT_FILE.
     */
    public static final String PROPERTY_MSG_CHECKING_FORMAT_FILE = "PROPERTY_MSG_CHECKING_FORMAT_FILE";

    /**
     * The Constant PROPERTY_MSG_INTEGER_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_INTEGER_VALUE_EXPECTED = "PROPERTY_MSG_INTEGER_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_DATE_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_DATE_VALUE_EXPECTED = "PROPERTY_MSG_DATE_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_FLOAT_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_FLOAT_VALUE_EXPECTED = "PROPERTY_MSG_FLOAT_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_VALUE_EXPECTED = "PROPERTY_MSG_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_ERROR_BAD_FORMAT.
     */
    public static final String PROPERTY_MSG_ERROR_BAD_FORMAT = "PROPERTY_MSG_ERROR_BAD_FORMAT";

    /**
     * The Constant PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION
     *
     * @link(String).
     */
    public static final String PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION = "PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION";

    /**
     *
     */
    public static final String PROPERTY_MSG_IMPROPER_BEGIN_DATE = "PROPERTY_MSG_IMPROPER_BEGIN_DATE";

    /**
     *
     */
    public static final String PROPERTY_MSG_IMPROPER_END_DATE = "PROPERTY_MSG_IMPROPER_END_DATE";

    /**
     *
     */
    public static final String PROPERTY_MSG_IMPROPER_SITE = "PROPERTY_MSG_IMPROPER_SITE";
    /**
     *
     */
    public static final String PROPERTY_MSG_IMPROPER_PLOT = "PROPERTY_MSG_IMPROPER_PLOT";
    /**
     *
     */
    public static final String YYYYMMJJHHMMSS = "yyyyMMddHHmmss";

    /**
     *
     */
    public static final String YYYYMMJJ = "yyyyMMdd";

    /**
     *
     */
    public static final String HHMMSS = "HHmmss";

    /**
     *
     */
    public static final String HH_MM_SS = "HH:mm:ss";

    /**
     *
     */
    public static final String DD_MM_YYYY_HHMMSS_FILE = "dd-MM-yyyy-HHmmss";

    /**
     *
     */
    public static final String DD_MM_YYYY_HHMMSS_DISPLAY = "dd-MM-yyyy-HHmmss";

    /**
     *
     */
    public static final String DD_MM_YYYY_HHMMSS_READ = "dd/MM/yyyyHH:mm:ss";

    /**
     *
     */
    public static final String DD_MM_YYYY_HHMMSS_DOUBLONS_FILE = "dd/MM/yyyy;HH:mm";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_BEGIN_TIME = "PROPERTY_MSG_INVALID_BEGIN_TIME";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_BEGIN_DATE_TIME = "PROPERTY_MSG_INVALID_BEGIN_DATE_TIME";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_END_TIME = "PROPERTY_MSG_INVALID_END_TIME";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_END_DATE_TIME = "PROPERTY_MSG_INVALID_END_DATE_TIME";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_DATE_TIME = "PROPERTY_MSG_INVALID_DATE_TIME";

    /**
     *
     */
    public static final String PROPERTY_MSG_WARNIN_INFO_IN8PUBLISH = "PROPERTY_MSG_WARNIN_INFO_IN8PUBLISH";

    /**
     *
     */
    public static final String PROPERTY_MSG_LINE_ERROR = "PROPERTY_MSG_LINE_ERROR";
    /**
     * The localization manager @link(ILocalizationManager).
     */
    static volatile ILocalizationManager localizationManager;
    /**
     *
     */
    protected INotificationsManager notificationsManager;
    /**
     * The delete record @link(IDeleteRecord).
     */
    IDeleteRecord deleteRecord;
    /**
     * The test format @link(ITestFormat).
     */
    ITestFormat testFormatInstance;
    /**
     * The process record @link(IProcessRecord).
     */
    IProcessRecord processRecordInstance;
    /**
     * The request properties name @link(String).
     */
    String requestPropertiesName;
    /**
     * The dataset descriptor path @link(String).
     */
    String datasetDescriptorPath;
    /**
     * The parcell datatype @link(boolean).
     */
    boolean parcelleDatatype = false;

    /**
     * Instantiates a new recorder acbb.
     */
    public RecorderACBB() {
        super();
    }

    /**
     * Gets the aCBB message.
     *
     * @param key
     * @return the ACBB message from key {@link String} the key
     * @link(String) the key
     */
    public static final String getACBBMessage(final String key) {
        return ACBBMessages.getACBBMessage(key);
    }

    /**
     * Gets the aCBB message.
     *
     * @param bundlePath
     * @param key
     * @return the ACBB message from key
     * @link(String) the bundle path
     * @link(String) the key
     * @link(String) the bundle path {@link String} the key
     */
    public static final String getACBBMessageWithBundle(final String bundlePath, final String key) {
        return ACBBMessages.getACBBMessageWithBundle(bundlePath, key);
    }

    /**
     * @param dateString
     * @param timeString
     * @return
     * @throws BusinessException
     */
    public static final LocalDateTime getDateTimeLocalFromDateStringaAndTimeString(String dateString, String timeString) throws BusinessException {

        if (Strings.isNullOrEmpty(dateString)) {
            return null;
        }
        if (Strings.isNullOrEmpty(timeString)) {
            timeString = "00:00:00";
        }
        timeString = (timeString + ":00").substring(0, DateUtil.HH_MM_SS.length());
        return DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM_SS, String.format("%s %s", dateString, timeString));
    }

    /**
     * @param beginDateddmmyyhhmmss
     * @param endDateddmmyyhhmmss
     * @return
     * @throws BadExpectedValueException
     * @throws DateTimeException
     */
    public static IntervalDate getIntervalDateLocaleForFile(final String beginDateddmmyyhhmmss,
                                                            final String endDateddmmyyhhmmss) throws BadExpectedValueException, DateTimeException {
        final IntervalDate intervalDate = new IntervalDate(beginDateddmmyyhhmmss,
                endDateddmmyyhhmmss, RecorderACBB.DD_MM_YYYY_HHMMSS_DISPLAY);
        return intervalDate;
    }

    /**
     * @param beginDateddmmyyhhmmss
     * @param endDateddmmyyhhmmss
     * @return
     * @throws BadExpectedValueException
     * @throws DateTimeException
     */
    public static IntervalDate getIntervalDateUTCForFile(final String beginDateddmmyyhhmmss,
                                                         final String endDateddmmyyhhmmss) throws BadExpectedValueException, DateTimeException {
        final IntervalDate intervalDate = new IntervalDate(beginDateddmmyyhhmmss,
                endDateddmmyyhhmmss, RecorderACBB.DD_MM_YYYY_HHMMSS_DISPLAY);
        return intervalDate;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see org.inra.ecoinfo.dataset.AbstractRecorder#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    static void setStaticLocalizationManager(final ILocalizationManager localizationManager) {
        RecorderACBB.localizationManager = localizationManager;
    }

    /**
     * Delete records.
     *
     * @param versionFile
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(VersionFile) the version file
     * @see org.inra.ecoinfo.dataset.IRecorder#deleteRecords(org.inra.ecoinfo.dataset
     * .versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecords(final VersionFile versionFile) throws BusinessException {
        this.deleteRecord.deleteRecord(versionFile);
    }

    private DatasetDescriptorACBB getDatasetDescriptor() throws IOException, SAXException,
            URISyntaxException {
        final String path = String.format(RecorderACBB.DATASET_DESCRIPTOR_PATH_PATTERN, this.datasetDescriptorPath);
        return DatasetDescriptorBuilderACBB.buildDescriptorACBB(this.getClass().getClassLoader()
                .getResourceAsStream(path));
    }

    /**
     * Gets the request properties.
     *
     * @return the request properties
     * @see org.inra.ecoinfo.acbb.dataset.IRecorderACBB#getRequestProperties()
     */
    IRequestPropertiesACBB getRequestProperties() {
        return (IRequestPropertiesACBB) ApplicationContextHolder.getContext().getBean(
                this.requestPropertiesName);
    }

    /**
     * @return the parcellDatatype
     */
    public boolean isParcelleDatatype() {
        return this.parcelleDatatype;
    }

    /**
     * process the record of the versionFile.
     *
     * @param parser                the parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding          the file encoding
     * @param datasetDescriptorACBB
     * @throws BusinessException    the business exception {@link VersionFile} {@link IRequestPropertiesACBB}
     *                              {@link DatasetDescriptorACBB} the dataset descriptor acbb
     * @throws InterruptedException
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     */
    void processRecord(final CSVParser parser, final VersionFile versionFile,
                       final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                       final DatasetDescriptorACBB datasetDescriptorACBB) throws BusinessException,
            InterruptedException {
        Allocator allocator = Allocator.getInstance();
        try {
            allocator.allocate("publish", versionFile.getFileSize() * 50);
            this.processRecordInstance.processRecord(parser, versionFile, requestProperties, fileEncoding,
                    datasetDescriptorACBB);
        } catch (InterruptedException | BusinessException e) {
            AbstractRecorder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        } finally {
            allocator.free("publish");
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param fileEncoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     * @see org.inra.ecoinfo.dataset.AbstractRecorder#processRecord(com.Ostermiller .util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile, java.lang.String) does nothing
     * because of overload record ()
     */
    @Override
    protected void processRecord(final CSVParser parser, final VersionFile versionFile,
                                 final String fileEncoding) throws BusinessException {
    }

    /**
     * Record.
     *
     * @param versionFile
     * @param fileEncoding
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     * @see org.inra.ecoinfo.dataset.AbstractRecorder#record(org.inra.ecoinfo
     * .dataset.versioning.entity.VersionFile, java.lang.String) test the format and process
     * the record of the version
     */
    @Override
    public void record(final VersionFile versionFile, final String fileEncoding)
            throws BusinessException {
        try {
            final IRequestPropertiesACBB requestProperties = this.getRequestProperties();
            final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(this
                    .getLocalizationManager().getMessage(RecorderACBB.ACBB_DATASET_BUNDLE_NAME,
                            RecorderACBB.PROPERTY_MSG_ERROR_BAD_FORMAT));
            byte[] datasSanitized = new byte[0];
            try {
                datasSanitized = Utils.sanitizeData(versionFile.getData(),
                        this.getLocalizationManager());
            } catch (final BusinessException e) {
                try {
                    AbstractRecorder.LOGGER.debug(e.getMessage(), e);
                    badsFormatsReport.addException(e);
                    throw new BadFormatException(badsFormatsReport);
                } catch (BadFormatException ex) {
                    LOGGER.info(ex.getMessage());
                }
            }
            CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                    datasSanitized), fileEncoding), AbstractRecorder.SEPARATOR);
            final DatasetDescriptorACBB datasetDescriptorACBB = this.getDatasetDescriptor();
            this.testFormat(parser, versionFile, requestProperties, fileEncoding,
                    datasetDescriptorACBB);
            parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                    versionFile.getData()), fileEncoding), AbstractRecorder.SEPARATOR);
            try {
                this.processRecord(parser, versionFile, requestProperties, fileEncoding,
                        datasetDescriptorACBB);
            } catch (InterruptedException e) {
                AbstractRecorder.LOGGER.error("interrupted thread", e);
            }
        } catch (final BadDelimiterException | IOException | SAXException | URISyntaxException | BadFormatException ex) {
            throw new BusinessException(ex);
        } catch (final BusinessException ex) {
            AbstractRecorder.LOGGER.error("uncatched error", ex);
            throw new BusinessException(ex);
        }
    }

    /**
     * Sets the dataset descriptor path @link(String).
     *
     * @param datasetDescriptorPath the new dataset descriptor path
     * @link(String)
     */
    public final void setDatasetDescriptorPath(final String datasetDescriptorPath) {
        this.datasetDescriptorPath = datasetDescriptorPath;
    }

    /**
     * Sets the delete record.
     *
     * @param deleteRecord the new delete record
     * @see org.inra.ecoinfo.acbb.dataset.IRecorderACBB#setDeleteRecord(org.inra .
     * ecoinfo.acbb.dataset.IDeleteRecord)
     */
    @Override
    public final void setDeleteRecord(final IDeleteRecord deleteRecord) {
        this.deleteRecord = deleteRecord;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see org.inra.ecoinfo.dataset.AbstractRecorder#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        RecorderACBB.setStaticLocalizationManager(localizationManager);
        super.setLocalizationManager(localizationManager);
    }

    /**
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * @param parcellDatatype the parcellDatatype to set
     */
    public final void setParcellDatatype(final boolean parcellDatatype) {
        this.parcelleDatatype = parcellDatatype;
    }

    /**
     * Sets the process record.
     *
     * @param processRecord the new process record
     * @see org.inra.ecoinfo.acbb.dataset.IRecorderACBB#setProcessRecord(org.inra
     * .ecoinfo.acbb.dataset.IProcessRecord)
     */
    @Override
    public final void setProcessRecord(final IProcessRecord processRecord) {
        this.processRecordInstance = processRecord;
    }

    /**
     * Sets the request properties name.
     *
     * @param requestPropertiesName the new request properties name
     * @see org.inra.ecoinfo.acbb.dataset.IRecorderACBB#setRequestPropertiesName(java.lang.String)
     */
    @Override
    public final void setRequestPropertiesName(final String requestPropertiesName) {
        this.requestPropertiesName = requestPropertiesName;
    }

    /**
     * Sets the test format.
     *
     * @param testFormat the new test format
     * @see org.inra.ecoinfo.acbb.dataset.IRecorderACBB#setTestFormat(org.inra.ecoinfo
     * .acbb.dataset.ITestFormat)
     */
    @Override
    public final void setTestFormat(final ITestFormat testFormat) {
        this.testFormatInstance = testFormat;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao {@link IVersionFileDAO}
     *                       the new version file dao
     */
    @Override
    public final void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
    }

    /**
     * Test format.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param datasetDescriptor
     * @throws BadFormatException the bad format exception
     * @throws BusinessException  call test the format of a version file
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser {@link VersionFile} {@link IRequestPropertiesACBB}
     * @link(String) the encoding {@link DatasetDescriptorACBB}
     */
    void testFormat(final CSVParser parser, final VersionFile versionFile,
                    final IRequestPropertiesACBB requestProperties, final String encoding,
                    final DatasetDescriptorACBB datasetDescriptor) throws BadFormatException,
            BusinessException {
        this.testFormatInstance.testFormat(parser, versionFile, requestProperties, encoding,
                datasetDescriptor);
    }

    /**
     * Test format.
     *
     * @param versionFile
     * @param encoding
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(String) the encoding
     * @link(VersionFile) the version file
     * @link(String) the encoding
     * @see org.inra.ecoinfo.dataset.AbstractRecorder#testFormat(org.inra.ecoinfo
     * .dataset.versioning.entity.VersionFile, java.lang.String)
     */
    @Override
    public void testFormat(final VersionFile versionFile, final String encoding)
            throws BusinessException {
        try {
            final IRequestPropertiesACBB requestProperties = this.getRequestProperties();
            CSVParser parser = null;
            final DatasetDescriptorACBB datasetDescriptorACBB;
            final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(this
                    .getLocalizationManager().getMessage(RecorderACBB.ACBB_DATASET_BUNDLE_NAME,
                            RecorderACBB.PROPERTY_MSG_ERROR_BAD_FORMAT));
            byte[] datasSanitized = null;
            try {
                datasSanitized = Utils.sanitizeData(versionFile.getData(),
                        this.getLocalizationManager());
                versionFile.setData(datasSanitized);
            } catch (final BusinessException e) {
                AbstractRecorder.LOGGER.debug(e.getMessage(), e);
                badsFormatsReport.addException(e);
                try {
                    throw new BadFormatException(badsFormatsReport);
                } catch (BadFormatException ex) {
                    throw new BusinessException(ex);
                }
            }
            try {
                parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                        datasSanitized), encoding), AbstractRecorder.SEPARATOR);
                datasetDescriptorACBB = this.getDatasetDescriptor();
            } catch (final BadDelimiterException | SAXException | URISyntaxException | IOException e) {
                AbstractRecorder.LOGGER.debug(e.getMessage(), e);
                throw new BusinessException(e.getMessage(), e);
            }
            this.testFormat(parser, versionFile, requestProperties, encoding, datasetDescriptorACBB);
        } catch (BadFormatException ex) {
            throw new BusinessException(ex);
        }
    }

    /**
     * Test header.
     *
     * @param badsFormatsReport
     * @param parser
     * @return the int
     * @throws BusinessException the business exception
     * @link(BadsFormatsReport) the bads formats report
     * @link(CSVParser) the parser
     * @link(BadsFormatsReport) the bads formats report
     * @link(CSVParser) the parser
     * @see org.inra.ecoinfo.dataset.AbstractRecorder#testHeader(org.inra.ecoinfo
     * .dataset.BadsFormatsReport, com.Ostermiller.util.CSVParser) does nothing because of
     * overload record ()
     */
    public int testHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser)
            throws BusinessException {
        return 0;
    }

}
