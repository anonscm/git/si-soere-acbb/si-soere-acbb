package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.util.List;

/**
 * process the record of flux files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
public abstract class AbstractProcessRecordITK extends
        AbstractProcessRecord<ListeItineraire, IListeItineraireDAO> {

    /**
     *
     */
    protected static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.impl.messages";
    /**
     *
     */
    protected static final String PROPERTY_MSG_BAD_DATE_FOR_INTERVAL = "PROPERTY_MSG_BAD_DATE_FOR_INTERVAL";
    static final String PROPERTY_MSG_MISSING_PLOT_TRACKING = "PROPERTY_MSG_MISSING_PLOT_TRACKING";
    static final String PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT = "PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new abstract process record itk.
     */
    public AbstractProcessRecordITK() {
        super();
    }

    /**
     * Builds the new line.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @throws PersistenceException
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(IRequestPropertiesITK) the request properties itk
     */
    protected abstract void buildNewLines(VersionFile version,
                                          List<? extends AbstractLineRecord> lines, ErrorsReport errorsReport,
                                          IRequestPropertiesITK requestPropertiesITK) throws PersistenceException;

    /**
     * Gets the generic columns.
     *
     * @param errorsReport          the errors report {@link ErrorsReport}
     * @param lineCount             the line count long
     * @param cleanerValues         the cleaner values {@link CleanerValues}
     * @param lineRecord            the line record {@link AbstractLineRecord}
     * @param datasetDescriptorACBB
     * @param intervalDate
     * @param requestPropertiesITK
     * @return the generic columns
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(IRequestPropertiesITK) the request properties itk
     */
    protected int getGenericColumns(final ErrorsReport errorsReport, final long lineCount,
                                    final CleanerValues cleanerValues, final AbstractLineRecord lineRecord,
                                    final DatasetDescriptorACBB datasetDescriptorACBB,
                                    final IRequestPropertiesITK requestPropertiesITK, IntervalDate intervalDate) {
        int index = 0;
        index = this.getParcelle(lineRecord, errorsReport, lineCount, index, cleanerValues,
                datasetDescriptorACBB, requestPropertiesITK);
        if (lineRecord.getParcelle() == null) {
            return ++index;
        }
        final LocalDate measureDate = this.getDate(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB);
        if (!intervalDate.isInto(measureDate.atStartOfDay())) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_NAME,
                    PROPERTY_MSG_BAD_DATE_FOR_INTERVAL),
                    lineCount,
                    index,
                    DateUtil.getUTCDateTextFromLocalDateTime(measureDate, DateUtil.DD_MM_YYYY),
                    intervalDate.getBeginDateToString(),
                    intervalDate.getEndDateToString()));
        }
        lineRecord.setDate(measureDate);
        this.updateVersionDeTraitement(lineRecord, errorsReport, lineCount, index,
                datasetDescriptorACBB, requestPropertiesITK);
        lineRecord.setObservation(this.getObservation(errorsReport, lineCount, index, cleanerValues));
        index++;
        final int rotationNumber = this.getInt(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB);
        lineRecord.setRotationNumber(rotationNumber <= 0 ? 0 : rotationNumber);
        lineRecord.setAnneeNumber(this.getInt(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB));
        lineRecord.setPeriodeNumber(this.getInt(errorsReport, lineCount, index++, cleanerValues,
                datasetDescriptorACBB));
        return index;
    }

    /**
     * @return
     */
    public IListeItineraireDAO getListeItineraireDAO() {
        return this.listeACBBDAO;
    }

    /**
     * @param listeACBBDAO
     */
    public void setListeItineraireDAO(IListeItineraireDAO listeACBBDAO) {
        super.setListeACBBDAO(listeACBBDAO);
    }

    /**
     * Gets the observation.
     *
     * @param cleanerValues
     * @return the observation {@link CleanerValues} the cleaner values
     * @link(CleanerValues) the cleaner values
     */
    protected String getObservation(ErrorsReport errorsReport, long lineNumber, int index, final CleanerValues cleanerValues) {
        try {
            return cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineNumber, index).getMessage());
            return null;
        }
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestProperties
     * @return
     */
    protected int getParcelle(final AbstractLineRecord lineRecord, final ErrorsReport errorsReport,
                              final long lineCount, final int index, final CleanerValues cleanerValues,
                              final DatasetDescriptorACBB datasetDescriptorACBB,
                              final IRequestPropertiesITK requestProperties) {
        int returnIndex = index;
        final Parcelle parcelle = this.readDbParcelle(errorsReport, lineCount, returnIndex++,
                cleanerValues, datasetDescriptorACBB, requestProperties);
        lineRecord.setParcelle(parcelle);
        return returnIndex;
    }

    /**
     * @param lineRecord
     * @param errorsReport
     * @param lineCount
     * @param index
     * @param datasetDescriptorACBB
     * @param requestPropertiesITK
     */
    public void updateVersionDeTraitement(AbstractLineRecord lineRecord, ErrorsReport errorsReport,
                                          long lineCount, int index, DatasetDescriptorACBB datasetDescriptorACBB,
                                          IRequestPropertiesITK requestPropertiesITK) {
        SuiviParcelle suiviParcelle;
        LocalDate localeDate = lineRecord.getDate();
        suiviParcelle = this.suiviParcelleDAO.retrieveSuiviParcelle(lineRecord.getParcelle(), localeDate).orElse(null);
        if (suiviParcelle == null) {
            String localizedParcelleName = this.localizationManager.newProperties(
                    Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getName(), lineRecord.getParcelle().getName());
            String localizedSiteName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getSite().getName(),
                    lineRecord.getParcelle().getSite().getName());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordITK.BUNDLE_NAME,
                    AbstractProcessRecordITK.PROPERTY_MSG_MISSING_PLOT_TRACKING),
                    DateUtil.getUTCDateTextFromLocalDateTime(localeDate, DateUtil.DD_MM_YYYY),
                    localizedParcelleName,
                    localizedSiteName));
            return;
        }
        lineRecord.setSuiviParcelle(suiviParcelle);
        lineRecord.setTraitementProgramme(suiviParcelle.getTraitement());
        VersionDeTraitement versionDeTraitement = this.versionDeTraitementDAO.retrieveVersiontraitement(lineRecord.getTraitementProgramme(), localeDate).orElse(null);
        if (versionDeTraitement == null) {
            String localizedTreatmentAffichage = this.localizationManager.newProperties(
                    TraitementProgramme.NAME_ENTITY_JPA,
                    TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE).getProperty(
                    lineRecord.getTraitementProgramme().getAffichage(),
                    lineRecord.getTraitementProgramme().getAffichage());
            String localizedSiteName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME).getProperty(
                    lineRecord.getParcelle().getSite().getName(),
                    lineRecord.getParcelle().getSite().getName());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    AbstractProcessRecordITK.BUNDLE_NAME,
                    AbstractProcessRecordITK.PROPERTY_MSG_MISSING_VERSION_OF_TREATMENT),
                    DateUtil.getUTCDateTextFromLocalDateTime(localeDate, DateUtil.DD_MM_YYYY),
                    lineRecord
                            .getTraitementProgramme().getNom(), localizedTreatmentAffichage,
                    localizedSiteName));
            return;
        }
        lineRecord.setVersionDeTraitement(versionDeTraitement);
        lineRecord.setVersion(versionDeTraitement.getVersion());
        lineRecord.setDateDebutDetraitementSurParcelle(suiviParcelle.getDateDebutTraitement());
    }
}
