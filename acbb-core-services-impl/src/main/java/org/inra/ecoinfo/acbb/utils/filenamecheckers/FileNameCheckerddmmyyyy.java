/*
 *
 */
package org.inra.ecoinfo.acbb.utils.filenamecheckers;

import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

import java.util.regex.Matcher;

/**
 * The Class FileNameCheckerddmmyyyy.
 */
public class FileNameCheckerddmmyyyy extends AbstractACBBFileNameChecker {

    /**
     * The Constant DATE_PATTERN @link(String).
     */
    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     * @see org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker
     * #getDatePattern()
     */
    @Override
    protected String getDatePattern() {
        return DateUtil.DD_MM_YYYY_FILE;
    }

    /**
     * Gets the file path.
     *
     * @param version
     * @return the file path @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo
     * .dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public String getFilePath(final VersionFile version) {
        Dataset dataset = version.getDataset();
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH,
                ACBBUtils.getSiteFromDataset(dataset).getPath(),
                ACBBUtils.getDatatypeFromDataset(dataset).getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), getDatePattern()),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), getDatePattern()),
                version.getVersionNumber());
    }

    /**
     * @param dataset
     * @return
     */
    @Override
    public String getFilePath(final Dataset dataset) {
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH,
                ACBBUtils.getSiteFromDataset(dataset).getPath(),
                ACBBUtils.getDatatypeFromDataset(dataset).getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), getDatePattern()),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), getDatePattern()),
                dataset.getVersions().size());
    }

    /**
     * Test dates.
     *
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(VersionFile) the version
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(Matcher)
     */
    @Override
    protected void testDates(final VersionFile version, final String currentSite,
                             final String currentDatatype, final Matcher splitFilename)
            throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateddMMyyyy(splitFilename.group(3),
                    splitFilename.group(4));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(
                    AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype,
                    DateUtil.DD_MM_YYYY_FILE, DateUtil.DD_MM_YYYY_FILE));
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }
}
