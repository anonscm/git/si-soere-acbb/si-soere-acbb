/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.agroecosystemesitethemedatatypeparcelle;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.IAgroEcosystemeDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<RealNode> {

    /**
     *
     */
    protected static final String PATTERN_ORDERED_PATH_WITH_PARCELLE = "^([^,]*),([^,]*),([^,]*),([^,]*),([^,]*)$";
    /**
     * The Constant BUNDLE_NAME_DATASET.
     */
    static final String BUNDLE_NAME_DATASET = "org.inra.ecoinfo.acbb.dataset.messages";

    /**
     * The Constant BUNDLE_NAME_DATASET.
     */
    static final String BUNDLE_NAME_SUPER_DATASET = "org.inra.ecoinfo.refdata.datatypevariableunite.messages";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.agroecosystemesitethemedatatype.messages";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB.
     *
     * @link(String).
     */
    static final String PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB";

    /**
     * The Constant PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB @link(String).
     */
    static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";

    /**
     * The LOGGER.
     */
    static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
    // a voir
    final static int CODE_CONF_WITH_PARCELLE = AbstractMgaIOConfigurator.DATASET_CONFIGURATION;
    final static int CODE_CONF_WITH_NO_PARCELLE = 7;
    private static final String PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB";
    /**
     * The sites possibles @link(Map<String,String[]>).
     */
    final Map<String, String[]> sitesPossibles = new ConcurrentHashMap();
    /**
     * The themes possibles @link(Map<String,String[]>).
     */
    final Map<String, String[]> themesPossibles = new ConcurrentHashMap();
    /**
     * The datatypes possibles @link(Map<String,String[]>).
     */
    final Map<String, String[]> datatypesPossibles = new ConcurrentHashMap();
    /**
     * The datatypes possibles @link(Map<String,String[]>).
     */
    final Map<String, String[]> parcellesPossibles = new ConcurrentHashMap();
    /**
     * The agroecosysteme site theme datatype dao.
     */
    IAgroecosystemeSiteThemeDatatypeParcelleDAO agroecosystemeSiteThemeDatatypeParcelleDAO;
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    /**
     * The datatype dao.
     */
    IDatatypeDAO datatypeDAO;
    /**
     * The theme dao.
     */
    IThemeDAO themeDAO;
    /**
     * The site dao.
     */
    ISiteACBBDAO siteDAO;
    /**
     * The agroecosysteme dao.
     */
    IAgroEcosystemeDAO agroecosystemeDAO;
    IParcelleDAO parcelleDAO;

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.CSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        Stream<String> buildOrderedPaths
                = mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                configurator.getConfiguration(CODE_CONF_WITH_NO_PARCELLE)
                        .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)))
                        .getEntryOrder(),
                PatternConfigurator.SPLITER, false);
        buildOrderedPaths
                .map(p -> mgaServiceBuilder.getRecorder().getRealNodeByNKey(p))
                .map(p -> p.orElse(null))
                .forEach(rn -> mgaServiceBuilder.getRecorder().remove(rn));
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
        treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
        treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
        policyManager.clearTreeFromSession();
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.CSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<RealNode> getAllElements() {

        List<RealNode> nodes = null;
        try {
            nodes = agroecosystemeSiteThemeDatatypeParcelleDAO.loadRealDatatypeNodes();
            nodes = nodes.stream().sorted((o1, o2) -> o1.getPath().compareTo(o2.getPath())).collect(Collectors.toList());
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return nodes;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param node
     * @return the new line model grid metadata @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata
     * (java.lang.Object)
     * @link(AgroecosystemeSiteThemeDatatype) the agroecosysteme site theme datatype
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final RealNode node) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata siteColumn = new ColumnModelGridMetadata(getNodeableCode(SiteACBB.class, node, (n) -> n.getCode()), this.sitesPossibles, null, true, false, true);
        final ColumnModelGridMetadata themeColumn = new ColumnModelGridMetadata(getNodeableCode(Theme.class, node, (n) -> n.getCode()), this.themesPossibles, null, true, false, true);
        final ColumnModelGridMetadata datatypeColumn = new ColumnModelGridMetadata(getNodeableCode(DataType.class, node, (n) -> n.getCode()), this.datatypesPossibles, null, true, false, true);
        final List<ColumnModelGridMetadata> refsSite = new LinkedList();
        final ColumnModelGridMetadata parcelleColumn = new ColumnModelGridMetadata(getNodeableCode(Parcelle.class, node, (n) -> n.getName()),
                this.parcellesPossibles, null, true, false, false);
        refsSite.add(parcelleColumn);
        siteColumn.setRefs(refsSite);
        parcelleColumn.setValue(getNodeableCode(Parcelle.class, node, (n) -> n.getName()));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(siteColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(themeColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(datatypeColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(parcelleColumn);
        return lineModelGridMetadata;
    }

    /**
     * @param <T>
     * @param clazz
     * @param node
     * @param method
     * @return
     */
    protected <T extends Nodeable> String getNodeableCode(Class<T> clazz, RealNode node, Function<RealNode, String> method) {
        return Optional.ofNullable(node)
                .map(n -> n.getNodeByNodeableTypeResource(clazz))
                .map(method)
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.CSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<RealNode> initModelGridMetadata() {
        try {
            this.updatePathsSitesPossibles();
            this.updateNamesThemesPossibles();
            this.updateNamesDatatypesPossibles();
            this.updateNamesParcellesPossibles();
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();

        try {
            List listNodes = new ArrayList();
            List<String> buildOrderedPaths = buildOrderedPathes(file);
            Stream<INode> listChild = buildLeaves(buildOrderedPaths)
                    .peek(node -> listNodes.add(node));
            persistNodes(listChild, parser);
            
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            addVariableNodes(listNodes);
            treeApplicationCacheManager.removeSkeletonTree(CODE_CONF_WITH_NO_PARCELLE);
            treeApplicationCacheManager.removeSkeletonTree(CODE_CONF_WITH_PARCELLE);
            treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
            policyManager.clearTreeFromSession();
        } catch (final javax.persistence.PersistenceException | BusinessException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void addVariableNodes(List<INode> listNodes) {
        Map<DataType, List<DatatypeVariableUniteACBB>> datatypeNodes = datatypeVariableUniteACBBDAO.getAllDatatypeVariableUniteACBB().stream()
                .collect(Collectors.groupingBy(DatatypeVariableUniteACBB::getDatatype));
        listNodes.stream()
                .forEach((node) -> {
                    DataType datatype = (DataType) node.getNodeable();
                    if (datatypeNodes.containsKey(datatype)) {
                        datatypeNodes.get(datatype).forEach((dvu) -> {
                            String path = String.format("%s%s%s", node.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = new RealNode(node.getRealNode(), null, dvu, path);
                            mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) node, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        });
                    }
                });
    }

    /**
     * @param listChild
     * @param parser
     */
    @Override
    protected void persistNodes(Stream<INode> listChild, final CSVParser parser) {
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        listChild.forEach((node) -> {
            mgaRecorder.persist(node);
            if (parser.getLastLineNumber() % 50 == 0) {
                mgaRecorder.getEntityManager().flush();
                mgaRecorder.getEntityManager().clear();
            }
        });
    }

    /**
     * <p>
     * from file, build new pathes for
     * agroecosystemeSiteThemeDatatypeParcelle</p>
     *
     * @param file
     * @return
     */
    protected List<String> buildOrderedPathes(final File file) throws BusinessException {
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        List<String> pathes = agroecosystemeSiteThemeDatatypeParcelleDAO.getPathes();
        List<String> buildOrderedPaths
                = mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                configurator.getConfiguration(CODE_CONF_WITH_PARCELLE)
                        .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)))
                        .getEntryOrder(),
                PatternConfigurator.SPLITER, true)
                .map(p -> completePath(p))
                .filter(p -> !pathes.contains(p))
                .collect(Collectors.toList());
        return buildOrderedPaths;
    }

    /**
     * <p>
     * from a list af ordered pathes build a list of INode leaves</p>
     *
     * @param buildOrderedPaths
     * @return
     * @throws javax.persistence.PersistenceException
     */
    protected Stream<INode> buildLeaves(List<String> buildOrderedPaths) throws BusinessException {
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        Stream<INodeable> nodeables = mgaRecorder.getNodeables();
        HashMap<String, INodeable> entities = new HashMap();
        nodeables.forEach((nodeable) -> {
            entities.put(nodeable.getUniqueCode(), nodeable);
        });
        final IMgaIOConfiguration configurationWithParcelle = configurator.getConfiguration(CODE_CONF_WITH_PARCELLE)
                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)));
        Class<INodeable> stickyLeafType = configurationWithParcelle
                .getStickyLeafType();
        List<INodeable> stickyNodeables = null;
        if (configurationWithParcelle.getStickyLeafType() != null) {
            stickyNodeables = mgaRecorder.getNodeables(stickyLeafType).collect(Collectors.toList());
        }
        Map<Boolean, List<String>> partitionOnPathes = buildOrderedPaths.stream()
                .collect(Collectors.partitioningBy(p -> p.matches(PATTERN_ORDERED_PATH_WITH_PARCELLE)));
        Stream<INode> listChild = mgaBuilder.buildLeavesForPathes(
                partitionOnPathes.get(Boolean.TRUE).stream(),
                configurationWithParcelle.getEntryType(),
                entities,
                WhichTree.TREEDATASET,
                stickyNodeables).collect(Collectors.toList()).stream();

        return StreamUtils.concat(listChild, mgaBuilder.buildLeavesForPathes(
                partitionOnPathes.get(Boolean.FALSE).stream(),
                configurator.getConfiguration(CODE_CONF_WITH_NO_PARCELLE)
                        .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_NO_PARCELLE))).getEntryType(),
                entities,
                WhichTree.TREEDATASET,
                stickyNodeables));
    }

    /**
     * @param agroecosystemeDAO
     */
    public void setAgroEcosystemeDAO(IAgroEcosystemeDAO agroecosystemeDAO) {
        this.agroecosystemeDAO = agroecosystemeDAO;
    }

    /**
     * Retrieve db datatype.
     *
     * @param errorsReport
     * @param datatypeCode
     * @return the data type
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     */
    DataType retrieveDBDatatype(final ErrorsReport errorsReport, final String datatypeCode)
            throws PersistenceException {
        final DataType datatype = this.datatypeDAO.getByCode(datatypeCode).orElse(null);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_NAME_SUPER_DATASET,
                    Recorder.PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE), datatypeCode));
        }
        return datatype;
    }

    Parcelle retrieveDBParcelle(final ErrorsReport errorsReport, final SiteACBB site,
                                final String parcelleCode) throws PersistenceException {
        final Parcelle parcelle = this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(parcelleCode, site)).orElse(null);
        if (parcelle == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB), site.getName(),
                    parcelleCode));
        }
        return parcelle;
    }

    /**
     * Retrieve db site.
     *
     * @param errorsReport
     * @param pathSite
     * @return the site acbb
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the path site
     * @link(ErrorsReport) the errors report
     * @link(String) the path site
     */
    SiteACBB retrieveDBSite(final ErrorsReport errorsReport, final String pathSite)
            throws PersistenceException {
        final SiteACBB site = (SiteACBB) this.siteDAO.getByPath(pathSite).orElse(null);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), pathSite));
        }
        return site;
    }

    /**
     * Retrieve db theme.
     *
     * @param errorsReport
     * @param themeCode
     * @return the theme
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the theme code
     * @link(ErrorsReport) the errors report
     * @link(String) the theme code
     */
    Theme retrieveDBTheme(final ErrorsReport errorsReport, final String themeCode)
            throws PersistenceException {
        final Theme theme = this.themeDAO.getByCode(themeCode).orElse(null);
        if (theme == null) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH,
                    Recorder.PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB), themeCode));
        }
        return theme;
    }

    /**
     * @param agroecosystemeSiteThemeDatatypeParcelleDAO
     */
    public void setAgroecosystemeSiteThemeDatatypeParcelleDAO(
            IAgroecosystemeSiteThemeDatatypeParcelleDAO agroecosystemeSiteThemeDatatypeParcelleDAO) {
        this.agroecosystemeSiteThemeDatatypeParcelleDAO = agroecosystemeSiteThemeDatatypeParcelleDAO;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public final void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * @param parcelleDAO
     */
    public void setParcelleDAO(IParcelleDAO parcelleDAO) {
        this.parcelleDAO = parcelleDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public final void setSiteDAO(final ISiteACBBDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Sets the theme dao.
     *
     * @param themeDAO the new theme dao
     */
    public final void setThemeDAO(final IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    /**
     * Update names datatypes possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    void updateNamesDatatypesPossibles() throws PersistenceException {
        this.datatypesPossibles.clear();
        final List<DataType> datatypes = this.datatypeDAO.getAll(DataType.class);
        final String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (final DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        this.datatypesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesDatatypesPossibles);
    }

    void updateNamesParcellesPossibles() throws PersistenceException {
        final List<SiteACBB> localSites = this.siteDAO.getAllSitesACBB();
        this.parcellesPossibles.clear();
        for (final Site site : localSites) {
            final List<Parcelle> parcelles = this.parcelleDAO.getBySite(site);
            final String[] namesParcellesPossibles = new String[parcelles.size() + 1];
            if (!parcelles.isEmpty()) {
                int index = 0;
                namesParcellesPossibles[index++] = org.apache.commons.lang.StringUtils.EMPTY;
                for (final Parcelle parcelle : parcelles) {
                    namesParcellesPossibles[index++] = parcelle.getName();
                }
                this.parcellesPossibles.put(site.getName(), namesParcellesPossibles);
            } else {
                this.parcellesPossibles.put(site.getName(), new String[]{""});
            }
            this.parcellesPossibles.put(site.getName(), namesParcellesPossibles);
        }

    }

    /**
     * Update names themes possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    void updateNamesThemesPossibles() throws PersistenceException {
        this.themesPossibles.clear();
        final List<Theme> themes = this.themeDAO.getAll(Theme.class);
        final String[] namesThemesPossibles = new String[themes.size()];
        int index = 0;
        for (final Theme theme : themes) {
            namesThemesPossibles[index++] = theme.getName();
        }
        this.themesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesThemesPossibles);
    }

    /**
     * Update paths sites possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    void updatePathsSitesPossibles() throws PersistenceException {
        this.sitesPossibles.clear();
        final List<SiteACBB> sites = this.siteDAO.getAllSitesACBB();
        final String[] pathsSitesPossibles = new String[sites.size()];
        int index = 0;
        for (final SiteACBB site : sites) {
            pathsSitesPossibles[index++] = site.getName();
        }
        this.sitesPossibles.put(ColumnModelGridMetadata.NULL_KEY, pathsSitesPossibles);
    }

    private String completePath(String p) {
        p = p.replaceAll("^([^,]*),([^,]*),([^,]*),([^,]*)$", "$1,$1_$4,$2,$3");
        p = p.replaceAll("^([^,]*),", getAgroEcosysteme(p) + ",$1,");
        return p;
    }

    private String getAgroEcosysteme(String p) {
        String[] split = p.split(PatternConfigurator.PATH_SEPARATOR);

        return siteDAO.getByPath(Utils.createCodeFromString(split[0]))
                .map(site -> ((SiteACBB) site).getAgroEcosysteme().getCode())
                .orElseGet(String::new);
    }

    public void setDatatypeVariableUniteACBBDAO(IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    public void setAgroecosystemeDAO(IAgroEcosystemeDAO agroecosystemeDAO) {
        this.agroecosystemeDAO = agroecosystemeDAO;
    }

}
