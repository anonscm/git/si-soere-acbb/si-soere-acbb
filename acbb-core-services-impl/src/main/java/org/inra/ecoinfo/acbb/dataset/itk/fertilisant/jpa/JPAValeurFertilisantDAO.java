/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.IValeurFertilisantDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant;

/**
 * The Class JPAValeurTravailDuSolDAO.
 */
public class JPAValeurFertilisantDAO extends AbstractJPADAO<ValeurFertilisant> implements
        IValeurFertilisantDAO {

}
