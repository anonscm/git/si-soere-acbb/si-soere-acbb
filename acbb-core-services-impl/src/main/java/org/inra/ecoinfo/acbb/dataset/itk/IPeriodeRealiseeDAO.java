/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;

import java.util.Optional;

/**
 * The Interface IPeriodeRealiseeDAO.
 */
public interface IPeriodeRealiseeDAO extends IDAO<PeriodeRealisee> {

    /**
     * Gets the by n key.
     *
     * @param anneeRealisee the annee realisee
     * @param periode       the periode
     * @return the by n key
     */
    Optional<PeriodeRealisee> getByNKey(AnneeRealisee anneeRealisee, int periode);
}
