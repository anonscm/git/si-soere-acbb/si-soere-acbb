package org.inra.ecoinfo.acbb.extraction;

import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Class AbstractDatesFormParam.
 */
public abstract class AbstractDatesFormParam implements IDateFormParameter {

    /**
     * The Constant START_INDEX.
     */
    public static final String START_INDEX = "start";
    /**
     * The Constant END_INDEX.
     */
    public static final String END_INDEX = "end";
    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractDatesFormParam.class);
    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.utils.vo.messages";
    /**
     * The Constant MSG_FROM.
     */
    protected static final String MSG_FROM = "PROPERTY_MSG_FROM";
    /**
     * The Constant MSG_TO.
     */
    protected static final String MSG_TO = "PROPERTY_MSG_TO";
    /**
     * The Constant DATE_FORMAT.
     */
    protected static final String DATE_FORMAT = "dd/MM/yyyy";
    /**
     * The Constant DATE_FORMAT_WT_YEAR.
     */
    protected static final String DATE_FORMAT_WT_YEAR = "dd/MM";
    /**
     * The Constant INITIAL_SQL_CONDITION.
     */
    protected static final String INITIAL_SQL_CONDITION = "and (";
    /**
     * The Constant SQL_CONDITION.
     */
    protected static final String SQL_CONDITION = "%s between :%s and :%s or ";
    /**
     * The Constant END_PREFIX.
     */
    protected static final String END_PREFIX = "end";
    /**
     * The Constant START_PREFIX.
     */
    protected static final String START_PREFIX = "start";
    /**
     * The Constant MSG_END_DATE_INVALID.
     */
    private static final String MSG_END_DATE_INVALID = "PROPERTY_MSG_END_DATE_INVALID";
    /**
     * The Constant MSG_START_DATE_INVALID.
     */
    private static final String MSG_START_DATE_INVALID = "PROPERTY_MSG_START_DATE_INVALID";
    /**
     * The Constant MSG_END_DATE_EMPTY.
     */
    private static final String MSG_END_DATE_EMPTY = "PROPERTY_MSG_END_DATE_EMPTY";
    /**
     * The Constant MSG_START_DATE_EMPTY.
     */
    private static final String MSG_START_DATE_EMPTY = "PROPERTY_MSG_START_DATE_EMPTY";
    /**
     * The Constant MSG_BAD_PERIODE.
     */
    private static final String MSG_BAD_PERIODE = "PROPERTY_MSG_BAD_PERIODE";
    /**
     * The Constant OR_CONDITION.
     */
    private static final String OR_CONDITION = "or";
    /**
     * The periods.
     */
    protected List<Map<String, String>> periods;
    /**
     * The localization manager.
     */
    protected ILocalizationManager localizationManager;
    /**
     * The s ql condition.
     */
    protected String sQLCondition;
    /**
     * The parameters map.
     */
    protected Map<String, LocalDate> parametersMap = new HashMap<>();
    /**
     * The validity messages.
     */
    protected List<String> validityMessages = new LinkedList<>();

    /**
     * Instantiates a new abstract dates form param.
     */
    public AbstractDatesFormParam() {
        super();
    }

    /**
     * Instantiates a new abstract dates form param.
     *
     * @param localizationManager the localization manager
     */
    public AbstractDatesFormParam(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    /**
     * Builds the parameter map and sql condition.
     *
     * @param hQLAliasDate the h ql alias date
     * @throws DateTimeException the parse exception
     */
    protected abstract void buildParameterMapAndSQLCondition(String hQLAliasDate)
            throws DateTimeException;

    /**
     * Builds the period.
     *
     * @param currentYear  the current year
     * @param periodIndex  the period index
     * @param periodStart  the period start
     * @param periodEnd    the period end
     * @param hQLAliasDate the h ql alias date
     * @throws DateTimeException the parse exception
     */
    protected void buildPeriod(int currentYear, int periodIndex, String periodStart,
                               String periodEnd, String hQLAliasDate) throws DateTimeException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0
                && periodEnd.trim().length() > 0) {
            final String startParameterName = processDate(AbstractDatesFormParam.START_PREFIX,
                    currentYear, periodIndex, periodStart);
            final String endParameterName = processDate(AbstractDatesFormParam.END_PREFIX,
                    currentYear, periodIndex, periodEnd);
            sQLCondition = sQLCondition.concat(String.format(AbstractDatesFormParam.SQL_CONDITION,
                    hQLAliasDate, startParameterName, endParameterName));
        }
    }

    /**
     * Builds the period string.
     *
     * @param currentYear the current year
     * @param periodStart the period start
     * @param periodEnd   the period end
     * @return the string
     * @throws DateTimeException the parse exception
     */
    protected String buildPeriodString(int currentYear, String periodStart, String periodEnd)
            throws DateTimeException {
        return String.format(
                "%s %s/%d %s %s/%d",
                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                        AbstractDatesFormParam.MSG_FROM),
                periodStart,
                currentYear,
                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                        AbstractDatesFormParam.MSG_TO), periodEnd, currentYear);
    }

    /**
     * Builds the sql condition.
     *
     * @param hQLAliasDate the h ql alias date
     * @return the string
     * @throws DateTimeException the parse exception
     */
    public String buildSQLCondition(String hQLAliasDate) throws DateTimeException {
        retrieveParametersMap(hQLAliasDate);
        return sQLCondition;
    }

    /**
     * Builds the summary.
     *
     * @param printStream the print stream
     * @throws DateTimeException the parse exception
     */
    public abstract void buildSummary(PrintStream printStream) throws DateTimeException;

    /**
     * Custom validate.
     */
    public abstract void customValidate();

    /**
     * Gets the checks if is valid.
     *
     * @return the checks if is valid
     */
    public Boolean getIsValid() {
        validityMessages = new LinkedList<>();
        customValidate();
        testValidityPeriods();
        return validityMessages.isEmpty();
    }

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Gets the pattern date.
     *
     * @return the pattern date
     */
    public abstract String getPatternDate();

    /**
     * Gets the periods.
     *
     * @return the periods
     */
    public List<Map<String, String>> getPeriods() {
        return periods;
    }

    /**
     * Sets the periods.
     *
     * @param periods the periods
     */
    public void setPeriods(List<Map<String, String>> periods) {
        this.periods = periods;
    }

    /**
     * Gets the summary html.
     *
     * @return the summary html
     * @throws DateTimeException the parse exception
     */
    public abstract String getSummaryHTML() throws DateTimeException;

    /**
     * Gets the validity messages.
     *
     * @return the validity messages
     */
    public List<String> getValidityMessages() {
        return validityMessages;
    }

    /**
     * Checks if is empty.
     *
     * @return true, if is empty
     */
    public abstract boolean isEmpty();

    private LocalDate parseEndDate(Integer index, final String dateFormat,
                                   final Map<String, String> periodsMap, LocalDate endDate) {
        try {
            if (!periodsMap.get(AbstractDatesFormParam.END_INDEX).matches(
                    getPatternDate().replaceAll("[^/]", "."))) {
                throw new DateTimeException("");
            }
            endDate = DateUtil.readLocalDateFromText(dateFormat, periodsMap.get(AbstractDatesFormParam.END_INDEX));
        } catch (final DateTimeException e) {
            validityMessages.add(String.format(
                    getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                            AbstractDatesFormParam.MSG_END_DATE_INVALID), index + 1,
                    getPatternDate(), periodsMap.get(AbstractDatesFormParam.END_INDEX)));
            UncatchedExceptionLogger.log("bads dates", e);
        }
        return endDate;
    }

    /**
     * Prints the validity messages.
     *
     * @param printStream the print stream
     */
    protected void printValidityMessages(PrintStream printStream) {
        printStream.println("<ul>");
        validityMessages.stream().forEach((validityMessage) -> {
            printStream.println(String.format("<li>%s</li>", validityMessage));
        });
        printStream.println("</ul>");
    }

    /**
     * Process date.
     *
     * @param prefix      the prefix
     * @param currentYear the current year
     * @param periodIndex the period index
     * @param date        the date
     * @return the string
     * @throws DateTimeException the parse exception
     */
    protected String processDate(String prefix, int currentYear, int periodIndex, String date)
            throws DateTimeException {
        final String parameterName = String.format("%s%d%d", prefix, periodIndex, currentYear);
        LocalDate formattedDate = DateUtil.readLocalDateFromText(AbstractDatesFormParam.DATE_FORMAT, String.format("%s/%d", date, currentYear));

        parametersMap.put(parameterName, formattedDate);
        return parameterName;
    }

    /**
     * Retrieve parameters map.
     *
     * @return the map
     * @throws DateTimeException the parse exception
     */
    public Map<String, LocalDate> retrieveParametersMap() throws DateTimeException {
        retrieveParametersMap("");
        return parametersMap;
    }

    /**
     * Retrieve parameters map.
     *
     * @param hQLAliasDate the h ql alias date
     * @return the map
     * @throws DateTimeException the parse exception
     */
    protected Map<String, LocalDate> retrieveParametersMap(String hQLAliasDate) throws DateTimeException {
        sQLCondition = AbstractDatesFormParam.INITIAL_SQL_CONDITION;
        buildParameterMapAndSQLCondition(hQLAliasDate);
        if (sQLCondition.lastIndexOf(AbstractDatesFormParam.OR_CONDITION) != -1) {
            sQLCondition = String.format(
                    "%s%s",
                    sQLCondition.substring(0,
                            sQLCondition.lastIndexOf(AbstractDatesFormParam.OR_CONDITION)), ")");
        } else {
            sQLCondition = String.format("%s%s", sQLCondition, ")");
        }
        return parametersMap;
    }

    /**
     * Retrieve parameters map and build sql condition.
     *
     * @param hQLAliasDate the h ql alias date
     * @return the map
     * @throws DateTimeException the parse exception
     */
    public Map<String, LocalDate> retrieveParametersMapAndBuildSQLCondition(String hQLAliasDate)
            throws DateTimeException {
        return retrieveParametersMap(hQLAliasDate);
    }

    /**
     * Test period empty.
     *
     * @param period the period
     * @return true, if successful
     */
    protected boolean testPeriodEmpty(String period) {
        return period == null || period.trim().length() == 0;
    }

    /**
     * Test validity periods.
     */
    protected void testValidityPeriods() {
        Integer index = 0;
        for (final Map<String, String> periodsMap : periods) {
            LocalDate startDate = null;
            LocalDate endDate = null;
            startDate = testValidityPeriodStep1(index, getPatternDate(), periodsMap, startDate);
            endDate = testValidityPeriodStep2(index, getPatternDate(), periodsMap, endDate);
            testValidityPeriodStep3(index, startDate, endDate);
            index++;
        }
    }

    private LocalDate testValidityPeriodStep1(Integer index, final String dateFormat,
                                              final Map<String, String> periodsMap, LocalDate startDate) {
        if (periodsMap.get(AbstractDatesFormParam.START_INDEX) == null
                || periodsMap.get(AbstractDatesFormParam.START_INDEX).trim().isEmpty()) {
            validityMessages.add(String.format(
                    getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                            AbstractDatesFormParam.MSG_START_DATE_EMPTY), index + 1,
                    getPatternDate()));
        } else {
            try {
                if (!periodsMap.get(AbstractDatesFormParam.START_INDEX).matches(
                        getPatternDate().replaceAll("[^/]", "."))) {
                    throw new DateTimeException("");
                }
                startDate = DateUtil.readLocalDateFromText(dateFormat, periodsMap.get(AbstractDatesFormParam.START_INDEX));
            } catch (final DateTimeException e) {
                validityMessages.add(String.format(
                        getLocalizationManager().getMessage(
                                AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                                AbstractDatesFormParam.MSG_START_DATE_INVALID), index + 1,
                        getPatternDate(), periodsMap.get(AbstractDatesFormParam.START_INDEX)));
                UncatchedExceptionLogger.log("bads dates", e);
            }
        }
        return startDate;
    }

    private LocalDate testValidityPeriodStep2(Integer index, final String dateFormat,
                                              final Map<String, String> periodsMap, LocalDate endDate) {
        if (periodsMap.get(AbstractDatesFormParam.END_INDEX) == null
                || periodsMap.get(AbstractDatesFormParam.END_INDEX).trim().isEmpty()) {
            validityMessages
                    .add(String.format(
                            getLocalizationManager().getMessage(
                                    AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                                    AbstractDatesFormParam.MSG_END_DATE_EMPTY), index + 1,
                            getPatternDate()));
        } else {
            endDate = parseEndDate(index, dateFormat, periodsMap, endDate);
        }
        return endDate;
    }

    private void testValidityPeriodStep3(Integer index, LocalDate startDate, LocalDate endDate) {
        if (startDate != null && endDate != null && startDate.compareTo(endDate) > 0) {
            validityMessages.add(String.format(
                    getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                            AbstractDatesFormParam.MSG_BAD_PERIODE), index + 1));
        }
    }
}
