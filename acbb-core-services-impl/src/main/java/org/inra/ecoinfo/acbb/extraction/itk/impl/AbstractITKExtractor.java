package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractor;
import org.inra.ecoinfo.acbb.extraction.itk.IInterventionExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.util.*;

/**
 * @param <T>
 * @author ptcherniati
 */
public abstract class AbstractITKExtractor<T> extends AbstractExtractor implements IITKExtractor {

    private static final String CST_EXTRACT = "EXTRACT_";
    private IInterventionExtractionDAO<T> interventionExtractionDAO;
    private String resultCode = org.apache.commons.lang.StringUtils.EMPTY;
    private String extractCode = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * @param parameters
     * @throws BusinessException
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void extract(final IParameter parameters) throws BusinessException {
        try {
            this.prepareRequestMetadatas(parameters.getParameters());
            final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
            //final Map<String, List> filteredResultsDatasMap = this.filterExtractedDatas(resultsDatasMap);
            if (((ITKParameterVO) parameters).getResults().get(this.getResultCode()) == null
                    || ((ITKParameterVO) parameters).getResults().get(this.getResultCode())
                    .isEmpty()) {
                ((DefaultParameter) parameters).getResults().put(this.getResultCode(),
                        resultsDatasMap);
            } else {
                ((ITKParameterVO) parameters)
                        .getResults()
                        .get(this.getResultCode())
                        .put(this.getExtractCode(),
                                resultsDatasMap.get(this.getExtractCode()));
            }
            ((ITKParameterVO) parameters)
                    .getResults()
                    .get(this.getResultCode())
                    .put(ITKExtractor.MAP_INDEX_0,
                            resultsDatasMap.get(this.getExtractCode()));
        } catch (NoExtractionResultException ex) {
            throw new BusinessException(ex);
        }
    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO.intervalsDate();
            final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                    .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
            final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
            if (selectedTraitementProgrammes.isEmpty() || selectedVariables.isEmpty()) {
                return 0L;
            }
            return interventionExtractionDAO.getSize(selectedTraitementProgrammes, selectedVariables, intervalsDates, null);
        } catch (DateTimeException ex) {
            return -1l;
        }
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws org.inra.ecoinfo.extraction.exception.NoExtractionResultException
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws NoExtractionResultException, BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final DatesFormParamVO datesYearsContinuousFormParamVO = (DatesFormParamVO) requestMetadatasMap
                    .get(DatesFormParamVO.class.getSimpleName());
            final List<TraitementProgramme> selectedTraitementProgrammes = (List<TraitementProgramme>) requestMetadatasMap
                    .getOrDefault(TraitementProgramme.class.getSimpleName(), new ArrayList());
            final List<Variable> selectedVariables = (List<Variable>) requestMetadatasMap
                    .getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(this.getExtractCode()), new ArrayList());
            final List<IntervalDate> intervalsDates = datesYearsContinuousFormParamVO
                    .intervalsDate();
            final List<T> mesures = this.interventionExtractionDAO.extractITKMesures(selectedTraitementProgrammes, selectedVariables, intervalsDates, policyManager.getCurrentUser());
            if (mesures == null || mesures.isEmpty()) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
            extractedDatasMap.put(this.getExtractCode(), mesures);
        } catch (final NoExtractionResultException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    /**
     * @return
     */
    @Override
    public String getExtractCode() {
        return this.extractCode;
    }

    /**
     * @param extractCode the extractCode to set
     */
    public void setExtractCode(String extractCode) {
        this.extractCode = extractCode;
        this.resultCode = new StringBuffer(AbstractITKExtractor.CST_EXTRACT).append(extractCode)
                .toString();
    }

    /**
     * @return
     */
    protected String getResultCode() {
        return this.resultCode;
    }

    /**
     * @param requestMetadatasMap
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     * (java.util.Map)
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable
     *                                     acbbdao
     */
    public void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
    }

    /**
     * @param interventionExtractionDAO the interventionExtractionDAO to set
     */
    public void setInterventionExtractionDAO(IInterventionExtractionDAO<T> interventionExtractionDAO) {
        this.interventionExtractionDAO = interventionExtractionDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
    }

    /**
     * Sort selected variables.
     *
     * @param requestMetadatasMap
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort(
                (List<Variable>) requestMetadatasMap.getOrDefault(Variable.class.getSimpleName().toLowerCase().concat(
                        this.getExtractCode()), new ArrayList()), new Comparator<Variable>() {

                    @Override
                    public int compare(final Variable o1, final Variable o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });
    }
}
