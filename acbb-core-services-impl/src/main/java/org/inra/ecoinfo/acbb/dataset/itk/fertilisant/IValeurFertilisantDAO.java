/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant;

/**
 * The Interface IValeurTravailDuSolDAO.
 */
public interface IValeurFertilisantDAO extends IDAO<ValeurFertilisant> {
}
