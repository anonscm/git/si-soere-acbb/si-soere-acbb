/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;

/**
 * The Interface IMesureSemisDAO.
 */
public interface IMesureSemisDAO extends IDAO<MesureSemis> {
}
