/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProduitPhytosanitaire> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.messages";

    /**
     * The Constant PROPERTY_MSG_PHYT_TYPE @link(String).
     */
    static final String PROPERTY_MSG_PHYT_TYPE = "PROPERTY_MSG_PHYT_TYPE";

    /**
     * The Constant PROPERTY_MSG_PHYT_NAMM @link(String).
     */
    static final String PROPERTY_MSG_PHYT_NAMM = "PROPERTY_MSG_PHYT_NAMM";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The produit phytosanitaire dao.
     */
    IProduitPhytosanitaireDAO produitPhytosanitaireDAO;

    /**
     * The liste itineraire dao.
     */
    IListeItineraireDAO listeItineraireDAO;

    /**
     * The availables phyt types @link(String[]).
     */
    String[] availablesPhytTypes;

    /**
     * The availables phyt namms @link(String[]).
     */
    String[] availablesPhytNamms;

    /**
     * Instantiates a new recorder.
     */
    public Recorder() {
        super();
    }

    /**
     * Creates the or update produit phytosanitaire.
     *
     * @param code
     * @param nom
     * @param dbType
     * @param dbNamm
     * @param finalDbProduitPhytosanitaire
     * @throws PersistenceException the persistence exception
     * @link(String) the code
     * @link(String) the nom
     * @link(PhytType) the db type
     * @link(PhytNamm) the db namm
     * @link(ProduitPhytosanitaire) the db produit phytosanitaire
     * @link(String) the code
     * @link(String) the nom
     * @link(PhytType) the db type
     * @link(PhytNamm) the db namm
     * @link(ProduitPhytosanitaire) the db produit phytosanitaire
     */
    void createOrUpdateProduitPhytosanitaire(final String code, final String nom,
                                             final PhytType dbType, final PhytNamm dbNamm,
                                             final ProduitPhytosanitaire dbProduitPhytosanitaire) throws PersistenceException {
        ProduitPhytosanitaire finalDbProduitPhytosanitaire = dbProduitPhytosanitaire;
        if (finalDbProduitPhytosanitaire == null) {
            finalDbProduitPhytosanitaire = new ProduitPhytosanitaire(nom, code, dbType, dbNamm);
        } else {
            finalDbProduitPhytosanitaire.setType(dbType);
            finalDbProduitPhytosanitaire.setNamm(dbNamm);
            finalDbProduitPhytosanitaire.setNom(nom);
            finalDbProduitPhytosanitaire.setCode(code);
        }
        try {
            this.produitPhytosanitaireDAO.saveOrUpdate(finalDbProduitPhytosanitaire);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.produitPhytosanitaireDAO.remove(this.produitPhytosanitaireDAO.getByCode(code)
                        .orElseThrow(() -> new BusinessException("bad produit")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ProduitPhytosanitaire> getAllElements() {
        List<ProduitPhytosanitaire> all = this.produitPhytosanitaireDAO
                .getAll(ProduitPhytosanitaire.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the availables phyt namms.
     *
     * @return the availables phyt namms
     */
    String[] getAvailablesPhytNamms() {
        final List<PhytNamm> namms = listeItineraireDAO.getAllForClass(PhytNamm.class);
        final List<String> nammsNames = new LinkedList();
        for (final PhytNamm namm : namms) {
            nammsNames.add(namm.getValeur());
        }
        return nammsNames.toArray(new String[]{});
    }

    /**
     * Gets the availables phyt types.
     *
     * @return the availables phyt types
     */
    String[] getAvailablesPhytTypes() {
        final List<PhytType> types = this.listeItineraireDAO.getAllForClass(PhytType.class);
        final List<String> nammsTypes = new LinkedList();
        for (final PhytType type : types) {
            nammsTypes.add(type.getValeur());
        }
        return nammsTypes.toArray(new String[]{});
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param produitPhytosanitaire
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(ProduitPhytosanitaire) the produit phytosanitaire
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final ProduitPhytosanitaire produitPhytosanitaire) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        produitPhytosanitaire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : produitPhytosanitaire.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        produitPhytosanitaire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : produitPhytosanitaire.getType().getValeur(),
                        this.availablesPhytTypes, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        produitPhytosanitaire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : produitPhytosanitaire.getNamm().getValeur(),
                        this.availablesPhytNamms, null, false, false, true));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<ProduitPhytosanitaire> initModelGridMetadata() {
        this.availablesPhytTypes = this.getAvailablesPhytTypes();
        this.availablesPhytNamms = this.getAvailablesPhytNamms();
        return super.initModelGridMetadata();
    }

    /**
     * Persist produit phytosanitaire.
     *
     * @param errorsReport
     * @param code
     * @param nom
     * @param type
     * @param namm
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code
     * @link(String) the nom
     * @link(String) the type
     * @link(String) the namm
     * @link(ErrorsReport) the errors report
     * @link(String) the code
     * @link(String) the nom
     * @link(String) the type
     * @link(String) the namm
     */
    void persistProduitPhytosanitaire(final ErrorsReport errorsReport, final String code,
                                      final String nom, final String type, final String namm) throws PersistenceException {
        final ProduitPhytosanitaire dbProduitPhytosanitaire = this.produitPhytosanitaireDAO
                .getByCode(code).orElse(null);
        ListeItineraire dbType = null;
        try {
            dbType = this.listeItineraireDAO.getByNKey(PhytType.PHYTOSANITAIRE_TYPE, type)
                    .filter(t -> t instanceof PhytType)
                    .orElseThrow(PersistenceException::new);
        } catch (final Exception e) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PHYT_TYPE), type));
        }
        ListeItineraire dbNamm = null;
        try {
            dbNamm = this.listeItineraireDAO.getByNKey(PhytNamm.NAMM, namm)
                    .filter(t -> t instanceof PhytNamm)
                    .orElseThrow(PersistenceException::new);
        } catch (final Exception e) {
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(
                    Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PHYT_NAMM), namm));
        }
        if (!errorsReport.hasErrors()) {
            this.createOrUpdateProduitPhytosanitaire(code, nom, (PhytType) dbType,
                    (PhytNamm) dbNamm, dbProduitPhytosanitaire);
        }
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        ProduitPhytosanitaire.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String type = tokenizerValues.nextToken();
                final String namm = tokenizerValues.nextToken();
                this.persistProduitPhytosanitaire(errorsReport, code, nom, type, namm);
            }
            if (errorsReport.hasErrors()) {

                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the liste itineraire dao.
     *
     * @param listeItineraireDAO the new liste itineraire dao
     */
    public final void setListeItineraireDAO(final IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    /**
     * Sets the produit phytosanitaire dao.
     *
     * @param produitPhytosanitaireDAO the new produit phytosanitaire dao
     */
    public final void setProduitPhytosanitaireDAO(
            final IProduitPhytosanitaireDAO produitPhytosanitaireDAO) {
        this.produitPhytosanitaireDAO = produitPhytosanitaireDAO;
    }

}
