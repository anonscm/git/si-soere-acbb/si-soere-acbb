/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.agroecosysteme;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<AgroEcosysteme> {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The agro ecosysteme dao.
     */
    IAgroEcosystemeDAO agroEcosystemeDAO;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * Creates the or update site.
     *
     * @param nomAgroEcosysteme
     * @param region
     * @param departement
     * @param dbAgroEcosysteme
     * @throws PersistenceException the persistence exception
     * @link(String) the nom agro ecosysteme
     * @link(String) the region
     * @link(String) the departement
     * @link(AgroEcosysteme) the db agro ecosysteme
     * @link(String) the nom agro ecosysteme
     * @link(String) the region
     * @link(String) the departement
     * @link(AgroEcosysteme) the db agro ecosysteme
     */
    void createOrUpdateSite(final String nomAgroEcosysteme, final String region,
                            final String departement, final AgroEcosysteme dbAgroEcosysteme)
            throws PersistenceException {
        if (dbAgroEcosysteme == null) {
            final AgroEcosysteme agroEcosysteme = new AgroEcosysteme(nomAgroEcosysteme, region,
                    departement);
            this.agroEcosystemeDAO.saveOrUpdate(agroEcosysteme);
        } else {
            dbAgroEcosysteme.setRegion(region);
            dbAgroEcosysteme.setDepartement(departement);
            this.agroEcosystemeDAO.saveOrUpdate(dbAgroEcosysteme);
        }

    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                mgaServiceBuilder.getRecorder().remove(this.agroEcosystemeDAO.getByCode(Utils
                        .createCodeFromString(tokenizerValues.nextToken()))
                        .orElseThrow(() -> new BusinessException("can't find agroecosystem")));
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<AgroEcosysteme> getAllElements() {
        List<AgroEcosysteme> agroecosystemes = this.agroEcosystemeDAO.getAll(AgroEcosysteme.class);
        Collections.sort(agroecosystemes);
        return agroecosystemes;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param agroEcosysteme
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(AgroEcosysteme) the agro ecosysteme
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final AgroEcosysteme agroEcosysteme)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        agroEcosysteme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : agroEcosysteme.getName(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        agroEcosysteme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomFR.getProperty(agroEcosysteme.getName(), agroEcosysteme.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        agroEcosysteme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.getProperty(agroEcosysteme.getName(), agroEcosysteme.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        agroEcosysteme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : agroEcosysteme.getRegion(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        agroEcosysteme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : agroEcosysteme.getDepartement(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<AgroEcosysteme> initModelGridMetadata() {

        this.propertiesNomFR = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRENCH);

        this.propertiesNomEN = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist agro ecosysteme.
     *
     * @param errorsReport
     * @param nomAgroEcosysteme
     * @param region
     * @param departement
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the nom agro ecosysteme
     * @link(String) the region
     * @link(String) the departement
     * @link(ErrorsReport) the errors report
     * @link(String) the nom agro ecosysteme
     * @link(String) the region
     * @link(String) the departement
     */
    void persistAgroEcosysteme(final ErrorsReport errorsReport, final String nomAgroEcosysteme,
                               final String region, final String departement) throws PersistenceException {
        final AgroEcosysteme dbAgroEcosysteme = this.agroEcosystemeDAO.getByCode(Utils
                .createCodeFromString(nomAgroEcosysteme))
                .orElse(null);
        this.createOrUpdateSite(nomAgroEcosysteme, region, departement, dbAgroEcosysteme);
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(AgroEcosysteme.class));
                // On parcourt chaque colonne d'une ligne
                final String nomAgroEcosysteme = tokenizerValues.nextToken();
                final String region = tokenizerValues.nextToken();
                final String departement = tokenizerValues.nextToken();
                this.persistAgroEcosysteme(errorsReport, nomAgroEcosysteme, region, departement);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the agro ecosysteme dao.
     *
     * @param agroEcosystemeDAO the new agro ecosysteme dao
     */
    public final void setAgroEcosystemeDAO(final IAgroEcosystemeDAO agroEcosystemeDAO) {
        this.agroEcosystemeDAO = agroEcosystemeDAO;
    }

}
