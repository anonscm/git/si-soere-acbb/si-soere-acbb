package org.inra.ecoinfo.acbb.extraction.impl;


import org.inra.ecoinfo.refdata.variable.Variable;

import java.util.Comparator;

/**
 * @author ptchernia
 */
public class ComparatorVariable implements Comparator<Variable> {

    /**
     * @param arg0
     * @param arg1
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final Variable arg0, final Variable arg1) {
        if (arg0.getId().equals(arg1.getId())) {
            return 0;
        }
        return arg0.getId() > arg1.getId() ? 1 : 0;
    }
}