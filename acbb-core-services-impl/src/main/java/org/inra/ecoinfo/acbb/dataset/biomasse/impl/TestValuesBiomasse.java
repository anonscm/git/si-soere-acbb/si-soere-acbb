/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.util.Map;

/**
 * @author ptcherniati
 */
public class TestValuesBiomasse extends GenericTestValues {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Check other type value.
     *
     * @param values
     * @param badsFormatsReport
     * @param lineNumber            long the line number
     * @param index
     * @param value
     * @param column
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the org.inra.ecoinfo.acbb.dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#checkOtherTypeValue(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, long, int, java.lang.String,
     * org.inra.ecoinfo.dataset.Column, java.util.Map)
     */
    @Override
    protected void checkOtherTypeValue(final String[] values,
                                       final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                       final String value, final Column column,
                                       final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                       final DatasetDescriptorACBB datasetDescriptor,
                                       final IRequestPropertiesACBB requestPropertiesACBB) {
        if (column.isFlag()
                && RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE.equals(column.getFlagType())) {
            Integer qualityClass;
            try {
                qualityClass = Strings.isNullOrEmpty(value) ? null : Integer.parseInt(value);
                if (qualityClass != null && qualityClass != 0 && qualityClass != 1
                        && qualityClass != 2) {
                    badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_QUALITY_CLASS),
                            lineNumber, index + 1)));
                }

            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_QUALITY_CLASS),
                        lineNumber, index + 1)));
            }
        }

    }
}
