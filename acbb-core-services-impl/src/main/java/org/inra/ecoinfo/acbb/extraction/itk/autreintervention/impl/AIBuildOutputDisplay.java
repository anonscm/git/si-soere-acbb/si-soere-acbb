/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.autreintervention.impl;

import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.ValeurAI;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class AIBuildOutputDisplay extends AbstractITKOutputBuilder<AI, ValeurAI, AI> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.autreintervention.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(AIBuildOutputDisplay.class
            .getName());

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public AIBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return AIBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }
}
