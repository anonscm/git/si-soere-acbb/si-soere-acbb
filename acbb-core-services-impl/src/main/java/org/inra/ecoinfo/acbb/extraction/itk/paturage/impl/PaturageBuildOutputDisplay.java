/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.paturage.impl;

import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class PaturageBuildOutputDisplay extends
        AbstractITKOutputBuilder<Paturage, ValeurPaturage, Paturage> {

    static final String PATTERN_10_FIELD = "%s;%s;%s;%s;%s;%s;%s(%s);%s;%s;%s;%s;%s";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.paturage.messages";

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public PaturageBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return PaturageBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }

    /**
     * @param datesYearsContinuousFormParamVO
     * @param selectedWsolVariables
     * @param availablesCouverts
     * @param rawDataPrintStream
     * @return
     */
    @Override
    protected OutputHelper getOutPutHelper(
            final DatesFormParamVO datesYearsContinuousFormParamVO,
            final List<Variable> selectedWsolVariables,
            final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts,
            final PrintStream rawDataPrintStream) {
        return new PaturageOutputHelper(rawDataPrintStream,
                selectedWsolVariables);
    }

    class PaturageOutputHelper extends OutputHelper {

        PaturageOutputHelper(PrintStream rawDataPrintStream, List<Variable> selectedWSolVariables) {
            super(rawDataPrintStream, selectedWSolVariables);
        }

        /**
         * @see org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder.OutputHelper#printLineGeneric(org.inra.ecoinfo.acbb.dataset.itk.impl.IMesureITK)
         */
        @Override
        protected void printLineGeneric(Paturage mesure) {
            final SuiviParcelle suiviParcelle = mesure.getSequence().getSuiviParcelle();
            final Parcelle parcelle = suiviParcelle.getParcelle();
            LocalDate dateFin = mesure.getDateDeFin();
            TraitementProgramme treatment = suiviParcelle.getTraitement();
            String line;
            final String blocName = parcelle.getBloc() != null ? parcelle.getBloc().getBlocName()
                    : StringUtils.EMPTY;
            final String repetitionName = parcelle.getBloc() != null ? parcelle.getBloc()
                    .getRepetitionName() : StringUtils.EMPTY;
            line = String.format(PaturageBuildOutputDisplay.PATTERN_10_FIELD,
                    this.propertiesSiteName.getProperty(parcelle.getSite().getName(), parcelle.getSite().getName()),
                    this.propertiesParcelleName.getProperty(parcelle.getName(), parcelle.getName()),
                    blocName,
                    repetitionName,
                    DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDate(), DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(dateFin, DateUtil.DD_MM_YYYY),
                    this.propertiesTreatmentName.getProperty(treatment.getNom(), treatment.getNom()),
                    this.propertiesTreatmentAffichage.getProperty(treatment.getAffichage(), treatment.getAffichage()),
                    DateUtil.getUTCDateTextFromLocalDateTime(suiviParcelle.getDateDebutTraitement(), DateUtil.DD_MM_YYYY),
                    mesure.getSequence().getObservation(),
                    mesure.getSequence().getVersionTraitementRealiseeNumber(),
                    mesure.getSequence().getAnneeRealiseeNumber(),
                    mesure.getSequence()
                            .getPeriodeRealiseeNumber());
            this.rawDataPrintStream.print(line);
        }
    }
}
