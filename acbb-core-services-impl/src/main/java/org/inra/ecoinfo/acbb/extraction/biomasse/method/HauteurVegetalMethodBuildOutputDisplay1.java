/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class HauteurVegetalMethodBuildOutputDisplay1
        extends
        AbstractMethodBuildOutputDisplay<org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.Recorder> {

    static final String CST_METHOD_NAME = "HauteurVegetalMethode";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return HauteurVegetalMethodBuildOutputDisplay1.CST_METHOD_NAME;
    }
}
