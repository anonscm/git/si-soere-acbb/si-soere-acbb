/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IMethodeLAIDAO.
 */
public interface IMethodeLAIDAO extends IMethodeDAO<MethodeLAI> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeLAI> getAll();
}
