/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;

/**
 * The Interface IValeurSemisDAO.
 */
public interface IValeurPaturageDAO extends IDAO<ValeurPaturage> {
}
