/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.jpa;


import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee_;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPAAnneeRealiseeDAO.
 */
public class JPAAnneeRealiseeDAO extends AbstractJPADAO<AnneeRealisee> implements IAnneeRealiseeDAO {
    /**
     * Gets the by n key.
     *
     * @param versionTraitementRealise
     * @param annee
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO#getByNKey(org.inra
     * .ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee, int)
     * @link(VersionTraitementRealisee) the version traitement realise
     * @link(int) the annee
     */
    @Override
    public Optional<AnneeRealisee> getByNKey(final VersionTraitementRealisee versionTraitementRealise,
                                             final int annee) {

        CriteriaQuery<AnneeRealisee> query = builder.createQuery(AnneeRealisee.class);
        Root<AnneeRealisee> ar = query.from(AnneeRealisee.class);
        query.where(
                builder.equal(ar.join(AnneeRealisee_.versionTraitementRealise), versionTraitementRealise),
                builder.equal(ar.get(AnneeRealisee_.annee), annee)
        );
        query.select(ar);
        return getOptional(query);
    }
}
