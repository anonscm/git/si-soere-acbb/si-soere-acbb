/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction;

import org.inra.ecoinfo.acbb.refdata.methode.IMethodeDAO;

import java.util.List;

/**
 * The Interface IBlocDAO.
 */
public interface IMethodeProductionDAO extends IMethodeDAO<MethodeProduction> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    @Override
    List<MethodeProduction> getAll();
}
