package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author vkoyao
 * <p>
 * The Class FluxMeteoOutputDisplayByRow.
 */
public class BiomasseOutputDisplay extends BiomasseOutputsBuildersResolver {

    /**
     * The flux chambre build output by row @link(IOutputBuilder).
     */
    IOutputBuilder hauteurVegetalBuildOutput;
    IOutputBuilder biomasseProductionBuildOutput;
    IOutputBuilder laiBuildOutput;
    IOutputBuilder biomasseRequestReminder;
    IOutputBuilder productionMethodBuildOutput;
    IOutputBuilder analyseMethodBuildOutput;
    IOutputBuilder laiMethodBuildOutput;
    IOutputBuilder hauteurVegetalMethodBuildOutput;

    // *add autre type de données biomasse*/
    // TODO

    /**
     * Instantiates a new flux meteo output display by row.
     */
    public BiomasseOutputDisplay() {
        super();
    }

    /**
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap,
                                          Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.
     *                           FluxMeteoOutputsBuildersResolver
     *                           #buildOutput(org.inra.ecoinfo.extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        this.biomasseRequestReminder.buildOutput(parameters);
        statusBar.setProgress(56);
        this.biomasseProductionBuildOutput.buildOutput(parameters);
        statusBar.setProgress(63);
        this.laiBuildOutput.buildOutput(parameters);
        statusBar.setProgress(69);
        this.hauteurVegetalBuildOutput.buildOutput(parameters);
        statusBar.setProgress(76);
        this.productionMethodBuildOutput.buildOutput(parameters);
        statusBar.setProgress(82);
        this.analyseMethodBuildOutput.buildOutput(parameters);
        statusBar.setProgress(89);
        this.laiMethodBuildOutput.buildOutput(parameters);
        statusBar.setProgress(95);
        this.hauteurVegetalMethodBuildOutput.buildOutput(parameters);
        statusBar.setProgress(100);
        // TODO
        return super.buildOutput(parameters);
    }

    /**
     * @param metadatasMap
     * @return
     */
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        return null;
    }

    /**
     * @param analyseMethodBuildOutput
     */
    public void setAnalyseMethodBuildOutput(IOutputBuilder analyseMethodBuildOutput) {
        this.analyseMethodBuildOutput = analyseMethodBuildOutput;
    }

    /**
     * @param biomasseProductionuildOutput
     */
    public void setBiomasseProductionuildOutput(IOutputBuilder biomasseProductionuildOutput) {
        this.biomasseProductionBuildOutput = biomasseProductionuildOutput;
    }

    /**
     * @param requestReminderBuildOutput the requestReminderBuildOutput to set
     */
    /**
     * public void setRequestReminderBuildOutput(IOutputBuilder
     * requestReminderBuildOutput) { this.requestReminderBuildOutput =
     * requestReminderBuildOutput; }
     */
    /**
     * @param biomasseRequestReminder
     */
    public void setBiomasseRequestReminder(IOutputBuilder biomasseRequestReminder) {
        this.biomasseRequestReminder = biomasseRequestReminder;
    }

    /**
     * @param hauteurVegetalBuildOutput the hauteurVegetalBuildOutput to set
     */
    public void setHauteurVegetalBuildOutput(IOutputBuilder hauteurVegetalBuildOutput) {
        this.hauteurVegetalBuildOutput = hauteurVegetalBuildOutput;
    }

    /**
     * @param hauteurVegetalMethodBuildOutput
     */
    public void setHauteurVegetalMethodBuildOutput(IOutputBuilder hauteurVegetalMethodBuildOutput) {
        this.hauteurVegetalMethodBuildOutput = hauteurVegetalMethodBuildOutput;
    }

    /**
     * @param laiBuildOutput
     */
    public void setLaiBuildOutput(IOutputBuilder laiBuildOutput) {
        this.laiBuildOutput = laiBuildOutput;
    }

    /**
     * @param laiMethodBuildOutput
     */
    public void setLaiMethodBuildOutput(IOutputBuilder laiMethodBuildOutput) {
        this.laiMethodBuildOutput = laiMethodBuildOutput;
    }

    /**
     * @param productionMethodBuildOutput
     */
    public void setProductionMethodBuildOutput(IOutputBuilder productionMethodBuildOutput) {
        this.productionMethodBuildOutput = productionMethodBuildOutput;
    }

}
