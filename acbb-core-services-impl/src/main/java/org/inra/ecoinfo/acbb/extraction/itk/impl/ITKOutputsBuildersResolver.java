/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipOutputStream;

/**
 * The Class MPSOutputsBuildersResolver.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class ITKOutputsBuildersResolver extends AbstractOutputBuilder implements
        IOutputsBuildersResolver {

    /**
     * The Constant PATTERN_HEADER @link(String).
     */
    static final String PATTERN_HEADER = "%s;%s;%s(%s)";
    /**
     * The Constant EXTRACTION @link(String).
     */
    static final String EXTRACTION = "extraction";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant SEPARATOR_TEXT.
     */
    static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant EXTENSION_ZIP.
     */
    static final String EXTENSION_ZIP = ".zip";
    /**
     * The Constant FILENAME_REMINDER.
     */
    static final String FILENAME_REMINDER = "recapitulatif_extraction";
    /**
     * The Constant EXTENSION_CSV.
     */
    static final String EXTENSION_CSV = ".csv";
    /**
     * The Constant EXTENSION_TXT.
     */
    static final String EXTENSION_TXT = ".txt";
    /**
     * The Constant MAP_INDEX_0.
     */
    static final String MAP_INDEX_0 = "0";
    /**
     * The Constant REQUEST_REMINDER @link(String).
     */
    @SuppressWarnings("unused")
    static final String REQUEST_REMINDER = "RequestReminder";
    /**
     * The LOGGER.
     */
    static final Logger LOGGER = LoggerFactory.getLogger(ITKOutputsBuildersResolver.class);
    private static final int KO = 1_024;

    /**
     *
     */
    public ITKOutputsBuildersResolver() {
        super();
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     */
    protected void buidOutputFile(final IParameter parameters) throws BusinessException {
        final Set<String> datatypeNames = new HashSet();
        // datatypeNames.add(getPrefixFileName());
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                ITKOutputsBuildersResolver.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     *                           .extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        RObuildZipOutputStream robuildZipOutputStream = null;
        try {
            robuildZipOutputStream = super.buildOutput(parameters);
            parameters.getParameters().put(ITKExtractor.CST_RESULTS, parameters.getResults());
            NoExtractionResultException noDataToExtract;
            try (ZipOutputStream zipOutputStream = robuildZipOutputStream.getZipOutputStream()) {
                noDataToExtract = null;
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    try {
                        AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                    } catch (IOException e) {
                        AbstractOutputBuilder.LOGGER.error("can't embed in zip", e);
                    }
                }
                zipOutputStream.flush();
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("can't build output", e1);
        }
        return robuildZipOutputStream;
    }

    /**
     * The Class BuildOutputHelper.
     */
    protected static class BuildOutputHelper {

        /**
         * The parameter.
         */
        protected ITKParameterVO parameter;
        /**
         * The print stream.
         */
        protected PrintStream printStream;

        /**
         * Instantiates a new builds the output helper.
         *
         * @param parameter   the parameter
         * @param printStream the print stream
         * @throws NoExtractionResultException the no extract data found
         *                                     exception
         */
        BuildOutputHelper(final ITKParameterVO parameter, final PrintStream printStream)
                throws org.inra.ecoinfo.extraction.exception.NoExtractionResultException {
            super();
            this.parameter = parameter;
            this.printStream = printStream;
        }
    }
}
