/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.bloc;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;

import java.util.List;
import java.util.Optional;

/**
 * The Interface IBlocDAO.
 */
public interface IBlocDAO extends IDAO<Bloc> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<Bloc> getAll();

    /**
     * Gets the by n key.
     *
     * @param siteId   the site id
     * @param codeBloc the code bloc
     * @return the by n key
     */
    Optional<Bloc> getByNKey(Long siteId, String codeBloc);

    /**
     * Gets the by site.
     *
     * @param site the site
     * @return the by site
     */
    List<Bloc> getBySite(SiteACBB site);

    /**
     * Gets the names blocs for site.
     *
     * @param site the site
     * @return the names blocs for site
     */
    List<String> getNamesBlocsForSite(SiteACBB site);
}
