/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle_;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme_;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.Entity;
import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ptcherniati
 */
public abstract class AbstractACBBExtractionJPA<V> extends AbstractJPADAO<V> {

    /**
     * @param user
     * @param criteria
     * @param predicatesAnd
     * @param builder
     * @param vns
     * @param dateMesure
     */
    @SuppressWarnings(value = "unchecked")

    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            List<String> groups = getGroups(user);
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            List<Predicate> orLogin = new LinkedList<>();
            predicatesAnd.add(er.get(ExtractActivity_.login).in(groups));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

    public List<String> getGroups(IUser user) {
        List<String> l = new LinkedList();
        if (user instanceof ICompositeGroup) {
            l = user.getAllGroups().stream()
                    .filter(g -> g.getWhichTree().equals(WhichTree.TREEDATASET))
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
        } else {
            l.add(user.getLogin());
        }
        return l;
    }

    abstract protected Class<? extends GenericSynthesisValue> getSynthesisValueClass();

    protected String getSynthesisValueTable() {
        return getSynthesisValueClass().getAnnotation(Entity.class).name().toLowerCase();
    }

    /**
     * Gets the availables agro ecosystemes.
     *
     * @param intervalsDate
     * @param user          the value of user
     * @return the java.util.List<org.inra.ecoinfo.acbb.refdata.site.SiteACBB>
     * @see org.inra.ecoinfo.acbb.extraction.fluxchambres.IFluxChambreDAO#
     * getAvailablesAgroEcosystemes()
     */
    @SuppressWarnings(value = "unchecked")
    public List<SiteACBB> getAvailablesSiteForPeriode(List<IntervalDate> intervalsDate, IUser user) {
        if (intervalsDate == null || intervalsDate.isEmpty()) {
            return null;
        }
        CriteriaQuery<SiteACBB> query = builder.createQuery(SiteACBB.class);
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Root<SiteACBB> site = query.from(SiteACBB.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rns;
        if (isParcelleDatatype()) {
            Join<RealNode, RealNode> rnp = rnt.join(RealNode_.parent);
            rns = rnp.join(RealNode_.parent);
        } else {
            rns = rnt.join(RealNode_.parent);
        }
        ArrayList<Predicate> and = new ArrayList<>();
        Path<LocalDateTime> date = sv.get(GenericSynthesisValue_.date);
        and.add(builder.equal(rns.get(RealNode_.nodeable), site.get(SiteACBB_.id)));
        intervalsDate.stream()
                .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(SynthesisValueWithParcelle_.idNode)));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(site)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    public List<TraitementProgramme> getAvailablesTraitementsForPeriode(List<IntervalDate> intervals, IUser user) {
        if (intervals == null || intervals.isEmpty()) {
            return null;
        }
        List<Long> siteIds = getAvailablesSiteForPeriode(intervals, user).stream()
                .map(s -> s.getId())
                .collect(Collectors.toList());
        if (siteIds.isEmpty()) {
            return new LinkedList<>();
        }
        CriteriaQuery<TraitementProgramme> query = builder.createQuery(TraitementProgramme.class);
        Root<SuiviParcelle> suiviParcelle = query.from(SuiviParcelle.class);
        Join<SuiviParcelle, TraitementProgramme> treatment = suiviParcelle.join(SuiviParcelle_.traitement);
        Join<TraitementProgramme, SiteACBB> site = treatment.join(TraitementProgramme_.site);
        query
                .select(treatment)
                .where(site.get(SiteACBB_.id).in(siteIds));
        return getResultList(query);

    }

    /**
     * Gets the availables variables by sites.
     *
     * @param sites
     * @param intervalsDate
     * @param user          the value of user
     * @return the java.util.List<org.inra.ecoinfo.refdata.variable.Variable>
     * @link(List<SiteACBB>) the sites
     */
    @SuppressWarnings(value = "unchecked")
    public List<NodeDataSet> getAvailablesVariablesBySites(final List<SiteACBB> sites, List<IntervalDate> intervalsDate, IUser user) {
        if (sites == null || sites.isEmpty() || intervalsDate == null
                || intervalsDate.isEmpty()) {
            return null;
        }

        CriteriaQuery<NodeDataSet> query = builder.createQuery(NodeDataSet.class);
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Root<SiteACBB> site = query.from(SiteACBB.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rns;
        if (isParcelleDatatype()) {
            Join<RealNode, RealNode> rnp = rnt.join(RealNode_.parent);
            rns = rnp.join(RealNode_.parent);
        } else {
            rns = rnt.join(RealNode_.parent);
        }
        ArrayList<Predicate> and = new ArrayList<>();
        Path<LocalDateTime> date = sv.get(GenericSynthesisValue_.date);
        and.add(builder.equal(rns.get(RealNode_.nodeable), site.get(SiteACBB_.id)));
        and.add(site.in(sites));
        intervalsDate.stream()
                .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(SynthesisValueWithParcelle_.idNode)));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(ndsv)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    public List<NodeDataSet> getAvailableVariableByTraitement(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervalsDate, IUser user) {
        if (selectedTraitements == null || selectedTraitements.isEmpty() || intervalsDate == null
                || intervalsDate.isEmpty()) {
            return null;
        }

        CriteriaQuery<NodeDataSet> query = builder.createQuery(NodeDataSet.class);
        Root<? extends SynthesisValueWithParcelle> sv = query.from((Class<SynthesisValueWithParcelle>) getSynthesisValueClass());
        Root<SuiviParcelle> suiviParcelle = query.from(SuiviParcelle.class);
        Join<SuiviParcelle, Parcelle> parcelle = suiviParcelle.join(SuiviParcelle_.parcelle);
        Join<SuiviParcelle, TraitementProgramme> traitement = suiviParcelle.join(SuiviParcelle_.traitement);
        Join<TraitementProgramme, SiteACBB> site = traitement.join(TraitementProgramme_.site);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rns;
        if (isParcelleDatatype()) {
            Join<RealNode, RealNode> rnp = rnt.join(RealNode_.parent);
            rns = rnp.join(RealNode_.parent);
        } else {
            rns = rnt.join(RealNode_.parent);
        }
        ArrayList<Predicate> and = new ArrayList<>();
        Path<LocalDateTime> date = sv.get(GenericSynthesisValue_.date);
        and.add(builder.equal(rns.get(RealNode_.nodeable), site.get(SiteACBB_.id)));
        and.add(builder.equal(parcelle.get(Parcelle_.code), sv.get(SynthesisValueWithParcelle_.parcelleCode)));
        and.add(traitement.in(traitement));
        intervalsDate.stream()
                .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(SynthesisValueWithParcelle_.idNode)));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(ndsv)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    protected abstract boolean isParcelleDatatype();
}
