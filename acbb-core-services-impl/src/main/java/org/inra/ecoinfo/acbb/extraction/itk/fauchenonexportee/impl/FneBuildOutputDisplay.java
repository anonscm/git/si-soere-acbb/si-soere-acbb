/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.fauchenonexportee.impl;

import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class FneBuildOutputDisplay extends AbstractITKOutputBuilder<Fne, ValeurFne, Fne> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(FneBuildOutputDisplay.class
            .getName());

    /**
     * Instantiates a new semis build output display by row.
     *
     * @param datasetDescriptorPath
     */
    public FneBuildOutputDisplay(String datasetDescriptorPath) {
        super(datasetDescriptorPath);
    }

    /**
     * @return
     */
    @Override
    protected String getBundleSourcePath() {
        return FneBuildOutputDisplay.BUNDLE_SOURCE_PATH;
    }
}
