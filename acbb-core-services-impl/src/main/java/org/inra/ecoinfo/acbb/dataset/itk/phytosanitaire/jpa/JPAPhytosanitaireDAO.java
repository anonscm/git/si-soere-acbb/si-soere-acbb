/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.jpa.JPAInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.IPhytosanitaireDAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPAPhytosanitaireDAO extends JPAInterventionDAO<Phytosanitaire> implements
        IPhytosanitaireDAO {

    @Override
    protected Class<Phytosanitaire> getInterventionClass() {
        return Phytosanitaire.class;
    }
}
