/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationTravailDuSolDAO.
 */
public class JPAPublicationPhytosanitaireDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * The Constant QUERY_DELETE_SEQUENCE @link(String).
     */
    static final String QUERY_DELETE_SEQUENCE = "delete from Phytosanitaire phyt where phyt.version=:version";
    /**
     * The Constant QUERY_DELETE_MWSOL_MWSOL_OBJECTIFS @link(String).
     */
    static final String QUERY_DELETE_PHYT_PHYT_OBJECTIFS = "delete from "
            + ValeurPhytosanitaire.NAME_ENTITY_VALEUR_LISTE_ITINERAIRE
            + " where "
            + ValeurPhytosanitaire.ID_JPA
            + " in (select "
            + ValeurPhytosanitaire.ID_JPA
            + " from "
            + ValeurPhytosanitaire.NAME_ENTITY_JPA
            + " where "
            + Phytosanitaire.ID_JPA
            + " in (select "
            + Phytosanitaire.ID_JPA
            + " from "
            + Phytosanitaire.NAME_ENTITY_JPA
            + " where "
            + VersionFile.ID_JPA
            + " = :version))";
    /**
     * The Constant QUERY_DELETE_VALEUR @link(String).
     */
    static final String QUERY_DELETE_VALEUR = "delete from valeur_phytosanitaire_vphyt v_phyt where v_phyt.itk_id in (select itk_id from phytosanitaire_phyt phyt where phyt.ivf_id=:version)";
    private static final Logger LOGGER = LoggerFactory.getLogger(JPAPublicationPhytosanitaireDAO.class
            .getName());

    /**
     * Removes the version.
     *
     * @param version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion(org.
     *                              inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        final String requeteDeletePhytPhytObj = JPAPublicationPhytosanitaireDAO.QUERY_DELETE_PHYT_PHYT_OBJECTIFS;
        Query query = this.entityManager.createNativeQuery(requeteDeletePhytPhytObj);
        query.setParameter("version", version.getId());
        query.executeUpdate();
        CriteriaDelete<ValeurPhytosanitaire> deleteQueryValeur = builder.createCriteriaDelete(ValeurPhytosanitaire.class);
        Root<ValeurPhytosanitaire> valeur = deleteQueryValeur.from(ValeurPhytosanitaire.class);
        Subquery<Phytosanitaire> subquery = deleteQueryValeur.subquery(Phytosanitaire.class);
        Root<Phytosanitaire> itk = subquery.from(Phytosanitaire.class);
        subquery.select(itk)
                .where(builder.equal(itk.get(AbstractIntervention_.version), version));
        deleteQueryValeur.where(valeur.get(ValeurPhytosanitaire_.phytosanitaire).in(subquery));
        entityManager.createQuery(deleteQueryValeur).executeUpdate();
        CriteriaDelete<Phytosanitaire> deleteQueryPhytosanitaire = builder.createCriteriaDelete(Phytosanitaire.class);
        itk = deleteQueryPhytosanitaire.from(Phytosanitaire.class);
        deleteQueryPhytosanitaire
                .where(builder.equal(itk.get(AbstractIntervention_.version), version));
        entityManager.createQuery(deleteQueryPhytosanitaire).executeUpdate();
    }
}
