package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IProcessRecord;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.IListeACBBDAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The Class AbstractProcessRecord.
 * <p>
 * <p>
 * <p>
 * generic abstract implementation of {
 *
 * @param <T>
 * @param <I>
 * @see IProcessRecord
 */
public abstract class AbstractProcessRecord<T extends ListeACBB, I extends IListeACBBDAO<T>>
        implements IProcessRecord, Serializable {

    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.acbb.dataset.impl.messages";

    /**
     *
     */
    protected static final String PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE = "PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE";

    /**
     *
     */
    protected static final String BAD_DATASET_DESCRIPTOR = "Bad dataset-descriptor";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;
    /**
     *
     */
    protected Logger logger;
    /**
     *
     */
    protected String datatypeName;
    /**
     * The localization manager.
     */
    @Transient
    protected ILocalizationManager localizationManager;
    /**
     * The notification manager.
     */
    protected INotificationsManager notificationsManager;
    /**
     * The variable dao.
     */
    @Transient
    protected IVariableACBBDAO variableDAO;
    /**
     * The version file dao.
     */
    @Transient
    protected IVersionFileDAO versionFileDAO;
    /**
     * The suivi parcelle dao.
     */
    @Transient
    protected ISuiviParcelleDAO suiviParcelleDAO;
    /**
     * The version de traitement dao.
     */
    @Transient
    protected IVersionDeTraitementDAO versionDeTraitementDAO;
    /**
     * The traitement dao.
     */
    @Transient
    protected ITraitementDAO traitementDAO;
    /**
     * The parcelle dao.
     */
    @Transient
    protected IParcelleDAO parcelleDAO;
    /**
     * The liste itineraire dao @link(ITDAO).
     */
    protected I listeACBBDAO;
    /**
     * The datatype unite variable acbbdao {@link IDatatypeVariableUniteACBBDAO}
     * .
     */
    protected IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    private IPolicyManager policyManager;

    /**
     * Instantiates a new abstract process record.
     */
    protected AbstractProcessRecord() {
        super();
        logger = LoggerFactory.getLogger(this
                .getClass()
                .getName());

    }

    /**
     * @param version
     * @return
     */
    protected static SiteACBB getSite(final VersionFile version) {
        return (SiteACBB) version.getDataset().getRealNode().getNodeByNodeableTypeResource(SiteACBB.class).getNodeable();
    }

    /**
     * @param datatypeName
     */
    public void setDatatypeName(String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     * Builds the variables header and skip header.
     *
     * @param parser
     * @param datasetDescriptor
     * @return the list
     * @throws PersistenceException the persistence exception
     * @throws IOException          Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{DatasetDescriptorACBB the dataset descriptor
     */
    protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(final CSVParser parser,
                                                                                final DatasetDescriptorACBB datasetDescriptor) throws PersistenceException, IOException {
        List<DatatypeVariableUniteACBB> dvus = new LinkedList();
        Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites = this.datatypeVariableUniteACBBDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode(this.datatypeName);
        for (final Column colonne : datasetDescriptor.getColumns()) {
            if (RecorderACBB.PROPERTY_CST_DATE_TYPE.equals(colonne.getFlagType())
                    || RecorderACBB.PROPERTY_CST_TIME_TYPE.equals(colonne.getFlagType())
                    || RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE.equals(colonne.getFlagType())) {

                continue;

            }
            if (colonne.isFlag()
                    && (colonne.getFlagType().equals(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE)
                    || colonne.getFlagType().equals(
                    RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE) || colonne
                    .getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE))) {
                final VariableACBB variable = (VariableACBB) this.variableDAO.getByAffichage(colonne.getName()).orElse(null);
                dvus.add(variable == null ? null : dbDatatypeVariableUnites.get(variable.getCode()));
            }
        }
        for (int i = 0; i < datasetDescriptor.getEnTete(); i++) {

            parser.getLine();

        }
        return dvus;

    }

    /**
     * @param errorsReport
     * @return
     */
    protected Notification buildWarningNotification(ErrorsReport errorsReport) {
        final Notification notification = new Notification();
        notification.setLevel(Notification.WARN);
        notification.setMessage(String.format(RecorderACBB
                .getACBBMessage(RecorderACBB.PROPERTY_MSG_WARNIN_INFO_IN8PUBLISH)));
        String bodyMessage = null;
        bodyMessage = errorsReport.getInfosMessages();
        notification.setBody(bodyMessage);
        return notification;
    }

    /**
     * Gets the date.
     *
     * @param errorsReport
     * @param lineCount
     * @param columnIndex
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @return the date
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{int the column index
     * @link{CleanerValues the cleaner values
     * @link
     * @link{IRequestPropertiesACBB the request properties
     */
    public LocalDate getDate(final ErrorsReport errorsReport, final long lineCount,
                             final int columnIndex, final CleanerValues cleanerValues,
                             final DatasetDescriptorACBB datasetDescriptorACBB) {

        LocalDate date = null;

        String dateString = null;

        try {

            dateString = cleanerValues.nextToken();

            date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateString);

        } catch (final DateTimeException | EndOfCSVLine e) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE),
                    dateString, lineCount, columnIndex + 1,
                    datasetDescriptorACBB.getColumnName(columnIndex),
                    RecorderACBB.PROPERTY_CST_FRENCH_DATE));

        }

        return date;

    }

    /**
     * Gets the date utc.
     *
     * @param errorsReport
     * @param lineCount
     * @param columnIndex
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestProperties
     * @return the date utc
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{int the column index
     * @link{CleanerValues the cleaner values
     * @link{DatasetDescriptorACBB the dataset descriptor acbb
     * @link{IRequestPropertiesACBB the request properties
     */
    public LocalDate getLocalDate(final ErrorsReport errorsReport, final long lineCount,
                                  final int columnIndex, final CleanerValues cleanerValues,
                                  final DatasetDescriptorACBB datasetDescriptorACBB,
                                  final IRequestPropertiesACBB requestProperties) {

        LocalDate date = null;

        String dateString = null;

        try {

            dateString = cleanerValues.nextToken();
            date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateString);

        } catch (final DateTimeException | EndOfCSVLine e) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE),
                    dateString, lineCount, columnIndex + 1,
                    datasetDescriptorACBB.getColumnName(columnIndex),
                    RecorderACBB.PROPERTY_CST_FRENCH_DATE));

            requestProperties.setDateDeDebut(null);

        }

        return date;

    }

    /**
     * <p>
     * If the next value is null or empty return
     * RecorderACBB.CST_INVALID_EMPTY_MEASURE</p>
     * <p>
     * If the next value can't be parsed to a float return localized
     * RecorderACBB.PROPERTY_MSG_INVALID_FLOAT_VALUE message</p>
     *
     * @param errorsReport
     * @param lineCount
     * @param columnIndex
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @return the next value to a float value
     */
    protected float getFloat(final ErrorsReport errorsReport, final long lineCount,
                             final int columnIndex, final CleanerValues cleanerValues,
                             final DatasetDescriptorACBB datasetDescriptorACBB) {

        float real = ACBBUtils.CST_INVALID_EMPTY_MEASURE;

        String floatString = null;
        try {
            floatString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, columnIndex).getMessage());
            return real;
        }

        if (Strings.isNullOrEmpty(floatString)) {

            return real;

        }

        try {

            real = Float.valueOf(floatString);

        } catch (final NumberFormatException e) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_FLOAT_VALUE),
                    lineCount, columnIndex + 1, datasetDescriptorACBB.getColumnName(columnIndex),
                    floatString));
            real = org.inra.ecoinfo.acbb.utils.ACBBUtils.CST_INVALID_BAD_MEASURE;

        }

        return real;

    }

    /**
     * <p>
     * If the next value is null or empty return
     * RecorderACBB.CST_INVALID_EMPTY_MEASURE</p>
     * <p>
     * If the next value can't be parsed to a integer return localized
     * RecorderACBB.PROPERTY_MSG_INVALID_FLOAT_VALUE message</p>
     *
     * @param errorsReport
     * @param lineCount
     * @param columnIndex
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @return the next value to a integer value
     */
    protected int getInt(final ErrorsReport errorsReport, final long lineCount,
                         final int columnIndex, final CleanerValues cleanerValues,
                         final DatasetDescriptorACBB datasetDescriptorACBB) {

        int integer = org.inra.ecoinfo.acbb.utils.ACBBUtils.CST_INVALID_EMPTY_MEASURE;
        String integerString = null;
        try {
            integerString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, columnIndex).getMessage());
            return integer;
        }

        if (Strings.isNullOrEmpty(integerString)) {

            return integer;

        }

        try {

            integer = Integer.parseInt(integerString);

        } catch (final NumberFormatException e) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_INT_VALUE),
                    lineCount, columnIndex + 1, datasetDescriptorACBB.getColumnName(columnIndex),
                    integerString));
            integer = org.inra.ecoinfo.acbb.utils.ACBBUtils.CST_INVALID_BAD_MEASURE;

        }

        return integer;

    }

    /**
     * @param obj
     * @return
     */
    protected int getObjectSize(Object obj) {

        if (obj == null) {

            return 0;

        }

        byte[] abyte0;

        try {

            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();

            ObjectOutputStream objectoutputstream = new ObjectOutputStream(bytearrayoutputstream);

            objectoutputstream.writeObject(obj);

            abyte0 = bytearrayoutputstream.toByteArray();

        } catch (IOException e) {

            return 100_000;

        }

        return abyte0.length;

    }

    /**
     * Gets the site.
     *
     * @param requestProperties
     * @return the site
     * @link{IRequestPropertiesACBB the request properties
     */
    protected SiteACBB getSite(final IRequestPropertiesACBB requestProperties) {

        return requestProperties.getSite();

    }

    /**
     * Gets the suivi parcelle.
     *
     * @param errorsReport
     * @param parcelle
     * @param traitementProgramme
     * @param dateDeDebutDeTraitementSurParcelle
     * @param lineCount
     * @return the suivi parcelle
     * @link{ErrorsReport the errors report
     * @link{Parcelle the parcelle
     * @link{TraitementProgramme the traitement programme
     * @link{Date the date de debut de traitement sur parcelle
     * @link{long the line count
     */
    protected SuiviParcelle getSuiviParcelle(final ErrorsReport errorsReport,
                                             final Parcelle parcelle, final TraitementProgramme traitementProgramme,
                                             final LocalDate dateDeDebutDeTraitementSurParcelle, final long lineCount) {

        SuiviParcelle suiviParcelle = null;
        try {
            suiviParcelle = this.suiviParcelleDAO.getByNKey(parcelle, traitementProgramme, dateDeDebutDeTraitementSurParcelle)
                    .orElse(null);
            if (suiviParcelle == null) {

                errorsReport
                        .addErrorMessage(String.format(
                                RecorderACBB
                                        .getACBBMessage(RecorderACBB.PROPERTY_MSG_BAD_MONITORING_PLOT_WITH_LINE_NUMBER),
                                lineCount,
                                parcelle.getName(),
                                traitementProgramme.getAffichage(),
                                DateUtil.getUTCDateTextFromLocalDateTime(dateDeDebutDeTraitementSurParcelle, DateUtil.DD_MM_YYYY)));

            }
        } catch (DateTimeException e) {

            errorsReport
                    .addErrorMessage(String.format(
                            RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER),
                            lineCount, parcelle.getName(), traitementProgramme.getAffichage()));

        }
        return suiviParcelle;

    }

    /**
     * Gets the traitement programme.
     *
     * @param errorsReport
     * @param lineCount
     * @param i
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestProperties
     * @return the traitement programme
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{int the i
     * @link{CleanerValues the cleaner values
     * @link{DatasetDescriptorACBB the dataset descriptor acbb
     * @link{IRequestPropertiesACBB the request properties
     */
    protected TraitementProgramme getTraitementProgramme(final ErrorsReport errorsReport,
                                                         final long lineCount, final int i, final CleanerValues cleanerValues,
                                                         final DatasetDescriptorACBB datasetDescriptorACBB,
                                                         final IRequestPropertiesACBB requestProperties) {

        TraitementProgramme traitementProgramme = null;
        assert requestProperties.getSite() != null : "le site ne doit pas être nul";
        String traitementCode = null;
        try {

            traitementCode = Utils.createCodeFromString(cleanerValues.nextToken());

        } catch (final ArrayIndexOutOfBoundsException | EndOfCSVLine e) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_TREATMENT),
                    lineCount, i));

        }
        try {
            traitementProgramme = this.traitementDAO.getByNKey(requestProperties.getSite().getId(), traitementCode).orElse(null);
            if (traitementProgramme == null) {

                throw new PersistenceException();

            }
        } catch (final PersistenceException e) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_TREATMENT),
                    lineCount, i));

        }
        return traitementProgramme;

    }

    /**
     * @param errorsReport
     * @param lineNumber
     * @param columnIndex
     * @param dvu
     * @param column
     * @param value
     * @return
     */
    protected ACBBVariableValue<T> getVariableValue(ErrorsReport errorsReport, Long lineNumber,
                                                    int columnIndex, DatatypeVariableUniteACBB dvu, Column column, String value) {

        ACBBVariableValue<T> variableValue = null;
        if (null != column.getFlagType()) {
            switch (column.getFlagType()) {
                case RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE:
                    try {
                        final T valeurQualitative = this.listeACBBDAO.getByNKey(Utils.createCodeFromString(column.getName()), Utils.createCodeFromString(value))
                                .orElseThrow(() -> new PersistenceException(AbstractProcessRecord.PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE));
                        variableValue = new ACBBVariableValue<>(dvu, valeurQualitative);
                    } catch (final PersistenceException e) {

                        errorsReport
                                .addErrorMessage(String.format(
                                        RecorderACBB
                                                .getACBBMessageWithBundle(
                                                        AbstractProcessRecord.BUNDLE_PATH,
                                                        AbstractProcessRecord.PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE),
                                        lineNumber, columnIndex + 1, value, column.getName()));

                    }
                    break;
                case RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE:
                    String[] valeursQualitativesArray = org.apache.commons.lang.StringUtils.split(value, ',');
                    List<T> valeursQualitativesList = new LinkedList();
                    for (String valeurQualitativeString : valeursQualitativesArray) {
                        try {
                            final T valeurQualitative = this.listeACBBDAO.getByNKey(Utils.createCodeFromString(column.getName()), Utils.createCodeFromString(valeurQualitativeString.trim())).orElse(null);
                            if (valeurQualitative == null) {

                                throw new PersistenceException(
                                        AbstractProcessRecord.PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE);

                            }
                            valeursQualitativesList.add(valeurQualitative);
                        } catch (final PersistenceException e) {

                            errorsReport
                                    .addErrorMessage(String.format(
                                            RecorderACBB
                                                    .getACBBMessageWithBundle(
                                                            AbstractProcessRecord.BUNDLE_PATH,
                                                            AbstractProcessRecord.PROPERTY_MSG_UNFOUND_VALEUR_QUALITATIVE),
                                            lineNumber, columnIndex + 1, valeurQualitativeString
                                                    .trim(), column.getName()));

                        }
                        variableValue = new ACBBVariableValue<>(dvu,
                                valeursQualitativesList);
                    }
                    break;
                default:
                    variableValue = new ACBBVariableValue<>(dvu,
                            value.replaceAll(",", "."));
                    break;
            }
        }
        return variableValue;

    }

    /**
     * Gets the version.
     *
     * @param errorsReport
     * @param lineCount
     * @param i
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestProperties
     * @return the version
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{int the i
     * @link{CleanerValues the cleaner values
     * @link{DatasetDescriptorACBB the dataset descriptor acbb
     * @link{IRequestPropertiesACBB the request properties
     */
    protected int getVersion(final ErrorsReport errorsReport, final long lineCount, final int i,
                             final CleanerValues cleanerValues, final DatasetDescriptorACBB datasetDescriptorACBB,
                             final IRequestPropertiesACBB requestProperties) {

        int version = -1;

        try {

            version = Integer.parseInt(cleanerValues.nextToken());

        } catch (final NumberFormatException | IndexOutOfBoundsException | EndOfCSVLine e) {

            errorsReport.addErrorMessage(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_VERSION_TREATMENT),
                    lineCount, i));

        }

        return version;

    }

    /**
     * Gets the version de traitement.
     *
     * @param errorsReport
     * @param requestProperties
     * @param traitementProgramme
     * @param version
     * @param lineCount
     * @return the version de traitement
     * @link{ErrorsReport the errors report
     * @link{IRequestPropertiesACBB the request properties
     * @link{TraitementProgramme the traitement programme
     * @link{int the version
     * @link{long the line count
     */
    protected VersionDeTraitement getVersionDeTraitement(final ErrorsReport errorsReport,
                                                         final IRequestPropertiesACBB requestProperties,
                                                         final TraitementProgramme traitementProgramme, final int version, final long lineCount) {

        final VersionDeTraitement versionDeTraitement = this.versionDeTraitementDAO.getByNKey(
                requestProperties.getSite().getCode(), traitementProgramme.getCode(), version).orElse(null);
        if (versionDeTraitement == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_SITE_TREATMENT_VERSION),
                    traitementProgramme.getAffichage(), lineCount, 2, requestProperties.getSite()
                            .getName(), version));

        }
        return versionDeTraitement;

    }

    /**
     * @param errorsReport
     * @throws BusinessException
     */
    protected void notifyInfoMessages(ErrorsReport errorsReport) throws BusinessException {

        if (errorsReport == null || !errorsReport.hasInfos()) {
            return;
        }
        Notification notification = this.buildWarningNotification(errorsReport);
        this.notificationsManager.addNotification(notification, this.policyManager.getCurrentUserLogin());
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {

        this.verifieSiteRepositoryEqualsSite(versionFile, requestProperties);
        Utils.testNotNullArguments(this.localizationManager, versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, this.localizationManager);

    }

    /**
     * @param versionFile
     * @return
     * @throws BadExpectedValueException
     */
    protected IntervalDate getIntervalDateForVersion(VersionFile versionFile) throws BadExpectedValueException {
        return new IntervalDate(versionFile.getDataset().getDateDebutPeriode(), versionFile.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY);

    }

    /**
     * Gets the parcelle.
     *
     * @param errorsReport
     * @param lineCount
     * @param i
     * @param cleanerValues
     * @param datasetDescriptorACBB
     * @param requestProperties
     * @return the parcelle
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{int the i
     * @link{CleanerValues the cleaner values
     * @link{DatasetDescriptorACBB the dataset descriptor acbb
     * @link{IRequestPropertiesACBB the request properties
     */
    protected Parcelle readDbParcelle(final ErrorsReport errorsReport, final long lineCount,
                                      final int i, final CleanerValues cleanerValues,
                                      final DatasetDescriptorACBB datasetDescriptorACBB,
                                      final IRequestPropertiesACBB requestProperties) {

        Parcelle parcelle = null;
        String parcelleCode = null;
        try {
            parcelleCode = Utils.createCodeFromString(cleanerValues.nextToken());
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, i).getMessage());
        }
        if (Strings.isNullOrEmpty(parcelleCode)) {
            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_PLOT), lineCount, i + 1));
            return parcelle;
        }
        parcelle = this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(parcelleCode, requestProperties.getSite())).orElse(null);
        if (parcelle == null) {

            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_PLOT),
                    parcelleCode, lineCount, i + 1, requestProperties.getSite().getName()));

        }
        return parcelle;

    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the new datatype unite variable acbbdao
     *                                     {@link IDatatypeVariableUniteACBBDAO}
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {

        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;

    }

    /**
     * @param listeACBBDAO
     */
    public void setListeACBBDAO(I listeACBBDAO) {

        this.listeACBBDAO = listeACBBDAO;

    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {

        this.localizationManager = localizationManager;

    }

    /**
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the parcelle dao.
     *
     * @param parcelleDAO the new parcelle dao
     */
    public final void setParcelleDAO(final IParcelleDAO parcelleDAO) {

        this.parcelleDAO = parcelleDAO;

    }

    /**
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the suivi parcelle dao.
     *
     * @param suiviParcelleDAO the new suivi parcelle dao
     */
    public final void setSuiviParcelleDAO(final ISuiviParcelleDAO suiviParcelleDAO) {
        this.suiviParcelleDAO = suiviParcelleDAO;
    }

    /**
     * Sets the traitement dao.
     *
     * @param traitementDAO the new traitement dao
     */
    public final void setTraitementDAO(final ITraitementDAO traitementDAO) {

        this.traitementDAO = traitementDAO;

    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public final void setVariableDAO(final IVariableACBBDAO variableDAO) {

        this.variableDAO = variableDAO;

    }

    /**
     * Sets the version de traitement dao.
     *
     * @param versionDeTraitementDAO the new version de traitement dao
     */
    public final void setVersionDeTraitementDAO(final IVersionDeTraitementDAO versionDeTraitementDAO) {

        this.versionDeTraitementDAO = versionDeTraitementDAO;

    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao
     */
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {

        this.versionFileDAO = versionFileDAO;

    }

    /**
     * Verifie site repository equals site.
     *
     * @param version
     * @param requestProperties
     * @throws BusinessException the business exception
     * @link{VersionFile the version
     * @link{IRequestPropertiesACBB the request properties
     */
    protected void verifieSiteRepositoryEqualsSite(final VersionFile version,
                                                   final IRequestPropertiesACBB requestProperties) throws BusinessException {
        final SiteACBB site = this.getSite(requestProperties);
        if (site == null
                || !site.getPath().equalsIgnoreCase(getSite(version).getPath())) {

            final String error = String.format(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_SITE_REPOSITORY),
                    site == null ? "Null" : site.getPath(),
                    getSite(version).getPath());

            final BusinessException businessException = new BusinessException(error);

            throw businessException;

        }
    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorACBB
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(final CSVParser parser,
                                                                                final Map<Integer, Column> columns, final DatasetDescriptorACBB datasetDescriptorACBB)
            throws PersistenceException, IOException {
        List<DatatypeVariableUniteACBB> dvus = new LinkedList();
        Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites = this.datatypeVariableUniteACBBDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode(this.datatypeName);
        for (final Map.Entry<Integer, Column> colonneEntry : columns.entrySet()) {
            if (colonneEntry.getValue().isFlag()
                    && (colonneEntry.getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE)
                    || colonneEntry.getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE) || colonneEntry
                    .getValue().getFlagType()
                    .equals(RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE))) {
                final VariableACBB variable = readVariableFromColumn(colonneEntry.getValue()).orElse(null);
                dvus.add(variable == null ? null : dbDatatypeVariableUnites.get(variable.getCode()));
            } else {
                dvus.add(null);
            }
        }
        for (int i = 0; i < datasetDescriptorACBB.getEnTete(); i++) {
            parser.getLine();
        }
        return dvus;
    }

    /**
     * @param column
     * @return
     */
    protected Optional<VariableACBB> readVariableFromColumn(final Column column) {
        return this.variableDAO.getByAffichage(column.getName())
                .map(v -> (VariableACBB) v);
    }

    /**
     * Gets the boolean.
     *
     * @param errorsReport          the errors report {@link ErrorsReport}
     * @param lineCount             the line count long
     * @param columnIndex           the column index
     * @param cleanerValues         the cleaner values {@link CleanerValues}
     * @param datasetDescriptorACBB
     * @return the boolean
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     */
    protected boolean getBoolean(final ErrorsReport errorsReport, final long lineCount,
                                 final int columnIndex, final CleanerValues cleanerValues,
                                 final DatasetDescriptorACBB datasetDescriptorACBB) {
        boolean bool = false;
        final String booleanString;
        try {
            booleanString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            return bool;
        }
        if (Strings.isNullOrEmpty(booleanString)) {
            return bool;
        }
        bool = Boolean.valueOf(booleanString);
        return bool;
    }

}
