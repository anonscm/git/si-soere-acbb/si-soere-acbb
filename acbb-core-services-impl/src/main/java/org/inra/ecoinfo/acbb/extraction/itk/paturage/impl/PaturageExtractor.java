package org.inra.ecoinfo.acbb.extraction.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKExtractor;

/**
 * @author ptcherniati
 */
public class PaturageExtractor extends AbstractITKExtractor<Paturage> {

}
