/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * The Class TestDuplicateWsol.
 *
 * implementation for Tillage of {@link ITestDuplicates}
 *
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux
 * files
 */
public class TestDuplicateWsol extends AbstractTestDuplicate {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String ACBB_DATASET_WSOL_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.travaildusol.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The date time line.
     *
     * @link(SortedMap<String,SortedMap<String,SortedSet<Long>>>).
     */
    final SortedMap<String, SortedMap<String, SortedMap<String, SortedSet<Long>>>> line;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateWsol() {
        this.line = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param parcelle
     * @param date
     * @link(String) the date
     * @param rang
     * @link(String) the rang
     * @param numero
     * @link(String) the numero
     * @param lineNumber long the line number
     * @link(String) the date
     * @link(String) the order of the intervention in the day
     * @link(String) the number of the tool in intervention
     */
    protected void addLine(final String parcelle, final String date, final String rang,
                           final String numero, final long lineNumber) {
        final String key = this.getKey(parcelle, date);
        if (!this.line.containsKey(key)) {
            this.line.put(key, new TreeMap<>());
        }
        if (!this.line.get(key).containsKey(rang)) {
            this.line.get(key).put(rang, new TreeMap<>());
        }
        if (!this.line.get(key).get(rang).containsKey(numero)) {
            this.line.get(key).get(rang).put(numero, new TreeSet<>());
            this.line.get(key).get(rang).get(numero).add(lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateWsol.ACBB_DATASET_WSOL_BUNDLE_NAME,
                    ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle, date, rang,
                    numero, this.line.get(key).get(rang).get(numero).first().intValue()));
        }

    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[6], values[7], lineNumber + 1);
    }
}
