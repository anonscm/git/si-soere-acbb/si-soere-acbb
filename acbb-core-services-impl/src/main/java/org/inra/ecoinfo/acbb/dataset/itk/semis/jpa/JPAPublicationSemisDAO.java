/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Query;
import java.util.List;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationSemisDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     * The Constant QUERY_SELECT_SEQUENCE @link(String).
     */
    private static final String QUERY_SELECT_SEQUENCE = "from Semis sem where sem.version=:version";

    /**
     * The Constant QUERY_DELETE_SEQUENCE @link(String).
     */
    private static final String QUERY_DELETE_SEQUENCE = "delete from Semis sem where sem in(:semis)";

    /**
     * The Constant QUERY_DELETE_MEASURE @link(String).
     */
    private static final String QUERY_DELETE_MEASURE = "delete from MesureSemis where  semis in (:semis)";

    /**
     * The Constant QUERY_DELETE_VALEUR @link(String).
     */
    private static final String QUERY_DELETE_VALEUR = "delete from ValeurSemis where mesureSemis in (select id from MesureSemis msem where  semis in (:semis))";

    /**
     * The Constant QUERY_DELETE_VALEUR_ATTR @link(String).
     */
    private static final String QUERY_DELETE_VALEUR_ATTR = "delete from ValeurSemisAttr where semis in (:semis)";

    /**
     * @param version
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        final String requeteSelectSemis = JPAPublicationSemisDAO.QUERY_SELECT_SEQUENCE;
        Query query = this.entityManager.createQuery(requeteSelectSemis);
        query.setParameter("version", version);
        List<Semis> semis = query.getResultList();
        if (semis.isEmpty()) {
            return;
        }
        final String requeteDeleteValeurs = JPAPublicationSemisDAO.QUERY_DELETE_VALEUR;
        query = this.entityManager.createQuery(requeteDeleteValeurs);
        query.setParameter("semis", semis);
        query.executeUpdate();
        final String requeteDeleteValeurAttr = JPAPublicationSemisDAO.QUERY_DELETE_VALEUR_ATTR;
        query = this.entityManager.createQuery(requeteDeleteValeurAttr);
        query.setParameter("semis", semis);
        query.executeUpdate();
        final String requeteDeleteMesure = JPAPublicationSemisDAO.QUERY_DELETE_MEASURE;
        query = this.entityManager.createQuery(requeteDeleteMesure);
        query.setParameter("semis", semis);
        query.executeUpdate();
        final String requeteDeleteSequence = JPAPublicationSemisDAO.QUERY_DELETE_SEQUENCE;
        query = this.entityManager.createQuery(requeteDeleteSequence);
        query.setParameter("semis", semis);
        query.executeUpdate();
    }

}
