package org.inra.ecoinfo.acbb.dataset.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.IErrorsReport;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DateTimeException;
import java.time.LocalDateTime;

/**
 * The Class AbstractTestDuplicate.
 */
public abstract class AbstractTestDuplicate implements ITestDuplicates {

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractTestDuplicate.class);
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The errors report @link(IErrorsReport).
     */
    protected IErrorsReport errorsReport = new ErrorsReport();

    /**
     *
     */
    protected IDatasetConfiguration configuration;

    /**
     * Instantiates a new abstract test duplicate.
     */
    public AbstractTestDuplicate() {
        super();
    }

    /**
     * Adds the errors.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report {@link BadsFormatsReport} the bads formats
     * report
     */
    @Override
    public void addErrors(final BadsFormatsReport badsFormatsReport) {
        badsFormatsReport
                .addException(new BusinessException(this.errorsReport.getErrorsMessages()));
    }

    /**
     * @param args
     * @return
     */
    protected String getKey(final String... args) {
        final StringBuffer buf = new StringBuffer();
        for (int i = 0; i < args.length; i++) {
            if (i != 0) {
                buf.append(RecorderACBB.CST_UNDERSCORE);
            }
            buf.append(args[i]);

        }
        return buf.toString();
    }

    /**
     * @param dateString
     * @param timeString
     * @return
     */
    protected String getLocalValue(String dateString, String timeString) {
        return String.format("%-"
                        + RecorderACBB.DD_MM_YYYY_HHMMSS_READ.length() + "."
                        + RecorderACBB.DD_MM_YYYY_HHMMSS_READ.length() + "s",
                dateString + timeString + ":00");
    }

    /**
     * Checks for error.
     *
     * @return true, if successful
     * @see org.inra.ecoinfo.acbb.dataset.ITestDuplicates#hasError()
     */
    @Override
    public boolean hasError() {
        return this.errorsReport.hasErrors();
    }

    /**
     * @param startDateDB
     * @param startTimeDB
     * @param startDate
     * @param startTime
     * @return
     */
    protected boolean isLimitDate(String startDateDB, String startTimeDB, String startDate, String startTime) {
        try {
            startTime = (startTime + ":00").substring(0, DateUtil.HH_MM_SS.length());
            startTimeDB = (startTimeDB + ":00").substring(0, DateUtil.HH_MM_SS.length());
            LocalDateTime date = DateUtil.readLocalDateTimeFromLocalDateAndLocaltime(DateUtil.DD_MM_YYYY, startDate, DateUtil.HH_MM_SS, startTime);
            LocalDateTime dbDate = DateUtil.readLocalDateTimeFromLocalDateAndLocaltime(DateUtil.DD_MM_YYYY, startDateDB, DateUtil.HH_MM_SS, startTimeDB);
            return date.equals(dbDate);
        } catch (DateTimeException e) {
            AbstractTestDuplicate.LOGGER.info("pas de date", e);
            return false;
        }
    }

    /**
     * @param configuration
     */
    public void setConfiguration(IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the errors report.
     *
     * @param errorsReport the new errors report @link(IErrorsReport) {@link ErrorsReport} the new errors
     *                     report
     */
    @Override
    public final void setErrorsReport(final IErrorsReport errorsReport) {

        this.errorsReport = errorsReport;
    }

    /**
     * @param dates
     * @param dateString
     * @param timeString
     * @param lineNumber
     * @param versionFile
     */
    protected void testLineForDuplicatesDateInDB(String[] dates, String dateString,
                                                 String timeString, final long lineNumber, VersionFile versionFile) {
        if (dates == null || dates.length != 4 || Strings.isNullOrEmpty(dateString)
                || Strings.isNullOrEmpty(timeString)) {
            return;
        }
        if (this.isLimitDate(dates[0], dates[1], dateString, timeString)) {
            this.testLineForDuplicatesLineinDb(dates[0], dates[1], lineNumber, versionFile);
        } else if (this.isLimitDate(dates[2], dates[3], dateString, timeString)) {
            this.testLineForDuplicatesLineinDb(dates[2], dates[3], lineNumber, versionFile);
        }
    }

    /**
     * @param dateDBString
     * @param timeDBString
     * @param lineNumber
     * @param versionFile
     */
    protected void testLineForDuplicatesLineinDb(String dateDBString, String timeDBString,
                                                 final long lineNumber, VersionFile versionFile) {
    }

}
