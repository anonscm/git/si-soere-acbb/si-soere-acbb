/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IRequestPropertiesSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDatatypeManager;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;

import java.io.Serializable;

/**
 * The Class RequestPropertiesSemis.
 */
public class RequestPropertiesSemis extends RequestPropertiesITK implements
        IRequestPropertiesSemis, Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new request properties semis.
     */
    public RequestPropertiesSemis() {
        super();
    }

    /**
     * Gets the nom de fichier.
     *
     * @param version
     * @return the nom de fichier
     * @link(VersionFile) the version
     * @link(VersionFile) the version
     * @see org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getNomDeFichier(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getNomDeFichier(final VersionFile version) {
        if (version == null) {
            return null;
        }
        final Dataset dataset = version.getDataset();
        if (ACBBUtils.getSiteFromDataset(dataset) == null
                || ACBBUtils.getDatatypeFromDataset(dataset) == null
                || dataset.getDateDebutPeriode() == null || dataset.getDateFinPeriode() == null) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier
                    .append((ACBBUtils.getSiteFromDataset(dataset)).getPath())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(ISemisDatatypeManager.CODE_DATATYPE_SEMIS)
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DateUtil.DD_MM_YYYY_FILE))
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DateUtil.DD_MM_YYYY_FILE))
                    .append(".")
                    .append(IRequestPropertiesACBB.FORMAT_FILE);
        } catch (final Exception e) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        return nomFichier.toString();
    }

    /**
     * @return @see
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB#getTestDuplicates()
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateSem();
    }
}
