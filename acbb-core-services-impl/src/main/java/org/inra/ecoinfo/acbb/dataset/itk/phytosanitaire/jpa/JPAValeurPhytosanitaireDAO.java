/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.IValeurPhytosanitaireDAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire;

/**
 * The Class JPAValeurTravailDuSolDAO.
 */
public class JPAValeurPhytosanitaireDAO extends AbstractJPADAO<ValeurPhytosanitaire> implements
        IValeurPhytosanitaireDAO {

}
