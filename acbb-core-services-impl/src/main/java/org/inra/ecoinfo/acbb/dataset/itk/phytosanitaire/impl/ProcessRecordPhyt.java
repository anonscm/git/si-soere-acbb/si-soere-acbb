package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.IProduitPhytosanitaireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.ProduitPhytosanitaire;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * process the record of tillage files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
public class ProcessRecordPhyt extends AbstractProcessRecordITK {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant CST_BEAN_SESSION_PROPERTIES_TRAVAIL_DU_SOL @link(String).
     */
    static final String PHYT_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.fertilisant.messages";
    /**
     * The Constant PROPERTY_MSG_INVALID_FERT_TYPE @link(String).
     */
    static final String PROPERTY_MSG_INVALID_PRODUIT_PHYTOSANITAIRE = "PROPERTY_MSG_INVALID_PRODUIT_PHYTOSANITAIRE";
    private static final Logger LOG = LoggerFactory.getLogger(ProcessRecordPhyt.class
            .getName());
    private IProduitPhytosanitaireDAO produitPhytosanitaireDAO;

    /**
     * Instantiates a new process record wsol.
     */
    public ProcessRecordPhyt() {
        super();
    }

    /**
     * Builds the new line.
     *
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesITK
     * @link(VersionFile) the version
     * @link(List<? extends AbstractLineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(VersionFile) the version
     * @link(List<? extends AbstractLineRecord>) the lines
     * @link(ErrorsReport) the errors report
     * @link(IRequestPropertiesITK) the request properties itk
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder#buildNewLine(org
     * .inra.ecoinfo.dataset.versioning.entity.VersionFile, java.util.LinkedList,
     * org.inra.ecoinfo.acbb.dataset.impl.AbstractACBBRecorder.ErrorsReport)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void buildNewLines(final VersionFile version,
                                 final List<? extends AbstractLineRecord> lines, final ErrorsReport errorsReport,
                                 final IRequestPropertiesITK requestPropertiesITK) {
        final BuildPhytosanitaireHelper instance = ApplicationContextHolder.getContext().getBean(
                BuildPhytosanitaireHelper.class);
        instance.build(version, (List<LineRecord>) lines, errorsReport, requestPropertiesITK);
    }

    private ProduitPhytosanitaire getProduitPhytosanitaire(ErrorsReport errorsReport,
                                                           long lineCount, int columnIndex, CleanerValues cleanerValues) {
        ProduitPhytosanitaire produitPhytosanitaire = null;
        String nomProduit = null;
        try {
            nomProduit = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, columnIndex).getMessage());
        }
        try {
            produitPhytosanitaire = this.produitPhytosanitaireDAO.getByCode(Utils
                    .createCodeFromString(nomProduit))
                    .orElseThrow(PersistenceException::new);

        } catch (PersistenceException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordPhyt.PHYT_BUNDLE_NAME,
                    ProcessRecordPhyt.PROPERTY_MSG_INVALID_PRODUIT_PHYTOSANITAIRE), lineCount,
                    columnIndex + 1, nomProduit));
        }
        return produitPhytosanitaire;
    }

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser)
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile finalVersionFile = versionFile;
        super.processRecord(parser, finalVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            String[] values = null;
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            finalVersionFile = this.versionFileDAO.merge(finalVersionFile);
            final List<LineRecord> lines = new LinkedList();
            lineCount = datasetDescriptor.getEnTete();
            while ((values = parser.getLine()) != null) {
                lineCount++;
                final List<ACBBVariableValue<ListeItineraire>> variablesValues = new LinkedList();
                final CleanerValues cleanerValues = new CleanerValues(values);
                final LineRecord lineRecord = new LineRecord(lineCount);
                int columnIndex = this.getGenericColumns(errorsReport, lineCount, cleanerValues,
                        lineRecord, datasetDescriptor, (IRequestPropertiesITK) requestProperties, intervalDate);
                final int rang = this.getInt(errorsReport, lineCount, columnIndex++, cleanerValues,
                        datasetDescriptor);
                ProduitPhytosanitaire produitPhytosanitaire = this.getProduitPhytosanitaire(
                        errorsReport, lineCount, columnIndex++, cleanerValues);
                final Iterator<DatatypeVariableUniteACBB> variableIterator = dbVariables.iterator();
                for (final Entry<Integer, Column> entry : ((IRequestPropertiesITK) requestProperties)
                        .getValueColumns().entrySet()) {
                    if (entry.getKey() >= values.length) {
                        break;
                    }
                    final String value = values[entry.getKey()];
                    if (entry.getKey() < columnIndex || Strings.isNullOrEmpty(value)) {
                        continue;
                    }
                    ACBBVariableValue<ListeItineraire> variableValue = null;
                    final DatatypeVariableUniteACBB dvu = variableIterator.next();
                    variableValue = this.getVariableValue(errorsReport, lineCount, entry.getKey(),
                            dvu, entry.getValue(), value);
                    variablesValues.add(variableValue);
                }
                lineRecord.update(rang, produitPhytosanitaire, variablesValues);
                lines.add(lineRecord);
            }
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(finalVersionFile, lines, errorsReport,
                        (IRequestPropertiesITK) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * @param produitPhytosanitaireDAO the produitPhytosanitaireDAO to set
     */
    public void setProduitPhytosanitaireDAO(IProduitPhytosanitaireDAO produitPhytosanitaireDAO) {
        this.produitPhytosanitaireDAO = produitPhytosanitaireDAO;
    }
}
