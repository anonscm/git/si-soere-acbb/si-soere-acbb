/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPAPeriodeRealiseeDAO.
 */
public class JPAPeriodeRealiseeDAO extends AbstractJPADAO<PeriodeRealisee> implements
        IPeriodeRealiseeDAO {

    /**
     * Gets the by n key.
     *
     * @param anneeRealisee
     * @param periode
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO#getByNKey(org.inra
     * .ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee, int)
     * @link(PeriodeRealisee) the annee realisee
     * @link(int) the periode
     */
    @Override
    public Optional<PeriodeRealisee> getByNKey(AnneeRealisee anneeRealisee, int periode) {

        CriteriaQuery<PeriodeRealisee> query = builder.createQuery(PeriodeRealisee.class);
        Root<PeriodeRealisee> ar = query.from(PeriodeRealisee.class);
        query.where(
                builder.equal(ar.get(PeriodeRealisee_.anneeRealisee), anneeRealisee),
                builder.equal(ar.get(PeriodeRealisee_.numero), periode)
        );
        query.select(ar);
        return getOptional(query);
    }
}
