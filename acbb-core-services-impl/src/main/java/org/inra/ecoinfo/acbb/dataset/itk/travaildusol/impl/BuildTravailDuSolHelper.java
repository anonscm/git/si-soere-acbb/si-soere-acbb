package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.IMesureTravailDuSolDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.ITravailDuSolDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.TravailDuSol;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildTravailDusolHelper.
 */
public class BuildTravailDuSolHelper extends AbstractBuildHelper<LineRecord> {

    /**
     * The travaux du sol.
     */
    Map<Parcelle, Map<LocalDate, Map<Integer, TravailDuSol>>> travauxDuSol = new HashMap();

    /**
     * The mesure travail du sol dao @link(IMesureTravailDuSolDAO).
     */
    IMesureTravailDuSolDAO mesureTravailDuSolDAO;

    /**
     * The travail du sol dao @link(ITravailDuSolDAO).
     */
    ITravailDuSolDAO travailDuSolDAO;

    /**
     * Builds the.
     *
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() {
        for (final LineRecord line : this.lines) {
            final TravailDuSol travailDuSol = this.getOrcreateTravailDuSol(line);
            if (travailDuSol == null) {
                break;
            }
            try {
                this.travailDuSolDAO.saveOrUpdate(travailDuSol);
            } catch (final PersistenceException e) {
                LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("mesureTravailDuSolDAO.saveOrUpdate", e);
            }
            final MesureTravailDuSol mesureTravailDuSol = this.getOrcreateMesureTravailDuSol(line,
                    travailDuSol);
            for (final ACBBVariableValue<? extends IValeurQualitative> variableValue : line
                    .getVariablesValues()) {
                ValeurTravailDuSol valeurTravailDuSol = null;

                if (Strings.isNullOrEmpty(variableValue.getValue())
                        && variableValue.getValeurQualitative() == null
                        && variableValue.getValeursQualitatives() == null) {
                    continue;
                }
                RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
                if (variableValue.isValeurQualitative()
                        && variableValue.getValeursQualitatives() != null) {
                    valeurTravailDuSol = new ValeurTravailDuSol(
                            variableValue.getValeursQualitatives(), realNode,
                            mesureTravailDuSol);
                } else if (variableValue.isValeurQualitative()) {
                    valeurTravailDuSol = new ValeurTravailDuSol(
                            (ListeItineraire) variableValue.getValeurQualitative(),
                            realNode, mesureTravailDuSol);
                } else {
                    valeurTravailDuSol = new ValeurTravailDuSol(Float.parseFloat(variableValue
                            .getValue()), realNode, mesureTravailDuSol);
                }
                mesureTravailDuSol.getValeursTravailDuSol().add(valeurTravailDuSol);
            }
            try {
                this.mesureTravailDuSolDAO.saveOrUpdate(mesureTravailDuSol);
            } catch (final PersistenceException e) {
                LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("mesureTravailDuSolDAO.saveOrUpdate", e);
            }
        }
    }

    /**
     * Instantiates a new builds the travail dusol helper.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines {@link AbstractLineRecord}
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(IRequestPropertiesITK) the request properties itk
     */
    public void build(final VersionFile version, final List<LineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK) {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    /**
     * Gets the orcreate mesure travail du sol.
     *
     * @param line
     * @param travailDuSol
     * @return the orcreate mesure travail du sol
     * @link(LineRecord) the line
     * @link(TravailDuSol) the travail du sol
     * @link(LineRecord) the line
     * @link(TravailDuSol) the travail du sol
     */
    MesureTravailDuSol getOrcreateMesureTravailDuSol(final LineRecord line,
                                                     final TravailDuSol travailDuSol) {
        MesureTravailDuSol mesureTravailDuSol = null;
        for (final MesureTravailDuSol mesureTravailDuSol2 : travailDuSol.getMesuresTravailDuSol()) {
            if (mesureTravailDuSol2.getNumero() == line.getNumeroPassage()) {
                mesureTravailDuSol = mesureTravailDuSol2;
                break;
            }
        }
        if (mesureTravailDuSol == null) {
            mesureTravailDuSol = new MesureTravailDuSol(travailDuSol, line.getObservation(),
                    line.getNumeroPassage());
            travailDuSol.getMesuresTravailDuSol().add(mesureTravailDuSol);
        }
        return mesureTravailDuSol;
    }

    /**
     * Gets the orcreate travail du sol.
     *
     * @param line
     * @return the orcreate travail du sol
     * @link(LineRecord) the line
     * @link(LineRecord) the line
     */
    TravailDuSol getOrcreateTravailDuSol(final LineRecord line) {
        Parcelle parcelle = line.getParcelle();
        return travauxDuSol
                .computeIfAbsent(parcelle, k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.rang, k -> createNewTravailDuSol(line));
    }

    /**
     * Sets the mesure travail du sol dao.
     *
     * @param mesureTravailDuSolDAO the mesureTravailDuSolDAO to set
     */
    public final void setMesureTravailDuSolDAO(final IMesureTravailDuSolDAO mesureTravailDuSolDAO) {
        this.mesureTravailDuSolDAO = mesureTravailDuSolDAO;
    }

    /**
     * Sets the travail du sol dao.
     *
     * @param travailDuSolDAO the travailDuSolDAO to set
     */
    public final void setTravailDuSolDAO(final ITravailDuSolDAO travailDuSolDAO) {
        this.travailDuSolDAO = travailDuSolDAO;
    }

    private TravailDuSol createNewTravailDuSol(LineRecord line) {
        final Integer rang = line.rang;
        TravailDuSol travailDuSol = new TravailDuSol(this.version, line.getObservation(), line.getDate(),
                line.getRotationNumber(), line.getAnneeNumber(), line.getPeriodeNumber(), rang);
        travailDuSol.setSuiviParcelle(line.getSuiviParcelle());
        return travailDuSol;
    }

}
