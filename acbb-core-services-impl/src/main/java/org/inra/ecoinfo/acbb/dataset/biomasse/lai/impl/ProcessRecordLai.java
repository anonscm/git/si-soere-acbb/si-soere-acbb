package org.inra.ecoinfo.acbb.dataset.biomasse.lai.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl.ProcessRecordBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractProcessRecordBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.LaiNature;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai.IMethodeLAIDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai.MethodeLAI;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ptcherniati
 */
public class ProcessRecordLai extends AbstractProcessRecordBiomasse {

    /**
     *
     */
    public static final String CST_COLUMN_LAI_MOYENNE = "lai_moyenne";
    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant SEMIS_BUNDLE_NAME.
     */
    static final String LOCAL_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.lai.messages";
    static final String PROPERTY_MSG_BAD_BIOMASSE_NATURE = "PROPERTY_MSG_BAD_BIOMASSE_NATURE";
    static final String PROPERTY_MSG_BAD_BIOMASSE_ES = "PROPERTY_MSG_BAD_BIOMASSE_ES";
    static final String PROPERTY_MSG_NULL_VALUE = "PROPERTY_MSG_NULL_VALUE";
    private IMethodeLAIDAO methodeLAIDAO;

    /**
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesBiomasse
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractProcessRecordBiomasse#buildNewLines(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.util.List, org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void buildNewLines(VersionFile version, List<? extends AbstractLineRecord> lines,
                                 ErrorsReport errorsReport, IRequestPropertiesBiomasse requestPropertiesBiomasse)
            throws PersistenceException {
        final BuildLaiHelper instance = ApplicationContextHolder.getContext().getBean(
                BuildLaiHelper.class);
        instance.build(version, (List<LaiLineRecord>) lines, errorsReport,
                requestPropertiesBiomasse);
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param fileEncoding
     * @param requestProperties
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile localVersionFile = versionFile;
        super.processRecord(parser, localVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;

            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesBiomasse) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            localVersionFile = this.versionFileDAO.merge(localVersionFile);
            final List<LaiLineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, intervalDate, (IRequestPropertiesBiomasse) requestProperties,
                    datasetDescriptor, errorsReport, lineCount, dbVariables, lines);
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(localVersionFile, lines, errorsReport,
                        (IRequestPropertiesBiomasse) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param requestProperties
     * @param datasetDescriptor
     * @param errorsReport
     * @param lineCount
     * @param dbVariables
     * @param lines
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{IRequestPropertiesACBB the request properties
     * @link{DatasetDescriptorACBB the dataset descriptor
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{java.util.List<VariableACBB> the db variables
     * @link{java.util.List<AILineRecord> the lines
     */
    long readLines(final CSVParser parser, final IntervalDate intervalDate, final IRequestPropertiesBiomasse requestProperties,
                   final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                   long lineCount, final List<DatatypeVariableUniteACBB> dbVariables, final List<LaiLineRecord> lines)
            throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final LaiLineRecord lineRecord = new LaiLineRecord(localLineCount);
            int index = this.readGenericColumns(errorsReport, localLineCount, cleanerValues,
                    lineRecord, datasetDescriptor, requestProperties);
            index = this.readNature(cleanerValues, lineRecord, errorsReport, lineCount, index);
            index = this.readNatureCouvert(cleanerValues, lineRecord, errorsReport, lineCount, index);
            index = this.readDateAndSetVersionTraitement(intervalDate, lineRecord, errorsReport, localLineCount,
                    index, cleanerValues, datasetDescriptor, requestProperties);
            while (index < values.length) {
                index = this.readVariableValueLai(lineRecord, errorsReport, localLineCount, index,
                        cleanerValues, dbVariables);
            }
            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }

    private int readMethod(final ErrorsReport errorsReport, final long lineCount, int index,
                           final CleanerValues cleanerValues, final VariableValueLai variableValue) {
        String methodString = null;
        try {
            methodString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        try {
            Long methodNumber = Long.parseLong(methodString);
            MethodeLAI methodeLai = this.methodeLAIDAO.getByNumberId(methodNumber).orElse(null);
            if (methodeLai == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        ProcessRecordBiomassProduction.BUNDLE_NAME,
                        ProcessRecordBiomassProduction.PROPERTY_MSG_BAD_METHOD), lineCount,
                        index + 1, methodString));
            } else {
                variableValue.setMethode(methodeLai);
            }
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    ProcessRecordBiomassProduction.BUNDLE_NAME,
                    ProcessRecordBiomassProduction.PROPERTY_MSG_BAD_NUMBER_FOR_METHOD), lineCount,
                    index + 1, methodString));
        }
        return index + 1;
    }

    private int readVariableValueLai(LaiLineRecord lineRecord, ErrorsReport errorsReport,
                                     long lineCount, int index, CleanerValues cleanerValues, List<DatatypeVariableUniteACBB> dbVariables) {
        int localIndex = index;
        DatatypeVariableUniteACBB dvu = dbVariables.get(index);
        VariableValueLai variableValue = new VariableValueLai(localIndex, 0, 0F,
                0, dvu, null, null, null);
        localIndex = this.readES(errorsReport, lineCount, localIndex, cleanerValues, variableValue);
        localIndex = this.readValue(cleanerValues, variableValue, errorsReport, lineCount, localIndex);
        if (Strings.isNullOrEmpty(variableValue.getValue())) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessage(
                    RecorderACBB.PROPERTY_MSG_MISSING_VALUE),
                    lineCount,
                    index + 1,
                    CST_COLUMN_LAI_MOYENNE));
        }
        localIndex = this.readMeasureNumber(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readStandardDeviation(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readMediane(errorsReport, lineCount, localIndex, cleanerValues, variableValue);
        localIndex = this.readMethod(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        localIndex = this.readQualityIndex(errorsReport, lineCount, localIndex, cleanerValues,
                variableValue);
        if (!Strings.isNullOrEmpty(variableValue.getValue())) {
            lineRecord.getVariablesValues().add(variableValue);
        }
        return localIndex;
    }

    /**
     * @param methodeLAIDAO
     */
    public void setMethodeLAIDAO(IMethodeLAIDAO methodeLAIDAO) {
        this.methodeLAIDAO = methodeLAIDAO;
    }

    /**
     * @param cleanerValues
     * @param line
     * @param errorsReport
     * @param lineCount
     * @param index
     * @return
     */
    protected int readNature(CleanerValues cleanerValues, LaiLineRecord line,
                             ErrorsReport errorsReport, long lineCount, int index) {
        String laiStringNatureString = null;
        try {
            laiStringNatureString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        LaiNature laiNature = (LaiNature) this.getListeVegetationDAO().getByNKey(LaiNature.LAI_NATURE, Utils.createCodeFromString(laiStringNatureString)).orElse(null);
        if (laiNature == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    LOCAL_BUNDLE_NAME,
                    PROPERTY_MSG_BAD_BIOMASSE_NATURE), lineCount,
                    index + 1, laiStringNatureString));

        } else {
            line.setNature(laiNature);
        }
        return index + 1;
    }

    private int readES(ErrorsReport errorsReport, long lineCount, int index,
                       CleanerValues cleanerValues, VariableValueLai variableValue) {
        String biomasseESString = null;
        try {
            biomasseESString = cleanerValues.nextToken();
        } catch (EndOfCSVLine ex) {
            errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
        }
        if (Strings.isNullOrEmpty(biomasseESString)) {
            return index + 1;
        }
        BiomasseEntreeSortie biomasseEntreeSortie = (BiomasseEntreeSortie) this.listeACBBDAO.getByNKey(BiomasseEntreeSortie.BIOMASSE_ENTREE_SORTIE, Utils.createCodeFromString(biomasseESString)).orElse(null);
        if (biomasseEntreeSortie == null) {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    LOCAL_BUNDLE_NAME,
                    PROPERTY_MSG_BAD_BIOMASSE_ES), lineCount, index + 1,
                    biomasseESString));

        } else {
            variableValue.setBiomasseEntreeSortie(biomasseEntreeSortie);
        }
        return index + 1;
    }

}
