package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.dataset.itk.semis.impl.LineRecord;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import java.util.Map;
import java.util.Map.Entry;

/**
 * The Class RotationDescriptor.
 * <p>
 * A rotation Descriptor describes the alternance of sowing on a plot for a
 * treatment.
 * </p>
 *
 * @author philippe Tcherniatinsky
 */
public class RotationDescriptor {

    /**
     * The cst coma @link(String).
     */
    static final String CST_COMA = ", ";

    /**
     * The rotation @link(Rotation).
     */
    org.inra.ecoinfo.acbb.refdata.modalite.Rotation rotation = null;

    /**
     * The couverts @link(Map<Integer,SemisCouvertVegetal>).
     */
    Map<Integer, SemisCouvertVegetal> couverts;

    /**
     * The expected couvert @link(SemisCouvertVegetal).
     */
    SemisCouvertVegetal expectedCouvert;

    /**
     * The is possible couvert @link(boolean).
     */
    boolean isPossibleCouvert;

    /**
     * The is expected couvert @link(boolean).
     */
    boolean isExpectedCouvert;

    /**
     * Instantiates a new rotation descriptor.
     * <p>
     * If there's a modality of rotation for the treatment on the plot, the
     * rotation arg is non null and the couverts and expectedCouverts are
     * informed.
     * </p>
     * <p>
     * in that case, the RotationDescriptor can say if the cover is possible and
     * expected for the year
     * </p>
     * <p>
     * else the rotation arg is null
     * </p>
     *
     * @param line
     * @param versionDeTraitement
     * @link(AbstractLineRecord) the line
     * @link(VersionDeTraitement) the version de traitement
     */
    public RotationDescriptor(final AbstractLineRecord line,
                              final VersionDeTraitement versionDeTraitement) {
        super();
        for (final Modalite modalite : versionDeTraitement.getModalites()) {
            if (modalite instanceof org.inra.ecoinfo.acbb.refdata.modalite.Rotation) {
                this.rotation = (org.inra.ecoinfo.acbb.refdata.modalite.Rotation) modalite;
                this.couverts = this.rotation.getCouvertsVegetal();
                this.expectedCouvert = this.couverts.get(line.getAnneeNumber());
                this.isPossibleCouvert = line instanceof LineRecord && this.couverts.containsValue(((LineRecord) line).getCouvertVegetal());
                this.isExpectedCouvert = this.expectedCouvert != null && (line instanceof LineRecord) && this.expectedCouvert
                        .equals(((LineRecord) line).getCouvertVegetal());
            }
        }

    }

    /**
     * Gets the couverts.
     *
     * @return the couverts
     */
    public Map<Integer, SemisCouvertVegetal> getCouverts() {
        return this.couverts;
    }

    /**
     * Sets the couverts.
     *
     * @param couverts the couverts to set
     */
    public void setCouverts(final Map<Integer, SemisCouvertVegetal> couverts) {
        this.couverts = couverts;
    }

    /**
     * Gets the expected couvert.
     *
     * @return the expectedCouvert
     */
    public SemisCouvertVegetal getExpectedCouvert() {
        return this.expectedCouvert;
    }

    /**
     * Gets the expected couvert list.
     *
     * @return the expected couvert list
     */
    public String getExpectedCouvertList() {
        if (this.couverts == null) {
            return null;
        }
        final StringBuffer buf = new StringBuffer();
        for (final Entry<Integer, SemisCouvertVegetal> entry : this.couverts.entrySet()) {
            if (buf.length() > 0) {
                buf.append(RotationDescriptor.CST_COMA);
            }
            buf.append(entry.getValue().getValeur());
        }
        return buf.toString();
    }

    /**
     * Gets the rotation.
     *
     * @return the rotation
     */
    public org.inra.ecoinfo.acbb.refdata.modalite.Rotation getRotation() {
        return this.rotation;
    }

    /**
     * Sets the rotation.
     *
     * @param rotation the rotation to set
     */
    public void setRotation(final org.inra.ecoinfo.acbb.refdata.modalite.Rotation rotation) {
        this.rotation = rotation;
    }

    /**
     * Checks if is expected couvert.
     *
     * @return the isExpectedCouvert
     */
    public boolean isExpectedCouvert() {
        return this.isExpectedCouvert;
    }

    /**
     * Sets the expected couvert.
     *
     * @param isExpectedCouvert the isExpectedCouvert to set
     */
    public void setExpectedCouvert(final boolean isExpectedCouvert) {
        this.isExpectedCouvert = isExpectedCouvert;
    }

    /**
     * Sets the expected couvert.
     *
     * @param expectedCouvert the expectedCouvert to set
     */
    public void setExpectedCouvert(final SemisCouvertVegetal expectedCouvert) {
        this.expectedCouvert = expectedCouvert;
    }

    /**
     * Checks if is possible couvert.
     *
     * @return the isPossibleCouvert
     */
    public boolean isPossibleCouvert() {
        return this.isPossibleCouvert;
    }

    /**
     * Sets the possible couvert.
     *
     * @param isPossibleCouvert the isPossibleCouvert to set
     */
    public void setPossibleCouvert(final boolean isPossibleCouvert) {
        this.isPossibleCouvert = isPossibleCouvert;
    }
}
