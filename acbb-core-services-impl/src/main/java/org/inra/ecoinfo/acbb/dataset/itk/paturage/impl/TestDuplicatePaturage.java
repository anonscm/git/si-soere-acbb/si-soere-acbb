/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.IErrorsReport;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.*;

/**
 * The Class TestDuplicateWsol.
 *
 * implementation for Tillage of {@link ITestDuplicates}
 *
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux
 * files
 */
public class TestDuplicatePaturage extends AbstractTestDuplicate {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String ACBB_DATASET_PAT_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.paturage.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE @link(String).
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The line @link(SortedMap<String,Long>).
     */
    final SortedMap<String, Long> line;

    @Transient
    final PeriodesPaturage periodesPaturage;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicatePaturage() {
        this.line = new TreeMap();
        this.periodesPaturage = new PeriodesPaturage();
    }

    /**
     * Adds the line.
     *
     * @param parcelle
     * @param dateDebut
     * @param lineNumber the line number
     * @param dateFin
     */
    protected void addLine(final String parcelle, final String dateDebut, final String dateFin,
                           final long lineNumber) {
        final String key = this.getKey(parcelle, dateDebut, dateFin);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            this.periodesPaturage.addInterval(lineNumber, parcelle, dateDebut, dateFin, this.errorsReport);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicatePaturage.ACBB_DATASET_PAT_BUNDLE_NAME,
                    TestDuplicatePaturage.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelle,
                    dateDebut, dateFin, this.line.get(key)));
        }

    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], lineNumber + 1);
    }

    static class PeriodesPaturage implements Serializable {

        private static final long serialVersionUID = 1L;
        @Transient
        final Map<String, List<IntervalDate>> periodes = new HashMap();

        protected void addInterval(final Long lineNumber, final String parcelle, final String beginDate,
                                   final String endDate, final IErrorsReport errorsReport) {
            if (!this.periodes.containsKey(parcelle)) {
                this.periodes.put(parcelle, new LinkedList<>());
            }
            IntervalDate intervalDate = null;
            try {
                intervalDate = new IntervalDate(beginDate, endDate,
                        DateUtil.DD_MM_YYYY);

            } catch (final BadExpectedValueException e) {
                String message = String.format(RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_LINE_ERROR), lineNumber, e.getMessage());
                errorsReport.addErrorMessage(message);
            }
            this.testInterval(parcelle, intervalDate);
        }

        void testInterval(final String parcelle, final IntervalDate intervalDate) {
            for (final IntervalDate interval : this.periodes.get(parcelle)) {
                if (intervalDate.overlaps(interval)) {
                    // TODO(ptcherniati) :
                    // org.inra.ecoinfo.acbb.dataset.itk.paturage.impl/TestDuplicatePaturage.PeriodesPaturage::testInterval()
                    //
                    UncatchedExceptionLogger.logUncatchedException("cas d'overlap à traiter",
                            new BusinessException());
                }
            }
        }
    }
}
