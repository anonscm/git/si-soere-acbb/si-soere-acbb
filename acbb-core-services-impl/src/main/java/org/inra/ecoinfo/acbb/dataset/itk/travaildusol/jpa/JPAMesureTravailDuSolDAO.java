/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.IMesureTravailDuSolDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.MesureTravailDuSol;

/**
 * The Class JPAMesureTravailDuSolDAO.
 */
public class JPAMesureTravailDuSolDAO extends AbstractJPADAO<MesureTravailDuSol> implements
        IMesureTravailDuSolDAO {

}
