/*
 *
 */
package org.inra.ecoinfo.acbb.extraction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * The Class MessagesACBBExtraction.
 */
public final class MessagesACBBExtraction {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.extraction.messages";
    /**
     * The Constant RESOURCE_BUNDLE @link(ResourceBundle).
     */
    static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(MessagesACBBExtraction.BUNDLE_NAME);
    private static final Logger LOGGER = LoggerFactory
            .getLogger(MessagesACBBExtraction.class
                    .getName());

    /**
     * Instantiates a new messages acbb extraction.
     */
    MessagesACBBExtraction() {
    }

    /**
     * Gets the string.
     *
     * @param key the key
     * @return the string
     */
    public static String getString(final String key) {
        try {
            return MessagesACBBExtraction.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
