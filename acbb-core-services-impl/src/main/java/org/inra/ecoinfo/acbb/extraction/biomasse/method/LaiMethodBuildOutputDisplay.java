/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.method;

import org.inra.ecoinfo.acbb.extraction.methods.AbstractMethodBuildOutputDisplay;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai.Recorder;

/**
 * The Class SemisBuildOutputDisplayByRow.
 */
public class LaiMethodBuildOutputDisplay extends AbstractMethodBuildOutputDisplay<Recorder> {

    static final String CST_METHOD_NAME = "LaiMethode";

    /**
     * @return
     */
    @Override
    protected String getFileName() {
        return LaiMethodBuildOutputDisplay.CST_METHOD_NAME;
    }
}
