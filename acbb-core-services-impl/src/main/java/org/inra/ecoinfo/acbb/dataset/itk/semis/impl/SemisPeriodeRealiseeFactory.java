package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultPeriodeRealiseeFactory;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * A factory for creating SemisPeriodeRealisee objects.
 */
public class SemisPeriodeRealiseeFactory extends DefaultPeriodeRealiseeFactory<LineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.impl.messages";
    /**
     * The Constant PROPERTY_MSG_SOWING_INVERSION @link(String)
     */
    static final String PROPERTY_MSG_SOWING_INVERSION = "PROPERTY_MSG_SOWING_INVERSION";
    /**
     * The Constant PROPERTY_MSG_ERROR_BAD_PERIODE_NUMBER @link(String).
     */
    static final String PROPERTY_MSG_ERROR_BAD_PERIODE_NUMBER = "PROPERTY_MSG_ERROR_BAD_PERIODE_NUMBER";
    /**
     * The Constant PROPERTY_MSG_ERROR_EXISTING_PERIODE @link(String).
     */
    static final String PROPERTY_MSG_ERROR_EXISTING_PERIODE = "PROPERTY_MSG_ERROR_EXISTING_PERIODE";
    private static final Logger LOGGER = LoggerFactory.getLogger(SemisPeriodeRealiseeFactory.class
            .getName());

    /**
     * <p>
     * for sowing there's a default periode for first sowing with null number
     * </p>
     * <p>
     * only second sowing must build a new period
     * </p>
     *
     * @param line
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultPeriodeRealiseeFactory#getOrCreatePeriodeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport)
     */
    @Override
    public Optional<PeriodeRealisee> getOrCreatePeriodeRealisee(final LineRecord line, final ErrorsReport errorsReport) {
        Optional<PeriodeRealisee> periodeRealisee = this.periodeRealiseeDAO.getByNKey(line.getTempo()
                .getAnneeRealisee(), line.getPeriodeNumber());
        final int errorsCount = errorsReport.getErrorsMessages().length();
        if (periodeRealisee.isPresent() && line.getPeriodeNumber() != 0) {
            String bindedMessages = ACBBUtils.getBindedsMessages(periodeRealisee.get().getBindedsITK());
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisPeriodeRealiseeFactory.BUNDLE_NAME,
                    SemisPeriodeRealiseeFactory.PROPERTY_MSG_ERROR_EXISTING_PERIODE), line
                    .getPeriodeNumber(), line.getAnneeNumber(), line.getRotationNumber(), line
                    .getTreatmentVersion(), line.getTreatmentAffichage(), bindedMessages));
            return Optional.empty();
        }
        if (line.getSemisRotationDescriptor().isExpectedCouvert() && line.getPeriodeNumber() == 1) {
            if (!periodeRealisee.isPresent()) {
                PeriodeRealisee periodeRealisee2 = new PeriodeRealisee(1, line.getTempo().getAnneeRealisee());
                periodeRealisee2.setCouvertVegetal(line.getCouvertVegetal());
                try {
                    this.periodeRealiseeDAO.saveOrUpdate(periodeRealisee2);
                } catch (final PersistenceException e) {
                    errorsReport
                            .addErrorMessage(RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
                }
                periodeRealisee = Optional.ofNullable(periodeRealisee2);
            }
        } else if (!line.getSemisRotationDescriptor().isExpectedCouvert()
                && line.getPeriodeNumber() == 0) {
            if (line.getSemisRotationDescriptor().isPossibleCouvert()) {
                final String expectedcover = line.getSemisRotationDescriptor().getCouverts()
                        .get(line.getAnneeNumber()).getValeur();
                final String cover = line.getCouvertVegetal().getValeur();
                errorsReport.addInfoMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        SemisPeriodeRealiseeFactory.BUNDLE_NAME,
                        SemisPeriodeRealiseeFactory.PROPERTY_MSG_SOWING_INVERSION), line
                                .getOriginalLineNumber(), line.getPeriodeNumber(), line.getAnneeNumber(),
                        line.getRotationNumber(), line.getTreatmentVersion(), line
                                .getTreatmentAffichage(), this
                                .getInternationalizedValeurQualitative(cover), this
                                .getInternationalizedValeurQualitative(expectedcover)));
            } else {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        SemisPeriodeRealiseeFactory.BUNDLE_NAME,
                        SemisPeriodeRealiseeFactory.PROPERTY_MSG_ERROR_BAD_PERIODE_NUMBER), line
                                .getOriginalLineNumber(), line.getPeriodeNumber(), line.getAnneeNumber(),
                        line.getRotationNumber(), line.getTreatmentVersion(), line
                                .getTreatmentAffichage()));
            }
        }
        if (errorsCount != errorsReport.getErrorsMessages().length()) {
            line.getTempo().setInErrors(Boolean.TRUE);
        }
        return periodeRealisee;
    }

}
