/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.acbb.extraction.AbstractRequestReminder;
import org.inra.ecoinfo.extraction.IParameter;
import java.io.PrintStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Stream;

/**
 * The Class BiomasseRequestReminderOutputBuilder.
 */
public class BiomasseRequestReminderOutputBuilder extends AbstractRequestReminder {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.biomasse.messages";
    private static final String PROPERTY_MSG_SELECTED_BIOAMSSE_PRODUCTION_VARIABLES = "PROPERTY_MSG_SELECTED_BIOAMSSE_PRODUCTION_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_HAUTEUR_HERBE_VARIABLES = "PROPERTY_MSG_SELECTED_HAUTEUR_HERBE_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_LAI_VARIABLES = "PROPERTY_MSG_SELECTED_LAI_VARIABLES";
    private String hauteurVegetalDatatype;
    private String biomasseProductionDatatype;
    private String laiDatatype;

    /**
     * Builds the output data.
     *
     * @param requestMetadatasMap
     * @param outputPrintStreamMap
     * @link(IParameter) the parameters
     * @link(Map<String,PrintStream>) the output print stream map @see
     * org.inra.ecoinfo.acbb.extraction.mps.impl. MPSOutputsBuildersResolver#
     * buildOutputData (org.inra.ecoinfo.extraction.IParameter, java.util.Map)
     */
    protected void buildOutputData(final Map<String, Object> requestMetadatasMap,
            final Map<String, PrintStream> outputPrintStreamMap) {
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            super.buildOutputData(requestMetadatasMap, datatypeNameEntry.getValue(),  
                    Stream.of(PROPERTY_MSG_SELECTED_BIOAMSSE_PRODUCTION_VARIABLES,PROPERTY_MSG_SELECTED_HAUTEUR_HERBE_VARIABLES, PROPERTY_MSG_SELECTED_LAI_VARIABLES).toArray(String[]::new ),  
                    Stream.of(biomasseProductionDatatype,hauteurVegetalDatatype, laiDatatype).toArray(String[]::new ));
        }
    }

    /**
     * @param biomasseProductionDatatype
     */
    public void setBiomasseProductionDatatype(String biomasseProductionDatatype) {
        this.biomasseProductionDatatype = biomasseProductionDatatype;
    }

    /**
     * @param hauteurVegetalDatatype
     */
    public void setHauteurVegetalDatatype(String hauteurVegetalDatatype) {
        this.hauteurVegetalDatatype = hauteurVegetalDatatype;
    }

    /**
     * @param laiDatatype
     */
    public void setLaiDatatype(String laiDatatype) {
        this.laiDatatype = laiDatatype;
    }

    @Override
    protected String getLocalBundle() {
        return BUNDLE_SOURCE_PATH;
    }

}
