package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import java.time.LocalDate;
import java.util.List;

/**
 * The Class LineRecord.
 * <p>
 * one line of record
 */
public class PaturageLineRecord extends AbstractLineRecord<PaturageLineRecord, ListeItineraire> {

    /**
     * The paturage rotation descriptor @link(RotationDescriptor).
     */
    protected RotationDescriptor paturageRotationDescriptor;
    LocalDate endDate;
    RotationDescriptor rotation;

    /**
     * Instantiates a new line record.
     *
     * @param lineCount long the line count
     * @link(SuiviParcelle) the suivi parcelle
     * @link(SuiviParcelle) the suivi parcelle
     */
    public PaturageLineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * @return the variablesValues
     */
    public List<ACBBVariableValue<ListeItineraire>> getACBBVariablesValues() {
        return this.variablesValues;
    }

    /**
     * @param variablesValues the variablesValues to set
     */
    public final void setACBBVariablesValues(
            final List<ACBBVariableValue<ListeItineraire>> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return this.endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public final void setEndDate(final LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return
     */
    public RotationDescriptor getPaturageRotationDescriptor() {
        return this.paturageRotationDescriptor;
    }

    /**
     * @return the rotation
     */
    public RotationDescriptor getRotation() {
        return this.rotation;
    }

    /**
     * Gets the treatment affichage.
     *
     * @return the treatment affichage
     */
    public String getTreatmentAffichage() {
        String returnString = org.apache.commons.lang.StringUtils.EMPTY;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null
                && this.getTempo().getVersionDeTraitement().getTraitementProgramme() != null) {
            returnString = this.getTempo().getVersionDeTraitement().getTraitementProgramme()
                    .getAffichage();
        }
        return returnString;
    }

    /**
     * Gets the treatment version.
     *
     * @return the treatment version
     */
    public int getTreatmentVersion() {
        int returnInt = -1;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null) {
            returnInt = this.getTempo().getVersionDeTraitement().getVersion();
        }
        return returnInt;
    }

    /**
     * @return
     */
    public boolean isRotation() {
        return this.getPaturageRotationDescriptor() != null;
    }

    /**
     * @param rotation
     */
    public final void setSemisRotationDescriptor(final RotationDescriptor rotation) {
        this.rotation = rotation;
    }
}
