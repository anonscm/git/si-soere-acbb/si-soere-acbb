/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.listeitineraire;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<ListeItineraire> {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The liste itineraire dao.
     */
    IListeItineraireDAO listeItineraireDAO;

    /**
     * The liste itineraire dao.
     */
    IModaliteDAO modaliteDAO;
    /**
     * The properties nom fr @link(Properties).
     */
    Properties propertiesValeurFR;
    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesValeurEN;
    /**
     * The properties valeur fr @link(Properties).
     */
    private Properties propertiesNomFR;
    /**
     * The properties valeur en @link(Properties).
     */
    private Properties propertiesNomEN;

    /**
     * Instantiates a new recorder.
     */
    public Recorder() {
        super();
    }

    /**
     * Creates the or update liste itineraire.
     *
     * @param nom
     * @param valeur
     * @param dbListeItineraire
     * @throws PersistenceException the persistence exception
     * @link(String) the code
     * @link(String) the nom
     * @link(ListeItineraire) the db liste itineraire
     * @link(String) the code
     * @link(String) the nom
     * @link(ListeItineraire) the db liste itineraire
     */
    void createOrUpdateListeItineraire(final String nom, final String valeur,
                                       final ListeItineraire dbListeItineraire) throws PersistenceException {
        ListeItineraire listeItineraireToUpdate = dbListeItineraire;
        if (dbListeItineraire == null) {
            listeItineraireToUpdate = new ListeItineraire(nom, valeur);
        }
        try {
            this.listeItineraireDAO.saveOrUpdate(listeItineraireToUpdate);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                this.listeItineraireDAO.remove(this.listeItineraireDAO.getByNKey(nom, valeur).orElseThrow(() -> new BusinessException("bad liste")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ListeItineraire> getAllElements() {
        List<ListeItineraire> all = this.listeItineraireDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param listeItineraire
     * @return the new line model grid metadata
     * @link(ListeItineraire) the liste itineraire
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final ListeItineraire listeItineraire) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : listeItineraire.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.get(listeItineraire.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.get(listeItineraire.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : listeItineraire.getValeur(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.propertiesValeurFR.getProperty(listeItineraire.getValeur(), listeItineraire.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.propertiesValeurEN.getProperty(listeItineraire.getValeur(), listeItineraire.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<ListeItineraire> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA,
                IValeurQualitative.ATTRIBUTE_JPA_CODE, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA,
                IValeurQualitative.ATTRIBUTE_JPA_CODE, Locale.ENGLISH);
        this.propertiesValeurFR = this.localizationManager.newProperties(ListeACBB.NAME_ENTITY_JPA,
                ValeurQualitative.ATTRIBUTE_JPA_VALUE, Locale.FRANCE);
        this.propertiesValeurEN = this.localizationManager.newProperties(ListeACBB.NAME_ENTITY_JPA,
                ValeurQualitative.ATTRIBUTE_JPA_VALUE, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist liste itineraire.
     *
     * @param errorsReport
     * @param nom
     * @param valeur
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the code
     * @link(String) the nom
     * @link(ErrorsReport) the errors report
     * @link(String) the code
     * @link(String) the nom
     */
    void persistListeItineraire(final ErrorsReport errorsReport, final String nom,
                                final String valeur) throws PersistenceException {
        Optional<ListeItineraire> dblistListeItineraire = this.listeItineraireDAO.getByNKey(nom, valeur);
        this.createOrUpdateListeItineraire(nom, valeur, dblistListeItineraire.orElse(null));
    }

    /**
     * Process record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ListeACBB.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String nom = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                this.persistListeItineraire(errorsReport, nom, valeur);
            }
            this.listeItineraireDAO.flush();
            final List<Rotation> rotations = this.modaliteDAO.getRotations();
            for (final Rotation rotation : rotations) {
                this.listeItineraireDAO.updateCouvertVegetalFromRotation(rotation);
            }
            if (errorsReport.hasErrors()) {

                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the liste itineraire dao.
     *
     * @param listeItineraireDAO the new liste itineraire dao
     */
    public final void setListeItineraireDAO(final IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    /**
     * @param modaliteDAO the modaliteDAO to set
     */
    public final void setModaliteDAO(final IModaliteDAO modaliteDAO) {
        this.modaliteDAO = modaliteDAO;
    }
}
