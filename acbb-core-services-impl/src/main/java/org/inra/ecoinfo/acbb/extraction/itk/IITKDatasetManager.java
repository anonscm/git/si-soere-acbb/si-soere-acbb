/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk;

import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.extraction.jsf.ITreatmentManager;
import org.inra.ecoinfo.acbb.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

import java.time.LocalDate;
import java.util.Map;
import java.util.SortedMap;

/**
 * The Interface IIKDatasetManager.
 */
public interface IITKDatasetManager extends ITreatmentManager, IVariableManager<TraitementProgramme> {

    /**
     * @param parcelle
     * @param date
     * @param availablesCouverts
     * @return
     */
    VegetalPeriode getCouvertVegetal(Parcelle parcelle, LocalDate date,
                                     Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts);

}
