/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.utils.Column;

import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Class RequestPropertiesBiomasse.
 */
public abstract class RequestPropertiesBiomasse extends AbstractRequestPropertiesACBB implements
        IRequestPropertiesBiomasse, Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The value columns.
     */
    @Transient
    Map<Integer, Column> valueColumns = new TreeMap();

    /**
     * Instantiates a new request properties biomasse.
     */
    public RequestPropertiesBiomasse() {
        super();
    }

    /**
     * @return
     */
    @Override
    public Map<Integer, Column> getValueColumns() {
        return this.valueColumns;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#setDateDeFin
     * (java.time.LocalDateTime)
     */
    @Override
    public final void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.DAYS, 1);
    }

    @Override
    protected void buildTree(LocalDateTime dateFin, ChronoUnit chronoUnit, int step, String dateFormatForCompare) {
        //o nothing
    }
}
