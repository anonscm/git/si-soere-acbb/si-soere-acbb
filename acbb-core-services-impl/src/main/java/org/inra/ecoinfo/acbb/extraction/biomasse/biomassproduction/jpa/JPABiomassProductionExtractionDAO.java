/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.biomassproduction.jpa;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction_;
import org.inra.ecoinfo.acbb.dataset.biomasse.jpa.AbstractJPABiomasseDAO;
import org.inra.ecoinfo.acbb.synthesis.biomasseproduction.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 *
 * @param <BiomassProduction>
 * @param <ValeurBiomassProduction>
 */
class JPABiomassProductionExtractionDAO extends
        AbstractJPABiomasseDAO<BiomassProduction, ValeurBiomassProduction> {

    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected Class getValeurBiomassClass() {
        return ValeurBiomassProduction.class;
    }

    @Override
    protected SingularAttribute<ValeurBiomassProduction, BiomassProduction> getMesureAttribute() {
        return ValeurBiomassProduction_.biomassProduction;
    }

    @Override
    protected Class<BiomassProduction> getBiomassClass() {
        return BiomassProduction.class;
    }
}
