package org.inra.ecoinfo.acbb.dataset.itk.paturage.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultAnneeRealiseeFactory;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

/**
 * A factory for creating SemisAnneeRealisee objects.
 */
public class PaturageAnneeRealiseeFactory extends DefaultAnneeRealiseeFactory<PaturageLineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.paturage.impl.messages";

    AnneeRealisee createNewAnneeRealisee(final PaturageLineRecord line) {
        final AnneeRealisee anneeRealisee = new AnneeRealisee(line.getAnneeNumber(), line
                .getTempo().getVersionTraitementRealisee());
        this.setDates(line, anneeRealisee);
        return anneeRealisee;
    }

    /**
     * @param line
     * @param infoReport
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeFactory#getOrCreateAnneeRealisee(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public AnneeRealisee getOrCreateAnneeRealisee(final PaturageLineRecord line,
                                                  final ErrorsReport errorsReport, final InfoReport infoReport) {
        assert line.getTempo().getVersionTraitementRealisee() != null : "la version de traitement ne doit pas être nulle";
        AnneeRealisee anneeRealisee = line.getTempo().getVersionTraitementRealisee()
                .getAnneesRealisees().get(line.getAnneeNumber());
        if (line.isRotation() && anneeRealisee == null) {
            // // TODO(ptcherniati) :
            // org.inra.ecoinfo.acbb.dataset.itk.paturage.impl/PaturageAnneeRealiseeFactory::getOrCreateAnneeRealisee()
            // l'année doit être générée par le fichier semis
            errorsReport.addErrorMessage("l'année doit être générée par le fichier semis");
            return null;
        } else if (anneeRealisee == null) {
            anneeRealisee = this.createNewAnneeRealisee(line);
        }
        try {
            this.updateAnneeRealisee(anneeRealisee, line, errorsReport);
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        return anneeRealisee;
    }

    void setDates(final PaturageLineRecord line, final AnneeRealisee anneeRealisee) {
        final LocalDate debutDeTraitement = line.getDateDebutDetraitementSurParcelle();
        LocalDateTime date = debutDeTraitement.atStartOfDay();
        LocalDateTime firstDay = date.plus(line.getAnneeNumber() - 1, ChronoUnit.YEARS).with(TemporalAdjusters.firstDayOfYear());
        LocalDateTime lastDay = firstDay.with(TemporalAdjusters.lastDayOfMonth());
        anneeRealisee.setDateDeDebut(firstDay.toLocalDate());
        anneeRealisee.setDateDeFin(lastDay.toLocalDate());
    }

    /**
     * Update annee realisee.
     *
     * @param anneeRealisee
     * @param errorsReport
     * @param line
     * @throws PersistenceException the persistence exception
     * @link(AnneeRealisee) the annee realisee
     */
    public void updateAnneeRealisee(final AnneeRealisee anneeRealisee,
                                    final PaturageLineRecord line, final ErrorsReport errorsReport)
            throws PersistenceException {
        this.anneeRealiseeDAO.saveOrUpdate(anneeRealisee);
    }
}
