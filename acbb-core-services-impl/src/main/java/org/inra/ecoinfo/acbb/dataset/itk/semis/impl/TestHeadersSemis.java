package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.impl.TestHeadersITK;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IRequestPropertiesSemis;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.io.IOException;

/**
 * @author ptcherniati
 */
public class TestHeadersSemis extends TestHeadersITK {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @return
     * @throws IOException
     */
    @Override
    protected long readLineHeader(final BadsFormatsReport badsFormatsReport,
                                  final CSVParser parser, long lineNumber, final DatasetDescriptor datasetDescriptor,
                                  final IRequestPropertiesACBB requestPropertiesACBB) throws IOException {
        final long returnLineNumber = lineNumber + 1;
        final IRequestPropertiesSemis requestPropertiesSemis = (IRequestPropertiesSemis) requestPropertiesACBB;
        int index = 0;
        final String[] values = parser.getLine();

        String value;
        this.testNameColumns(datasetDescriptor, values, badsFormatsReport, returnLineNumber);
        int repeatedColumnsIndex = 0;
        for (index = datasetDescriptor.getUndefinedColumn() + 1; index < datasetDescriptor
                .getColumns().size(); index++) {
            requestPropertiesSemis.getValueColumns().put(repeatedColumnsIndex,
                    datasetDescriptor.getColumns().get(index));
            repeatedColumnsIndex++;

        }
        index = datasetDescriptor.getUndefinedColumn() + 1;
        repeatedColumnsIndex = 0;
        while (index < values.length) {
            if (repeatedColumnsIndex >= requestPropertiesSemis.getValueColumns().size()) {
                repeatedColumnsIndex = 0;
            }
            value = values[index];
            if (!requestPropertiesSemis.getValueColumns().get(repeatedColumnsIndex).getName()
                    .equalsIgnoreCase(value)) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_COLUMN_HEADER),
                        returnLineNumber, index + 1, value, requestPropertiesSemis
                                .getValueColumns().get(repeatedColumnsIndex).getName())));
            }
            index++;
            repeatedColumnsIndex++;
        }
        return returnLineNumber;
    }

    /**
     * @param datasetDescriptor
     * @param values
     * @param badsFormatsReport
     * @param returnLineNumber
     */
    protected void testNameColumns(final DatasetDescriptor datasetDescriptor,
                                   final String[] values, final BadsFormatsReport badsFormatsReport,
                                   final long returnLineNumber) {
        String value;
        for (int index = 0; index < datasetDescriptor.getUndefinedColumn(); index++) {
            value = values[index];
            if (org.apache.commons.lang.StringUtils.EMPTY.equals(value)) {
                break;
            }
            final String name = datasetDescriptor.getColumns().get(index).getName();
            if (!name.equals(value)) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_COLUMN_HEADER),
                        returnLineNumber, index + 1, value, name)));
            }
        }
    }

}
