/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author ptcherniati
 */
public class EndOfCSVLine extends BusinessException {
    public static final String END_OF_CSV_LINE = "END_OF_CSV_LINE";
    public static final String BUNDLE_NAME = EndOfCSVLine.class.getPackage().getName() + ".messages";
    private Number lineNumber;
    private Number index;

    @Override
    public String getMessage() {
        return String.format(RecorderACBB.getACBBMessageWithBundle(BUNDLE_NAME, END_OF_CSV_LINE), lineNumber, index);
    }

    public EndOfCSVLine setMessage(Number lineNumber, Number index) {
        this.lineNumber = lineNumber;
        this.index = index;
        return this;
    }

}
