/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;

/**
 * @author ptcherniati
 */
public interface IFertilisantDAO extends IInterventionDAO<Fertilisant> {
}
