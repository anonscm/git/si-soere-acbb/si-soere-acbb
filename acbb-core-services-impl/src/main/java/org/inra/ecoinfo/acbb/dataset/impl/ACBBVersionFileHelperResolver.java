/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException;
import org.inra.ecoinfo.utils.Utils;

import java.util.Map;

/**
 * The Class ACBBVersionFileHelperResolver.
 *
 * @see org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver The Class
 * ACBBVersionFileHelperResolver.
 */
public class ACBBVersionFileHelperResolver implements IVersionFileHelperResolver {

    /**
     * The datatypes version file helpers map.
     */
    Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap;

    /**
     * Gets the datatypes version file helpers map.
     *
     * @return the datatypes version file helpers map
     */
    public Map<String, IVersionFileHelper> getDatatypesVersionFileHelpersMap() {
        return this.datatypesVersionFileHelpersMap;
    }

    /**
     * Sets the datatypes version file helpers map.
     *
     * @param datatypesVersionFileHelpersMap the datatypes version file helpers map
     */
    public final void setDatatypesVersionFileHelpersMap(
            final Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap) {
        this.datatypesVersionFileHelpersMap = datatypesVersionFileHelpersMap;
    }

    /**
     * Resolve by datatype.
     *
     * @param datatypeName
     * @return the i version file helper
     * @link(String) the datatype name
     * @link(String) the datatype name
     * @see org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver#resolveByDatatype(java.lang.String)
     */
    @Override
    public IVersionFileHelper resolveByDatatype(final String datatypeName)
            throws NoVersionFileHelperResolvedException {
        final String datatypeCode = Utils.createCodeFromString(datatypeName);
        if (!this.datatypesVersionFileHelpersMap.containsKey(datatypeCode)) {
            this.datatypesVersionFileHelpersMap.put(datatypeCode, new VersionFileHelper());
            // throw new NoVersionFileHelperResolvedException(datatypeCode);
        }
        return this.datatypesVersionFileHelpersMap.get(datatypeCode);
    }

}
