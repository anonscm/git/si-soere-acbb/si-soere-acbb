/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionForNumDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface IPaturageDAO extends IInterventionForNumDAO<Paturage> {

    /**
     * @param version
     * @param dateDebut
     * @param dateFin
     * @return
     */
    Optional<Paturage> getByNKey(VersionFile version, LocalDate dateDebut, LocalDate dateFin);
}
