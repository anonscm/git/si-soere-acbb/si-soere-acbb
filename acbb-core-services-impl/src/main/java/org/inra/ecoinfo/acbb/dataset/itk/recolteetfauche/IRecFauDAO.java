/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche;

import org.inra.ecoinfo.acbb.dataset.itk.IInterventionForNumDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface IRecFauDAO extends IInterventionForNumDAO<RecFau> {

    /**
     * @param version
     * @param date
     * @return
     */
    @Override
    Optional<RecFau> getByNKey(VersionFile version, LocalDate date);
}
