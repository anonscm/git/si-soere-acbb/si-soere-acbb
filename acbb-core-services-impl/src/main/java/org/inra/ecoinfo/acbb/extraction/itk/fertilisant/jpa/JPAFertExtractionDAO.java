/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.fertilisant.jpa;

import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.ValeurFertilisant_;
import org.inra.ecoinfo.acbb.extraction.itk.jpa.AbstractJPAITKDAO;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.fertilisant.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
@SuppressWarnings("unchecked")
public class JPAFertExtractionDAO extends AbstractJPAITKDAO<Fertilisant, Fertilisant, ValeurFertilisant> {

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<Fertilisant> getMesureITKClass() {
        return Fertilisant.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurFertilisant> getValeurITKClass() {
        return ValeurFertilisant.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<?, Fertilisant> getMesureAttribute() {
        return ValeurFertilisant_.fertilisant;
    }
}
