/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.*;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire_;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.synthesis.semis.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.semis.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author tcherniatinsky
 */
public class SemisSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        Stream<SynthesisValue> stream = Stream.empty();
        stream = getValueAndQualitatifFromValeurSemis(stream);
        stream = getValueAndQualitativesFromValeurSemisAttr(stream);
        return stream;
    }

    /**
     * @param stream
     * @return
     */
    public Stream getValueAndQualitatifFromValeurSemis(Stream stream) {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurSemis> v = query.from(ValeurSemis.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<ValeurSemis, ListeACBB> vq = v.join(ValeurSemis_.valeurQualitative, JoinType.LEFT);
        Join<MesureSemis, Semis> s = v.join(ValeurSemis_.mesureSemis).join(MesureSemis_.semis);
        final Join<ValeurSemis, RealNode> varRn = v.join(ValeurSemis_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Path<String> parcelleCode = s.join(Semis_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        final Path<LocalDate> date = s.get(Semis_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<Float> valeur = v.get(ValeurSemis_.valeur);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                date,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                vq.get(ListeACBB_.valeur),
                                builder.avg(valeur),
                                idNode
                        )
                )
                .distinct(true)
                .where(
                        builder.and(
                                builder.equal(node.get(NodeDataSet_.realNode), varRn),
                                builder.or(
                                        builder.isNull(valeur),
                                        builder.gt(valeur, -9999)
                                )
                        )
                )
                .groupBy(
                        sitePath,
                        parcelleCode,
                        variableCode,
                        date,
                        vq.get(ListeACBB_.valeur),
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(date)
                );
        return Stream.concat(stream, getResultAsStream(query));
    }

    /**
     * @param stream
     * @return
     */
    public Stream getValueAndQualitativesFromValeurSemisAttr(Stream stream) {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurSemisAttr> v = query.from(ValeurSemisAttr.class);
        ListJoin<ValeurSemisAttr, ListeItineraire> vqs = v.join(ValeurSemisAttr_.valeursQualitatives, JoinType.LEFT);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<ValeurSemisAttr, ListeACBB> vq = v.join(ValeurSemisAttr_.valeurQualitative, JoinType.LEFT);
        Join<ValeurSemisAttr, AbstractIntervention> s = v.join(ValeurSemisAttr_.semis);
        Join<ValeurSemisAttr, RealNode> varRn = v.join(ValeurSemisAttr_.realNode);
        Join<RealNode, RealNode> dtyRn = varRn.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = dtyRn.join(RealNode_.parent).join(RealNode_.parent);
        Path<String> parcelleCode = s.join(Semis_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        query.distinct(true);
        final Path<LocalDate> date = s.get(Semis_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<Float> valeur = v.get(ValeurSemisAttr_.valeur);
        Path<String> vqValue = vq.get(ListeACBB_.valeur);
        Path<String> vqsValue = vqs.get(ListeItineraire_.valeur);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                date,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                builder.avg(valeur),
                                vq.get(ListeACBB_.valeur),
                                vqsValue,
                                idNode
                        )
                )
                .where(
                        builder.and(
                                builder.equal(node.get(NodeDataSet_.realNode), varRn),
                                builder.or(
                                        builder.isNull(valeur),
                                        builder.gt(valeur, -9999)
                                )
                        )
                )
                .groupBy(
                        sitePath,
                        parcelleCode,
                        variableCode,
                        date,
                        vqValue,
                        vqsValue,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(date)
                );
        return Stream.concat(stream, getResultAsStream(query));
    }

}
