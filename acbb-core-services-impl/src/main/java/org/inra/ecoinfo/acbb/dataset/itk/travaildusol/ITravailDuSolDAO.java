/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.TravailDuSol;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The Interface ITravailDuSolDAO.
 */
public interface ITravailDuSolDAO extends IDAO<TravailDuSol> {

    /**
     * Gets the by n key.
     *
     * @param version the version
     * @param rang    the rang
     * @param date    the date
     * @return the by n key
     */
    Optional<TravailDuSol> getByNKey(VersionFile version, int rang, LocalDate date);
}
