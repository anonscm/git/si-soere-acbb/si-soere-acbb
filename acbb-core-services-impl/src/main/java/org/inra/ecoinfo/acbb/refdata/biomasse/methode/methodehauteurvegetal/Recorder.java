package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.methode.AbstractMethodeRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Class Recorder.
 *
 * @author "Vivianne Koyao Yayende"
 */
public class Recorder extends AbstractMethodeRecorder<MethodeHauteurVegetal> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal.messages";

    private IMethodeHauteurVegetalDAO methodeHauteurVegetalDAO;

    /**
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String numberIdString = tokenizerValues.nextToken();
                ErrorsReport errorsReport = new ErrorsReport();
                MethodeHauteurVegetal methodeHauteurVegetal = this.getByNumberId(numberIdString,
                        errorsReport).orElseThrow(() -> new BusinessException("bad methode"));
                this.methodeHauteurVegetalDAO.remove(methodeHauteurVegetal);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param allMethods
     * @return
     */
    @Override
    protected Set<MethodeHauteurVegetal> filter(Set<AbstractMethode> allMethods) {
        Set<MethodeHauteurVegetal> methodes = new HashSet();
        for (AbstractMethode methode : allMethods) {
            if (methode instanceof MethodeHauteurVegetal) {
                methodes.add((MethodeHauteurVegetal) methode);
            }
        }
        return methodes;
    }

    /**
     * @return
     */
    @Override
    protected List<MethodeHauteurVegetal> getAllElements() {

        List<MethodeHauteurVegetal> all = this.methodeHauteurVegetalDAO.getAll();
        Collections.sort(all);
        return all;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeDefinitionName() {
        return AbstractMethode.ATTRIBUTE_JPA_DEFINITION;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeEntityName() {
        return MethodeHauteurVegetal.NAME_ENTITY_JPA;
    }

    /**
     * @return
     */
    @Override
    protected String getMethodeLibelleName() {
        return AbstractMethode.ATTRIBUTE_JPA_LIBELLE;
    }

    /**
     * @param methodeHauteurVegetal
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final MethodeHauteurVegetal methodeHauteurVegetal) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = super
                .getNewLineModelGridMetadata(methodeHauteurVegetal);

        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<MethodeHauteurVegetal> initModelGridMetadata() {
        this.initProperties();
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(
                        values, MethodeHauteurVegetal.NAME_ENTITY_JPA);
                final Long numberId = this.getNumberId(errorsReport, lineNumber, tokenizerValues);
                final String libelle = this.getLibelle(errorsReport, lineNumber, tokenizerValues);
                final String definition = this.getDefinition(errorsReport, lineNumber,
                        tokenizerValues);
                if (!errorsReport.hasErrors()) {
                    this.saveorUpdateMethodeHauteurVegetal(errorsReport, numberId, libelle,
                            definition);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void saveorUpdateMethodeHauteurVegetal(
            AbstractCSVMetadataRecorder.ErrorsReport errorsReport, Long numberId,
            String libelle, String definition) {

        MethodeHauteurVegetal methodeHauteurVegetalDb = this.methodeHauteurVegetalDAO.getByNKey(numberId).orElse(null);
        if (methodeHauteurVegetalDb == null) {

            methodeHauteurVegetalDb = new MethodeHauteurVegetal(definition, libelle, numberId);

        } else {

            methodeHauteurVegetalDb.update(definition);
        }
        try {

            this.methodeHauteurVegetalDAO.saveOrUpdate(methodeHauteurVegetalDb);

        } catch (PersistenceException e) {

            String message = RecorderACBB
                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION);
            errorsReport.addErrorMessage(message);

        }

    }

    /**
     * @param methodeHauteurVegetalDAO
     */
    public void setMethodeHauteurVegetalDAO(IMethodeHauteurVegetalDAO methodeHauteurVegetalDAO) {
        this.methodeHauteurVegetalDAO = methodeHauteurVegetalDAO;
    }

}
