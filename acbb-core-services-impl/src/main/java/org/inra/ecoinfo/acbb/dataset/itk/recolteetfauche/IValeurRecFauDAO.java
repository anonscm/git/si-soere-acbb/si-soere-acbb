/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.ValeurRecFau;

/**
 * The Interface IValeurSemisDAO.
 */
public interface IValeurRecFauDAO extends IDAO<ValeurRecFau> {
}
