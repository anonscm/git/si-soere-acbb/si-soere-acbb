package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractBuildHelper;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.IPhytosanitaireDAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BuildTravailDusolHelper.
 */
public class BuildPhytosanitaireHelper extends AbstractBuildHelper<LineRecord> {

    /**
     * The travaux du sol.
     */
    Map<Parcelle, Map<LocalDate, Map<Integer, Phytosanitaire>>> phytosanitaires = new HashMap();

    IPhytosanitaireDAO phytosanitaireDAO;

    /**
     * Builds the.
     *
     * @see org.inra.ecoinfo.acbb.dataset.itk.AbstractITKSRecorder.org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractBuildHelper#build()
     */
    @Override
    public void build() {
        for (final LineRecord line : this.lines) {
            final Phytosanitaire phytosanitaire = this.getOrCreatePhytosanitaire(line);
            if (phytosanitaire == null) {
                break;
            }
            try {
                ValeurPhytosanitaire valeur = null;
                for (ACBBVariableValue<? extends IValeurQualitative> variableValue : line
                        .getVariablesValues()) {
                    RealNode realNode = getRealNodeForSequenceAndVariable(variableValue.getDatatypeVariableUnite());
                    if (Strings.isNullOrEmpty(variableValue.getValue())
                            && variableValue.getValeurQualitative() == null
                            && variableValue.getValeursQualitatives() == null) {
                        continue;
                    }
                    if (variableValue.isValeurQualitative()
                            && variableValue.getValeursQualitatives() != null) {
                        valeur = new ValeurPhytosanitaire(variableValue.getValeursQualitatives(),
                                realNode, phytosanitaire);
                    } else if (variableValue.isValeurQualitative()) {
                        valeur = new ValeurPhytosanitaire(
                                (ListeItineraire) variableValue.getValeurQualitative(),
                                realNode, phytosanitaire);
                    } else {
                        valeur = new ValeurPhytosanitaire(
                                Float.parseFloat(variableValue.getValue()),
                                realNode, phytosanitaire);
                    }
                    phytosanitaire.getValeursPhytosanitaire().add(valeur);
                }
                this.phytosanitaireDAO.saveOrUpdate(phytosanitaire);
            } catch (final PersistenceException e) {
                LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("mesureTravailDuSolDAO.saveOrUpdate", e);
            }
        }
    }

    /**
     * Instantiates a new builds the fertilisant helper.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines {@link AbstractLineRecord}
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(IRequestPropertiesITK) the request properties itk
     */
    public void build(final VersionFile version, final List<LineRecord> lines,
                      final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK) {
        this.update(version, lines, errorsReport, requestPropertiesITK);
        this.build();
    }

    Phytosanitaire getOrCreatePhytosanitaire(final LineRecord line) {
        VersionDeTraitement versionDeTraitement = line.getVersionDeTraitement();
        Parcelle parcelle = line.getParcelle();
        return phytosanitaires
                .computeIfAbsent(parcelle, k -> new HashMap<>())
                .computeIfAbsent(line.getDate(), k -> new HashMap<>())
                .computeIfAbsent(line.getRang(), k -> createPhytosanitaire(line, versionDeTraitement, parcelle));
    }

    /**
     * @param phytosanitaireDAO the phytosanitaireDAO to set
     */
    public void setPhytosanitaireDAO(IPhytosanitaireDAO phytosanitaireDAO) {
        this.phytosanitaireDAO = phytosanitaireDAO;
    }

    private Phytosanitaire createPhytosanitaire(LineRecord line, VersionDeTraitement versionDeTraitement, Parcelle parcelle) {
        this.getTempo(line, versionDeTraitement, parcelle);
        Phytosanitaire phytosanitaire = null;
        if (!line.getTempo().isInErrors()) {
            phytosanitaire = new Phytosanitaire(this.version, line.getObservation(),
                    line.getDate(), line.getRotationNumber(), line.getAnneeNumber(),
                    line.getPeriodeNumber());
            phytosanitaire.setSuiviParcelle(line.getSuiviParcelle());
            phytosanitaire.setRang(line.getRang());
            phytosanitaire.setProduitPhytosanitaire(line.getProduitPhytosanitaire());
        }
        return phytosanitaire;
    }
}
