package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * The Class FluxMeteoOutputDisplayByRow.
 */
public class ITKOutputDisplay extends ITKOutputsBuildersResolver {

    /**
     * The flux chambre build output by row @link(IOutputBuilder).
     */
    IOutputBuilder semisBuildOutput;
    IOutputBuilder wsolBuildOutput;
    IOutputBuilder paturageBuildOutput;
    IOutputBuilder recFauBuildOutput;
    IOutputBuilder aiBuildOutput;
    IOutputBuilder fneBuildOutput;
    IOutputBuilder fertilisantBuildOutput;
    IOutputBuilder phytosanitaireBuildOutput;
    IOutputBuilder itkRequestReminder;

    /**
     * Instantiates a new flux meteo output display by row.
     */
    public ITKOutputDisplay() {
        super();
    }

    /**
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap,
                                          Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.
     *                           FluxMeteoOutputsBuildersResolver
     *                           #buildOutput(org.inra.ecoinfo.extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        StatusBar statusBar = (StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        statusBar.setProgress(50);
        this.itkRequestReminder.buildOutput(parameters);
        statusBar.setProgress(55);
        this.semisBuildOutput.buildOutput(parameters);
        statusBar.setProgress(61);
        this.wsolBuildOutput.buildOutput(parameters);
        statusBar.setProgress(66);
        this.paturageBuildOutput.buildOutput(parameters);
        statusBar.setProgress(72);
        this.recFauBuildOutput.buildOutput(parameters);
        statusBar.setProgress(77);
        this.aiBuildOutput.buildOutput(parameters);
        statusBar.setProgress(82);
        this.fneBuildOutput.buildOutput(parameters);
        statusBar.setProgress(88);
        this.fertilisantBuildOutput.buildOutput(parameters);
        statusBar.setProgress(94);
        this.phytosanitaireBuildOutput.buildOutput(parameters);
        statusBar.setProgress(100);
        return super.buildOutput(parameters);
    }

    /**
     * @param metadatasMap
     * @return
     */
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        return null;
    }

    /**
     * @param aiBuildOutput the aiBuildOutput to set
     */
    public void setAiBuildOutput(IOutputBuilder aiBuildOutput) {
        this.aiBuildOutput = aiBuildOutput;
    }

    /**
     * @param fertilisantBuildOutput the fertilisantBuildOutput to set
     */
    public void setFertilisantBuildOutput(IOutputBuilder fertilisantBuildOutput) {
        this.fertilisantBuildOutput = fertilisantBuildOutput;
    }

    /**
     * @param fneBuildOutput the fneBuildOutput to set
     */
    public void setFneBuildOutput(IOutputBuilder fneBuildOutput) {
        this.fneBuildOutput = fneBuildOutput;
    }

    /**
     * @param itkRequestReminder
     */
    public void setItkRequestReminder(IOutputBuilder itkRequestReminder) {
        this.itkRequestReminder = itkRequestReminder;
    }

    /**
     * @param paturageBuildOutput
     */
    public void setPaturageBuildOutput(IOutputBuilder paturageBuildOutput) {
        this.paturageBuildOutput = paturageBuildOutput;
    }

    /**
     * @param phytosanitaireBuildOutput the phytosanitaireBuildOutput to set
     */
    public void setPhytosanitaireBuildOutput(IOutputBuilder phytosanitaireBuildOutput) {
        this.phytosanitaireBuildOutput = phytosanitaireBuildOutput;
    }

    /**
     * @param recFauBuildOutput
     */
    public void setRecFauBuildOutput(IOutputBuilder recFauBuildOutput) {
        this.recFauBuildOutput = recFauBuildOutput;
    }

    /**
     * Sets the flux chambre build output by row.
     *
     * @param semisBuildOutput
     */
    public final void setSemisBuildOutput(final IOutputBuilder semisBuildOutput) {
        this.semisBuildOutput = semisBuildOutput;
    }

    /**
     * @param wSolBuildOutput
     */
    public final void setWsolBuildOutput(final IOutputBuilder wSolBuildOutput) {
        this.wsolBuildOutput = wSolBuildOutput;
    }
}
