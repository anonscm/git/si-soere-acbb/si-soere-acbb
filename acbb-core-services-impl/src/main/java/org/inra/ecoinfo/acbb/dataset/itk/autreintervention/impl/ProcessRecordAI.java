package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * The Class ProcessRecordAI.
 * <p>
 * process the record of a file
 */
public class ProcessRecordAI extends AbstractProcessRecordITK {

    /**
     * The Constant serialVersionUID.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant SEMIS_BUNDLE_NAME.
     */
    static final String SEMIS_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.autreintervention.messages";

    /**
     * @param version
     * @param lines
     * @param errorsReport
     * @param requestPropertiesITK
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractProcessRecordITK#buildNewLines(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.util.List, org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK)
     */
    @SuppressWarnings(value = "unchecked")
    @Override
    protected void buildNewLines(final VersionFile version,
                                 final List<? extends AbstractLineRecord> lines, final ErrorsReport errorsReport,
                                 final IRequestPropertiesITK requestPropertiesITK) throws PersistenceException {
        final BuildAIHelper instance = ApplicationContextHolder.getContext().getBean(
                BuildAIHelper.class);
        instance.build(version, (List<AILineRecord>) lines, errorsReport, requestPropertiesITK);
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @param versionFile
     * @param fileEncoding
     * @param requestProperties
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile localVersionFile = versionFile;
        super.processRecord(parser, localVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        IntervalDate intervalDate = null;
        try {
            intervalDate = getIntervalDateForVersion(versionFile);
        } catch (BadExpectedValueException ex) {
            errorsReport.addErrorMessage("cant get interval for version", ex);
        }
        try {
            long lineCount = 1;
            final List<DatatypeVariableUniteACBB> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    ((IRequestPropertiesITK) requestProperties).getValueColumns(),
                    datasetDescriptor);
            lineCount = datasetDescriptor.getEnTete();
            localVersionFile = this.versionFileDAO.merge(localVersionFile);
            final List<AILineRecord> lines = new LinkedList();
            lineCount = this.readLines(parser, requestProperties, datasetDescriptor, errorsReport,
                    lineCount, dbVariables, lines, intervalDate);
            if (errorsReport.hasErrors()) {
                logger.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            } else {
                this.buildNewLines(localVersionFile, lines, errorsReport,
                        (IRequestPropertiesITK) requestProperties);
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Read lines.
     *
     * @param parser
     * @param requestProperties
     * @param datasetDescriptor
     * @param errorsReport
     * @param lineCount
     * @param dbVariables
     * @param lines
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link{CSVParser the parser
     * @link{IRequestPropertiesACBB the request properties
     * @link{DatasetDescriptorACBB the dataset descriptor
     * @link{ErrorsReport the errors report
     * @link{long the line count
     * @link{java.util.List<VariableACBB> the db variables
     * @link{java.util.List<AILineRecord> the lines
     */
    long readLines(final CSVParser parser, final IRequestPropertiesACBB requestProperties,
                   final DatasetDescriptorACBB datasetDescriptor, final ErrorsReport errorsReport,
                   long lineCount, final List<DatatypeVariableUniteACBB> dbVariables, final List<AILineRecord> lines, IntervalDate intervalDate)
            throws IOException {
        long localLineCount = lineCount;
        String[] values = null;
        while ((values = parser.getLine()) != null) {
            localLineCount++;
            final CleanerValues cleanerValues = new CleanerValues(values);
            final AILineRecord lineRecord = new AILineRecord(localLineCount);
            int genericIndex = this.getGenericColumns(errorsReport, localLineCount, cleanerValues,
                    lineRecord, datasetDescriptor, (IRequestPropertiesITK) requestProperties, intervalDate);
            for (final Entry<Integer, Column> entry : ((IRequestPropertiesITK) requestProperties)
                    .getValueColumns().entrySet()) {
                if (entry.getKey() >= values.length) {
                    break;
                }
                final String value = values[entry.getKey()];
                if (entry.getKey() < genericIndex || Strings.isNullOrEmpty(value)) {
                    continue;
                }
                ACBBVariableValue<ListeItineraire> variableValue = null;
                final DatatypeVariableUniteACBB dvu = dbVariables.get(entry.getKey());
                variableValue = this.getVariableValue(errorsReport, lineCount, entry.getKey(),
                        dvu, entry.getValue(), value);
                lineRecord.getACBBVariablesValues().add(variableValue);
            }
            if (!errorsReport.hasErrors()) {
                lines.add(lineRecord);
            }
        }
        return localLineCount;
    }
}
