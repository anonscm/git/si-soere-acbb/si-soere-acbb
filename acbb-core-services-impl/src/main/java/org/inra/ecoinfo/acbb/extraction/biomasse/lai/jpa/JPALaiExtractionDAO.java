/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.lai.jpa;


import org.inra.ecoinfo.acbb.dataset.biomasse.jpa.AbstractJPABiomasseDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.Lai;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.ValeurLai;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.ValeurLai_;
import org.inra.ecoinfo.acbb.synthesis.lai.SynthesisValue;

import javax.persistence.metamodel.SingularAttribute;

/**
 * The Class JPATravailDuSolDAO.
 */
public class JPALaiExtractionDAO extends AbstractJPABiomasseDAO<Lai, ValeurLai> {

    /**
     * @return
     */
    @Override
    protected Class getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     * @return
     */
    @Override
    protected Class<ValeurLai> getValeurBiomassClass() {
        return ValeurLai.class;
    }

    /**
     * @return
     */
    @Override
    protected SingularAttribute<ValeurLai, Lai> getMesureAttribute() {
        return ValeurLai_.lai;
    }

    /**
     * @return
     */
    @Override
    protected Class<Lai> getBiomassClass() {
        return Lai.class;
    }
}
