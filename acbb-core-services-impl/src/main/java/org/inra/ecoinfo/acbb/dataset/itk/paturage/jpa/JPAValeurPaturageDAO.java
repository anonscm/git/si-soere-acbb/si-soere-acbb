/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IValeurPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;

/**
 * The Class JPAValeurSemisDAO.
 */
public class JPAValeurPaturageDAO extends AbstractJPADAO<ValeurPaturage> implements
        IValeurPaturageDAO {

}
