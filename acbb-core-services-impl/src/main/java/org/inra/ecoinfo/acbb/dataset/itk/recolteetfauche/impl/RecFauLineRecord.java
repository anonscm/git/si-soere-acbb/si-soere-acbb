package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import java.time.LocalDate;
import java.util.List;

/**
 * The Class LineRecord.
 * <p>
 * one line of record
 */
public class RecFauLineRecord extends AbstractLineRecord<RecFauLineRecord, ListeItineraire> {

    LocalDate endDate;
    RotationDescriptor rotation;
    int recFauPassage;

    /**
     * Instantiates a new line record.
     *
     * @param lineCount long the line count
     * @link(SuiviParcelle) the suivi parcelle
     * @link(SuiviParcelle) the suivi parcelle
     */
    public RecFauLineRecord(final long lineCount) {
        super(lineCount);
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return this.endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public final void setEndDate(final LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the recFauPassage
     */
    protected int getRecFauPassage() {
        return this.recFauPassage;
    }

    /**
     * @return the rotation
     */
    public RotationDescriptor getRotation() {
        return this.rotation;
    }

    /**
     * Gets the treatment affichage.
     *
     * @return the treatment affichage
     */
    public String getTreatmentAffichage() {
        String returnString = org.apache.commons.lang.StringUtils.EMPTY;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null
                && this.getTempo().getVersionDeTraitement().getTraitementProgramme() != null) {
            returnString = this.getTempo().getVersionDeTraitement().getTraitementProgramme()
                    .getAffichage();
        }
        return returnString;
    }

    /**
     * Gets the treatment version.
     *
     * @return the treatment version
     */
    public int getTreatmentVersion() {
        int returnInt = -1;
        if (this.getTempo() != null && this.getTempo().getVersionDeTraitement() != null) {
            returnInt = this.getTempo().getVersionDeTraitement().getVersion();
        }
        return returnInt;
    }

    /**
     * @return
     */
    public boolean isRotation() {
        return this.rotation != null && this.rotation.getRotation() != null;
    }

    /**
     * @param variablesValues the variablesValues to set
     */
    public final void setACBBVariablesValues(
            final List<ACBBVariableValue<ListeItineraire>> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     * @param passageString
     * @return
     */
    public boolean setRecFauPassage(String passageString) {
        boolean isNumber = true;
        try {
            this.recFauPassage = Integer.parseInt(passageString);
        } catch (NumberFormatException e) {
            isNumber = false;
        }
        return isNumber;
    }

    /**
     * @param rotation
     */
    public final void setRotationDescriptor(final RotationDescriptor rotation) {
        this.rotation = rotation;
    }
}
