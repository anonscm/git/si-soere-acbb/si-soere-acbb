/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AI;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.Fertilisant;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.Phytosanitaire;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFau;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.TravailDuSol;
import org.inra.ecoinfo.acbb.extraction.itk.IITKDatasetManager;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.itk.IInterventionExtractionDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;

import java.time.LocalDate;
import java.util.*;

/**
 * The Class FluxMeteoDatasetManager.
 */
public class ITKDatasetManager extends DefaultDatasetManager implements IITKDatasetManager {

    static final String SITE_SEMIS_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/semis/*:datatype)";
    static final String VARIABLE_SEMIS_PRIVILEGE_PATTERN = "E->(%s:site,*,*/semis:datatype,%s:variable)";
    static final String SITE_WSOL_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/travail_du_sol/*:datatype)";
    static final String VARIABLE_WSOL_PRIVILEGE_PATTERN = "E->(%s:site,*,*/travail_du_sol:datatype,%s:variable)";
    static final String SITE_PATURAGE_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/paturage/*:datatype)";
    static final String VARIABLE_PATURAGE_PRIVILEGE_PATTERN = "E->(%s:site,*,*/paturage:datatype,%s:variable)";
    static final String SITE_REC_FAU_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/recolteetfauche/*:datatype)";
    static final String VARIABLE_REC_FAU_PRIVILEGE_PATTERN = "E->(%s:site,*,*/recolteetfauche:datatype,%s:variable)";
    static final String SITE_AI_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/autreintervention/*:datatype)";
    static final String VARIABLE_AI_PRIVILEGE_PATTERN = "E->(%s:site,*,*/autreintervention:datatype,%s:variable)";
    static final String SITE_FNE_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/fauchenonexportee/*:datatype)";
    static final String VARIABLE_FNE_PRIVILEGE_PATTERN = "E->(%s:site,*,*/fauchenonexportee:datatype,%s:variable)";
    static final String SITE_FERT_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/fertilisant/*:datatype)";
    static final String VARIABLE_FERT_PRIVILEGE_PATTERN = "E->(%s:site,*,*/fertilisant:datatype,%s:variable)";
    static final String SITE_PHYT_PRIVILEGE_PATTERN = "E->(%s:agroecosysteme,%s:site,*,*/phytosanitaire/*:datatype)";
    static final String VARIABLE_PHYT_PRIVILEGE_PATTERN = "E->(%s:site,*,*/phytosanitaire:datatype,%s:variable)";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    IITKExtractionDAO itkExtractionDAO;
    Map<String, IInterventionExtractionDAO> extractionsDAO = new HashMap();

    /**
     *
     */
    public ITKDatasetManager() {
        super();
    }

    /**
     * @param intervalsDate
     * @return
     */
    @Override
    public Collection<? extends TraitementProgramme> getAvailableTraitements(
            List<IntervalDate> intervalsDate) {
        final Set<TraitementProgramme> availablesTraitements = new HashSet();
        for (Map.Entry<String, IInterventionExtractionDAO> entry : extractionsDAO.entrySet()) {
            IInterventionExtractionDAO interventionDAO = entry.getValue();
            availablesTraitements.addAll(interventionDAO.getAvailablesTraitementsForPeriode(intervalsDate, policyManager.getCurrentUser()));
        }
        return availablesTraitements;
    }

    /**
     * @param linkedList
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByTreatmentAndDatesInterval(List<TraitementProgramme> linkedList, List<IntervalDate> intervals) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        for (Map.Entry<String, IInterventionExtractionDAO> entry : extractionsDAO.entrySet()) {
            String datatype = entry.getKey();
            IInterventionExtractionDAO interventionDAO = entry.getValue();
            variablesByDatatype.put(datatype, interventionDAO.getAvailableVariableByTraitement(linkedList, intervals, policyManager.getCurrentUser()));
        }
        return variablesByDatatype;

    }

    /**
     * @param parcelle
     * @param date
     * @param availablesCouverts
     * @return
     */
    @Override
    public VegetalPeriode getCouvertVegetal(Parcelle parcelle, LocalDate date,
                                            Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts) {
        final Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> couverts = availablesCouverts;
        SortedMap<LocalDate, VegetalPeriode> mapCouverts = couverts.get(parcelle);
        if (mapCouverts == null || mapCouverts.isEmpty()) {
            return new VegetalPeriode();
        }
        mapCouverts = ((TreeMap<LocalDate, VegetalPeriode>) mapCouverts).headMap(date, true);
        VegetalPeriode vegetalPeriode = !mapCouverts.isEmpty() ? mapCouverts.get(mapCouverts
                .lastKey()) : null;
        if (vegetalPeriode == null) {
            return new VegetalPeriode();
        }
        LocalDate dateDeFin = vegetalPeriode.getEndDate();
        if (!dateDeFin.isAfter(date)) {
            return new VegetalPeriode();
        }
        return vegetalPeriode;
    }

    /**
     * @param aiExtractionDAO
     */
    public void setAiExtractionDAO(IInterventionExtractionDAO<AI> aiExtractionDAO) {
        this.extractionsDAO.put(AI.AI_DISCRIMINATOR, aiExtractionDAO);
    }

    /**
     * @param fertilisantExtractionDAO
     */
    public void setFertilisantExtractionDAO(
            IInterventionExtractionDAO<Fertilisant> fertilisantExtractionDAO) {
        this.extractionsDAO.put(Fertilisant.FERTILISANT_DISCRIMINATOR, fertilisantExtractionDAO);
    }

    /**
     * @param fneExtractionDAO
     */
    public void setFneExtractionDAO(IInterventionExtractionDAO<Fne> fneExtractionDAO) {
        this.extractionsDAO.put(Fne.FNE_DISCRIMINATOR, fneExtractionDAO);
    }

    /**
     * @param itkExtractionDAO the itkExtractionDAO to set
     */
    public void setItkExtractionDAO(IITKExtractionDAO itkExtractionDAO) {
        this.itkExtractionDAO = itkExtractionDAO;
    }

    /**
     * @param paturageExtractionDAO
     */
    public void setPaturageExtractionDAO(IInterventionExtractionDAO<Paturage> paturageExtractionDAO) {
        this.extractionsDAO.put(Paturage.PATURAGE_DESCRIMINATOR, paturageExtractionDAO);
    }

    /**
     * @param phytosanitaireExtractionDAO
     */
    public void setPhytosanitaireExtractionDAO(
            IInterventionExtractionDAO<Phytosanitaire> phytosanitaireExtractionDAO) {
        this.extractionsDAO.put(Phytosanitaire.PHYTOSANITAIRE_DESCRIMINATOR, phytosanitaireExtractionDAO);
    }

    /**
     * @param recFauExtractionDAO
     */
    public void setRecFauExtractionDAO(IInterventionExtractionDAO<RecFau> recFauExtractionDAO) {
        this.extractionsDAO.put(RecFau.RECOLTE_ET_FAUCHE_DESRIMINATOR, recFauExtractionDAO);
    }

    /**
     * @param semisDAO the semisDAO to set
     */
    public void setSemisExtractionDAO(IInterventionExtractionDAO<Semis> semisDAO) {
        this.extractionsDAO.put(Semis.SEMIS_DESCRIMINATOR, semisDAO);
    }

    /**
     * @param wsolExtractionDAO the wsolExtractionDAO to set
     */
    public void setWsolExtractionDAO(IInterventionExtractionDAO<TravailDuSol> wsolExtractionDAO) {
        this.extractionsDAO.put(TravailDuSol.TRAVAIL_DU_SOL_DESCRIMINATOR, wsolExtractionDAO);
    }
}
