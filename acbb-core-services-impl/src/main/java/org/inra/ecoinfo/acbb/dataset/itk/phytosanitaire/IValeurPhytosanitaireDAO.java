/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity.ValeurPhytosanitaire;

/**
 * The Interface IValeurTravailDuSolDAO.
 */
public interface IValeurPhytosanitaireDAO extends IDAO<ValeurPhytosanitaire> {
}
