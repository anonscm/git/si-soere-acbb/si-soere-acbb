package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.IversionDeTraitementRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class AbstractBuildHelper.
 *
 * @param <T> extends {@link AbstractLineRecord} the generic type
 */
public abstract class AbstractBuildHelper<T extends ILineRecord<T, ListeItineraire>> {

    /**
     *
     */
    static protected final Logger LOGGER = LoggerFactory.getLogger(AbstractBuildHelper.class);

    /**
     * The lines .
     */
    protected List<T> lines;

    /**
     * The errors report {@link ErrorsReport}.
     */
    protected ErrorsReport errorsReport;

    /**
     * The errors report {@link InfoReport}.
     */
    protected InfoReport infoReport = new InfoReport();

    /**
     * The version {@link VersionFile}.
     */
    protected VersionFile version;

    /**
     * The request properties itk @link(IRequestPropertiesITK).
     */
    @Transient
    protected IRequestPropertiesITK requestPropertiesITK;

    /**
     * The version de traitement realisee factory
     *
     * @link(IversionDeTraitementRealiseeFactory<T>).
     */
    @Transient
    protected IversionDeTraitementRealiseeFactory<T> versionDeTraitementRealiseeFactory;

    /**
     * The datatype unite variable acbbdao @link(IDatatypeVariableUniteACBBDAO).
     */
    @Transient
    protected IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;

    /**
     *
     */
    protected Map<Variable, RealNode> dvus = new HashMap();

    /**
     * The versionDeTraitementDAO @link(IVersionDeTraitementDAO).
     */
    @Transient
    protected IVersionDeTraitementDAO versionDeTraitementDAO;
    /**
     * The datatype name @link(String).
     */
    String datatypeName;

    /**
     * Builds the.
     *
     * @throws PersistenceException
     */
    public abstract void build() throws PersistenceException;

    /**
     * @return the requestPropertiesITK
     */
    @Transient
    protected IRequestPropertiesITK getRequestPropertiesITK() {
        return this.requestPropertiesITK;
    }

    /**
     * Gets the versiontraitement realise.
     *
     * @param line
     * @param parcelle
     * @param versionTraitement
     * @return the versiontraitement realise {@link AbstractLineRecord} the line
     * {@link VersionDeTraitement} the version traitement
     * @link(T) the line
     * @link(VersionDeTraitement) the version traitement
     */
    protected Tempo getTempo(final T line, VersionDeTraitement versionTraitement,
                             final Parcelle parcelle) {
        try {
            versionTraitement = versionDeTraitementDAO.merge(versionTraitement);
        } catch (PersistenceException ex) {
            final String message = "can't merge versionTraitement";
            this.errorsReport.addErrorMessage(message);
            LOGGER.error(message);
        }
        return this.versionDeTraitementRealiseeFactory.getTempo(line, versionTraitement, parcelle,
                this.errorsReport, this.infoReport);
    }

    /**
     * Sets the datatype name.
     *
     * @param datatypeName the datatypeName to set
     */
    public final void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     * Sets the datatype unite variable acbbdao.
     *
     * @param datatypeVariableUniteACBBDAO the datatypeVariableUniteACBBDAO to
     *                                     set
     */
    public final void setDatatypeVariableUniteACBBDAO(
            final IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO) {
        this.datatypeVariableUniteACBBDAO = datatypeVariableUniteACBBDAO;
    }

    /**
     * @param versionDeTraitementDAO the versionDeTraitementDAO to set
     */
    public final void setVersionDeTraitementDAO(final IVersionDeTraitementDAO versionDeTraitementDAO) {
        this.versionDeTraitementDAO = versionDeTraitementDAO;
    }

    /**
     * Sets the version de traitement realisee factory.
     *
     * @param versionDeTraitementRealiseeFactory the
     *                                           versionDeTraitementRealiseeFactory to set
     */
    public final void setVersionDeTraitementRealiseeFactory(
            final IversionDeTraitementRealiseeFactory<T> versionDeTraitementRealiseeFactory) {
        this.versionDeTraitementRealiseeFactory = versionDeTraitementRealiseeFactory;
    }

    /**
     * Instantiates a new abstract build helper.
     *
     * @param version              the version {@link VersionFile}
     * @param lines                the lines {@link List}
     * @param errorsReport         the errors report {@link ErrorsReport}
     * @param requestPropertiesITK
     * @link(IRequestPropertiesITK) the request properties itk
     * @link(IRequestPropertiesITK) the request properties itk
     */
    public void update(final VersionFile version, final List<T> lines,
                       final ErrorsReport errorsReport, final IRequestPropertiesITK requestPropertiesITK) {
        this.version = version;
        this.lines = lines;
        this.errorsReport = errorsReport;
        this.requestPropertiesITK = requestPropertiesITK;
        this.dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(version.getDataset().getRealNode());
    }

    /**
     * @param dvu
     * @return
     */
    protected RealNode getRealNodeForSequenceAndVariable(DatatypeVariableUniteACBB dvu) {

        return dvus.get(dvu.getVariable());
    }
}
