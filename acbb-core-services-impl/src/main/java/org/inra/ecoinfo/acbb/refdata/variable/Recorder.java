/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.acbb.refdata.variable;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariableACBB> {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    /**
     * The variable acbbdao.
     */
    IVariableACBBDAO variableACBBDAO;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    Properties propertiesNomEN;

    /**
     * The properties description en @link(Properties).
     */
    Properties propertiesDescriptionEN;

    /**
     * Creates the or update variable.
     *
     * @param nom
     * @param affichage
     * @param definition
     * @param isQualitativeString
     * @param dbVariable
     * @throws PersistenceException the persistence exception
     * @link(String) the nom
     * @link(String) the affichage
     * @link(String) the definition
     * @link(VariableACBB) the db variable
     * @link(String) the nom
     * @link(String) the affichage
     * @link(String) the definition
     * @link(VariableACBB) the db variable
     */
    void createOrUpdateVariable(final String nom, final String affichage, final String definition,
                                final String isQualitativeString, final VariableACBB dbVariable)
            throws PersistenceException {

        boolean isQualitative = false;
        if (Boolean.TRUE.toString().equalsIgnoreCase(isQualitativeString)) {
            isQualitative = true;
        }
        if (dbVariable == null) {
            final VariableACBB variable = new VariableACBB(nom, definition, affichage,
                    isQualitative);
            this.variableACBBDAO.saveOrUpdate(variable);
        } else {
            dbVariable.setAffichage(affichage);
            dbVariable.setDefinition(definition);
            dbVariable.setIsQualitative(isQualitative);
            this.variableACBBDAO.saveOrUpdate(dbVariable);
        }
    }

    /**
     * Delete record.
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.
     *                           Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.variableACBBDAO.remove(this.variableACBBDAO.getByCode(variableCode)
                        .orElseThrow(() -> new BusinessException("bad variable")));
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<VariableACBB> getAllElements() {
        List<VariableACBB> all = this.variableACBBDAO.getAllVariableACBB(Variable.class);
        Collections.sort(all);
        return all;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param variable
     * @return the new line model grid metadata
     * @link(VariableACBB) the variable
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final VariableACBB variable) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomFR.getProperty(variable.getName(), variable.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesNomEN.getProperty(variable.getName(), variable.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable
                                .getAffichage(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable
                                .getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : this.propertiesDescriptionEN.getProperty(variable.getDefinition(), variable.getDefinition()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null ? Boolean.FALSE.toString() : variable
                        .getIsQualitative(), new String[]{Boolean.FALSE.toString(),
                        Boolean.TRUE.toString()}, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<VariableACBB> initModelGridMetadata() {

        this.propertiesNomFR = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        this.propertiesNomEN = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        this.propertiesDescriptionEN = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), "definition", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist variable.
     *
     * @param errorsReport
     * @param nom
     * @param affichage
     * @param definition
     * @param isQualitativeString
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the nom
     * @link(String) the affichage
     * @link(String) the definition
     * @link(ErrorsReport) the errors report
     * @link(String) the nom
     * @link(String) the affichage
     * @link(String) the definition
     */
    void persistVariable(final ErrorsReport errorsReport, final String nom, final String affichage,
                         final String definition, final String isQualitativeString) throws PersistenceException {
        final VariableACBB dbVariable = (VariableACBB) this.variableACBBDAO.getByCode(nom)
                .orElse(null);
        this.createOrUpdateVariable(nom, affichage, definition, isQualitativeString, dbVariable);
    }

    /**
     * Process record.
     *
     * @param parser   the parser
     * @param file     the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(VariableACBB.class));
                final String nom = tokenizerValues.nextToken();
                final String affichage = tokenizerValues.nextToken();
                final String definition = tokenizerValues.nextToken();
                final String isQualitativeString = values.length > 4 ? tokenizerValues.nextToken()
                        : "false";
                this.persistVariable(errorsReport, nom, affichage, definition, isQualitativeString);
                if (parser.getLastLineNumber() % 50 == 0) {
                    variableACBBDAO.flush();
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);

            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the variable acbbdao.
     *
     * @param variableDAO the new variable acbbdao
     */
    public final void setVariableACBBDAO(final IVariableACBBDAO variableDAO) {
        this.variableACBBDAO = variableDAO;
    }
}
