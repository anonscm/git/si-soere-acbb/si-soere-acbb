/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.Fne;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.ValeurFne_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationFneDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     * @throws PersistenceException
     */

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        deleteValeur(version);
        deleteBiomasse(version);
    }

    private void deleteValeur(VersionFile versionFile) {
        CriteriaDelete<ValeurFne> deletevaleur = builder.createCriteriaDelete(ValeurFne.class);
        Root<ValeurFne> v = deletevaleur.from(ValeurFne.class);
        Subquery<Fne> subqueryIntervention = deletevaleur.subquery(Fne.class);
        Root<Fne> intervention = subqueryIntervention.from(Fne.class);
        Path<VersionFile> version = intervention.get(AbstractIntervention_.version);
        subqueryIntervention
                .select(intervention)
                .where(builder.equal(version, versionFile));
        deletevaleur
                .where(v.get(ValeurFne_.intervention).in(subqueryIntervention));
        delete(deletevaleur);
    }

    private void deleteBiomasse(VersionFile versionFile) {
        CriteriaDelete<Fne> deleteIntervention = builder.createCriteriaDelete(Fne.class);
        Root<Fne> intervention = deleteIntervention.from(Fne.class);
        Path<VersionFile> version = intervention.get(AbstractIntervention_.version);
        deleteIntervention
                .where(builder.equal(version, versionFile));
        delete(deleteIntervention);
    }
}
