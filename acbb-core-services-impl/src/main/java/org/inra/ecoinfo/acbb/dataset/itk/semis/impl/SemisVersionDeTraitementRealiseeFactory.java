package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultVersionDeTraitementRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating SemisVersionDeTraitementRealisee objects.
 */
public class SemisVersionDeTraitementRealiseeFactory extends
        DefaultVersionDeTraitementRealiseeFactory<LineRecord> {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.semis.impl.messages";
    /**
     * The Constant PROPERTY_MSG_MISSING_MAIN_CROP @link(String).
     */
    static final String PROPERTY_MSG_MISSING_MAIN_CROP = "PROPERTY_MSG_MISSING_MAIN_CROP";
    /**
     * The Constant PROPERTY_MSG_BAD_ROTATION_NUMBER @link(String).
     */
    static final String PROPERTY_MSG_BAD_ROTATION_NUMBER = "PROPERTY_MSG_BAD_ROTATION_NUMBER";
    private static final Logger LOGGER = LoggerFactory.getLogger(SemisVersionDeTraitementRealiseeFactory.class
            .getName());

    /**
     * Gets the new rotation.
     *
     * @param rotationNumber
     * @param line
     * @return the new rotation
     * @link(int) the line number
     * @link(LineRecord) the line
     */
    VersionTraitementRealisee getNewRotation(final int rotationNumber, final LineRecord line) {
        final VersionTraitementRealisee versionTraitementRealisee = new VersionTraitementRealisee(
                rotationNumber, line.getTempo().getVersionDeTraitement(), line.getTempo()
                .getParcelle());
        return versionTraitementRealisee;
    }

    /**
     * @param line
     * @param infoReport
     * @param errorsReport
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.impl.DefaultVersionDeTraitementRealiseeFactory#getOrCreateVersiontraitementRealise(org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord,
     * org.inra.ecoinfo.acbb.dataset.impl.ErrorsReport,
     * org.inra.ecoinfo.acbb.dataset.impl.InfoReport)
     */
    @Override
    public VersionTraitementRealisee getOrCreateVersiontraitementRealise(final LineRecord line,
                                                                         final ErrorsReport errorsReport, final InfoReport infoReport) {
        final Tempo tempo = line.getTempo();
        assert tempo.getVersionDeTraitement() != null : "la version de traitement ne doit pas être nulle";
        VersionTraitementRealisee versionDeTraitementRealisee = tempo
                .getVersionTraitementRealisee();
        final RotationDescriptor rotation = this.getRotationDescriptor(line);
        line.setRotationDescriptor(rotation);
        if (line.isRotation()) {
            if (rotation.isPossibleCouvert()) {
                versionDeTraitementRealisee = versionDeTraitementRealisee != null ? versionDeTraitementRealisee
                        : this.getNewRotation(line.getRotationNumber(), line);
            } else if (versionDeTraitementRealisee == null) {
                errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                        SemisVersionDeTraitementRealiseeFactory.BUNDLE_NAME,
                        SemisVersionDeTraitementRealiseeFactory.PROPERTY_MSG_MISSING_MAIN_CROP),
                        line.getOriginalLineNumber(), line.getAnneeNumber(), line
                                .getRotationNumber(), line.getTreatmentVersion(), line
                                .getTreatmentAffichage(),
                        rotation != null && rotation.getCouverts() != null ? rotation.getCouverts()
                                .size() : 1));
            }
        } else if (line.getRotationNumber() == 0) {
            versionDeTraitementRealisee = versionDeTraitementRealisee != null ? versionDeTraitementRealisee
                    : this.getNewRotation(0, line);
        } else {
            errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    SemisVersionDeTraitementRealiseeFactory.BUNDLE_NAME,
                    SemisVersionDeTraitementRealiseeFactory.PROPERTY_MSG_BAD_ROTATION_NUMBER), line
                    .getOriginalLineNumber(), line.getRotationNumber(), line
                    .getTreatmentAffichage()));
        }
        try {
            if (versionDeTraitementRealisee != null) {
                this.versionDeTraitementRealiseeDAO.saveOrUpdate(versionDeTraitementRealisee);
            }
        } catch (final PersistenceException e) {
            errorsReport
                    .addErrorMessage(RecorderACBB
                            .getACBBMessage(RecorderACBB.PROPERTY_MSG_UNKNOWN_PUBLISH_PERSISTENCE_EXCEPTION));
        }
        tempo.setVersionTraitementRealisee(versionDeTraitementRealisee);
        return versionDeTraitementRealisee;
    }

    /**
     * Gets the semis rotation descriptor.
     *
     * @param line
     * @return the semis rotation descriptor
     * @link(LineRecord) the line
     */
    RotationDescriptor getRotationDescriptor(final LineRecord line) {
        return new RotationDescriptor(line, line.getTempo().getVersionDeTraitement());
    }

    /**
     * Update annee realisee.
     *
     * @param anneeRealisee
     * @param line
     * @param errorsReport
     * @throws PersistenceException the persistence exception
     * @link(AnneeRealisee) the annee realisee
     * @link(LineRecord) the line
     * @link(ErrorsReport) the errors report
     */
    public void updateAnneeRealisee(final AnneeRealisee anneeRealisee, final LineRecord line,
                                    final ErrorsReport errorsReport) throws PersistenceException {
        ((SemisAnneeRealiseeFactory) this.anneeRealiseeFactory).updateAnneeRealisee(anneeRealisee,
                line, errorsReport);
    }
}
