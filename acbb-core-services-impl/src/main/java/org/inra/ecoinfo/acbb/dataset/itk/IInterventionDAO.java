package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The Interface IInterventionDAO.
 *
 * @param <T> the generic type
 */
public interface IInterventionDAO<T extends AbstractIntervention> extends IDAO<T> {

    /**
     * Delete all.
     *
     * @param interventionToDelete
     * @link{java.util.List the intervention to delete
     */
    void deleteAll(List<T> interventionToDelete);

    /**
     * @param parcelle
     * @param date
     * @param anneeNumber
     * @param valeur
     * @return
     */
    Optional<AbstractIntervention> getinterventionForDateAndType(Parcelle parcelle, LocalDate date,
                                                                 Integer anneeNumber, String valeur);

    /**
     * Gets the intervention for version.
     *
     * @param version
     * @return the intervention for version
     * @link{VersionFile the version
     */
    List<T> getInterventionForVersion(VersionFile version);

    /**
     * Gets the by n key.
     *
     * @param version
     * @param date
     * @return the by n key
     * @link{VersionFile the version
     * @link{Date the date
     */
    Optional<T> getByNKey(VersionFile version, LocalDate date);

}
