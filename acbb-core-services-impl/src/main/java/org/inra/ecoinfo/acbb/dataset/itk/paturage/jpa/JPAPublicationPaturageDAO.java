/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention_;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.ValeurPaturage_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * The Class JPAPublicationSemisDAO.
 */
public class JPAPublicationPaturageDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     * @param version
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        CriteriaDelete<ValeurPaturage> deleteQueryValeur = builder.createCriteriaDelete(ValeurPaturage.class);
        Root<ValeurPaturage> valeur = deleteQueryValeur.from(ValeurPaturage.class);
        Subquery<Paturage> subquery = deleteQueryValeur.subquery(Paturage.class);
        Root<Paturage> itk = subquery.from(Paturage.class);
        subquery.select(itk)
                .where(builder.equal(itk.get(AbstractIntervention_.version), version));
        deleteQueryValeur.where(valeur.get(ValeurPaturage_.paturage).in(subquery));
        delete(deleteQueryValeur);
        CriteriaDelete<Paturage> deleteQueryPaturage = builder.createCriteriaDelete(Paturage.class);
        itk = deleteQueryPaturage.from(Paturage.class);
        deleteQueryPaturage
                .where(builder.equal(itk.get(AbstractIntervention_.version), version));
        delete(deleteQueryPaturage);
    }
}
