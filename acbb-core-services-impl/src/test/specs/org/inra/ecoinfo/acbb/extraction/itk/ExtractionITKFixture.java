package org.inra.ecoinfo.acbb.extraction.itk;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.acbb.ACBBTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.ITKParameterVO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ACBBTransactionalTestFixtureExecutionListener.class})
public class ExtractionITKFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    ITraitementDAO traitementDAO;
    ISiteACBBDAO siteACBBDAO;
    ILocalizationManager localizationManager;
    IVariableACBBDAO variableACBBDAO;

    /**
     *
     */
    public ExtractionITKFixture() {
        super();
        MockitoAnnotations.initMocks(this);
        this.traitementDAO = ACBBTransactionalTestFixtureExecutionListener.traitementDAO;
        this.siteACBBDAO = ACBBTransactionalTestFixtureExecutionListener.siteACBBDAO;
        this.localizationManager = ACBBTransactionalTestFixtureExecutionListener.localizationManager;
        this.variableACBBDAO = ACBBTransactionalTestFixtureExecutionListener.variableACBBDAO;
    }

    /**
     * @param traitementsSiteCodes
     * @param variablesListNames
     * @param dateDedebut
     * @param datedeFin
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     */
    public String extract(String traitementsSiteCodes, String variablesListNames, String dateDedebut, String datedeFin, String filecomps, String commentaire, String affichage) throws BusinessException, PersistenceException, DateTimeException {
        List<SiteACBB> sites = new LinkedList();
        List<TraitementProgramme> traitementProgrammes = new LinkedList();
        for (String traitementSiteCode : traitementsSiteCodes.split(",")) {
            String[] listeTraitementSiteCode = traitementSiteCode.split("@");
            SiteACBB site = (SiteACBB) this.siteACBBDAO.getByPath(listeTraitementSiteCode[1]).orElse(null);
            sites.add(site);
            traitementProgrammes.add(this.traitementDAO.getByNKey(site.getId(), listeTraitementSiteCode[0]).orElse(null));
        }
        final Map<String, Object> metadatasMap = new HashMap();
        metadatasMap.put(TraitementProgramme.class.getSimpleName(), traitementProgrammes);
        List<VariableACBB> variablesSemis = new LinkedList();
        List<VariableACBB> variablesWsol = new LinkedList();
        List<VariableACBB> variablesPaturage = new LinkedList();
        List<VariableACBB> variablesRecfau = new LinkedList();
        List<VariableACBB> variablesAI = new LinkedList();
        List<VariableACBB> variablesFne = new LinkedList();
        List<VariableACBB> variablesFert = new LinkedList();
        List<VariableACBB> variablesPhyt = new LinkedList();
        for (String variableAffichage : variablesListNames.split(",")) {
            VariableACBB variable = (VariableACBB) this.variableACBBDAO.getByAffichage(variableAffichage).orElse(null);
            if (variable == null) {
                continue;
            }
            switch (variable.getAffichage().split("_")[0]) {
                case "sem":
                    variablesSemis.add(variable);
                    break;
                case "wsol":
                    variablesWsol.add(variable);
                    break;
                case "pat":
                    variablesPaturage.add(variable);
                    break;
                case "recFau":
                    variablesRecfau.add(variable);
                    break;
                case "fert":
                    variablesFert.add(variable);
                    break;
                case "fne":
                    variablesFne.add(variable);
                    break;
                case "aut":
                    variablesAI.add(variable);
                    break;
                case "phyt":
                    variablesPhyt.add(variable);
                    break;

                default:
                    break;
            }
        }
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("semis"), variablesSemis);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("travail_du_sol"), variablesWsol);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("paturage"), variablesPaturage);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("recolte_et_fauche"), variablesRecfau);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("autre_intervention"), variablesAI);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("fauche_non_exportee"), variablesFne);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("fertilisation"), variablesFert);
        metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("phytosanitaire"), variablesPhyt);
        final DatesFormParamVO datesYearsContinuousFormParamVO = new DatesFormParamVO(this.localizationManager);
        final List<Map<String, String>> periods = this.buildNewMapPeriod(dateDedebut, datedeFin);
        datesYearsContinuousFormParamVO.setPeriods(periods);
        datesYearsContinuousFormParamVO.setDateStart(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDedebut));
        datesYearsContinuousFormParamVO.setDateEnd(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, datedeFin));
        metadatasMap.put(DatesFormParamVO.class.getSimpleName(), datesYearsContinuousFormParamVO);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);
        final ITKParameterVO parameters = new ITKParameterVO(metadatasMap);
        try {
            ACBBTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException | BusinessException e) {
            return "false : " + e.getMessage();
        }
        return "true";

    }
}
