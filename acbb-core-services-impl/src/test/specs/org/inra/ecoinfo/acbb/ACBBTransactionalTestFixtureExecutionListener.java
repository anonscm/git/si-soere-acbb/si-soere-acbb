package org.inra.ecoinfo.acbb;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.refdata.agroecosystemesitethemedatatypeparcelle.IAgroecosystemeSiteThemeDatatypeParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.springframework.test.context.TestContext;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author ptcherniati
 */
public class ACBBTransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {

    /**
     *
     */
    public static IMetadataManager metadataManager;

    /**
     *
     */
    public static IDatasetManager datasetManager;

    /**
     *
     */
    public static IAgroecosystemeSiteThemeDatatypeParcelleDAO agroecosystemeSiteThemeDatatypeParcelleDAO;

    /**
     *
     */
    public static ISiteACBBDAO siteACBBDAO;

    /**
     *
     */
    public static IDatasetDAO datasetDAO;

    /**
     *
     */
    public static IVersionFileDAO versionFileDAO;

    /**
     *
     */
    public static IVariableDAO variableDAO;

    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static IExtractionManager extractionManager;

    /**
     *
     */
    public static INotificationsManager notificationsManager;

    /**
     *
     */
    public static IFileCompManager fileCompManager;

    /**
     *
     */
    public static IFileCompConfiguration fileCompConfiguration;

    /**
     *
     */
    public static ITraitementDAO traitementDAO;

    /**
     *
     */
    public static IVariableACBBDAO variableACBBDAO;
    private static String testName = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * Before test class.
     *
     * @param testContext
     * @throws Exception the exception @see
     *                   org.springframework.test.context.support.AbstractTestExecutionListener
     *                   #beforeTestClass(org.springframework.test.context.TestContext)
     * @link(TestContext) the test context
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        ACBBTransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        ACBBTransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        ACBBTransactionalTestFixtureExecutionListener.agroecosystemeSiteThemeDatatypeParcelleDAO = (IAgroecosystemeSiteThemeDatatypeParcelleDAO) TransactionalTestFixtureExecutionListener.applicationContext
                .getBean("agroecosystemeSiteThemeDatatypeParcelleDAO");
        ACBBTransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        ACBBTransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        ACBBTransactionalTestFixtureExecutionListener.siteACBBDAO = (ISiteACBBDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteACBBDAO");
        ACBBTransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        ACBBTransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        ACBBTransactionalTestFixtureExecutionListener.extractionManager = (IExtractionManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("extractionManager");
        ACBBTransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        ACBBTransactionalTestFixtureExecutionListener.fileCompManager = (IFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompManager");
        ACBBTransactionalTestFixtureExecutionListener.fileCompConfiguration = (IFileCompConfiguration) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompConfiguration");
        ACBBTransactionalTestFixtureExecutionListener.traitementDAO = (ITraitementDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("traitementDAO");
        ACBBTransactionalTestFixtureExecutionListener.variableACBBDAO = (IVariableACBBDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableACBBDAO");
        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
        System.setProperty("concordion.output.dir", testName);
    }

    /**
     * Clean tables.
     *
     * @throws SQLException the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from group_utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from groupe where group_name!='public';")
                .execute();
    }

    /**
     * After super class.
     *
     * @param testContext
     * @throws IOException           Signals that an I/O exception has occurred.
     * @throws DataSetException      the data set exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException          the sQL exception
     * @throws Exception             the exception
     * @link(TestContext) the test context
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        super.afterTestClass(testContext);
    }
}
