package org.inra.ecoinfo.acbb.dataset;

/**
 * @author ptcherniati
 */
public class TreeDescriptor {


    String agroecosystemeCode;
    String pathSite;
    String themeCode;
    String datatypeCode;
    String parcelleCode;

    /**
     * @param agroecosystemeCode
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @param parcelleCode
     */
    public TreeDescriptor(String agroecosystemeCode, String pathSite, String themeCode, String datatypeCode, String parcelleCode) {
        super();
        this.agroecosystemeCode = agroecosystemeCode;
        this.pathSite = pathSite;
        this.themeCode = themeCode;
        this.datatypeCode = datatypeCode;
        this.parcelleCode = parcelleCode;
    }

    /**
     * @return
     */
    public String getDatatypeCode() {
        return this.datatypeCode;
    }

    /**
     * @return
     */
    public String getPathSite() {
        return this.pathSite;
    }

    /**
     * @return
     */
    public String getThemeCode() {
        return this.themeCode;
    }

    /**
     * @return
     */
    public String getAgroecosystemeCode() {
        return this.agroecosystemeCode;
    }

    /**
     * @return
     */
    public String getParcelleCode() {
        return this.parcelleCode;
    }
}
