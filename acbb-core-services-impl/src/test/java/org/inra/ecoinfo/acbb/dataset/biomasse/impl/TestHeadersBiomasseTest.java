package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author ptcherniati
 */
public class TestHeadersBiomasseTest {

    /**
     *
     */
    protected static final String BIOMASSE_PRODUCTION_TENEUR = "biomasse_production_teneur";
    private final Map<String, String> columnsPatterns = new LinkedHashMap();
    MockUtils m = MockUtils.getInstance();
    @Mock
    RequestPropertiesBiomasse requestPropertiesBiomasse;
    @Mock
    Column columnParcelle;
    @Mock
    Column columnObservation;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    Map<Integer, Column> valueColumns;
    @Mock
    List<Column> columns;
    BadsFormatsReport badsFormatsReport = new BadsFormatsReport("error");
    String[] values = new String[]{"parcelle", "observation",
            "Mav_valeur", "Mav_nb", "Mav_et", "Mav_methode", "Mav_IQ", "N_valeur", "N_nb", "N_et",
            "N_methode", "N_IQ"};
    List<Column> columnToParse;
    TestHeadersBiomasse instance;
    /**
     *
     */
    public TestHeadersBiomasseTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private void initColumnPatterns() {
        this.columnsPatterns.put("valeur", "^(.*)_(valeur)$");
        this.columnsPatterns.put("nb", "^(.*)_(nb)$");
        this.columnsPatterns.put("et", "^(.*)_(et)$");
        this.columnsPatterns.put("methode", "^(.*)_(methode)$");
        this.columnsPatterns.put("IQ", "^(.*)_(IQ)$");
        this.instance.setColumnPatterns(this.columnsPatterns);
    }

    private void initInstance(TestHeadersBiomasse instance) {
        instance.setDatatypeName(BIOMASSE_PRODUCTION_TENEUR);
        instance.setDatasetConfiguration(this.m.datasetConfiguration);
        // instance.setColumnPatterns(Arrays.asList(new String[]{"^(.*)_(valeur)$", "^(.*)_(nb)$",
        // "^(.*)_(et)$", "^(.*)_(methode)$", "^(.*)_(IQ)$"}));
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSiteDAO(this.m.siteDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableACBBDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
    }

    /**
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        this.instance = new TestHeadersBiomasse();
        this.initInstance(this.instance);
        Mockito.when(this.datasetDescriptor.getUndefinedColumn()).thenReturn(2);
        Mockito.when(this.columnParcelle.getName()).thenReturn("parcelle");
        Mockito.when(this.columnObservation.getName()).thenReturn("observation");
        this.columnToParse = new LinkedList();
        this.columnToParse.add(this.columnParcelle);
        this.columnToParse.add(this.columnObservation);
        Mockito.when(this.m.variableDAO.getByAffichage("Mav")).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.m.variableDAO.getByAffichage("N")).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.m.variableDAO.getByAffichageAndDatatypeCode("Mav", BIOMASSE_PRODUCTION_TENEUR)).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.m.variableDAO.getByAffichageAndDatatypeCode("N", BIOMASSE_PRODUCTION_TENEUR)).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.m.variableDAO.getByAffichageAndDatatypeCode(MockUtils.VARIABLE_AFFICHAGE, BIOMASSE_PRODUCTION_TENEUR)).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.requestPropertiesBiomasse.getValueColumns())
                .thenReturn(this.valueColumns);
        this.initColumnPatterns();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of errorBadColumnName method, of class TestHeadersBiomasse.
     */
    @Test
    public void testErrorBadColumnName() {
        BusinessException result = this.instance.errorBadColumnName("value", 1L, 1,
                "expectedColumn");
        Assert.assertEquals(
                "Ligne 1, colonne 2, le nom de colonne \"value\" n'est pas celui attendu : \"expectedColumn\".",
                result.getMessage());
    }

    /**
     *
     */
    @Test
    public void testErrorMessages() {
        BusinessException e = this.instance.errorBadColumnName("badColumn", 1L, 4, "good column");
        Assert.assertEquals(
                "Ligne 1, colonne 5, le nom de colonne \"badColumn\" n'est pas celui attendu : \"good column\".",
                e.getMessage());
        e = this.instance.errorMissingVariable("badVariableName", 1L, 4, "columnName");
        Assert.assertEquals(
                "Ligne 1, colonne 5, il n'existe aucune variable \"badVariableName\" définie pour le type de donnée \"biomasse_production_teneur\" pour le nom de colonne \"columnName\".",
                e.getMessage());
        e = this.instance.errorMissingExtraColumns("expectedColumnName", this.m.variable, 1L);
        Assert.assertEquals("Ligne 1 la colonne \"expectedColumnName\" est manquante.",
                e.getMessage());
        this.instance.setMandatoryVariableNames(new String[]{MockUtils.VARIABLE_AFFICHAGE});
        e = this.instance.errorMissingMandatoryExtraColumns(1L, 4, 0);
        Assert.assertEquals("Ligne 1, colonne 5, des colonnes sont attendues pour la variable \""
                + MockUtils.VARIABLE_AFFICHAGE + "\".", e.getMessage());
        e = this.instance.errorMissingUndefinedColumns(1L, 4, "colonneManquante");
        Assert.assertEquals("Ligne 1, colonne 5, la colonne \"colonneManquante\" est manquante.",
                e.getMessage());
    }

    /**
     * Test of errorMissingExtraColumns method, of class TestHeadersBiomasse.
     */
    @Test
    public void testErrorMissingExtraColumns() {
        BusinessException result = this.instance.errorMissingExtraColumns("^(.*)_(valeur)$",
                this.m.variable, 1L);
        Assert.assertEquals("Ligne 1 la colonne \"variableAffichage_valeur\" est manquante.",
                result.getMessage());
    }

    /**
     * Test of errorMissingMandatoryExtraColumns method, of class TestHeadersBiomasse.
     */
    @Test
    public void testErrorMissingMandatoryExtraColumns() {
        this.instance.mandatoryVariableNames = new String[4];
        this.instance.mandatoryVariableNames[3] = MockUtils.VARIABLE_AFFICHAGE;
        BusinessException result = this.instance.errorMissingMandatoryExtraColumns(1L, 2, 3);
        Assert.assertEquals(
                "Ligne 1, colonne 3, des colonnes sont attendues pour la variable \"variableAffichage\".",
                result.getMessage());
    }

    /**
     * Test of errorMissingUndefinedColumns method, of class TestHeadersBiomasse.
     */
    @Test
    public void testErrorMissingUndefinedColumns() {
        BusinessException result = this.instance.errorMissingUndefinedColumns(1L, 1, "columnName");
        Assert.assertEquals("Ligne 1, colonne 2, la colonne \"columnName\" est manquante.",
                result.getMessage());
    }

    /**
     * Test of errorMissingVariable method, of class TestHeadersBiomasse.
     */
    @Test
    public void testErrorMissingVariable() {
        BusinessException result = this.instance.errorMissingVariable(MockUtils.VARIABLE_NOM, 1L,
                2, "column");
        Assert.assertEquals(
                "Ligne 1, colonne 3, il n'existe aucune variable \"variableNom\" définie pour le type de donnée \"biomasse_production_teneur\" pour le nom de colonne \"column\".",
                result.getMessage());
    }

    /**
     * Test of getColumnName method, of class TestHeadersBiomasse.
     */
    @Test
    public void testGetColumnName() {
        String result = this.instance.getColumnName("^(.*)_(et)$", this.m.variable);
        Assert.assertEquals("variableAffichage_et", result);
    }

    /**
     * Test of getColumns method, of class TestHeadersBiomasse.
     */
    @Test
    public void testGetColumns() {
        CleanerValues cleanerValues = new CleanerValues(this.values);
        int result = this.instance.getColumns(this.badsFormatsReport, 1L,
                this.requestPropertiesBiomasse, this.values, cleanerValues, this.columnToParse,
                this.datasetDescriptor);
        Assert.assertEquals(12, result);
    }

    /**
     * Test of getColumns method, of class TestHeadersBiomasse.
     */
    @Test
    public void testGetColumnsNominal() {
        long lineNumber = 8L;
        CleanerValues cleanerValues = new CleanerValues(this.values);
        ArgumentCaptor<Integer> indexValue = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Column> columnValue = ArgumentCaptor.forClass(Column.class);
        this.instance.getColumns(this.badsFormatsReport, lineNumber,
                this.requestPropertiesBiomasse, this.values, cleanerValues, this.columnToParse,
                this.datasetDescriptor);
        List<Integer> indexesExpected = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                10, 11);
        Mockito.verify(this.valueColumns, Mockito.times(12)).put(indexValue.capture(),
                columnValue.capture());
        Assert.assertEquals(indexesExpected, indexValue.getAllValues());
        List<Column> columnsExpected = columnValue.getAllValues();
        Assert.assertEquals(this.columnParcelle, columnsExpected.get(0));
        Assert.assertEquals(this.columnObservation, columnsExpected.get(1));
        for (int i = 2; i < 12; i++) {
            this.verifyColumn(i, columnsExpected.get(i), this.values);
        }

    }

    /**
     * Test of getColumnTarget method, of class TestHeadersBiomasse.
     */
    @Test
    public void testGetColumnTarget() {
        // Nominal
        Column result = this.instance.getColumnTarget(this.m.variable, "valeur", "column");
        Assert.assertEquals("column", result.getFieldName());
        Assert.assertTrue(result.isFlag());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE, result.getFlagType());
        Assert.assertFalse(result.isNullable());
        Assert.assertEquals("column", result.getName());
        Assert.assertEquals(MockUtils.VARIABLE_CODE, result.getRefVariableName());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_FLOAT_TYPE, result.getValueType());
        Assert.assertTrue(result.isVariable());
        // Nominal IQ
        result = this.instance.getColumnTarget(this.m.variable, "IQ", "column_IQ");
        Assert.assertEquals("column_IQ", result.getFieldName());
        Assert.assertTrue(result.isFlag());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE, result.getFlagType());
        Assert.assertTrue(result.isNullable());
        Assert.assertEquals("column_IQ", result.getName());
        Assert.assertEquals(MockUtils.VARIABLE_CODE, result.getRefVariableName());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_FLOAT_TYPE, result.getValueType());
        Assert.assertTrue(result.isVariable());
        // Nominal other
        result = this.instance.getColumnTarget(this.m.variable, "other", "column");
        Assert.assertEquals("column", result.getFieldName());
        Assert.assertTrue(result.isFlag());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE, result.getFlagType());
        Assert.assertTrue(result.isNullable());
        Assert.assertEquals("column", result.getName());
        Assert.assertEquals(MockUtils.VARIABLE_CODE, result.getRefVariableName());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_FLOAT_TYPE, result.getValueType());
        Assert.assertTrue(result.isVariable());
    }

    /**
     * Test of getVariable method, of class TestHeadersBiomasse.
     */
    @Test
    public void testGetVariable() {
        VariableACBB result = (VariableACBB) this.instance.getVariable(MockUtils.VARIABLE_AFFICHAGE, 1).orElse(null);
        Assert.assertEquals(this.m.variable, result);
    }

    /**
     * Test of readLineHeader method, of class TestHeadersBiomasse.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLineHeader() throws Exception {
        String str = "parcelle;observation;Mav_valeur;Mav_nb;Mav_et;Mav_methode;Mav_IQ;N_valeur;N_nb;N_et;N_methode;N_IQ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(str.getBytes()), ';');
        TestHeaderBiomasseInstance instance = new TestHeaderBiomasseInstance();
        long expResult = 1L;
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columnToParse);
        long result = instance.readLineHeader(this.badsFormatsReport, parser, 0L,
                this.datasetDescriptor, this.requestPropertiesBiomasse);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setDatatypeName method, of class TestHeadersBiomasse.
     */
    @Test
    public void testSetDatatypeName() {
        this.instance.setDatatypeName("datatypeName");
        Assert.assertEquals("datatypeName", this.instance.datatypeName);
    }

    /**
     * Test of testColumnTarget method, of class TestHeadersBiomasse.
     */
    @Test
    public void testTestColumnTarget() {
        Mockito.when(this.requestPropertiesBiomasse.getValueColumns())
                .thenReturn(this.valueColumns);
        Assert.assertTrue(this.columnToParse.contains(this.columnParcelle));
        // nominal
        this.instance.testColumnTarget(this.columnParcelle, this.badsFormatsReport, 1L, "valeur",
                2, this.requestPropertiesBiomasse, this.columnToParse);
        Mockito.verify(this.valueColumns).put(2, this.columnParcelle);
        Assert.assertFalse(this.columnToParse.contains(this.columnParcelle));
        // no column target
        this.instance.testColumnTarget(null, this.badsFormatsReport, 1L, "valeur", 2,
                this.requestPropertiesBiomasse, this.columnToParse);
        Assert.assertTrue(this.badsFormatsReport.hasErrors());
        Assert.assertEquals("Ligne 1 : L'intitulé \"valeur\" de la colonne 3 est inconnu.",
                this.badsFormatsReport.getErrorsMessages().get(0));
    }

    /**
     * Test of testHeaders method, of class TestHeadersBiomasse.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        Mockito.when(this.m.versionFile.getData()).thenReturn(org.apache.commons.lang.StringUtils.EMPTY.getBytes());
        String str = "parcelle;observation;Mav_valeur;Mav_nb;Mav_et;Mav_methode;Mav_IQ;N_valeur;N_nb;N_et;N_methode;N_IQ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(str.getBytes()), ';');
        VersionFile versionFile = this.m.versionFile;
        String encoding = "UTF-8";
        TestHeaderBiomasseInstance instance = new TestHeaderBiomasseInstance();
        long result = instance.testHeaders(parser, versionFile, this.requestPropertiesBiomasse,
                encoding, this.badsFormatsReport, this.datasetDescriptor);
        Assert.assertEquals(11, result);
    }

    /**
     * Test of testNextColumn method, of class TestHeadersBiomasse.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestNextColumn() throws Exception {
        Iterator<String> it = Mockito.mock(Iterator.class);
        Mockito.when(it.hasNext()).thenReturn(Boolean.TRUE, Boolean.TRUE, Boolean.FALSE);
        Mockito.when(it.next()).thenReturn("next");
        // nominal
        String result = this.instance.testNextColumn(it, this.values, 1L, 2, this.m.variable);
        Assert.assertEquals("Mav_valeur", result);
        // bad column
        try {
            result = this.instance.testNextColumn(it, this.values, 1L, 15, this.m.variable);
            Assert.fail("must throw exception");
        } catch (BusinessException ex) {
            Assert.assertEquals("Ligne 1 la colonne \"next\" est manquante.", ex.getMessage());
        }
    }

    /**
     * Test of testPatternColumn method, of class TestHeadersBiomasse.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestPatternColumn() throws Exception {
        ArgumentCaptor<Integer> indexValue = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Column> columnValue = ArgumentCaptor.forClass(Column.class);
        int result = this.instance.testPatternColumn(this.values, 1L, 2,
                this.requestPropertiesBiomasse, this.columnToParse);
        Mockito.verify(this.valueColumns, Mockito.times(10)).put(indexValue.capture(),
                columnValue.capture());
        Assert.assertEquals(12, result);
        List<Integer> indexesExpected = Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10,
                11);
        Assert.assertEquals(indexesExpected, indexValue.getAllValues());
        List<Column> columnsExpected = columnValue.getAllValues();
        for (int i = 2; i < 12; i++) {
            this.verifyColumn(i, columnsExpected.get(i - 2), this.values);
        }
    }

    private void verifyColumn(int i, Column column, String[] values) {
        Assert.assertNotNull(column);
        Assert.assertTrue(column.isFlag());
        Assert.assertTrue(column.isVariable());
        Assert.assertEquals(6 == i || 11 == i ? RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE
                : RecorderACBB.PROPERTY_CST_VARIABLE_TYPE, column.getFlagType());
        Assert.assertEquals(RecorderACBB.PROPERTY_CST_FLOAT_TYPE, column.getValueType());
        Assert.assertEquals(MockUtils.VARIABLE_CODE, column.getRefVariableName());
        Assert.assertEquals(values[i], column.getName());
        Assert.assertEquals(values[i], column.getFieldName());
        Assert.assertEquals(i == 2 || i == 7 ? Boolean.FALSE : Boolean.TRUE, column.isInull());
    }

    class TestHeaderBiomasseInstance extends TestHeadersBiomasse {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        String[] values;
        List<Column> columnToParse;

        TestHeaderBiomasseInstance() {
            initInstance(this);
        }

        @Override
        protected long jumpLines(CSVParser parser, long lineNumber, int numberOfJumpedLines)
                throws IOException {
            return lineNumber + numberOfJumpedLines;
        }

        @Override
        protected long readBeginAndEndDates(VersionFile version,
                                            BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber,
                                            IRequestPropertiesACBB requestProperties) throws IOException {
            return lineNumber + 2;
        }

        @Override
        protected long readCommentaire(CSVParser parser, long lineNumber,
                                       IRequestPropertiesACBB requestProperties) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                    long lineNumber, String datatypeName) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readEmptyLine(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                     long lineNumber) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readSite(VersionFile version, BadsFormatsReport badsFormatsReport,
                                CSVParser parser, long lineNumber, IRequestPropertiesACBB requestProperties)
                throws IOException, BusinessException {
            return lineNumber + 1;
        }
    }
}
