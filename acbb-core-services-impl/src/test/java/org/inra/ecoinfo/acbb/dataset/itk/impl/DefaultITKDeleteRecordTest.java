/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class DefaultITKDeleteRecordTest {

    DefaultITKDeleteRecord instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    ITempoDAO tempoDAO;
    @Mock
    ILocalPublicationDAO publicationDAO;
    /**
     *
     */
    public DefaultITKDeleteRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        this.instance = new DefaultITKDeleteRecord();
        MockitoAnnotations.initMocks(this);
        this.instance.setDatasetConfiguration(this.m.datasetConfiguration);
        this.instance.setPublicationDAO(this.publicationDAO);
        this.instance.setTempoDAO(this.tempoDAO);
        this.instance.setVersionFileDAO(this.m.versionFileDAO);
        Mockito.when(this.m.versionFileDAO.merge(this.m.versionFile))
                .thenReturn(this.m.versionFile);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class DefaultITKDeleteRecord.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        this.instance = Mockito.spy(this.instance);
        this.instance.deleteRecord(this.m.versionFile);
        Mockito.verify(this.tempoDAO).deleteUnusedTempos();
        Mockito.verify(this.instance).deleteRecord(this.m.versionFile);
        Mockito.verify(this.publicationDAO).removeVersion(this.m.versionFile);
        // exception
        Mockito.doThrow(new PersistenceException("erreur")).when(this.tempoDAO)
                .deleteUnusedTempos();
        try {
            this.instance.deleteRecord(this.m.versionFile);
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals("erreur", e.getMessage());
        }
    }

}
