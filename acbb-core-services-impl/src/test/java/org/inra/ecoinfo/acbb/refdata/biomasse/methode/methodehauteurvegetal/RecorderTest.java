/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.test.utils.BiomassMockUtils;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class RecorderTest {

    BiomassMockUtils m = BiomassMockUtils.getInstance();
    Recorder instance;
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    IMethodeHauteurVegetalDAO methodeHauteurVegetalDAO;
    @Mock
    MethodeHauteurVegetal methodeHauteurVegetal;
    @Mock
    Properties propertiesLibelleEN;
    @Mock
    Properties propertiesDefinitionEN;
    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    protected void init(Recorder instance) {
        MockitoAnnotations.initMocks(this);
        this.instance = instance;
        this.instance.setMethodeHauteurVegetalDAO(this.methodeHauteurVegetalDAO);
        this.instance.setMethodeDAO(this.methodeHauteurVegetalDAO);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.init(new Recorder());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "104";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()));
        Mockito.when(this.methodeHauteurVegetalDAO.getByNumberId(104L)).thenReturn(
                Optional.of(this.methodeHauteurVegetal));
        this.instance.deleteRecord(parser, null, "UTF-8");
        // nominal
        Mockito.verify(this.methodeHauteurVegetalDAO).getByNumberId(104L);
        Mockito.verify(this.methodeHauteurVegetalDAO).remove(this.methodeHauteurVegetal);
        // persitence exception
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()));
        Mockito.doThrow(new PersistenceException("erreur")).when(this.methodeHauteurVegetalDAO)
                .remove(this.methodeHauteurVegetal);
        try {
            this.instance.deleteRecord(parser, null, "UTF-8");
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals("erreur", e.getMessage());
        }
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        List<MethodeHauteurVegetal> methodes = new LinkedList();
        Mockito.when(this.methodeHauteurVegetalDAO.getAll()).thenReturn(methodes);
        List<MethodeHauteurVegetal> result = this.instance.getAllElements();
        Assert.assertEquals(methodes, result);
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        this.testInitModelGridMetadata();
        this.methodeHauteurVegetal = new MethodeHauteurVegetal("définition", "libellé", 102L);
        Mockito.when(this.propertiesLibelleEN.getProperty("libellé", "libellé")).thenReturn(
                "libellé_en");
        Mockito.when(this.propertiesDefinitionEN.getProperty("définition", "définition"))
                .thenReturn("définition_en");
        LineModelGridMetadata result = this.instance
                .getNewLineModelGridMetadata(this.methodeHauteurVegetal);
        Assert.assertTrue(5 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("102", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("libellé", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("libellé_en", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("définition", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("définition_en", result.getColumnModelGridMetadataAt(4).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     */
    @Test
    public void testInitModelGridMetadata() {
        this.instance = Mockito.spy(this.instance);
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        Mockito.when(
                MockUtils.localizationManager.newProperties(this.instance.getMethodeEntityName(),
                        this.instance.getMethodeLibelleName(), Locale.ENGLISH)).thenReturn(
                this.propertiesLibelleEN);
        Mockito.when(
                MockUtils.localizationManager.newProperties(this.instance.getMethodeEntityName(),
                        this.instance.getMethodeDefinitionName(), Locale.ENGLISH)).thenReturn(
                this.propertiesDefinitionEN);
        this.instance.initModelGridMetadata();
        Mockito.verify(MockUtils.localizationManager).newProperties(
                this.instance.getMethodeEntityName(), this.instance.getMethodeLibelleName(),
                Locale.ENGLISH);
        Mockito.verify(MockUtils.localizationManager).newProperties(
                this.instance.getMethodeEntityName(), this.instance.getMethodeDefinitionName(),
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String text = "identifiant;libellé_fr;libellé_en;définition_fr;définition_en\n"
                + "400;distance entre noeud;distance between node;distance entre noeud;distance between node";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        String encoding = "UTF-8";
        // nominal
        when(methodeHauteurVegetalDAO.getByNKey(any())).thenReturn(Optional.empty());
        this.instance.processRecord(parser, null, encoding);
        ArgumentCaptor<MethodeHauteurVegetal> ma = ArgumentCaptor.forClass(MethodeHauteurVegetal.class);
        Mockito.verify(this.methodeHauteurVegetalDAO).saveOrUpdate(ma.capture());
        this.methodeHauteurVegetal = ma.getValue();
        Assert.assertEquals("distance entre noeud", this.methodeHauteurVegetal.getLibelle());
        Assert.assertEquals("distance entre noeud", this.methodeHauteurVegetal.getDefinition());
        Assert.assertTrue(400 == this.methodeHauteurVegetal.getNumberId());
        // with error
        text = "identifiant;libellé_fr;libellé_en;définition_fr;définition_en\n"
                + "quatre-cent;distance entre noeud;distance between node;distance entre noeud;distance between node";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        try {
            this.instance.processRecord(parser, null, encoding);
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals(
                    ".Ligne 1 , colonne 1, l'identifiant quatre-cent de la méthode doit être un entier.\n",
                    e.getMessage());
        }
    }

}
