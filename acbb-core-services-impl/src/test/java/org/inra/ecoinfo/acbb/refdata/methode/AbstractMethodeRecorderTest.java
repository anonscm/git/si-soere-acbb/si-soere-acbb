/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.methode;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.util.*;

/**
 * @author ptcherniati
 */
public class AbstractMethodeRecorderTest {

    /**
     *
     */
    @Mock
    public ILocalizationDAO localizationDAO;
    MockUtils m = MockUtils.getInstance();
    String encoding = "UTF-8";
    AbstractMethodeRecorderImpl<MethodeImpl> instance;
    @Mock
    IMethodeDAO methodeDAO;
    @Mock
    MethodeImpl methode;
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    Properties propertiesNomEN;
    @Mock
    Properties propertiesDefinitionEN;

    /**
     *
     */
    public AbstractMethodeRecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.m = MockUtils.getInstance();
        Localization.setLocalisations(Arrays.asList("fr", "en"));
        this.instance = new AbstractMethodeRecorderImpl();
        MockitoAnnotations.initMocks(this);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        this.instance.setMethodeDAO(this.methodeDAO);
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getByNumberId method, of class AbstractMethodeRecorder.
     */
    @Test
    public void testGetByNumberId() {
        String numberIdString = "24";
        AbstractMethode expResult = this.methode;
        // nominal case
        Mockito.doReturn(Optional.of(this.methode)).when(this.instance.methodeDAO)
                .getByNumberId(any(Long.class));
        AbstractCSVMetadataRecorder.ErrorsReport errorsReport = this.instance.initErrorsReport();
        AbstractMethode result = (AbstractMethode) this.instance.getByNumberId(numberIdString, errorsReport).orElse(null);
        Assert.assertEquals(expResult, result);
        Assert.assertFalse(this.instance.getErrorsReport().hasErrors());
        // empty numberID
        result = (AbstractMethode) this.instance.getByNumberId(org.apache.commons.lang.StringUtils.EMPTY, errorsReport).orElse(null);
        Assert.assertNull(result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(".L'identifiant  de la méthode doit être un entier.\n",
                errorsReport.getErrorsMessages());
        // bad number id
        errorsReport = this.instance.initErrorsReport();
        result = (AbstractMethode) this.instance.getByNumberId("vingt-quatre", errorsReport).orElse(null);
        Assert.assertNull(result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(".L'identifiant vingt-quatre de la méthode doit être un entier.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of getLibelle method, of class AbstractMethodeRecorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetLibelle() throws Exception {
        AbstractCSVMetadataRecorder.ErrorsReport errorsReport = this.instance.initErrorsReport();
        // nominal
        String result = this.instance.getLibelle(errorsReport, 0,
                this.instance.getTokenizer(new String[]{"libelle"}));
        Assert.assertEquals("libelle", result);
        Assert.assertFalse(errorsReport.hasErrors());
        // empty libelle
        result = this.instance.getLibelle(errorsReport, 1,
                this.instance.getTokenizer(new String[]{org.apache.commons.lang.StringUtils.EMPTY}));
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(".Ligne 1 , colonne 1, le libellé doit être renseigné.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of getNumberId method, of class AbstractMethodeRecorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNumberId() throws Exception {
        AbstractCSVMetadataRecorder.ErrorsReport errorsReport = this.instance.initErrorsReport();
        // nominal
        Long result = this.instance.getNumberId(errorsReport, 1,
                this.instance.getTokenizer(new String[]{"2"}));
        Assert.assertTrue(2L == result);
        Assert.assertFalse(errorsReport.hasErrors());
        // null numberId
        result = this.instance.getNumberId(errorsReport, 1,
                this.instance.getTokenizer(new String[]{org.apache.commons.lang.StringUtils.EMPTY}));
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(".Ligne 1 , colonne 1, l'identifiant de la méthode est manquant.\n",
                errorsReport.getErrorsMessages());
        // bad numberId
        errorsReport = this.instance.initErrorsReport();
        result = this.instance.getNumberId(errorsReport, 1,
                this.instance.getTokenizer(new String[]{"quatre"}));
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                ".Ligne 1 , colonne 1, l'identifiant quatre de la méthode doit être un entier.\n",
                errorsReport.getErrorsMessages());

    }

    /**
     * @return
     */
    protected Class<MethodeImpl> getMethodeClass() {
        return MethodeImpl.class;
    }

    /**
     * @param <MethodeImpl>
     */
    public class AbstractMethodeRecorderImpl<MethodeImpl> extends AbstractMethodeRecorder {

        ErrorsReport errorsReport;

        /**
         *
         */
        public AbstractMethodeRecorderImpl() {
            entityClass = getMethodeClass();
            hasGenericNodeable = false;
        }

        /**
         * @param parser
         * @param file
         * @param encoding
         * @throws BusinessException
         */
        @Override
        public void deleteRecord(CSVParser parser, File file, String encoding)
                throws BusinessException {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @param allMethods
         * @return
         */
        @Override
        protected Set filter(Set allMethods) {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @return
         */
        @Override
        protected List getAllElements() {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @return
         */
        public String getErrorsMessages() {
            return this.errorsReport.getErrorsMessages();
        }

        /**
         * @return
         */
        public ErrorsReport getErrorsReport() {
            return this.errorsReport;
        }

        /**
         * @return
         */
        @Override
        protected String getMethodeDefinitionName() {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @return
         */
        @Override
        protected String getMethodeEntityName() {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @return
         */
        @Override
        protected String getMethodeLibelleName() {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @param element
         * @return
         */
        @Override
        public LineModelGridMetadata getNewLineModelGridMetadata(Object element) {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }

        /**
         * @param values
         * @return
         */
        public TokenizerValues getTokenizer(String[] values) {
            return new TokenizerValues(values);
        }

        /**
         * @return
         */
        protected ErrorsReport initErrorsReport() {
            this.errorsReport = new ErrorsReport();
            return this.errorsReport;
        }

        /**
         * @param parser
         * @param file
         * @param encoding
         * @throws BusinessException
         */
        @Override
        public void processRecord(CSVParser parser, File file, String encoding)
                throws BusinessException {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of
            // generated methods,
            // choose Tools |
            // Templates.
        }
    }

    /**
     *
     */
    public class MethodeImpl extends AbstractMethode {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
    }
}
