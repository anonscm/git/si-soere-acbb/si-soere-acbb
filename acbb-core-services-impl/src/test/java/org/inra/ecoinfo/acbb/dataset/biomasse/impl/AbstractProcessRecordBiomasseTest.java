/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl.VariableValueBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.IPaturageDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class AbstractProcessRecordBiomasseTest {

    MockUtils m = MockUtils.getInstance();
    AbstractProcessRecordBiomasse instance;
    @Mock(name = "interventionDAO")
    IInterventionDAO interventionDAO;
    @Mock(name = "interventionForNumDAO")
    IPaturageDAO interventionForNumDAO;
    @Mock(name = "listeVegetationDAO")
    IListeVegetationDAO listeVegetationDAO;
    @Mock(name = "listeItineraireDAO")
    IListeItineraireDAO listeItineraireDAO;
    @Mock(name = "line")
    AbstractLineRecord line;
    @Mock(name = "datasetDescriptorACBB")
    DatasetDescriptorACBB datasetDescriptorACBB;
    @Mock(name = "requestPropertiesBiomasse")
    RequestPropertiesBiomasse requestPropertiesBiomasse;
    @Mock(name = "variableValues")
    List<ACBBVariableValue> variableValues;
    @Mock(name = "column1")
    Column column1;
    @Mock(name = "column2")
    Column column2;
    @Mock(name = "columnNoFlag")
    Column columnNoFlag;
    @Mock(name = "variableACBB1")
    VariableACBB variableACBB1;
    @Mock(name = "variableACBB2")
    VariableACBB variableACBB2;
    @Mock(name = "dvu1")
    DatatypeVariableUniteACBB dvu1;
    @Mock(name = "dvu2")
    DatatypeVariableUniteACBB dvu2;
    @Mock(name = "parser")
    CSVParser parser;
    @Mock(name = "columnEntry")
    Map.Entry<Integer, Column> columnEntry;
    @Mock(name = "lineRecord")
    AbstractLineRecord lineRecord;
    Integer rotationNumber = 2;
    Integer anneeNumber = 3;
    Integer periodeNumber = 4;
    @Mock
    Properties properties;
    @Mock(name = "vv1")
    private ACBBVariableValue<ListeVegetation> vv1;
    @Mock(name = "vv2")
    private ACBBVariableValue<ListeVegetation> vv2;
    @Mock(name = "typeIntervention")
    private InterventionType typeIntervention;
    @Mock(name = "intervention")
    private AbstractIntervention intervention;
    @Mock(name = "interventionForNum")
    private Paturage interventionForNum;

    /**
     *
     */
    public AbstractProcessRecordBiomasseTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of readNatureCouvert method, of class
     * ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadNatureCouvert() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"nautre couvert"};
        CleanerValues cleanerValues = new CleanerValues(values);
        AbstractLineRecord line = Mockito.mock(AbstractLineRecord.class);
        // nominal
        int result = this.instance.readNatureCouvert(cleanerValues, line, errorsReport, 1L, 0);
        Assert.assertEquals(1, result);
        Mockito.verify(line).setNatureCouvert("nautre couvert");
    }

    private void initColumns() {
        Mockito.when(this.column1.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.column1.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.column2.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.column2.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
    }

    /**
     * @param instance
     */
    protected void initInstance(AbstractProcessRecordBiomasse instance) {
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setInterventionDAO(this.interventionDAO);
        instance.setListeACBBDAO(this.listeVegetationDAO);
        instance.setListeItineraireDAO(this.listeItineraireDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setVersionFileDAO(this.m.versionFileDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(any(), any())).thenReturn(Optional.empty());
        when(m.versionDeTraitementDAO.getByNKey(any(), any(), anyInt())).thenReturn(Optional.empty());
        when(m.variableDAO.getByAffichage(any())).thenReturn(Optional.empty());
        when(m.parcelleDAO.getByNKey(any())).thenReturn(Optional.empty());
    }

    private void initLine() {
        Mockito.when(this.line.getAnneeNumber()).thenReturn(this.anneeNumber);
        Mockito.when(this.line.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.line.getDateDebuttraitementSurParcelle()).thenReturn(this.m.dateFin);
        Mockito.when(this.line.getObservation()).thenReturn("observation");
        Mockito.when(this.line.getOriginalLineNumber()).thenReturn(12L);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(this.periodeNumber);
        Mockito.when(this.line.getSerie()).thenReturn("série 1");
        Mockito.when(this.line.getRotationNumber()).thenReturn(this.rotationNumber);
        Mockito.when(this.line.getSuiviParcelle()).thenReturn(this.m.suiviParcelle);
        Mockito.when(this.line.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(this.line.getTypeIntervention()).thenReturn(this.typeIntervention);
        Mockito.when(this.line.getVariablesValues()).thenReturn(this.variableValues);
        Mockito.when(this.line.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractProcessRecordBiomasseImpl();
        this.initInstance(this.instance);
        this.variableValues = Arrays.asList(new ACBBVariableValue[]{this.vv1, this.vv2});
        Mockito.when(this.columnEntry.getValue()).thenReturn(this.column1);
        this.initLine();
        this.initColumns();
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(any(), any())).thenReturn(Optional.empty());
        when(m.versionDeTraitementDAO.getByNKey(any(), any(), anyInt())).thenReturn(Optional.empty());
        when(m.variableDAO.getByAffichage(any())).thenReturn(Optional.empty());
        when(m.parcelleDAO.getByNKey(any())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getListeVegetationDAO method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testGetListeVegetationDAO() {
        IListeVegetationDAO result = this.instance.getListeVegetationDAO();
        Assert.assertEquals(this.listeVegetationDAO, result);

    }

    /**
     * Test of getVariable method, of class AbstractProcessRecordBiomasse.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetVariable() throws Exception {
        this.instance = new AbstractProcessRecordBiomasseImplNoVariable();
        this.initInstance(this.instance);
        // by code
        Mockito.when(this.column1.getName()).thenReturn(MockUtils.VARIABLE_AFFICHAGE);
        when(m.variableDAO.getByAffichage(MockUtils.VARIABLE_AFFICHAGE)).thenReturn(Optional.of(m.variable));
        VariableACBB result = this.instance.getVariable(this.columnEntry).orElse(null);
        Assert.assertEquals(this.m.variable, result);
        Mockito.verify(this.m.variableDAO).getByAffichage(MockUtils.VARIABLE_AFFICHAGE);
        // by affichage
        when(m.variableDAO.getByCode(MockUtils.VARIABLE_CODE)).thenReturn(Optional.of(m.variable));
        Mockito.when(this.column1.getRefVariableName()).thenReturn("VariableCode");
        result = this.instance.getVariable(this.columnEntry).orElse(null);
        Assert.assertEquals(this.m.variable, result);
        Mockito.verify(this.m.variableDAO).getByCode(MockUtils.VARIABLE_CODE);
    }

    /**
     * Test of readDateAndSetVersionTraitement method, of class
     * AbstractProcessRecordBiomasse.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testReadDateAndSetVersionTraitement() throws BadExpectedValueException {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.DATE_DEBUT, "2",
                "3"});
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(this.m.dateDebut).when(this.instance)
                .getDate(errorsReport, 1L, 1, cleanerValues, this.datasetDescriptorACBB);
        Mockito.doNothing()
                .when(this.instance)
                .updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                        this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        IntervalDate intervalDate = new IntervalDate(m.dateTimeDebut, m.dateTimeFin, DateUtil.YYYY_MM_DD_HHMMSS_SSSZ);
        // bnominal
        int result = this.instance.readDateAndSetVersionTraitement(intervalDate, this.lineRecord, errorsReport,
                1L, 1, cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(2, result);
        Mockito.verify(this.instance).getDate(errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptorACBB);
        Mockito.verify(this.instance).updateVersionDeTraitement(this.lineRecord, errorsReport, 1L,
                1, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        // null date
        Mockito.doReturn(null).when(this.lineRecord).getDate();
        result = this.instance.readDateAndSetVersionTraitement(intervalDate, this.lineRecord, errorsReport, 1L,
                1, cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(2, result);
        Mockito.verify(this.instance, Mockito.times(2)).getDate(errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptorACBB);
        Mockito.verify(this.instance, Mockito.times(1)).updateVersionDeTraitement(this.lineRecord,
                errorsReport, 1L, 1, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
    }

    /**
     * Test of readGenericColumns method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadGenericColumns() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(null);
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(1)
                .when(this.instance)
                .readParcelle(this.lineRecord, errorsReport, 1L, 0, cleanerValues,
                        this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.doReturn(2).when(this.instance).readObservation(errorsReport, cleanerValues, this.lineRecord, 1);
        Mockito.doReturn(5)
                .when(this.instance)
                .readRotationAnneePeriode(cleanerValues, errorsReport, this.lineRecord, 1L, 2,
                        this.datasetDescriptorACBB);
        Mockito.doReturn(7)
                .when(this.instance)
                .readIntervention(cleanerValues, errorsReport, this.lineRecord, 1L, 5,
                        this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.doReturn(8)
                .when(this.instance)
                .readSerie(cleanerValues, errorsReport, lineRecord, 7);
        // nominal
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle, (Parcelle) null);
        int result = this.instance.readGenericColumns(errorsReport, 1L, cleanerValues,
                this.lineRecord, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(8, result);
        // nill parcelle
        result = this.instance.readGenericColumns(errorsReport, 1L, cleanerValues, this.lineRecord,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(8, result);
    }

    /**
     * Test of readIntervention method, of class AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadIntervention() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.DATE_DEBUT, "202",
                " nopattern", " nopattern"});
        this.instance = Mockito.spy(this.instance);
        Mockito.when(this.lineRecord.getTypeIntervention()).thenReturn(this.typeIntervention);
        Mockito.doReturn(4)
                .when(this.instance)
                .readTypeIntervention(this.lineRecord, errorsReport, 1L, 3, cleanerValues,
                        this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.doNothing()
                .when(this.instance)
                .readInterventionForDate(MockUtils.DATE_DEBUT, errorsReport, this.lineRecord, 1L,
                        5, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.doNothing()
                .when(this.instance)
                .readInterventionForNum("202", errorsReport, this.lineRecord, 1L, 5,
                        this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        // nominal date
        Mockito.doReturn(this.typeIntervention).when(this.line).getTypeIntervention();
        int result = this.instance.readIntervention(cleanerValues, errorsReport, this.lineRecord,
                1L, 3, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(5, result);
        Mockito.verify(this.instance).readInterventionForDate(MockUtils.DATE_DEBUT, errorsReport,
                this.lineRecord, 1L, 5, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        // nominal num
        Mockito.when(this.line.getTypeIntervention()).thenReturn(this.typeIntervention);
        result = this.instance.readIntervention(cleanerValues, errorsReport, this.lineRecord, 1L,
                3, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(5, result);
        Mockito.verify(this.instance).readInterventionForNum("202", errorsReport, this.lineRecord,
                1L, 5, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        // no pattern
        Mockito.when(this.datasetDescriptorACBB.getColumnName(4)).thenReturn("column");
        result = this.instance.readIntervention(cleanerValues, errorsReport, this.lineRecord, 1L,
                3, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(5, result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, colonne 5, la colonne \"column\" doit comporter un entier ou une date au format \"dd/MM/yyyy\".\n",
                errorsReport.getErrorsMessages());
        // no typeintervention
        Mockito.when(this.lineRecord.getTypeIntervention()).thenReturn(null);
        Mockito.when(this.datasetDescriptorACBB.getColumnName(3)).thenReturn("previouscolumn");
        result = this.instance.readIntervention(cleanerValues, errorsReport, this.lineRecord, 1L,
                3, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(5, result);
        Assert.assertTrue(errorsReport.hasInfos());
        Assert.assertEquals(
                "-Ligne 1, colonne 4, vous avez renseigné la colonne \"column\" sans renseigner la colonne \"previouscolumn\".\n"
                        + "L'information a été ignorée.\n", errorsReport.getInfosMessages());
    }

    /**
     * Test of readInterventionForDate method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadInterventionForDate() throws DateTimeException {
        ErrorsReport errorsReport = new ErrorsReport();
        final String valeur = "valeur";
        LocalDate date = this.m.dateDebut;
        // nominal case
        Mockito.when(this.typeIntervention.getValeur()).thenReturn(valeur);
        Mockito.when(
                this.interventionDAO.getinterventionForDateAndType(this.line.getParcelle(), date,
                        this.line.getAnneeNumber(), this.typeIntervention.getValeur())).thenReturn(
                Optional.of(this.intervention));
        this.instance.readInterventionForDate(MockUtils.DATE_DEBUT, errorsReport, this.line, 2L, 3,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.verify(this.line).setIntervention(this.intervention);
        // null intervention
        Mockito.when(
                this.interventionDAO.getinterventionForDateAndType(this.line.getParcelle(), date,
                        this.line.getAnneeNumber(), this.typeIntervention.getValeur())).thenReturn(
                Optional.empty());
        Mockito.verify(this.line, Mockito.times(1)).setIntervention(this.intervention);
        this.instance.readInterventionForDate(MockUtils.DATE_DEBUT, errorsReport, this.line, 3L, 4,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 3, colonne 4, il n'existe pas d'intervention de type \"valeur\" pour la parcelle \"Parcelle\" et pour la date \"01/01/2012\".\n",
                errorsReport.getErrorsMessages());
        // badDateformat
        errorsReport = new ErrorsReport();
        this.instance.readInterventionForDate("bad date", errorsReport, this.line, 4L, 5,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals("-Ligne 4, colonne 5, la date doit être au format  \"dd/MM/yyyy\".\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readInterventionForNum method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadInterventionForNum() {
        ErrorsReport errorsReport = new ErrorsReport();
        final String valeur = "valeur";
        instance.setPaturageDAO(interventionForNumDAO);
        // nominal case
        instance.setInterventionDAO(interventionForNumDAO);
        Mockito.when(this.typeIntervention.getValeur()).thenReturn(valeur);
        Mockito.when(interventionForNumDAO.hasType(typeIntervention)).thenReturn(Boolean.TRUE);
        Mockito.when(
                interventionForNumDAO.getinterventionForNumYearAndType(this.m.parcelle, 204, this.line.getRotationNumber(),
                        this.line.getAnneeNumber(), this.typeIntervention.getValeur())).thenReturn(
                Optional.of(interventionForNum));
        this.instance.readInterventionForNum("204", errorsReport, this.line, 2L, 3,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.verify(this.line).setIntervention(interventionForNum);
        // null intervention
        Mockito.when(
                interventionForNumDAO.getinterventionForNumYearAndType(this.m.parcelle, 204, this.line.getRotationNumber(),
                        this.line.getAnneeNumber(), this.typeIntervention.getValeur())).thenReturn(
                Optional.empty());
        Mockito.verify(this.line, Mockito.times(1)).setIntervention(interventionForNum);
        this.instance.readInterventionForNum("204", errorsReport, this.line, 2L, 3,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2, colonne 3, il n'existe pas d'intervention de type \"valeur\" pour la parcelle \"Parcelle\", l'année \"3\" et le numéro d'intervention \"204\".\n",
                errorsReport.getErrorsMessages());
        // bad type
        errorsReport = new ErrorsReport();
        Mockito.when(interventionForNumDAO.hasType(typeIntervention)).thenReturn(Boolean.FALSE);
        Mockito.when(
                interventionForNumDAO.getinterventionForNumYearAndType(this.m.parcelle, 204, this.line.getRotationNumber(),
                        this.line.getAnneeNumber(), this.typeIntervention.getValeur())).thenReturn(
                Optional.of(interventionForNum));
        Mockito.verify(this.line, Mockito.times(1)).setIntervention(interventionForNum);
        this.instance.readInterventionForNum("204", errorsReport, this.line, 2L, 3,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2, colonne 3, Le type d'intervention \"valeur\" n'a pas de numéro incrémental de passage.\n",
                errorsReport.getErrorsMessages());
        // missing date
        errorsReport = new ErrorsReport();
        when(line.getAnneeNumber()).thenReturn(null);
        Mockito.when(
                interventionForNumDAO.getinterventionForNumYearAndType(this.m.parcelle, 204, this.line.getRotationNumber(),
                        this.line.getAnneeNumber(), this.typeIntervention.getValeur())).thenReturn(
                Optional.of(interventionForNum));
        Mockito.verify(this.line, Mockito.times(1)).setIntervention(interventionForNum);
        this.instance.readInterventionForNum("204", errorsReport, this.line, 2L, 3,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2, colonne 3, vous avez renseigné un numéro incrémental de passage \"\n"
                        + "\" sans renseigner la colonne \"année\".\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readObservation method, of class AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadObservation() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"observation", null});
        // nominal
        int result = this.instance.readObservation(errorsReport, cleanerValues, this.line, 2);
        Assert.assertEquals(3, result);
        Mockito.verify(this.line).setObservation("observation");
        // nominal
        result = this.instance.readObservation(errorsReport, cleanerValues, this.line, 3);
        Assert.assertEquals(4, result);
        Mockito.verify(this.line).setObservation(null);

    }

    /**
     * Test of readParcelle method, of class AbstractProcessRecordBiomasse.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testReadParcelle() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.PARCELLE_NOM,
                "badParcelle", ""});
        // nominal
        Mockito.when(this.requestPropertiesBiomasse.getSite()).thenReturn(this.m.site);
        Mockito.when(this.m.parcelleDAO.getByNKey("site_parcelle")).thenReturn(Optional.of(m.parcelle));
        int result = this.instance.readParcelle(this.lineRecord, errorsReport, 1L, 2,
                cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(3, result);
        // null parcelle
        Mockito.when(this.m.parcelleDAO.getByNKey("site_badparcelle")).thenReturn(Optional.empty());
        result = this.instance.readParcelle(this.lineRecord, errorsReport, 1L, 3, cleanerValues,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(4, result);
        Assert.assertEquals(
                "-La parcelle badparcelle définie ligne 1 colonne 4 n'existe pas pour le site Site.\n",
                errorsReport.getErrorsMessages());
        // null parcelle
        errorsReport = new ErrorsReport();
        result = this.instance.readParcelle(this.lineRecord, errorsReport, 1L, 5, cleanerValues,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(6, result);
        Assert.assertEquals(
                "-Aucune parcelle n'a été définie ligne 1 colonne 6.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readRotationAnneePeriode method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadRotationAnneePeriode() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"1", "2", "3"});
        int result = this.instance.readRotationAnneePeriode(cleanerValues, errorsReport,
                this.lineRecord, 1L, 1, this.datasetDescriptorACBB);
        Assert.assertEquals(4, result);
        Mockito.verify(this.lineRecord).setRotationNumber(1);
        Mockito.verify(this.lineRecord).setAnneeNumber(2);
        Mockito.verify(this.lineRecord).setPeriodeNumber(3);
    }

    /**
     * Test of readTypeIntervention method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testReadTypeIntervention() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"type d'intervention",
                "type d'intervention"});
        Mockito.when(
                this.listeItineraireDAO.getByNKey(InterventionType.INTERVENTION_TYPE,
                        "type d'intervention")).thenReturn(Optional.of(this.typeIntervention), Optional.empty());
        // nominal
        int result = this.instance.readTypeIntervention(this.lineRecord, errorsReport, 1L, 2,
                cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertEquals(3, result);
        Mockito.verify(this.lineRecord).setTypeIntervention(this.typeIntervention);
        // null type
        result = this.instance.readTypeIntervention(this.lineRecord, errorsReport, 1L, 3,
                cleanerValues, this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(4, result);
        Assert.assertEquals(
                "-Ligne 1, colonne 3, le type d'intervention type d'intervention n'existe pas en base de données.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitementNominal() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.of(this.m.suiviParcelle));
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.of(this.m.versionDeTraitement));
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.verify(this.lineRecord).setSuiviParcelle(this.m.suiviParcelle);
        Mockito.verify(this.lineRecord).setTraitementProgramme(this.m.traitement);
        Mockito.verify(this.lineRecord).setVersionDeTraitement(this.m.versionDeTraitement);
        Mockito.verify(this.lineRecord).setDateDebuttraitementSurParcelle(this.m.dateDebut);
        Assert.assertFalse(errorsReport.hasErrors());

    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitementNullSuiviParcelle() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        ILocalizationManager localizationManager = Mockito.mock(ILocalizationManager.class);
        Properties properties = Mockito.mock(Properties.class);
        this.instance.setLocalizationManager(localizationManager);
        Mockito.when(this.m.traitement.getNom()).thenReturn(MockUtils.TRAITEMENT);
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.empty());
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(null);
        Mockito.when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        Mockito.when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        Mockito.when(properties.getProperty(MockUtils.PARCELLE_NOM, MockUtils.PARCELLE_NOM))
                .thenReturn(MockUtils.PARCELLE_NOM);
        Mockito.when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn(
                MockUtils.SITE_NOM);
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.verify(this.lineRecord, Mockito.never()).setSuiviParcelle(this.m.suiviParcelle);
        Mockito.verify(this.lineRecord, Mockito.never()).setTraitementProgramme(this.m.traitement);
        Mockito.verify(this.lineRecord, Mockito.never()).setVersionDeTraitement(
                this.m.versionDeTraitement);
        Mockito.verify(this.lineRecord, Mockito.never()).setDateDebuttraitementSurParcelle(
                this.m.dateDebut);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Il n'existe pas de suivi de parcelle pour la date 01/01/2012, sur la parcelle Parcelle du site Site;\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitementNullVersionTraitement() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        ILocalizationManager localizationManager = Mockito.mock(ILocalizationManager.class);
        Properties properties = Mockito.mock(Properties.class);
        this.instance.setLocalizationManager(localizationManager);
        Mockito.when(this.m.traitement.getNom()).thenReturn(MockUtils.TRAITEMENT);
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.of(this.m.suiviParcelle));
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.empty());
        Mockito.when(
                localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA,
                        TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE)).thenReturn(properties);
        Mockito.when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        Mockito.when(
                properties.getProperty(MockUtils.TRAITEMENT_AFFICHAGE,
                        MockUtils.TRAITEMENT_AFFICHAGE)).thenReturn(MockUtils.TRAITEMENT_AFFICHAGE);
        Mockito.when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn(
                MockUtils.SITE_NOM);
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesBiomasse);
        Mockito.verify(this.lineRecord).setSuiviParcelle(this.m.suiviParcelle);
        Mockito.verify(this.lineRecord).setTraitementProgramme(this.m.traitement);
        Mockito.verify(this.lineRecord, Mockito.never()).setVersionDeTraitement(
                this.m.versionDeTraitement);
        Mockito.verify(this.lineRecord, Mockito.never()).setDateDebuttraitementSurParcelle(
                this.m.dateDebut);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Il n'existe pas de version de traitement pour la date 01/01/2012, pour le traitement traitement (Traitement) du site Site;\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class
     * AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitement() {
        ErrorsReport errorsReport = new ErrorsReport();
        instance.setLocalizationManager(MockUtils.localizationManager);
        final String affichage = "affichage";
        when(MockUtils.localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, affichage)).thenReturn(properties);
        when(MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(MockUtils.localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, affichage)).thenReturn(properties);
        when(MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        when(properties.getProperty(affichage, affichage)).thenReturn("un affichage");
        when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn("un nom de site");
        when(properties.getProperty(MockUtils.PARCELLE_NOM, MockUtils.PARCELLE_NOM)).thenReturn("un nom de parcelle");
        //nominal
        when(lineRecord.getParcelle()).thenReturn(m.parcelle);
        when(lineRecord.getDate()).thenReturn(m.dateDebut);
        when(m.suiviParcelle.getTraitement()).thenReturn(m.traitement);
        when(lineRecord.getTraitementProgramme()).thenReturn(m.traitement);
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(m.parcelle, m.dateDebut)).thenReturn(Optional.of(m.suiviParcelle));
        when(m.suiviParcelle.getDateDebutTraitement()).thenReturn(m.dateFin);
        when(m.versionDeTraitementDAO
                .retrieveVersiontraitement(m.traitement, m.dateDebut)).thenReturn(Optional.of(m.versionDeTraitement));
        instance.updateVersionDeTraitement(lineRecord, errorsReport, 2L, 1, datasetDescriptorACBB, requestPropertiesBiomasse);
        verify(lineRecord).setSuiviParcelle(m.suiviParcelle);
        verify(lineRecord).setTraitementProgramme(m.traitement);
        verify(lineRecord).setVersionDeTraitement(m.versionDeTraitement);
        verify(lineRecord).setDateDebuttraitementSurParcelle(m.dateFin);
        //with null versiondetraitement
        when(m.versionDeTraitementDAO
                .retrieveVersiontraitement(m.traitement, m.dateDebut)).thenReturn(Optional.empty());
        when(m.traitement.getAffichage()).thenReturn(affichage);
        when(m.traitement.getNom()).thenReturn("nom du traitement");
        instance.updateVersionDeTraitement(lineRecord, errorsReport, 2L, 1, datasetDescriptorACBB, requestPropertiesBiomasse);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Il n'existe pas de version de traitement pour la date 01/01/2012, pour le traitement nom du traitement (un affichage) du site un nom de site;\n", errorsReport.getErrorsMessages());
        //with null suiviParcelle
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(m.parcelle, m.dateDebut)).thenReturn(Optional.empty());
        errorsReport = new ErrorsReport();
        instance.updateVersionDeTraitement(lineRecord, errorsReport, 2L, 1, datasetDescriptorACBB, requestPropertiesBiomasse);
        assertTrue(errorsReport.hasErrors());
        assertEquals("-Il n'existe pas de suivi de parcelle pour la date 01/01/2012, sur la parcelle un nom de parcelle du site un nom de site;\n", errorsReport.getErrorsMessages());

    }

    /**
     * Test of readMeasureNumber method, of class
     * ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadMeasureNumber() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"2"};
        CleanerValues cleanerValues = new CleanerValues(values);
        AbstractVariableValueBiomasse variableValue = Mockito
                .mock(AbstractVariableValueBiomasse.class);
        // nominal
        int result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue).setMeasureNumber(2);
        // empty mesearenumber
        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues, variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(1)).setMeasureNumber(1);
        Assert.assertFalse(errorsReport.hasErrors());
        // null mesearenumber
        values = new String[]{null};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues, variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(2)).setMeasureNumber(1);
        Assert.assertFalse(errorsReport.hasErrors());
        // bad mesearenumber
        values = new String[]{"deux"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMeasureNumber(errorsReport, 1L, 0, cleanerValues, variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(2)).setMeasureNumber(1);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, colonne 1, le nombre de mesure \"deux\" doit être un entier positif.\n",
                errorsReport.getErrorsMessages());

    }

    /**
     * Test of readStandardDeviation method, of class
     * ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadStandardDeviation() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"2.3"};
        CleanerValues cleanerValues = new CleanerValues(values);
        AbstractVariableValueBiomasse variableValue = Mockito
                .mock(AbstractVariableValueBiomasse.class);
        // nominal
        int result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(1)).setStandardDeviation(2.3F);
        Assert.assertFalse(errorsReport.hasErrors());
        // empty standart deviation
        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(1)).setStandardDeviation(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        Assert.assertFalse(errorsReport.hasErrors());
        // null standart deviation
        values = new String[]{null};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(2)).setStandardDeviation(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        Assert.assertFalse(errorsReport.hasErrors());
        // bad standart deviation
        values = new String[]{"deux"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readStandardDeviation(errorsReport, 1L, 0, cleanerValues,
                variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(2)).setStandardDeviation(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals("-Ligne 1, colonne 1, l'écart-type \"deux\" doit être un décimal.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readQualityIndex method, of class ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadQualityIndex() {
        String[] values = new String[]{org.apache.commons.lang.StringUtils.EMPTY, "2", "deux", "-5"};
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(values);
        VariableValueBiomassProduction variableValue = Mockito
                .mock(VariableValueBiomassProduction.class);
        //null qualityIndex
        int result = instance.readQualityIndex(errorsReport, 2L, 1, cleanerValues, variableValue);
        assertTrue(2 == result);
        verify(variableValue).setQualityClass(ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        //good qualityIndex
        result = instance.readQualityIndex(errorsReport, 2L, 2, cleanerValues, variableValue);
        assertTrue(3 == result);
        verify(variableValue).setQualityClass(2);
        //bad qualityIndex
        result = instance.readQualityIndex(errorsReport, 4L, 3, cleanerValues, variableValue);
        assertTrue(4 == result);
        assertTrue(errorsReport.hasErrors());
        Assert.assertEquals("-Ligne 4, colonne 4, l'indice de qualité \"deux\" doit être un entier positif.\n", errorsReport.getErrorsMessages());
        //negative qualityIndex
        errorsReport = new ErrorsReport();
        result = instance.readQualityIndex(errorsReport, 5L, 4, cleanerValues, variableValue);
        assertTrue(5 == result);
        assertTrue(errorsReport.hasErrors());
        Assert.assertEquals("-Ligne 5, colonne 5, l'indice de qualité \"-5\" doit être un entier positif.\n", errorsReport.getErrorsMessages());
    }

    /**
     * Test of readValue method, of class ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadValue() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"12.0", "2", "11.4", "102", "2"};
        VariableValueBiomassProduction variableValue = Mockito
                .mock(VariableValueBiomassProduction.class);
        CleanerValues cleanerValues = new CleanerValues(values);
        int result = instance.readValue(cleanerValues, variableValue, errorsReport, 1L, 1);
        assertTrue(2 == result);
        verify(variableValue).setValue("12.0");
        result = instance.readValue(cleanerValues, variableValue, errorsReport, 1L, 2);
        assertTrue(3 == result);
        verify(variableValue).setValue("2");
    }

    /**
     *
     */
    @Test
    public void testReadVariableFromColumn() {
        when(column1.getName()).thenReturn("variable_moyenne");
        Mockito.doReturn(Optional.of(variableACBB1)).when(m.variableDAO).getByAffichage("variable");
        VariableACBB result = instance.readVariableFromColumn(column1).orElse(null);
        verify(m.variableDAO).getByAffichage("variable");
        assertEquals(variableACBB1, result);
    }

    /**
     * Test of readMediane method, of class AbstractProcessRecordBiomasse.
     */
    /*@Test
    public void testReadMediane() {
    System.out.println("readMediane");
    ErrorsReport errorsReport = new ErrorsReport();
    final String mediane = "mediane";
    String[] values = new String[] { mediane};
    CleanerValues cleanerValues = new CleanerValues(values);
    AbstractLineRecord line = Mockito.mock(AbstractLineRecord.class);
    // nominal
    int result = this.instance.readMediane(2, cleanerValues, vv1);
    Assert.assertEquals(1, result);
    Mockito.verify(line).setNatureCouvert(mediane);
    }*/

    /**
     *
     */
    public class AbstractProcessRecordBiomasseImpl extends AbstractProcessRecordBiomasse {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        Stack<VariableACBB> variableACBBs = new Stack();

        /**
         *
         */
        public AbstractProcessRecordBiomasseImpl() {
            this.variableACBBs.push(variableACBB2);
            this.variableACBBs.push(variableACBB1);
        }

        /**
         * @param version
         * @param lines
         * @param errorsReport
         * @param requestPropertiesBiomasse
         * @throws PersistenceException
         */
        @Override
        public void buildNewLines(VersionFile version, List<? extends AbstractLineRecord> lines,
                                  ErrorsReport errorsReport, IRequestPropertiesBiomasse requestPropertiesBiomasse)
                throws PersistenceException {
        }

        /**
         * @param colonneEntry
         * @return
         * @throws PersistenceException
         */
        @Override
        protected Optional<VariableACBB> getVariable(Map.Entry<Integer, Column> colonneEntry)
                throws PersistenceException {
            VariableACBB variable = this.variableACBBs.isEmpty() ? null : this.variableACBBs.pop();
            return Optional.of(variable);
        }
    }

    /**
     *
     */
    public class AbstractProcessRecordBiomasseImplNoVariable extends AbstractProcessRecordBiomasse {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * @param version
         * @param lines
         * @param errorsReport
         * @param requestPropertiesBiomasse
         * @throws PersistenceException
         */
        @Override
        public void buildNewLines(VersionFile version, List<? extends AbstractLineRecord> lines,
                                  ErrorsReport errorsReport, IRequestPropertiesBiomasse requestPropertiesBiomasse)
                throws PersistenceException {
        }
    }

}
