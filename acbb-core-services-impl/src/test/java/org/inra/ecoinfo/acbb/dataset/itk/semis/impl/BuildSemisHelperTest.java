/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IMesureSemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisAttrDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.junit.*;

import java.util.List;

/**
 * @author ptcherniati
 */
@Ignore
public class BuildSemisHelperTest {

    /**
     *
     */
    public BuildSemisHelperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addMesuresSemis method, of class BuildSemisHelper.
     */
    @Test
    public void testAddMesuresSemis() {
        LineRecord line = null;
        Semis semis = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.addMesuresSemis(line, semis);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of addValeurSemis method, of class BuildSemisHelper.
     */
    @Test
    public void testAddValeurSemis() {
        MesureSemis mesureSemis = null;
        List<VariableValue> variableValues = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.addValeurSemis(mesureSemis, variableValues);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of build method, of class BuildSemisHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild_0args() throws Exception {
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.build();
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of build method, of class BuildSemisHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild_4args() throws Exception {
        VersionFile version = null;
        List<LineRecord> lines = null;
        ErrorsReport errorsReport = null;
        IRequestPropertiesITK requestPropertiesITK = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.build(version, lines, errorsReport, requestPropertiesITK);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of createNewMeadowYear method, of class BuildSemisHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateNewMeadowYear() throws Exception {
        int annee = 0;
        LineRecord line = null;
        AbstractIntervention semis = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.createNewMeadowYear(annee, line, semis);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of createNewSemis method, of class BuildSemisHelper.
     */
    @Test
    public void testCreateNewSemis() {
        LineRecord line = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        Semis expResult = null;
        Semis result = instance.createNewSemis(line);
        Assert.assertEquals(expResult, result);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of generateTemporariesMeadows method, of class BuildSemisHelper.
     */
    @Test
    public void testGenerateTemporariesMeadows() {
        LineRecord line = null;
        AbstractIntervention semis = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.generateTemporariesMeadows(line, semis);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of getOrCreateMesureSemis method, of class BuildSemisHelper.
     */
    @Test
    public void testGetOrCreateMesureSemis() {
        LineRecord line = null;
        Semis semis = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        MesureSemis expResult = null;
        MesureSemis result = instance.getOrCreateMesureSemis(line, semis);
        Assert.assertEquals(expResult, result);
        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of getOrCreateSemis method, of class BuildSemisHelper.
     */
    @Test
    public void testGetOrCreateSemis() {
        LineRecord line = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        Semis expResult = null;
        Semis result = instance.getOrCreateSemis(line);
        Assert.assertEquals(expResult, result);

        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of setMesureSemisDAO method, of class BuildSemisHelper.
     */
    @Test
    public void testSetMesureSemisDAO() {
        IMesureSemisDAO mesureSemisDAO = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.setMesureSemisDAO(mesureSemisDAO);

        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of setSemisDAO method, of class BuildSemisHelper.
     */
    @Test
    public void testSetSemisDAO() {
        ISemisDAO semisDAO = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.setSemisDAO(semisDAO);

        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of setTempoDAO method, of class BuildSemisHelper.
     */
    @Test
    public void testSetTempoDAO() {
        ITempoDAO tempoDAO = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.setTempoDAO(tempoDAO);

        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of setValeurSemisAttrDAO method, of class BuildSemisHelper.
     */
    @Test
    public void testSetValeurSemisAttrDAO() {
        IValeurSemisAttrDAO valeurSemisAttrDAO = null;
        BuildSemisHelper instance = new BuildSemisHelper();
        instance.setValeurSemisAttrDAO(valeurSemisAttrDAO);

        Assert.fail("The test case is a prototype.");
    }

}
