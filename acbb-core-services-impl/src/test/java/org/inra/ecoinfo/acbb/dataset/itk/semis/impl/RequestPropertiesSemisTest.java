/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.junit.*;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class RequestPropertiesSemisTest {

    RequestPropertiesSemis instance;
    ITKMockUtils m = ITKMockUtils.getInstance();

    /**
     *
     */
    public RequestPropertiesSemisTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new RequestPropertiesSemis();
        this.instance.setDatasetConfiguration(this.m.datasetConfiguration);
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getNomDeFichier method, of class RequestPropertiesSemis.
     */
    @Test
    public void testGetNomDeFichier() {
        String result = this.instance.getNomDeFichier(this.m.versionFile);
        Assert.assertEquals("agro-écosysteme/site_semis_01-01-2012_31-12-2012.csv", result);
    }

    /**
     * Test of getTestDuplicates method, of class RequestPropertiesSemis.
     */
    @Test
    public void testGetTestDuplicates() {
        ITestDuplicates result = this.instance.getTestDuplicates();
        ITestDuplicates result2 = this.instance.getTestDuplicates();
        Assert.assertNotNull(result);
        Assert.assertTrue(result instanceof TestDuplicateSem);
        Assert.assertNotNull(result2);
        Assert.assertNotEquals(result, result2);
    }

}
