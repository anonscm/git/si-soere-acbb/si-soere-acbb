/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ptcherniati
 */
public class TestHeadersSemisTest {

    TestHeadersSemis instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    BadsFormatsReport badsFormatsReport = new BadsFormatsReport("error");
    @Mock
    DatasetDescriptor datasetDescriptor;
    @Mock
    Column column;
    @Mock
    Column column0;
    @Mock
    Column column1;
    @Mock
    Column column2;
    @Mock
    Column column3;
    List<Column> columns = new LinkedList();
    RequestPropertiesSemis requestPropertiesSemis = new RequestPropertiesSemis();
    /**
     *
     */
    public TestHeadersSemisTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     * @return
     */
    protected TestHeadersSemis initInstance(TestHeadersSemis instance) {
        instance.setDatatypeName("datatypename");
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSiteDAO(this.m.siteDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setDatasetConfiguration(this.m.datasetConfiguration);
        return instance;
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new TestHeadersSemis();
        MockitoAnnotations.initMocks(this);
        this.initInstance(this.instance);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * @throws Exception
     */
    @Test
    public void testNameColumnsTet() throws Exception {
        Mockito.when(this.datasetDescriptor.getUndefinedColumn()).thenReturn(2);
        this.columns = Arrays.asList(this.column, this.column, this.column,
                this.column);
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        Mockito.when(this.column.getName()).thenReturn("column1", "column2", "column3", "column4");
        long lineNumber = 2L;
        String[] values = new String[]{"column1", "column2", "column3", "column4"};
        this.instance.testNameColumns(this.datasetDescriptor, values, this.badsFormatsReport,
                lineNumber);
        Mockito.verify(this.column, Mockito.times(2)).getName();
        // test with error
        Mockito.when(this.column.getName()).thenReturn("column3", "column4");
        this.instance.testNameColumns(this.datasetDescriptor, values, this.badsFormatsReport,
                lineNumber);
        Assert.assertTrue(this.badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "error :- Ligne 2 : L'intitulé de la colonne 1 est \"column1\", alors que \"column3\" est attendu - Ligne 2 : L'intitulé de la colonne 2 est \"column2\", alors que \"column4\" est attendu ",
                this.badsFormatsReport.getMessages());
        Mockito.verify(this.column, Mockito.times(4)).getName();
    }

    /**
     * Test of readLineHeader method, of class TestHeadersSemis.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLineHeader() throws Exception {
        String text = "column0;column1;column2;column3\n";
        this.instance = new TestHeadersSemisImpl();
        this.initInstance(this.instance);
        Mockito.when(this.datasetDescriptor.getUndefinedColumn()).thenReturn(0);
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        this.columns.add(this.column0);
        this.columns.add(this.column1);
        this.columns.add(this.column2);
        this.columns.add(this.column3);
        Mockito.when(this.column0.getName()).thenReturn("column0");
        Mockito.when(this.column1.getName()).thenReturn("column1");
        Mockito.when(this.column2.getName()).thenReturn("column2");
        Mockito.when(this.column3.getName()).thenReturn("column3");
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        long lineNumber = 2L;
        long result = this.instance.readLineHeader(this.badsFormatsReport, parser, lineNumber,
                this.datasetDescriptor, this.requestPropertiesSemis);
        Assert.assertFalse(this.badsFormatsReport.hasErrors());
        Assert.assertTrue(3L == result);
        // with mismatch column
        text = "column0;column1;column3;column2\n";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        result = this.instance.readLineHeader(this.badsFormatsReport, parser, lineNumber,
                this.datasetDescriptor, this.requestPropertiesSemis);
        Assert.assertTrue(this.badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "error :- Ligne 3 : L'intitulé de la colonne 3 est \"column3\", alors que \"column2\" est attendu - Ligne 3 : L'intitulé de la colonne 4 est \"column2\", alors que \"column3\" est attendu ",
                this.badsFormatsReport.getMessages());
    }

    class TestHeadersSemisImpl extends TestHeadersSemis {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        @Override
        protected void testNameColumns(DatasetDescriptor datasetDescriptor, String[] values,
                                       BadsFormatsReport badsFormatsReport, long returnLineNumber) {
        }

    }

}
