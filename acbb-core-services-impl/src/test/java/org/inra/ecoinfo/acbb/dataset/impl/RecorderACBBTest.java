/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.*;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.*;

import java.time.LocalDateTime;

/**
 * @author ptcherniati
 */
public class RecorderACBBTest {

    @Mock
    IDeleteRecord deleteRecord;
    @Mock
    IProcessRecord processRecord;
    @Mock
    ITestFormat testFormat;
    @Spy
    RecorderACBB instance;
    @Mock
    IRequestPropertiesACBB requestPropertiesACBB;
    @Mock
    DatasetDescriptorACBB datasetDescriptorACBB;
    @Mock
    ILocalizationManager localizationManager;
    private MockUtils m;
    /**
     *
     */
    public RecorderACBBTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.m = MockUtils.getInstance();
        RecorderACBB.setStaticLocalizationManager(MockUtils.localizationManager);
        this.instance.setDeleteRecord(this.deleteRecord);
        this.instance.setProcessRecord(this.processRecord);
        this.instance.setTestFormat(this.testFormat);
        this.instance.setDatasetDescriptor(this.datasetDescriptorACBB);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecords method, of class RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecords() throws Exception {
        VersionFile versionFile = this.m.versionFile;
        this.instance.deleteRecords(versionFile);
        Mockito.verify(this.deleteRecord).deleteRecord(versionFile);
    }

    /**
     * Test of getACBBMessage method, of class RecorderACBB.
     */
    @Test
    public void testGetACBBMessage() {
        String key = "PROPERTY_MSG_MISMACH_COLUMN_HEADER";
        String expResult = "Ligne %d : L'intitulé de la colonne %d est \"%s\", alors que \"%s\" est attendu";
        String result = RecorderACBB.getACBBMessage(key);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getACBBMessageWithBundle method, of class RecorderACBB.
     */
    @Test
    public void testGetACBBMessageWithBundle() {
        String expResult = "La profondeur indiquée doit être un entier positif.";
        final String key = "PROPERTY_MSG_BAD_DEPTH";
        final String bundle = "org.inra.ecoinfo.acbb.extraction.messages";
        String result = RecorderACBB.getACBBMessageWithBundle(bundle, key);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getDateTimeStringFromDateStringaAndTimeString method, of class
     * RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetDateTimeStringFromDateStringaAndTimeString() throws Exception {
        String dateString = "25/12/2012";
        String[] times = new String[]{"23:53", "23:53:00", "23:53:25"};
        String[] results = new String[]{"25/12/2012 23:53:00", "25/12/2012 23:53:00", "25/12/2012 23:53:25"};
        for (int i = 0; i < times.length; i++) {
            String timeString = times[i];
            String expectedResult = results[i];
            LocalDateTime expResult = DateUtil.readLocalDateTimeFromText("dd/MM/yyyy HH:mm:ss", expectedResult);
            LocalDateTime result = RecorderACBB.getDateTimeLocalFromDateStringaAndTimeString(dateString, timeString);
            Assert.assertEquals(expResult, result);

        }
    }

    /**
     * Test of getIntervalDateLocaleForFile method, of class RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateLocaleForFile() throws Exception {
        String beginDateddmmyyhhmmss = "25-12-2014-235312";
        String endDateddmmyyhhmmss = "01-01-2015-000101";
        IntervalDate result = RecorderACBB.getIntervalDateLocaleForFile(beginDateddmmyyhhmmss,
                endDateddmmyyhhmmss);
        Assert.assertEquals(beginDateddmmyyhhmmss, result.getBeginDateToString());
        Assert.assertEquals(endDateddmmyyhhmmss, result.getEndDateToString());
        //Assert.assertEquals(TimeZone.getDefault(), result.getDateFormat().getTimeZone());
    }

    /**
     * Test of getIntervalDateUTCForFile method, of class RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateUTCForFile() throws Exception {
        String beginDateddmmyyhhmmss = "25-12-2014-235312";
        String endDateddmmyyhhmmss = "01-01-2015-000101";
        IntervalDate result = RecorderACBB.getIntervalDateUTCForFile(beginDateddmmyyhhmmss,
                endDateddmmyyhhmmss);
        Assert.assertEquals(beginDateddmmyyhhmmss, result.getBeginDateToString());
        Assert.assertEquals(endDateddmmyyhhmmss, result.getEndDateToString());
        //Assert.assertEquals(new SimpleTimeZone(0, DateUtil.GMT), result.getDateFormat().getTimeZone());
    }

    /**
     * Test of isParcelleDatatype method, of class RecorderACBB.
     */
    @Test
    public void testIsParcelleDatatype() {
        RecorderACBB instance = new RecorderACBB();
        boolean result = instance.isParcelleDatatype();
        Assert.assertFalse(result);
        instance.setParcellDatatype(true);
        result = instance.isParcelleDatatype();
        Assert.assertTrue(result);
    }

    /**
     * Test of record method, of class RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRecord() throws Exception {
        VersionFile versionFile = this.m.versionFile;
        String fileEncoding = "UTF8";
        Mockito.when(this.m.versionFile.getData()).thenReturn("data;autre data".getBytes());
        this.instance.setDatasetDescriptorPath("datasetDescriptorPath.xml");
        Mockito.doReturn(this.requestPropertiesACBB).when(this.instance).getRequestProperties();
        Mockito.doReturn(this.localizationManager).when(this.instance).getLocalizationManager();
        Mockito.when(
                this.localizationManager.getMessage(RecorderACBB.ACBB_DATASET_BUNDLE_NAME,
                        RecorderACBB.PROPERTY_MSG_ERROR_BAD_FORMAT)).thenReturn("message");
        ArgumentCaptor<CSVParser> parser = ArgumentCaptor.forClass(CSVParser.class);
        ArgumentCaptor<VersionFile> vf = ArgumentCaptor.forClass(VersionFile.class);
        ArgumentCaptor<IRequestPropertiesACBB> sp = ArgumentCaptor
                .forClass(IRequestPropertiesACBB.class);
        ArgumentCaptor<String> fe = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<DatasetDescriptorACBB> ds = ArgumentCaptor
                .forClass(DatasetDescriptorACBB.class);
        Mockito.doNothing()
                .when(this.instance)
                .processRecord(parser.capture(), vf.capture(), sp.capture(), fe.capture(),
                        ds.capture());
        this.instance.record(versionFile, fileEncoding);
        // Mockito.verify(processRecord).processRecord(parser.capture(),
        // vf.capture(), sp.capture(), fe.capture(), ds.capture());
        Assert.assertEquals("data", parser.getValue().nextValue());
        Assert.assertEquals("autre data", parser.getValue().nextValue());
        Assert.assertEquals(this.m.versionFile, vf.getValue());
        Assert.assertEquals(this.requestPropertiesACBB, sp.getValue());
        Assert.assertEquals("UTF8", fe.getValue());
        Assert.assertEquals("fertilisation", ds.getValue().getName());
    }

    /**
     * Test of setLocalizationManager method, of class RecorderACBB.
     */
    @Test
    public void testSetLocalizationManager() {
        RecorderACBB instance = new RecorderACBB();
        instance.setLocalizationManager(this.localizationManager);
        Assert.assertEquals(this.localizationManager, instance.getLocalizationManager());
    }

    /**
     * Test of setParcellDatatype method, of class RecorderACBB.
     */
    @Test
    public void testSetParcellDatatype() {
        boolean parcellDatatype = true;
        RecorderACBB instance = new RecorderACBB();
        Assert.assertFalse(instance.isParcelleDatatype());
        instance.setParcellDatatype(parcellDatatype);
        Assert.assertTrue(instance.isParcelleDatatype());
    }

    /**
     * Test of testFormat method, of class RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestFormat_5args() throws Exception {
        CSVParser parser = Mockito.mock(CSVParser.class);
        String encoding = "UTF-8";
        this.instance.testFormat(parser, this.m.versionFile, this.requestPropertiesACBB, encoding,
                this.datasetDescriptorACBB);
        Mockito.verify(this.testFormat).testFormat(parser, this.m.versionFile,
                this.requestPropertiesACBB, encoding, this.datasetDescriptorACBB);
    }

    /**
     * Test of testHeader method, of class RecorderACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeader() throws Exception {
        BadsFormatsReport badsFormatsReport = null;
        CSVParser parser = null;
        RecorderACBB instance = new RecorderACBB();
        int expResult = 0;
        int result = instance.testHeader(badsFormatsReport, parser);
        Assert.assertEquals(expResult, result);
    }

}
