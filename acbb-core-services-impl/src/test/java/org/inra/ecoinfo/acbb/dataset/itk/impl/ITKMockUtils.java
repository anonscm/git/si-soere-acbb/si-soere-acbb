/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.config.impl.DatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import java.time.DateTimeException;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author ptcherniati
 */
public class ITKMockUtils extends MockUtils {

    /**
     * rotation 3ans blé, mais, orge +3 ans prairie
     */
    @Mock
    public IRequestPropertiesITK requestPropertiesITK;
    /**
     *
     */
    public Rotation rotation3_3;
    /**
     *
     */
    public TraitementProgramme traitement3_3;
    /**
     *
     */
    public SuiviParcelle suiviPracelle3_3;
    /**
     *
     */
    public VersionDeTraitement versionTraitement3_3;
    /**
     * rotation 3ans blé, mais, orge
     */
    public Rotation rotation3;
    /**
     *
     */
    public TraitementProgramme traitement3;
    /**
     *
     */
    public SuiviParcelle suiviPracelle3;
    /**
     *
     */
    public VersionDeTraitement versionTraitement3;
    /**
     * rotation 3ans blé, mais, orge
     */
    public Rotation rotationPrairie3;
    /**
     *
     */
    public TraitementProgramme traitementPrairie3;
    /**
     *
     */
    public SuiviParcelle suiviPracellePrairie3;
    /**
     *
     */
    public VersionDeTraitement versionTraitementPrairie3;
    /**
     * rotation prairie>20
     */
    public Rotation rotationN;
    /**
     *
     */
    public TraitementProgramme traitementN;
    /**
     *
     */
    public SuiviParcelle suiviPracelleN;
    /**
     *
     */
    public VersionDeTraitement versionTraitementN;
    /**
     * autre que rotation
     */
    public Modalite nonRotation;
    /**
     *
     */
    public TraitementProgramme nonTraitement;
    /**
     *
     */
    public SuiviParcelle nonSuiviPracelle;
    /**
     *
     */
    public VersionDeTraitement nonVersionTraitement;
    /**
     *
     */
    @Mock
    public AbstractIntervention intervention1;
    /**
     *
     */
    @Mock
    public VersionFile version1;
    /**
     *
     */
    @Mock
    public Dataset dataset1;
    /**
     *
     */
    @Mock
    public AbstractIntervention intervention2;
    /**
     *
     */
    @Mock
    public VersionFile version2;
    /**
     *
     */
    @Mock
    public Dataset dataset2;
    /**
     *
     */
    @Mock
    public DatasetConfiguration configuration;

    /**
     * @return
     */
    public static ITKMockUtils getInstance() {
        ITKMockUtils instance = new ITKMockUtils();
        MockitoAnnotations.initMocks(instance);

        try {

            instance.initMocks();

        } catch (DateTimeException | BadExpectedValueException ex) {

            return null;

        }
        instance.initLocalization();

        return instance;
    }

    private Rotation getRotation(String nom, String code, String description) {
        Rotation rotation = new Rotation();
        rotation.setNom(nom);
        rotation.setCode(code);
        rotation.setDescription(description);
        return rotation;
    }

    /**
     * @throws DateTimeException
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Override
    public void initMocks() throws DateTimeException, BadExpectedValueException {

        super.initMocks();
        this.initModalites();
        this.initRequestProperties();
        initInterventions();
        ACBBUtils.localizationManager = localizationManager;
        try {

            Mockito.when(this.versionFileDAO.merge(this.versionFile)).thenReturn(this.versionFile);

        } catch (PersistenceException ex) {
            LoggerFactory.getLogger(ITKMockUtils.class.getName()).info(ex.getMessage(), ex);
        }

    }

    private void initModalites() {
        this.rotation3_3 = this.getRotation("blé, maïs, orge, PT, PT, PT ", "R1",
                "rotation de 3 ans de cultures annuelles suivie de 3 ans de prairie temporaire");
        this.traitement3_3 = new TraitementProgramme("t1", "t1",
                "rotation de 3 ans de cultures annuelles suivie de 3 ans de prairie temporaire",
                "2", this.dateDebut, this.dateFin);
        this.suiviPracelle3_3 = new SuiviParcelle(this.traitement3_3, this.parcelle, this.dateDebut);
        SortedSet<Modalite> s1 = new TreeSet();
        s1.add(this.rotation3_3);
        this.versionTraitement3_3 = new VersionDeTraitement(1, this.dateDebut, this.dateFin,
                MockUtils.DATE_DEBUT, this.traitementPrairie3, s1);
        this.rotation3 = this.getRotation("blé, maïs, orge, PT, PT, PT ", "R1",
                "rotation de 3 ans de cultures annuelles suivie de 3 ans de prairie temporaire");
        this.traitement3 = new TraitementProgramme("t2", "t2",
                "rotation de 3 ans de cultures annuelles", "2", this.dateDebut, this.dateFin);
        this.suiviPracelle3 = new SuiviParcelle(this.traitement3, this.parcelle, this.dateDebut);
        SortedSet<Modalite> s2 = new TreeSet();
        s2.add(this.rotation3);
        this.versionTraitement3 = new VersionDeTraitement(1, this.dateDebut, this.dateFin,
                MockUtils.DATE_DEBUT, this.traitementPrairie3, s2);
        this.rotationPrairie3 = this.getRotation("switchgrass", "R3",
                "rotation de 3 ans de switchgrass");
        this.traitementPrairie3 = new TraitementProgramme("t3", "t3",
                "rotation de 3 ans de switchgrass", "2", this.dateDebut, this.dateFin);
        this.suiviPracellePrairie3 = new SuiviParcelle(this.traitementPrairie3, this.parcelle,
                this.dateDebut);
        SortedSet<Modalite> s3 = new TreeSet();
        s3.add(this.rotationPrairie3);
        this.versionTraitementPrairie3 = new VersionDeTraitement(1, this.dateDebut, this.dateFin,
                MockUtils.DATE_DEBUT, this.traitementPrairie3, s3);
        this.rotationN = this.getRotation("PT>20", "R4", "prairie permanente témoin");
        this.traitementN = new TraitementProgramme("t4", "t4", "prairie permanente témoin", "2",
                this.dateDebut, this.dateFin);
        this.suiviPracelleN = new SuiviParcelle(this.traitementN, this.parcelle, this.dateDebut);
        SortedSet<Modalite> s4 = new TreeSet();
        s4.add(this.rotationN);
        this.versionTraitementN = new VersionDeTraitement(1, this.dateDebut, this.dateFin,
                MockUtils.DATE_DEBUT, this.traitementPrairie3, s4);
        this.nonRotation = new Modalite("Pature", "U1", "prairie permanente");
        this.nonTraitement = new TraitementProgramme("t5", "t5", "prairie permanente", "2",
                this.dateDebut, this.dateFin);
        this.nonSuiviPracelle = new SuiviParcelle(this.nonTraitement, this.parcelle, this.dateDebut);
        SortedSet<Modalite> s5 = new TreeSet();
        s5.add(this.nonRotation);
        this.nonVersionTraitement = new VersionDeTraitement(1, this.dateDebut, this.dateFin,
                MockUtils.DATE_DEBUT, this.traitementPrairie3, s5);
    }

    private void initRequestProperties() {
        Mockito.when(this.requestPropertiesITK.getSite()).thenReturn(this.site);
    }

    private void initInterventions() {
        Mockito.when(intervention1.getVersion()).thenReturn(version1);
        Mockito.when(intervention1.getDate()).thenReturn(dateDebut);
        Mockito.when(version1.getDataset()).thenReturn(dataset1);
        Mockito.when(version1.getVersionNumber()).thenReturn(1L);
        Mockito.when(dataset1.buildDownloadFilename(null)).thenReturn("file 1");
        Mockito.when(intervention2.getDate()).thenReturn(dateFin);
        Mockito.when(intervention2.getVersion()).thenReturn(version2);
        Mockito.when(version2.getDataset()).thenReturn(dataset2);
        Mockito.when(dataset2.buildDownloadFilename(null)).thenReturn("file 2");
        Mockito.when(version2.getVersionNumber()).thenReturn(2L);
    }

}
