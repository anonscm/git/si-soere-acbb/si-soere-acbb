/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import com.Ostermiller.util.CSVParser;
import mockit.Mocked;
import mockit.Verifications;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.Paturage;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.IMasseVegClassDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.IMethodeAnalyseDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.MethodeAnalyse;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.IMethodeProductionDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction.MethodeProduction;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.BiomassMockUtils;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.*;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class ProcessRecordBiomassProductionTest {

    BiomassMockUtils m = BiomassMockUtils.getInstance();
    ProcessRecordBiomassProduction instance;
    @Mock
    IInterventionDAO interventionDAO;
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    IListeItineraireDAO listeItineraireDAO;
    @Mock
    IMasseVegClassDAO masseVegClassDAO;
    @Mock
    MasseVegClass masseVegClass;
    @Mock
    IMethodeAnalyseDAO methodeAnalyseDAO;
    @Mock
    IMethodeProductionDAO methodeProductionDAO;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    List<DatatypeVariableUniteACBB> variables;
    @Mock
    InterventionType paturageType;
    @Mock
    Paturage paturage;
    @Mock
    MethodeAnalyse methode;
    List<BiomassProductionLineRecord> lines = new LinkedList();

    /**
     *
     */
    public ProcessRecordBiomassProductionTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    protected void initInstance(ProcessRecordBiomassProduction instance) {
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setInterventionDAO(this.interventionDAO);
        instance.setListeACBBDAO(this.listeVegetationDAO);
        instance.setListeItineraireDAO(this.listeItineraireDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setMasseVegClassDAO(this.masseVegClassDAO);
        instance.setMethodeAnalyseDAO(this.methodeAnalyseDAO);
        instance.setMethodeProductionDAO(this.methodeProductionDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setVersionFileDAO(this.m.versionFileDAO);
        when(methodeAnalyseDAO.getByNKey(any())).thenReturn(Optional.empty());
        when(methodeProductionDAO.getByNKey(any())).thenReturn(Optional.empty());
        when(methodeAnalyseDAO.getByNumberId(any())).thenReturn(Optional.empty());
        when(methodeProductionDAO.getByNumberId(any())).thenReturn(Optional.empty());
        when(masseVegClassDAO.getByNKey(any())).thenReturn(Optional.empty());
        when(interventionDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        when(listeItineraireDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        when(listeVegetationDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        this.instance = instance;
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.initInstance(new ProcessRecordBiomassProduction());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * @param dbVariables
     * @param abstractProcessRecord
     * @throws Exception
     */
    @Test
    public void testProcessRecord(@Mocked final List<VariableACBB> dbVariables,
                                  @Mocked final AbstractProcessRecord abstractProcessRecord) throws Exception {
        Mockito.when(this.datasetDescriptor.getEnTete()).thenReturn(4);
        Mockito.when(this.m.versionFileDAO.merge(this.m.versionFile))
                .thenReturn(this.m.versionFile);
        this.initInstance(new InstanceImpl());
        // nominal
        this.instance.processRecord(((InstanceImpl) this.instance).parser, this.m.versionFile,
                BiomassMockUtils.requestPropertiesBiomasse, "UTF-8", this.datasetDescriptor);
        Assert.assertTrue(((InstanceImpl) this.instance).errorsReport.hasInfos());
        Assert.assertEquals("-true\n-true\n",
                ((InstanceImpl) this.instance).errorsReport.getInfosMessages());
        new Verifications() {
            {
                abstractProcessRecord.processRecord(((InstanceImpl) instance).parser,
                        m.versionFile, BiomassMockUtils.requestPropertiesBiomasse, "UTF-8",
                        datasetDescriptor);
            }
        };
        Mockito.verify(this.m.versionFileDAO).merge(this.m.versionFile);
        // if errorreport
        try {
            this.instance.processRecord(((InstanceImpl) this.instance).parser, this.m.versionFile,
                    BiomassMockUtils.requestPropertiesBiomasse, "UTF-8", this.datasetDescriptor);
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals("org.inra.ecoinfo.utils.exceptions.PersistenceException: -error\n",
                    e.getMessage());
        }
    }

    /**
     * Test of readDateDebutrepousse method, of class
     * ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadDateDebutrepousse() throws EndOfCSVLine {
        String[] values = new String[]{MockUtils.DATE_DEBUT};
        CleanerValues cleanerValues = new CleanerValues(values);
        BiomassProductionLineRecord line = Mockito.mock(BiomassProductionLineRecord.class);
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        int result = this.instance.readDateDebutrepousse(cleanerValues, line, errorsReport, 1L, 0,
                this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse, values);
        Assert.assertTrue(1 == result);
        Assert.assertFalse(errorsReport.hasErrors());
        Mockito.verify(line).setDateDebutRepousse(this.m.dateDebut);
        // no value
        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY, "fin"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readDateDebutrepousse(cleanerValues, line, errorsReport, 1L, 0,
                this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse, values);
        Assert.assertFalse(errorsReport.hasErrors());
        Assert.assertTrue(1 == result);
        Mockito.verify(line).setDateDebutRepousse(this.m.dateDebut);
        Assert.assertEquals("fin", cleanerValues.nextToken());
        // no value
        values = new String[]{"baddate", "fin"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readDateDebutrepousse(cleanerValues, line, errorsReport, 1L, 0,
                this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse, values);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-\"baddate\" n'est pas un format de date valide à la ligne 1 colonne 1 (null). La date doit-être au format \"dd/MM/yyyy\"\n",
                errorsReport.getErrorsMessages());
        Assert.assertTrue(1 == result);
        Mockito.verify(line).setDateDebutRepousse(this.m.dateDebut);
        Assert.assertEquals("fin", cleanerValues.nextToken());
    }

    /**
     * Test of readLines method, of class ProcessRecordBiomassProduction.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLines() throws Exception {
        String text = MockUtils.PARCELLE_NOM + ";observation;1;2;3;paturage;"
                + MockUtils.DATE_DEBUT + ";série 1;masse vegetale;nature couvert;" + MockUtils.DATE_DEBUT
                + ";" + MockUtils.DATE_FIN + ";12.0;2;11.4;102;2";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        ErrorsReport errorsReport = new ErrorsReport();
        List<DatatypeVariableUniteACBB> dbVariables = Mockito.mock(List.class);
        DatatypeVariableUniteACBB dvu = Mockito.mock(DatatypeVariableUniteACBB.class);
        when(dvu.getVariable()).thenReturn(m.variable);
        Mockito.when(this.masseVegClassDAO.getByNKey("masse vegetale")).thenReturn(
                Optional.of(this.masseVegClass));
        Mockito.when(BiomassMockUtils.requestPropertiesBiomasse.getSite()).thenReturn(this.m.site);
        Mockito.when(this.m.parcelleDAO.getByNKey(contains(MockUtils.PARCELLE)))
                .thenReturn(Optional.of(this.m.parcelle));
        Mockito.when(dbVariables.get(13)).thenReturn(dvu);
        Mockito.when(dbVariables.get(13)).thenReturn(dvu);
        Mockito.when(
                this.listeItineraireDAO.getByNKey(InterventionType.INTERVENTION_TYPE, "paturage"))
                .thenReturn(Optional.of(this.paturageType));
        Mockito.when(this.paturageType.getValeur()).thenReturn("paturage");
        Mockito.when(
                this.interventionDAO.getinterventionForDateAndType(this.m.parcelle,
                        this.m.dateDebut, 2, "paturage")).thenReturn(Optional.of(this.paturage));
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle,
                        this.m.dateDebut)).thenReturn(Optional.of(this.m.suiviParcelle));
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.of(this.m.versionDeTraitement));
        Mockito.when(dbVariables.get(12)).thenReturn(dvu);
        Mockito.when(this.methodeAnalyseDAO.getByNumberId(102L)).thenReturn(Optional.of(this.methode));
        IntervalDate intervalDate = new IntervalDate(m.dateTimeDebut, m.dateTimeFin, DateUtil.YYYY_MM_DD_HHMMSS_SSSZ);
        // nominal
        long result = this.instance.readLines(parser, intervalDate, BiomassMockUtils.requestPropertiesBiomasse,
                this.datasetDescriptor, errorsReport, 1L, dbVariables, this.lines);
        Assert.assertFalse(errorsReport.hasErrors());
        Assert.assertNotNull(this.lines.get(0));
        BiomassProductionLineRecord line = this.lines.get(0);
        Assert.assertEquals(this.m.dateFin, line.dateDebutRepousse);
        Assert.assertEquals(this.masseVegClass, line.masseVegClass);
        Assert.assertEquals("nature couvert", line.getNatureCouvert());
        Assert.assertEquals("série 1", line.getSerie());
        Assert.assertTrue(1 == line.getRotationNumber());
        Assert.assertTrue(2 == line.getAnneeNumber());
        Assert.assertTrue(3 == line.getPeriodeNumber());
        Assert.assertEquals(this.paturage, line.getIntervention());
        Assert.assertEquals(this.paturageType, line.getTypeIntervention());
        Assert.assertEquals(this.m.dateDebut, line.getDate());
        Assert.assertEquals(this.m.parcelle, line.getParcelle());
        Assert.assertEquals(this.m.traitement, line.getTraitementProgramme());
        Assert.assertEquals(this.m.versionDeTraitement, line.getVersionDeTraitement());
        Assert.assertEquals(this.m.suiviParcelle, line.getSuiviParcelle());
        VariableValueBiomassProduction valeur = line.getVariablesValues().get(0);
        Assert.assertEquals("12.0", valeur.getValue());
        Assert.assertEquals(2, valeur.getMeasureNumber());
        Assert.assertTrue(11.4F == valeur.getStandardDeviation());
        Assert.assertEquals(this.methode, valeur.getMethode());
        Assert.assertEquals(m.variable, valeur.getDatatypeVariableUnite().getVariable());
        Assert.assertTrue(2 == valeur.getQualityClass());
        Assert.assertTrue(2 == result);
        // with errorreport
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        this.lines.clear();
        errorsReport.addErrorMessage("message");
        result = this.instance.readLines(parser, intervalDate, BiomassMockUtils.requestPropertiesBiomasse,
                this.datasetDescriptor, errorsReport, 1L, dbVariables, this.lines);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertTrue(2 == result);
    }

    /**
     * Test of readMasseVegClass method, of class
     * ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadMasseVegClass() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"masse vegetale"};
        CleanerValues cleanerValues = new CleanerValues(values);
        BiomassProductionLineRecord line = Mockito.mock(BiomassProductionLineRecord.class);
        Mockito.when(this.masseVegClassDAO.getByNKey("masse vegetale")).thenReturn(
                Optional.of(this.masseVegClass));
        // nominal
        int result = this.instance.readMasseVegClass(cleanerValues, line, errorsReport, 1L, 0);
        Assert.assertEquals(1, result);
        Mockito.verify(line).setMasseVegClass(this.masseVegClass);
        // null masse vegetale
        values = new String[]{"coucou"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMasseVegClass(cleanerValues, line, errorsReport, 1L, 0);
        Assert.assertEquals(1, result);
        Assert.assertTrue(errorsReport.hasErrors());
        Mockito.verify(line).setMasseVegClass(this.masseVegClass);
        Assert.assertEquals(
                "-Ligne 1, colonne 1, la masse végétale classification \"coucou\" n'est pas renseignée en base de données.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readMethod method, of class ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadMethod() {
        ErrorsReport errorsReport = new ErrorsReport();
        String[] values = new String[]{"102"};
        CleanerValues cleanerValues = new CleanerValues(values);
        VariableValueBiomassProduction variableValue = Mockito
                .mock(VariableValueBiomassProduction.class);
        MethodeProduction methodeProduction = Mockito.mock(MethodeProduction.class);
        MethodeAnalyse methodeAnalyse = Mockito.mock(MethodeAnalyse.class);
        Mockito.when(this.methodeProductionDAO.getByNumberId(102L)).thenReturn(Optional.of(methodeProduction));
        // nominal merthode production
        int result = this.instance.readMethod(errorsReport, 1L, 0, cleanerValues, variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(1)).setMethode(methodeProduction);
        Assert.assertFalse(errorsReport.hasErrors());
        // nominal merthode analyse
        values = new String[]{"103"};
        cleanerValues = new CleanerValues(values);
        Mockito.when(this.methodeAnalyseDAO.getByNumberId(103L)).thenReturn(Optional.of(methodeAnalyse));
        result = this.instance.readMethod(errorsReport, 1L, 0, cleanerValues, variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(1)).setMethode(methodeAnalyse);
        Assert.assertFalse(errorsReport.hasErrors());
        // empty method
        values = new String[]{"104"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readMethod(errorsReport, 1L, 0, cleanerValues, variableValue);
        Assert.assertEquals(1, result);
        Mockito.verify(variableValue, Mockito.times(2)).setMethode(
                any(AbstractMethode.class));
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, colonne 1, la méthode \"104\" n'est pas renseignée en base de données .\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of readVariableValueBiomassProduction method, of class
     * ProcessRecordBiomassProduction.
     */
    @Test
    public void testReadVariableValueBiomassProduction() {
        String[] values = new String[]{"12.0", "2", "11.4", "102", "2"};
        CleanerValues cleanerValues = new CleanerValues(values);
        BiomassProductionLineRecord line = Mockito.mock(BiomassProductionLineRecord.class);
        LinkedList variableValues = Mockito.mock(LinkedList.class);
        Mockito.when(line.getVariablesValues()).thenReturn(variableValues);
        List<DatatypeVariableUniteACBB> dbVariables = Mockito.mock(List.class);
        DatatypeVariableUniteACBB dvu = Mockito.mock(DatatypeVariableUniteACBB.class);
        when(dvu.getVariable()).thenReturn(m.variable);
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.when(dbVariables.get(1)).thenReturn(dvu);
        Mockito.when(this.methodeAnalyseDAO.getByNumberId(102L)).thenReturn(Optional.of(this.methode));
        // nominal
        int result = this.instance.readVariableValueBiomassProduction(line, errorsReport, 1L, 0,
                cleanerValues, this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse,
                dbVariables);
        Assert.assertEquals(5, result);
        ArgumentCaptor<VariableValueBiomassProduction> vv = ArgumentCaptor
                .forClass(VariableValueBiomassProduction.class);
        Mockito.verify(variableValues).add(vv.capture());
        VariableValueBiomassProduction valeur = vv.getValue();
        Assert.assertEquals("12.0", valeur.getValue());
        Assert.assertEquals(2, valeur.getMeasureNumber());
        Assert.assertTrue(11.4F == valeur.getStandardDeviation());
        Assert.assertEquals(this.methode, valeur.getMethode());
        Assert.assertTrue(2 == valeur.getQualityClass());
        // nominal
        values = new String[]{org.apache.commons.lang.StringUtils.EMPTY, "2", "11.4", "102", "2"};
        cleanerValues = new CleanerValues(values);
        result = this.instance.readVariableValueBiomassProduction(line, errorsReport, 1L, 0,
                cleanerValues, this.datasetDescriptor, BiomassMockUtils.requestPropertiesBiomasse,
                dbVariables);
        Assert.assertEquals(5, result);
        Mockito.verify(variableValues, Mockito.times(1)).add(vv.capture());

    }

    class InstanceImpl extends ProcessRecordBiomassProduction {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        public CSVParser parser;
        public ErrorsReport errorsReport;
        int counter = 0;

        InstanceImpl() {
            super();
            logger = LoggerFactory.getLogger("logger");
            this.parser = new CSVParser(new ByteArrayInputStream(org.apache.commons.lang.StringUtils.EMPTY.getBytes()), ';');
        }

        @Override
        protected void buildNewLines(VersionFile version, List<? extends AbstractLineRecord> l,
                                     ErrorsReport er, IRequestPropertiesBiomasse sp) throws PersistenceException {
            Assert.assertEquals(m.versionFile, version);
            Assert.assertEquals(lines, l);
            Assert.assertNotNull(er);
            Assert.assertFalse(er.hasErrors());
            Assert.assertEquals(BiomassMockUtils.requestPropertiesBiomasse, sp);
            er.addInfoMessage("true");
            this.errorsReport = er;
        }

        @Override
        protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(CSVParser p,
                                                                                    Map<Integer, Column> c, DatasetDescriptorACBB dd) throws PersistenceException,
                IOException {
            return variables;
        }

        @Override
        long readLines(CSVParser p, IntervalDate intervalDate, IRequestPropertiesBiomasse sp, DatasetDescriptorACBB dd,
                       ErrorsReport er, long lineCount, List<DatatypeVariableUniteACBB> dbv,
                       List<BiomassProductionLineRecord> l) throws IOException {
            Assert.assertEquals(this.parser, p);
            Assert.assertEquals(BiomassMockUtils.requestPropertiesBiomasse, sp);
            Assert.assertNotNull(er);
            Assert.assertTrue(4L == lineCount);
            Assert.assertEquals(variables, dbv);
            Assert.assertEquals(lines, l);
            er.addInfoMessage("true");
            this.errorsReport = er;
            if (2 == ++this.counter) {
                er.addErrorMessage("error");
            }
            return lineCount;
        }

        @Override
        public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
            this.versionFileDAO = versionFileDAO;
        }
    }
}
