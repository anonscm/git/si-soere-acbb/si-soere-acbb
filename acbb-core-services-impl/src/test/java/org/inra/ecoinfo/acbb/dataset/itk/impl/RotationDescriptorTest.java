/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.dataset.itk.semis.impl.LineRecord;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;

/**
 * @author ptcherniati
 */
public class RotationDescriptorTest {

    RotationDescriptor instance;
    @Mock
    AbstractLineRecord line;
    @Mock
    LineRecord lineSemis;
    @Mock
    VersionDeTraitement versionDeTraitement;
    @Mock
    Rotation rotation;
    @Mock
    Modalite nonRotation;
    @Mock
    SemisCouvertVegetal ble;
    @Mock
    SemisCouvertVegetal mais;
    @Mock
    SemisCouvertVegetal choux;
    @Mock
    SemisCouvertVegetal pt;
    Map<Integer, SemisCouvertVegetal> couverts = new HashMap();
    ITKMockUtils m = ITKMockUtils.getInstance();
    SortedSet<Modalite> modalites;
    /**
     *
     */
    public RotationDescriptorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    SortedSet<Modalite> getModalités(Modalite... modalites) {
        SortedSet<Modalite> m = new TreeSet();
        m.addAll(Arrays.asList(modalites));
        this.modalites = m;
        return m;
    }

    private void initVersionTraitment(Modalite m, SemisCouvertVegetal... cvs) {
        this.couverts.clear();
        int i = 0;
        for (SemisCouvertVegetal cv : cvs) {
            this.couverts.put(++i, cv);
        }
        this.modalites = this.getModalités(m);
        Mockito.when(this.versionDeTraitement.getModalites()).thenReturn(this.modalites);
    }

    private void initVersionTraitment(Rotation r, SemisCouvertVegetal... cvs) {
        this.couverts.clear();
        int i = 0;
        for (SemisCouvertVegetal cv : cvs) {
            this.couverts.put(++i, cv);
        }
        Mockito.when(r.getCouvertsVegetal()).thenReturn(this.couverts);
        this.modalites = this.getModalités(r);
        Mockito.when(this.versionDeTraitement.getModalites()).thenReturn(this.modalites);
    }

    /**
     * Test of getCouverts method, of class RotationDescriptor.
     */
    @Test
    public void RotationDescriptorTest() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        Assert.assertNotNull(instance.rotation);
        Assert.assertEquals(this.rotation, instance.rotation);
        Map<Integer, SemisCouvertVegetal> result = instance.getCouverts();
        Assert.assertEquals(this.ble, result.get(1));
        Assert.assertEquals(this.mais, result.get(2));
        Assert.assertEquals(this.pt, result.get(3));
        Assert.assertEquals(this.ble, instance.expectedCouvert);
        Assert.assertTrue(instance.isPossibleCouvert);
        Assert.assertTrue(instance.isExpectedCouvert);
        // rotation case bad cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertNotNull(instance.rotation);
        Assert.assertEquals(this.rotation, instance.rotation);
        result = instance.getCouverts();
        Assert.assertEquals(this.ble, result.get(1));
        Assert.assertEquals(this.mais, result.get(2));
        Assert.assertEquals(this.pt, result.get(3));
        Assert.assertEquals(this.ble, instance.expectedCouvert);
        Assert.assertTrue(instance.isPossibleCouvert);
        Assert.assertFalse(instance.isExpectedCouvert);
        // rotation case inexpected cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.choux);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertNotNull(instance.rotation);
        Assert.assertEquals(this.rotation, instance.rotation);
        result = instance.getCouverts();
        Assert.assertEquals(this.ble, result.get(1));
        Assert.assertEquals(this.mais, result.get(2));
        Assert.assertEquals(this.pt, result.get(3));
        Assert.assertEquals(this.ble, instance.expectedCouvert);
        Assert.assertFalse(instance.isPossibleCouvert);
        Assert.assertFalse(instance.isExpectedCouvert);
        // non rotation
        this.initVersionTraitment(this.nonRotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.pt);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertNull(instance.rotation);
        result = instance.getCouverts();
        Assert.assertNull(result);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.versionDeTraitement.getModalites()).thenReturn(this.modalites);
        Mockito.when(this.ble.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.ble.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.ble.getValeur()).thenReturn("blé");
        Mockito.when(this.mais.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.mais.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.mais.getValeur()).thenReturn("mais");
        Mockito.when(this.choux.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.choux.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.choux.getValeur()).thenReturn("choux");
        Mockito.when(this.pt.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.pt.getCode()).thenReturn("sem_couvert");
        Mockito.when(this.pt.getValeur()).thenReturn("PT");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getCouverts method, of class RotationDescriptor.
     */
    @Test
    public void testGetCouverts() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        Map<Integer, SemisCouvertVegetal> result = instance.getCouverts();
        Assert.assertEquals(this.ble, result.get(1));
        Assert.assertEquals(this.mais, result.get(2));
        Assert.assertEquals(this.pt, result.get(3));
        // rotation case bad cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        result = instance.getCouverts();
        Assert.assertEquals(this.ble, result.get(1));
        Assert.assertEquals(this.mais, result.get(2));
        Assert.assertEquals(this.pt, result.get(3));
        // non rotation
        this.initVersionTraitment(this.nonRotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.pt);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        result = instance.getCouverts();
        Assert.assertNull(result);
    }

    /**
     * Test of getExpectedCouvert method, of class RotationDescriptor.
     */
    @Test
    public void testGetExpectedCouvert() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        Assert.assertEquals(this.ble, instance.getExpectedCouvert());
        // rotation case bad cover
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertEquals(this.ble, instance.getExpectedCouvert());
    }

    /**
     * Test of getExpectedCouvertList method, of class RotationDescriptor.
     */
    @Test
    public void testGetExpectedCouvertList() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        instance.getCouverts();
        Assert.assertEquals("blé, mais, PT", instance.getExpectedCouvertList());
        // rotation case bad cover
        this.initVersionTraitment(this.nonRotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertNull(instance.getExpectedCouvertList());
    }

    /**
     * Test of getRotation method, of class RotationDescriptor.
     */
    @Test
    public void testGetRotation() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        Assert.assertEquals(this.rotation, instance.getRotation());
        // rotation case bad cover
        this.initVersionTraitment(this.nonRotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertNull(instance.getRotation());
    }

    /**
     * Test of isExpectedCouvert method, of class RotationDescriptor.
     */
    @Test
    public void testIsExpectedCouvert() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        Assert.assertTrue(instance.isExpectedCouvert);
        // rotation case bad cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertFalse(instance.isExpectedCouvert);
        // non rotation
        this.initVersionTraitment(this.nonRotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.pt);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertFalse(instance.isExpectedCouvert);
    }

    /**
     * Test of isPossibleCouvert method, of class RotationDescriptor.
     */
    @Test
    public void testIsPossibleCouvert() {
        // rotation case good cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.ble);
        RotationDescriptor instance = new RotationDescriptor(this.lineSemis,
                this.versionDeTraitement);
        Assert.assertTrue(instance.isPossibleCouvert);
        // rotation case bad cover
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.mais);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertTrue(instance.isPossibleCouvert);
        // rotation case bad cover unExpected
        this.initVersionTraitment(this.rotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.choux);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertFalse(instance.isPossibleCouvert);
        // non rotation
        this.initVersionTraitment(this.nonRotation, this.ble, this.mais, this.pt);
        Mockito.when(this.lineSemis.getAnneeNumber()).thenReturn(1);
        Mockito.when(this.lineSemis.getCouvertVegetal()).thenReturn(this.pt);
        instance = new RotationDescriptor(this.lineSemis, this.versionDeTraitement);
        Assert.assertFalse(instance.isPossibleCouvert);
    }

}
