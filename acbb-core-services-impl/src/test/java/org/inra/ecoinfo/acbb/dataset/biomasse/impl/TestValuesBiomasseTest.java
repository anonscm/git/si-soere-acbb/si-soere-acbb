/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.Mockito;

/**
 * @author ptcherniati
 */
public class TestValuesBiomasseTest {

    MockUtils m = MockUtils.getInstance();

    /**
     *
     */
    public TestValuesBiomasseTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of checkOtherTypeValue method, of class TestValuesBiomasse.
     */
    @Test
    public void testCheckOtherTypeValue() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        TestValuesBiomasse instance = new TestValuesBiomasse();
        Column column = Mockito.mock(Column.class);
        // no flag
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, null, column, null, null, null);
        // bad flag
        Mockito.when(column.isFlag()).thenReturn(Boolean.TRUE);
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, null, column, null, null, null);
        // with flag but flag null
        Mockito.when(column.getFlagType()).thenReturn(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, null, column, null, null, null);
        // with flag but flag 0
        Mockito.when(column.getFlagType()).thenReturn(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, "0", column, null, null, null);
        // with flag but flag 1
        Mockito.when(column.getFlagType()).thenReturn(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, "1", column, null, null, null);
        // with flag but flag 2
        Mockito.when(column.getFlagType()).thenReturn(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, "2", column, null, null, null);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // with flag but flag 3
        instance.checkOtherTypeValue(null, badsFormatsReport, 1L, 1, "3", column, null, null, null);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "A la ligne 1, colonne 2, l'indice de qualité ne peut accepter que les valeurs 0,1 ou 2",
                badsFormatsReport.getErrorsMessages().get(0));
        // with flag but flag 3
        instance.checkOtherTypeValue(null, badsFormatsReport, 2L, 2, "trois", column, null, null,
                null);
        Assert.assertTrue(2 == badsFormatsReport.getErrorsMessages().size());
        Assert.assertEquals(
                "A la ligne 2, colonne 3, l'indice de qualité ne peut accepter que les valeurs 0,1 ou 2",
                badsFormatsReport.getErrorsMessages().get(1));
    }

}
