package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class GenericTestHeaderTest {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(GenericTestHeaderTest.class
                    .getName());

    static String[] EMPTY_LINE = new String[]{};

    static String[] LINE_WITH_EMPTY_ELEMENT = new String[]{org.apache.commons.lang.StringUtils.EMPTY,
            org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY};

    static String[] LINE_WITH_NOT_EMPTY_ELEMENT = new String[]{org.apache.commons.lang.StringUtils.EMPTY,
            "chien", org.apache.commons.lang.StringUtils.EMPTY};

    GenericTestHeader instance;

    MockUtils m;

    /**
     *
     */
    public GenericTestHeaderTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.m = MockUtils.getInstance();
        this.instance = new GenericTestHeader();
        this.instance.setDatasetConfiguration(this.m.datasetConfiguration);
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.instance.setParcelleDAO(this.m.parcelleDAO);
        this.instance.setSiteDAO(this.m.siteDAO);
        this.instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        this.instance.setTraitementDAO(this.m.traitementDAO);
        this.instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);

        when(m.parcelleDAO.getByNKey(any())).thenReturn(Optional.empty());
        when(m.versionDeTraitementDAO.getByNKey(anyString(), anyString(), anyInt())).thenReturn(Optional.empty());

    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of estLigneVide method, of class GenericTestHeader.
     */
    @Test
    public void testEstLigneVide() {

        Boolean result;
        // ligne vide

        result = this.instance.estLigneVide(GenericTestHeaderTest.EMPTY_LINE);
        Assert.assertTrue(result);
        // string vide

        result = this.instance.estLigneVide(GenericTestHeaderTest.LINE_WITH_EMPTY_ELEMENT);
        Assert.assertTrue(result);
        // lign existante

        result = this.instance.estLigneVide(GenericTestHeaderTest.LINE_WITH_NOT_EMPTY_ELEMENT);
        Assert.assertFalse(result);

    }

    /**
     * Test of getLogger method, of class GenericTestHeader.
     */
    @Test
    public void testGetLogger() {

        Logger result = this.instance.getLogger();
        Assert.assertNotNull(result);
        Assert.assertEquals(GenericTestHeader.class.getName(), result.getName());

    }

    /**
     * Test of getParcelle method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetParcelle() throws Exception {

        SiteACBB siteACBB = this.m.site;
        Parcelle parcelle = this.m.parcelle;
        String parcelleCode = MockUtils.PARCELLE;
        // cas nominal
        when(m.parcelleDAO.getByNKey("site_parcelle")).thenReturn(Optional.ofNullable(m.parcelle));
        Parcelle result = this.instance.getParcelle(siteACBB, parcelleCode).orElse(null);
        Assert.assertEquals(parcelle, result);
        // bad parcelle

        Mockito.when(this.m.parcelleDAO.getByNKey(any(String.class)))
                .thenReturn(Optional.empty());
        result = this.instance.getParcelle(siteACBB, "rien").orElse(null);
        Assert.assertNull(result);
        // null parcelle

        Mockito.when(
                this.m.parcelleDAO.getByNKey(contains("rien"))).thenReturn(Optional.empty());
        result = this.instance.getParcelle(siteACBB, null).orElse(null);
        Assert.assertNull(result);

    }

    /**
     * Test of getSite method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetSite() throws Exception {

        String siteName = MockUtils.SITE_CODE;
        // cas nominal

        Site result = this.instance.getSite(siteName).orElse(null);
        Assert.assertEquals(this.m.site, result);
        // bad site

        Mockito.when(this.m.siteDAO.getByPath("rien")).thenReturn(Optional.empty());
        result = this.instance.getSite("rien").orElse(null);
        Assert.assertNull(result);
        // site null

        Mockito.when(this.m.siteDAO.getByPath(null)).thenReturn(Optional.empty());
        result = this.instance.getSite(null).orElse(null);
        Assert.assertNull(result);

    }

    /**
     * Test of getTraitement method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetTraitement() throws Exception {

        SiteACBB site = this.m.site;
        String traitementCode = MockUtils.TRAITEMENT;
        // cas nominal

        TraitementProgramme result = this.instance.getTraitement(site, traitementCode).orElse(null);
        Assert.assertEquals(this.m.traitement, result);
        // bad traitement

        Mockito.when(this.m.traitementDAO.getByNKey(any(Long.class), eq("rien")))
                .thenReturn(Optional.empty());
        result = this.instance.getTraitement(site, "rien").orElse(null);
        Assert.assertNull(result);
        // traitement null

        Mockito.when(
                this.m.traitementDAO.getByNKey(any(Long.class),
                        isNull(String.class))).thenReturn(Optional.empty());
        result = this.instance.getTraitement(site, null).orElse(null);
        Assert.assertNull(result);

    }

    /**
     * Test of jumpLines method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testJumpLines() throws Exception {

        String text = "ligne 1;coucou\nligne 2;bonjour\n\nligne 3;fin";
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        // cas nominal (1Ligne)

        long result = this.instance.jumpLines(parser, 0, 1);
        Assert.assertEquals(parser.getLastLineNumber(), result);
        // saute 2Ligne + 1 ligne vide

        result = this.instance.jumpLines(parser, parser.getLastLineNumber(), 2);
        Assert.assertEquals(parser.getLastLineNumber(), result);// saute les lignes
        // vides

    }

    /**
     * Test of readBeginAndEndDates method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadBadBeginAndEndDates() throws Exception {

        String text = "date de debut :;21/01/201;000:00\n" + "date de fin :;3/01/2014;2330:00\n"
                + "date de debut :\n" + "date de fin ;\n" + "date de debut :;21/01/2014\n"
                + "date de fin ;31/01/2014";
        VersionFile version = this.m.versionFile;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        // invalid date and time

        long result = this.instance.readBeginAndEndDates(version, badsFormatsReport, parser, 1L,
                requestProperties);
        String error = badsFormatsReport.getMessages();
        Assert.assertTrue(badsFormatsReport.hasErrors());
        String expectedMessage = "Erreur :- La date de début spécifiée \"21/01/201\" à la ligne 2 colonne 1 est non valide. Le format de date doit être dd/MM/yyyy. - L'heure de début spécifiée \"000:00\" à la ligne 2 colonne 2 est non valide. Le format d'heure doit être HH:mm. - La date de fin spécifiée \"3/01/2014\" à la ligne 3 colonne 1 est non valide. Le format de date doit être dd/MM/yyyy. - L'heure de fin spécifiée \"2330:00\" à la ligne 3 colonne 1 est non valide. Le format d'heure doit être HH:mm. ";
        Assert.assertEquals(3L, result);
        // missing date

        badsFormatsReport = new BadsFormatsReport("Erreur");
        result = this.instance.readBeginAndEndDates(version, badsFormatsReport, parser, 3L,
                requestProperties);
        error = badsFormatsReport.getMessages();
        Assert.assertTrue(badsFormatsReport.hasErrors());
        expectedMessage = "Erreur :- La date de début est manquante à la ligne 4 colonne 1 - La date de fin est manquante à la ligne 5 colonne 1 ";
        Assert.assertEquals(expectedMessage, error);
        Assert.assertEquals(5L, result);
        // missing time

        badsFormatsReport = new BadsFormatsReport("Erreur");
        result = this.instance.readBeginAndEndDates(version, badsFormatsReport, parser, 5L,
                requestProperties);
        error = badsFormatsReport.getMessages();
        Assert.assertTrue(badsFormatsReport.hasErrors());
        expectedMessage = "Erreur :- La date de début est manquante à la ligne 6 colonne 1 - La date de fin est manquante à la ligne 7 colonne 1 ";
        Assert.assertEquals(expectedMessage, error);
        Assert.assertEquals(7L, result);
        // test inconsitant date

        text = "date de debut :;21/01/2014;00:00:00\n" + "date de fin :;31/01/2014;23:30:00\n"
                + "date de debut :;21/01/2014;00:00:00\n" + "date de fin :;31/01/2014;23:30:00\n";
        parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        badsFormatsReport = new BadsFormatsReport("Erreur");
        MockUtils.getInstance();
        LocalDateTime dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "21/01/2014");
        LocalDateTime dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "31/01/2014");
        Mockito.when(requestProperties.getDateDeDebut()).thenReturn(dateDebut);
        Mockito.when(requestProperties.getDateDeFin()).thenReturn(dateFin);
        parser.changeDelimiter(';');
        result = this.instance.readBeginAndEndDates(version, badsFormatsReport, parser, 7L,
                requestProperties);
        Assert.assertEquals(9L, result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- La date de début spécifiée \"21/01/2014 00:00\" à la ligne 9 colonne 1 ne correspond pas à celle du nom de fichier agro-écosysteme_site_datatype_01-01-2012_31-12-2012.csv - La date de fin spécifiée \"31/01/2014 00:00\" à la ligne 9 colonne 2 ne correspond pas à celle du nom de fichier agro-écosysteme_site_datatype_01-01-2012_31-12-2012.csv ",
                badsFormatsReport.getMessages());
        // test inconsitant date

        text = "date de debut :;21/01/2014;00:00:00\n" + "date de fin :;11/01/2014;23:30:00\n"
                + "date de debut :;21/01/2014;00:00:00\n" + "date de fin :;31/01/2014;23:30:00\n";
        parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        badsFormatsReport = new BadsFormatsReport("Erreur");
        MockUtils.getInstance();
        dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "21/01/2014");
        dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "11/01/2014");
        Mockito.when(requestProperties.getDateDeDebut()).thenReturn(dateDebut);
        Mockito.when(requestProperties.getDateDeFin()).thenReturn(dateFin);
        Mockito.when(requestProperties.getDateFormat()).thenReturn(DateUtil.DD_MM_YYYY);
        result = this.instance.readBeginAndEndDates(version, badsFormatsReport, parser, 9L,
                requestProperties);
        Assert.assertEquals(11L, result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- La date de début \"21/01/2014\" spécifiée à la ligne 10 colonne 1 doit être antérieure ou égale à la date de fin \"11/01/2014\" spécifiée à la ligne 11 colonne 1 - La date de début spécifiée \"21/01/2014 00:00\" à la ligne 11 colonne 1 ne correspond pas à celle du nom de fichier agro-écosysteme_site_datatype_01-01-2012_31-12-2012.csv - La date de fin spécifiée \"11/01/2014 00:00\" à la ligne 11 colonne 2 ne correspond pas à celle du nom de fichier agro-écosysteme_site_datatype_01-01-2012_31-12-2012.csv ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of readBeginAndEndDates method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadBeginAndEndDates() throws Exception {

        String text = "date de debut :;21/01/2014;00:00:00\n"
                + "date de fin :;31/01/2014;23:30:00\n" + "date de debut :;21/01/2014;00:00:00\n"
                + "date de fin :;31/01/2014;23:30:00";
        VersionFile version = this.m.versionFile;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        // cas nominal

        ArgumentCaptor<LocalDateTime> dateDeDebut = ArgumentCaptor.forClass(LocalDateTime.class);
        ArgumentCaptor<LocalDateTime> dateDeFin = ArgumentCaptor.forClass(LocalDateTime.class);
        long result = this.instance.readBeginAndEndDates(version, badsFormatsReport, parser, 1L,
                requestProperties);
        Assert.assertEquals(3L, result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties).setDateDeDebut(dateDeDebut.capture());
        Mockito.verify(requestProperties).setDateDeFin(dateDeFin.capture());
        final String beginDate = DateUtil.getUTCDateTextFromLocalDateTime(dateDeDebut.getValue(), RecorderACBB.DD_MM_YYYY_HHMMSS_FILE);
        Assert.assertEquals("21-01-2014-000000", beginDate);
        final String endDate = DateUtil.getUTCDateTextFromLocalDateTime(dateDeFin.getValue(), RecorderACBB.DD_MM_YYYY_HHMMSS_FILE);
        Assert.assertEquals("31-01-2014-233000", endDate);
        Assert.assertEquals(3L, result);

    }

    /**
     * Test of readCommentaire method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadCommentaire() throws Exception {

        String text = "commentaire :;un commentaire\ncommentaire :";
        new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        // cas nominal

        ArgumentCaptor<String> commentaire = ArgumentCaptor.forClass(String.class);
        Long result = this.instance.readCommentaire(parser, 2L, requestProperties);
        Mockito.verify(requestProperties, Mockito.times(1)).setCommentaire(commentaire.capture());
        Assert.assertEquals("un commentaire", commentaire.getAllValues().get(0));
        Assert.assertTrue(3L == result);
        // null comment

        result = this.instance.readCommentaire(parser, 3L, requestProperties);
        Assert.assertTrue(4L == result);
        Mockito.verify(requestProperties, Mockito.times(2)).setCommentaire(commentaire.capture());
        Assert.assertEquals(org.apache.commons.lang.StringUtils.EMPTY, commentaire.getAllValues().get(2));

    }

    /**
     * Test of readDatatype method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadDatatype() throws Exception {

        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(
                "datatype:;datatype\ndatatype:;pas datatype\ndatatype:"));
        parser.changeDelimiter(';');
        Mockito.mock(IRequestPropertiesACBB.class);

        long result = this.instance.readDatatype(badsFormatsReport, parser, 2L, MockUtils.DATATYPE);
        Assert.assertEquals(3L, result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // bad datatype

        result = this.instance.readDatatype(badsFormatsReport, parser, 3L, MockUtils.DATATYPE);
        Assert.assertEquals(4L, result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- A la ligne 4 colonne 2, le type de donnée doit être datatype. ",
                badsFormatsReport.getMessages());
        // null datatype

        badsFormatsReport = new BadsFormatsReport("Erreur");
        result = this.instance.readDatatype(badsFormatsReport, parser, 4L, MockUtils.DATATYPE);
        Assert.assertEquals(5L, result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals("Erreur :- Aucun type de données n'a été défini ligne 5 colonne 2. ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of readEmptyLine method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadEmptyLine() throws Exception {

        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(";\n;quelquechose"));
        parser.changeDelimiter(';');
        // cas nominal

        long result = this.instance.readEmptyLine(badsFormatsReport, parser, 1L);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Assert.assertEquals(2L, result);
        // ligne non vide

        result = this.instance.readEmptyLine(badsFormatsReport, parser, 2L);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(3L, result);
        Assert.assertEquals("Erreur :- Vous devez laisser la ligne 3 vide. ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of readLineHeader method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLineHeader() throws Exception {

        String text = "colonne1;colonne2;colonne3;colonne4;colonne5;colonne6;colonne7;colonne8;colonne9;colonne10";

        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");

        CSVParser parser = new CSVParser(new StringReader(text));

        parser.changeDelimiter(';');

        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);

        // cas nominal
        Long result = this.instance.readLineHeader(badsFormatsReport, parser, 1L,
                this.m.datasetDescriptor, requestProperties);

        Assert.assertFalse(badsFormatsReport.hasErrors());

        Assert.assertTrue(2L == result);

        // bad columns
        text = "colonne1;colonne3;colonne3;colonne4;colonne5;colonne6;colonne7;colonne8;colonne9;colonne10";

        parser = new CSVParser(new StringReader(text));

        parser.changeDelimiter(';');

        this.m = MockUtils.getInstance();

        result = this.instance.readLineHeader(badsFormatsReport, parser, 2L,
                this.m.datasetDescriptor, requestProperties);

        String expectedError = "Erreur :- Ligne 3 : L'intitulé de la colonne 2 est \"colonne3\", alors que \"colonne2\" est attendu ";

        Assert.assertTrue(badsFormatsReport.hasErrors());

        Assert.assertEquals(expectedError, badsFormatsReport.getMessages());

        Assert.assertTrue(3L == result);

    }

    /**
     * Test of readSite method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadSite_4args() throws Exception {

        String text = "site:;site\nsite:;no site\nsite:";
        VersionFile version = this.m.versionFile;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site, this.m.site, null, null);
        // cas nominal

        Long result = this.instance.readSite(version, badsFormatsReport, parser, 1L,
                requestProperties);
        Assert.assertTrue(2L == result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        // bad site
        when(m.siteDAO.getByPath(anyString())).thenReturn(Optional.empty());
        result = this.instance.readSite(version, badsFormatsReport, parser, 2L, requestProperties);
        Assert.assertTrue(3L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        // null site

        result = this.instance.readSite(version, badsFormatsReport, parser, 3L, requestProperties);
        Assert.assertEquals(
                "Erreur :- Le site no site défini ligne 3 colonne 2 n'existe pas. - Aucun site n'a été défini ligne 4 colonne 2. ",
                badsFormatsReport.getMessages());
        Assert.assertTrue(4L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        ArgumentCaptor<SiteACBB> site = ArgumentCaptor.forClass(SiteACBB.class);
        Mockito.verify(requestProperties, Mockito.times(1)).setSite(site.capture());
        Assert.assertEquals(this.m.site, site.getAllValues().get(0));
        Assert.assertTrue(site.getAllValues().size() == 1);
        // empty file

        BusinessException exc = null;
        try {

            result = this.instance.readSite(version, badsFormatsReport, parser, 3L,
                    requestProperties);

        } catch (BusinessException e) {

            exc = e;

        }
        Assert.assertNotNull(exc);
        Assert.assertEquals("Le fichier est vide.", exc.getMessage());

    }

    /**
     * Test of readSiteAndParcelle method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadSiteAndParcelle() throws Exception {

        String text = "site:;site;parcelle\nsite:no site;;parcelle\nsite:;site;no parcelle\nsite:;site;parcelle\nsite:;site;parcelle\nsite:;site\nsite:;site;parcelle";
        VersionFile version = this.m.versionFile;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        // cas nominal

        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(requestProperties.getTraitement()).thenReturn(this.m.traitement);
        when(m.parcelleDAO.getByNKey("site_parcelle")).thenReturn(Optional.of(m.parcelle));
        Long result = this.instance.readSiteAndParcelle(version, badsFormatsReport, parser, 1L, requestProperties);
        ArgumentCaptor<SiteACBB> site = ArgumentCaptor.forClass(SiteACBB.class);
        ArgumentCaptor<Parcelle> parcelle = ArgumentCaptor.forClass(Parcelle.class);
        ArgumentCaptor<SuiviParcelle> suiviParcelle = ArgumentCaptor.forClass(SuiviParcelle.class);
        Assert.assertTrue(result == 2L);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(1)).setSite(site.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setParcelle(parcelle.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setSuiviParcelle(
                suiviParcelle.capture());
        Assert.assertEquals(this.m.site, site.getValue());
        Assert.assertEquals(this.m.parcelle, parcelle.getValue());
        Assert.assertEquals(this.m.suiviParcelle, suiviParcelle.getValue());
        // site null

        badsFormatsReport = new BadsFormatsReport("Erreur");
        Mockito.when(requestProperties.getSite()).thenReturn(null);
        when(m.siteDAO.getByPath(any())).thenReturn(Optional.empty());
        result = this.instance.readSiteAndParcelle(version, badsFormatsReport, parser, 2L,
                requestProperties);
        Assert.assertTrue(3L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(1)).setSite(site.capture());
        Assert.assertEquals("Erreur :- Le site  défini ligne 3 colonne 2 n'existe pas. ",
                badsFormatsReport.getMessages());
        // parcelle null

        when(m.siteDAO.getByPath(any())).thenReturn(Optional.of(m.site));
        badsFormatsReport = new BadsFormatsReport("Erreur");
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getParcelle()).thenReturn(null);
        Mockito.when(requestProperties.getTraitement()).thenReturn(null);
        result = this.instance.readSiteAndParcelle(version, badsFormatsReport, parser, 3L,
                requestProperties);
        Assert.assertTrue(4L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(2)).setSite(site.capture());
        Assert.assertEquals(
                "Erreur :- Aucune parcelle n'a été définie ligne 4 colonne 3. ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of readTraitement method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadTraitement_4args_1() throws Exception {

        String text = "traitement:;traitement;24/12/2012\ntraitement:;traitement;24/12/2012\ntraitement:;24/12/2012\ntraitement:;traitement\ntraitement:;traitement;24-12/2012";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getTraitement()).thenReturn(this.m.traitement);
        // cas nominal

        Long result = this.instance.readTraitement(badsFormatsReport, parser, 1L, requestProperties);
        Assert.assertTrue(2L == result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        ArgumentCaptor<TraitementProgramme> traitement = ArgumentCaptor
                .forClass(TraitementProgramme.class);
        ArgumentCaptor<LocalDate> dateDebutTraitement = ArgumentCaptor.forClass(LocalDate.class);
        Mockito.verify(requestProperties, Mockito.times(1)).setTraitement(traitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setDateDebutTraitement(
                dateDebutTraitement.capture());
        Assert.assertEquals(this.m.traitement, traitement.getValue());
        Assert.assertEquals(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "24/12/2012"), dateDebutTraitement.getValue());
        // null site

        Mockito.when(requestProperties.getSite()).thenReturn(null);
        result = this.instance.readTraitement(badsFormatsReport, parser, 2L, requestProperties);
        Assert.assertTrue(3L == result);
        Mockito.verify(requestProperties, Mockito.times(1)).setTraitement(traitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setDateDebutTraitement(
                dateDebutTraitement.capture());
        Assert.assertFalse(badsFormatsReport.hasErrors());// vérifié avant
        // missing date

        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        result = this.instance.readTraitement(badsFormatsReport, parser, 3L, requestProperties);
        Assert.assertTrue(4L == result);
        Mockito.verify(requestProperties, Mockito.times(1)).setTraitement(traitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setDateDebutTraitement(
                dateDebutTraitement.capture());
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- La date de début de traitement est manquante à la ligne 4 colonne 2 ",
                badsFormatsReport.getMessages());
        // null date

        badsFormatsReport = new BadsFormatsReport("Erreur");
        result = this.instance.readTraitement(badsFormatsReport, parser, 4L, requestProperties);
        Assert.assertTrue(5L == result);
        Mockito.verify(requestProperties, Mockito.times(1)).setTraitement(traitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setDateDebutTraitement(
                dateDebutTraitement.capture());
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- La date de début de traitement est manquante à la ligne 5 colonne 2 ",
                badsFormatsReport.getMessages());
        // bad date

        badsFormatsReport = new BadsFormatsReport("Erreur");
        result = this.instance.readTraitement(badsFormatsReport, parser, 5L, requestProperties);
        Assert.assertTrue(6L == result);
        Mockito.verify(requestProperties, Mockito.times(1)).setTraitement(traitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setDateDebutTraitement(
                dateDebutTraitement.capture());
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- La date de début de traitement \"24-12/2012\" ligne 6 colonne 3 est invalide. La date doit  être au  format dd/MM/yyyy. ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of readTraitement method, of class GenericTestHeader.
     */
    @Test
    public void testReadTraitement_4args_2() {

        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getTraitement()).thenReturn(this.m.traitement);
        // cas nominal

        Long result = this.instance.readTraitement(badsFormatsReport, 1L,
                new String[]{"traitement :"}, requestProperties);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertTrue(2L == result);

    }

    /**
     * Test of readTraitementAndSuiviParcelle method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadTraitementAndSuiviParcelle() throws Exception {

        String text = "traitement:;traitement;24/12/2012\ntraitement:;traitement;24/12/2012\ntraitement:;24/12/2012\ntraitement:;traitement\ntraitement:;traitement;24-12/2012";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getTraitement()).thenReturn(this.m.traitement);
        Mockito.when(requestProperties.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(requestProperties.getDateDebutTraitement()).thenReturn(this.m.dateDebut);
        ArgumentCaptor<SuiviParcelle> suiviParcelle = ArgumentCaptor.forClass(SuiviParcelle.class);
        // cas nominal
        Long result = this.instance.readTraitementAndSuiviParcelle(badsFormatsReport, parser, 1L, requestProperties);
        Mockito.verify(requestProperties, Mockito.times(1)).setSuiviParcelle(
                suiviParcelle.capture());
        Assert.assertTrue(2L == result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Assert.assertEquals(this.m.suiviParcelle, suiviParcelle.getValue());
        // persistence exception

        Mockito.when(
                this.m.suiviParcelleDAO.getByNKey(this.m.parcelle, this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.empty());
        result = this.instance.readTraitementAndSuiviParcelle(badsFormatsReport, parser, 2L,
                requestProperties);
        Mockito.verify(requestProperties, Mockito.times(1)).setSuiviParcelle(
                suiviParcelle.capture());
        Assert.assertTrue(3L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Erreur :- Ligne 3 aucun suivi de parcelle n'est défini pour la parcelle Parcelle, le traitement Traitement ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of readTraitementVersionAndDateDebutTraitement method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadTraitementVersionAndDateDebutTraitement() throws Exception {

        // cas nominal
        String text = "traitementNominal:;traitement;24/12/2012;1\n"
                + "traitementSiteNull:;24/12/2012\n"
                + "traitementnull:;traitementnull;24/12/2012;1\n"
                + "versionTraitementNull:;traitement;24/12/2012;1\n"
                + "badNumber:;traitement;24/12/2012;un\n" + "missingNumber:;traitement;24/12/2012";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur1");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        IRequestPropertiesACBB requestProperties = Mockito.mock(IRequestPropertiesACBB.class);
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getTraitement()).thenReturn(this.m.traitement);
        Mockito.when(requestProperties.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(requestProperties.getDateDebutTraitement()).thenReturn(this.m.dateDebut);
        Mockito.when(requestProperties.getVersion()).thenReturn(1);
        ArgumentCaptor<VersionDeTraitement> versionTraitement = ArgumentCaptor
                .forClass(VersionDeTraitement.class);
        doReturn(Optional.of(m.versionDeTraitement)).when(m.versionDeTraitementDAO).getByNKey(MockUtils.SITE_CODE, MockUtils.TRAITEMENT, 1);
        // cas nominal

        Long result = this.instance.readTraitementVersionAndDateDebutTraitement(badsFormatsReport,
                parser, 1L, requestProperties);
        Mockito.verify(requestProperties, Mockito.times(1)).setVersionDeTraitement(
                versionTraitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersion(1);
        Assert.assertTrue(2L == result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Assert.assertEquals(this.m.versionDeTraitement, versionTraitement.getValue());
        // cas site null

        badsFormatsReport = new BadsFormatsReport("Erreur2");
        Mockito.when(requestProperties.getSite()).thenReturn(null);
        result = this.instance.readTraitementVersionAndDateDebutTraitement(badsFormatsReport,
                parser, 2L, requestProperties);
        Assert.assertTrue(3L == result);
        Assert.assertFalse(badsFormatsReport.hasErrors());// pas d'opération puisque le
        // site est null donc rien
        // n'est fait
        Mockito.verify(requestProperties, Mockito.times(1)).setVersionDeTraitement(
                versionTraitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersion(1);
        // cas traitement null
        when(m.traitementDAO.getByNKey(any(), any())).thenReturn(Optional.empty());

        badsFormatsReport = new BadsFormatsReport("Erreur3");
        Mockito.when(requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(requestProperties.getTraitement()).thenReturn(null);
        result = this.instance.readTraitementVersionAndDateDebutTraitement(badsFormatsReport,
                parser, 3L, requestProperties);
        Assert.assertTrue(4L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersionDeTraitement(
                versionTraitement.capture());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersion(1);
        Assert.assertEquals(
                "Erreur3 :- Le traitement  traitementnull  défini ligne 4 colonne 2 n'existe pas pour le site Site. ",
                badsFormatsReport.getMessages());
        // cas versionTraitement null

        badsFormatsReport = new BadsFormatsReport("Erreur4");
        Mockito.when(requestProperties.getTraitement()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.versionDeTraitementDAO.getByNKey(eq(MockUtils.SITE_CODE),
                        eq(MockUtils.TRAITEMENT), eq(1))).thenReturn(Optional.empty());
        result = this.instance.readTraitementVersionAndDateDebutTraitement(badsFormatsReport,
                parser, 4L, requestProperties);
        Assert.assertTrue(5L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersionDeTraitement(
                versionTraitement.capture());
        Mockito.verify(requestProperties, Mockito.times(2)).setVersion(1);
        Assert.assertEquals(
                "Erreur4 :- Le traitement  traitement  défini ligne 5 colonne 2 n'existe pas pour le site Site. - Aucun traitement n'a été défini ligne 5 colonne 2. ",
                badsFormatsReport.getMessages());
        // cas bad number for version

        badsFormatsReport = new BadsFormatsReport("Erreur5");
        Mockito.when(
                this.m.versionDeTraitementDAO.getByNKey(eq(MockUtils.SITE_CODE),
                        eq(MockUtils.TRAITEMENT), eq(1))).thenReturn(
                Optional.of(this.m.versionDeTraitement));
        result = this.instance.readTraitementVersionAndDateDebutTraitement(badsFormatsReport,
                parser, 5L, requestProperties);
        Assert.assertTrue(6L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersionDeTraitement(
                versionTraitement.capture());
        Mockito.verify(requestProperties, Mockito.times(2)).setVersion(1);
        Assert.assertEquals("Erreur5 :- Le traitement  traitement  défini ligne 6 colonne 2 n'existe pas pour le site Site. - La version de traitement 1 défini ligne 6 colonne 2 n'existe pas pour le site site et le traitement traitement. ",
                badsFormatsReport.getMessages());
        // cas missing version

        badsFormatsReport = new BadsFormatsReport("Erreur5");
        result = this.instance.readTraitementVersionAndDateDebutTraitement(badsFormatsReport,
                parser, 6L, requestProperties);
        Assert.assertTrue(7L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Mockito.verify(requestProperties, Mockito.times(1)).setVersionDeTraitement(
                versionTraitement.capture());
        Mockito.verify(requestProperties, Mockito.times(2)).setVersion(1);
        Assert.assertEquals("Erreur5 :- Le traitement  traitement  défini ligne 7 colonne 2 n'existe pas pour le site Site. - La version de traitement 1 défini ligne 7 colonne 2 n'existe pas pour le site site et le traitement traitement. ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of testAreConsistentDates method, of class GenericTestHeader.
     */
    public void testTestAreConsistentDates() {

        BadsFormatsReport badsFormatsReport = null;
        VersionFile version = this.m.versionFile;
        IRequestPropertiesACBB requestProperties = null;
        long lineNumber = 0L;
        this.instance.testAreConsistentDates(badsFormatsReport, version, requestProperties,
                lineNumber);
        // TODO review the generated test code and remove the default call to
        // fail.

        Assert.fail("The test case is a prototype.");

    }

    /**
     * Test of testHeaders method, of class GenericTestHeader.
     *
     * @throws java.lang.Exception
     */
    public void testTestHeaders() throws Exception {

        CSVParser parser = null;
        VersionFile versionFile = this.m.versionFile;
        IRequestPropertiesACBB requestProperties = null;
        String encoding = org.apache.commons.lang.StringUtils.EMPTY;
        BadsFormatsReport badsFormatsReport = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        long expResult = 0L;
        long result = this.instance.testHeaders(parser, versionFile, requestProperties, encoding,
                badsFormatsReport, datasetDescriptor);
        Assert.assertEquals(expResult, result);

        Assert.fail("The test case is a prototype.");

    }

    /**
     * Test of testIntervalDate method, of class GenericTestHeader.
     */
    public void testTestIntervalDate() {

        BadsFormatsReport badsFormatsReport = null;
        long lineNumber = 0L;
        IRequestPropertiesACBB requestProperties = null;
        this.instance.testIntervalDate(badsFormatsReport, lineNumber, requestProperties);

        Assert.fail("The test case is a prototype.");

    }

    /**
     * Test of updateMinMaxDatatypeVariableUnite method, of class GenericTestHeader.
     */
    public void testUpdateMinMaxDatatypeVariableUnite() {

        CSVParser parser = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        this.instance.updateMinMaxDatatypeVariableUnite(parser, datasetDescriptor);

        Assert.fail("The test case is a prototype.");

    }

    /**
     * Test of updateSuiviParcelle method, of class GenericTestHeader.
     */
    public void testUpdateSuiviParcelle() {
        BadsFormatsReport badsFormatsReport = null;

        long lineNumber = 0L;

        IRequestPropertiesACBB requestProperties = null;

        this.instance.updateSuiviParcelle(badsFormatsReport, lineNumber, requestProperties);
        Assert.fail("The test case is a prototype.");
    }

}
