package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class SemisPeriodeRealiseeFactoryTest {

    @Spy
    SemisPeriodeRealiseeFactory instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    IPeriodeRealiseeDAO periodeRealiseeDAO;
    @Mock
    LineRecord line;
    @Mock
    Tempo tempo;
    @Mock
    AnneeRealisee anneeRealisee;
    @Mock
    PeriodeRealisee periodeRealisee;
    @Mock
    RotationDescriptor rotationDescriptor;
    @Mock
    SemisCouvertVegetal couvertVegetal;
    @Mock
    ILocalizationManager localizationManager;
    @Mock
    IPolicyManager policyManager;
    @Mock
    AbstractIntervention intervention1;
    @Mock
    AbstractIntervention intervention2;
    /**
     *
     */
    public SemisPeriodeRealiseeFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new SemisPeriodeRealiseeFactory();
        MockitoAnnotations.initMocks(this);
        this.instance.setPeriodeRealiseeDAO(this.periodeRealiseeDAO);
        this.instance.setLocalizationManager(this.localizationManager);
        this.instance.setPolicyManager(this.policyManager);
        Mockito.when(this.tempo.getAnneeRealisee()).thenReturn(this.anneeRealisee);
        Mockito.doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0, String.class);
            }
        }).when(this.instance).getInternationalizedValeurQualitative(any(String.class));
        when(periodeRealiseeDAO.getByNKey(any(), anyInt())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getOrCreatePeriodeRealisee method, of class
     * SemisPeriodeRealiseeFactory.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetOrCreatePeriodeRealisee() throws PersistenceException {
        ACBBUtils.localizationManager = MockUtils.localizationManager;
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.when(this.line.getOriginalLineNumber()).thenReturn(1L);
        Mockito.when(this.line.getRotationNumber()).thenReturn(10);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(2);
        Mockito.when(this.line.getTreatmentVersion()).thenReturn(1);
        Mockito.when(this.line.getTreatmentAffichage()).thenReturn("T4");
        // case of a secondary periode
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(0);
        Mockito.when(this.line.getSemisRotationDescriptor()).thenReturn(this.rotationDescriptor);
        Mockito.when(this.rotationDescriptor.isExpectedCouvert()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(1);
        Mockito.when(this.line.getCouvertVegetal()).thenReturn(this.couvertVegetal);
        final Map<Integer, SemisCouvertVegetal> covers = new HashMap();
        Mockito.doReturn(covers).when(this.rotationDescriptor).getCouverts();
        Mockito.when(this.couvertVegetal.getValeur()).thenReturn("expected cover", "cover");
        this.rotationDescriptor.getCouverts().put(2, this.couvertVegetal);
        PeriodeRealisee result = this.instance.getOrCreatePeriodeRealisee(this.line, errorsReport).orElse(null);
        Assert.assertNotNull(result);
        Assert.assertEquals(this.couvertVegetal, result.getCouvertVegetal());
        Mockito.verify(this.periodeRealiseeDAO).saveOrUpdate(result);
        // primary sowing with not expected cover but possible
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(0);
        Mockito.when(this.rotationDescriptor.isExpectedCouvert()).thenReturn(Boolean.FALSE);
        Mockito.when(this.rotationDescriptor.isPossibleCouvert()).thenReturn(Boolean.TRUE);
        result = this.instance.getOrCreatePeriodeRealisee(this.line, errorsReport).orElse(null);
        Assert.assertNull(result);
        Assert.assertTrue(errorsReport.hasInfos());
        Assert.assertFalse(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, une anomalie est survenue lors de la génération de la période 0 de l'année 2 de la rotation 10 de la version 1 du traitement T4 :  : Le Semis attendu est cover au lieu de expected cover.\n",
                errorsReport.getInfosMessages());
        Mockito.verify(this.tempo, Mockito.never()).setInErrors(Boolean.TRUE);
        // primary sowing with not expected cover and not possible cover (secondary periode)
        Mockito.when(this.rotationDescriptor.isPossibleCouvert()).thenReturn(Boolean.FALSE);
        errorsReport = new ErrorsReport();
        result = this.instance.getOrCreatePeriodeRealisee(this.line, errorsReport).orElse(null);
        Assert.assertNull(result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertFalse(errorsReport.hasInfos());
        Assert.assertEquals(
                "-Ligne 1, une erreur est survenue lors de la génération de la période 0 de l'année 2 de la rotation 10 de la version 1 du traitement T4 :  :la période doit être 1 pour une culture secondaire.\n",
                errorsReport.getErrorsMessages());
        Mockito.verify(this.tempo, Mockito.times(1)).setInErrors(Boolean.TRUE);
        // existing periode réalisee with non null period
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(1);
        Mockito.when(this.periodeRealiseeDAO.getByNKey(this.anneeRealisee, 1)).thenReturn(Optional.ofNullable(this.periodeRealisee));
        Set<AbstractIntervention> interventions = new TreeSet();
        Mockito.when(this.periodeRealisee.getBindedsITK()).thenReturn(interventions);
        interventions.add(m.intervention1);
        interventions.add(m.intervention2);
        result = this.instance.getOrCreatePeriodeRealisee(this.line, errorsReport).orElse(null);
        Assert.assertNull(result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertFalse(errorsReport.hasInfos());
        Assert.assertEquals(
                "-Ligne 1, une erreur est survenue lors de la génération de la période 0 de l'année 2 de la rotation 10 de la version 1 du traitement T4 :  :la période doit être 1 pour une culture secondaire.\n"
                        + "-La période 1 de l'année 2 de la rotation 10 de la version 1 du traitement T4 existe déjà. Elle a pu être générée par le ou les fichier(s) suivant(s)  : \n"
                        + "\n"
                        + "version 1 du fichier file 1 à la date 01/01/2012\n"
                        + "version 2 du fichier file 2 à la date 31/12/2012\n"
                        + ".\n",
                errorsReport.getErrorsMessages());
    }

}
