/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.IBiomassProductionDAO;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.ValeurBiomassProduction;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;

/**
 * @author ptcherniati
 */
public class BuildBiomassProductionHelperTest {

    MockUtils m = MockUtils.getInstance();
    TestInstance instance;
    @Mock
    IBiomassProductionDAO biomassProductionDAO;
    @Mock
    BiomassProduction biomassProduction;
    @Mock
    BiomassProductionLineRecord line;
    @Mock
    List<BiomassProductionLineRecord> lines;
    @Mock
    VariableValueBiomassProduction valueBiomassProduction1;
    @Mock
    VariableValueBiomassProduction valueBiomassProduction2;
    @Mock
    VariableACBB variable1;
    @Mock
    VariableACBB variable2;
    @Mock
    RequestPropertiesBiomassProduction requestProperties;
    @Mock(name = "dbDatatypeVariableUnites")
    Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites;
    @Mock
    DatatypeVariableUniteACBB dvu1;
    @Mock
    DatatypeVariableUniteACBB dvu2;
    @Mock
    AbstractMethode method1;
    @Mock
    AbstractMethode method2;
    List<VariableValueBiomassProduction> variablesValues = new LinkedList();
    Map<Parcelle, Map<LocalDate, Map<Long, BiomassProduction>>> biomassProductions = new HashMap();
    Map<LocalDate, Map<Long, BiomassProduction>> biomassProductionsByDate = new HashMap();
    Map<Long, BiomassProduction> biomassProductionsByDateAndNature = new HashMap();
    @Mock
    MasseVegClass masseVegClass;
    ErrorsReport errorsReport;
    /**
     *
     */
    public BuildBiomassProductionHelperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    protected void initVariablesValues() {
        Mockito.when(this.line.getVariablesValues()).thenReturn(this.variablesValues);
        this.variablesValues.add(this.valueBiomassProduction1);
        this.variablesValues.add(this.valueBiomassProduction2);
        Mockito.when(this.valueBiomassProduction1.getDatatypeVariableUnite()).thenReturn(this.dvu1);
        Mockito.when(this.valueBiomassProduction1.getValue()).thenReturn("1.1");
        Mockito.when(this.valueBiomassProduction1.getMeasureNumber()).thenReturn(11);
        Mockito.when(this.valueBiomassProduction1.getMethode()).thenReturn(this.method1);
        Mockito.when(this.valueBiomassProduction1.getQualityClass()).thenReturn(12);
        Mockito.when(this.valueBiomassProduction1.getStandardDeviation()).thenReturn(13F);
        Mockito.when(this.valueBiomassProduction2.getDatatypeVariableUnite()).thenReturn(this.dvu2);
        Mockito.when(this.valueBiomassProduction2.getValue()).thenReturn("2.1");
        Mockito.when(this.valueBiomassProduction2.getMeasureNumber()).thenReturn(21);
        Mockito.when(this.valueBiomassProduction2.getMethode()).thenReturn(this.method2);
        Mockito.when(this.valueBiomassProduction2.getQualityClass()).thenReturn(22);
        Mockito.when(this.valueBiomassProduction2.getStandardDeviation()).thenReturn(23F);
        Mockito.when(this.variable1.getCode()).thenReturn("variable1");
        Mockito.when(this.variable2.getCode()).thenReturn("variable2");
    }

    /**
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        this.instance = new TestInstance();
        this.instance.setBiomassProductionDAO(this.biomassProductionDAO);
        this.instance.setDatatypeName(MockUtils.DATATYPE);
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.updateInstance();
        this.initVariablesValues();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addValeursBiomassProduction method, of class
     * BuildBiomassProductionHelper.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testAddValeursBiomassProduction() throws PersistenceException {
        // nominal
        List<ValeurBiomassProduction> valeurBiomassProductions = new LinkedList();
        Mockito.when(this.biomassProduction.getValeursBiomassProduction()).thenReturn(
                valeurBiomassProductions);
        this.instance.addValeursBiomassProduction(this.line, this.biomassProduction);
        this.verifieValeur(valeurBiomassProductions.get(0), this.method1, 1.1F, this.dvu1, 11, 13F,
                12);
        this.verifieValeur(valeurBiomassProductions.get(1), this.method2, 2.1F, this.dvu2, 21, 23F,
                22);
    }

    /**
     * Test of build method, of class BuildBiomassProductionHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild() throws Exception {
        // nominal
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(this.biomassProduction).when(this.instance)
                .getOrCreateBiomassProduction(this.line);
        this.lines = new LinkedList();
        this.lines.add(this.line);
        this.updateInstance();
        Mockito.doNothing().when(this.instance)
                .addValeursBiomassProduction(this.line, this.biomassProduction);
        this.instance.build();
        Mockito.verify(this.biomassProductionDAO, Mockito.times(1)).saveOrUpdate(
                this.biomassProduction);
        // with errormessage
        this.errorsReport.addErrorMessage("erreur");
        try {
            this.instance.build();
            Assert.fail("no exception");
        } catch (PersistenceException e) {
            Assert.assertEquals("-erreur\n", e.getMessage());
        }
    }

    /**
     * Test of build method, of class BuildBiomassProductionHelper.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuild_4args() throws Exception {
        this.instance = Mockito.spy(this.instance);
        Mockito.doNothing().when(this.instance)
                .update(this.m.versionFile, this.lines, this.errorsReport, this.requestProperties);
        Mockito.doNothing().when(this.instance).build();
        this.instance.build(this.m.versionFile, this.lines, this.errorsReport,
                this.requestProperties);
    }

    /**
     * Test of createNewBiomassProduction method, of class
     * BuildBiomassProductionHelper.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testCreateNewBiomassProduction() throws PersistenceException {
        MasseVegClass masseVegClass = Mockito.mock(MasseVegClass.class);
        InterventionType interventionType = Mockito.mock(InterventionType.class);
        AbstractIntervention intervention = Mockito.mock(AbstractIntervention.class);
        Mockito.when(this.line.getNatureCouvert()).thenReturn("natureCouvert");
        Mockito.when(this.line.getMasseVegClass()).thenReturn(masseVegClass);
        Mockito.when(this.line.getDateDebutRepousse()).thenReturn(this.m.dateDebut);
        Mockito.when(this.line.getObservation()).thenReturn("observation");
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateFin);
        Mockito.when(this.line.getRotationNumber()).thenReturn(1);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(2);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(3);
        Mockito.when(this.line.getSuiviParcelle()).thenReturn(this.m.suiviParcelle);
        Mockito.when(this.line.getTypeIntervention()).thenReturn(interventionType);
        Mockito.when(this.line.getIntervention()).thenReturn(intervention);
        // nominal
        BiomassProduction result = this.instance.createNewBiomassProduction(this.line);
        ArgumentCaptor<BiomassProduction> biomassProduction = ArgumentCaptor
                .forClass(BiomassProduction.class);
        Mockito.verify(this.biomassProductionDAO).saveOrUpdate(biomassProduction.capture());
        Assert.assertEquals(result, biomassProduction.getValue());
        Assert.assertEquals("natureCouvert", biomassProduction.getValue().getNatureCouvert());
        Assert.assertEquals(masseVegClass, biomassProduction.getValue().getMasseVegClass());
        Assert.assertEquals(this.m.dateDebut, biomassProduction.getValue().getDateDebutRepousse());
        Assert.assertEquals("observation", biomassProduction.getValue().getObservation());
        Assert.assertEquals(this.m.dateFin, biomassProduction.getValue().getDate());
        Assert.assertEquals((Integer) 1, biomassProduction.getValue()
                .getVersionTraitementRealiseeNumber());
        Assert.assertEquals((Integer) 2, biomassProduction.getValue().getAnneeRealiseeNumber());
        Assert.assertEquals((Integer) 3, biomassProduction.getValue().getPeriodeRealiseeNumber());
        Assert.assertEquals(this.m.suiviParcelle, biomassProduction.getValue().getSuiviParcelle());
        Assert.assertEquals(interventionType, biomassProduction.getValue().getInterventionType());
        Assert.assertEquals(intervention, biomassProduction.getValue().getIntervention());
        // with exception
        Mockito.doThrow(new PersistenceException("Erreur")).when(this.biomassProductionDAO)
                .saveOrUpdate(any(BiomassProduction.class));
        result = this.instance.createNewBiomassProduction(this.line);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Une erreur inconnue est survenue lors de l'enregistrement des données. La publication a été annulée.\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getOrCreateBiomassProduction method, of class
     * BuildBiomassProductionHelper.
     */
    @Test
    public void testGetOrCreateBiomassProduction() {
        instance.biomassProductions = this.biomassProductions;
        Mockito.when(this.line.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateDebut);
        this.biomassProductions.put(this.m.parcelle, this.biomassProductionsByDate);
        this.biomassProductionsByDate.put(this.m.dateDebut, biomassProductionsByDateAndNature);
        this.biomassProductionsByDateAndNature.put(2L, biomassProduction);
        Mockito.when(this.biomassProduction.getMasseVegClass()).thenReturn(this.masseVegClass);
        Mockito.when(this.masseVegClass.getId()).thenReturn(2L);
        Mockito.when(this.line.getMasseVegClass()).thenReturn(masseVegClass);
        // existing biomasseProduction
        this.instance.biomassProductions = this.biomassProductions;
        BiomassProduction result = this.instance.getOrCreateBiomassProduction(this.line);
        Assert.assertEquals(this.biomassProduction, result);
        // new biomasseProduction
        this.biomassProductions.get(this.m.parcelle).get(this.m.dateDebut).remove(2L);
        this.instance = Mockito.spy(this.instance);
        BiomassProduction bm = Mockito.mock(BiomassProduction.class);
        Mockito.doReturn(bm).when(this.instance).createNewBiomassProduction(this.line);
        result = this.instance.getOrCreateBiomassProduction(this.line);
        Assert.assertEquals(bm, this.biomassProductions.get(this.m.parcelle).get(this.m.dateDebut).get(2L));
        Assert.assertEquals(bm, result);
        // new biomassProductionsByDate and new biomasseProduction
        this.biomassProductions.get(this.m.parcelle).remove(this.m.dateDebut);
        result = this.instance.getOrCreateBiomassProduction(this.line);
        Assert.assertEquals(bm, this.biomassProductions.get(this.m.parcelle).get(this.m.dateDebut).get(2L));
        Assert.assertEquals(bm, result);
        // new biomassProductionsByDate and new biomasseProduction
        this.biomassProductions.remove(this.m.parcelle);
        result = this.instance.getOrCreateBiomassProduction(this.line);
        Assert.assertEquals(bm, this.biomassProductions.get(this.m.parcelle).get(this.m.dateDebut).get(2L));
        Assert.assertEquals(bm, result);
    }

    /**
     * @throws PersistenceException
     */
    protected void updateInstance() throws PersistenceException {
        this.errorsReport = new ErrorsReport();
        Mockito.when(
                this.m.datatypeVariableUniteDAO
                        .getAllVariableTypeDonneesByDataTypeMapByVariableCode(MockUtils.DATATYPE))
                .thenReturn(this.dbDatatypeVariableUnites);
        Mockito.when(this.dbDatatypeVariableUnites.get("variable1")).thenReturn(this.dvu1);
        Mockito.when(this.dbDatatypeVariableUnites.get("variable2")).thenReturn(this.dvu2);
        Mockito.when(this.m.datatypeVariableUniteDAO.merge(this.dvu1)).thenReturn(this.dvu1);
        Mockito.when(this.m.datatypeVariableUniteDAO.merge(this.dvu2)).thenReturn(this.dvu2);
        this.instance.update(this.m.versionFile, this.lines, this.errorsReport,
                this.requestProperties);
    }

    private void verifieValeur(ValeurBiomassProduction valeur, AbstractMethode methode,
                               Float valeurFloat, DatatypeVariableUniteACBB dvu, int measureNumber,
                               Float standardDeviation, int qualityIndex) {
        Assert.assertEquals(this.biomassProduction, valeur.getBiomassProduction());
        Assert.assertEquals(valeurFloat, valeur.getValeur());
        Assert.assertEquals(methode, valeur.getMethode());
        Assert.assertEquals(dvu, instance.variables.pop());
        Assert.assertEquals(measureNumber, valeur.getMeasureNumber());
        Assert.assertEquals(standardDeviation, valeur.getStandardDeviation());
        Assert.assertEquals(qualityIndex, valeur.getQualityIndex());
    }

    class TestInstance extends BuildBiomassProductionHelper {
        RealNode realNode = mock(RealNode.class);
        LinkedList<DatatypeVariableUniteACBB> variables = new LinkedList();

        @Override
        protected RealNode getRealNodeForSequenceAndVariable(DatatypeVariableUniteACBB variable) {
            variables.add(variable);
            return realNode;
        }

    }

}
