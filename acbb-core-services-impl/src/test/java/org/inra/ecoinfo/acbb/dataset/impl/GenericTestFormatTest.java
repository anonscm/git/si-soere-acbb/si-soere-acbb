package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.*;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.*;
import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class GenericTestFormatTest {

    MockUtils m = MockUtils.getInstance();
    @Mock
    ITestHeaders testHeaders;
    @Mock
    ITestValues testValues;
    @Mock
    CSVParser parser;
    @Mock
    IRequestPropertiesACBB requestProperties;
    @Mock
    BadsFormatsReport badsFormatsReport;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Spy
    ITestFormat instance = new GenericTestFormat();
    /**
     *
     */
    public GenericTestFormatTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance.setTestHeaders(this.testHeaders);
        this.instance.setTestValues(this.testValues);
        this.instance.setDatatypeName("name");

    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of testFormat method, of class GenericTestFormat.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestFormat() throws Exception {
        String encoding = "UTF-8";
        ArgumentCaptor<BadsFormatsReport> bf = ArgumentCaptor.forClass(BadsFormatsReport.class);
        this.instance.testFormat(this.parser, this.m.versionFile, this.requestProperties, encoding,
                this.datasetDescriptor);
        Mockito.verify(this.testHeaders).testHeaders(same(this.parser),
                same(this.m.versionFile), same(this.requestProperties),
                same(encoding), bf.capture(), same(this.datasetDescriptor));
        Mockito.verify(this.testValues).testValues(eq(0L), same(this.parser),
                same(this.m.versionFile), same(this.requestProperties),
                same(encoding), bf.capture(), same(this.datasetDescriptor),
                same("name"));

    }

}
