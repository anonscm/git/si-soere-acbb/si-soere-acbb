/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class DefaultVersionDeTraitementRealiseeFactoryTest {

    DefaultVersionDeTraitementRealiseeFactory<AbstractLineRecord> instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    VersionTraitementRealisee versionTraitementRealisee;
    @Mock
    AnneeRealisee anneeRealisee;
    @Mock
    PeriodeRealisee periodeRealisee;
    @Mock
    IVersionDeTraitementRealiseeDAO versionDeTraitementRealiseeDAO;
    @Mock
    DefaultAnneeRealiseeFactory anneeRealiseeFactory;
    @Mock
    DefaultPeriodeRealiseeFactory periodeRealiseeFactory;
    @Mock
    ITempoDAO tempoDAO;
    @Mock
    AbstractLineRecord line;
    @Mock
    Tempo tempo;
    /**
     *
     */
    public DefaultVersionDeTraitementRealiseeFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new DefaultVersionDeTraitementRealiseeFactory();
        MockitoAnnotations.initMocks(this);
        this.instance.setAnneeRealiseeFactory(this.anneeRealiseeFactory);
        this.instance.setTempoDAO(this.tempoDAO);
        this.instance.setVersionDeTraitementRealiseeDAO(this.versionDeTraitementRealiseeDAO);
        when(versionTraitementRealisee.getNumero()).thenReturn(1);
        when(anneeRealisee.getAnnee()).thenReturn(2);
        when(periodeRealisee.getNumero()).thenReturn(3);
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.line.getRotationNumber()).thenReturn(1);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(2);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(3);
        Mockito.when(this.anneeRealiseeFactory.getPeriodeRealiseeFactory()).thenReturn(this.periodeRealiseeFactory);
        when(tempoDAO.getByNKey(any(), any(), anyInt(), anyInt(), anyInt())).thenReturn(Optional.empty());
        when(versionDeTraitementRealiseeDAO.getByNKey(any(), any(), anyInt())).thenReturn(Optional.empty());
        when(versionTraitementRealisee.getAnneesRealisees()).thenReturn(new HashMap());
        when(anneeRealisee.getPeriodesRealisees()).thenReturn(new HashMap());
        this.tempo = spy(new Tempo());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getNewTempo method, of class DefaultVersionDeTraitementRealiseeFactory.
     */
    @Test
    public void testGetNewTempo() {
        // nullVersion
        Tempo result = this.instance.getNewTempo(this.m.versionDeTraitement, this.m.parcelle, 1, 2, 3);
        Assert.assertNull(result.getVersionTraitementRealisee());
        Assert.assertNull(result.getPeriodeRealisee());
        Assert.assertNull(result.getAnneeRealisee());
        // nonNullVersion
        Mockito.when(this.versionDeTraitementRealiseeDAO.getByNKey(this.m.versionDeTraitement, this.m.parcelle, 1)).thenReturn(Optional.of(this.versionTraitementRealisee));
        Mockito.when(this.versionTraitementRealisee.getVersionDeTraitement()).thenReturn(
                this.m.versionDeTraitement);
        result = this.instance.getNewTempo(this.m.versionDeTraitement, this.m.parcelle, 1, 2, 3);
        Assert.assertEquals(this.versionTraitementRealisee, result.getVersionTraitementRealisee());
        Assert.assertNull(result.getPeriodeRealisee());
        Assert.assertNull(result.getAnneeRealisee());
    }

    /**
     * Test of getOrCreateVersiontraitementRealise method, of class
     * DefaultVersionDeTraitementRealiseeFactory.
     */
    @Test
    public void testGetOrCreateVersiontraitementRealise() {
        // new version
        InfoReport inforeport = new InfoReport();
        ErrorsReport errorsReport = new ErrorsReport();
        VersionTraitementRealisee expResult = null;
        VersionTraitementRealisee result = this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, inforeport);
        Assert.assertEquals(expResult, result);
        // existing version
        doReturn(tempo).when(line).getTempo();
        doReturn(this.versionTraitementRealisee).when(tempo).getVersionTraitementRealisee();
        result = this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, inforeport);
        Assert.assertEquals(this.versionTraitementRealisee, result);
    }

    /**
     * Test of getTempo method, of class DefaultVersionDeTraitementRealiseeFactory.
     */
    @Test
    public void testGetTempo() {
        this.instance = Mockito.spy(this.instance);
        // with new tempo
        InfoReport infoReport = new InfoReport();
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doReturn(this.tempo).when(this.instance).getNewTempo(this.m.versionDeTraitement, this.m.parcelle, 1, 2, 3);
        Mockito.doReturn(this.versionTraitementRealisee).when(instance).getOrCreateVersiontraitementRealise(this.line, errorsReport, infoReport);
        when(anneeRealiseeFactory.getOrCreateAnneeRealisee(line, errorsReport, infoReport)).thenReturn(anneeRealisee);
        doReturn(Optional.of(periodeRealisee)).when(periodeRealiseeFactory).getOrCreatePeriodeRealisee(line, errorsReport);
        doAnswer((invocation) -> {
            VersionTraitementRealisee v = invocation.getArgument(0, VersionTraitementRealisee.class);
            Assert.assertEquals(versionTraitementRealisee, v);
            invocation.callRealMethod();
            return null;
        }).when(tempo).setVersionTraitementRealisee(versionTraitementRealisee);
        doAnswer((invocation) -> {
            AnneeRealisee ar = invocation.getArgument(0, AnneeRealisee.class);
            Assert.assertEquals(anneeRealisee, ar);
            return null;
        }).when(tempo).setAnneeRealisee(anneeRealisee);
        doAnswer((invocation) -> {
            PeriodeRealisee p = invocation.getArgument(0, PeriodeRealisee.class);
            Assert.assertEquals(periodeRealisee, p);
            return null; //To change body of generated lambdas, choose Tools | Templates.
        }).when(tempo).setPeriodeRealisee(periodeRealisee);
        Tempo result = this.instance.getTempo(this.line, this.m.versionDeTraitement, this.m.parcelle, errorsReport, infoReport);
        Assert.assertEquals(this.tempo, result);
        verify(tempo).setVersionTraitementRealisee(versionTraitementRealisee);
        verify(tempo).setAnneeRealisee(anneeRealisee);
        verify(tempo).setPeriodeRealisee(periodeRealisee);
        Mockito.verify(this.line).setTempo(this.tempo);
        // with existing tempo
        Mockito.doReturn(Optional.of(this.tempo)).when(this.tempoDAO).getByNKey(this.m.versionDeTraitement, this.m.parcelle, 1, 2, 3);
        result = this.instance.getTempo(this.line, this.m.versionDeTraitement, this.m.parcelle,
                errorsReport, infoReport);
        Assert.assertEquals(this.versionTraitementRealisee, result.getVersionTraitementRealisee());
        verify(tempo, times(1)).setVersionTraitementRealisee(versionTraitementRealisee);
        verify(tempo, times(1)).setAnneeRealisee(anneeRealisee);
        verify(tempo, times(1)).setPeriodeRealisee(periodeRealisee);
        Mockito.verify(this.line, Mockito.times(2)).setTempo(this.tempo);
        // with annee
        Mockito.doReturn(this.anneeRealisee).when(this.anneeRealiseeFactory)
                .getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Map<Integer, AnneeRealisee> anneesRealisees = new HashMap();
        anneesRealisees.put(1, this.anneeRealisee);
        Mockito.when(this.versionTraitementRealisee.getAnneesRealisees()).thenReturn(
                anneesRealisees);
        when(tempoDAO.getByNKey(any(), any(), anyInt(), anyInt(), anyInt())).thenReturn(Optional.empty());
        result = this.instance.getTempo(this.line, this.m.versionDeTraitement, this.m.parcelle,
                errorsReport, infoReport);
        Assert.assertEquals(this.versionTraitementRealisee, result.getVersionTraitementRealisee());
        verify(tempo, times(2)).setVersionTraitementRealisee(versionTraitementRealisee);
        verify(tempo, times(2)).setAnneeRealisee(anneeRealisee);
        verify(tempo, times(2)).setPeriodeRealisee(periodeRealisee);
        Mockito.verify(this.line, Mockito.times(3)).setTempo(this.tempo);
        // with periode
        Map<Integer, PeriodeRealisee> periodesRealisees = new HashMap();
        periodesRealisees.put(1, this.periodeRealisee);
        Mockito.when(this.anneeRealisee.getPeriodesRealisees()).thenReturn(periodesRealisees);
        Mockito.when(
                this.periodeRealiseeFactory.getOrCreatePeriodeRealisee(this.line, errorsReport))
                .thenReturn(Optional.of(this.periodeRealisee));
        result = this.instance.getTempo(this.line, this.m.versionDeTraitement, this.m.parcelle,
                errorsReport, infoReport);
        Assert.assertEquals(this.versionTraitementRealisee, result.getVersionTraitementRealisee());
        verify(tempo, times(3)).setVersionTraitementRealisee(versionTraitementRealisee);
        verify(tempo, times(3)).setAnneeRealisee(anneeRealisee);
        verify(tempo, times(3)).setPeriodeRealisee(periodeRealisee);
        Mockito.verify(this.line, Mockito.times(4)).setTempo(this.tempo);

    }

}
