/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.modalite;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.variablededescription.IVariableDeDescriptionDAO;
import org.inra.ecoinfo.acbb.refdata.variablededescription.VariableDescription;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    public static final String MODALITE_NOM = "PT>20, blé, maïs";

    /**
     *
     */
    public static final String MODALITE_DESCRIPTION = "témoins prairie temporaire";
    MockUtils m = MockUtils.getInstance();
    Recorder instance;
    String encoding = "UTF-8";
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    Properties propertiesNomEN;
    @Mock
    Properties propertiesCommentaireEN;
    @Mock
    IListeItineraireDAO listeItineraireDAO;
    @Mock
    IModaliteDAO modaliteDAO;
    @Mock
    Modalite modalite;
    @Mock
    Rotation rotation;
    @Mock
    IVariableDeDescriptionDAO variableDeDescriptionDAO;
    @Mock
    VariableDescription variableDescription;
    @Mock
    SemisCouvertVegetal ble;
    @Mock
    SemisCouvertVegetal mais;
    @Mock
    SemisCouvertVegetal pt;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    public void initInstance(Recorder instance) {
        MockitoAnnotations.initMocks(this);
        instance.setListeItineraireDAO(this.listeItineraireDAO);
        instance.setModaliteDAO(this.modaliteDAO);
        instance.setVariableDeDescriptionDAO(this.variableDeDescriptionDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        when(modaliteDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
    }

    private void initModalite() {
        Mockito.when(this.modalite.getVariableDescription()).thenReturn(this.variableDescription);
        Mockito.when(this.variableDescription.getNom()).thenReturn("Rotation");
        Mockito.when(this.modalite.getCode()).thenReturn("r1");
        Mockito.when(this.modalite.getNom()).thenReturn(RecorderTest.MODALITE_NOM);
        Mockito.when(this.modalite.getDescription()).thenReturn(RecorderTest.MODALITE_DESCRIPTION);
        Mockito.when(
                this.propertiesNomEN.getProperty(RecorderTest.MODALITE_NOM,
                        RecorderTest.MODALITE_NOM)).thenReturn("nom_en");
        Mockito.when(
                this.propertiesCommentaireEN.getProperty(RecorderTest.MODALITE_DESCRIPTION,
                        RecorderTest.MODALITE_DESCRIPTION)).thenReturn("description_en");
        when(modaliteDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.m = MockUtils.getInstance();
        Localization.setLocalisations(Arrays.asList("fr", "en"));
        this.instance = new Recorder();
        this.initInstance(this.instance);
        Mockito.when(
                m.localizationDAO.newProperties(Modalite.NAME_ENTITY_JPA,
                        Modalite.ATTRIBUTE_JPA_NOM, Locale.ENGLISH)).thenReturn(
                this.propertiesNomEN);
        Mockito.when(
                m.localizationDAO.newProperties(Modalite.NAME_ENTITY_JPA,
                        Modalite.ATTRIBUTE_JPA_DESCRIPTION, Locale.ENGLISH)).thenReturn(
                this.propertiesCommentaireEN);
        Mockito.when(this.ble.getCode()).thenReturn(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL);
        Mockito.when(this.mais.getCode()).thenReturn(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL);
        Mockito.when(this.pt.getCode()).thenReturn(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL);
        Mockito.when(this.ble.getValeur()).thenReturn("Ble");
        Mockito.when(this.mais.getValeur()).thenReturn("Mais");
        Mockito.when(this.pt.getValeur()).thenReturn("PT");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        BusinessException error = null;
        String text = "Rotation;R1;PT>20, blé, maïs, orge;TP>20, wheat, corn, barley;témoins prairie temporaire;ley witnesses";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(this.modaliteDAO.getByNKey("rotation", "r1")).thenReturn(Optional.of(this.modalite));
        File file = null;
        // nominal case
        this.instance.deleteRecord(parser, file, this.encoding);
        Mockito.verify(this.modaliteDAO).remove(this.modalite);
        // null modalite
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doReturn(Optional.empty()).when(this.modaliteDAO).getByNKey("rotation", "r1");
        try {
            this.instance.deleteRecord(parser, file, this.encoding);
            fail();
        } catch (BusinessException e) {
            Mockito.verify(this.modaliteDAO).remove(this.modalite);
            Assert.assertEquals("bad modality", e.getMessage());
        }
        Mockito.verify(this.modaliteDAO).remove(this.modalite);
        // peristence exception
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(this.modaliteDAO.getByNKey("rotation", "r1")).thenReturn(Optional.of(this.modalite));
        Mockito.doThrow(new PersistenceException("error2")).when(this.modaliteDAO)
                .remove(this.modalite);
        try {
            this.instance.deleteRecord(parser, file, this.encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<Modalite> modalites = Arrays.asList(this.modalite);
        Mockito.when(this.modaliteDAO.getAll()).thenReturn(modalites);
        List<Modalite> result = this.instance.getAllElements();
        Mockito.verify(this.modaliteDAO).getAll();
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(modalites.get(0), result.get(0));
    }

    /**
     * Test of getNamesVariablesPossibles method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNamesVariablesPossibles() throws Exception {
        Mockito.when(this.variableDeDescriptionDAO.getAll(VariableDescription.class)).thenReturn(
                Arrays.asList(this.variableDescription));
        Mockito.when(this.variableDescription.getNom()).thenReturn("Rotation");
        String[] result = this.instance.getNamesVariablesPossibles();
        Assert.assertEquals("Rotation", result[0]);
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        final List<Modalite> modalites = Arrays.asList(this.modalite);
        Mockito.when(this.modaliteDAO.getAll()).thenReturn(modalites);
        this.instance.initModelGridMetadata();
        this.initModalite();
        LineModelGridMetadata result = this.instance.getNewLineModelGridMetadata(this.modalite);
        Assert.assertTrue(6 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("Rotation", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("r1", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals(RecorderTest.MODALITE_NOM, result.getColumnModelGridMetadataAt(2)
                .getValue());
        Assert.assertEquals("nom_en", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals(RecorderTest.MODALITE_DESCRIPTION,
                result.getColumnModelGridMetadataAt(4).getValue());
        Assert.assertEquals("description_en", result.getColumnModelGridMetadataAt(5).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        Mockito.when(this.variableDeDescriptionDAO.getAll(VariableDescription.class)).thenReturn(
                Arrays.asList(this.variableDescription));
        Mockito.when(this.variableDescription.getNom()).thenReturn("Rotation");
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        this.instance.initModelGridMetadata();
        Mockito.verify(m.localizationDAO).newProperties(Modalite.NAME_ENTITY_JPA,
                Modalite.ATTRIBUTE_JPA_DESCRIPTION, Locale.ENGLISH);
        Mockito.verify(m.localizationDAO).newProperties(Modalite.NAME_ENTITY_JPA,
                Modalite.ATTRIBUTE_JPA_NOM, Locale.ENGLISH);
        Mockito.verify(this.variableDeDescriptionDAO).getAll(VariableDescription.class);
        Assert.assertEquals("Rotation", this.instance.variablesDeForcagePossibles[0]);
        Assert.assertEquals(this.propertiesCommentaireEN, this.instance.propertiesCommentaireEN);
        Assert.assertEquals(this.propertiesNomEN, this.instance.propertiesNomEN);
    }

    /**
     * Test of persistModalite method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testPersistModalite() throws Exception {
        ErrorsReport errorsReport = Mockito.mock(ErrorsReport.class);
        final LinkedList modalites = new LinkedList();
        Mockito.when(this.variableDeDescriptionDAO.getByCode("rotation")).thenReturn(Optional.of(this.variableDescription));
        Mockito.when(this.variableDescription.getCode()).thenReturn("rotation");
        Mockito.when(this.variableDescription.getModalites()).thenReturn(modalites);
        Mockito.when(this.listeItineraireDAO.getByNKey(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL, "ble")).thenReturn(Optional.of(this.ble));
        Mockito.when(this.listeItineraireDAO.getByNKey(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL, "mais")).thenReturn(Optional.of(this.mais));
        Mockito.when(this.listeItineraireDAO.getByNKey(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL, "pt")).thenReturn(Optional.of(this.pt));
        ArgumentCaptor<Modalite> newModalite = ArgumentCaptor.forClass(Modalite.class);
        // create rotation
        this.instance.persistModalite(errorsReport, "rotation", "r1", RecorderTest.MODALITE_NOM, RecorderTest.MODALITE_DESCRIPTION);
        Mockito.verify(this.modaliteDAO).saveOrUpdate(newModalite.capture());
        Rotation r = (Rotation) newModalite.getValue();
        Assert.assertEquals("r1", r.getCode());
        Assert.assertEquals(RecorderTest.MODALITE_DESCRIPTION, r.getDescription());
        Assert.assertEquals(RecorderTest.MODALITE_NOM, r.getNom());
        Assert.assertEquals(this.variableDescription, r.getVariableDescription());
        Assert.assertEquals("PT", r.getRepeatedCouvert());
        Assert.assertTrue(r.isInfiniteRepeatedCouvert());
        Map<Integer, SemisCouvertVegetal> couvertsVegetal = r.getCouvertsVegetal();
        Assert.assertTrue(22 == couvertsVegetal.size());
        for (int i = 1; i < 21; i++) {
            Assert.assertEquals(this.pt, couvertsVegetal.get(i));
        }
        Assert.assertEquals(this.ble, couvertsVegetal.get(21));
        Assert.assertEquals(this.mais, couvertsVegetal.get(22));
        // update rotation
        Mockito.when(this.modaliteDAO.getByNKey("rotation", "r1")).thenReturn(Optional.of(this.rotation));
        Mockito.when(this.rotation.getNom()).thenReturn(RecorderTest.MODALITE_NOM);
        Mockito.when(this.rotation.getCouvertsVegetal()).thenReturn(couvertsVegetal);
        this.instance.persistModalite(errorsReport, "rotation", "r1", RecorderTest.MODALITE_NOM,
                RecorderTest.MODALITE_DESCRIPTION);
        Mockito.verify(this.modaliteDAO).saveOrUpdate(this.rotation);
        Assert.assertTrue(couvertsVegetal.isEmpty());
        Mockito.verify(this.rotation).updateRepeatedCouverts(true, 20, this.pt, 1);
        Mockito.verify(this.rotation).updateRepeatedCouverts(false, 1, this.ble, 0);
        Mockito.verify(this.rotation).updateRepeatedCouverts(false, 1, this.mais, 0);
        // create modalite
        Mockito.when(this.variableDeDescriptionDAO.getByCode("pasrotation")).thenReturn(
                Optional.of(this.variableDescription));
        Mockito.when(this.variableDescription.getCode()).thenReturn("pasRotation");
        this.instance.persistModalite(errorsReport, "pasrotation", "r1", RecorderTest.MODALITE_NOM,
                RecorderTest.MODALITE_DESCRIPTION);
        Mockito.verify(this.modaliteDAO, Mockito.times(3)).saveOrUpdate(newModalite.capture());
        Modalite m = newModalite.getValue();
        Assert.assertEquals("r1", m.getCode());
        Assert.assertEquals(RecorderTest.MODALITE_DESCRIPTION, m.getDescription());
        Assert.assertEquals(RecorderTest.MODALITE_NOM, m.getNom());
        Assert.assertEquals(this.variableDescription, m.getVariableDescription());
        // update modalite
        Mockito.when(this.modaliteDAO.getByNKey("pasrotation", "r1")).thenReturn(Optional.of(this.modalite));
        this.instance.persistModalite(errorsReport, "pasrotation", "r1", RecorderTest.MODALITE_NOM,
                RecorderTest.MODALITE_DESCRIPTION);
        Mockito.verify(this.modaliteDAO).saveOrUpdate(this.modalite);
        Mockito.verify(this.modaliteDAO, Mockito.times(4)).saveOrUpdate(newModalite.capture());
        m = newModalite.getValue();
        Assert.assertEquals(this.modalite, m);
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String text = "Variable de forcage;code;nom_fr;nom_en;description_fr;description_en\n"
                + "Rotation;R1;PT>20, blé, maïs, orge;TP>20, wheat, corn, barley;témoins prairie temporaire;ley witnesses";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        this.instance = new RecorderImpl();
        this.initInstance(this.instance);
        when(modaliteDAO.getByNKey(anyString(), anyString())).thenReturn(Optional.empty());
        this.instance.processRecord(parser, null, this.encoding);
        Assert.assertFalse(((RecorderImpl) this.instance).errorsReport.hasErrors());
    }

    /**
     * Test of retrieveDBVariable method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRetrieveDBVariable() throws Exception {
        // nominal
        Mockito.when(this.variableDeDescriptionDAO.getByCode("rotation")).thenReturn(
                Optional.of(this.variableDescription));
        ErrorsReport errorsReport = Mockito.mock(ErrorsReport.class);
        VariableDescription result = this.instance.retrieveDBVariable(errorsReport, "rotation");
        Assert.assertEquals(this.variableDescription, result);
        ArgumentCaptor<String> message = ArgumentCaptor.forClass(String.class);
        Mockito.verify(errorsReport, Mockito.never()).addErrorMessage(message.capture());
        // no variable
        Mockito.doReturn(Optional.empty()).when(this.variableDeDescriptionDAO).getByCode("rotation");
        result = this.instance.retrieveDBVariable(errorsReport, "rotation");
        Mockito.verify(errorsReport).addErrorMessage(message.capture());
        Assert.assertNull(result);
        Assert.assertEquals(
                "la variable de forçage portant le code \"rotation\" n'existe pas en base de données",
                message.getValue());
    }

    /**
     * Test of setListeItineraireDAO method, of class Recorder.
     */
    @Test
    public void testSetListeItineraireDAO() {
        Assert.assertEquals(this.listeItineraireDAO, this.instance.listeItineraireDAO);
    }

    /**
     * Test of setModaliteDAO method, of class Recorder.
     */
    @Test
    public void testSetModaliteDAO() {
        Assert.assertEquals(this.modaliteDAO, this.instance.modaliteDAO);
    }

    /**
     * Test of setVariableDeDescriptionDAO method, of class Recorder.
     */
    @Test
    public void testSetVariableDeDescriptionDAO() {
        Assert.assertEquals(this.variableDeDescriptionDAO, this.instance.variableDeDescriptionDAO);
    }

    class RecorderImpl extends Recorder {

        ErrorsReport errorsReport;

        RecorderImpl() {
            entityClass = Modalite.class;
            hasGenericNodeable = false;
        }

        @Override
        void persistModalite(ErrorsReport errorsReport, String codeVariable, String codeModalite,
                             String nom, String description) throws PersistenceException {
            this.errorsReport = errorsReport;
            Assert.assertEquals("rotation", codeVariable);
            Assert.assertEquals("R1", codeModalite);
            Assert.assertEquals("PT>20, blé, maïs, orge", nom);
            Assert.assertEquals("témoins prairie temporaire", description);
        }
    }
}
