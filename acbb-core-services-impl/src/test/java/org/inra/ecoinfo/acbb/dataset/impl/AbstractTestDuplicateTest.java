/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.impl;


import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;

/**
 * @author ptcherniati
 */
public class AbstractTestDuplicateTest {

    AbstractTestDuplicate instance = new AbstractTestDuplicateImpl();

    /**
     *
     */
    public AbstractTestDuplicateTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance.setErrorsReport(new ErrorsReport());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addErrors method, of class AbstractTestDuplicate.
     */
    @Test
    public void testAddErrors() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("msg");
        Assert.assertFalse(badsFormatsReport.hasErrors());
        this.instance.addErrors(badsFormatsReport);
        Assert.assertTrue(badsFormatsReport.hasErrors());
    }

    /**
     * Test of getKey method, of class AbstractTestDuplicate.
     */
    @Test
    public void testGetKey() {
        String[] args = new String[]{"1", "2", "3"};
        AbstractTestDuplicate instance = new AbstractTestDuplicateImpl();
        String result = instance.getKey(args);
        Assert.assertEquals("1_2_3", result);
    }

    /**
     * Test of getLocalValue method, of class AbstractTestDuplicate.
     */
    @Test
    public void testGetLocalValue() {
        String dateString = "25-12-1965";
        String timeString = "01:25";
        AbstractTestDuplicate instance = new AbstractTestDuplicateImpl();
        String expResult = "25-12-196501:25:00";
        String result = instance.getLocalValue(dateString, timeString);
        Assert.assertEquals(expResult, result);
        timeString = "01:25:12";
        expResult = "25-12-196501:25:00";
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of hasError method, of class AbstractTestDuplicate.
     */
    @Test
    public void testHasError() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("msg");
        Assert.assertFalse(badsFormatsReport.hasErrors());
        this.instance.addErrors(badsFormatsReport);
        Assert.assertFalse(this.instance.hasError());
        this.instance.errorsReport.addErrorMessage("error");
        Assert.assertTrue(this.instance.hasError());
    }

    /**
     * Test of isLimitDate method, of class AbstractTestDuplicate.
     */
    @Test
    public void testIsLimitDate() {
        String datedbString = "25/12/1965";
        String timedbString = "01:25";
        String dateString = "25/12/1965";
        String timeString = "01:25:00";
        AbstractTestDuplicate instance = new AbstractTestDuplicateImpl();
        boolean result = instance.isLimitDate(datedbString, timedbString, dateString, timeString);
        Assert.assertTrue(result);
        result = instance.isLimitDate(datedbString, timedbString, dateString, "01:25:14");
        Assert.assertFalse(result);
    }

    /**
     * Test of testLineForDuplicatesDateInDB method, of class AbstractTestDuplicate.
     */
    @Test
    public void testTestLineForDuplicatesDateInDB() {
        String[] dates = null;
        String dateString = org.apache.commons.lang.StringUtils.EMPTY;
        String timeString = org.apache.commons.lang.StringUtils.EMPTY;
        long lineNumber = 0L;
        VersionFile versionFile = null;
        AbstractTestDuplicate instance = new AbstractTestDuplicateImpl();
        instance.testLineForDuplicatesDateInDB(dates, dateString, timeString, lineNumber,
                versionFile);
    }

    /**
     *
     */
    public class AbstractTestDuplicateImpl extends AbstractTestDuplicate {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        String[] values;
        long lineNumber;
        String[] dates;
        VersionFile versionFile;

        /**
         * @param values
         * @param lineNumber
         * @param dates
         * @param versionFile
         */
        @Override
        public void addLine(String[] values, long lineNumber, String[] dates,
                            VersionFile versionFile) {
            this.values = values;
            this.lineNumber = lineNumber;
            this.dates = dates;
            this.versionFile = versionFile;
        }
    }

}
