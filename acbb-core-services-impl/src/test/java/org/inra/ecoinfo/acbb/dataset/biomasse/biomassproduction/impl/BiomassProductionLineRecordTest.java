/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.RequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati
 */
public class BiomassProductionLineRecordTest {

    MockUtils m = MockUtils.getInstance();
    @Mock
    RequestPropertiesBiomasse requestPropertiesBiomasse;
    @Mock
    Column columnParcelle;
    @Mock
    Column columnObservation;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    Map<Integer, Column> valueColumns;
    @Mock
    List<Column> columns;
    @Mock
    MasseVegClass masseVegClass;
    BadsFormatsReport badsFormatsReport = new BadsFormatsReport("error");
    String[] values = new String[]{"parcelle", "observation",
            "Mav_valeur", "Mav_nb", "Mav_et", "Mav_methode", "Mav_IQ", "N_valeur", "N_nb", "N_et",
            "N_methode", "N_IQ"};
    List<Column> columnToParse;
    BiomassProductionLineRecord instance;
    /**
     *
     */
    public BiomassProductionLineRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new BiomassProductionLineRecord(0L);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDateDebutRepousse method, of class BiomassProductionLineRecord.
     */
    @Test
    public void testGetDateDebutRepousse() {
        this.instance.setDateDebutRepousse(this.m.dateDebut);
        LocalDate result = this.instance.getDateDebutRepousse();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getMasseVegClass method, of class BiomassProductionLineRecord.
     */
    @Test
    public void testGetMasseVegClass() {
        this.instance.setMasseVegClass(this.masseVegClass);
        MasseVegClass result = this.instance.getMasseVegClass();
        Assert.assertEquals(this.masseVegClass, result);
    }

    /**
     * Test of getNatureCouvert method, of class BiomassProductionLineRecord.
     */
    @Test
    public void testGetNatureCouvert() {
        this.instance.setNatureCouvert("natureCouvert");
        String result = this.instance.getNatureCouvert();
        Assert.assertEquals("natureCouvert", result);
    }

    /**
     * Test of getVariableValues method, of class BiomassProductionLineRecord.
     */
    @Test
    public void testGetVariableValues() {
        List<VariableValueBiomassProduction> expResult = new LinkedList();
        this.instance.setVariablesValues(expResult);
        List<VariableValueBiomassProduction> result = this.instance.getVariablesValues();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of updateBiomassProductionLineRecord method, of class BiomassProductionLineRecord.
     */
    @Test
    public void testUpdateBiomassProductionLineRecord() {
        this.instance.updateBiomassProductionLineRecord(this.masseVegClass, this.m.dateDebut);
        Assert.assertEquals(this.masseVegClass, this.instance.getMasseVegClass());
        Assert.assertEquals(this.m.dateDebut, this.instance.getDateDebutRepousse());
    }

}
