/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

/**
 * @author ptcherniati
 */
public class AbstractBuildHelperTest {


    MockUtils m = MockUtils.getInstance();
    AbstractBuildHelperImpl instance;
    @Mock
    RequestPropertiesBiomasse requestPropertiesBiomasse;
    @Mock
    private List<AbstractLineRecord> lines;
    /**
     *
     */
    public AbstractBuildHelperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractBuildHelperImpl();
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.instance.setDatatypeName(MockUtils.DATATYPE);
        this.instance.requestPropertiesBiomasse = this.requestPropertiesBiomasse;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getRequestPropertiesBiomasse method, of class AbstractBuildHelper.
     */
    @Test
    public void testGetRequestPropertiesBiomasse() {
        IRequestPropertiesBiomasse expResult = this.requestPropertiesBiomasse;
        IRequestPropertiesBiomasse result = this.instance.getRequestPropertiesBiomasse();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setDatatypeName method, of class AbstractBuildHelper.
     */
    @Test
    public void testSetDatatypeName() {
        Assert.assertEquals(MockUtils.DATATYPE, this.instance.datatypeName);
    }

    /**
     * Test of setDatatypeVariableUniteACBBDAO method, of class AbstractBuildHelper.
     */
    @Test
    public void testSetDatatypeVariableUniteACBBDAO() {
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        Assert.assertEquals(this.m.datatypeVariableUniteDAO,
                this.instance.datatypeVariableUniteACBBDAO);
    }

    /**
     * Test of update method, of class AbstractBuildHelper.
     */
    @Test
    public void testUpdate() {
        ErrorsReport errorsReport = new ErrorsReport();
        this.instance.update(this.m.versionFile, this.lines, errorsReport,
                this.requestPropertiesBiomasse);
        Mockito.verify(this.m.datatypeVariableUniteDAO)
                .getAllVariableTypeDonneesByDataTypeMapByVariableCode(MockUtils.DATATYPE);
        Assert.assertEquals(this.m.versionFile, this.instance.version);
        Assert.assertEquals(this.lines, this.instance.lines);
        Assert.assertEquals(errorsReport, this.instance.errorsReport);
        Assert.assertEquals(this.requestPropertiesBiomasse, this.instance.requestPropertiesBiomasse);
    }

    /**
     *
     */
    public class AbstractBuildHelperImpl extends AbstractBuildHelper {

        /**
         * @throws PersistenceException
         */
        @Override
        public void build() throws PersistenceException {
        }
    }

}
