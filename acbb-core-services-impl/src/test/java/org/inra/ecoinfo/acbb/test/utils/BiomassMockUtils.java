/*
 *
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.test.utils;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.RequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.IModaliteDAO;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.DateTimeException;

/**
 * @author ptcherniati
 */
public class BiomassMockUtils extends MockUtils {


    /**
     *
     */
    @Mock
    public static Modalite modalite;

    /**
     *
     */
    @Mock
    public static IModaliteDAO modaliteDAO;

    /**
     *
     */
    @Mock
    public static RequestPropertiesBiomasse requestPropertiesBiomasse;

    /**
     *
     */
    @Mock
    public static IInterventionDAO interventionDAO;

    /**
     *
     */
    @Mock
    public static IListeVegetationDAO listeVegetationDAO;

    /**
     *
     */
    @Mock
    public static IListeItineraireDAO listeItineraireDAO;

    /**
     *
     */
    @Mock
    public static DatasetDescriptorACBB datasetDescriptorACBB;

    /**
     *
     */
    @Mock
    public static AbstractIntervention intervention;

    /**
     *
     */
    @Mock
    public static CSVParser parser;

    /**
     *
     */
    public BiomassMockUtils() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * @return
     */
    public static BiomassMockUtils getInstance() {

        BiomassMockUtils instance = new BiomassMockUtils();

        MockitoAnnotations.initMocks(instance);

        try {

            instance.initMocks();

        } catch (DateTimeException | BadExpectedValueException ex) {

            return null;

        }

        instance.initLocalization();

        return instance;
    }

}
