/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IMesureSemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.ISemisDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.IValeurSemisAttrDAO;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.MesureSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.Semis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemis;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemisAttr;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author ptcherniati
 */
public class SemisDeleteTest {

    SemisDelete instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    IVersionDeTraitementRealiseeDAO versionDeTraitementRealiseeDAO;
    @Mock
    IAnneeRealiseeDAO anneeRealiseeDAO;
    @Mock
    IPeriodeRealiseeDAO periodeRealiseeDAO;
    @Mock
    IMesureSemisDAO mesureSemisDAO;
    @Mock
    ILocalPublicationDAO publicationDAO;
    @Mock
    ISemisDAO semisDAO;
    @Mock
    ITempoDAO tempoDAO;
    @Mock
    Semis semis;
    @Mock
    Tempo tempo;
    @Mock
    AbstractIntervention intervention;
    @Mock
    IValeurSemisAttrDAO valeurSemisAttrDAO;
    @Mock
    MesureSemis mesure;
    @Mock
    ValeurSemis valeur;
    @Mock
    ValeurSemisAttr attribut;
    @Spy
    List<Semis> semisToDelete = new LinkedList();
    @Spy
    Set<AbstractIntervention> bindedITK = new TreeSet();
    @Spy
    List<MesureSemis> mesuresToDelete = new LinkedList();
    @Spy
    List<ValeurSemis> valeursToDelete = new LinkedList();
    @Spy
    List<ValeurSemisAttr> attributs = new LinkedList();
    /**
     *
     */
    public SemisDeleteTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public void fillLists() {
        this.semisToDelete.add(this.semis);
        this.mesuresToDelete.add(this.mesure);
        this.valeursToDelete.add(this.valeur);
        this.attributs.add(this.attribut);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new SemisDelete();
        this.instance.setAnneeRealiseeDAO(this.anneeRealiseeDAO);
        this.instance.setDatasetConfiguration(this.m.datasetConfiguration);
        this.instance.setMesureSemisDAO(this.mesureSemisDAO);
        this.instance.setPeriodeRealiseeDAO(this.periodeRealiseeDAO);
        this.instance.setPublicationDAO(this.publicationDAO);
        this.instance.setSemisDAO(this.semisDAO);
        this.instance.setTempoDAO(this.tempoDAO);
        this.instance.setValeurSemisAttrDAO(this.valeurSemisAttrDAO);
        this.instance.setVersionDeTraitementRealiseeDAO(this.versionDeTraitementRealiseeDAO);
        this.instance.setVersionFileDAO(this.m.versionFileDAO);
        this.fillLists();
        Mockito.when(this.semis.getTempo()).thenReturn(this.tempo);
        Mockito.doReturn(this.mesuresToDelete).when(this.semis).getMesuresSemis();
        Mockito.doReturn(this.valeursToDelete).when(this.mesure).getvaleursSemis();
        Mockito.doReturn(this.attributs).when(this.semis).getAttributs();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class SemisDelete.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        ACBBUtils.localizationManager = MockUtils.localizationManager;
        Mockito.when(this.tempo.getBindedsITK()).thenReturn(this.bindedITK);
        // case semisToDelete.containsAll(bindedITK
        this.bindedITK.add(this.semis);
        Mockito.when(this.semisDAO.getInterventionForVersion(this.m.versionFile)).thenReturn(
                this.semisToDelete);
        this.instance.deleteRecord(this.m.versionFile);
        Mockito.verify(this.valeursToDelete).clear();
        Mockito.verify(this.mesureSemisDAO).saveOrUpdate(this.mesure);
        Mockito.verify(this.mesuresToDelete).clear();
        Mockito.verify(this.semisDAO).saveOrUpdate(this.semis);
        Mockito.verify(this.tempoDAO).remove(this.tempo);
        Assert.assertTrue(this.attributs.isEmpty());
        Mockito.verify(this.valeurSemisAttrDAO).remove(this.attribut);
        Mockito.verify(this.tempoDAO).deleteUnusedTempos();
        Mockito.verify(this.publicationDAO).removeVersion(this.m.versionFile);
        // case semisToDelete.not containsAll(bindedITK
        this.bindedITK.clear();
        this.bindedITK.add(m.intervention1);
        try {
            this.instance.deleteRecord(this.m.versionFile);
            Assert.fail("NOEXCEPTION");
        } catch (BusinessException e) {
            Assert.assertEquals(
                    "Il n'est pas possible de supprimer la version 1 du fichier agro-écosysteme_site_datatype_01-01-2012_31-12-2012.csv ; \n"
                            + "d'autres interventions utilisent les informations de ce fichier : \n"
                            + "version 1 du fichier file 1 à la date 01/01/2012\n"
                            + "\n"
                            + "Vous devez d'abord dépubiler ces versions avant de supprimer ce fichier.",
                    e.getMessage());
        }
        Mockito.verify(this.semis).setTempo(null);
        Mockito.verify(this.publicationDAO).removeVersion(this.m.versionFile);
        Mockito.verify(this.tempoDAO).deleteUnusedTempos();
    }

}
