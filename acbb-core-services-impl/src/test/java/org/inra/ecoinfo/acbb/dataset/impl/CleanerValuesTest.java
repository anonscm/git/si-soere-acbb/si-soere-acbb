/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import org.junit.*;

/**
 * @author ptcherniati
 */
public class CleanerValuesTest {

    /**
     *
     */
    public CleanerValuesTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of currentToken method, of class CleanerValues.
     */
    @Test
    public void testCurrentToken() throws EndOfCSVLine {
        CleanerValues instance = new CleanerValues(new String[]{"1", "2", "3"});
        Assert.assertEquals("1", instance.currentToken());
        Assert.assertEquals("1", instance.nextToken());
        Assert.assertEquals("2", instance.currentToken());
        Assert.assertEquals("2", instance.nextToken());
        Assert.assertEquals("3", instance.currentToken());
        Assert.assertEquals("3", instance.nextToken());
    }

    /**
     * Test of currentTokenIndex method, of class CleanerValues.
     */
    @Test
    public void testCurrentTokenIndex() throws EndOfCSVLine {
        CleanerValues instance = new CleanerValues(new String[]{"1", "2", "3"});
        Assert.assertTrue(0 == instance.currentTokenIndex());
        Assert.assertEquals("1", instance.nextToken());
        Assert.assertTrue(1 == instance.currentTokenIndex());
        Assert.assertEquals("2", instance.nextToken());
        Assert.assertTrue(2 == instance.currentTokenIndex());
        Assert.assertEquals("3", instance.nextToken());
        Assert.assertTrue(3 == instance.currentTokenIndex());
    }

    /**
     * Test of nextToken method, of class CleanerValues.
     */
    @Test
    public void testNextToken() throws EndOfCSVLine {
        CleanerValues instance = new CleanerValues(new String[]{"1", "2", "3"});
        Assert.assertEquals("1", instance.nextToken());
        Assert.assertEquals("2", instance.nextToken());
        Assert.assertEquals("3", instance.nextToken());
    }

}
