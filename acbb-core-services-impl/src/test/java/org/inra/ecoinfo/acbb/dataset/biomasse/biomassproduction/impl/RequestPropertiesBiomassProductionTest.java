/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.biomasse.impl.RequestPropertiesBiomasse;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.junit.*;
import org.mockito.Mock;

/**
 * @author ptcherniati
 */
public class RequestPropertiesBiomassProductionTest {

    MockUtils m = MockUtils.getInstance();
    @Mock
    RequestPropertiesBiomasse requestPropertiesBiomasse;
    RequestPropertiesBiomassProduction instance;
    /**
     *
     */
    public RequestPropertiesBiomassProductionTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new RequestPropertiesBiomassProduction();
        this.instance.setDatasetConfiguration(this.m.datasetConfiguration);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getNomDeFichier method, of class RequestPropertiesBiomassProduction.
     */
    @Test
    public void testGetNomDeFichier() {
        String result = this.instance.getNomDeFichier(this.m.versionFile);
        Assert.assertEquals(this.m.dataset.buildDownloadFilename(this.m.datasetConfiguration),
                result);
    }

    /**
     * Test of getTestDuplicates method, of class RequestPropertiesBiomassProduction.
     */
    @Test
    public void testGetTestDuplicates() {
        ITestDuplicates result = this.instance.getTestDuplicates();
        Assert.assertNotNull(result);
        Assert.assertTrue(result instanceof TestDuplicateBiomassProduction);
    }

}
