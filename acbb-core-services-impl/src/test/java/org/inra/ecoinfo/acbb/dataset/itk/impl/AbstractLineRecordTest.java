/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author ptcherniati
 */
public class AbstractLineRecordTest {


    private final Integer anneeNumber = 2;
    private final String observation = "observation";
    private final long originalLineNumber = 12L;
    private final Integer periodeNumber = 3;
    private final Integer rotationNumber = 4;
    MockUtils m = MockUtils.getInstance();
    AbstractLineRecord instance;
    private LocalDate date;
    @Mock
    private ACBBVariableValue<ListeItineraire> vv1;
    @Mock
    private ACBBVariableValue<ListeItineraire> vv2;
    @Mock
    private List variablesValues;
    /**
     *
     */
    public AbstractLineRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractLineRecordImpl(0);
        date = m.dateDebut;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class AbstractLineRecord.
     */
    @Test
    public void testCompareTo() {
        AbstractLineRecord obj = new AbstractLineRecordImpl(0L);
        int result = this.instance.compareTo(obj);
        Assert.assertEquals(0, result);
        obj = new AbstractLineRecordImpl(11L);
        result = this.instance.compareTo(obj);
        Assert.assertEquals(1, result);
        result = obj.compareTo(this.instance);
        Assert.assertEquals(-1, result);
    }

    /**
     * Test of copy method, of class AbstractLineRecord.
     */
    @Test
    public void testCopy() {
        this.instance.setAnneeNumber(this.anneeNumber);
        this.instance.setDate(this.date);
        this.instance.setObservation(this.observation);
        this.instance.setOriginalLineNumber(this.originalLineNumber);
        this.instance.setParcelle(this.m.parcelle);
        this.instance.setPeriodeNumber(this.periodeNumber);
        this.instance.setRotationNumber(this.rotationNumber);
        this.instance.setSuiviParcelle(this.m.suiviParcelle);
        this.instance.setTraitementProgramme(this.m.traitement);
        this.instance.setVariablesValues(this.variablesValues);
        this.instance.setVersionDeTraitement(this.m.versionDeTraitement);
        AbstractLineRecord copy = new AbstractLineRecordImpl(2L);
        Assert.assertTrue(2L == copy.getOriginalLineNumber());
        copy.copy(this.instance);
        Assert.assertTrue(this.anneeNumber == copy.getAnneeNumber());
        Assert.assertEquals(this.date, copy.getDate());
        Assert.assertEquals(this.observation, copy.getObservation());
        Assert.assertTrue(this.originalLineNumber == copy.getOriginalLineNumber());
        Assert.assertEquals(this.m.parcelle, copy.getParcelle());
        Assert.assertTrue(this.periodeNumber == copy.getPeriodeNumber());
        Assert.assertEquals(this.m.traitement, copy.getTraitementProgramme());
        Assert.assertEquals(this.variablesValues, copy.getVariablesValues());
        Assert.assertEquals(this.m.versionDeTraitement, copy.getVersionDeTraitement());
        Assert.assertEquals(this.m.versionDeTraitement, copy.getVersionDeTraitement());
    }

    /**
     * Test of equals method, of class AbstractLineRecord.
     */
    @Test
    public void testEquals() {
        Object obj = new AbstractLineRecordImpl(0L);
        boolean result = this.instance.equals(obj);
        Assert.assertTrue(result);
        obj = new AbstractLineRecordImpl(2L);
        result = this.instance.equals(obj);
        Assert.assertFalse(result);
        result = this.instance.equals("s");
        Assert.assertFalse(result);
    }

    /**
     * Test of getAnneeNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetAnneeNumber() {
        Integer expResult = 32;
        this.instance.setAnneeNumber(expResult);
        Integer result = this.instance.getAnneeNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class AbstractLineRecord.
     */
    @Test
    public void testGetDate() {
        this.instance.setDate(this.m.dateDebut);
        LocalDate result = this.instance.getDate();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getDateDebutDetraitementSurParcelle method, of class AbstractLineRecord.
     */
    @Test
    public void testGetDateDebutDetraitementSurParcelle() {
        this.instance.setDateDebutDetraitementSurParcelle(this.m.dateDebut);
        instance.setDateDebutDetraitementSurParcelle(m.dateDebut);
        LocalDate result = this.instance.getDateDebutDetraitementSurParcelle();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getObservation method, of class AbstractLineRecord.
     */
    @Test
    public void testGetObservation() {
        String expResult = "observation";
        this.instance.setObservation(expResult);
        String result = this.instance.getObservation();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getOriginalLineNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetOriginalLineNumber() {
        long expResult = 133L;
        this.instance.setOriginalLineNumber(expResult);
        long result = this.instance.getOriginalLineNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getParcelle method, of class AbstractLineRecord.
     */
    @Test
    public void testGetParcelle() {
        this.instance.setParcelle(this.m.parcelle);
        Parcelle result = this.instance.getParcelle();
        Assert.assertEquals(this.m.parcelle, result);
    }

    /**
     * Test of getPeriodeNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetPeriodeNumber() {
        Integer expResult = 22;
        this.instance.setPeriodeNumber(expResult);
        Integer result = this.instance.getPeriodeNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getRotationNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetRotationNumber() {
        Integer expResult = 24;
        this.instance.setRotationNumber(expResult);
        Integer result = this.instance.getRotationNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractLineRecord.
     */
    @Test
    public void testGetSuiviParcelle() {
        this.instance.setSuiviParcelle(this.m.suiviParcelle);
        SuiviParcelle result = this.instance.getSuiviParcelle();
        Assert.assertEquals(this.m.suiviParcelle, result);
    }

    /**
     * Test of getTempo method, of class AbstractLineRecord.
     */
    @Test
    public void testGetTempo() {
        Tempo tempo = new Tempo();
        this.instance.setTempo(tempo);
        Tempo result = this.instance.getTempo();
        Assert.assertEquals(tempo, result);
    }

    /**
     * Test of getTraitementProgramme method, of class AbstractLineRecord.
     */
    @Test
    public void testGetTraitementProgramme() {
        this.instance.setTraitementProgramme(this.m.traitement);
        TraitementProgramme result = this.instance.getTraitementProgramme();
        Assert.assertEquals(this.m.traitement, result);
    }

    /**
     * Test of getVariablesValues method, of class AbstractLineRecord.
     */
    @Test
    public void testGetVariablesValues() {
        List<ACBBVariableValue> expResult = Arrays.asList(new ACBBVariableValue[]{this.vv1,
                this.vv2});
        this.instance.setVariablesValues(expResult);
        List<ACBBVariableValue> result = this.instance.getVariablesValues();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVersion method, of class AbstractLineRecord.
     */
    @Test
    public void testGetVersion() {
        this.instance.setVersion(1);
        Assert.assertEquals(1, this.instance.getVersion());
    }

    /**
     * Test of getVersionDeTraitement method, of class AbstractLineRecord.
     */
    @Test
    public void testGetVersionDeTraitement() {
        this.instance.setVersionDeTraitement(this.m.versionDeTraitement);
        VersionDeTraitement result = this.instance.getVersionDeTraitement();
        Assert.assertEquals(this.m.versionDeTraitement, result);
    }

    /**
     *
     */
    public class AbstractLineRecordImpl extends AbstractLineRecord {

        /**
         * @param lineNumber
         */
        public AbstractLineRecordImpl(long lineNumber) {
            super(lineNumber);
        }
    }

}
