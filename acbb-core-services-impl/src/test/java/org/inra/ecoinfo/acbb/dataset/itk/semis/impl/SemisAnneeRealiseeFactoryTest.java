/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class SemisAnneeRealiseeFactoryTest {

    @Spy
    SemisAnneeRealiseeFactory instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    LineRecord line;
    @Mock
    SemisCouvertVegetal couvertVegetal;
    @Mock
    SemisCouvertVegetal expectedCouvertVegetal;
    @Mock
    RotationDescriptor rotationDescriptor;
    @Mock
    Tempo tempo;
    @Mock
    VersionTraitementRealisee versionTraitementRealisee;
    @Mock
    VersionTraitementRealisee otherVersionTraitementRealisee;
    @Mock
    AnneeRealisee anneeRealisee;
    @Mock
    AnneeRealisee otherAnneeRealisee;
    @Mock
    Rotation rotation;
    @Mock
    SemisPeriodeRealiseeFactory periodeRealiseeFactory;
    @Mock
    IAnneeRealiseeDAO anneeRealiseeDAO;
    @Mock
    IPolicyManager policyManager;
    Map<Integer, AnneeRealisee> anneesRealisees = new HashMap();
    Map<Integer, VersionTraitementRealisee> versionsDeTraitementRealisees = new HashMap();
    Map<Integer, SemisCouvertVegetal> couverts = new HashMap();
    /**
     *
     */
    public SemisAnneeRealiseeFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    public void initInstance(SemisAnneeRealiseeFactory instance) {
        instance.setAnneeRealiseeDAO(this.anneeRealiseeDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setPeriodeRealiseeFactory(this.periodeRealiseeFactory);
        instance.setPolicyManager(this.policyManager);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = Mockito.spy(new SemisAnneeRealiseeFactory());
        this.initInstance(this.instance);
        this.anneesRealisees.put(2, this.anneeRealisee);
        Mockito.when(this.couvertVegetal.getCode()).thenReturn("couvert");
        Mockito.when(this.expectedCouvertVegetal.getCode()).thenReturn("expected couvert");
        Mockito.when(this.line.getSemisRotationDescriptor()).thenReturn(this.rotationDescriptor);
        Mockito.when(this.line.getCouvertVegetal()).thenReturn(this.couvertVegetal);
        Mockito.when(this.line.getSemisRotationDescriptor()).thenReturn(this.rotationDescriptor);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(2);
        Mockito.when(this.line.getRotationNumber()).thenReturn(10);
        Mockito.when(this.line.getTreatmentVersion()).thenReturn(1);
        Mockito.when(this.line.getTreatmentAffichage()).thenReturn("T4");
        Mockito.when(this.rotationDescriptor.getExpectedCouvert()).thenReturn(
                this.expectedCouvertVegetal);
        Mockito.when(this.line.getOriginalLineNumber()).thenReturn(1L);
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.tempo.getVersionTraitementRealisee()).thenReturn(
                this.versionTraitementRealisee);
        Mockito.doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArgument(0, String.class);
            }
        }).when(this.instance).getInternationalizedValeurQualitative(any(String.class));
        when(this.line.getSemisRotationDescriptor()).thenReturn(this.rotationDescriptor);
        when(this.rotationDescriptor.getRotation()).thenReturn(this.rotation);
        when(this.rotationDescriptor.getCouverts()).thenReturn(this.couverts);
        this.couverts.put(1, this.couvertVegetal);
        this.couverts.put(2, this.couvertVegetal);
        this.couverts.put(3, this.couvertVegetal);
        when(anneeRealiseeDAO.getByNKey(any(), anyInt())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of alertNonExpectedCouvert method, of class
     * SemisAnneeRealiseeFactory.
     */
    @Test
    public void testAlertNonExpectedCouvert() {
        InfoReport infoReport = new InfoReport();
        this.instance.alertNonExpectedCouvert(this.line, infoReport);
        Assert.assertEquals(
                "-Ligne 1, le couvert vegetal couvert de l'année 2 de la rotation 10 de la version 1 du traitement T4 n'est pas conforme au couvert végétal attendu expected couvert.\n",
                infoReport.getInfoMessages());
    }

    /**
     * Test of getOrCreateAnneeRealisee method, of class
     * SemisAnneeRealiseeFactory.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetOrCreateAnneeRealisee() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        InfoReport infoReport = new InfoReport();
        // create new annee
        Mockito.when(this.line.isGoodYearForRotation()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isGoodYearForTemporaryGrassland()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isPositiveYear()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isRotation()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isAnnuelle()).thenReturn(Boolean.TRUE);
        Mockito.when(this.rotationDescriptor.isPossibleCouvert()).thenReturn(Boolean.TRUE);
        Mockito.doNothing().when(this.instance)
                .updateAnneeForRotation(this.line, this.anneeRealisee, errorsReport, infoReport);
        AnneeRealisee result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport,
                infoReport);
        Assert.assertNotNull(result);
        Mockito.verify(this.periodeRealiseeFactory).createDefaultPeriode(result, errorsReport);
        Mockito.verify(this.instance).updateAnneeForRotation(this.line, result, errorsReport,
                infoReport);
        Mockito.verify(this.anneeRealiseeDAO).saveOrUpdate(result);
        // not replanting missing main crop
        Mockito.when(this.line.isReplanting()).thenReturn(Boolean.TRUE);
        result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Assert.assertNull(result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, impossible de générer une culture secondaire pour l'année 2 pour la rotation 10 de la version 0 du traitement 1 : la culture principale n'est pas définie. Si elle est présente dans le même fichier, elle doit être déclarée avant.\n",
                errorsReport.getErrorsMessages());
        // existing YEAR
        Mockito.when(this.versionTraitementRealisee.getAnneesRealisees()).thenReturn(
                this.anneesRealisees);
        Mockito.when(this.line.isReplanting()).thenReturn(Boolean.FALSE);
        Set<AbstractIntervention> interventions = new TreeSet();
        Mockito.when(this.anneeRealisee.getBindedsITK()).thenReturn(interventions);
        interventions.add(m.intervention1);
        interventions.add(m.intervention2);
        errorsReport = new ErrorsReport();
        result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Assert.assertEquals(this.anneeRealisee, result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, l'année 2 pour la rotation 10 de la version 1 du traitement T4 existe déjà. Elle a pu être générée par le ou les fichier(s) suivant(s)  : \n"
                        + " \n"
                        + "version 1 du fichier file 1 à la date 01/01/2012\n"
                        + "version 2 du fichier file 2 à la date 31/12/2012\n"
                        + ".\n",
                errorsReport.getErrorsMessages());
        // annee invalide for infinite repeating grassland
        errorsReport = new ErrorsReport();
        Mockito.when(this.line.isGoodYearForTemporaryGrassland()).thenReturn(Boolean.FALSE);
        result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Assert.assertEquals(this.anneeRealisee, result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, l'année 2 est invalide pour la rotation 10 de la version 1 du traitement T4. Elle doit être supérieure à 1.\n",
                errorsReport.getErrorsMessages());
        // annee invalide for repeating grassland
        errorsReport = new ErrorsReport();
        Mockito.when(this.line.isGoodYearForRotation()).thenReturn(Boolean.FALSE);
        result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Assert.assertEquals(this.anneeRealisee, result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, l'année 2 est invalide pour la rotation 10 de la version 1 du traitement T4. Elle doit être comprise entre 1 et 3\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of getOrCreateAnneeRealisee method, of class
     * SemisAnneeRealiseeFactory.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetOrCreateAnneeRealiseeWithErrors() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        InfoReport infoReport = new InfoReport();
        // bad cover
        Mockito.when(this.line.isGoodYearForRotation()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isGoodYearForTemporaryGrassland()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isPositiveYear()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isRotation()).thenReturn(Boolean.TRUE);
        Mockito.when(this.line.isAnnuelle()).thenReturn(Boolean.TRUE);
        Mockito.when(this.rotationDescriptor.isPossibleCouvert()).thenReturn(Boolean.FALSE);
        Mockito.doNothing().when(this.instance)
                .updateAnneeForRotation(this.line, this.anneeRealisee, errorsReport, infoReport);
        Mockito.when(this.couvertVegetal.getValeur()).thenReturn("Grassland");
        Mockito.when(this.rotationDescriptor.getExpectedCouvertList())
                .thenReturn("blé, mais, orge");
        AnneeRealisee result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport,
                infoReport);
        Assert.assertNotNull(result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, le couvert végétal Grassland est inattendu pour l'année 2 pour la rotation 10 de la version 1 du traitement T4 : il doit être dans l'un des couverts blé, mais, orge.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of getPreviousRotation method, of class SemisAnneeRealiseeFactory.
     */
    @Test
    public void testGetPreviousRotation() {
        Mockito.when(this.versionTraitementRealisee.getVersionDeTraitement()).thenReturn(
                this.m.versionDeTraitement);
        Mockito.when(this.m.versionDeTraitement.getVersionsDeTraitementRealisees()).thenReturn(
                this.versionsDeTraitementRealisees);
        this.versionsDeTraitementRealisees.put(5, this.versionTraitementRealisee);
        this.versionsDeTraitementRealisees.put(4, this.otherVersionTraitementRealisee);
        Mockito.when(this.versionTraitementRealisee.getNumero()).thenReturn(5);
        VersionTraitementRealisee result = this.instance
                .getPreviousRotation(this.versionTraitementRealisee);
        Assert.assertEquals(this.otherVersionTraitementRealisee, result);
    }

    /**
     * Test of getPreviousYear method, of class SemisAnneeRealiseeFactory.
     */
    @Test
    public void testGetPreviousYear() {
        Mockito.when(this.anneeRealisee.getAnnee()).thenReturn(2);
        Mockito.when(this.anneeRealisee.getVersionTraitementRealise()).thenReturn(
                this.versionTraitementRealisee);
        Mockito.when(this.anneeRealiseeDAO.getByNKey(this.versionTraitementRealisee, 1))
                .thenReturn(Optional.of(this.anneeRealisee));
        AnneeRealisee result = this.instance.getPreviousYear(this.anneeRealisee).orElse(null);
        Assert.assertEquals(this.anneeRealisee, result);
        // first year
        Mockito.when(this.anneeRealisee.getAnnee()).thenReturn(0);
        Mockito.doReturn(this.otherVersionTraitementRealisee).when(this.instance)
                .getPreviousRotation(this.versionTraitementRealisee);
        Mockito.when(this.otherVersionTraitementRealisee.getAnneesRealisees()).thenReturn(
                this.anneesRealisees);
        Mockito.when(this.versionTraitementRealisee.getNumero()).thenReturn(2);
        this.anneesRealisees.put(1, this.otherAnneeRealisee);
        Assert.assertTrue(2 == this.anneesRealisees.size());
        result = this.instance.getPreviousYear(this.anneeRealisee).orElse(null);
        Assert.assertEquals(this.anneeRealisee, result);
        // no previous year
        Mockito.when(this.versionTraitementRealisee.getNumero()).thenReturn(1);
        result = this.instance.getPreviousYear(this.anneeRealisee).orElse(null);
        Assert.assertNull(result);
    }

    /**
     * Test of setBeginAndEndDateForTemporalMeadows method, of class
     * SemisAnneeRealiseeFactory.
     */
    @Test
    public void testSetBeginAndEndDateForTemporalMeadows() {
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doReturn(Optional.of(this.otherAnneeRealisee)).when(this.instance).getPreviousYear(this.anneeRealisee);
        Mockito.doReturn(this.couvertVegetal).when(this.otherAnneeRealisee).getCouvertVegetal();
        Mockito.when(this.otherAnneeRealisee.getDateDeFin()).thenReturn(this.m.dateFin);
        ArgumentCaptor<LocalDate> dateDebut = ArgumentCaptor.forClass(LocalDate.class);
        ArgumentCaptor<LocalDate> dateFin = ArgumentCaptor.forClass(LocalDate.class);
        this.instance.setBeginAndEndDateForTemporalMeadows(this.line, this.anneeRealisee,
                errorsReport);
        Mockito.verify(this.anneeRealisee).setDateDeDebut(dateDebut.capture());
        Mockito.verify(this.anneeRealisee).setDateDeFin(dateFin.capture());
        Assert.assertEquals("2013-01-01", dateDebut.getValue().toString());
        Assert.assertEquals("2014-01-01", dateFin.getValue().toString());
        // null previous year
        Mockito.doReturn(null).when(this.instance).getPreviousYear(this.anneeRealisee);
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateDebut);
        Mockito.doNothing().when(this.instance)
                .setLastYearDayDateForMeadows(this.anneeRealisee, this.m.dateDebut);
        doReturn(Optional.empty()).when(instance).getPreviousYear(any());
        this.instance.setBeginAndEndDateForTemporalMeadows(this.line, this.anneeRealisee,
                errorsReport);
        Mockito.verify(this.anneeRealisee).setDateDeDebut(this.m.dateDebut);
    }

    /**
     * Test of setBeginDate method, of class SemisAnneeRealiseeFactory.
     */
    @Test
    public void testSetBeginDate() {
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doNothing().when(this.instance)
                .setBeginAndEndDateForTemporalMeadows(this.line, this.anneeRealisee, errorsReport);
        Mockito.doNothing().when(this.instance)
                .updatePreviousYear(this.line, this.anneeRealisee, errorsReport);
        this.instance.setBeginDate(this.line, this.anneeRealisee, errorsReport);
        Mockito.verify(this.instance).setBeginAndEndDateForTemporalMeadows(this.line,
                this.anneeRealisee, errorsReport);
        Mockito.verify(this.instance).updatePreviousYear(this.line, this.anneeRealisee,
                errorsReport);
        // alternate case
        Mockito.when(this.line.getDate()).thenReturn(LocalDate.now());
        Mockito.when(this.line.isAnnuelle()).thenReturn(Boolean.TRUE);
        this.instance.setBeginDate(this.line, this.anneeRealisee, errorsReport);
        Mockito.verify(this.instance).setBeginAndEndDateForTemporalMeadows(this.line,
                this.anneeRealisee, errorsReport);
        Mockito.verify(this.instance, Mockito.times(2)).updatePreviousYear(this.line,
                this.anneeRealisee, errorsReport);
        Mockito.verify(this.anneeRealisee).setDateDeDebut(LocalDate.now());
    }

    /**
     * Test of setLastYearDayDateForMeadows method, of class
     * SemisAnneeRealiseeFactory.
     */
    @Test
    public void testSetLastYearDayDateForMeadows() {
        ArgumentCaptor<LocalDate> dateFin = ArgumentCaptor.forClass(LocalDate.class);
        this.instance.setLastYearDayDateForMeadows(this.anneeRealisee, this.m.dateDebut);
        Mockito.verify(this.anneeRealisee).setDateDeFin(dateFin.capture());
        Assert.assertEquals("2012-12-31", dateFin.getValue().toString());
    }

    /**
     * Test of updateAnneeForRotation method, of class
     * SemisAnneeRealiseeFactory.
     */
    @Test
    public void testUpdateAnneeForRotation() {
        this.instance = Mockito.spy(new SemisAnneeRealiseeFactoryImpl());
        this.initInstance(this.instance);
        ErrorsReport errorsReport = new ErrorsReport();
        InfoReport infoReport = new InfoReport();
        this.instance.updateAnneeForRotation(this.line, this.anneeRealisee, errorsReport,
                infoReport);
        Mockito.verify(this.instance).setBeginDate(this.line, this.anneeRealisee, errorsReport);
        Mockito.verify(this.anneeRealisee).setCouvertVegetal(this.couvertVegetal);
    }

    /**
     * Test of updateAnneeRealisee method, of class SemisAnneeRealiseeFactory.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateAnneeRealisee_3args() throws Exception {
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doNothing().when(this.instance)
                .setBeginDate(this.line, this.anneeRealisee, errorsReport);
        Mockito.doNothing().when(this.instance).updateAnneeRealisee(this.anneeRealisee);
        this.instance.updateAnneeRealisee(this.anneeRealisee, this.line, errorsReport);
        Mockito.verify(this.instance).setBeginDate(this.line, this.anneeRealisee, errorsReport);
        Mockito.verify(this.instance).updateAnneeRealisee(this.anneeRealisee);
        Mockito.verify(this.anneeRealisee).setCouvertVegetal(this.couvertVegetal);
    }

    /**
     * Test of updateAnneeRealisee method, of class SemisAnneeRealiseeFactory.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateAnneeRealisee_AnneeRealisee() throws Exception {
        this.instance.updateAnneeRealisee(this.anneeRealisee);
        Mockito.verify(this.anneeRealiseeDAO).saveOrUpdate(this.anneeRealisee);

    }

    /**
     * Test of updateEndDate method, of class SemisAnneeRealiseeFactory.
     */
    @Test
    public void testUpdateEndDate() {
        // null annee
        this.instance.updateEndDate(this.m.dateFin, null);
        Mockito.verify(this.anneeRealisee, Mockito.never()).setDateDeFin(this.m.dateFin);
        // with annee
        this.instance.updateEndDate(this.m.dateFin, this.anneeRealisee);
        Mockito.verify(this.anneeRealisee).setDateDeFin(this.m.dateFin);
    }

    /**
     * Test of updatePreviousYear method, of class SemisAnneeRealiseeFactory.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testUpdatePreviousYear() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doReturn(Optional.of(this.otherAnneeRealisee)).when(this.instance).getPreviousYear(this.anneeRealisee);
        Mockito.doNothing().when(this.instance).updateEndDate(null, this.otherAnneeRealisee);
        this.instance.updatePreviousYear(this.line, this.anneeRealisee, errorsReport);
        Mockito.verify(this.anneeRealiseeDAO).saveOrUpdate(this.otherAnneeRealisee);
        // with null year
        Mockito.doReturn(Optional.empty()).when(this.instance).getPreviousYear(this.anneeRealisee);
        this.instance.updatePreviousYear(this.line, this.anneeRealisee, errorsReport);
        Mockito.verify(this.anneeRealiseeDAO).saveOrUpdate(this.otherAnneeRealisee);
        // with persistenceException
        Mockito.doReturn(Optional.of(this.anneeRealisee)).when(this.instance).getPreviousYear(this.anneeRealisee);
        Mockito.doThrow(new PersistenceException("error")).when(this.anneeRealiseeDAO)
                .saveOrUpdate(this.anneeRealisee);
        this.instance.updatePreviousYear(this.line, this.anneeRealisee, errorsReport);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1, une erreur inconnue est survenue lors de la mise à jour de la date de fin de l'année 1  pour la rotation 2 de la version 10 du traitement 1.\n",
                errorsReport.getErrorsMessages());
    }

    class SemisAnneeRealiseeFactoryImpl extends SemisAnneeRealiseeFactory {

        @Override
        protected void setBeginDate(final LineRecord line, final AnneeRealisee anneeRealisee,
                                    final ErrorsReport errorsReport) {
        }

    }

}
