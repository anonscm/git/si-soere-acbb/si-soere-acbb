package org.inra.ecoinfo.acbb.dataset.itk.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IInterventionDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class AbstractProcessRecordITKTest {


    MockUtils m = ITKMockUtils.getInstance();
    AbstractProcessRecordITK instance;
    @Mock
    IInterventionDAO interventionDAO;
    @Mock
    IListeItineraireDAO listeItineraireDAO;
    @Mock
    AbstractLineRecord line;
    @Mock
    DatasetDescriptorACBB datasetDescriptorACBB;
    @Mock
    RequestPropertiesITK requestPropertiesITK;
    @Mock
    List<ACBBVariableValue> variableValues;
    @Mock
    Column column1;
    @Mock
    Column column2;
    @Mock
    Column columnNoFlag;
    @Mock
    VariableACBB variableACBB1;
    @Mock
    VariableACBB variableACBB2;
    @Mock
    VariableACBB variableACBB3;
    @Mock
    CSVParser parser;
    @Mock
    Map.Entry<Integer, Column> columnEntry;
    @Mock
    AbstractLineRecord lineRecord;
    Integer rotationNumber = 2;
    Integer anneeNumber = 3;
    Integer periodeNumber = 4;
    @Mock
    private ACBBVariableValue<ListeItineraire> vv1;
    @Mock
    private ACBBVariableValue<ListeItineraire> vv2;
    /**
     *
     */
    public AbstractProcessRecordITKTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private void initColumns() throws PersistenceException {
        Mockito.when(this.column1.getName()).thenReturn("colonne 1");
        Mockito.when(this.column1.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.column1.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.column2.getName()).thenReturn("colonne 2");
        Mockito.when(this.column2.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.column2.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.columnNoFlag.getName()).thenReturn("colonne no Flag");
        Mockito.when(this.m.variableDAO.getByAffichage("colonne 1")).thenReturn(Optional.of(this.variableACBB1));
        Mockito.when(this.m.variableDAO.getByAffichage("colonne 2")).thenReturn(Optional.of(this.variableACBB2));
        Mockito.when(this.m.variableDAO.getByAffichage("colonne no Flag")).thenReturn(
                Optional.of(this.variableACBB3));

    }

    /**
     * @param instance
     */
    protected void initInstance(AbstractProcessRecordITK instance) {
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setListeItineraireDAO(this.listeItineraireDAO);
        instance.setListeACBBDAO(this.listeItineraireDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setVersionFileDAO(this.m.versionFileDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
    }

    private void initLine() {
        Mockito.when(this.line.getAnneeNumber()).thenReturn(this.anneeNumber);
        Mockito.when(this.line.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.line.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.line.getDateDebutDetraitementSurParcelle()).thenReturn(this.m.dateFin);
        Mockito.when(this.line.getObservation()).thenReturn("observation");
        Mockito.when(this.line.getOriginalLineNumber()).thenReturn(12L);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(this.periodeNumber);
        Mockito.when(this.line.getRotationNumber()).thenReturn(this.rotationNumber);
        Mockito.when(this.line.getSuiviParcelle()).thenReturn(this.m.suiviParcelle);
        Mockito.when(this.line.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(this.line.getVariablesValues()).thenReturn(this.variableValues);
        Mockito.when(this.line.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
    }

    /**
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractProcessRecordITKImpl();
        this.initInstance(this.instance);
        this.variableValues = Arrays.asList(new ACBBVariableValue[]{this.vv1, this.vv2});
        Mockito.when(this.columnEntry.getValue()).thenReturn(this.column1);
        this.initLine();
        this.initColumns();
        when(interventionDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        when(listeItineraireDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getGenericColumns method, of class AbstractProcessRecordITK.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetGenericColumns() throws PersistenceException {
        this.instance = Mockito.spy(this.instance);
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doNothing()
                .when(this.instance)
                .updateVersionDeTraitement(this.lineRecord, errorsReport, 2L, 2,
                        this.datasetDescriptorACBB, this.requestPropertiesITK);
        CleanerValues cleanerValues = new CleanerValues(new String[]{"parcelle",
                MockUtils.DATE_DEBUT, "semis maïs", "1", "2", "3"});
        int expResult = 6;
        Mockito.when(this.requestPropertiesITK.getSite()).thenReturn(this.m.site);
        Mockito.when(this.m.parcelleDAO.getByNKey("site_parcelle")).thenReturn(Optional.of(this.m.parcelle));
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        int result = this.instance.getGenericColumns(errorsReport, 2L, cleanerValues,
                this.lineRecord, this.datasetDescriptorACBB, this.requestPropertiesITK, m.intervalDate);
        Mockito.verify(this.lineRecord).setParcelle(this.m.parcelle);
        Assert.assertEquals(this.m.dateDebut, this.lineRecord.date);
        Assert.assertEquals("semis maïs", this.lineRecord.observation);
        Assert.assertEquals(1, this.lineRecord.rotationNumber);
        Assert.assertEquals(2, this.lineRecord.anneeNumber);
        Assert.assertEquals(3, this.lineRecord.periodeNumber);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getListeItineraireDAO method, of class AbstractProcessRecordITK.
     */
    @Test
    public void testGetListeItineraireDAO() {
        Assert.assertEquals(this.listeItineraireDAO, this.instance.getListeItineraireDAO());
    }

    /**
     * Test of getObservation method, of class AbstractProcessRecordITK.
     */
    @Test
    public void testGetObservation() {
        CleanerValues cleanerValues = new CleanerValues(new String[]{"observation"});
        ErrorsReport errorsReport = new ErrorsReport();
        String result = this.instance.getObservation(errorsReport, 1l, 12, cleanerValues);
        Assert.assertEquals("observation", result);
    }

    /**
     * Test of getParcelle method, of class AbstractProcessRecordITK.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetParcelle() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"parcelle", org.apache.commons.lang.StringUtils.EMPTY, "badvalue"});
        // nominal
        Mockito.when(this.requestPropertiesITK.getSite()).thenReturn(this.m.site);
        Mockito.when(this.m.parcelleDAO.getByNKey("site_parcelle")).thenReturn(Optional.of(this.m.parcelle));
        Mockito.when(this.m.parcelleDAO.getByNKey("site_")).thenReturn(Optional.empty());
        Mockito.doReturn(Optional.empty()).when(this.m.parcelleDAO).getByNKey("site_badvalue");
        int result = this.instance.getParcelle(this.lineRecord, errorsReport, 2L, 1, cleanerValues,
                this.datasetDescriptorACBB, this.requestPropertiesITK);
        Assert.assertTrue(2 == result);
        Mockito.verify(this.lineRecord).setParcelle(this.m.parcelle);
        // nullParcelle
        result = this.instance.getParcelle(this.lineRecord, errorsReport, 2L, 1, cleanerValues,
                this.datasetDescriptorACBB, this.requestPropertiesITK);
        Assert.assertTrue(2 == result);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Aucune parcelle n'a été définie ligne 2 colonne 2.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitementNominal() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.of(this.m.suiviParcelle));
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.of(this.m.versionDeTraitement));
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesITK);
        Mockito.verify(this.lineRecord).setSuiviParcelle(this.m.suiviParcelle);
        Mockito.verify(this.lineRecord).setVersionDeTraitement(this.m.versionDeTraitement);
        Mockito.verify(this.lineRecord).setDateDebutDetraitementSurParcelle(this.m.dateDebut);
        Assert.assertFalse(errorsReport.hasErrors());
    }

    /**
     * Test of updateVersionDeTraitement method, of class AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitementNullSuiviParcelle() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        ILocalizationManager localizationManager = Mockito.mock(ILocalizationManager.class);
        Properties properties = Mockito.mock(Properties.class);
        this.instance.setLocalizationManager(localizationManager);
        Mockito.when(this.m.traitement.getNom()).thenReturn(MockUtils.TRAITEMENT);
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.empty());
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.empty());
        Mockito.when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(Parcelle.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        Mockito.when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        Mockito.when(properties.getProperty(m.parcelle.getName(), m.parcelle.getName()))
                .thenReturn(MockUtils.PARCELLE_NOM);
        Mockito.when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn(
                MockUtils.SITE_NOM);
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesITK);
        Mockito.verify(this.lineRecord, Mockito.never()).setSuiviParcelle(this.m.suiviParcelle);
        Assert.assertNull(this.lineRecord.traitementProgramme);
        Assert.assertNull(this.lineRecord.versionDeTraitement);
        Assert.assertNull(this.lineRecord.dateDebutDetraitementSurParcelle);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Il n'existe pas de suivi de parcelle pour la date 01/01/2012, sur la parcelle Parcelle du site Site;\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of updateVersionDeTraitement method, of class AbstractProcessRecordBiomasse.
     */
    @Test
    public void testUpdateVersionDeTraitementNullVersionTraitement() {
        ErrorsReport errorsReport = new ErrorsReport();
        // nominal
        ILocalizationManager localizationManager = Mockito.mock(ILocalizationManager.class);
        Properties properties = Mockito.mock(Properties.class);
        this.instance.setLocalizationManager(localizationManager);
        Mockito.when(this.m.traitement.getNom()).thenReturn(MockUtils.TRAITEMENT);
        Mockito.when(this.lineRecord.getDate()).thenReturn(this.m.dateDebut);
        Mockito.when(this.lineRecord.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.lineRecord.getTraitementProgramme()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.suiviParcelleDAO.retrieveSuiviParcelle(this.m.parcelle, this.m.dateDebut))
                .thenReturn(Optional.of(this.m.suiviParcelle));
        Mockito.when(this.m.suiviParcelle.getTraitement()).thenReturn(this.m.traitement);
        Mockito.when(
                this.m.versionDeTraitementDAO.retrieveVersiontraitement(this.m.traitement,
                        this.m.dateDebut)).thenReturn(Optional.empty());
        Mockito.when(
                localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA,
                        TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE)).thenReturn(properties);
        Mockito.when(
                localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME)).thenReturn(properties);
        Mockito.when(
                properties.getProperty(MockUtils.TRAITEMENT_AFFICHAGE,
                        MockUtils.TRAITEMENT_AFFICHAGE)).thenReturn(MockUtils.TRAITEMENT_AFFICHAGE);
        Mockito.when(properties.getProperty(MockUtils.SITE_NOM, MockUtils.SITE_NOM)).thenReturn(
                MockUtils.SITE_NOM);
        this.instance.updateVersionDeTraitement(this.lineRecord, errorsReport, 1L, 1,
                this.datasetDescriptorACBB, this.requestPropertiesITK);
        Mockito.verify(this.lineRecord).setSuiviParcelle(this.m.suiviParcelle);
        Assert.assertNull(this.lineRecord.versionDeTraitement);
        Mockito.verify(this.lineRecord, Mockito.never()).setVersionDeTraitement(
                this.m.versionDeTraitement);
        Assert.assertNull(this.lineRecord.dateDebutDetraitementSurParcelle);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Il n'existe pas de version de traitement pour la date 01/01/2012, pour le traitement traitement (Traitement) du site Site;\n",
                errorsReport.getErrorsMessages());
    }

    /**
     *
     */
    public class AbstractProcessRecordITKImpl extends AbstractProcessRecordITK {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        Stack<VariableACBB> variableACBBs = new Stack();

        /**
         *
         */
        public AbstractProcessRecordITKImpl() {
            this.variableACBBs.push(variableACBB2);
            this.variableACBBs.push(variableACBB1);
        }

        /**
         * @param version
         * @param lines
         * @param errorsReport
         * @param requestPropertiesITK
         * @throws PersistenceException
         */
        @Override
        protected void buildNewLines(VersionFile version, List<? extends AbstractLineRecord> lines,
                                     ErrorsReport errorsReport, IRequestPropertiesITK requestPropertiesITK)
                throws PersistenceException {
        }
    }

}
