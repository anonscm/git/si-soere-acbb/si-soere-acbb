/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IErrorsReport;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.StringReader;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

/**
 * @author ptcherniati
 */
public class GenericTestValuesTest {

    MockUtils m;
    @Spy
    GenericTestValues instance = new GenericTestValues();
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    IDatatypeVariableUniteACBBDAO datatypeVariableUniteACBBDAO;
    @Mock
    IRequestPropertiesACBB requestPropertiesACBB;
    @Mock
    Map<String, DatatypeVariableUniteACBB> VariableNodeable;
    @Mock
    ITestDuplicates testDuplicates;
    @Mock
    Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees;
    @Mock
    DatatypeVariableUniteACBB datatypeVariableUniteACBB;
    String datatypeName = "datatype";
    @Mock
    ILocalizationManager localizationManager;
    @Mock
    UserLocale userLocale;
    /**
     *
     */
    public GenericTestValuesTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.m = MockUtils.getInstance();
        this.instance.setDatatypeVariableUniteACBBDAO(this.datatypeVariableUniteACBBDAO);
        when(this.requestPropertiesACBB.getTestDuplicates())
                .thenReturn(this.testDuplicates);
        when(
                this.datatypeVariableUniteACBBDAO
                        .getAllVariableTypeDonneesByDataTypeMapByVariableAffichage(this.datatypeName))
                .thenReturn(this.VariableNodeable);
        when(this.variablesTypesDonnees.containsKey("colonne")).thenReturn(true);
        when(this.variablesTypesDonnees.get("colonne")).thenReturn(
                this.datatypeVariableUniteACBB);
        doReturn(userLocale).when(localizationManager).getUserLocale();
        doReturn(Locale.FRANCE).when(userLocale).getLocale();
        ACBBUtils.localizationManager = localizationManager;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of checkDateTypeValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckDateTypeValue() throws DateTimeException {
        String[] values = null;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Msg :");
        long lineNumber = 1L;
        int index = 2;
        String value = "25/12/2012";
        Column column = new Column();
        String format = "dd/MM/yyyy";
        column.setFormatType(format);
        column.setName("colonne");
        column.setInull(false);
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        IRequestPropertiesACBB requestPropertiesACBB = null;
        LocalDate expResult = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, value).toLocalDate();
        LocalDate result = this.instance.checkDateTypeValue(values, badsFormatsReport, lineNumber,
                index, value, column, variablesTypesDonnees, datasetDescriptor,
                requestPropertiesACBB);
        Assert.assertEquals(expResult, result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        value = "25-12-2012";
        result = this.instance.checkDateTypeValue(values, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertTrue(1 == badsFormatsReport.getErrorsMessages().size());
        Assert.assertEquals(
                "Msg : :- \"25-12-2012\" n'est pas un format de date valide à la ligne 1 colonne 3 (colonne). La date doit-être au format \"dd/MM/yyyy\" ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkFloatTypeValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckFloatTypeValue() {
        String[] values = null;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Msg :");
        long lineNumber = 1L;
        int index = 1;
        String value = "1.04";
        Column column = new Column();
        column.setFlagType(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        column.setName("colonne");
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = new HashMap();
        variablesTypesDonnees.put("colonne", null);
        IRequestPropertiesACBB requestPropertiesACBB = null;
        GenericTestValues instance = new GenericTestValues();
        Float expResult = 1.04F;
        Map<String, Float> mins = new HashMap();
        mins.put(MockUtils.VARIABLE_CODE, 0F);
        Map<String, Float> maxs = new HashMap();
        mins.put(MockUtils.VARIABLE_CODE, 2F);
        when(this.datasetDescriptor.getMins()).thenReturn(mins);
        when(this.datasetDescriptor.getMaxs()).thenReturn(maxs);
        Float result = instance
                .checkFloatTypeValue(values, badsFormatsReport, lineNumber, index, value, column,
                        variablesTypesDonnees, this.datasetDescriptor, requestPropertiesACBB);
        Assert.assertEquals(expResult, result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        value = "cinquante";
        result = instance.checkFloatTypeValue(values, badsFormatsReport, lineNumber, index, value,
                column, variablesTypesDonnees, this.datasetDescriptor, requestPropertiesACBB);
        Assert.assertNull(result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Msg : :- Une valeur flottante est attendue à la ligne 1, colonne 2 (colonne): la valeur actuelle est \"cinquante\" ",
                badsFormatsReport.getMessages());
        column.setFlagType("type");
        result = instance.checkFloatTypeValue(values, badsFormatsReport, lineNumber, index, "12",
                column, variablesTypesDonnees, this.datasetDescriptor, requestPropertiesACBB);
        Assert.assertTrue(12L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Msg : :- Une valeur flottante est attendue à la ligne 1, colonne 2 (colonne): la valeur actuelle est \"cinquante\" ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkIntegerTypeValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckIntegerTypeValue() {
        String[] values = null;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Msg :");
        long lineNumber = 1L;
        int index = 1;
        String value = "12";
        Column column = new Column();
        column.setFlagType(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        column.setName("colonne");
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = new HashMap();
        variablesTypesDonnees.put("colonne", null);
        IRequestPropertiesACBB requestPropertiesACBB = null;
        GenericTestValues instance = new GenericTestValues();
        Map<String, Float> mins = new HashMap();
        mins.put(MockUtils.VARIABLE_CODE, 12F);
        Map<String, Float> maxs = new HashMap();
        mins.put(MockUtils.VARIABLE_CODE, 13F);
        when(this.datasetDescriptor.getMins()).thenReturn(mins);
        when(this.datasetDescriptor.getMaxs()).thenReturn(maxs);
        Integer result = instance.checkIntegerTypeValue(values, badsFormatsReport, lineNumber,
                index, value, column, variablesTypesDonnees, this.datasetDescriptor,
                requestPropertiesACBB);
        Assert.assertTrue(12 == result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        value = "cinquante";
        result = instance
                .checkIntegerTypeValue(values, badsFormatsReport, lineNumber, index, value, column,
                        variablesTypesDonnees, this.datasetDescriptor, requestPropertiesACBB);
        Assert.assertTrue(ACBBUtils.CST_INVALID_BAD_MEASURE == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Msg : :- Une valeur entière est attendue à la ligne 1, colonne 2 (colonne): la valeur actuelle est \"cinquante\" ",
                badsFormatsReport.getMessages());
        column.setFlagType("type");
        result = instance.checkIntegerTypeValue(values, badsFormatsReport, lineNumber, index, "12",
                column, variablesTypesDonnees, this.datasetDescriptor, requestPropertiesACBB);
        Assert.assertTrue(12 == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Msg : :- Une valeur entière est attendue à la ligne 1, colonne 2 (colonne): la valeur actuelle est \"cinquante\" ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkIntervalValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckIntervalValue() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err :");
        long lineNumber = 1L;
        int index = 1;
        Column column = new Column();
        column.setName("colonne");
        column.setFlagType(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Map<String, Float> mins = Mockito.mock(Map.class);
        Map<String, Float> maxs = Mockito.mock(Map.class);
        when(this.datasetDescriptor.getMins()).thenReturn(mins);
        when(this.datasetDescriptor.getMaxs()).thenReturn(maxs);
        when(mins.get("colonne")).thenReturn(12F);
        when(maxs.get("colonne")).thenReturn(13F);
        GenericTestValues instance = new GenericTestValues();
        instance.checkIntervalValue(badsFormatsReport, lineNumber, index, column,
                this.variablesTypesDonnees, this.datasetDescriptor, 12.2F);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        instance.checkIntervalValue(badsFormatsReport, lineNumber, index, column,
                this.variablesTypesDonnees, this.datasetDescriptor, 14.2F);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err : :- La valeur attendue à la ligne 1, colonne 2 (colonne) est hors limites: la valeur actuelle est \"14,200000\" elle devrait être comprise entre 12,000000 et 13,000000 ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkTimeTypeValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckTimeTypeValue() throws DateTimeException {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err :");
        long lineNumber = 1L;
        int index = 1;
        Column column = new Column();
        column.setName("colonne");
        column.setFormatType("HH:mm");
        column.setFlagType(RecorderACBB.PROPERTY_CST_TIME_TYPE);
        String value = "12:23";
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        IRequestPropertiesACBB requestPropertiesACBB = null;
        LocalTime expResult = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, value);
        LocalTime result = this.instance.checkTimeTypeValue(null, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Assert.assertEquals(expResult, result);
        value = "24:00";
        expResult = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, "00:00");
        result = this.instance.checkTimeTypeValue(null, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        Assert.assertEquals(expResult, result);
        this.instance.checkTimeTypeValue(null, badsFormatsReport, lineNumber, index, "midi",
                column, variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Err : :- \"midi:\" n'est pas un format d'heure valide à la ligne 1 colonne 2 (colonne). L'heure doit-être au format \"HH:mm\" ",
                badsFormatsReport.getMessages());

    }

    /**
     * Test of checkValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckValue() throws DateTimeException {
        GenericTestValues instance = Mockito.spy(new GenericTestValues());
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err :");
        final LocalDate date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "25/12/2014");
        final LocalTime time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, "12:32");
        long lineNumber = 1L;
        int index = 1;
        String value = "valeur";
        Column column = new Column();
        String[] values = null;
        Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees = null;
        DatasetDescriptorACBB datasetDescriptor = null;
        IRequestPropertiesACBB requestPropertiesACBB = null;
        Mockito.doReturn(10.2F)
                .when(instance)
                .checkFloatTypeValue(values, badsFormatsReport, lineNumber, index, value, column,
                        variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.doReturn(10)
                .when(instance)
                .checkIntegerTypeValue(values, badsFormatsReport, lineNumber, index, value, column,
                        variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.doReturn(date)
                .when(instance)
                .checkDateTypeValue(values, badsFormatsReport, lineNumber, index, value, column,
                        variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.doReturn(time)
                .when(instance)
                .checkTimeTypeValue(values, badsFormatsReport, lineNumber, index, value, column,
                        variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        // case PROPERTY_CST_FLOAT_TYPE
        column.setValueType(RecorderACBB.PROPERTY_CST_FLOAT_TYPE);
        instance.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.verify(instance, Mockito.times(1)).checkFloatTypeValue(values, badsFormatsReport,
                lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor,
                requestPropertiesACBB);
        // case PROPERTY_CST_INTEGER_TYPE
        column.setValueType(RecorderACBB.PROPERTY_CST_INTEGER_TYPE);
        instance.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.verify(instance, Mockito.times(1)).checkIntegerTypeValue(values, badsFormatsReport,
                lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor,
                requestPropertiesACBB);
        // case PROPERTY_CST_DATE_TYPE
        column.setValueType("other");
        column.setFlag(true);
        column.setFlagType(RecorderACBB.PROPERTY_CST_DATE_TYPE);
        instance.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.verify(instance, Mockito.times(1)).checkDateTypeValue(values, badsFormatsReport,
                lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor,
                requestPropertiesACBB);
        // case PROPERTY_CST_TIME_TYPE
        column.setFlagType(RecorderACBB.PROPERTY_CST_TIME_TYPE);
        instance.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.verify(instance, Mockito.times(1)).checkTimeTypeValue(values, badsFormatsReport,
                lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor,
                requestPropertiesACBB);
        // case "other"
        column.setFlag(false);
        instance.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                variablesTypesDonnees, datasetDescriptor, requestPropertiesACBB);
        Mockito.verify(instance, Mockito.times(1)).checkOtherTypeValue(values, badsFormatsReport,
                lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor,
                requestPropertiesACBB);
        Assert.assertFalse(badsFormatsReport.hasErrors());

    }

    /**
     * Test of cleanValue method, of class GenericTestValues.
     */
    @Test
    public void testCleanValue() {
        String value = "12,5";
        String expResult = "12.5";
        String result = this.instance.cleanValue(value);
        Assert.assertEquals(expResult, result);
        value = "12 5,0";
        expResult = "125.0";
        result = this.instance.cleanValue(value);
        Assert.assertEquals(expResult, result);

    }

    /**
     * Test of getIntervalDateFromVersion method, of class GenericTestValues.
     */
    @Test
    public void testGetIntervalDateFromVersion_IntervalDate() {
        IntervalDate intervalDate = this.instance.getIntervalDateFromVersion(this.m.versionFile);
        String[] expResult = new String[]{"01/01/2012", "00:00", "31/12/2012", "23:30"};
        String[] result = this.instance.getIntervalDateFromVersion(intervalDate);
        Assert.assertArrayEquals(expResult, result);
    }

    /**
     * Test of getIntervalDateFromVersion method, of class GenericTestValues.
     */
    @Test
    public void testGetIntervalDateFromVersion_VersionFile() {
        VersionFile versionFile = null;
        IntervalDate result = this.instance.getIntervalDateFromVersion(versionFile);
        Assert.assertNull(result);
        versionFile = this.m.versionFile;
        result = this.instance.getIntervalDateFromVersion(versionFile);
        Assert.assertEquals("du 01/01/2012;00:00 au 31/12/2012;23:30", result.toString());
    }

    /**
     * Test of testValues method, of class GenericTestValues.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestValues() throws Exception {
        long startline = 1L;
        String text = "ligne 1;coucou\nligne 2;bonjour\n\nligne 3;fin";
        Duplicates testDuplicates = new Duplicates(false);
        when(this.requestPropertiesACBB.getTestDuplicates()).thenReturn(testDuplicates);
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        VersionFile versionFile = this.m.versionFile;
        IRequestPropertiesACBB requestProperties = this.requestPropertiesACBB;
        String encoding = "UTF-8";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("error");
        DatasetDescriptorACBB datasetDescriptor = this.m.datasetDescriptor;
        Mockito.doReturn(this.variablesTypesDonnees).when(this.datatypeVariableUniteACBBDAO)
                .getAllVariableTypeDonneesByDataTypeMapByVariableAffichage(this.datatypeName);
        this.instance.testValues(startline, parser, versionFile, requestProperties, encoding,
                badsFormatsReport, datasetDescriptor, this.datatypeName);
        Mockito.verify(this.datatypeVariableUniteACBBDAO, Mockito.times(1))
                .getAllVariableTypeDonneesByDataTypeMapByVariableAffichage(this.datatypeName);
        Assert.assertTrue(0 == testDuplicates.reports.size());
        List<Parameters> parameters = testDuplicates.parameters;
        Assert.assertTrue(3 == parameters.size());
        Assert.assertEquals("ligne 1", parameters.get(0).values[0]);
        Assert.assertEquals("coucou", parameters.get(0).values[1]);
        Assert.assertEquals("ligne 2", parameters.get(1).values[0]);
        Assert.assertEquals("bonjour", parameters.get(1).values[1]);
        Assert.assertEquals("ligne 3", parameters.get(2).values[0]);
        Assert.assertEquals("fin", parameters.get(2).values[1]);
        Assert.assertTrue(4 == parameters.get(0).dates.length);
        Assert.assertTrue(4 == parameters.get(1).dates.length);
        Assert.assertTrue(4 == parameters.get(2).dates.length);
        Assert.assertTrue(2 == parameters.get(0).lineNumber);
        Assert.assertTrue(3 == parameters.get(1).lineNumber);
        Assert.assertTrue(4 == parameters.get(2).lineNumber);
        text = "ligne 1";
        parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        testDuplicates = new Duplicates(true);
        when(this.requestPropertiesACBB.getTestDuplicates()).thenReturn(testDuplicates);
        this.instance.testValues(startline, parser, versionFile, requestProperties, encoding,
                badsFormatsReport, datasetDescriptor, this.datatypeName);
        Assert.assertTrue(1 == testDuplicates.reports.size());
        Assert.assertEquals(testDuplicates.reports.get(0), badsFormatsReport);
        parameters = testDuplicates.parameters;
        Assert.assertTrue(1 == parameters.size());
        Assert.assertEquals("ligne 1", parameters.get(0).values[0]);
        Assert.assertTrue(1 == parameters.get(0).values.length);
        Assert.assertTrue(4 == parameters.get(0).dates.length);
        Assert.assertTrue(2 == parameters.get(0).lineNumber);

    }

    class Duplicates implements ITestDuplicates {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        List<BadsFormatsReport> reports = new LinkedList();
        IErrorsReport errorsReport;
        List<Parameters> parameters = new LinkedList();
        boolean hasError = false;

        Duplicates(boolean hasError) {
            this.hasError = hasError;
        }

        @Override
        public void addErrors(BadsFormatsReport badsFormatsReport) {
            this.reports.add(badsFormatsReport);
        }

        @Override
        public void addLine(String[] values, long lineNumber, String[] dates,
                            VersionFile versionFile) {
            this.parameters.add(new Parameters(values, lineNumber, dates, versionFile));
        }

        @Override
        public boolean hasError() {
            return this.hasError;
        }

        @Override
        public void setErrorsReport(IErrorsReport errorsReport) {
            this.errorsReport = errorsReport;
        }
    }

    class Parameters {

        String[] values;
        long lineNumber;
        String[] dates;
        VersionFile versionFile;

        Parameters(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
            this.values = values;
            this.lineNumber = lineNumber;
            this.dates = dates;
            this.versionFile = versionFile;
        }
    }
}
