/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.test.utils.BiomassMockUtils;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class RecorderTest {

    BiomassMockUtils m = BiomassMockUtils.getInstance();
    Recorder instance;
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    IMethodeLAIDAO methodeLAIDAO;
    @Mock
    MethodeLAI methodeLAI;
    @Mock
    Properties propertiesLibelleEN;
    @Mock
    Properties propertiesDefinitionEN;
    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    protected void init(Recorder instance) {
        MockitoAnnotations.initMocks(this);
        this.instance = instance;
        this.instance.setMethodeLAIDAO(this.methodeLAIDAO);
        this.instance.setMethodeDAO(this.methodeLAIDAO);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.init(new Recorder());
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "104";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()));
        Mockito.when(this.methodeLAIDAO.getByNumberId(104L)).thenReturn(Optional.of(this.methodeLAI));
        this.instance.deleteRecord(parser, null, "UTF-8");
        // nominal
        Mockito.verify(this.methodeLAIDAO).getByNumberId(104L);
        Mockito.verify(this.methodeLAIDAO).remove(this.methodeLAI);
        // persitence exception
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()));
        Mockito.doThrow(new PersistenceException("erreur")).when(this.methodeLAIDAO)
                .remove(this.methodeLAI);
        try {
            this.instance.deleteRecord(parser, null, "UTF-8");
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals("erreur", e.getMessage());
        }
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        List<MethodeLAI> methodes = new LinkedList();
        Mockito.when(this.methodeLAIDAO.getAll()).thenReturn(methodes);
        List<MethodeLAI> result = this.instance.getAllElements();
        Assert.assertEquals(methodes, result);
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        this.testInitModelGridMetadata();
        this.methodeLAI = new MethodeLAI("libellé", "définition", 102L);
        Mockito.when(this.propertiesLibelleEN.getProperty("libellé", "libellé")).thenReturn(
                "libellé_en");
        Mockito.when(this.propertiesDefinitionEN.getProperty("définition", "définition"))
                .thenReturn("définition_en");
        LineModelGridMetadata result = this.instance.getNewLineModelGridMetadata(this.methodeLAI);
        Assert.assertTrue(5 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("102", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("libellé", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("libellé_en", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("définition", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("définition_en", result.getColumnModelGridMetadataAt(4).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     */
    @Test
    public void testInitModelGridMetadata() {
        this.instance = Mockito.spy(this.instance);
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        Mockito.when(
                MockUtils.localizationManager.newProperties(this.instance.getMethodeEntityName(),
                        this.instance.getMethodeLibelleName(), Locale.ENGLISH)).thenReturn(
                this.propertiesLibelleEN);
        Mockito.when(
                MockUtils.localizationManager.newProperties(this.instance.getMethodeEntityName(),
                        this.instance.getMethodeDefinitionName(), Locale.ENGLISH)).thenReturn(
                this.propertiesDefinitionEN);
        this.instance.initModelGridMetadata();
        Mockito.verify(MockUtils.localizationManager).newProperties(
                this.instance.getMethodeEntityName(), this.instance.getMethodeLibelleName(),
                Locale.ENGLISH);
        Mockito.verify(MockUtils.localizationManager).newProperties(
                this.instance.getMethodeEntityName(), this.instance.getMethodeDefinitionName(),
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String text = "identifiant;libellé_fr;libellé_en;définition_fr;définition_en\n"
                + "300;manuelle;manual;relevé manuel;manual reading";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        String encoding = "UTF-8";
        // nominal
        when(methodeLAIDAO.getByNKey(any())).thenReturn(Optional.empty());
        this.instance.processRecord(parser, null, encoding);
        ArgumentCaptor<MethodeLAI> ma = ArgumentCaptor.forClass(MethodeLAI.class);
        Mockito.verify(this.methodeLAIDAO).saveOrUpdate(ma.capture());
        this.methodeLAI = ma.getValue();
        Assert.assertEquals("manuelle", this.methodeLAI.getLibelle());
        Assert.assertEquals("relevé manuel", this.methodeLAI.getDefinition());
        Assert.assertTrue(300 == this.methodeLAI.getNumberId());
        // with error
        text = "identifiant;libellé_fr;libellé_en;définition_fr;définition_en\n"
                + "trois-cents;manuelle;manual;relevé manuel;manual reading";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        try {
            this.instance.processRecord(parser, null, encoding);
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals(
                    ".Ligne 1 , colonne 1, l'identifiant trois-cents de la méthode doit être un entier.\n",
                    e.getMessage());
        }
    }

}
