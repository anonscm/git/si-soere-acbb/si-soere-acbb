package org.inra.ecoinfo.acbb.dataset.impl;

import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;
import org.mockito.MockitoAnnotations;

import java.time.DateTimeException;
import java.time.LocalDateTime;

/**
 * @author philippe
 */
public class VersionFileHelperIT {

    LocalDateTime dateDebut;
    LocalDateTime dateFin;
    MockUtils m = MockUtils.getInstance();
    /**
     *
     */
    public VersionFileHelperIT() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @throws DateTimeException
     */
    @Before
    public void setUp() throws DateTimeException {
        MockitoAnnotations.initMocks(this);
        dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "23/01/2014");
        dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "23/02/2014");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildDownloadFilename method, of class VersionFileHelper.
     */
    @Test
    public void testBuildDownloadFilename() {
        VersionFileHelper versionFileHelper = new VersionFileHelper();
        String name = versionFileHelper.buildDownloadFilename(m.versionFile);
        Assert.assertEquals("site_datatype_parcelle_23-01-2014_23-02-2014.csv", name);
        name = versionFileHelper.buildDownloadFilename(m.versionFile);
        Assert.assertEquals("site_sousSite_datatype_parcelle_23-01-2014_23-02-2014.csv", name);
        name = versionFileHelper.buildDownloadFilename(m.versionFile);
        Assert.assertEquals("site_sousSite_datatype_23-01-2014_23-02-2014.csv", name);
    }

}
