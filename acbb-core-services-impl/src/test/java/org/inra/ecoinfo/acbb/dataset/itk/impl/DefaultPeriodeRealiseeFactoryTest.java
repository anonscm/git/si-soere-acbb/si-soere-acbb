/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class DefaultPeriodeRealiseeFactoryTest {

    @Spy
    DefaultPeriodeRealiseeFactory<ILineRecord> instance = new DefaultPeriodeRealiseeFactory();
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    IPeriodeRealiseeDAO periodeRealiseeDAO;
    @Mock
    AnneeRealisee anneeRealisee;
    @Mock
    PeriodeRealisee periodeRealisee;
    @Mock
    ILineRecord line;
    @Mock
    Tempo tempo;
    /**
     *
     */
    public DefaultPeriodeRealiseeFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance.setPeriodeRealiseeDAO(this.periodeRealiseeDAO);
        when(periodeRealiseeDAO.getByNKey(any(), anyInt())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of createDefaultPeriode method, of class DefaultPeriodeRealiseeFactory.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testCreateDefaultPeriode() throws PersistenceException {
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.doReturn(this.periodeRealisee).when(this.instance)
                .getDefaultPeriode(this.anneeRealisee);
        this.instance.createDefaultPeriode(this.anneeRealisee, errorsReport);
        Mockito.verify(this.periodeRealiseeDAO).saveOrUpdate(this.periodeRealisee);
        // persitence exception
        Mockito.doThrow(new PersistenceException("erreur")).when(this.periodeRealiseeDAO)
                .saveOrUpdate(this.periodeRealisee);
        this.instance.createDefaultPeriode(this.anneeRealisee, errorsReport);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Une erreur inconnue est survenue lors de l'enregistrement des données. La publication a été annulée.\n",
                errorsReport.getErrorsMessages());
    }

    /**
     * Test of getDefaultPeriode method, of class DefaultPeriodeRealiseeFactory.
     */
    @Test
    public void testGetDefaultPeriode() {
        PeriodeRealisee result = this.instance.getDefaultPeriode(this.anneeRealisee);
        Assert.assertTrue(0 == result.getNumero());
        Assert.assertEquals(this.anneeRealisee, result.getAnneeRealisee());
    }

    /**
     * Test of getOrCreatePeriodeRealisee method, of class DefaultPeriodeRealiseeFactory.
     */
    @Test
    public void testGetOrCreatePeriodeRealisee() {
        ErrorsReport errorsReport = new ErrorsReport();
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        PeriodeRealisee result = this.instance.getOrCreatePeriodeRealisee(this.line, errorsReport).orElse(null);
        Assert.assertNull(result);
        // with tempo in line
        Mockito.when(this.tempo.getPeriodeRealisee()).thenReturn(this.periodeRealisee);
        result = this.instance.getOrCreatePeriodeRealisee(this.line, errorsReport).orElse(null);
        Assert.assertEquals(this.periodeRealisee, result);
        // with periode in DB
        Mockito.when(this.periodeRealisee.getAnneeRealisee()).thenReturn(this.anneeRealisee);
        Mockito.when(this.periodeRealisee.getNumero()).thenReturn(1);
        Mockito.when(this.periodeRealiseeDAO.getByNKey(this.anneeRealisee, 1)).thenReturn(
                Optional.of(this.periodeRealisee));
        Mockito.when(this.tempo.getPeriodeRealisee()).thenReturn(null);
        Assert.assertEquals(this.periodeRealisee, result);
    }

}
