/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * @author ptcherniati
 */
public class VariableValueBiomassProductionTest {

    VariableValueBiomassProduction instance;
    @Mock
    MasseVegClass masseVegClass;
    MockUtils m = MockUtils.getInstance();
    /**
     *
     */
    public VariableValueBiomassProductionTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new VariableValueBiomassProduction(1, 4.2F, null, 2, null, this.m.datatypeVariableUnite,
                "2.4");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testConstructor() {
        AbstractMethode method = Mockito.mock(AbstractMethode.class);
        this.instance = new VariableValueBiomassProduction(1, 4.2F, this.masseVegClass, 2, method,
                this.m.datatypeVariableUnite, "2.4");
        Assert.assertEquals(1, this.instance.getMeasureNumber());
        Assert.assertEquals(4.2F, this.instance.getStandardDeviation(), 0.0F);
        Assert.assertEquals(this.masseVegClass, this.instance.getMasseVegClass());
        Assert.assertEquals(this.m.datatypeVariableUnite, this.instance.getDatatypeVariableUnite());
        Assert.assertEquals(new Integer(2), this.instance.getQualityClass());
        Assert.assertEquals("2.4", this.instance.getValue());
    }

    /**
     * Test of getMasseVegClass method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testGetMasseVegClass() {
        this.instance.setMasseVegClass(this.masseVegClass);
        MasseVegClass result = this.instance.getMasseVegClass();
        Assert.assertEquals(this.masseVegClass, result);
    }

    /**
     * Test of getMeasureNumber method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testGetMeasureNumber() {
        int expResult = 24;
        this.instance.setMeasureNumber(expResult);
        int result = this.instance.getMeasureNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getStandardDeviation method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testGetStandardDeviation() {
        float expResult = 24.3F;
        this.instance.setStandardDeviation(expResult);
        float result = this.instance.getStandardDeviation();
        Assert.assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setMasseVegClass method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testSetMasseVegClass() {
        this.instance.setMasseVegClass(this.masseVegClass);
        Assert.assertEquals(this.masseVegClass, this.instance.getMasseVegClass());

    }

    /**
     * Test of setMeasureNumber method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testSetMeasureNumber() {
        this.instance.setMeasureNumber(24);
        Assert.assertTrue(24 == this.instance.getMeasureNumber());
    }

    /**
     * Test of setMethode method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testSetMethode() {
        AbstractMethode method = Mockito.mock(AbstractMethode.class);
        this.instance.setMethode(method);
        Assert.assertEquals(method, this.instance.getMethode());
    }

    /**
     * Test of setStandardDeviation method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testSetStandardDeviation() {
        this.instance.setStandardDeviation(2.4F);
        Assert.assertTrue(2.4F == this.instance.getStandardDeviation());
    }

    /**
     * Test of getMethode method, of class VariableValueBiomassProduction.
     */
    @Test
    public void testGetMethode() {
        AbstractMethode methode = mock(AbstractMethode.class);
        instance.setMethode(methode);
        assertEquals(methode, instance.getMethode());
    }

}
