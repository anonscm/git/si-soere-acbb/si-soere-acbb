/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.IversionDeTraitementRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO;

/**
 * @author ptcherniati
 */
public class AbstractBuildHelperTest {


    AbstractBuildHelper instance = new AbstractBuildHelperImpl();
    ITKMockUtils m = ITKMockUtils.getInstance();
    ErrorsReport errorsReport;
    InfoReport infoReport;
    @Mock
    ILineRecord line;
    @Mock
    IversionDeTraitementRealiseeFactory versionDeTraitementRealiseeFactory;
    @Mock
    VersionTraitementRealisee versionTraitementRealisee;
    @Mock
    Tempo tempo;
    /**
     *
     */
    public AbstractBuildHelperTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance.setDatatypeName("datatypeName");
        this.errorsReport = new ErrorsReport();
        this.instance.errorsReport = this.errorsReport;
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        this.instance
                .setVersionDeTraitementRealiseeFactory(this.versionDeTraitementRealiseeFactory);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getRequestPropertiesITK method, of class AbstractBuildHelper.
     */
    @Test
    public void testGetRequestPropertiesITK() {
        this.instance.requestPropertiesITK = this.m.requestPropertiesITK;
        IRequestPropertiesITK result = this.instance.getRequestPropertiesITK();
        Assert.assertEquals(this.m.requestPropertiesITK, result);
    }

    /**
     * Test of getTempo method, of class AbstractBuildHelper.
     */
    @Test
    public void testGetTempo() throws PersistenceException {
        when(m.versionDeTraitementDAO.merge(m.versionDeTraitement)).thenReturn(m.versionDeTraitement);
        Mockito.when(
                this.versionDeTraitementRealiseeFactory.getTempo(any(ILineRecord.class),
                        any(VersionDeTraitement.class), any(Parcelle.class),
                        any(ErrorsReport.class), any(InfoReport.class)))
                .thenReturn(this.tempo);
        Tempo result = this.instance.getTempo(this.line, this.m.versionDeTraitement,
                this.m.parcelle);
        Assert.assertEquals(this.tempo, result);
    }

    /**
     * Test of update method, of class AbstractBuildHelper.
     */
    @Test
    public void testUpdate() {
        List<AbstractLineRecord> lines = new LinkedList();
        this.instance.update(this.m.versionFile, lines, this.errorsReport,
                this.m.requestPropertiesITK);
        final HashMap allDVU = new HashMap();
        Mockito.when(
                this.m.datatypeVariableUniteDAO
                        .getAllVariableTypeDonneesByDataTypeMapByVariableCode("datatypeName"))
                .thenReturn(allDVU);
        Assert.assertEquals(this.m.versionFile, this.instance.version);
        Assert.assertEquals(lines, this.instance.lines);
        Assert.assertEquals(this.errorsReport, this.instance.errorsReport);
        Assert.assertEquals(this.m.requestPropertiesITK, this.instance.requestPropertiesITK);
        //Assert.assertEquals(allDVU, this.instance.dbDatatypeVariableUnites);
    }

    /**
     * @param <ILineRecord>
     */
    public class AbstractBuildHelperImpl<ILineRecord> extends AbstractBuildHelper {

        /**
         * @throws PersistenceException
         */
        @Override
        public void build() throws PersistenceException {
        }
    }

}
