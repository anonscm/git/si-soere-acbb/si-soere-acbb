package org.inra.ecoinfo.acbb.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.*;

import java.io.StringReader;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class AbstractProcessRecordTest {


    MockUtils m = MockUtils.getInstance();
    List<Column> columns = new LinkedList();
    AbstractProcessRecordImpl instance;
    DatasetDescriptorACBB datasetDescriptor;
    ErrorsReport errorsReport = new ErrorsReport();
    @Mock
    IRequestPropertiesACBB requestProperties;
    @Mock
    DatasetDescriptorACBB datasetDescriptorACBB;
    @Mock
    Column column1;
    @Mock
    Column column2;
    @Mock
    Column columnNoFlag;
    @Mock
    VariableACBB variableACBB1;
    @Mock
    VariableACBB variableACBB2;
    @Mock
    VariableACBB variableACBB3;
    @Mock
    CSVParser parser;
    /**
     *
     */
    public AbstractProcessRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractProcessRecordImpl();
        this.instance.setVariableDAO(this.m.variableDAO);
        this.instance.setParcelleDAO(this.m.parcelleDAO);
        this.instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        this.instance.setTraitementDAO(this.m.traitementDAO);
        this.instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        this.instance.setVersionFileDAO(this.m.versionFileDAO);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        this.instance.setListeACBBDAO(this.m.listeACBBDAO);
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.datasetDescriptor = this.m.datasetDescriptor;
        Column colonne1 = this.m.column;
        Column colonne2 = this.m.column;
        Column colonne3 = this.m.column;
        Column colonne4 = this.m.column;
        Column colonne5 = this.m.column;
        Column colonne6 = this.m.column;
        Column colonne7 = this.m.column;
        Column colonne8 = this.m.column;
        this.columns.add(colonne1);
        this.columns.add(colonne2);
        this.columns.add(colonne3);
        this.columns.add(colonne4);
        this.columns.add(colonne5);
        this.columns.add(colonne6);
        this.columns.add(colonne7);
        this.columns.add(colonne8);
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        Mockito.when(this.column1.getName()).thenReturn("colonne 1");
        Mockito.when(this.column1.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.column1.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.column2.getName()).thenReturn("colonne 2");
        Mockito.when(this.column2.isFlag()).thenReturn(Boolean.TRUE);
        Mockito.when(this.column2.getFlagType())
                .thenReturn(RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.columnNoFlag.getName()).thenReturn("colonne no Flag");
        Mockito.when(this.m.variableDAO.getByAffichage("colonne 1")).thenReturn(Optional.of(this.variableACBB1));
        Mockito.when(this.m.variableDAO.getByAffichage("colonne 2")).thenReturn(Optional.of(this.variableACBB2));
        Mockito.when(this.m.variableDAO.getByAffichage("colonne no Flag")).thenReturn(
                Optional.of(this.variableACBB3));
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildVariablesHeaderAndSkipHeader method, of class AbstractProcessRecord.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildVariablesHeaderAndSkipHeader() throws Exception {
        String text = "un text";
        instance.setDatatypeName("datatype");
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        Mockito.when(this.m.column.getFlagType()).thenReturn(text);
        Mockito.when(this.m.column.getFlagType()).thenReturn(
                /* test column date */
                RecorderACBB.PROPERTY_CST_DATE_TYPE,
                /* test column time */
                RecorderACBB.PROPERTY_CST_TIME_TYPE, RecorderACBB.PROPERTY_CST_TIME_TYPE,
                /* test variable */
                RecorderACBB.PROPERTY_CST_VARIABLE_TYPE, RecorderACBB.PROPERTY_CST_VARIABLE_TYPE,
                RecorderACBB.PROPERTY_CST_VARIABLE_TYPE, RecorderACBB.PROPERTY_CST_VARIABLE_TYPE,
                /* test list */
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE,
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE,
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE,
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE,
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE,
                /* test valeur qualitative */
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE,
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE,
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE,
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE,
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE,
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE,
                /* test non flag */
                "rien", "rien", "rien",
                /* test flag mais rien */
                "rien", "rien", "rien", "rien", "rien", "rien",
                /* test Quality class */
                RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC,
                RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC,
                RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC,
                /* fin des colonnes */
                "fin", null);
        Mockito.when(this.m.column.isFlag()).thenReturn(Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
        ArgumentCaptor<String> columnName = ArgumentCaptor.forClass(String.class);
        Mockito.when(this.m.variableDAO.getByAffichage("colonne1")).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.m.variableDAO.getByAffichage("colonne2")).thenReturn(Optional.of(this.m.variable));
        Mockito.when(this.m.variableDAO.getByAffichage("colonne3")).thenReturn(Optional.of(this.m.variable));
        Map<String, DatatypeVariableUniteACBB> dvus = Mockito.mock(Map.class);
        Mockito.when(dvus.get(MockUtils.VARIABLE_CODE)).thenReturn(m.datatypeVariableUnite);
        Mockito.when(m.datatypeVariableUniteDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode("datatype")).thenReturn(dvus);
        List<VariableACBB> result = this.instance.buildVariablesHeaderAndSkipHeader(parser,
                this.datasetDescriptor);
        Mockito.verify(this.m.variableDAO, Mockito.times(3)).getByAffichage(columnName.capture());
        Mockito.verify(this.m.column, Mockito.times(3)).getName();
        Assert.assertTrue(result.size() == 3);
        Assert.assertEquals("colonne1", columnName.getAllValues().get(0));
        Assert.assertEquals("colonne2", columnName.getAllValues().get(1));
        Assert.assertEquals("colonne3", columnName.getAllValues().get(2));
        Assert.assertTrue(result.size() == 3);
        Assert.assertEquals(this.m.datatypeVariableUnite, result.get(0));
        Assert.assertEquals(this.m.datatypeVariableUnite, result.get(2));
        Assert.assertEquals(this.m.datatypeVariableUnite, result.get(2));
        Assert.assertEquals("fin", this.m.column.getFlagType());

        Map<Integer, Column> columns = new HashMap<>();
        columns.put(1, this.columnNoFlag);
        columns.put(2, this.column1);
        columns.put(3, this.column2);
        instance.setDatatypeName("datatypeName");
        Map<String, DatatypeVariableUniteACBB> dbDatatypeVariableUnites = Mockito.mock(Map.class);
        DatatypeVariableUniteACBB dvu1 = Mockito.mock(DatatypeVariableUniteACBB.class);
        DatatypeVariableUniteACBB dvu2 = Mockito.mock(DatatypeVariableUniteACBB.class);
        Mockito.when(m.datatypeVariableUniteDAO.getAllVariableTypeDonneesByDataTypeMapByVariableCode("datatypeName")).thenReturn(dbDatatypeVariableUnites);
        Mockito.when(m.variableDAO.getByAffichage("colonne 1")).thenReturn(Optional.of(variableACBB1));
        Mockito.when(m.variableDAO.getByAffichage("colonne 2")).thenReturn(Optional.of(variableACBB2));
        Mockito.when(variableACBB1.getCode()).thenReturn("variable1");
        Mockito.when(variableACBB2.getCode()).thenReturn("variable2");
        Mockito.when(dbDatatypeVariableUnites.get("variable1")).thenReturn(dvu1);
        Mockito.when(dbDatatypeVariableUnites.get("variable2")).thenReturn(dvu2);
        // nminal
        Mockito.when(this.datasetDescriptorACBB.getEnTete()).thenReturn(3);
        result = this.instance.buildVariablesHeaderAndSkipHeader(this.parser, columns, this.datasetDescriptorACBB);
        Assert.assertTrue(3 == result.size());
        Assert.assertNull(result.get(0));
        Assert.assertEquals(dvu1, result.get(1));
        Assert.assertEquals(dvu2, result.get(2));
        Mockito.verify(this.parser, Mockito.times(3)).getLine();
    }

    /**
     * Test of getDate method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetDate() throws DateTimeException {
        // cas nominal
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.DATE_DEBUT});
        LocalDate date = this.instance.getDate(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertEquals(m.dateDebut, date);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        cleanerValues = new CleanerValues(new String[]{"bad date"});
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        date = this.instance.getDate(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertNull(date);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-\"bad date\" n'est pas un format de date valide à la ligne 1 colonne 2 (Colonne 2). La date doit-être au format \"dd/MM/yyyy\"\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getDateUTC method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetLocalDate() throws DateTimeException {
        // cas nominal
        CleanerValues cleanerValues = new CleanerValues(new String[]{MockUtils.DATE_DEBUT});
        LocalDate date = this.instance.getLocalDate(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertEquals(m.dateDebut, date);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        cleanerValues = new CleanerValues(new String[]{"bad date"});
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        date = this.instance.getLocalDate(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertNull(date);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-\"bad date\" n'est pas un format de date valide à la ligne 1 colonne 2 (Colonne 2). La date doit-être au format \"dd/MM/yyyy\"\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getFloat method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetFloat() {
        // cas nominal
        CleanerValues cleanerValues = new CleanerValues(new String[]{"120.26489"});
        float floatValue = this.instance.getFloat(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertTrue(120.26489F == floatValue);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        cleanerValues = new CleanerValues(new String[]{"120*26489"});
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        floatValue = this.instance.getFloat(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertTrue(ACBBUtils.CST_INVALID_BAD_MEASURE == floatValue);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Une valeur flottante est attendue à la ligne 1, colonne 2 (Colonne 2): la valeur actuelle est \"120*26489\"\n",
                this.errorsReport.getErrorsMessages());
        // empty or null value
        cleanerValues = new CleanerValues(new String[]{org.apache.commons.lang.StringUtils.EMPTY, null});
        this.errorsReport = new ErrorsReport();
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        floatValue = this.instance.getFloat(this.errorsReport, 2L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertTrue(ACBBUtils.CST_INVALID_EMPTY_MEASURE == floatValue);
        Assert.assertFalse(this.errorsReport.hasErrors());
        floatValue = this.instance.getFloat(this.errorsReport, 2L, 2, cleanerValues,
                this.datasetDescriptor);
        Assert.assertFalse(this.errorsReport.hasErrors());
    }

    /**
     * Test of getInt method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetInt() {
        // cas nominal
        CleanerValues cleanerValues = new CleanerValues(new String[]{"-120"});
        int entier = this.instance.getInt(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertTrue(-120D == entier);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        cleanerValues = new CleanerValues(new String[]{"120m26489"});
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        entier = this.instance.getInt(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertTrue(entier == ACBBUtils.CST_INVALID_BAD_MEASURE);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Une valeur entière est attendue à la ligne 1, colonne 2 (Colonne 2): la valeur actuelle est \"120m26489\"\n",
                this.errorsReport.getErrorsMessages());
        // empty or null value
        cleanerValues = new CleanerValues(new String[]{org.apache.commons.lang.StringUtils.EMPTY, null});
        this.errorsReport = new ErrorsReport();
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        entier = this.instance.getInt(this.errorsReport, 2L, 1, cleanerValues,
                this.datasetDescriptor);
        Assert.assertTrue(entier == ACBBUtils.CST_INVALID_EMPTY_MEASURE);
        Assert.assertFalse(this.errorsReport.hasErrors());
        entier = this.instance.getInt(this.errorsReport, 2L, 2, cleanerValues,
                this.datasetDescriptor);
        Assert.assertFalse(this.errorsReport.hasErrors());
    }

    /**
     * Test of getObjectSize method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetObjectSize() {
        Object o = 145L;
        int size = this.instance.getObjectSize(o);
        Assert.assertTrue(size == 82);
        o = "bonjour";
        size = this.instance.getObjectSize(o);
        Assert.assertTrue(size == 7 + 7 * 1);
    }

    /**
     * Test of getSite method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetSite() {
        // cas nominal
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site, (SiteACBB) null);
        SiteACBB site = this.instance.getSite(this.requestProperties);
        Assert.assertEquals(this.m.site, site);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        site = this.instance.getSite(this.requestProperties);
        Assert.assertNull(site);
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractProcessRecord.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetSuiviParcelle() throws PersistenceException, DateTimeException {
        // cas nominal
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        SuiviParcelle suiviParcelle = this.instance.getSuiviParcelle(this.errorsReport,
                this.m.parcelle, this.m.traitement, this.m.dateDebut, 1L);
        Assert.assertEquals(this.m.suiviParcelle, suiviParcelle);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // no suivi parcelle
        final String badDate = "20/10/2012";
        final LocalDate badlocaleDate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, badDate);
        when(m.suiviParcelleDAO.getByNKey(any(), any(), any())).thenReturn(Optional.empty());
        suiviParcelle = this.instance.getSuiviParcelle(this.errorsReport, this.m.parcelle,
                this.m.traitement, badlocaleDate, 1L);
        Assert.assertNull(suiviParcelle);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 1 aucun suivi de parcelle n'est défini pour la parcelle Parcelle, le traitement Traitement et la date de début de suivi 20/10/2012\n",
                this.errorsReport.getErrorsMessages());
        this.errorsReport = new ErrorsReport();
        Mockito.when(
                this.m.suiviParcelleDAO
                        .getByNKey(this.m.parcelle, this.m.traitement, badlocaleDate)).thenReturn(Optional.empty());
        suiviParcelle = this.instance.getSuiviParcelle(this.errorsReport, this.m.parcelle,
                this.m.traitement, badlocaleDate, 2L);
        Assert.assertNull(suiviParcelle);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2 aucun suivi de parcelle n'est défini pour la parcelle Parcelle, le traitement Traitement et la date de début de suivi 20/10/2012\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getTraitementProgramme method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetTraitementProgramme() {
        // cas nominal
        CleanerValues cleanerValues = new CleanerValues(new String[]{"traitement"});
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        TraitementProgramme traitementProgramme = this.instance.getTraitementProgramme(
                this.errorsReport, 1L, 1, cleanerValues, this.datasetDescriptor,
                this.requestProperties);
        Assert.assertEquals(this.m.traitement, traitementProgramme);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // pas de traitement
        cleanerValues = new CleanerValues(new String[]{});
        Mockito.when(this.m.traitementDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        when(m.suiviParcelleDAO.getByNKey(any(), any(), any())).thenReturn(Optional.empty());
        traitementProgramme = this.instance.getTraitementProgramme(this.errorsReport, 2L, 1,
                cleanerValues, this.datasetDescriptor, this.requestProperties);
        Assert.assertNull(traitementProgramme);
        Assert.assertTrue(this.errorsReport.hasErrors());
        this.errorsReport = new ErrorsReport();
        // traitement invalide
        cleanerValues = new CleanerValues(new String[]{"pas de traitement"});
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        traitementProgramme = this.instance.getTraitementProgramme(this.errorsReport, 2L, 1,
                cleanerValues, this.datasetDescriptor, this.requestProperties);
        Assert.assertNull(traitementProgramme);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals("-Aucun traitement n'a été défini ligne 2 colonne 1.\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getVariableValue method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetVariableValue() {
        Mockito.when(this.m.column.getName()).thenReturn("name");
        // cas nominal pour valeur qualitative
        ArgumentCaptor<String> nom = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> value = ArgumentCaptor.forClass(String.class);
        Mockito.when(this.m.column.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE);
        Mockito.when(this.m.variable.getIsQualitative()).thenReturn(true);
        ACBBVariableValue variableValue = this.instance.getVariableValue(this.errorsReport, 1L, 1,
                this.m.datatypeVariableUnite, this.m.column, "valeur");
        Assert.assertFalse(this.errorsReport.hasErrors());
        Assert.assertEquals(this.m.datatypeVariableUnite, variableValue.getDatatypeVariableUnite());
        Assert.assertEquals(this.m.valeurQualitative, variableValue.getValeurQualitative());
        Assert.assertTrue(variableValue.isQualitative());
        Mockito.verify(this.m.listeACBBDAO, Mockito.times(1)).getByNKey(nom.capture(),
                value.capture());
        Assert.assertEquals("name", nom.getValue());
        Assert.assertEquals("valeur", value.getValue());
        // cas nominal pour liste de valeurs qualitatives
        Mockito.when(this.m.column.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE);
        Mockito.when(this.m.variable.getIsQualitative()).thenReturn(true);
        variableValue = this.instance.getVariableValue(this.errorsReport, 2L, 1, this.m.datatypeVariableUnite,
                this.m.column, "valeur1, valeur2, valeur3");
        Assert.assertFalse(this.errorsReport.hasErrors());
        Assert.assertEquals(this.m.variable, variableValue.getDatatypeVariableUnite().getVariable());
        Assert.assertTrue(variableValue.getValeursQualitatives().size() == 3);
        Assert.assertEquals(this.m.valeurQualitative, variableValue.getValeursQualitatives().get(0));
        Assert.assertEquals(this.m.valeurQualitative, variableValue.getValeursQualitatives().get(1));
        Assert.assertEquals(this.m.valeurQualitative, variableValue.getValeursQualitatives().get(2));
        Assert.assertTrue(variableValue.isQualitative());
        Mockito.verify(this.m.listeACBBDAO, Mockito.times(4)).getByNKey(nom.capture(),
                value.capture());
        Assert.assertEquals("name", nom.getAllValues().get(1));
        Assert.assertEquals("valeur1", value.getAllValues().get(2));
        Assert.assertEquals("name", nom.getAllValues().get(2));
        Assert.assertEquals("valeur2", value.getAllValues().get(3));
        Assert.assertEquals("name", nom.getAllValues().get(3));
        Assert.assertEquals("valeur3", value.getAllValues().get(4));
        // cas nominal pour valeur
        Mockito.when(this.m.column.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_VARIABLE_TYPE);
        Mockito.when(this.m.variable.getIsQualitative()).thenReturn(false);
        variableValue = this.instance.getVariableValue(this.errorsReport, 3L, 1, this.m.datatypeVariableUnite,
                this.m.column, "120.3");
        Assert.assertFalse(this.errorsReport.hasErrors());
        Assert.assertEquals(this.m.variable, variableValue.getDatatypeVariableUnite().getVariable());
        Assert.assertEquals("120.3", variableValue.getValue());
        Assert.assertFalse(variableValue.isQualitative());
        Mockito.verify(this.m.listeACBBDAO, Mockito.times(4)).getByNKey(nom.capture(),
                value.capture());
        // cas valeur qualitative avec persistence exception
        Mockito.when(this.m.listeACBBDAO.getByNKey(this.m.column.getName(), "valeur")).thenReturn(
                null);
        Mockito.when(this.m.column.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_VALEUR_QUALITATIVE_TYPE);
        Mockito.when(this.m.variable.getIsQualitative()).thenReturn(true);
        when(m.listeACBBDAO.getByNKey(anyString(), anyString())).thenReturn(Optional.empty());
        variableValue = this.instance.getVariableValue(this.errorsReport, 3L, 1, this.m.datatypeVariableUnite,
                this.m.column, "valeur");
        Assert.assertTrue(this.errorsReport.hasErrors());
        Mockito.verify(this.m.listeACBBDAO, Mockito.times(5)).getByNKey(nom.capture(),
                value.capture());
        Assert.assertEquals("name", nom.getValue());
        Assert.assertEquals("valeur", value.getValue());
        Assert.assertEquals(
                "-Ligne 3, colonne 2, la valeur valeur n'existe pas pour la colonne name.\n",
                this.errorsReport.getErrorsMessages());
        // cas valeur qualitative avec persistence exception
        this.errorsReport = new ErrorsReport();
        Mockito.when(this.m.column.getFlagType()).thenReturn(
                RecorderACBB.PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE);
        Mockito.when(this.m.listeACBBDAO.getByNKey(this.m.column.getName(), "valeur1")).thenReturn(
                Optional.of(this.m.valeurQualitative));
        Mockito.when(this.m.listeACBBDAO.getByNKey(this.m.column.getName(), "valeur2")).thenReturn(
                Optional.of(this.m.valeurQualitative));
        Mockito.when(this.m.listeACBBDAO.getByNKey(this.m.column.getName(), "valeur3")).thenReturn(
                Optional.empty());
        Mockito.when(this.m.variable.getIsQualitative()).thenReturn(true);
        variableValue = this.instance.getVariableValue(this.errorsReport, 4L, 1, this.m.datatypeVariableUnite,
                this.m.column, "valeur1, valeur2, valeur3");
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(this.m.variable, variableValue.getDatatypeVariableUnite().getVariable());
        Assert.assertNull(variableValue.getValeurQualitative());
        Mockito.verify(this.m.listeACBBDAO, Mockito.times(8)).getByNKey(nom.capture(),
                value.capture());
        Assert.assertTrue(variableValue.getValeursQualitatives().size() == 2);
        Assert.assertEquals(this.m.valeurQualitative, variableValue.getValeursQualitatives().get(0));
        Assert.assertEquals(this.m.valeurQualitative, variableValue.getValeursQualitatives().get(1));
        Assert.assertEquals("name", nom.getAllValues().get(15));
        Assert.assertEquals("valeur1", value.getAllValues().get(15));
        Assert.assertEquals("name", nom.getAllValues().get(16));
        Assert.assertEquals("valeur2", value.getAllValues().get(16));
        Assert.assertEquals("name", nom.getAllValues().get(17));
        Assert.assertEquals("valeur3", value.getAllValues().get(17));
        Assert.assertEquals(
                "-Ligne 4, colonne 2, la valeur valeur3 n'existe pas pour la colonne name.\n",
                this.errorsReport.getErrorsMessages());

    }

    /**
     * Test of getVersion method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetVersion() {
        // cas nominal
        CleanerValues cleanerValues = new CleanerValues(new String[]{"2"});
        int version = this.instance.getVersion(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertTrue(2 == version);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        cleanerValues = new CleanerValues(new String[]{"trois"});
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        version = this.instance.getVersion(this.errorsReport, 2L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertTrue(version == -1);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals("-Aucun traitement n'a été défini ligne 2 colonne 1.\n",
                this.errorsReport.getErrorsMessages());
        // parse null version
        cleanerValues = new CleanerValues(new String[]{});
        this.errorsReport = new ErrorsReport();
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        version = this.instance.getVersion(this.errorsReport, 3L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertTrue(version == -1);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals("-Aucun traitement n'a été défini ligne 3 colonne 1.\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of getVersionDeTraitement method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetVersionDeTraitement() {
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        // cas nominal
        VersionDeTraitement version = this.instance.getVersionDeTraitement(this.errorsReport,
                this.requestProperties, this.m.traitement, 1, 1L);
        Assert.assertEquals(this.m.versionDeTraitement, version);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // parse exception
        Mockito.when(
                this.m.versionDeTraitementDAO.getByNKey(MockUtils.SITE_CODE, MockUtils.TRAITEMENT,
                        1)).thenReturn(null);
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        when(m.versionDeTraitementDAO.getByNKey(any(), any(), anyInt())).thenReturn(Optional.empty());
        version = this.instance.getVersionDeTraitement(this.errorsReport, this.requestProperties,
                this.m.traitement, 1, 2L);
        Assert.assertNull(version);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Le traitement  Traitement  défini ligne 2 colonne 2 n'existe pas pour le site Site et la version 1.\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of processRecord method, of class AbstractProcessRecord.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        // cas nominal
        CSVParser parser = null;
        final VersionFile versionFile = new VersionFile();
        this.instance = Mockito.spy(this.instance);
        Mockito.doNothing().when(this.instance)
                .verifieSiteRepositoryEqualsSite(versionFile, this.requestProperties);
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        this.instance.processRecord(parser, versionFile, this.requestProperties, "UTF-8",
                this.m.datasetDescriptor);
    }

    /**
     * Test of readDbParcelle method, of class AbstractProcessRecord.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testReadDbParcelle() throws PersistenceException {
        CleanerValues cleanerValues = new CleanerValues(new String[]{"parcelle"});
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        Parcelle parcelle = this.instance.readDbParcelle(this.errorsReport, 1L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertEquals(this.m.parcelle, parcelle);
        Assert.assertFalse(this.errorsReport.hasErrors());
        // persistence exception
        cleanerValues = new CleanerValues(new String[]{"parcelle"});
        this.errorsReport = new ErrorsReport();
        Mockito.when(this.m.datasetDescriptor.getColumnName(1)).thenReturn("Colonne 2");
        Mockito.when(this.m.parcelleDAO.getByNKey(contains(MockUtils.PARCELLE))).thenReturn(Optional.empty());
        parcelle = this.instance.readDbParcelle(this.errorsReport, 2L, 1, cleanerValues,
                this.datasetDescriptor, this.requestProperties);
        Assert.assertNull(parcelle);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals("-La parcelle parcelle définie ligne 2 colonne 2 n'existe pas pour le site Site.\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of verifieSiteRepositoryEqualsSite method, of class AbstractProcessRecord.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testVerifieSiteRepositoryEqualsSite() throws Exception {
        // cas nominal
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        this.instance.verifieSiteRepositoryEqualsSite(this.m.versionFile, this.requestProperties);
        // site null
        Mockito.when(this.requestProperties.getSite()).thenReturn(null);
        try {
            this.instance.verifieSiteRepositoryEqualsSite(this.m.versionFile,
                    this.requestProperties);
            Assert.fail("pas d'exception");
        } catch (BusinessException e) {
            Assert.assertEquals(
                    "Le site de depot choisi \"Null\" ne  correspond pas au site déclaré dans l'en-tête \"agro-écosysteme/site\".",
                    e.getMessage());
        }
        // site bad site
        Mockito.when(this.requestProperties.getSite()).thenReturn(this.m.site);
        Mockito.when(this.m.site.getPath()).thenReturn("bad path", MockUtils.SITE_PATH, "bad path",
                MockUtils.SITE_PATH);
        try {
            this.instance.verifieSiteRepositoryEqualsSite(this.m.versionFile,
                    this.requestProperties);
            Assert.fail("pas d'exception");
        } catch (BusinessException e) {
            Assert.assertEquals(
                    "Le site de depot choisi \"bad path\" ne  correspond pas au site déclaré dans l'en-tête \"agro-écosysteme/site\".",
                    e.getMessage());
        }
    }


    /**
     * Test of getBoolean method, of class AbstractProcessRecordITK.
     */
    @Test
    public void testGetBoolean() {
        ErrorsReport errorsReport = new ErrorsReport();
        CleanerValues cleanerValues = new CleanerValues(new String[]{"true", org.apache.commons.lang.StringUtils.EMPTY, "badvalue"});
        // nominal
        boolean result = this.instance.getBoolean(errorsReport, 1L, 2, cleanerValues, this.datasetDescriptorACBB);
        Assert.assertTrue(result);
        // empty
        result = this.instance.getBoolean(errorsReport, 1L, 2, cleanerValues, this.datasetDescriptorACBB);
        Assert.assertFalse(result);
        // not boolean
        result = this.instance.getBoolean(errorsReport, 1L, 2, cleanerValues, this.datasetDescriptorACBB);
        Assert.assertFalse(errorsReport.hasErrors());
        Assert.assertFalse(result);
    }

    /**
     *
     */
    public class AbstractProcessRecordImpl extends AbstractProcessRecord {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
    }

}
