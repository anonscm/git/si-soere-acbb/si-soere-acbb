/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.*;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.modalite.Rotation;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;

/**
 * @author ptcherniati
 */
public class LineRecordTest {

    LineRecord instance;
    @Mock
    LineRecord line;
    @Mock
    List variableValues;
    @Mock
    ACBBVariableValue<? extends IValeurQualitative> variableValue;
    @Mock
    SemisEspece semisEspece;
    @Mock
    IValeurQualitative valeurQualitative;
    @Mock
    Tempo tempo;
    ITKMockUtils m = ITKMockUtils.getInstance();
    /**
     *
     */
    public LineRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new LineRecord(2L);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of copy method, of class LineRecord.
     */
    @Test
    public void testCopy() {
        this.instance = Mockito.spy(this.instance);
        LineRecord line = new LineRecord(3L);
        line.setDate(this.m.dateDebut);
        line.setObservation("observation");
        line.setRotationNumber(2);
        line.setAnneeNumber(20);
        line.setPeriodeNumber(1);
        line.setSuiviParcelle(this.m.suiviParcelle);
        line.setVersionDeTraitement(this.m.versionDeTraitement);
        line.setVariablesValues(this.variableValues);
        line.setTraitementProgramme(this.m.traitement);
        line.setParcelle(this.m.parcelle);
        line.setVersion(3);
        line.setTempo(this.tempo);
        line.setDateDebutDetraitementSurParcelle(this.m.dateFin);
        Mockito.doNothing().when(this.instance).setAttributesVariablesValues(this.variableValues);
        this.instance.copy(line);
        Assert.assertEquals(this.m.dateDebut, this.instance.getDate());
        Assert.assertEquals("observation", this.instance.getObservation());
        Assert.assertTrue(3L == this.instance.getOriginalLineNumber());
        Assert.assertTrue(2 == this.instance.getRotationNumber());
        Assert.assertTrue(20 == this.instance.getAnneeNumber());
        Assert.assertTrue(1L == this.instance.getPeriodeNumber());
        Assert.assertEquals(this.m.parcelle, this.instance.getParcelle());
        Assert.assertEquals(this.m.suiviParcelle, this.instance.getSuiviParcelle());
        Assert.assertEquals(this.m.versionDeTraitement, this.instance.getVersionDeTraitement());
        Assert.assertEquals(this.variableValues, this.instance.getVariablesValues());
        Assert.assertEquals(this.m.traitement, this.instance.getTraitementProgramme());
        Assert.assertTrue(3 == this.instance.getVersion());
        Assert.assertEquals(this.tempo, this.instance.getTempo());
        Assert.assertEquals(this.m.dateFin, this.instance.getDateDebutDetraitementSurParcelle());
    }

    /**
     * Test of getCouvertVegetal method, of class LineRecord.
     */
    @Test
    public void testGetCouvertVegetal() {
        SemisCouvertVegetal couvertVegetal = Mockito.mock(SemisCouvertVegetal.class);
        this.instance.setCouvertVegetal(couvertVegetal);
        SemisCouvertVegetal result = this.instance.getCouvertVegetal();
        Assert.assertEquals(couvertVegetal, result);
    }

    /**
     * Test of getEspece method, of class LineRecord.
     */
    @Test
    public void testGetEspece() {
        this.variableValues = Arrays.asList(this.variableValue, this.variableValue,
                this.variableValue);
        this.instance.setVariablesValues(this.variableValues);
        Mockito.when(this.variableValue.isQualitative()).thenReturn(Boolean.FALSE, Boolean.TRUE,
                Boolean.TRUE);
        Mockito.when(this.variableValue.getValeurQualitative()).thenReturn(this.valeurQualitative,
                this.semisEspece);
        SemisEspece result = this.instance.getEspece();
        Assert.assertNotNull(result);
        Mockito.verify(this.variableValue, Mockito.times(3)).getValeurQualitative();
    }

    /**
     * Test of getObjectifs method, of class LineRecord.
     */
    @Test
    public void testGetObjectifs() {
        SemisObjectif semisObjectif = Mockito.mock(SemisObjectif.class);
        this.variableValues = Arrays.asList(this.variableValue, this.variableValue,
                this.variableValue);
        this.instance.setAttributesVariablesValues(this.variableValues);
        Mockito.when(this.variableValue.isQualitative()).thenReturn(Boolean.TRUE);
        Mockito.when(this.variableValue.getValeurQualitative()).thenReturn(this.valeurQualitative,
                semisObjectif);
        List<SemisObjectif> result = this.instance.getObjectifs();
        Assert.assertTrue(2 == result.size());
        Assert.assertEquals(semisObjectif, result.get(0));
        Assert.assertEquals(semisObjectif, result.get(1));
    }

    /**
     * Test of getProfondeur method, of class LineRecord.
     */
    @Test
    public void testGetProfondeur() {
        this.variableValues = Arrays.asList(this.variableValue, this.variableValue,
                this.variableValue);
        this.instance.setAttributesVariablesValues(this.variableValues);
        Mockito.when(this.variableValue.isQualitative()).thenReturn(Boolean.FALSE);
        Mockito.when(this.variableValue.getDatatypeVariableUnite()).thenReturn(this.m.datatypeVariableUnite);
        Mockito.when(this.m.variable.getCode()).thenReturn("code", "code",
                Semis.ATTRIBUTE_JPA_PROFONDEUR);
        Mockito.when(this.variableValue.getValue()).thenReturn("20.2");
        Float result = this.instance.getProfondeur();
        Assert.assertTrue(20.2F == result);
    }

    /**
     * Test of getSemisRotationDescriptor method, of class LineRecord.
     */
    @Test
    public void testGetSemisRotationDescriptor() {
        RotationDescriptor rotationDescriptor = Mockito.mock(RotationDescriptor.class);
        this.instance.setRotationDescriptor(rotationDescriptor);
        RotationDescriptor result = this.instance.getSemisRotationDescriptor();
        Assert.assertEquals(rotationDescriptor, result);
    }

    /**
     * Test of getTreatmentAffichage method, of class LineRecord.
     */
    @Test
    public void testGetTreatmentAffichage() {
        String expResult = "T4";
        String result = this.instance.getTreatmentAffichage();
        Assert.assertEquals(org.apache.commons.lang.StringUtils.EMPTY, result);
        this.instance.setTempo(this.tempo);
        result = this.instance.getTreatmentAffichage();
        Assert.assertEquals(org.apache.commons.lang.StringUtils.EMPTY, result);
        Mockito.when(this.tempo.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
        Mockito.when(this.m.versionDeTraitement.getTraitementProgramme()).thenReturn(null);
        result = this.instance.getTreatmentAffichage();
        Assert.assertEquals(org.apache.commons.lang.StringUtils.EMPTY, result);
        Mockito.when(this.m.versionDeTraitement.getTraitementProgramme()).thenReturn(
                this.m.traitement);
        Mockito.when(this.m.traitement.getAffichage()).thenReturn(expResult);
        result = this.instance.getTreatmentAffichage();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVariete method, of class LineRecord.
     */
    @Test
    public void testGetVariete() {
        SemisVariete variete = Mockito.mock(SemisVariete.class);
        this.variableValues = Arrays.asList(this.variableValue);
        this.instance.setVariablesValues(this.variableValues);
        Mockito.when(this.variableValue.isQualitative()).thenReturn(Boolean.TRUE);
        Mockito.when(this.variableValue.getValeurQualitative()).thenReturn(this.semisEspece);
        SemisEspece espece = this.instance.getEspece();
        Mockito.when(this.variableValue.getValeurQualitative()).thenReturn(variete);
        Assert.assertNotNull(espece);
        List<ListeItineraire> varietesValues = new LinkedList();
        varietesValues.add(variete);
        Map<String, ListeItineraire> varietes = Mockito.mock(Map.class);
        Mockito.when(varietes.values()).thenReturn(varietesValues);
        Mockito.when(espece.getVarietes()).thenReturn(varietes);
        ListeItineraire result = this.instance.getVariete();
        Assert.assertEquals(variete, result);
    }

    /**
     * Test of isAnnualCrop method, of class LineRecord.
     */
    @Test
    public void testIsAnnualCrop() {
        this.instance = Mockito.spy(this.instance);
        boolean result = this.instance.isAnnualCrop();
        Assert.assertFalse(result);
        Mockito.doReturn(Boolean.TRUE).when(this.instance).isRotation();
        Mockito.doReturn(Boolean.FALSE).when(this.instance).isAnnuelle();
        result = this.instance.isAnnualCrop();
        Assert.assertFalse(result);
        Mockito.doReturn(Boolean.TRUE).when(this.instance).isAnnuelle();
        result = this.instance.isAnnualCrop();
        Assert.assertTrue(result);
    }

    /**
     * Test of isAnnuelle method, of class LineRecord.
     */
    @Test
    public void testIsAnnuelle() {
        this.instance.setAnnuelle(true);
        SemisAnnuelle semisAnnuelle = Mockito.mock(SemisAnnuelle.class);
        this.variableValues = Arrays.asList(this.variableValue);
        this.instance.setVariablesValues(this.variableValues);
        Mockito.when(this.variableValue.isQualitative()).thenReturn(Boolean.TRUE);
        Mockito.when(this.variableValue.getValeurQualitative()).thenReturn(semisAnnuelle);
        Mockito.when(semisAnnuelle.getValeur()).thenReturn("true");
        Boolean result = this.instance.isAnnuelle();
        Assert.assertTrue(result);
    }

    /**
     * Test of isDefinedPermanentGrassland method, of class LineRecord.
     */
    @Test
    public void testIsDefinedPermanentGrassland() {
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(false).when(this.instance).isRotation();
        boolean result = this.instance.isDefinedPermanentGrassland();
        Assert.assertTrue(result);
        Mockito.doReturn(true).when(this.instance).isRotation();
        result = this.instance.isDefinedPermanentGrassland();
        Assert.assertFalse(result);
    }

    /**
     * Test of isFirstLeyYear method, of class LineRecord.
     */
    @Test
    public void testIsFirstLeyYear() {
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(true).when(this.instance).isRotation();
        SemisCouvertVegetal couvertVegetal = Mockito.mock(SemisCouvertVegetal.class);
        RotationDescriptor rotationDescriptor = Mockito.mock(RotationDescriptor.class);
        Map<Integer, SemisCouvertVegetal> couverts = new HashMap();
        couverts.put(1, couvertVegetal);
        Mockito.when(rotationDescriptor.getCouverts()).thenReturn(couverts);
        Mockito.when(this.instance.getCouvertVegetal()).thenReturn(couvertVegetal);
        Mockito.when(this.instance.getSemisRotationDescriptor()).thenReturn(rotationDescriptor);
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        boolean result = this.instance.isFirstLeyYear();
        Assert.assertTrue(result);
        Mockito.when(this.instance.getAnneeNumber()).thenReturn(2);
        result = this.instance.isFirstLeyYear();
        Assert.assertFalse(result);
    }

    /**
     * Test of isGoodYearForRotation method, of class LineRecord.
     */
    @Test
    public void testIsGoodYearForRotation() {
        this.instance = Mockito.spy(this.instance);
        RotationDescriptor rotationDescriptor = Mockito.mock(RotationDescriptor.class);
        Map couverts = Mockito.mock(Map.class);
        Mockito.doReturn(false).when(this.instance).isTemporary();
        Mockito.doReturn(false).when(this.instance).isPermanentGrassland();
        Mockito.doReturn(3).when(this.instance).getAnneeNumber();
        boolean result = this.instance.isGoodYearForRotation();
        Assert.assertFalse(result);
        // permanent grassland
        Mockito.doReturn(false).when(this.instance).isTemporary();
        Mockito.doReturn(true).when(this.instance).isPermanentGrassland();
        Mockito.doReturn(3).when(this.instance).getAnneeNumber();
        result = this.instance.isGoodYearForRotation();
        Assert.assertTrue(result);
        // not good yer
        Mockito.doReturn(false).when(this.instance).isTemporary();
        Mockito.doReturn(false).when(this.instance).isPermanentGrassland();
        Mockito.doReturn(3).when(this.instance).getAnneeNumber();
        result = this.instance.isGoodYearForRotation();
        Assert.assertFalse(result);
        // temporary not good year
        Mockito.when(this.instance.getSemisRotationDescriptor()).thenReturn(rotationDescriptor);
        Mockito.when(rotationDescriptor.getCouverts()).thenReturn(couverts);
        Mockito.doReturn(true).when(this.instance).isTemporary();
        Mockito.doReturn(false).when(this.instance).isPermanentGrassland();
        Mockito.doReturn(3).when(this.instance).getAnneeNumber();
        result = this.instance.isGoodYearForRotation();
        Mockito.when(couverts.size()).thenReturn(2);
        Assert.assertFalse(result);
        // temporary good yer
        Mockito.doReturn(true).when(this.instance).isTemporary();
        Mockito.doReturn(false).when(this.instance).isPermanentGrassland();
        Mockito.doReturn(3).when(this.instance).getAnneeNumber();
        Mockito.when(couverts.size()).thenReturn(4);
        result = this.instance.isGoodYearForRotation();
        Assert.assertTrue(result);
    }

    /**
     * Test of isGoodYearForTemporaryGrassland method, of class LineRecord.
     */
    @Test
    public void testIsGoodYearForTemporaryGrassland() {
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isPositiveYear();
        boolean result = this.instance.isGoodYearForTemporaryGrassland();
        Assert.assertFalse(result);
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isPositiveYear();
        result = this.instance.isGoodYearForTemporaryGrassland();
        Assert.assertTrue(result);
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(true).when(this.instance).isPositiveYear();
        result = this.instance.isGoodYearForTemporaryGrassland();
        Assert.assertTrue(result);
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(true).when(this.instance).isPositiveYear();
        result = this.instance.isGoodYearForTemporaryGrassland();
        Assert.assertTrue(result);
    }

    /**
     * Test of isMainCrop method, of class LineRecord.
     */
    @Test
    public void testIsMainCrop() {
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isReplanting();
        Mockito.doReturn(false).when(this.instance).isFirstLeyYear();
        boolean result = this.instance.isMainCrop();
        Assert.assertTrue(result);
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isReplanting();
        Mockito.doReturn(true).when(this.instance).isFirstLeyYear();
        result = this.instance.isMainCrop();
        Assert.assertFalse(result);
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(true).when(this.instance).isReplanting();
        Mockito.doReturn(false).when(this.instance).isFirstLeyYear();
        result = this.instance.isMainCrop();
        Assert.assertFalse(result);
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(true).when(this.instance).isReplanting();
        Mockito.doReturn(true).when(this.instance).isFirstLeyYear();
        result = this.instance.isMainCrop();
        Assert.assertFalse(result);
    }

    /**
     * Test of isPermanentGrassland method, of class LineRecord.
     */
    @Test
    public void testIsPermanentGrassland() {
        this.instance = Mockito.spy(this.instance);
        RotationDescriptor rotationDescriptor = Mockito.mock(RotationDescriptor.class);
        Rotation rotation = Mockito.mock(Rotation.class);
        Mockito.when(rotationDescriptor.getRotation()).thenReturn(rotation);
        Mockito.doReturn(false).when(this.instance).isDefinedPermanentGrassland();
        Mockito.doReturn(rotationDescriptor).when(this.instance).getSemisRotationDescriptor();
        Mockito.doReturn(false).when(rotation).isInfiniteRepeatedCouvert();
        boolean result = this.instance.isPermanentGrassland();
        Assert.assertFalse(result);
        Mockito.doReturn(false).when(this.instance).isDefinedPermanentGrassland();
        Mockito.doReturn(true).when(rotation).isInfiniteRepeatedCouvert();
        result = this.instance.isPermanentGrassland();
        Assert.assertTrue(result);
        Mockito.doReturn(true).when(this.instance).isDefinedPermanentGrassland();
        Mockito.doReturn(false).when(rotation).isInfiniteRepeatedCouvert();
        result = this.instance.isPermanentGrassland();
        Assert.assertTrue(result);
        Mockito.doReturn(true).when(this.instance).isDefinedPermanentGrassland();
        Mockito.doReturn(true).when(rotation).isInfiniteRepeatedCouvert();
        result = this.instance.isPermanentGrassland();
        Assert.assertTrue(result);
    }

    /**
     * Test of isPositiveYear method, of class LineRecord.
     */
    @Test
    public void testIsPositiveYear() {
        this.instance = Mockito.spy(this.instance);
        Mockito.when(this.instance.getAnneeNumber()).thenReturn(0);
        boolean result = this.instance.isPositiveYear();
        Assert.assertFalse(result);
        Mockito.when(this.instance.getAnneeNumber()).thenReturn(1);
        result = this.instance.isPositiveYear();
        Assert.assertTrue(result);
        Mockito.when(this.instance.getAnneeNumber()).thenReturn(8);
        result = this.instance.isPositiveYear();
        Assert.assertTrue(result);
    }

    /**
     * Test of isReplanting method, of class LineRecord.
     */
    @Test
    public void testIsReplanting() {
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        Mockito.doReturn(true).when(this.instance).isFirstLeyYear();
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(0).when(this.instance).getPeriodeNumber();
        boolean result = this.instance.isReplanting();
        Assert.assertFalse(result);
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        Mockito.doReturn(true).when(this.instance).isFirstLeyYear();
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(0).when(this.instance).getPeriodeNumber();
        result = this.instance.isReplanting();
        Assert.assertFalse(result);
        // isContinuityOfTemporryGrassland
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        Mockito.doReturn(false).when(this.instance).isFirstLeyYear();
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(0).when(this.instance).getPeriodeNumber();
        result = this.instance.isReplanting();
        Assert.assertTrue(result);
        // isSecondPeriod
        Mockito.doReturn(true).when(this.instance).isAnnuelle();
        Mockito.doReturn(false).when(this.instance).isFirstLeyYear();
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(1).when(this.instance).getPeriodeNumber();
        result = this.instance.isReplanting();
        Assert.assertTrue(result);
        // not isSecondPeriod
        Mockito.doReturn(true).when(this.instance).isAnnuelle();
        Mockito.doReturn(false).when(this.instance).isFirstLeyYear();
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(0).when(this.instance).getPeriodeNumber();
        result = this.instance.isReplanting();
        Assert.assertFalse(result);
    }

    /**
     * Test of isRotation method, of class LineRecord.
     */
    @Test
    public void testIsRotation() {
        RotationDescriptor rotationDescriptor = Mockito.mock(RotationDescriptor.class);
        Rotation rotation = Mockito.mock(Rotation.class);
        this.instance = Mockito.spy(this.instance);
        // rotation not Annual for first layYear
        boolean result = this.instance.isRotation();
        Assert.assertFalse(result);
        Mockito.doReturn(rotationDescriptor).when(this.instance).getSemisRotationDescriptor();
        result = this.instance.isRotation();
        Assert.assertFalse(result);
        Mockito.when(rotationDescriptor.getRotation()).thenReturn(rotation);
        result = this.instance.isRotation();
        Assert.assertTrue(result);
    }

    /**
     * Test of isTemporary method, of class LineRecord.
     */
    @Test
    public void testIsTemporary() {
        this.instance = Mockito.spy(this.instance);
        Mockito.when(this.instance.isAnnualCrop()).thenReturn(false);
        Mockito.when(this.instance.isTemporaryGrassland()).thenReturn(false);
        boolean result = this.instance.isTemporary();
        Assert.assertFalse(result);
        Mockito.when(this.instance.isAnnualCrop()).thenReturn(true);
        Mockito.when(this.instance.isTemporaryGrassland()).thenReturn(false);
        result = this.instance.isTemporary();
        Assert.assertTrue(result);
        Mockito.when(this.instance.isAnnualCrop()).thenReturn(false);
        Mockito.when(this.instance.isTemporaryGrassland()).thenReturn(true);
        result = this.instance.isTemporary();
        Assert.assertTrue(result);
        Mockito.when(this.instance.isAnnualCrop()).thenReturn(true);
        Mockito.when(this.instance.isTemporaryGrassland()).thenReturn(true);
        result = this.instance.isTemporary();
        Assert.assertTrue(result);
    }

    /**
     * Test of isTemporaryGrassland method, of class LineRecord.
     */
    @Test
    public void testIsTemporaryGrassland() {
        this.instance = Mockito.spy(this.instance);
        RotationDescriptor rotationDescriptor = Mockito.mock(RotationDescriptor.class);
        Rotation rotation = Mockito.mock(Rotation.class);
        Mockito.when(this.instance.getSemisRotationDescriptor()).thenReturn(rotationDescriptor);
        Mockito.when(rotationDescriptor.getRotation()).thenReturn(rotation);
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        Mockito.when(rotation.isInfiniteRepeatedCouvert()).thenReturn(false);
        boolean result = this.instance.isTemporaryGrassland();
        Assert.assertFalse(result);
        Mockito.doReturn(true).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        Mockito.when(rotation.isInfiniteRepeatedCouvert()).thenReturn(false);
        result = this.instance.isTemporaryGrassland();
        Assert.assertTrue(result);
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(true).when(this.instance).isAnnuelle();
        Mockito.when(rotation.isInfiniteRepeatedCouvert()).thenReturn(false);
        result = this.instance.isTemporaryGrassland();
        Assert.assertFalse(result);
        Mockito.doReturn(false).when(this.instance).isRotation();
        Mockito.doReturn(false).when(this.instance).isAnnuelle();
        Mockito.when(rotation.isInfiniteRepeatedCouvert()).thenReturn(true);
        result = this.instance.isTemporaryGrassland();
        Assert.assertFalse(result);
    }

}
