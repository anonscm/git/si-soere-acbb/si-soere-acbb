/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ptcherniati
 */
public class TestHeadersITKTest {


    TestHeadersITK instance;
    BadsFormatsReport badsFormatsReport;
    ITKMockUtils m = ITKMockUtils.getInstance();
    String[] values = new String[]{"parcelle", "observation", "other"};
    @Mock
    Column columnParcelle;
    @Mock
    Column columnObservation;
    @Mock
    Column columnOther;
    List<Column> columnToParse;
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    Map<Integer, Column> valueColumns;
    @Mock
    List<Column> columns;
    /**
     *
     */
    public TestHeadersITKTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    public void initInstance(TestHeadersITK instance) {
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setSiteDAO(this.m.siteDAO);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new TestHeadersITK();
        this.initInstance(this.instance);
        this.badsFormatsReport = new BadsFormatsReport("error");
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.datasetDescriptor.getUndefinedColumn()).thenReturn(2);
        Mockito.when(this.columnParcelle.getName()).thenReturn("parcelle");
        Mockito.when(this.columnObservation.getName()).thenReturn("observation");
        Mockito.when(this.columnOther.getName()).thenReturn("other");
        Mockito.when(this.m.requestPropertiesITK.getValueColumns()).thenReturn(this.valueColumns);
        this.columnToParse = new LinkedList();
        this.columnToParse.add(this.columnParcelle);
        this.columnToParse.add(this.columnObservation);
        this.columnToParse.add(this.columnOther);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getColumns method, of class TestHeadersITK.
     */
    @Test
    public void testGetColumns() {
        // nominal
        long lineNumber = 8L;
        CleanerValues cleanerValues = new CleanerValues(this.values);
        ArgumentCaptor<Integer> indexValue = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Column> columnValue = ArgumentCaptor.forClass(Column.class);
        this.instance.getColumns(this.badsFormatsReport, lineNumber, this.m.requestPropertiesITK,
                this.values, cleanerValues, this.columnToParse);
        List<Integer> indexesExpected = Arrays.asList(0, 1, 2);
        Mockito.verify(this.valueColumns, Mockito.times(3)).put(indexValue.capture(),
                columnValue.capture());
        Assert.assertEquals(indexesExpected, indexValue.getAllValues());
        List<Column> columnsExpected = columnValue.getAllValues();
        Assert.assertEquals(this.columnParcelle, columnsExpected.get(0));
        Assert.assertEquals(this.columnObservation, columnsExpected.get(1));
        Assert.assertEquals(this.columnOther, columnsExpected.get(2));
        // with basd column
        cleanerValues = new CleanerValues(this.values);
        this.columnToParse.remove(this.columnOther);
        this.instance.getColumns(this.badsFormatsReport, lineNumber, this.m.requestPropertiesITK,
                this.values, cleanerValues, this.columnToParse);
        Assert.assertTrue(this.badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "error :- Ligne 8 : L'intitulé \"other\" de la colonne 3 est inconnu. ",
                this.badsFormatsReport.getMessages());
    }

    /**
     * Test of readLineHeader method, of class TestHeadersITK.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLineHeader() throws Exception {
        String str = "parcelle;observation;other\n";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(str.getBytes()), ';');
        long expResult = 1L;
        Mockito.when(this.datasetDescriptor.getColumns()).thenReturn(this.columnToParse);
        long result = this.instance.readLineHeader(this.badsFormatsReport, parser, 0L,
                this.datasetDescriptor, this.m.requestPropertiesITK);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setDatatypeName method, of class TestHeadersITK.
     */
    @Test
    public void testSetDatatypeName() {
        this.instance.setDatatypeName("datatypeName");
        Assert.assertEquals("datatypeName", this.instance.datatypeName);

    }

    /**
     * Test of testHeaders method, of class TestHeadersITK.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        this.instance = new TestHeaderITKInstance();
        Mockito.when(this.m.versionFile.getData()).thenReturn(org.apache.commons.lang.StringUtils.EMPTY.getBytes());
        String str = "parcelle;observation;other\n";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(str.getBytes()), ';');
        VersionFile versionFile = this.m.versionFile;
        String encoding = "UTF-8";
        long result = this.instance.testHeaders(parser, versionFile, this.m.requestPropertiesITK,
                encoding, this.badsFormatsReport, this.datasetDescriptor);
        Assert.assertEquals(11, result);
    }

    class TestHeaderITKInstance extends TestHeadersITK {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        String[] values;
        List<Column> columnToParse;

        TestHeaderITKInstance() {
            initInstance(this);
        }

        @Override
        protected long jumpLines(CSVParser parser, long lineNumber, int numberOfJumpedLines)
                throws IOException {
            return lineNumber + numberOfJumpedLines;
        }

        @Override
        protected long readBeginAndEndDates(VersionFile version,
                                            BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber,
                                            IRequestPropertiesACBB requestProperties) throws IOException {
            return lineNumber + 2;
        }

        @Override
        protected long readCommentaire(CSVParser parser, long lineNumber,
                                       IRequestPropertiesACBB requestProperties) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                    long lineNumber, String datatypeName) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readEmptyLine(BadsFormatsReport badsFormatsReport, CSVParser parser,
                                     long lineNumber) throws IOException {
            return lineNumber + 1;
        }

        @Override
        protected long readSite(VersionFile version, BadsFormatsReport badsFormatsReport,
                                CSVParser parser, long lineNumber, IRequestPropertiesACBB requestProperties)
                throws IOException, BusinessException {
            return lineNumber + 1;
        }
    }

}
