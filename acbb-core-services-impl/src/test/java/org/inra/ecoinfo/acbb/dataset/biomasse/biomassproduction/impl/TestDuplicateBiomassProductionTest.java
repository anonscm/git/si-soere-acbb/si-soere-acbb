/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.impl;

import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.junit.*;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class TestDuplicateBiomassProductionTest {

    TestDuplicateBiomassProduction instance;
    MockUtils m = MockUtils.getInstance();
    ErrorsReport errorsReport;
    /**
     *
     */
    public TestDuplicateBiomassProductionTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new TestDuplicateBiomassProduction();
        this.errorsReport = new ErrorsReport();
        this.instance.setErrorsReport(this.errorsReport);
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addLine method, of class TestDuplicateBiomassProduction.
     */
    @Test
    public void testAddLine_3args() {
        this.instance.addLine(MockUtils.PARCELLE, "nature", MockUtils.DATE_DEBUT, 1L);
        Assert.assertFalse(this.instance.hasError());
        this.instance.addLine(MockUtils.PARCELLE, "nature", MockUtils.DATE_DEBUT, 6L);
        Assert.assertTrue(this.instance.hasError());
        Assert.assertEquals(
                "-La ligne 6 a la même parcelle parcelle, la même nature de masse végétale nature et la même date 01/01/2012 que la ligne 1\n",
                this.errorsReport.getErrorsMessages());
    }

    /**
     * Test of addLine method, of class TestDuplicateBiomassProduction.
     */
    @Test
    public void testAddLine_4args() {
        String[] values = new String[]{MockUtils.PARCELLE, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, "nature", org.apache.commons.lang.StringUtils.EMPTY,
                MockUtils.DATE_DEBUT};
        this.instance.addLine(values, 1L, null, null);
        Assert.assertFalse(this.instance.hasError());
        values = new String[]{MockUtils.PARCELLE, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, org.apache.commons.lang.StringUtils.EMPTY, "nature", org.apache.commons.lang.StringUtils.EMPTY,
                MockUtils.DATE_DEBUT};
        this.instance.addLine(values, 6L, null, null);
        Assert.assertTrue(this.instance.hasError());
        Assert.assertEquals(
                "-La ligne 6 a la même parcelle parcelle, la même nature de masse végétale nature et la même date 01/01/2012 que la ligne 1\n",
                this.errorsReport.getErrorsMessages());
    }

}
