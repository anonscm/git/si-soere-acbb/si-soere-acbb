/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.ComparatorWithFirstStringThenAlpha;
import org.junit.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author tcherniatinsky
 */
public class ComparatorWithFirstStringThenAlphaTest {

    /**
     *
     */
    public ComparatorWithFirstStringThenAlphaTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class ComparatorWithFirstStringThenAlpha.
     */
    @Test
    public void testCompare() {
        //not first string
        String o1 = "c";
        String o2 = "chat";
        ComparatorWithFirstStringThenAlpha instance = new ComparatorWithFirstStringThenAlpha("chateau");
        int result = instance.compare(o1, o2);
        assertTrue(result < 0);
        //first string by left
        o1 = "chateau";
        o2 = "chat";
        result = instance.compare(o1, o2);
        assertTrue(result < 0);
        //first string by right
        o1 = "c";
        o2 = "chateau";
        result = instance.compare(o1, o2);
        assertTrue(result > 0);
        //sort list
        List<String> listToOrder = Arrays.asList("c", "b", "a", "chateau", "chat", "e");
        Collections.sort(listToOrder, instance);
        assertEquals("[chateau, a, b, c, chat, e]", listToOrder.toString());
        instance = new ComparatorWithFirstStringThenAlpha("e");
        Collections.sort(listToOrder, instance);
        assertEquals("[e, a, b, c, chat, chateau]", listToOrder.toString());
        instance = new ComparatorWithFirstStringThenAlpha("b");
        Collections.sort(listToOrder, instance);
        assertEquals("[b, a, c, chat, chateau, e]", listToOrder.toString());
        instance = new ComparatorWithFirstStringThenAlpha("f");
        Collections.sort(listToOrder, instance);
        assertEquals("[a, b, c, chat, chateau, e]", listToOrder.toString());
    }

}
