/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author ptcherniati
 */
public class AbstractLineRecordTest {


    private final Integer anneeNumber = 2;
    private final String observation = "observation";
    private final long originalLineNumber = 12L;
    private final Integer periodeNumber = 3;
    private final Integer rotationNumber = 4;
    MockUtils m = MockUtils.getInstance();
    AbstractLineRecord instance;
    private LocalDate date;
    private LocalDate dateDebutTraitement;
    @Mock
    private AbstractIntervention intervention;
    @Mock
    private ACBBVariableValue<ListeVegetation> vv1;
    @Mock
    private ACBBVariableValue<ListeVegetation> vv2;
    @Mock
    private InterventionType typeIntervention;
    @Mock
    private List variablesValues;
    /**
     *
     */
    public AbstractLineRecordTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractLineRecordImpl(0);
        date = LocalDate.of(2012, Month.JANUARY, 1);
        dateDebutTraitement = LocalDate.of(2011, Month.JANUARY, 1);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class AbstractLineRecord.
     */
    @Test
    public void testCompareTo() {
        AbstractLineRecord obj = new AbstractLineRecordImpl(0L);
        int result = this.instance.compareTo(obj);
        Assert.assertEquals(0, result);
        obj = new AbstractLineRecordImpl(11L);
        result = this.instance.compareTo(obj);
        Assert.assertEquals(1, result);
        result = obj.compareTo(this.instance);
        Assert.assertEquals(-1, result);
    }

    /**
     * Test of copy method, of class AbstractLineRecord.
     */
    @Test
    public void testCopy() {
        this.instance.setAnneeNumber(this.anneeNumber);
        this.instance.setDate(this.date);
        this.instance.setDateDebuttraitementSurParcelle(this.dateDebutTraitement);
        this.instance.setIntervention(this.intervention);
        this.instance.setObservation(this.observation);
        this.instance.setOriginalLineNumber(this.originalLineNumber);
        this.instance.setParcelle(this.m.parcelle);
        this.instance.setPeriodeNumber(this.periodeNumber);
        this.instance.setRotationNumber(this.rotationNumber);
        this.instance.setSuiviParcelle(this.m.suiviParcelle);
        this.instance.setTraitementProgramme(this.m.traitement);
        this.instance.setTypeIntervention(this.typeIntervention);
        this.instance.setVariablesValues(this.variablesValues);
        this.instance.setVersionDeTraitement(this.m.versionDeTraitement);
        AbstractLineRecord copy = new AbstractLineRecordImpl(2L);
        Assert.assertTrue(2L == copy.getOriginalLineNumber());
        copy.copy(this.instance);
        Assert.assertEquals(this.anneeNumber, copy.getAnneeNumber());
        Assert.assertEquals(this.date, copy.getDate());
        Assert.assertEquals(this.dateDebutTraitement, copy.getDateDebuttraitementSurParcelle());
        Assert.assertEquals(this.intervention, copy.getIntervention());
        Assert.assertEquals(this.observation, copy.getObservation());
        Assert.assertTrue(this.originalLineNumber == copy.getOriginalLineNumber());
        Assert.assertEquals(this.m.parcelle, copy.getParcelle());
        Assert.assertEquals(this.periodeNumber, copy.getPeriodeNumber());
        Assert.assertEquals(this.m.traitement, copy.getTraitementProgramme());
        Assert.assertEquals(this.typeIntervention, copy.getTypeIntervention());
        Assert.assertEquals(this.variablesValues, copy.getVariablesValues());
        Assert.assertEquals(this.m.versionDeTraitement, copy.getVersionDeTraitement());
    }

    /**
     * Test of equals method, of class AbstractLineRecord.
     */
    @Test
    public void testEquals() {
        Object obj = new AbstractLineRecordImpl(0L);
        boolean result = this.instance.equals(obj);
        Assert.assertTrue(result);
        obj = new AbstractLineRecordImpl(2L);
        result = this.instance.equals(obj);
        Assert.assertFalse(result);
        result = this.instance.equals("s");
        Assert.assertFalse(result);
    }

    /**
     * Test of getAnneeNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetAnneeNumber() {
        Integer expResult = 32;
        this.instance.setAnneeNumber(expResult);
        Integer result = this.instance.getAnneeNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class AbstractLineRecord.
     */
    @Test
    public void testGetDate() {
        this.instance.setDate(this.m.dateDebut);
        LocalDate result = this.instance.getDate();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getDateDebuttraitementSurParcelle method, of class
     * AbstractLineRecord.
     */
    @Test
    public void testGetDateDebuttraitementSurParcelle() {
        this.instance.setDateDebuttraitementSurParcelle(this.m.dateDebut);
        LocalDate result = this.instance.getDateDebuttraitementSurParcelle();
        Assert.assertEquals(this.m.dateDebut, result);
    }

    /**
     * Test of getIntervention method, of class AbstractLineRecord.
     */
    @Test
    public void testGetIntervention() {
        this.instance.setIntervention(this.intervention);
        AbstractIntervention result = this.instance.getIntervention();
        Assert.assertEquals(this.intervention, result);
    }

    /**
     * Test of getObservation method, of class AbstractLineRecord.
     */
    @Test
    public void testGetObservation() {
        String expResult = "observation";
        this.instance.setObservation(expResult);
        String result = this.instance.getObservation();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getOriginalLineNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetOriginalLineNumber() {
        long expResult = 133L;
        this.instance.setOriginalLineNumber(expResult);
        long result = this.instance.getOriginalLineNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getParcelle method, of class AbstractLineRecord.
     */
    @Test
    public void testGetParcelle() {
        this.instance.setParcelle(this.m.parcelle);
        Parcelle result = this.instance.getParcelle();
        Assert.assertEquals(this.m.parcelle, result);
    }

    /**
     * Test of getPeriodeNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetPeriodeNumber() {
        Integer expResult = 22;
        this.instance.setPeriodeNumber(expResult);
        Integer result = this.instance.getPeriodeNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getRotationNumber method, of class AbstractLineRecord.
     */
    @Test
    public void testGetRotationNumber() {
        Integer expResult = 24;
        this.instance.setRotationNumber(expResult);
        Integer result = this.instance.getRotationNumber();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractLineRecord.
     */
    @Test
    public void testGetSuiviParcelle() {
        this.instance.setSuiviParcelle(this.m.suiviParcelle);
        SuiviParcelle result = this.instance.getSuiviParcelle();
        Assert.assertEquals(this.m.suiviParcelle, result);
    }

    /**
     * Test of getTraitementProgramme method, of class AbstractLineRecord.
     */
    @Test
    public void testGetTraitementProgramme() {
        this.instance.setTraitementProgramme(this.m.traitement);
        TraitementProgramme result = this.instance.getTraitementProgramme();
        Assert.assertEquals(this.m.traitement, result);
    }

    /**
     * Test of getTypeIntervention method, of class AbstractLineRecord.
     */
    @Test
    public void testGetTypeIntervention() {
        this.instance.setTypeIntervention(this.typeIntervention);
        InterventionType result = this.instance.getTypeIntervention();
        Assert.assertEquals(this.typeIntervention, result);
    }

    /**
     * Test of getVariablesValues method, of class AbstractLineRecord.
     */
    @Test
    public void testGetVariablesValues() {
        List<ACBBVariableValue> expResult = Arrays.asList(new ACBBVariableValue[]{this.vv1,
                this.vv2});
        this.instance.setVariablesValues(expResult);
        List<ACBBVariableValue> result = this.instance.getVariablesValues();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVersionDeTraitement method, of class AbstractLineRecord.
     */
    @Test
    public void testGetVersionDeTraitement() {
        this.instance.setVersionDeTraitement(this.m.versionDeTraitement);
        VersionDeTraitement result = this.instance.getVersionDeTraitement();
        Assert.assertEquals(this.m.versionDeTraitement, result);
    }

    /**
     * Test of getNatureCouvert method, of class AbstractLineRecord.
     */
    @Test
    public void testGetNatureCouvert() {
        final String nature = "nature du couvert";
        instance.setNatureCouvert(nature);
        String result = instance.getNatureCouvert();
        assertEquals(nature, result);
    }

    /**
     *
     */
    public class AbstractLineRecordImpl extends AbstractLineRecord<AbstractLineRecordImpl, AbstractVariableValueBiomasse> {

        /**
         * @param lineNumber
         */
        public AbstractLineRecordImpl(long lineNumber) {
            super(lineNumber);
        }
    }

}
