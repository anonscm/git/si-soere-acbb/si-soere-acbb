/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.test.utils.BiomassMockUtils;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class RecorderTest {

    BiomassMockUtils m = BiomassMockUtils.getInstance();
    Recorder instance;
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    IMethodeAnalyseDAO methodeAnalyseDAO;
    @Mock
    MethodeAnalyse methodeAnalyse;
    @Mock
    MethodeAnalyseComposant methodeAnalyseComposant;
    @Mock
    MethodeAnalyseComposant composant;
    @Mock
    Properties propertiesLibelleEN;
    @Mock
    Properties propertiesDefinitionEN;
    List<MethodeAnalyseComposant> composants = new LinkedList();
    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new Recorder();
        this.instance.setListeVegetationDAO(this.listeVegetationDAO);
        this.instance.setMethodeAnalyseBiomasseDAO(this.methodeAnalyseDAO);
        this.instance.setMethodeDAO(this.methodeAnalyseDAO);
        this.composants.add(this.composant);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        Mockito.when(this.methodeAnalyse.getComposants()).thenReturn(this.composants);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "104";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()));
        Mockito.when(this.methodeAnalyseDAO.getByNumberId(104L)).thenReturn(Optional.of(this.methodeAnalyse));
        this.instance.deleteRecord(parser, null, "UTF-8");
        // nominal
        Mockito.verify(this.methodeAnalyseDAO).getByNumberId(104L);
        Mockito.verify(this.methodeAnalyseDAO).remove(this.methodeAnalyse);
        Mockito.verify(this.methodeAnalyseDAO).saveOrUpdate(this.methodeAnalyse);
        Assert.assertTrue(0 == this.composants.size());
        // persitence exception
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()));
        Mockito.doThrow(new PersistenceException("erreur")).when(this.methodeAnalyseDAO)
                .saveOrUpdate(this.methodeAnalyse);
        try {
            this.instance.deleteRecord(parser, null, "UTF-8");
            Assert.fail("no exception");
        } catch (BusinessException e) {
            Assert.assertEquals("erreur", e.getMessage());
        }
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        List<MethodeAnalyse> methodes = new LinkedList();
        Mockito.when(this.methodeAnalyseDAO.getAll()).thenReturn(methodes);
        List<MethodeAnalyse> result = this.instance.getAllElements();
        Assert.assertEquals(methodes, result);
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        this.methodeAnalyse = new MethodeAnalyse("libellé", "définition", "equationQualitePrediction", 102L, this.composants);
        this.testInitModelGridMetadata();
        Mockito.when(this.composant.getValeur()).thenReturn("composant");
        Mockito.when(this.propertiesLibelleEN.getProperty("libellé", "libellé")).thenReturn(
                "libellé_en");
        Mockito.when(this.propertiesDefinitionEN.getProperty("définition", "définition"))
                .thenReturn("définition_en");
        LineModelGridMetadata result = this.instance
                .getNewLineModelGridMetadata(this.methodeAnalyse);
        Assert.assertTrue(7 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("102", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("libellé", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("libellé_en", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("définition", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("définition_en", result.getColumnModelGridMetadataAt(4).getValue());
        Assert.assertEquals("equationQualitePrediction", result.getColumnModelGridMetadataAt(5).getValue());
        Assert.assertEquals("composant", result.getColumnModelGridMetadataAt(6).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     */
    public void testInitModelGridMetadata() {
        this.instance = Mockito.spy(this.instance);
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        Mockito.when(
                MockUtils.localizationManager.newProperties(this.instance.getMethodeEntityName(),
                        this.instance.getMethodeLibelleName(), Locale.ENGLISH)).thenReturn(
                this.propertiesLibelleEN);
        Mockito.when(
                MockUtils.localizationManager.newProperties(this.instance.getMethodeEntityName(),
                        this.instance.getMethodeDefinitionName(), Locale.ENGLISH)).thenReturn(
                this.propertiesDefinitionEN);
        this.instance.initModelGridMetadata();
        Mockito.verify(MockUtils.localizationManager).newProperties(
                this.instance.getMethodeEntityName(), this.instance.getMethodeLibelleName(),
                Locale.ENGLISH);
        Mockito.verify(MockUtils.localizationManager).newProperties(
                this.instance.getMethodeEntityName(), this.instance.getMethodeDefinitionName(),
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String text = "identifiant;libellé_fr;libellé_en;définition_fr;définition_en;équation et qualité prédiction;composante\n"
                + "101;Dumas_C_N;Dumas_C_N;Dumas_C_N;Dumas_C_N;v1;C, N";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        String encoding = "UTF-8";
        when(methodeAnalyseDAO.getByNKey(any())).thenReturn(Optional.empty());
        Mockito.when(
                this.listeVegetationDAO.getByNKey(MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS, "v1"))
                .thenReturn(Optional.of(this.methodeAnalyseComposant));
        Mockito.when(
                this.listeVegetationDAO.getByNKey(MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS, "C"))
                .thenReturn(Optional.of(this.methodeAnalyseComposant));
        Mockito.when(
                this.listeVegetationDAO.getByNKey(MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS, "N"))
                .thenReturn(Optional.of(this.methodeAnalyseComposant));

        this.instance.processRecord(parser, null, encoding);
        ArgumentCaptor<MethodeAnalyse> ma = ArgumentCaptor.forClass(MethodeAnalyse.class);
        Mockito.verify(this.methodeAnalyseDAO).saveOrUpdate(ma.capture());
        this.methodeAnalyse = ma.getValue();
        Assert.assertEquals("Dumas_C_N", this.methodeAnalyse.getLibelle());
        Assert.assertEquals("Dumas_C_N", this.methodeAnalyse.getDefinition());
        Assert.assertEquals(this.methodeAnalyseComposant, this.methodeAnalyse.getComposants().get(0));
        Assert.assertTrue(101 == this.methodeAnalyse.getNumberId());
        // with error
        text = "identifiant;libellé_fr;libellé_en;définition_fr;définition_en;équation et qualité prédiction;composante\n"
                + "101;Dumas_C_N;Dumas_C_N;Dumas_C_N;Dumas_C_N;v2;C, N";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        try {
            this.instance.processRecord(parser, null, encoding);
        } catch (BusinessException e) {
            Assert.assertEquals(
                    ".Ligne 1 , colonne 5, l'élément v2 n'existe pas dans la base de données.\n"
                            + ".Vous devez fournir une liste d'éléments séparées par des virgules ligne 1, colonne 5.\n",
                    e.getMessage());
        }
    }

}
