/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.itk.semis.impl;

import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.junit.*;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * @author ptcherniati
 */
@Ignore
public class SemisBuildOutputDisplayTest {

    /**
     *
     */
    public SemisBuildOutputDisplayTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getBundleSourcePath method, of class SemisBuildOutputDisplay.
     */
    @Test
    public void testGetBundleSourcePath() {
        SemisBuildOutputDisplay instance = new SemisBuildOutputDisplay("");
        String expResult = org.apache.commons.lang.StringUtils.EMPTY;
        String result = instance.getBundleSourcePath();
        Assert.assertEquals(expResult, result);

        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of getOutPutHelper method, of class SemisBuildOutputDisplay.
     */
    @Test
    public void testGetOutPutHelper() {
        DatesFormParamVO datesYearsContinuousFormParamVO = null;
        List<Variable> selectedSemisVariables = null;
        Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts = null;
        PrintStream rawDataPrintStream = null;
        SemisBuildOutputDisplay instance = new SemisBuildOutputDisplay("");
        AbstractITKOutputBuilder.OutputHelper expResult = null;
        AbstractITKOutputBuilder.OutputHelper result = instance.getOutPutHelper(
                datesYearsContinuousFormParamVO, selectedSemisVariables, availablesCouverts,
                rawDataPrintStream);
        Assert.assertEquals(expResult, result);

        Assert.fail("The test case is a prototype.");
    }

}
