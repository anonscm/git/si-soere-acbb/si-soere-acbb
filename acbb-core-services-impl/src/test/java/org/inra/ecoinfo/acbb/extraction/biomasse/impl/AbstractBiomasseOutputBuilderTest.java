/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.biomasse.IMesureBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKDatasetManager;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.junit.*;

import java.io.File;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
public class AbstractBiomasseOutputBuilderTest {

    /**
     *
     */
    public AbstractBiomasseOutputBuilderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * @throws ParseException
     */
    @Test
    public void test1() throws ParseException {
        Float floatValue1 = Float.valueOf("4230.226564");
        System.out.println(NumberFormat.getNumberInstance(Locale.FRANCE).parse("4230,226564"));
        System.out.println(NumberFormat.getNumberInstance(Locale.ENGLISH).parse("42,30.226564"));
        System.out.println(new DecimalFormat("00.###E00").parse("4,256E-01"));
        Float floatValue2 = Float.valueOf("-4");
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRANCE);
        decimalFormat.setMaximumFractionDigits(2);
        System.out.println(decimalFormat.format(floatValue1));
        System.out.println(decimalFormat.format(floatValue2));
        System.out.println(decimalFormat.toLocalizedPattern());
        decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        decimalFormat.setMaximumFractionDigits(2);
        System.out.println(decimalFormat.format(floatValue1));
        System.out.println(decimalFormat.format(floatValue2));
        System.out.println(decimalFormat.toLocalizedPattern());
        System.out.println(String.format(Locale.FRANCE, "String format : %,.3f", floatValue1));
        System.out.println(String.format(Locale.FRANCE, "String format : %,.3f", floatValue2));
        System.out.println(String.format(Locale.ENGLISH, "String format : %,.3f", floatValue1));
    }

    /**
     * Test of getFirstVariable method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetFirstVariable() {
        AbstractBiomasseOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getFirstVariable();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addGenericColumns method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testAddGenericColumns() {
        StringBuilder stringBuilder1 = null;
        StringBuilder stringBuilder2 = null;
        StringBuilder stringBuilder3 = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.addGenericColumns(stringBuilder1, stringBuilder2, stringBuilder3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addObservation method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testAddObservation() {
        StringBuilder stringBuilder1 = null;
        StringBuilder stringBuilder2 = null;
        StringBuilder stringBuilder3 = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.addObservation(stringBuilder1, stringBuilder2, stringBuilder3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addSpecificColumns method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testAddSpecificColumns() {
        StringBuilder stringBuilder1 = null;
        StringBuilder stringBuilder2 = null;
        StringBuilder stringBuilder3 = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.addSpecificColumns(stringBuilder1, stringBuilder2, stringBuilder3);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildBody method, of class AbstractBiomasseOutputBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildBody() throws Exception {
        String headers = "";
        Map<String, List> resultsDatasMap = null;
        Map<String, Object> requestMetadatasMap = null;
        AbstractBiomasseOutputBuilder instance = null;
        Map<String, File> expResult = null;
        Map<String, File> result = instance.buildBody(headers, resultsDatasMap, requestMetadatasMap);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildHeader method, of class AbstractBiomasseOutputBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildHeader() throws Exception {
        Map<String, Object> requestMetadatasMap = null;
        AbstractBiomasseOutputBuilder instance = null;
        String expResult = "";
        String result = instance.buildHeader(requestMetadatasMap);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildOutput method, of class AbstractBiomasseOutputBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildOutput() throws Exception {
        IParameter parameters = null;
        AbstractBiomasseOutputBuilder instance = null;
        RObuildZipOutputStream expResult = null;
        RObuildZipOutputStream result = instance.buildOutput(parameters);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBundleSourcePath method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetBundleSourcePath() {
        AbstractBiomasseOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getBundleSourcePath();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getColumnPatterns method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetColumnPatterns() {
        AbstractBiomasseOutputBuilder instance = null;
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getColumnPatterns();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtractCode method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetExtractCode() {
        AbstractBiomasseOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getExtractCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOutPutHelper method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetOutPutHelper() {
        DatesFormParamVO datesYearsContinuousFormParamVO = null;
        List<Variable> selectedWsolVariables = null;
        Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts = null;
        PrintStream rawDataPrintStream = null;
        AbstractBiomasseOutputBuilder instance = null;
        AbstractBiomasseOutputBuilder.OutputHelper expResult = null;
        AbstractBiomasseOutputBuilder.OutputHelper result = instance.getOutPutHelper(datesYearsContinuousFormParamVO, selectedWsolVariables, availablesCouverts, rawDataPrintStream);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getResultCode method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetResultCode() {
        AbstractBiomasseOutputBuilder instance = null;
        String expResult = "";
        String result = instance.getResultCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetSuiviParcelle() {
        Parcelle parcelle = null;
        LocalDate date = null;
        AbstractBiomasseOutputBuilder instance = null;
        SuiviParcelle expResult = null;
        SuiviParcelle result = (SuiviParcelle) instance.getSuiviParcelle(parcelle, date).orElse(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getValueColumns method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetValueColumns() {
        AbstractBiomasseOutputBuilder instance = null;
        List<ListeVegetation> expResult = null;
        List<ListeVegetation> result = instance.getValueColumns();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setColumnPatterns method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetColumnPatterns() {
        Map<String, String> columnPatterns = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setColumnPatterns(columnPatterns);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatasetDescriptor method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetDatasetDescriptor() {
        DatasetDescriptor datasetDescriptor = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setDatasetDescriptor(datasetDescriptor);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatatypeVariableUniteACBBDAO method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetDatatypeVariableUniteACBBDAO() {
        IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setDatatypeVariableUniteACBBDAO(datatypeVariableUniteDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExtractCode method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetExtractCode() {
        String extractCode = "";
        AbstractBiomasseOutputBuilder instance = null;
        instance.setExtractCode(extractCode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setItkDatasetManager method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetItkDatasetManager() {
        IITKDatasetManager itkDatasetManager = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setItkDatasetManager(itkDatasetManager);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListeVegetationDAO method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetListeVegetationDAO() {
        IListeVegetationDAO listeBioassedAO = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setListeVegetationDAO(listeBioassedAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSuiviParcelleDAO method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetSuiviParcelleDAO() {
        ISuiviParcelleDAO suiviParcelleDAO = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setSuiviParcelleDAO(suiviParcelleDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVariableDAO method, of class AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testSetVariableDAO() {
        IVariableACBBDAO variableDAO = null;
        AbstractBiomasseOutputBuilder instance = null;
        instance.setVariableDAO(variableDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    class AbstractBiomasseImpl extends AbstractBiomasse {

    }

    class ValeurBiomasseImpl extends ValeurBiomasse {

        @Override
        public int compareTo(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    class MesureBiomasseImpl implements IMesureBiomasse<AbstractBiomasseImpl, ValeurBiomasseImpl> {

        @Override
        public AbstractBiomasseImpl getSequence() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<ValeurBiomasseImpl> getValeurs() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /**
     *
     */
    public class AbstractBiomasseOutputBuilderImpl extends AbstractBiomasseOutputBuilder<AbstractBiomasseImpl, ValeurBiomasseImpl, MesureBiomasseImpl> {

        /**
         *
         */
        public AbstractBiomasseOutputBuilderImpl() {
            super("");
        }

        @Override
        public String getFirstVariable() {
            return "";
        }

        @Override
        public String getBundleSourcePath() {
            return "";
        }

        @Override
        public OutputHelper getOutPutHelper(DatesFormParamVO datesYearsContinuousFormParamVO, List<Variable> selectedWsolVariables, Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> availablesCouverts, PrintStream rawDataPrintStream) {
            return null;
        }
    }

}
