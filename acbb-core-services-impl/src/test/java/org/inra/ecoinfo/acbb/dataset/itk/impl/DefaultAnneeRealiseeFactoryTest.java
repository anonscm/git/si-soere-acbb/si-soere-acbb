/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.impl;

import org.inra.ecoinfo.acbb.dataset.itk.IAnneeRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.ILineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.IPeriodeRealiseeFactory;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class DefaultAnneeRealiseeFactoryTest {

    DefaultAnneeRealiseeFactory<ILineRecord> instance = new DefaultAnneeRealiseeFactory();
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    IAnneeRealiseeDAO anneeRealiseeDAO;
    @Mock
    DefaultPeriodeRealiseeFactory periodeRealiseeFactory;
    @Mock
    ILineRecord line;
    @Mock
    Tempo tempo;
    @Mock
    AnneeRealisee anneeRealisee;
    @Mock
    VersionTraitementRealisee versionTraitementRealisee;
    /**
     *
     */
    public DefaultAnneeRealiseeFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance.setAnneeRealiseeDAO(this.anneeRealiseeDAO);
        this.instance.setPeriodeRealiseeFactory(this.periodeRealiseeFactory);
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.line.getRotationNumber()).thenReturn(1);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(2);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(3);
        when(anneeRealiseeDAO.getByNKey(any(), anyInt())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getOrCreateAnneeRealisee method, of class DefaultAnneeRealiseeFactory.
     */
    @Test
    public void testGetOrCreateAnneeRealisee() {
        ErrorsReport errorsReport = new ErrorsReport();
        InfoReport infoReport = new InfoReport();
        // no tempo
        AnneeRealisee result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport,
                infoReport);
        Assert.assertNull(result);
        // with annee realisee
        Mockito.doReturn(this.anneeRealisee).when(this.tempo).getAnneeRealisee();
        result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Assert.assertEquals(this.anneeRealisee, result);
        // With db annee realisee
        Mockito.doReturn(null).when(this.tempo).getAnneeRealisee();
        Mockito.doReturn(this.versionTraitementRealisee).when(tempo).getVersionTraitementRealisee();
        Mockito.doReturn(Optional.of(this.anneeRealisee)).when(this.anneeRealiseeDAO)
                .getByNKey(this.versionTraitementRealisee, 2);
        result = this.instance.getOrCreateAnneeRealisee(this.line, errorsReport, infoReport);
        Assert.assertEquals(this.anneeRealisee, result);
    }

    /**
     * Test of getPeriodeRealiseeFactory method, of class DefaultAnneeRealiseeFactory.
     */
    @Test
    public void testGetPeriodeRealiseeFactory() {
        IPeriodeRealiseeFactory result = this.instance.getPeriodeRealiseeFactory();
        Assert.assertEquals(this.periodeRealiseeFactory, result);
    }

}
