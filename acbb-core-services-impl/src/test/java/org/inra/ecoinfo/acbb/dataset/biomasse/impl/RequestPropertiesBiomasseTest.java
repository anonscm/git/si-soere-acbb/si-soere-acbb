/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.impl;

import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;
import java.util.SortedMap;

/**
 * @author ptcherniati
 */
public class RequestPropertiesBiomasseTest {


    MockUtils m = MockUtils.getInstance();
    RequestPropertiesBiomasse instance;
    @Mock
    ITestDuplicates testDuplicates;
    @Mock
    Map<Integer, Column> valuesColumns;
    /**
     *
     */
    public RequestPropertiesBiomasseTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new RequestPropertiesBiomasseImpl();
        this.instance.setLocalizationManager(MockUtils.localizationManager);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getValueColumns method, of class RequestPropertiesBiomasse.
     */
    @Test
    public void testGetValueColumns() {
        this.instance.valueColumns = this.valuesColumns;
        Map<Integer, Column> result = this.instance.getValueColumns();
        Assert.assertEquals(this.valuesColumns, result);
    }

    /**
     * Test of setDateDeFin method, of class RequestPropertiesBiomasse.
     */
    @Test
    public void testSetDateDeFin() {
        this.instance.setDateDeDebut(this.m.dateTimeDebut);
        this.instance.setDateDeFin(this.m.dateTimeFin);
        Assert.assertEquals(this.m.dateTimeFin, this.instance.getDateDeFin());
        SortedMap<String, Boolean> dates = this.instance.getDates();
        Assert.assertTrue(0 == dates.size());
        Assert.assertEquals(this.m.dateTimeDebut, this.instance.getDateDeDebut());
        Assert.assertEquals(this.m.dateTimeFin, this.instance.getDateDeFin());
    }

    /**
     *
     */
    public class RequestPropertiesBiomasseImpl extends RequestPropertiesBiomasse {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        /**
         *
         */
        public String nomDeFichier = m.dataset
                .buildDownloadFilename(m.datasetConfiguration);

        /**
         * @param version
         * @return
         */
        @Override
        public String getNomDeFichier(VersionFile version) {
            return this.nomDeFichier;
        }

        /**
         * @return
         */
        @Override
        public ITestDuplicates getTestDuplicates() {
            return testDuplicates;
        }
    }

}
