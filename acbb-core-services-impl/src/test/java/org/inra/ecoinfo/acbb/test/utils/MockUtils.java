/*
 *
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.test.utils;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.IAgroEcosystemeDAO;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.IDatatypeVariableUniteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.IListeACBBDAO;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.parcelle.IParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.ISuiviParcelleDAO;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.IVersionDeTraitementDAO;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class MockUtils {

    private static final String UTILISATEUR = "utilisateur";
    /**
     *
     */
    public static String DATE_DEBUT = "01/01/2012";
    /**
     *
     */
    public static String DATE_FIN = "31/12/2012";
    /**
     *
     */
    public static String TIME_DEBUT = "00:00:00";
    /**
     *
     */
    public static String TIME_FIN = "00:00:00";
    /**
     *
     */
    public static String TIME_FIN_23_30 = "23:30:00";
    /**
     *
     */
    public static String DATE_DEBUT_COMPLETE = "01/01/2012 00:00:00";
    /**
     *
     */
    public static String DATE_FIN_COMPLETE = "31/12/2012 00:00:00";
    /**
     *
     */
    public static String PARCELLE = "parcelle";
    /**
     *
     */
    public static String PARCELLE_NOM = "Parcelle";
    /**
     *
     */
    public static String AGROECOSYSTEM = "agro-écosysteme";
    /**
     *
     */
    public static String SITE_CODE = "site";
    /**
     *
     */
    public static String SITE_NOM = "Site";
    /**
     *
     */
    public static Long SITE_ID = 1L;
    /**
     *
     */
    public static String THEME = "theme";
    /**
     *
     */
    public static String DATATYPE = "datatype";
    /**
     *
     */
    public static String DATATYPE_NOM = "Datatype";
    /**
     *
     */
    public static String VARIABLE_CODE = "variablecode";
    /**
     *
     */
    public static String VARIABLE_NOM = "variableNom";
    /**
     *
     */
    public static String VARIABLE_AFFICHAGE = "variableAffichage";
    /**
     *
     */
    public static String UNITE = "unite";
    /**
     *
     */
    public static String TRAITEMENT = "traitement";
    /**
     *
     */
    public static String TRAITEMENT_AFFICHAGE = "Traitement";
    /**
     *
     */
    public static String SITE_PATH = String.format(
            "%s/%s",
            MockUtils.AGROECOSYSTEM,
            MockUtils.SITE_CODE);
    /**
     *
     */
    public static ILocalizationManager localizationManager;
    /**
     *
     */
    @Mock(name = "MockUtil_versionFile")
    public VersionFile versionFile;
    /**
     *
     */
    @Mock(name = "MockUtils_versionFileDAO")
    public IVersionFileDAO versionFileDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_dataset")
    public Dataset dataset;
    /**
     *
     */
    @Mock(name = "MockUtils_dataTypeNode")
    public INode dataTypeNode;
    /**
     *
     */
    @Mock(name = "MockUtils_dataTypeRealNode")
    public RealNode dataTypeRealNode;
    /**
     *
     */
    @Mock(name = "MockUtils_ParcelleNode")
    public INode ParcelleNode;
    /**
     *
     */
    @Mock(name = "MockUtils_ParcelleRealNode")
    public RealNode ParcelleRealNode;
    /**
     *
     */
    @Mock(name = "MockUtils_themeNode")
    public INode themeNode;
    /**
     *
     */
    @Mock(name = "MockUtils_themeRealNode")
    public RealNode themeRealNode;
    /**
     *
     */
    @Mock(name = "MockUtils_siteNode")
    public INode siteNode;
    /**
     *
     */
    @Mock(name = "MockUtils_siteRealNode")
    public RealNode siteRealNode;
    /**
     *
     */
    @Mock(name = "MockUtils_agroecosystemeNode")
    public INode agroecosystemeNode;
    /**
     *
     */
    @Mock(name = "MockUtils_agroecosystemeRealNode")
    public RealNode agroecosystemeRealNode;
    /**
     *
     */
    @Mock(name = "MockUtils_site")
    public SiteACBB site;
    /**
     *
     */
    @Mock(name = "MockUtils_siteDAO")
    public ISiteACBBDAO siteDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_theme")
    public Theme theme;
    /**
     *
     */
    @Mock(name = "MockUtils_themeDAO")
    public IThemeDAO themeDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_parcelle")
    public Parcelle parcelle;
    /**
     *
     */
    @Mock(name = "MockUtils_parcelleDAO")
    public IParcelleDAO parcelleDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_datatype")
    public DataType datatype;
    /**
     *
     */
    @Mock(name = "MockUtils_datatypeDAO")
    public IDatatypeDAO datatypeDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_agroEcosysteme")
    public AgroEcosysteme agroEcosysteme;
    /**
     *
     */
    @Mock(name = "MockUtils_agroEcosystemeDAO")
    public IAgroEcosystemeDAO agroEcosystemeDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_traitement")
    public TraitementProgramme traitement;
    /**
     *
     */
    @Mock(name = "MockUtils_traitementDAO")
    public ITraitementDAO traitementDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_versionDeTraitement")
    public VersionDeTraitement versionDeTraitement;
    /**
     *
     */
    @Mock(name = "MockUtils_versionDeTraitementDAO")
    public IVersionDeTraitementDAO versionDeTraitementDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_suiviParcelleDAO")
    public ISuiviParcelleDAO suiviParcelleDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_suiviParcelle")
    public SuiviParcelle suiviParcelle;
    /**
     *
     */
    @Mock(name = "MockUtils_utilisateur")
    public Utilisateur utilisateur;
    /**
     *
     */
    @Mock(name = "MockUtils_variable")
    public VariableACBB variable;
    /**
     *
     */
    @Mock(name = "MockUtils_dvu")
    public DatatypeVariableUniteACBB dvu;
    /**
     *
     */
    @Mock(name = "MockUtils_variableDAO")
    public IVariableACBBDAO variableDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_unite")
    public Unite unite;
    /**
     *
     */
    @Mock(name = "MockUtils_uniteDAO")
    public IUniteDAO uniteDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_datatypeVariableUnite")
    public DatatypeVariableUniteACBB datatypeVariableUnite;
    /**
     *
     */
    @Mock(name = "MockUtils_datatypeVariableUnites")
    public List<DatatypeVariableUniteACBB> datatypeVariableUnites;
    /**
     *
     */
    @Mock(name = "MockUtils_datatypeVariableUniteDAO")
    public IDatatypeVariableUniteACBBDAO datatypeVariableUniteDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_datasetDescriptor")
    public DatasetDescriptorACBB datasetDescriptor;
    /**
     *
     */
    @Mock(name = "MockUtils_column")
    public Column column;
    /**
     *
     */
    @Mock(name = "MockUtils_columns")
    public List<Column> columns;
    /**
     *
     */
    @Mock(name = "MockUtils_valeurQualitative")
    public ListeACBB valeurQualitative;
    /**
     *
     */
    @Mock(name = "MockUtils_listeACBBDAO")
    public IListeACBBDAO listeACBBDAO;
    /**
     *
     */
    @Mock(name = "MockUtils_policyManager")
    public IPolicyManager policyManager;
    /**
     *
     */
    public LocalDate dateDebut, dateFin;
    /**
     *
     */
    public LocalDateTime dateTimeDebut, dateTimeFin, dateTimeDebutComplete, dateTimeFinComplete;
    /**
     *
     */
    public LocalTime timeDebut, timeFin, timeFin2330;
    /**
     *
     */
    public IntervalDate intervalDate;
    /**
     *
     */
    @Mock(name = "MockUtils_datasetConfiguration")
    public IDatasetConfiguration datasetConfiguration;
    /**
     *
     */
    @Mock(name = "MockUtils_localizationDAO")
    public ILocalizationDAO localizationDAO;

    /**
     *
     */
    public MockUtils() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * @return
     */
    public static MockUtils getInstance() {
        MockUtils instance = new MockUtils();
        MockitoAnnotations.initMocks(instance);

        try {

            instance.initMocks();

        } catch (DateTimeException | BadExpectedValueException ex) {

            return null;

        }

        instance.initLocalization();

        return instance;
    }

    private Map<Long, VersionFile> getVersions() {

        Map<Long, VersionFile> versions = new HashMap();
        versions.put(1L, this.versionFile);
        return versions;

    }

    private void initDAOs() throws PersistenceException {

        // daos
        when(this.parcelleDAO.getByNKey(any())).thenReturn(Optional.empty());
        when(this.parcelleDAO.getByNKey(contains(PARCELLE))).thenReturn(Optional.of(this.parcelle));
        when(this.siteDAO.getByPath(any())).thenReturn(Optional.empty());
        when(this.siteDAO.getByPath(MockUtils.SITE_PATH)).thenReturn(Optional.of(this.site));
        when(this.siteDAO.getByPath(any())).thenReturn(Optional.empty());
        when(this.siteDAO.getByPath(MockUtils.SITE_CODE)).thenReturn(Optional.of(this.site));
        when(this.datatypeDAO.getByCode(any())).thenReturn(Optional.empty());
        when(this.datatypeDAO.getByCode(MockUtils.DATATYPE)).thenReturn(Optional.of(this.datatype));
        when(this.traitementDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        when(this.traitementDAO.getByNKey(MockUtils.SITE_ID, MockUtils.TRAITEMENT)).thenReturn(Optional.of(this.traitement));
        when(this.suiviParcelleDAO.getByNKey(any(), any(), any())).thenReturn(Optional.empty());
        when(this.suiviParcelleDAO.getByNKey(this.parcelle, this.traitement, this.dateDebut)).thenReturn(Optional.of(this.suiviParcelle));
        when(this.versionDeTraitementDAO.getByNKey(any(), any(), anyInt())).thenReturn(Optional.empty());
        when(this.versionDeTraitementDAO.getByNKey(MockUtils.SITE_CODE, MockUtils.TRAITEMENT, 1))
                .thenReturn(Optional.of(this.versionDeTraitement));
        when(this.variableDAO.getByCode(any())).thenReturn(Optional.empty());
        when(this.variableDAO.getByCode(MockUtils.VARIABLE_CODE)).thenReturn(Optional.of(this.variable));
        when(this.variableDAO.getByAffichage(any())).thenReturn(Optional.empty());
        when(this.variableDAO.getByAffichage(MockUtils.VARIABLE_AFFICHAGE)).thenReturn(Optional.of(this.variable));
        when(this.datatypeVariableUniteDAO.getByDatatype(MockUtils.DATATYPE)).thenReturn(new LinkedList());
        when(this.datatypeVariableUniteDAO.getByNKey(any(), any(), any())).thenReturn(Optional.empty());
        when(this.datatypeVariableUniteDAO.getByNKey(MockUtils.DATATYPE, MockUtils.VARIABLE_CODE, MockUtils.UNITE))
                .thenReturn(Optional.of(this.datatypeVariableUnite));
        when(this.uniteDAO.getByCode(any())).thenReturn(Optional.empty());
        when(this.uniteDAO.getByCode(MockUtils.UNITE)).thenReturn(Optional.of(this.unite));
        when(this.listeACBBDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
        when(this.listeACBBDAO.getByNKey(anyString(), anyString())).thenReturn(Optional.of(this.valeurQualitative));

    }

    private void initDataset() throws DateTimeException {
        // init user
        when(this.utilisateur.getLogin()).thenReturn(MockUtils.UTILISATEUR);
        // init dataset
        when(this.dataset.getDateDebutPeriode()).thenReturn(this.dateTimeDebutComplete);
        when(this.dataset.getDateFinPeriode()).thenReturn(this.dateTimeFinComplete);
        when(this.dataset.getVersions()).thenReturn(this.getVersions());
        when(this.dataset.getLastVersion()).thenReturn(this.versionFile);
        when(this.dataset.getVersions()).thenReturn(this.getVersions());
        when(this.dataset.getRealNode()).thenReturn(this.dataTypeRealNode);
        // init version
        when(this.versionFile.getDataset()).thenReturn(this.dataset);
        when(this.versionFile.getVersionNumber()).thenReturn(1L);
        when(this.versionFile.getUploadUser()).thenReturn(this.utilisateur);
        when(this.dataset.buildDownloadFilename(this.datasetConfiguration)).thenReturn(
                String.format("%s_%s_%s_%s.csv", MockUtils.SITE_PATH.replaceAll("/", "_"),
                        MockUtils.DATATYPE,
                        DateUtil.getUTCDateTextFromLocalDateTime(dateDebut, DateUtil.DD_MM_YYYY_FILE),
                        DateUtil.getUTCDateTextFromLocalDateTime(dateFin, DateUtil.DD_MM_YYYY_FILE)
                ));

    }

    private void initDatasetDescriptor() {

        when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        when(this.column.getName()).thenReturn("colonne1", "colonne2", "colonne3",
                "colonne4", "colonne5", "colonne6", "colonne7", "colonne8", "colonne9",
                "colonne10", null);
        when(this.columns.size()).thenReturn(10);
        when(this.datasetDescriptor.getUndefinedColumn()).thenReturn(2);
        when(this.datasetDescriptor.getEnTete()).thenReturn(2);
        for (int i = 0; i < 10; i++) {
            when(this.columns.get(i)).thenReturn(this.column);
        }

    }

    private void initDates() throws DateTimeException, BadExpectedValueException {
        this.dateDebut = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, DATE_DEBUT);
        this.dateFin = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, DATE_FIN);
        this.dateTimeDebut = dateDebut.atStartOfDay();
        this.dateTimeFin = dateFin.atStartOfDay();
        this.dateTimeDebutComplete = DateUtil.readLocalDateTimeFromLocalDateAndLocaltime(DateUtil.DD_MM_YYYY, DATE_DEBUT, DateUtil.HH_MM_SS, TIME_DEBUT);
        this.dateTimeFinComplete = DateUtil.readLocalDateTimeFromLocalDateAndLocaltime(DateUtil.DD_MM_YYYY, DATE_FIN, DateUtil.HH_MM_SS, TIME_FIN_23_30);
        this.timeDebut = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, TIME_DEBUT);
        this.timeFin = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, TIME_FIN);
        this.timeFin2330 = DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, TIME_FIN_23_30);

        this.intervalDate = new IntervalDate(dateTimeDebutComplete, dateTimeFinComplete, DateUtil.YYYY_MM_DD_HHMMSS_SSSZ);

    }

    private void initEntities() {
        // init site
        when(this.site.getPath()).thenReturn(MockUtils.SITE_PATH);
        when(this.site.getCode()).thenReturn(MockUtils.SITE_CODE);
        when(this.site.getName()).thenReturn(MockUtils.SITE_NOM);
        when(this.site.getId()).thenReturn(MockUtils.SITE_ID);
        // init theme
        when(this.theme.getCode()).thenReturn(MockUtils.THEME);
        // init datatype
        when(this.datatype.getCode()).thenReturn(MockUtils.DATATYPE);
        // init agroecosystem
        when(this.agroEcosysteme.getCode()).thenReturn(MockUtils.AGROECOSYSTEM);
        // init parcelle
        when(this.parcelle.getCode()).thenReturn(MockUtils.PARCELLE);
        when(this.parcelle.getName()).thenReturn(MockUtils.PARCELLE_NOM);
        // init traitement
        when(this.traitement.getCode()).thenReturn(MockUtils.TRAITEMENT);
        when(this.traitement.getAffichage()).thenReturn(MockUtils.TRAITEMENT_AFFICHAGE);
        when(this.traitement.getDateDebutTraitement()).thenReturn(this.dateDebut);
        when(this.traitement.getDateFinTraitement()).thenReturn(this.dateFin);
        // init suivi parcelle
        when(this.suiviParcelle.getDateDebutTraitement()).thenReturn(this.dateDebut);
        when(this.suiviParcelle.getParcelle()).thenReturn(this.parcelle);
        when(this.suiviParcelle.getTraitement()).thenReturn(this.traitement);
        // init version de traitement
        when(this.versionDeTraitement.getDateDebutVersionTraitement()).thenReturn(
                this.dateDebut);
        when(this.versionDeTraitement.getDateFinVersionTraitement()).thenReturn(
                this.dateFin);
        when(this.versionDeTraitement.getTraitementProgramme()).thenReturn(this.traitement);
        when(this.versionDeTraitement.getVersion()).thenReturn(1);
        // init ast
        when(this.siteRealNode.getNodeable()).thenReturn(this.site);
        when(this.themeRealNode.getNodeable()).thenReturn(this.theme);
        when(this.dataTypeRealNode.getNodeable()).thenReturn(this.datatype);
        when(this.agroecosystemeRealNode.getNodeable()).thenReturn(this.agroEcosysteme);
        when(this.ParcelleRealNode.getNodeable()).thenReturn(this.parcelle);

        when(this.siteNode.getRealNode()).thenReturn(this.siteRealNode);
        when(this.dataTypeNode.getRealNode()).thenReturn(this.dataTypeRealNode);
        when(this.agroecosystemeNode.getRealNode()).thenReturn(this.agroecosystemeRealNode);
        when(this.themeNode.getRealNode()).thenReturn(this.themeRealNode);
        when(this.ParcelleNode.getRealNode()).thenReturn(this.ParcelleRealNode);

        when(this.ParcelleNode.getAncestor()).thenReturn(this.siteNode);
        when(this.ParcelleRealNode.getAncestor()).thenReturn(this.siteRealNode);
        when(this.dataTypeNode.getParent()).thenReturn(this.themeNode);
        when(this.dataTypeRealNode.getParent()).thenReturn(this.themeRealNode);
        when(this.themeNode.getParent()).thenReturn(this.siteNode);
        when(this.themeRealNode.getParent()).thenReturn(this.siteRealNode);
        when(this.siteNode.getAncestor()).thenReturn(this.agroecosystemeNode);
        when(this.siteRealNode.getAncestor()).thenReturn(this.agroecosystemeRealNode);

        when(this.dataTypeRealNode.getNodeByNodeableTypeResource(DataType.class)).thenReturn(this.dataTypeRealNode);
        when(this.dataTypeRealNode.getNodeByNodeableTypeResource(Theme.class)).thenReturn(this.themeRealNode);
        when(this.dataTypeRealNode.getNodeByNodeableTypeResource(SiteACBB.class)).thenReturn(this.siteRealNode);
        when(this.dataTypeRealNode.getNodeByNodeableTypeResource(Parcelle.class)).thenReturn(this.ParcelleRealNode);
        when(this.dataTypeRealNode.getNodeByNodeableTypeResource(AgroEcosysteme.class)).thenReturn(this.agroecosystemeRealNode);

        // init variable
        when(this.variable.getCode()).thenReturn(MockUtils.VARIABLE_CODE);
        when(this.variable.getName()).thenReturn(MockUtils.VARIABLE_NOM);
        when(this.variable.getAffichage()).thenReturn(MockUtils.VARIABLE_AFFICHAGE);
        // init unite
        when(this.unite.getCode()).thenReturn(MockUtils.UNITE);
        when(this.datatypeVariableUnite.getUnite()).thenReturn(this.unite);
        when(this.parcelle.getSite()).thenReturn(this.site);
        when(datatypeVariableUnite.getVariable()).thenReturn(variable);
    }

    /**
     *
     */
    public void initLocalization() {

        Localization.setDefaultLocalisation("fr");
        Localization.setLocalisations(Arrays.asList("fr", "en"));
        MockUtils.localizationManager = new SpringLocalizationManager();
        ((SpringLocalizationManager) MockUtils.localizationManager).setUserLocale(new UserLocale());
        ((SpringLocalizationManager) MockUtils.localizationManager)
                .setLocalizationDAO(this.localizationDAO);
        ACBBMessages.setLocalizationManager(MockUtils.localizationManager);
        doReturn(Optional.empty()).when(localizationDAO).getByNKey(any(), any(), any(), any());

    }

    /**
     * @throws DateTimeException
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    public void initMocks() throws DateTimeException, BadExpectedValueException {

        try {
            this.initDates();
            this.initEntities();
            this.initDAOs();
            this.initDatasetDescriptor();
            this.initDataset();
        } catch (PersistenceException ex) {

            LoggerFactory.getLogger(MockUtils.class.getName()).info(ex.getMessage(), ex);

        }

    }

    /**
     * @throws DateTimeException
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void test() throws DateTimeException, BadExpectedValueException {

        MockUtils mockUtils = MockUtils.getInstance();

        VersionFile v = mockUtils.versionFile;

        Assert.assertEquals(MockUtils.UTILISATEUR, v.getUploadUser().getLogin());

        Assert.assertTrue(1L == v.getVersionNumber());

        Assert.assertEquals(
                MockUtils.DATE_DEBUT,
                DateUtil.getUTCDateTextFromLocalDateTime(v.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY));

        Assert.assertEquals(MockUtils.DATE_FIN,
                DateUtil.getUTCDateTextFromLocalDateTime(v.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY));

        Assert.assertEquals(v, v.getDataset().getVersions().get(1L));

        Assert.assertEquals(MockUtils.DATATYPE, v.getDataset().getRealNode().getNodeable()
                .getCode());

    }

}
