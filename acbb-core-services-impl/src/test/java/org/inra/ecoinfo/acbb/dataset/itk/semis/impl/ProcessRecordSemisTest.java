/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.acbb.dataset.itk.impl.AbstractLineRecord;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class ProcessRecordSemisTest {

    ProcessRecordSemis instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    DatasetDescriptorACBB datasetDescriptor;
    @Mock
    IListeItineraireDAO listeItineraireDAO;
    @Mock
    INotificationsManager notificationsManager;
    @Mock
    IPolicyManager policyManager;
    @Mock
    List<LineRecord> lines;
    @Mock
    ErrorsReport errorsReport;
    @Mock
    BuildSemisHelper buildSemisHelper;
    @Mock
    ApplicationContext context;
    /**
     *
     */
    public ProcessRecordSemisTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * @param instance
     */
    public void initInstance(ProcessRecordSemis instance) {
        instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        instance.setListeACBBDAO(this.listeItineraireDAO);
        instance.setListeItineraireDAO(this.listeItineraireDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setNotificationsManager(this.notificationsManager);
        instance.setParcelleDAO(this.m.parcelleDAO);
        instance.setPolicyManager(this.policyManager);
        instance.setSuiviParcelleDAO(this.m.suiviParcelleDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setVariableDAO(this.m.variableDAO);
        instance.setVersionDeTraitementDAO(this.m.versionDeTraitementDAO);
        instance.setVersionFileDAO(this.m.versionFileDAO);
        when(listeItineraireDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance = new ProcessRecordSemis();
        this.initInstance(this.instance);
        new ApplicationContextHolder().setApplicationContext(this.context);
        Mockito.doReturn(this.buildSemisHelper).when(this.context).getBean(BuildSemisHelper.class);
        when(listeItineraireDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildNewLines method, of class ProcessRecordSemis.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildNewLines() throws Exception {
        this.instance.buildNewLines(this.m.versionFile, this.lines, this.errorsReport,
                this.m.requestPropertiesITK);
        Mockito.verify(this.buildSemisHelper).build(this.m.versionFile, this.lines,
                this.errorsReport, this.m.requestPropertiesITK);
    }

    /**
     * Test of processRecord method, of class ProcessRecordSemis.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String texte = org.apache.commons.lang.StringUtils.EMPTY;
        CSVParser parser = new CSVParser(new ByteArrayInputStream(texte.getBytes()), ';');
        Mockito.when(this.datasetDescriptor.getEnTete()).thenReturn(5);
        this.instance = new ProcessRecordSemisImpl();
        this.initInstance(this.instance);
        Mockito.when(this.m.versionFileDAO.merge(this.m.versionFile))
                .thenReturn(this.m.versionFile);
        this.instance.processRecord(parser, this.m.versionFile, this.m.requestPropertiesITK,
                "utf-8", this.datasetDescriptor);
        Assert.assertTrue(111 == ((ProcessRecordSemisImpl) this.instance).function);
    }

    /**
     * Test of readLines method, of class ProcessRecordSemis.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReadLines() throws Exception {
        String texte = "5;25/05/2005;semis maïs;1;1;0;1er semis;true;maïs;5;maïs;clovis;8800395;2;7\n";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(texte.getBytes()), ';');
        List<DatatypeVariableUniteACBB> dbVariables = new LinkedList();
        IntervalDate intervalDate = new IntervalDate(m.dateDebut.atStartOfDay(), m.dateFin.atStartOfDay(), DateUtil.DD_MM_YYYY);
        when(m.parcelleDAO.getByNKey(any())).thenReturn(Optional.of(m.parcelle));
        final LocalDate date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "25/05/2005");
        when(m.suiviParcelleDAO.retrieveSuiviParcelle(eq(m.parcelle), eq(date))).thenReturn(Optional.of(m.suiviParcelle));
        when(m.versionDeTraitementDAO.retrieveVersiontraitement(eq(m.traitement), eq(date))).thenReturn(Optional.of(m.versionDeTraitement));
        this.instance.readLines(parser, m.requestPropertiesITK, this.datasetDescriptor,
                this.errorsReport, 5L, dbVariables, this.lines, intervalDate);
    }

    private static class ProcessRecordSemisImpl extends ProcessRecordSemis {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        final int BUILD_VARIABLES = 1;
        final int READ_LINES = 10;
        final int BUILD_NEW_LINES = 100;
        @Mock
        List<DatatypeVariableUniteACBB> variables;
        int function = 0;

        ProcessRecordSemisImpl() {
            MockitoAnnotations.initMocks(this);
        }

        @Override
        protected void buildNewLines(VersionFile version, List<? extends AbstractLineRecord> lines,
                                     ErrorsReport errorsReport, IRequestPropertiesITK requestPropertiesITK)
                throws PersistenceException {
            this.function += this.BUILD_NEW_LINES;
        }

        @Override
        protected List<DatatypeVariableUniteACBB> buildVariablesHeaderAndSkipHeader(CSVParser parser,
                                                                                    Map<Integer, Column> columns, DatasetDescriptorACBB datasetDescriptorACBB)
                throws PersistenceException, IOException {
            this.function += this.BUILD_VARIABLES;
            return this.variables;
        }

        @Override
        long readLines(CSVParser parser, IRequestPropertiesACBB requestProperties,
                       DatasetDescriptorACBB datasetDescriptor, ErrorsReport errorsReport, long lineCount,
                       List<DatatypeVariableUniteACBB> dbVariables, List<LineRecord> lines, IntervalDate intervalDate) throws IOException {
            Assert.assertTrue(5 == lineCount);
            this.function += this.READ_LINES;
            return 10L;
        }
    }

}
