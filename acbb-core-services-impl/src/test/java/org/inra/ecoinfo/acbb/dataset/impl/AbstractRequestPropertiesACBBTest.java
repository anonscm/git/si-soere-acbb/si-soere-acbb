package org.inra.ecoinfo.acbb.dataset.impl;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;
import org.mockito.Mock;

import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author ptcherniati
 */
public class AbstractRequestPropertiesACBBTest {


    @Mock
    ITestDuplicates testDuplicates;
    AbstractRequestPropertiesACBB instance;
    MockUtils m = MockUtils.getInstance();

    /**
     *
     */
    public AbstractRequestPropertiesACBBTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private int initDates() {
        // case with date debut and date fin
        this.instance.dateFormat = DateUtil.DD_MM_YYYY_HH_MM;
        this.instance.initDate();
        this.instance.setDateDeDebut(this.m.dateTimeDebut);
        this.instance.setDateDeFin(this.m.dateTimeFin, ChronoUnit.MINUTES, 30);
        assertEquals(this.m.dateTimeDebut, DateUtil.readLocalDateTimeFromText(RecorderACBB.YYYYMMJJHHMMSS, this.instance.dates.firstKey()));
        assertEquals(this.m.dateTimeFin, DateUtil.readLocalDateTimeFromText(RecorderACBB.YYYYMMJJHHMMSS, this.instance.dates.lastKey()));
        Map<String, Integer> keys = new HashMap<>();
        this.instance.dates.keySet()
                .forEach((k) -> keys.compute(k.substring(8, 14), (t, u) -> u == null ? 1 : u + 1));
        keys.forEach((u, v) -> test(u, v));
        assertTrue(17_521 == this.instance.dates.size());
        assertFalse(this.instance.dates.containsValue(Boolean.TRUE));
        return this.instance.dates.size();
    }

    private void test(String u, int v) {
        final int expected = "000000".equals(u) ? 366 : 365;
        assertTrue(String.format("%s -> %s; expected : %s", u, v, expected), expected == v);
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new AbstractRequestPropertiesACBBImpl();
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        ((AbstractRequestPropertiesACBBImpl) this.instance).testDuplicates = this.testDuplicates;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addDate method, of class AbstractRequestPropertiesACBB.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddDate() throws Exception {
        // simple case
        this.instance.addDate(MockUtils.DATE_DEBUT);
        assertTrue(this.instance.getDates().get(MockUtils.DATE_DEBUT));
        assertTrue(this.instance.getDates().size() == 1);
        // nominal case with semi-horaire dates
        int size = this.initDates();
        this.instance.addDate("20120121223000");
        this.instance.addDate("20120201130000");
        assertTrue(this.instance.getDates().get("20120121223000"));
        assertTrue(this.instance.getDates().get("20120201130000"));
        assertFalse(this.instance.getDates().get("20120121220000"));
        assertFalse(this.instance.getDates().get("20120201133000"));
        assertTrue(this.instance.getDates().size() == size);
        assertTrue(2 == Maps.filterValues(this.instance.dates, new Predicate<Boolean>() {

            @Override
            public boolean apply(Boolean input) {
                return input;
            }
        }).size());
    }

    /**
     * Test of getCommentaire method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetCommentaire() {
    }

    /**
     * Test of getDateDebutTraitement method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetDateDebutTraitement() {
    }

    /**
     * Test of getDateDeDebut method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetDateDeDebut() {
    }

    /**
     * Test of getDateDeFin method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetDateDeFin() {
    }

    /**
     * Test of getDateFormat method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetDateFormat() {
    }

    /**
     * Test of getDates method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetDates() {
    }

    /**
     * Test of getDoublonsLine method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetDoublonsLine() {
    }

    /**
     * Test of getLocalizationManager method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetLocalizationManager() {
    }

    /**
     * Test of getParcelle method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetParcelle() {
    }

    /**
     * Test of getSite method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetSite() {
    }

    /**
     * Test of getSuiviParcelle method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetSuiviParcelle() {
    }

    /**
     * Test of getTraitement method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetTraitement() {
    }

    /**
     * Test of getVersion method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetVersion() {
    }

    /**
     * Test of getVersionDeTraitement method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testGetVersionDeTraitement() {
    }

    /**
     * Test of initDate method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testInitDate() {
    }

    /**
     * Test of setCommentaire method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetCommentaire() {
    }

    /**
     * Test of setDateDebutTraitement method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDateDebutTraitement() {
    }

    /**
     * Test of setDateDeDebut method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDateDeDebut() {
    }

    /**
     * Test of setDateDeFin method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDateDeFin_3args() {
    }

    /**
     * Test of setDateDeFin method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDateDeFin_Date() {
    }

    /**
     * Test of setDateFormat method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDateFormat() {
    }

    /**
     * Test of setDates method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDates() {
    }

    /**
     * Test of setDoublonsLine method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetDoublonsLine() {
    }

    /**
     * Test of setLocalizationManager method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetLocalizationManager() {
    }

    /**
     * Test of setParcelle method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetParcelle() {
    }

    /**
     * Test of setSite method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetSite() {
    }

    /**
     * Test of setSuiviParcelle method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetSuiviParcelle() {
    }

    /**
     * Test of setTraitement method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetTraitement() {
    }

    /**
     * Test of setVersion method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetVersion() {
    }

    /**
     * Test of setVersionDeTraitement method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testSetVersionDeTraitement() {
    }

    /**
     * Test of testDates method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testTestDates() {
    }

    /**
     * Test of testNonMissingDates method, of class AbstractRequestPropertiesACBB.
     */
    @Test
    public void testTestNonMissingDates() {
    }

    /**
     *
     */
    public class AbstractRequestPropertiesACBBImpl extends AbstractRequestPropertiesACBB {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        ITestDuplicates testDuplicates;

        /**
         *
         */
        public AbstractRequestPropertiesACBBImpl() {
        }

        /**
         * @param version
         * @return
         */
        @Override
        public String getNomDeFichier(VersionFile version) {
            return m.dataset.buildDownloadFilename(MockUtils.getInstance().datasetConfiguration);
        }

        /**
         * @return
         */
        @Override
        public ITestDuplicates getTestDuplicates() {
            return this.testDuplicates;
        }
    }

}
