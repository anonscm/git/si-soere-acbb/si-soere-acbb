/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.itk.ITempoDAO;
import org.inra.ecoinfo.acbb.dataset.itk.IVersionDeTraitementRealiseeDAO;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.dataset.itk.impl.RotationDescriptor;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class SemisVersionDeTraitementRealiseeFactoryTest {

    SemisVersionDeTraitementRealiseeFactory instance = new SemisVersionDeTraitementRealiseeFactory();
    ITKMockUtils m = ITKMockUtils.getInstance();
    @Mock
    SemisAnneeRealiseeFactory anneeRealiseeFactory;
    @Mock
    IPolicyManager policyManager;
    @Mock
    VersionTraitementRealisee versionTraitementRealisee;
    @Mock
    AnneeRealisee anneeRealisee;
    @Mock
    ITempoDAO tempoDAO;
    @Mock
    Tempo tempo;
    @Mock
    LineRecord line;
    @Mock
    RotationDescriptor rotationDescriptor;
    @Mock
    IVersionDeTraitementRealiseeDAO versionDeTraitementRealiseeDAO;
    /**
     *
     */
    public SemisVersionDeTraitementRealiseeFactoryTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance.setAnneeRealiseeFactory(this.anneeRealiseeFactory);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        this.instance.setPolicyManager(this.policyManager);
        this.instance.setVersionDeTraitementRealiseeDAO(this.versionDeTraitementRealiseeDAO);
        this.instance.setTempoDAO(this.tempoDAO);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getNewRotation method, of class
     * SemisVersionDeTraitementRealiseeFactory.
     */
    @Test
    public void testGetNewRotation() {
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.tempo.getParcelle()).thenReturn(this.m.parcelle);
        Mockito.when(this.tempo.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
        VersionTraitementRealisee result = this.instance.getNewRotation(2, this.line);
        Assert.assertEquals(this.m.versionDeTraitement, result.getVersionDeTraitement());
        Assert.assertEquals(this.m.parcelle, result.getParcelle());
        Assert.assertTrue(2 == result.getNumero());
    }

    /**
     * Test of getOrCreateVersiontraitementRealise method, of class
     * SemisVersionDeTraitementRealiseeFactory.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testGetOrCreateVersiontraitementRealise() throws PersistenceException {
        Mockito.when(this.line.getRotationNumber()).thenReturn(1);
        Mockito.when(this.line.getAnneeNumber()).thenReturn(4);
        Mockito.when(this.line.getPeriodeNumber()).thenReturn(0);
        Mockito.when(this.line.getTreatmentVersion()).thenReturn(1);
        Mockito.when(this.line.getOriginalLineNumber()).thenReturn(2L);
        ErrorsReport errorsReport = new ErrorsReport();
        InfoReport infosReport = new InfoReport();
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.tempo.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
        this.instance = Mockito.spy(this.instance);
        Mockito.doReturn(this.rotationDescriptor).when(this.instance)
                .getRotationDescriptor(this.line);
        Mockito.when(this.tempo.getVersionTraitementRealisee()).thenReturn(
                this.versionTraitementRealisee);
        // nominalwith exixting version
        Mockito.when(this.line.isRotation()).thenReturn(Boolean.TRUE);
        Mockito.when(this.rotationDescriptor.isPossibleCouvert()).thenReturn(Boolean.TRUE);
        this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, infosReport);
        Mockito.verify(this.versionDeTraitementRealiseeDAO).saveOrUpdate(
                this.versionTraitementRealisee);
        Mockito.verify(this.tempo).setVersionTraitementRealisee(this.versionTraitementRealisee);
        // nominalwith new version
        Mockito.when(this.tempo.getVersionTraitementRealisee()).thenReturn(null);
        Mockito.when(this.line.getRotationNumber()).thenReturn(2);
        Mockito.doReturn(this.versionTraitementRealisee).when(this.instance)
                .getNewRotation(2, this.line);
        this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, infosReport);
        Mockito.verify(this.versionDeTraitementRealiseeDAO, Mockito.times(2)).saveOrUpdate(
                this.versionTraitementRealisee);
        Mockito.verify(this.tempo, Mockito.times(2)).setVersionTraitementRealisee(
                this.versionTraitementRealisee);
        // not possible cover = secondary crop
        Mockito.when(this.line.getTreatmentAffichage()).thenReturn("T2");
        Mockito.when(this.rotationDescriptor.isPossibleCouvert()).thenReturn(Boolean.FALSE);
        this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, infosReport);
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2, impossible de générer une culture secondaire pour l'année 4 pour la rotation 2 de la version 1 du traitement T2 : la culture principale n'est pas définie. Si elle est présente dans le même fichier, elle doit être déclarée avant.\n",
                errorsReport.getErrorsMessages());
        // with not null versionœ
        Mockito.when(this.tempo.getVersionTraitementRealisee()).thenReturn(
                this.versionTraitementRealisee);
        this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, infosReport);
        Mockito.verify(this.versionDeTraitementRealiseeDAO, Mockito.times(3)).saveOrUpdate(
                this.versionTraitementRealisee);
        Mockito.verify(this.tempo, Mockito.times(3)).setVersionTraitementRealisee(
                this.versionTraitementRealisee);
        // no rotation (prairie permanente bad
        Mockito.when(this.line.isRotation()).thenReturn(Boolean.FALSE);
        errorsReport = new ErrorsReport();
        this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, infosReport);
        Assert.assertTrue(0 != this.line.getRotationNumber());
        Assert.assertTrue(errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2, le numéro de la rotation 2 est invalide pour le traitement T2. Pour une prairie permanente, il n'y a pas de rotation ou le numéro de rotation est 0.\n"
                        + org.apache.commons.lang.StringUtils.EMPTY, errorsReport.getErrorsMessages());
        // no rotation (prairie permanente ok
        Mockito.when(this.line.getRotationNumber()).thenReturn(0);
        errorsReport = new ErrorsReport();
        this.instance.getOrCreateVersiontraitementRealise(this.line, errorsReport, infosReport);
        Assert.assertFalse(errorsReport.hasErrors());
        Mockito.verify(this.versionDeTraitementRealiseeDAO, Mockito.times(5)).saveOrUpdate(
                this.versionTraitementRealisee);
        Mockito.verify(this.tempo, Mockito.times(5)).setVersionTraitementRealisee(
                this.versionTraitementRealisee);
    }

    /**
     * Test of getRotationDescriptor method, of class
     * SemisVersionDeTraitementRealiseeFactory.
     */
    @Test
    public void testGetRotationDescriptor() {
        Mockito.when(this.line.getTempo()).thenReturn(this.tempo);
        Mockito.when(this.tempo.getVersionDeTraitement()).thenReturn(this.m.versionDeTraitement);
        RotationDescriptor result = this.instance.getRotationDescriptor(this.line);
        Assert.assertNotNull(result);
        Assert.assertTrue(result instanceof RotationDescriptor);
    }

    /**
     * Test of updateAnneeRealisee method, of class
     * SemisVersionDeTraitementRealiseeFactory.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateAnneeRealisee() throws Exception {
        ErrorsReport errorsReport = new ErrorsReport();
        this.instance.updateAnneeRealisee(this.anneeRealisee, this.line, errorsReport);
        Mockito.verify(this.anneeRealiseeFactory).updateAnneeRealisee(this.anneeRealisee,
                this.line, errorsReport);

    }

}
