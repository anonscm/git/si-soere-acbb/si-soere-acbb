/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.IListeVegetationDAO;
import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @Mock
    public ILocalizationDAO localizationDAO;
    MockUtils m = MockUtils.getInstance();
    Recorder instance;
    String encoding = "UTF-8";
    @Mock
    IMasseVegClassDAO masseVegClassDAO;
    @Mock
    MasseVegClass masseVegClass;
    @Mock
    MassVegClassAerienSouterrain massVegClassAerienSouterrain;
    @Mock
    MassVegClassCategorieCouvert massVegClassCategorieCouvert;
    @Mock
    MassVegClassCouvertVegetalType massVegClassCouvertVegetalType;
    @Mock
    MassVegClassDevenir massVegClassDevenir;
    @Mock
    MassVegClassPartieVegetaleType massVegClassPartieVegetaleType;
    @Mock
    MassVegClassTypeMasseVegetale massVegClassTypeMasseVegetale;
    @Mock
    MassVegClassTypeUtilisation massVegClassTypeUtilisation;
    @Mock
    IListeVegetationDAO listeVegetationDAO;
    @Mock
    Properties propertiesNomEN;
    @Mock
    Properties propertiesDefinitionEN;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private void initDefaultMassvegClass() {
        Mockito.when(this.masseVegClass.getAerien()).thenReturn(this.massVegClassAerienSouterrain);
        Mockito.when(this.massVegClassAerienSouterrain.getValeur()).thenReturn("aérien:souterrain");
        Mockito.when(this.masseVegClass.getCategorieCouvert()).thenReturn(
                this.massVegClassCategorieCouvert);
        Mockito.when(this.massVegClassCategorieCouvert.getValeur()).thenReturn(
                "catégories de couvert");
        Mockito.when(this.masseVegClass.getCouvertVegetal()).thenReturn(
                this.massVegClassCouvertVegetalType);
        Mockito.when(this.massVegClassCouvertVegetalType.getValeur()).thenReturn("type de couvert");
        Mockito.when(this.masseVegClass.getDefinition()).thenReturn("définition");
        Mockito.when(this.masseVegClass.getDevenir()).thenReturn(this.massVegClassDevenir);
        Mockito.when(this.massVegClassDevenir.getValeur()).thenReturn("devenir");
        Mockito.when(this.masseVegClass.getLibelle()).thenReturn("libellé");
        Mockito.when(this.masseVegClass.getPartiesPlante()).thenReturn(
                this.massVegClassPartieVegetaleType);
        Mockito.when(this.massVegClassPartieVegetaleType.getValeur()).thenReturn("partie végétale");
        Mockito.when(this.masseVegClass.getTypeMasseVegetale()).thenReturn(
                this.massVegClassTypeMasseVegetale);
        Mockito.when(this.masseVegClass.getDatatypeVariableUnite()).thenReturn(
                this.m.datatypeVariableUnite);
        Mockito.when(this.massVegClassTypeMasseVegetale.getValeur()).thenReturn("type de donnée");

        when(masseVegClassDAO.getByNKey(any())).thenReturn(Optional.empty());
        when(listeVegetationDAO.getByNKey(any(), any())).thenReturn(Optional.empty());
    }

    /**
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        this.m = MockUtils.getInstance();
        Localization.setLocalisations(Arrays.asList("fr", "en"));
        this.instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        this.initDefaultMassvegClass();
        this.instance.setDatatypeVariableUniteACBBDAO(this.m.datatypeVariableUniteDAO);
        this.instance.setListeVegetationDAO(this.listeVegetationDAO);
        this.instance.setListeItineraireDAO(this.m.listeACBBDAO);
        this.instance.setMasseVegClassDAO(this.masseVegClassDAO);
        ((SpringLocalizationManager) MockUtils.localizationManager).setLocalizationDAO(this.localizationDAO);
        when(localizationDAO.getByNKey(any(), any(), any(), any())).thenReturn(Optional.empty());
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        Mockito.when(
                this.localizationDAO.newProperties(MasseVegClass.NAME_ENTITY_JPA, "definition",
                        Locale.ENGLISH)).thenReturn(this.propertiesDefinitionEN);
        Mockito.when(
                this.localizationDAO.newProperties(MasseVegClass.ID_JPA, "définition",
                        Locale.ENGLISH)).thenReturn(this.propertiesDefinitionEN);
        Mockito.when(this.propertiesNomEN.getProperty(MockUtils.VARIABLE_NOM)).thenReturn(
                "variableNomen");
        Mockito.when(this.m.variable.getDefinition()).thenReturn("définition");
        Mockito.when(this.propertiesDefinitionEN.getProperty("définition")).thenReturn(
                "définitionen");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        BusinessException error = null;
        String text = "CAn_Prod_AerEx_gr;culture annuelle;production;aérienne;grain;exporté;production exportée de grain;production exported grain;colza blé pois orge maïs;gMS/m²";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(this.masseVegClassDAO.getByNKey("can_prod_aerex_gr")).thenReturn(
                Optional.of(this.masseVegClass));
        File file = null;
        // nominal case
        this.instance.deleteRecord(parser, file, this.encoding);
        Mockito.verify(this.masseVegClassDAO).remove(this.masseVegClass);
        /*
         * // bad type site code text =
         * "badCode;culture annuelle;production;aérienne;grain;exporté;production exportée de grain;production exported grain;colza blé pois orge maïs;gMS/m²"
         * ; parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
         * Mockito.when(m.variableDAO.getByName("badcode")).thenThrow( new
         * PersistenceException("error")); BusinessException error = null; try {
         * instance.deleteRecord(parser, file, encoding); } catch (BusinessException e) { error = e;
         * } Assert.assertNotNull("une exception doit être lancée", error);
         * Assert.assertEquals("error", error.getMessage());
         */
        error = null;
        // peristence exception
        text = "can_prod_aerex_gr;";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doThrow(new PersistenceException("error2")).when(this.masseVegClassDAO)
                .remove(this.masseVegClass);
        try {
            this.instance.deleteRecord(parser, file, this.encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<MasseVegClass> masseVegClasses = Arrays
                .asList(this.masseVegClass);
        Mockito.when(this.masseVegClassDAO.getAll()).thenReturn(masseVegClasses);
        List<MasseVegClass> result = this.instance.getAllElements();
        Mockito.verify(this.masseVegClassDAO).getAll();
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(masseVegClasses.get(0), result.get(0));
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        final List<MasseVegClass> masseVegClasses = Arrays
                .asList(this.masseVegClass);
        Mockito.when(this.masseVegClassDAO.getAll()).thenReturn(masseVegClasses);
        this.instance.initModelGridMetadata();
        LineModelGridMetadata result = this.instance
                .getNewLineModelGridMetadata(this.masseVegClass);
        Assert.assertTrue(10 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("libellé", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("catégories de couvert", result.getColumnModelGridMetadataAt(1)
                .getValue());
        Assert.assertEquals("type de donnée", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("aérien:souterrain", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("partie végétale", result.getColumnModelGridMetadataAt(4).getValue());
        Assert.assertEquals("devenir", result.getColumnModelGridMetadataAt(5).getValue());
        Assert.assertEquals("définition", result.getColumnModelGridMetadataAt(6).getValue());
        Assert.assertEquals("définitionen", result.getColumnModelGridMetadataAt(7).getValue());
        Assert.assertEquals("type de couvert", result.getColumnModelGridMetadataAt(8).getValue());
        Assert.assertEquals("unite", result.getColumnModelGridMetadataAt(9).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        this.instance.setLocalizationManager(MockUtils.localizationManager);
        this.instance.initModelGridMetadata();
        Mockito.verify(this.localizationDAO).newProperties(MasseVegClass.NAME_ENTITY_JPA,
                MasseVegClass.ATTRIBUTE_JPA_DEFINITION, Locale.ENGLISH);
        Mockito.verify(this.listeVegetationDAO).getByName(
                MassVegClassTypeUtilisation.MAV_CLASS_TYPE_UTILISATION_TYPE);
        Mockito.verify(this.listeVegetationDAO).getByName(
                MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN);
        Mockito.verify(this.listeVegetationDAO).getByName(
                MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT);
        Mockito.verify(this.listeVegetationDAO).getByName(MassVegClassDevenir.MAV_CLASS_DEVENIR);
        Mockito.verify(this.m.datatypeVariableUniteDAO).getAllDatatypeVariableUniteACBB();
        Mockito.verify(this.listeVegetationDAO).getByName(
                MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE);
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        // Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(m.site);
        String text = "libelle_var;catégories de couvert;type de donnée;aérien_souterrain;partie végétale;devenir;définition_fr;définition_en;couvert;unité\n"
                + "CAn_Prod_AerEx_gr;culture annuelle;production;aérienne;grain;exporté;production exportée de grain;production exported grain;colza blé pois orge maïs;gMS/m²";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;

        // nominal case
        Mockito.when(this.listeVegetationDAO.getByNKey(MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT, "culture_annuelle"))
                .thenReturn(Optional.of(this.massVegClassCategorieCouvert));
        Mockito.when(
                this.listeVegetationDAO.getByNKey(MassVegClassTypeMasseVegetale.MAV_CLASS_TYPE_MASSE_VEGETALE, "production"))
                .thenReturn(Optional.of(this.massVegClassTypeMasseVegetale));
        Mockito.when(this.listeVegetationDAO.getByNKey(MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN, "aerienne"))
                .thenReturn(Optional.of(this.massVegClassAerienSouterrain));
        Mockito.when(this.listeVegetationDAO.getByNKey(MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE, "grain"))
                .thenReturn(Optional.of(this.massVegClassPartieVegetaleType));
        Mockito.when(
                this.listeVegetationDAO.getByNKey(MassVegClassDevenir.MAV_CLASS_DEVENIR, "exporte"))
                .thenReturn(Optional.of(this.massVegClassDevenir));
        Mockito.when(this.listeVegetationDAO.getByNKey(MassVegClassCouvertVegetalType.MAV_CLASS_COUVERT_VEGETAL, "colza_ble_pois_orge_mais")).thenReturn(Optional.of(this.massVegClassCouvertVegetalType));
        Mockito.when(this.m.datatypeVariableUniteDAO.getByNKey("biomasse_production_teneur", "masse_vegetale", "gMS/m²")).thenReturn(Optional.of(this.m.datatypeVariableUnite));
        Mockito.when(this.masseVegClassDAO.getByNKey("can_prod_aerex_gr")).thenReturn(Optional.empty());
        ArgumentCaptor<MasseVegClass> ts = ArgumentCaptor.forClass(MasseVegClass.class);
        when(masseVegClassDAO.getByNKey(anyString())).thenReturn(Optional.empty());
        this.instance.processRecord(parser, file, this.encoding);
        Mockito.verify(this.masseVegClassDAO).saveOrUpdate(ts.capture());
        Assert.assertEquals("CAn_Prod_AerEx_gr", ts.getValue().getLibelle());
        Assert.assertEquals("can_prod_aerex_gr", ts.getValue().getCode());
        Assert.assertEquals(this.massVegClassCategorieCouvert, ts.getValue().getCategorieCouvert());
        Assert.assertEquals(this.massVegClassTypeMasseVegetale, ts.getValue()
                .getTypeMasseVegetale());
        Assert.assertEquals(this.massVegClassAerienSouterrain, ts.getValue().getAerien());
        Assert.assertEquals(this.massVegClassPartieVegetaleType, ts.getValue().getPartiesPlante());
        Assert.assertEquals(this.massVegClassDevenir, ts.getValue().getDevenir());
        Assert.assertEquals("production exportée de grain", ts.getValue().getDefinition());
        Assert.assertEquals(this.massVegClassCouvertVegetalType, ts.getValue().getCouvertVegetal());
        Assert.assertEquals(this.m.datatypeVariableUnite, ts.getValue().getDatatypeVariableUnite());
        ts = ArgumentCaptor.forClass(MasseVegClass.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(this.masseVegClassDAO.getByNKey("can_prod_aerex_gr")).thenReturn(
                Optional.of(this.masseVegClass));
        this.instance.processRecord(parser, file, this.encoding);
        Mockito.verify(this.masseVegClassDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertEquals(this.masseVegClass, ts.getValue());
        Assert.assertEquals(null, this.masseVegClass.getCode());
        Assert.assertEquals(this.massVegClassCategorieCouvert,
                this.masseVegClass.getCategorieCouvert());
        Assert.assertEquals(this.massVegClassTypeMasseVegetale,
                this.masseVegClass.getTypeMasseVegetale());
        Assert.assertEquals(this.massVegClassAerienSouterrain, this.masseVegClass.getAerien());
        Assert.assertEquals(this.massVegClassPartieVegetaleType,
                this.masseVegClass.getPartiesPlante());
        Assert.assertEquals(this.massVegClassDevenir, this.masseVegClass.getDevenir());
        Assert.assertEquals(this.massVegClassCouvertVegetalType,
                this.masseVegClass.getCouvertVegetal());
        Assert.assertEquals(this.m.datatypeVariableUnite,
                this.masseVegClass.getDatatypeVariableUnite());
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecordMissingValues() throws Exception {
        // Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(m.site);
        String text = "libelle_var;catégories de couvert;type de donnée;aérien_souterrain;partie végétale;devenir;définition_fr;définition_en;couvert;unité\n"
                + ";culture annuelle;production;aérienne;grain;exporté;production exportée de grain;production exported grain;colza blé pois orge maïs;gMS/m²";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;

        // nominal case
        Mockito.when(this.masseVegClassDAO.getByNKey(any())).thenReturn(Optional.empty());
        Mockito.when(this.masseVegClassDAO.getByNKey("can_prod_aerex_gr")).thenReturn(Optional.of(this.masseVegClass));
        ArgumentCaptor<MasseVegClass> ts = ArgumentCaptor.forClass(MasseVegClass.class);
        when(masseVegClassDAO.getByNKey(anyString())).thenReturn(Optional.empty());
        try {
            this.instance.processRecord(parser, file, this.encoding);
        } catch (BusinessException e) {
            ts = ArgumentCaptor.forClass(MasseVegClass.class);
            Mockito.verify(this.masseVegClassDAO, Mockito.never()).saveOrUpdate(ts.capture());
            Assert.assertEquals(
                    ".Ligne 1 , colonne 1, le libellé doit être renseigné.\n"
                            + ".Ligne 1 , colonne 2, la catégorie de couvert végétal \"culture annuelle\" n'est pas disponible.\n"
                            + ".Ligne 1 , colonne 3, le type de masse végétale \"production\" n'est pas disponible.\n"
                            + ".Ligne 1 , colonne 4, la valeur aérien / souterrain \"aérienne\" n'est pas disponible.\n"
                            + ".Ligne 1 , colonne 5, la partie de plante \"grain\" n'est pas disponible.\n"
                            + ".Ligne 1 , colonne 6, le devenir \"exporté\" n'est pas disponible.\n"
                            + ".Ligne 1 , colonne 9, le couvert végétal \"colza blé pois orge maïs\" n'est pas disponible.\n"
                            + ".Ligne 1 , colonne 10, l'unité \"gMS/m²\" n'est pas disponible.\n",
                    e.getMessage());
        }
    }

}
