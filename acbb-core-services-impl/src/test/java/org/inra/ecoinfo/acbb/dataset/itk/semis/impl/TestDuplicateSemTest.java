/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.impl;

import org.inra.ecoinfo.acbb.dataset.itk.impl.ITKMockUtils;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.junit.*;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class TestDuplicateSemTest {

    TestDuplicateSem instance;
    ITKMockUtils m = ITKMockUtils.getInstance();
    ErrorsReport errorsReport = new ErrorsReport();
    /**
     *
     */
    public TestDuplicateSemTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.instance = new TestDuplicateSem();
        MockitoAnnotations.initMocks(this);
        this.instance.setConfiguration(this.m.datasetConfiguration);
        this.instance.setErrorsReport(this.errorsReport);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addLine method, of class TestDuplicateSem.
     */
    @Test
    public void testAddLine_4args() {
        String[] values = new String[12];
        for (int i = 0; i < values.length; i++) {
            values[i] = "value" + i;
        }
        this.instance = Mockito.spy(this.instance);
        // values[0], values[1], values[8], values[10], values[11]
        Mockito.doNothing().when(this.instance)
                .addLine("value0", "value1", "value8", "value10", "value11", 2L);
        this.instance.addLine(values, 2L, null, null);
        Mockito.verify(this.instance).addLine("value0", "value1", "value8", "value10", "value11",
                2L);
    }

    /**
     * Test of addLine method, of class TestDuplicateSem.
     */
    @Test
    public void testAddLine_6args() {
        // nomibnal
        this.instance.addLine("parcelle", "dateDebut", "couvertVegetal", "espece", "variete", 2L);
        Assert.assertEquals("couvertVegetal", this.instance.lineCouvert.get("parcelle_dateDebut"));
        Assert.assertTrue(2 == this.instance.line.get("parcelle_dateDebut_espece_variete"));
        Assert.assertFalse(this.errorsReport.hasErrors());
        // with same couvert
        this.instance.line.clear();
        this.instance.addLine("parcelle", "dateDebut", "couvertVegetal", "espece", "variete", 2L);
        Assert.assertEquals("couvertVegetal", this.instance.lineCouvert.get("parcelle_dateDebut"));
        Assert.assertTrue(2 == this.instance.line.get("parcelle_dateDebut_espece_variete"));
        Assert.assertFalse(this.errorsReport.hasErrors());
        // with different couvert
        this.instance.line.clear();
        this.instance.lineCouvert.put("parcelle_dateDebut", "autreCouvert");
        this.instance.addLine("parcelle", "dateDebut", "couvertVegetal", "espece", "variete", 2L);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2 : sur la parcelle parcelle à la date dateDebut le couvert végétal a déjà été défini en autreCouvert. il ne peut donc pas être couvertVegetal.\n",
                this.errorsReport.getErrorsMessages());
        // with same key
        this.instance.setErrorsReport(new ErrorsReport());
        this.instance.line.put("parcelle_dateDebut_espece_variete", 2L);
        this.instance.addLine("parcelle", "dateDebut", "couvertVegetal", "espece", "variete", 2L);
        Assert.assertTrue(this.errorsReport.hasErrors());
        Assert.assertEquals(
                "-Ligne 2 : sur la parcelle parcelle à la date dateDebut le couvert végétal a déjà été défini en autreCouvert. il ne peut donc pas être couvertVegetal.\n",
                this.errorsReport.getErrorsMessages());
    }

}
