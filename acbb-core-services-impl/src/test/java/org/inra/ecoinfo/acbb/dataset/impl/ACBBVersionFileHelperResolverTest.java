package org.inra.ecoinfo.acbb.dataset.impl;

import org.inra.ecoinfo.acbb.test.utils.MockUtils;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.junit.*;
import org.mockito.*;
import static org.mockito.Mockito.*;

import java.util.Map;

/**
 * @author ptcherniati
 */
public class ACBBVersionFileHelperResolverTest {

    @Mock
    Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap;
    ACBBVersionFileHelperResolver instance = new ACBBVersionFileHelperResolver();

    /**
     *
     */
    public ACBBVersionFileHelperResolverTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.instance.setDatatypesVersionFileHelpersMap(this.datatypesVersionFileHelpersMap);
        Assert.assertEquals(this.datatypesVersionFileHelpersMap,
                this.instance.getDatatypesVersionFileHelpersMap());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of resolveByDatatype method, of class ACBBVersionFileHelperResolver.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testResolveByDatatype() throws Exception {
        MockUtils.getInstance();
        ArgumentCaptor<IVersionFileHelper> versionFileHelper = ArgumentCaptor
                .forClass(IVersionFileHelper.class);
        Mockito.when(this.datatypesVersionFileHelpersMap.containsKey(MockUtils.DATATYPE))
                .thenReturn(Boolean.FALSE, Boolean.TRUE);
        this.instance.resolveByDatatype(MockUtils.DATATYPE);
        this.instance.resolveByDatatype(MockUtils.DATATYPE);
        Mockito.verify(this.datatypesVersionFileHelpersMap, Mockito.times(1)).put(
                eq(MockUtils.DATATYPE), versionFileHelper.capture());
        Mockito.verify(this.datatypesVersionFileHelpersMap, Mockito.times(2)).get(
                MockUtils.DATATYPE);
        Assert.assertTrue(versionFileHelper.getAllValues().size() == 1);
        Assert.assertNotNull(versionFileHelper.getAllValues().get(0));
    }

}
