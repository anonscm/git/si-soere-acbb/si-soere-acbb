package org.inra.ecoinfo.acbb.extraction.fluxmeteo.jsf;

import java.io.Serializable;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.IFluxMeteoDatasetManager;
import org.inra.ecoinfo.acbb.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.acbb.extraction.jsf.UIAssociate;
import org.inra.ecoinfo.acbb.extraction.jsf.UIDate;
import org.inra.ecoinfo.acbb.extraction.jsf.UISite;
import org.inra.ecoinfo.acbb.extraction.jsf.UIVariable;
import org.inra.ecoinfo.acbb.synthesis.jpa.IIntervalDateSynthesisDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.impl.DefaultMotivation;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIBeanFluxMeteo.
 */
@ManagedBean(name = "uiFluxMeteo")
@ViewScoped
public class UIBeanFluxMeteo extends AbstractUIBeanForSteps implements Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The flux meteo dataset manager.
     */
    @ManagedProperty(value = "#{fluxMeteoDatasetManager}")
    @Transient
    IFluxMeteoDatasetManager fluxMeteoDatasetManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    @Transient
    INotificationsManager notificationsManager;

    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    @Transient
    IExtractionManager extractionManager;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    @Transient
    JpaTransactionManager transactionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    @Transient
    ILocalizationManager localizationManager;
    
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;

    @ManagedProperty(value = "#{intervalDateSynthesisDAO}")
    IIntervalDateSynthesisDAO intervalDateSynthesisDAO;
    List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypeClasses = Stream.of(
            org.inra.ecoinfo.acbb.synthesis.fluxchambre.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.fluxtours.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.meteorologie.SynthesisDatatype.class
    ).collect(Collectors.toList());

    /**
     * The parameters request @link(ParametersRequest).
     */
    @Transient
    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The affichage @link(String).
     */
    String affichage = "1";
    UISite uiSite;
    UIDate uiDate;
    UIVariable uiVariable;
    UIAssociate uIAssociate;

    /**
     * Instantiates a new uI bean flux meteo.
     */
    public UIBeanFluxMeteo() {
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    public void initProperties() {
        setEnabledMotivation(true);
        uiSite = new UISite();
        uiSite.initProperties(localizationManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager, intervalDateSynthesisDAO, synthesisDatatypeClasses);
        uiVariable = new UIVariable();
        uiVariable.initVariable(localizationManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
    }

    /**
     *
     * @return
     */
    public UIVariable getUiVariable() {
        return uiVariable;
    }

    /**
     *
     * @return
     */
    public UISite getUiSite() {
        return uiSite;
    }

    /**
     *
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        uiSite.addSitetoMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        uiDate.addDatestoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final FluxMeteoParameterVO parameters = new FluxMeteoParameterVO(metadatasMap);
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, Integer.parseInt(this.affichage));
        return null;
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public String getAffichage() {
        return this.affichage;
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public boolean getIsStepValid() {
        switch (getStep()) {
            case 1:
                return uiDate.getDateStepIsValid();
            case 2:
                return uiSite.getSiteStepIsValid();
            case 3:
                return uiVariable.getVariableStepIsValid();
            case 4:
                return uiVariable.getVariableStepIsValid();
            default:
                return false;
        }
    }

    /**
     * Gets the locale.
     *
     * @return the locale
     */
    public Locale getLocale() {
        return FacesContext.getCurrentInstance().getViewRoot().getLocale();
    }

    /**
     * Gets the parameters request.
     *
     * @return the parameters request
     */
    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return "fluxMeteo";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 2:
                uiSite.updateAgroAvailables(fluxMeteoDatasetManager, uiDate.getDatesForm1ParamVO().intervalsDate());
                break;
            case 3:
                uiVariable.updateVariablesAvailables(fluxMeteoDatasetManager, uiDate, uiSite);
                break;
            case 4:
                final String dateStartString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateStart();
                final String dateEndString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateEnd();
                Map<Long, UIVariable.VariableJSF> variables = uiVariable.getVariables().entrySet().stream()
                        .map(e -> e.getValue().entrySet())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                IntervalDate intervalDate = null;
                try {
                    intervalDate = new IntervalDate(dateStartString, dateEndString, DateUtil.DD_MM_YYYY);
                    uIAssociate.caseassociate(variables, intervalDate, fileCompManager);
                } catch (BadExpectedValueException ex) {
                    LoggerFactory.getLogger(UIBeanFluxMeteo.class.getName()).error(ex.getMessage(), ex);
                } catch (DateTimeParseException ex) {
                    LoggerFactory.getLogger(UIBeanFluxMeteo.class.getName()).error(ex.getMessage(), ex);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public final void setAffichage(final String affichage) {
        this.affichage = affichage;
    }

    /**
     * Sets the extraction manager.
     *
     * @param extractionManager the new extraction manager
     */
    public final void setExtractionManager(final IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * Sets the flux meteo dataset manager.
     *
     * @param fluxMeteoDatasetManager the new flux meteo dataset manager
     */
    public final void setFluxMeteoDatasetManager(
            final IFluxMeteoDatasetManager fluxMeteoDatasetManager) {
        this.fluxMeteoDatasetManager = fluxMeteoDatasetManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public final void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the parameters request.
     *
     * @param parametersRequest the new parameters request
     */
    public final void setParametersRequest(final ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     * Sets the transaction manager.
     *
     * @param transactionManager the new transaction manager
     */
    public final void setTransactionManager(final JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setIntervalDateSynthesisDAO(IIntervalDateSynthesisDAO intervalDateSynthesisDAO) {
        this.intervalDateSynthesisDAO = intervalDateSynthesisDAO;
    }

    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    public UIAssociate getUiAssociate() {
        return uIAssociate;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        String commentExtraction;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiSite.getSiteStepIsValid() && uiVariable.getVariableStepIsValid()
                    && uiDate.getDateStepIsValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }
    }

}
