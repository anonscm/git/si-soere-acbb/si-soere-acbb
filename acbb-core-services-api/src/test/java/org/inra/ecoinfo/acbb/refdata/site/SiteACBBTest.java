/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.site;

import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author ptcherniati
 */
public class SiteACBBTest {

    /**
     *
     */
    public SiteACBBTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAdresse method, of class SiteACBB.
     */
    @Test
    public void testGetAdresse() {
        SiteACBB instance = new SiteACBB();
        instance.setAdresse("adress");
        String result = instance.getAdresse();
        Assert.assertEquals("adress", result);
    }

    /**
     * Test of getAgroEcosysteme method, of class SiteACBB.
     */
    @Test
    public void testGetAgroEcosysteme() {
        SiteACBB instance = new SiteACBB();
        AgroEcosysteme agr = new AgroEcosysteme();
        instance.setAgroEcosysteme(agr);
        AgroEcosysteme result = instance.getAgroEcosysteme();
        Assert.assertEquals(agr, result);
    }

    /**
     * Test of getAltitudeMoyenne method, of class SiteACBB.
     */
    @Test
    public void testGetAltitudeMoyenne() {
        SiteACBB instance = new SiteACBB();
        Float expResult = 12.6F;
        instance.setAltitudeMoyenne(expResult);
        Float result = instance.getAltitudeMoyenne();
        Assert.assertTrue(expResult == result);
    }

    /**
     * Test of getBlocs method, of class SiteACBB.
     */
    @Test
    public void testGetBlocs() {
        SiteACBB instance = new SiteACBB();
        List<Bloc> expResult = Arrays.asList(new Bloc());
        instance.setBlocs(expResult);
        List<Bloc> result = instance.getBlocs();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getClimat method, of class SiteACBB.
     */
    @Test
    public void testGetClimat() {
        SiteACBB instance = new SiteACBB();
        String expResult = "climat";
        instance.setClimat(expResult);
        String result = instance.getClimat();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getCoordonnees method, of class SiteACBB.
     */
    @Test
    public void testGetCoordonnees() {
        SiteACBB instance = new SiteACBB();
        String expResult = "coordonnées";
        instance.setCoordonnees(expResult);
        String result = instance.getCoordonnees();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getDateMiseEnService method, of class SiteACBB.
     */
    @Test
    public void testGetDateMiseEnService() throws DateTimeException {
        SiteACBB instance = new SiteACBB();
        LocalDate expResult = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "21/06/2012");
        instance.setDateMiseEnService(expResult);
        LocalDate result = instance.getDateMiseEnService();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getMilieu method, of class SiteACBB.
     */
    @Test
    public void testGetMilieu() {
        SiteACBB instance = new SiteACBB();
        String expResult = "milieu";
        instance.setMilieu(expResult);
        String result = instance.getMilieu();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getParcelles method, of class SiteACBB.
     */
    @Test
    public void testGetParcelles() {
        SiteACBB instance = new SiteACBB();
        List<Parcelle> expResult = Arrays.asList(new Parcelle());
        instance.setParcelles(expResult);
        List<Parcelle> result = instance.getParcelles();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getPluviometrieMoyenne method, of class SiteACBB.
     */
    @Test
    public void testGetPluviometrieMoyenne() {
        SiteACBB instance = new SiteACBB();
        Float expResult = 12.3F;
        instance.setPluviometrieMoyenne(expResult);
        Float result = instance.getPluviometrieMoyenne();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getProfondeurMoyenneSol method, of class SiteACBB.
     */
    @Test
    public void testGetProfondeurMoyenneSol() {
        SiteACBB instance = new SiteACBB();
        Float expResult = 12.3F;
        instance.setProfondeurMoyenneSol(expResult);
        Float result = instance.getProfondeurMoyenneSol();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getTemperatureMoyenne method, of class SiteACBB.
     */
    @Test
    public void testGetTemperatureMoyenne() {
        SiteACBB instance = new SiteACBB();
        Float expResult = 12.3F;
        instance.setTemperatureMoyenne(expResult);
        Float result = instance.getTemperatureMoyenne();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getTraitements method, of class SiteACBB.
     */
    @Test
    public void testGetTraitements() {
        SiteACBB instance = new SiteACBB();
        List<TraitementProgramme> expResult = Arrays
                .asList(new TraitementProgramme());
        instance.setTraitements(expResult);
        List<TraitementProgramme> result = instance.getTraitements();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getTypeDeSol method, of class SiteACBB.
     */
    @Test
    public void testGetTypeDeSol() {
        SiteACBB instance = new SiteACBB();
        String expResult = "typesol";
        instance.setTypeDeSol(expResult);
        String result = instance.getTypeDeSol();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVentDominant method, of class SiteACBB.
     */
    @Test
    public void testGetVentDominant() {
        SiteACBB instance = new SiteACBB();
        String expResult = "vent";
        instance.setVentDominant(expResult);
        String result = instance.getVentDominant();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVille method, of class SiteACBB.
     */
    @Test
    public void testGetVille() {
        SiteACBB instance = new SiteACBB();
        String expResult = "ville";
        instance.setVille(expResult);
        String result = instance.getVille();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getVitesseMoyenne method, of class SiteACBB.
     */
    @Test
    public void testGetVitesseMoyenne() {
        SiteACBB instance = new SiteACBB();
        Float expResult = 12.3F;
        instance.setVitesseMoyenne(expResult);
        Float result = instance.getVitesseMoyenne();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setNom method, of class SiteACBB.
     */
    @Test
    public void testSetNom() {
        String nom = "Prénom Nom";
        SiteACBB instance = new SiteACBB();
        instance.setName(nom);
        Assert.assertEquals(nom, instance.getName());
        //Assert.assertEquals("prenom_nom", instance.getCode());
    }

    /**
     * Test of toString method, of class SiteACBB.
     */
    @Test
    public void testToString() {
        SiteACBB instance = new SiteACBB();
        String nom = "Prénom Nom";
        instance.setName(nom);
        String result = instance.toString();
        Assert.assertEquals(nom, result);
    }

}
