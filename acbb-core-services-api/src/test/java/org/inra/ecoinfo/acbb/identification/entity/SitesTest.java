/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.identification.entity;

import org.junit.*;

import static org.inra.ecoinfo.acbb.identification.entity.Sites.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author ptcherniati
 */
public class SitesTest {

    /**
     *
     */
    public SitesTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of valueOf method, of class Sites.
     */
    @Test
    public void testValueOf() {
        assertEquals(LAQUEUILLE, Sites.valueOf("LAQUEUILLE"));
        assertEquals(ESTREES_MONS, Sites.valueOf("ESTREES_MONS"));
        assertEquals(LUSIGNAN, Sites.valueOf("LUSIGNAN"));
        assertEquals(THEIX, Sites.valueOf("THEIX"));
    }

    /**
     * Test of getNom method, of class Sites.
     */
    @Test
    public void testGetNom() {
        assertEquals("Laqueuille", LAQUEUILLE.getNom());
        assertEquals("Estrées-Mons", ESTREES_MONS.getNom());
        assertEquals("Lusignan", LUSIGNAN.getNom());
        assertEquals("Theix", THEIX.getNom());
    }

    /**
     * Test of toString method, of class Sites.
     */
    @Test
    public void testToString() {
        assertEquals("Laqueuille", LAQUEUILLE.toString());
        assertEquals("Estrées-Mons", ESTREES_MONS.toString());
        assertEquals("Lusignan", LUSIGNAN.toString());
        assertEquals("Theix", THEIX.toString());
    }

    /**
     * Test of values method, of class Sites.
     */
    @Test
    public void testValues() {
        Sites[] expResult = new Sites[]{ESTREES_MONS, LAQUEUILLE, LUSIGNAN, THEIX};
        Sites[] result = Sites.values();
        assertArrayEquals(expResult, result);
    }


}
