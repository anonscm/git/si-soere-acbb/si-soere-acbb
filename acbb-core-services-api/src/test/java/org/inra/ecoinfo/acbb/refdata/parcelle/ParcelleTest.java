/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.parcelle;

import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.junit.*;
import org.mockito.MockitoAnnotations;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * @author tcherniatinsky
 */
public class ParcelleTest {

    Parcelle a43 = new Parcelle();
    Parcelle a5 = new Parcelle();
    Parcelle a52 = new Parcelle();
    Parcelle a1 = new Parcelle();
    Parcelle b43 = new Parcelle();
    Parcelle b5 = new Parcelle();
    Parcelle b52 = new Parcelle();
    Parcelle c1 = new Parcelle();
    SiteACBB site = new SiteACBB();
    /**
     *
     */
    public ParcelleTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        a43.setCode("a43");
        a5.setCode("a5");
        a52.setCode("a52");
        a1.setCode("a1");
        b43.setCode("b43");
        b5.setCode("b5");
        b52.setCode("b52");
        c1.setCode("c1");
        a43.site = site;
        a5.site = site;
        a52.site = site;
        a1.site = site;
        b43.site = site;
        b5.site = site;
        b52.site = site;
        c1.site = site;
        a43.id = 1L;
        a5.id = 2L;
        a52.id = 3L;
        a1.id = 4L;
        b43.id = 5L;
        b5.id = 6L;
        b52.id = 7L;
        c1.id = 8L;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testCompareTo() {
        SortedSet<Parcelle> traitementProgrammes = new TreeSet();
        traitementProgrammes.add(c1);
        traitementProgrammes.add(b5);
        traitementProgrammes.add(a43);
        traitementProgrammes.add(a5);
        traitementProgrammes.add(b43);
        traitementProgrammes.add(b52);
        traitementProgrammes.add(a52);
        traitementProgrammes.add(a1);
        assertEquals("[a1, a5, a43, a52, b5, b43, b52, c1]", traitementProgrammes.toString());
    }

    /**
     * Test of compareTo method, of class Parcelle.
     */
    @Test
    public void testGetAlphanumeAffichage() {
        Parcelle instance = new Parcelle();
        instance.setCode("A43");
        String result = instance.getAlphanumeCode();
        assertEquals("A*********043", result);
    }

}
