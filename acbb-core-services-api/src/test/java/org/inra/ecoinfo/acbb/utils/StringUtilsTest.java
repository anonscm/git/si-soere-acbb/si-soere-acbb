/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.utils;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * @author tcherniatinsky
 */
public class StringUtilsTest {

    /**
     *
     */
    public StringUtilsTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of orderedString method, of class StringUtils.
     */
    @Test
    public void testOrderedString() {
        String str = "A43";
        String expResult = "A*********043";
        String result = StringUtils.orderedString(str);
        assertEquals(expResult, result);
        str = "Bonjour2";
        expResult = "Bonjour***002";
        result = StringUtils.orderedString(str);
        assertEquals(expResult, result);
    }

}
