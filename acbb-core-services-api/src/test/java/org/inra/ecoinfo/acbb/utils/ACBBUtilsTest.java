/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.utils;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.junit.*;

import java.util.Collection;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author tcherniatinsky
 */
@Ignore
public class ACBBUtilsTest {

    /**
     *
     */
    public ACBBUtilsTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAgroEcosystemeFromDataset method, of class ACBBUtils.
     */
    @Test
    public void testGetAgroEcosystemeFromDataset() {
        Dataset dataset = null;
        AgroEcosysteme expResult = null;
        AgroEcosysteme result = ACBBUtils.getAgroEcosystemeFromDataset(dataset);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getVariableFromDataset method, of class ACBBUtils.
     */
    @Test
    public void testGetVariableFromDataset() {
        Dataset dataset = null;
        VariableACBB expResult = null;
        VariableACBB result = ACBBUtils.getVariableFromDataset(dataset);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getThemeFromDataset method, of class ACBBUtils.
     */
    @Test
    public void testGetThemeFromDataset() {
        Dataset dataset = null;
        Theme expResult = null;
        Theme result = ACBBUtils.getThemeFromDataset(dataset);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSiteFromDataset method, of class ACBBUtils.
     */
    @Test
    public void testGetSiteFromDataset() {
        Dataset dataset = null;
        SiteACBB expResult = null;
        SiteACBB result = ACBBUtils.getSiteFromDataset(dataset);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParcelleFromDataset method, of class ACBBUtils.
     */
    @Test
    public void testGetParcelleFromDataset() {
        Dataset dataset = null;
        Parcelle expResult = null;
        Parcelle result = ACBBUtils.getParcelleFromDataset(dataset);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDatatypeFromDataset method, of class ACBBUtils.
     */
    @Test
    public void testGetDatatypeFromDataset() {
        Dataset dataset = null;
        DataType expResult = null;
        DataType result = ACBBUtils.getDatatypeFromDataset(dataset);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNAIfBadValueOrNVIfEmptyValue method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetNAIfBadValueOrNVIfEmptyValue_Float() {
        Float number = null;
        ACBBUtils instance = null;
        String expResult = "";
        String result = ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(number);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNAIfBadValueOrNVIfEmptyValue method, of class
     * AbstractBiomasseOutputBuilder.
     */
    @Test
    @Ignore
    public void testGetNAIfBadValueOrNVIfEmptyValue_Integer() {
        Integer number = null;
        ACBBUtils instance = null;
        String expResult = "";
        String result = ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(number);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBindedsMessages method, of class ACBBUtils.
     */
    @Test
    public void testGetBindedsMessages() {
        Collection<AbstractIntervention> bindedsITK = null;
        String expResult = "";
        String result = ACBBUtils.getBindedsMessages(bindedsITK);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBindedMessages method, of class ACBBUtils.
     */
    @Test
    public void testGetBindedMessages() {
        AbstractIntervention intervention = null;
        String expResult = "";
        String result = ACBBUtils.getBindedMessages(intervention);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDatasetConfiguration method, of class ACBBUtils.
     */
    @Test
    public void testGetDatasetConfiguration() {
        IDatasetConfiguration expResult = null;
        IDatasetConfiguration result = ACBBUtils.getDatasetConfiguration();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLocalizationManager method, of class ACBBUtils.
     */
    @Test
    public void testGetLocalizationManager() {
        Optional<ILocalizationManager> expResult = null;
        Optional<ILocalizationManager> result = ACBBUtils.getLocalizationManager();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
