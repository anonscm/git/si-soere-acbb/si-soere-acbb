/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.utils;

import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.text.NumberFormat;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

/**
 * @author tcherniatinsky
 */
public class NumberFormatUtilsTest {

    @Spy
    NumberFormatUtils instance;
    @Mock
    ILocalizationManager localizationManager;
    @Mock
    UserLocale userLocale;
    /**
     *
     */
    public NumberFormatUtilsTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new NumberFormatUtils();
        MockitoAnnotations.initMocks(this);
        doReturn(userLocale).when(localizationManager).getUserLocale();
        doReturn(Locale.FRANCE).when(userLocale).getLocale();
        ACBBUtils.localizationManager = localizationManager;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getLocalInstance method, of class NumberFormatUtils.
     */
    @Test
    public void testGetLocalInstance_Locale() {
        NumberFormat result = NumberFormatUtils.getLocalInstance(Locale.FRANCE);
        assertEquals("4,2", result.format(4.2f));
        result = NumberFormatUtils.getLocalInstance(Locale.ENGLISH);
        assertEquals("4.2", result.format(4.2f));
    }

    /**
     * Test of getLocalInstance method, of class NumberFormatUtils.
     */
    @Test
    public void testGetLocalInstance() {
        NumberFormat result = NumberFormatUtils.getLocalInstance();
        assertEquals("4,2", result.format(4.2f));
    }

}
