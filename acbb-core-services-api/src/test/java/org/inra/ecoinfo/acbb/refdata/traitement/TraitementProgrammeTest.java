/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata.traitement;

import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.junit.*;
import org.mockito.MockitoAnnotations;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * @author tcherniatinsky
 */
public class TraitementProgrammeTest {

    TraitementProgramme a43 = new TraitementProgramme();
    TraitementProgramme a5 = new TraitementProgramme();
    TraitementProgramme a52 = new TraitementProgramme();
    TraitementProgramme a1 = new TraitementProgramme();
    TraitementProgramme t9 = new TraitementProgramme();
    TraitementProgramme b5 = new TraitementProgramme();
    TraitementProgramme t10 = new TraitementProgramme();
    TraitementProgramme c1 = new TraitementProgramme();
    SiteACBB site = new SiteACBB();
    /**
     *
     */
    public TraitementProgrammeTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        a43.affichage = a43.nom = "a43";
        a5.affichage = a5.nom = "a5";
        a52.affichage = a52.nom = "a52";
        a1.affichage = a1.nom = "a1";
        t9.affichage = t9.nom = "t9";
        b5.affichage = b5.nom = "b5";
        t10.affichage = t10.nom = "t10";
        c1.affichage = c1.nom = "c1";
        a43.site = site;
        a5.site = site;
        a52.site = site;
        a1.site = site;
        t9.site = site;
        b5.site = site;
        t10.site = site;
        c1.site = site;
        a43.id = 1L;
        a5.id = 2L;
        a52.id = 3L;
        a1.id = 4L;
        t9.id = 5L;
        b5.id = 6L;
        t10.id = 7L;
        c1.id = 8L;
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testCompareTo() {
        SortedSet<TraitementProgramme> traitementProgrammes = new TreeSet();
        traitementProgrammes.add(c1);
        traitementProgrammes.add(b5);
        traitementProgrammes.add(a43);
        traitementProgrammes.add(a5);
        traitementProgrammes.add(t9);
        traitementProgrammes.add(t10);
        traitementProgrammes.add(a52);
        traitementProgrammes.add(a1);
        assertEquals("[a1, a5, a43, a52, b5, c1, t9, t10]", traitementProgrammes.toString());
    }

    /**
     * Test of compareTo method, of class TraitementProgramme.
     */
    @Test
    public void testGetAlphanumeAffichage() {
        TraitementProgramme instance = new TraitementProgramme();
        instance.nom = "A43";
        String result = instance.getAlphanumeAffichage();
        assertEquals("A*********043", result);
    }
}
