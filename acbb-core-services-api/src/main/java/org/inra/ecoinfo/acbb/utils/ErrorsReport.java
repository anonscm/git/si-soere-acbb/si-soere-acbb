package org.inra.ecoinfo.acbb.utils;

import org.inra.ecoinfo.acbb.dataset.IErrorsReport;

/**
 * The Class ErrorsReport.
 *
 * @author Tcherniatinsky Philippe
 * @see org.inra.ecoinfo.acbb.dataset.IErrorsReport
 */
public class ErrorsReport implements IErrorsReport {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant CST_HYPHEN @link(String).
     */
    static final String CST_HYPHEN = "-";

    /**
     * The Constant CST_NEW_LINE @link(String).
     */
    static final String CST_NEW_LINE = "\n";

    /**
     * The Constant CST_SPACE_TAB @link(String).
     */
    static final String CST_SPACE_TAB = "      ";
    InfoReport infoReport = new InfoReport();

    /**
     * The errors messages @link(String).
     */
    String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * Instantiates a new errors report.
     */
    public ErrorsReport() {
        super();
    }

    /**
     * Adds the error message.
     *
     * @param errorMessage
     * @link(String) the error message
     * @link(String) the error message
     * @see org.inra.ecoinfo.acbb.dataset.IErrorsReport#addErrorMessage(java.lang.String)
     */
    @Override
    public void addErrorMessage(final String errorMessage) {
        this.errorsMessages = this.errorsMessages.concat(ErrorsReport.CST_HYPHEN)
                .concat(errorMessage).concat(ErrorsReport.CST_NEW_LINE);
    }

    /**
     * Adds the error message.
     *
     * @param errorMessage
     * @param e
     * @link(String) the error message
     * @link(Throwable) the e
     * @link(String) the error message
     * @link(Throwable) the exception
     * @see org.inra.ecoinfo.acbb.dataset.IErrorsReport#addErrorMessage(java.lang.String,
     * java.lang.Throwable) Adds the error message and add it the exception message.
     */
    @Override
    public void addErrorMessage(final String errorMessage, final Throwable e) {
        this.errorsMessages = this.errorsMessages.concat(ErrorsReport.CST_HYPHEN)
                .concat(errorMessage).concat(ErrorsReport.CST_NEW_LINE)
                .concat(ErrorsReport.CST_SPACE_TAB).concat(e.getMessage())
                .concat(ErrorsReport.CST_NEW_LINE);
    }

    /**
     * @param infoMessage
     */
    @Override
    public void addInfoMessage(String infoMessage) {
        this.infoReport.addInfoMessage(infoMessage);
    }

    /**
     * Gets the errors messages.
     *
     * @return the errors messages
     * @see org.inra.ecoinfo.acbb.dataset.IErrorsReport#getErrorsMessages()
     */
    @Override
    public String getErrorsMessages() {
        return this.errorsMessages;
    }

    /**
     * @return
     */
    @Override
    public String getInfosMessages() {
        return this.infoReport.getInfoMessages();
    }

    /**
     * Checks for errors.
     *
     * @return true, if successful
     * @see org.inra.ecoinfo.acbb.dataset.IErrorsReport#hasErrors()
     */
    @Override
    public boolean hasErrors() {
        return this.errorsMessages.length() > 0;
    }

    /**
     * @return
     */
    @Override
    public boolean hasInfos() {
        return this.infoReport.hasInfo();
    }

}
