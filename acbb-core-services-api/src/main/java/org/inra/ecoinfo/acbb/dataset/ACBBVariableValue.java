package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import java.util.List;

/**
 * @param <T>
 * @author ptcherniati
 */
public class ACBBVariableValue<T extends IValeurQualitative> extends VariableValue {

    T valeurQualitative;
    List<T> valeursQualitatives;
    AbstractMethode methode;
    Float mediane;

    /**
     * @param value
     */
    public ACBBVariableValue(final String value) {
        super(value);
    }

    /**
     * @param dvu
     * @param valeursQualitatives
     */
    public ACBBVariableValue(final DatatypeVariableUniteACBB dvu, final List<T> valeursQualitatives) {
        super(dvu, null);
        this.setValeursQualitatives(valeursQualitatives);
    }

    /**
     * @param dvu
     * @param value
     */
    public ACBBVariableValue(final DatatypeVariableUniteACBB dvu, final String value) {
        super(dvu, value);
    }

    /**
     * @param dvu
     * @param value
     * @param methode
     * @param mediane
     */
    public ACBBVariableValue(final DatatypeVariableUniteACBB dvu, final String value, final AbstractMethode methode, final Float mediane) {
        super(dvu, value);
        this.methode = methode;
        this.mediane = mediane;
    }

    /**
     * @param dvu
     * @param value
     * @param methode
     * @param mediane
     * @param qualityClass
     */
    public ACBBVariableValue(final DatatypeVariableUniteACBB dvu, final String value, final AbstractMethode methode, final Float mediane, int qualityClass) {
        super(dvu, value);
        this.methode = methode;
        this.mediane = mediane;
        this.qualityClass = qualityClass;
    }

    /**
     * @param dvu
     * @param value
     * @param methode
     */
    public ACBBVariableValue(final DatatypeVariableUniteACBB dvu, final String value, final AbstractMethode methode) {
        super(dvu, value);
        this.methode = methode;
    }

    /**
     * @param dvu
     * @param valeurQualitative
     */
    public ACBBVariableValue(final DatatypeVariableUniteACBB dvu, final T valeurQualitative) {
        super(dvu, null);
        this.setValeurQualitative(valeurQualitative);
    }

    /**
     * @return
     */
    public T getValeurQualitative() {
        return this.valeurQualitative;
    }

    /**
     * @param valeurQualitative
     */
    public final void setValeurQualitative(final T valeurQualitative) {
        this.valeurQualitative = valeurQualitative;
    }

    /**
     * @return the valeursQualitatives
     */
    public List<T> getValeursQualitatives() {
        return this.valeursQualitatives;
    }

    /**
     * @param valeursQualitatives the valeursQualitatives to set
     */
    public void setValeursQualitatives(List<T> valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

    /**
     * @return
     */
    public final Boolean isValeurQualitative() {
        return this.dvu.getVariable().getIsQualitative();
    }

    /**
     * @return
     */
    public AbstractMethode getMethode() {
        return methode;
    }

    /**
     * @param methode
     */
    public void setMethode(AbstractMethode methode) {
        this.methode = methode;
    }

    /**
     * @return
     */
    public Float getMediane() {
        return this.mediane;
    }

    /**
     * @param mediane
     */
    public void setMediane(Float mediane) {
        this.mediane = mediane;
    }

}
