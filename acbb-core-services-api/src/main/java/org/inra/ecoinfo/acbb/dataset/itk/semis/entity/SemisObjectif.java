/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class SemisObjectif.
 */
@Entity
@DiscriminatorValue(SemisObjectif.SEMIS_OBJECTIF)
public class SemisObjectif extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_obj_id";

    /**
     * The Constant SEMIS_OBJECTIF.
     */
    public static final String SEMIS_OBJECTIF = "sem_objectifs";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
