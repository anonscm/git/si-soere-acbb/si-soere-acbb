package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.util.List;

/**
 * The Class ValeurSemisAttr.
 */
@Entity
@Table(name = ValeurSemisAttr.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, AbstractIntervention.ID_JPA}),
        indexes = {
                @Index(name = "vsematt_sem", columnList = AbstractIntervention.ID_JPA)})
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurSemisAttr.ID_JPA), name = "id")})
public class ValeurSemisAttr extends ValeurIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "valeur_semis_vsema";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vsema_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE @link(String).
     */
    private static final String NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE = "vsema_vq";
    /**
     * The semis @link(Semis).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
            CascadeType.REMOVE})
    @JoinColumn(name = AbstractIntervention.ID_JPA, referencedColumnName = AbstractIntervention.ID_JPA, nullable = true)
    AbstractIntervention semis;

    /**
     * The valeurs qualitatives @link(List<? extends IValeurQualitative>).
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = ListeItineraire.class)
    @JoinTable(name = ValeurSemisAttr.NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE, joinColumns = @JoinColumn(name = ValeurSemisAttr.ID_JPA, referencedColumnName = ValeurSemisAttr.ID_JPA), inverseJoinColumns = @JoinColumn(name = ListeACBB.ID_JPA, referencedColumnName = ListeACBB.ID_JPA))
    List<ListeACBB> valeursQualitatives;

    /**
     * Instantiates a new valeur semis attr.
     */
    public ValeurSemisAttr() {
        super();
    }

    /**
     * Instantiates a new valeur semis attr.
     *
     * @param valeur
     * @param realNode
     * @param semis
     * @link(Float)
     * @link(DatatypeVariableUnite) the datatype variable unite
     * @link(Semis) the semis
     */
    public ValeurSemisAttr(final Float valeur, final RealNode realNode,
                           AbstractIntervention semis) {
        super(valeur, realNode);
        this.setSemis(semis);
    }

    /**
     * Instantiates a new valeur semis attr.
     *
     * @param valeur
     * @param realNode
     * @param semis
     * @link(List<ListeItineraire>)
     * @link(DatatypeVariableUniteACBB) the datatype variable unite
     * @link(Semis) the semis
     */
    @SuppressWarnings("unchecked")
    public ValeurSemisAttr(List<ListeItineraire> valeur,
                           RealNode realNode, AbstractIntervention semis) {
        super(valeur, realNode);
        this.setSemis(semis);
    }

    /**
     * Instantiates a new valeur semis attr.
     *
     * @param valeur
     * @param realNode
     * @param semis
     * @link(ListeItineraire)
     * @link(DatatypeVariableUnite) the datatype variable unite
     * @link(Semis) the semis
     */
    public ValeurSemisAttr(final ListeItineraire valeur,
                           final RealNode realNode, AbstractIntervention semis) {
        super(valeur, realNode);
        this.setSemis(semis);
    }

    /**
     * Gets the semis.
     *
     * @return the semis
     */
    public AbstractIntervention getSemis() {
        return this.semis;
    }

    /**
     * Sets the semis.
     *
     * @param semis the new semis
     */
    public void setSemis(final AbstractIntervention semis) {
        this.semis = semis;
    }

    /**
     * @return @see
     * org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention#getValeursQualitatives()
     */
    @Override
    public List getValeursQualitatives() {
        return this.valeursQualitatives;
    }

    /**
     * @param valeursQualitatives
     * @see org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention#setValeursQualitatives(java.util.List)
     */
    @Override
    public void setValeursQualitatives(List valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

}
