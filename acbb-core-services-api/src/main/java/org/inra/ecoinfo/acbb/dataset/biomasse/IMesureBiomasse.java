package org.inra.ecoinfo.acbb.dataset.biomasse;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;

import java.util.List;

/**
 * @param <T>
 * @param <V>
 * @author ptcherniati
 */
public interface IMesureBiomasse<T extends AbstractBiomasse, V extends ValeurBiomasse> {

    /**
     * @return
     */
    T getSequence();

    /**
     * @return
     */
    List<V> getValeurs();
}
