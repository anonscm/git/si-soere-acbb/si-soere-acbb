/*
 *
 */
package org.inra.ecoinfo.acbb.utils.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The Class DatePlotVO.
 */
public class DatePlotVO implements Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The date @link(Date).
     */
    LocalDateTime date;

    /**
     * The value @link(Float).
     */
    Float value;

    /**
     * Instantiates a new date plot vo.
     */
    public DatePlotVO() {
        super();
    }

    /**
     * Instantiates a new date plot vo.
     *
     * @param date the date
     */
    public DatePlotVO(final LocalDateTime date) {
        super();
        this.setDate(date);
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDate() {
        return this.date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }

}
