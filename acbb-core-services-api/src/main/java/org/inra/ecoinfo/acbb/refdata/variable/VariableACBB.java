package org.inra.ecoinfo.acbb.refdata.variable;

import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.refdata.variable.Variable;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class VariableACBB.
 */
@Entity(name = "VariableACBB")
@Table(name = VariableACBB.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = VariableACBB.ID_JPA)
public class VariableACBB extends Variable {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "variable_acbb_var";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "var_id";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The valeurs qualitatives @link(List<ValeurQualitative>).
     */
    @OneToMany(cascade = ALL)
    List<ListeACBB> valeursQualitatives = new LinkedList();

    /**
     * Instantiates a new variable acbb.
     */
    public VariableACBB() {
        super();
    }

    /**
     * Instantiates a new variable acbb.
     *
     * @param id the id
     */
    public VariableACBB(final Long id) {
        super(id);
    }

    /**
     * Instantiates a new variable acbb.
     *
     * @param nom           the nom
     * @param definition    the definition
     * @param affichage     the affichage
     * @param isQualitative the is qualitative
     */
    public VariableACBB(final String nom, final String definition, final String affichage,
                        final boolean isQualitative) {
        super(nom, definition, affichage, isQualitative);
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(VariableACBB) the o
     */
    public int compareTo(final Variable o) {
        return this.getAffichage().compareTo(o.getAffichage());
    }

    /**
     * Gets the valeurs qualitatives.
     *
     * @return the valeurs qualitatives
     */
    public List<ListeACBB> getValeursQualitatives() {
        return this.valeursQualitatives;
    }

    /**
     * Sets the valeurs qualitatives.
     *
     * @param valeursQualitatives the new valeurs qualitatives
     */
    public void setValeursQualitatives(final List<ListeACBB> valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

}
