/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.SortedMap;

/**
 * The Interface IRequestPropertiesACBB.
 * <p>
 * object interface used for temporarily storing the information of a file
 *
 * @author Tcherniatinsky Philippe
 */
public interface IRequestPropertiesACBB extends Serializable {

    /**
     * The Constant BUNDLE_NAME.
     */
    String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.messages";

    /**
     * The Constant PROPERTY_MSG_UNDEFINED_PERIOD.
     */
    String PROPERTY_MSG_UNDEFINED_PERIOD = "PROPERTY_MSG_UNDEFINED_PERIOD";

    /**
     * The Constant PROPERTY_MSG_MISSING_DATE.
     */
    String PROPERTY_MSG_MISSING_DATE = "PROPERTY_MSG_MISSING_DATE";

    /**
     * The Constant PROPERTY_MSG_DATE_OFF_LIMITS.
     */
    String PROPERTY_MSG_DATE_OFF_LIMITS = "PROPERTY_MSG_DATE_OFF_LIMITS";

    /**
     * The Constant PROPERTY_MSG_BAD_NAME_FILE.
     */
    String PROPERTY_MSG_BAD_NAME_FILE = "PROPERTY_MSG_BAD_NAME_FILE";

    /**
     * The Constant PROPERTY_MSG_BAD_SITE.
     */
    String PROPERTY_MSG_BAD_SITE = "PROPERTY_MSG_BAD_SITE";

    /**
     * The Constant PROPERTY_MSG_BAD_DATATYPE.
     */
    String PROPERTY_MSG_BAD_DATATYPE = "PROPERTY_MSG_BAD_DATATYPE";

    /**
     * The Constant PROPERTY_MSG_BAD_BEGIN_DATE.
     */
    String PROPERTY_MSG_BAD_BEGIN_DATE = "PROPERTY_MSG_BAD_BEGIN_DATE";

    /**
     * The Constant PROPERTY_MSG_BAD_END_DATE.
     */
    String PROPERTY_MSG_BAD_END_DATE = "PROPERTY_MSG_BAD_END_DATE";

    /**
     * The Constant FORMAT_FILE.
     */
    String FORMAT_FILE = "csv";

    /**
     * Adds the date.
     *
     * @param stringDate
     * @throws BadExpectedValueException the bad expected value exception
     * @throws DateTimeException         the parse exception {@link Date} the string date
     * @link(String) the string date
     * @link(String) the string date
     */
    void addDate(String stringDate) throws BadExpectedValueException, DateTimeException;

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     */
    String getCommentaire();

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire {@link String} the new commentaire
     */
    void setCommentaire(String commentaire);

    /**
     * Gets the date debut traitement.
     *
     * @return the date debut traitement
     */
    LocalDate getDateDebutTraitement();

    /**
     * Sets the date debut traitement.
     *
     * @param dateDebutTraitement the new date debut traitement {@link Date} the new date debut traitement
     */
    void setDateDebutTraitement(LocalDate dateDebutTraitement);

    /**
     * Gets the date de debut.
     *
     * @return the date de debut
     */
    LocalDateTime getDateDeDebut();

    /**
     * Sets the date de debut.
     *
     * @param dateDeDebut the new date de debut {@link Date} the new date de debut
     */
    void setDateDeDebut(LocalDateTime dateDeDebut);

    /**
     * Gets the date de fin.
     *
     * @return the date de fin
     */
    LocalDateTime getDateDeFin();

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin {@link Date} the new date de fin
     */
    void setDateDeFin(LocalDateTime dateDeFin);

    /**
     * Gets the date format.
     *
     * @return the date format
     */
    String getDateFormat();

    /**
     * Sets the date format.
     *
     * @param dateFormat the new date format
     */
    void setDateFormat(String dateFormat);

    /**
     * Gets the dates.
     *
     * @return the dates
     */
    SortedMap<String, Boolean> getDates();

    /**
     * Sets the dates.
     *
     * @param dates the dates
     */
    void setDates(SortedMap<String, Boolean> dates);

    /**
     * Gets the doublons line.
     *
     * @return the doublons line
     */
    ITestDuplicates getDoublonsLine();

    /**
     * Sets the doublons line.
     *
     * @param doublonsLine the new doublons line {@link ITestDuplicates} the new doublons line
     */
    void setDoublonsLine(ITestDuplicates doublonsLine);

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    ILocalizationManager getLocalizationManager();

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager {@link ILocalizationManager} the new localization
     *                            manager
     */
    void setLocalizationManager(ILocalizationManager localizationManager);

    /**
     * Gets the nom de fichier.
     *
     * @param version the version
     * @return the nom de fichier
     */
    String getNomDeFichier(VersionFile version);

    /**
     * Gets the parcelle.
     *
     * @return the parcelle
     */
    Parcelle getParcelle();

    /**
     * Sets the parcelle.
     *
     * @param parcelle the new parcelle {@link Parcelle} the new parcelle
     */
    void setParcelle(Parcelle parcelle);

    /**
     * Gets the site.
     *
     * @return the site
     */
    SiteACBB getSite();

    /**
     * Sets the site.
     *
     * @param site the new site {@link SiteACBB} the new site
     */
    void setSite(SiteACBB site);

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    SuiviParcelle getSuiviParcelle();

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle {@link SuiviParcelle} the new suivi parcelle
     */
    void setSuiviParcelle(SuiviParcelle suiviParcelle);

    /**
     * @return
     */
    ITestDuplicates getTestDuplicates();

    /**
     * Gets the traitement.
     *
     * @return the traitement
     */
    TraitementProgramme getTraitement();

    /**
     * Sets the traitement.
     *
     * @param traitement the new traitement {@link TraitementProgramme} the new traitement
     */
    void setTraitement(TraitementProgramme traitement);

    /**
     * Gets the version.
     *
     * @return the version
     */
    int getVersion();

    /**
     * Sets the version.
     *
     * @param version the new version {@link VersionFile} the new version
     */
    void setVersion(int version);

    /**
     * Gets the version de traitement.
     *
     * @return the version de traitement
     */
    VersionDeTraitement getVersionDeTraitement();

    /**
     * Sets the version de traitement.
     *
     * @param versionDeTraitement the new version de traitement {@link VersionDeTraitement} the new version de
     *                            traitement
     */
    void setVersionDeTraitement(VersionDeTraitement versionDeTraitement);

    /**
     * Inits the date.
     */
    void initDate();

    /**
     * Sets the date de fin.
     *
     * @param dateDeFinp
     * @param chronoUnit
     * @param step       int the step {@link Date} the date de fin
     * @link(Date)
     * @link(Date) the date de fin
     */
    void setDateDeFin(LocalDateTime dateDeFinp, ChronoUnit chronoUnit, int step);

    /**
     * Test dates.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @link(BadsFormatsReport) the bads formats report {@link BadsFormatsReport} the bads formats
     * report
     */
    void testDates(BadsFormatsReport badsFormatsReport);

    /**
     * Test non missing dates.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @link(BadsFormatsReport) the bads formats report {@link BadsFormatsReport the bads formats
     * report
     */
    void testNonMissingDates(BadsFormatsReport badsFormatsReport);

}
