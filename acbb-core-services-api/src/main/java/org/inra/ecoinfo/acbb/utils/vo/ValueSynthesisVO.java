/*
 *
 */
package org.inra.ecoinfo.acbb.utils.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The Class ValueSynthesisVO.
 */
public class ValueSynthesisVO implements Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The date @link(Date).
     */
    LocalDateTime date;

    /**
     * The site @link(String).
     */
    String site;

    /**
     * The variable @link(String).
     */
    String variable;

    /**
     * The value @link(Float).
     */
    Float value;

    /**
     * Instantiates a new value synthesis vo.
     *
     * @param date     the date
     * @param site     the site
     * @param variable the variable
     * @param value    the value
     */
    public ValueSynthesisVO(final LocalDateTime date, final String site, final String variable,
                            final Float value) {
        super();
        this.setDate(date);
        this.site = site;
        this.variable = variable;
        this.value = value;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDate() {
        return this.date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public String getSite() {
        return this.site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public String getVariable() {
        return this.variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final String variable) {
        this.variable = variable;
    }

}
