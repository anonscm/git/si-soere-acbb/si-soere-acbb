package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * The Class SemisEspece.
 */
@Entity
@DiscriminatorValue(SemisEspece.SEMIS_SPECIES)
public class SemisEspece extends ListeItineraire {


    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_espece";

    /**
     * The Constant SEMIS_SPECIES.
     */
    public static final String SEMIS_SPECIES = "sem_espece";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The varietes @link(Map<String,ListeItineraire>).
     */
    @Transient
    Map<String, ListeItineraire> varietes = new HashMap();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(super.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * Gets the varietes.
     *
     * @return the varietes
     */
    public Map<String, ListeItineraire> getVarietes() {
        return this.varietes;
    }

    /**
     * Sets the varietes.
     *
     * @param varietes the varietes
     */
    public final void setVarietes(final Map<String, ListeItineraire> varietes) {
        this.varietes = varietes;
    }

}
