/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.autreintervention;

import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;

/**
 * The Interface IRequestPropertiesAI.
 */
public interface IRequestPropertiesAI extends IRequestPropertiesITK {

}
