/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(HauteurVegetalNature.HAV_NATURE)
public class HauteurVegetalNature extends ListeVegetation {

    /**
     *
     */
    public static final String HAV_NATURE = "hav_nature";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
