/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodehauteurvegetal;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class Bloc.
 */
@Entity
@Table(name = MethodeHauteurVegetal.NAME_ENTITY_JPA)
public class MethodeHauteurVegetal extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_hauteur_vegetal";

    /**
     * The Constant ID_JPA.
     */
    // LES CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public MethodeHauteurVegetal() {
        super();
    }

    // LES CONSTRUCTEURS

    /**
     * Instantiates a new bloc.
     *
     * @param definition
     * @param libelle
     * @param numberId
     */
    // GETTERS ET SETTERS
    public MethodeHauteurVegetal(String definition, String libelle, Long numberId) {
        super(libelle, definition, numberId);
    }

    /**
     * @param definition
     */
    public void update(String definition) {
        this.definition = definition;
    }

}
