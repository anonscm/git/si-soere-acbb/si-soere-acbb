/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import java.io.Serializable;

/**
 * The Interface IRapportReport.
 * <p>
 * interface objects that generate infos reports
 *
 * @author Tcherniatinsky Philippe
 */
public interface IInfoReport extends Serializable {

    /**
     * Adds the info message.
     *
     * @param infoMessage
     * @link(String) the info message
     * @link(String) the info message {@link String} add info to report
     */
    void addInfoMessage(String infoMessage);

    /**
     * Gets the info messages.
     *
     * @return the info report message
     */
    String getInfoMessages();

    /**
     * Checks for info.
     *
     * @return true if report has info
     */
    boolean hasInfo();
}
