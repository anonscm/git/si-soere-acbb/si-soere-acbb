package org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity;

import org.inra.ecoinfo.acbb.dataset.biomasse.IMesureBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class Lai.
 * <p>
 * Record one line of the lai file
 */
@Entity
@Table(name = Lai.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(
                columnNames = {LaiNature.LAI_NATURE, AbstractBiomasse.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "lai_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "lai_itk_idx", columnList = AbstractIntervention.ID_JPA),
                @Index(name = "lai_date_idx", columnList = AbstractBiomasse.ATTRIBUTE_JPA_DATE),
                @Index(name = "lai_suiviparcelle_idx", columnList = SuiviParcelle.ID_JPA),
                @Index(name = "lai_date_suiviparcelle_nature_idx", columnList = LaiNature.LAI_NATURE + "," + AbstractBiomasse.ATTRIBUTE_JPA_DATE + "," + SuiviParcelle.ID_JPA)
        })
@DiscriminatorValue(Lai.LAI)
@PrimaryKeyJoinColumn(name = Lai.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA)
public class Lai extends AbstractBiomasse implements IMesureBiomasse<Lai, ValeurLai> {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "leaf_index_area_lai";

    /**
     * The Constant AUTRE_INTERVENTION.
     */
    static final String LAI = "lai";

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.messages";

    /**
     * The Constant PROPERTY_MSG_DESCRIPTION.
     */
    static final String PROPERTY_MSG_DESCRIPTION = "PROPERTY_MSG_DESCRIPTION";

    /**
     * The Constant ID_JPA.
     */
    static final String ID_JPA = "lai_id";

    /**
     * The valeurs lai.
     */
    @OneToMany(mappedBy = ValeurLai.ATTRIBUTE_JPA_LAI, cascade = ALL)
    List<ValeurLai> valeursLai = new LinkedList();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = LaiNature.LAI_NATURE, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    LaiNature laiNature;

    /**
     * Instantiates a new Lai.
     */
    public Lai() {
        super();
        this.setType(Lai.LAI);
    }

    /**
     * @param valeursLai
     */
    public Lai(List<ValeurLai> valeursLai) {
        super();
        this.setType(Lai.LAI);
        this.valeursLai = valeursLai;
    }

    /**
     * @param version
     * @param natureCouvert
     * @param observation
     * @param nature
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param serie
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     * @param suiviParcelle
     * @param interventionType
     * @param intervention
     */
    public Lai(VersionFile version, String natureCouvert, LaiNature nature, String observation, LocalDate date, String serie,
               Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber,
               Integer periodeRealiseeNumber, SuiviParcelle suiviParcelle,
               InterventionType interventionType, AbstractIntervention intervention) {
        super(natureCouvert, version, Lai.LAI, observation, date, serie, versionTraitementRealiseeNumber,
                anneeRealiseeNumber, periodeRealiseeNumber, suiviParcelle, interventionType,
                intervention);
        this.laiNature = nature;
    }

    /**
     * @return
     */
    @Override
    public Lai getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurLai> getValeurs() {
        return this.valeursLai;
    }

    /**
     * Gets the valeurs lai.
     *
     * @return the valeurs lai
     */
    public List<ValeurLai> getValeursLai() {
        return this.valeursLai;
    }

    /**
     * Sets the valeurs lai.
     *
     * @param valeursLai the new valeurs lai
     */
    public void setValeursLai(List<ValeurLai> valeursLai) {
        this.valeursLai = valeursLai;
    }

    /**
     * @return
     */
    public LaiNature getLaiNature() {
        return laiNature;
    }

    /**
     * @param laiNature
     */
    public void setLaiNature(LaiNature laiNature) {
        this.laiNature = laiNature;
    }

}
