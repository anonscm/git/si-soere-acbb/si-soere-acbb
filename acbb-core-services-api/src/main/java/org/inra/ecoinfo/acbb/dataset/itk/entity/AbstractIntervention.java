package org.inra.ecoinfo.acbb.dataset.itk.entity;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import static javax.persistence.CascadeType.*;

/**
 * The Class AbstractIntervention.
 * <p>
 * generic entity for interventions
 */
@Entity(name = AbstractIntervention.INTERVENTION_NAME_ENTITIE)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorFormula(value = AbstractIntervention.ATTRIBUTE_JPA_TYPE)
@DiscriminatorValue(AbstractIntervention.OTHER)
public abstract class AbstractIntervention implements Serializable,
        Comparable<AbstractIntervention> {

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String INTERVENTION_NAME_ENTITIE = "Intervention";

    /**
     *
     */
    public static final String ID_JPA = "itk_id";

    /**
     * The Constant OTHER.
     */
    public static final String OTHER = "OTHER";
    /**
     * The Constant VARIABLE_NAME_ID @link(String).
     */
    public static final String VARIABLE_NAME_ID = "id";

    /**
     * The Constant ATTRIBUTE_JPA_ANNEE.
     */
    public static final String ATTRIBUTE_JPA_ANNEE = "annee";

    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TYPE = "type";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TEMPO = "tempo";
    /**
     * The Constant ATTRIBUTE_JPA_OBSERVATION.
     */
    public static final String ATTRIBUTE_JPA_OBSERVATION = "observation";

    static final String RESOURCE_PATH = "%s/%s/%s/%s";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The type.
     */
    @Column(name = AbstractIntervention.ATTRIBUTE_JPA_TYPE, nullable = false)
    protected String type;
    /**
     * The observation.
     */
    @Column(name = AbstractIntervention.ATTRIBUTE_JPA_OBSERVATION, nullable = true, columnDefinition = "TEXT")
    protected String observation = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = AbstractIntervention.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    Long id;

    /**
     * The version @link(VersionFile).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    VersionFile version;

    /**
     * The tempo {@link Tempo}.
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Tempo.class)
    @JoinColumn(name = Tempo.ID_JPA, referencedColumnName = Tempo.ID_JPA, nullable = true)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    Tempo tempo;

    /**
     * The date @link(Date).
     */
    @Column(name = AbstractIntervention.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate date;

    /**
     * The version traitement realisee number @link(Integer).
     */
    Integer versionTraitementRealiseeNumber;

    /**
     * The annee realisee number @link(Integer).
     */
    Integer anneeRealiseeNumber;

    /**
     * The periode realisee number @link(Integer).
     */
    Integer periodeRealiseeNumber;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SuiviParcelle.ID_JPA, referencedColumnName = SuiviParcelle.ID_JPA, nullable = false)
    SuiviParcelle suiviParcelle;

    /**
     * Instantiates a new intervention.
     */
    public AbstractIntervention() {
        super();
    }

    /**
     * Instantiates a new intervention.
     *
     *
     * @param versionFile the version file
     * @param type the type
     * @param observation the observation
     * @param date the date
     * @param versionTraitementRealisee
     * @link(int) the version traitement realisee
     * @param anneeRealisee the annee realisee
     * @param periodeRealisee the periode realisee
     * @param traitementProgramme the traitement programme
     */
    /**
     * @param versionFile
     * @param type
     * @param observation
     * @param date
     * @param versionTraitementRealisee
     * @param anneeRealisee
     * @param periodeRealisee
     */
    public AbstractIntervention(final VersionFile versionFile, final String type, final String observation, final LocalDate date, final int versionTraitementRealisee, final int anneeRealisee, final int periodeRealisee) {
        super();
        this.version = versionFile;
        this.type = type;
        this.observation = observation;
        this.setDate(date);
        this.versionTraitementRealiseeNumber = versionTraitementRealisee;
        this.anneeRealiseeNumber = anneeRealisee;
        this.periodeRealiseeNumber = periodeRealisee;
    }

    /**
     * Instantiates a new intervention.
     *
     * @param version
     * @param type
     * @param observation
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     * @param tempo
     * @link(VersionFile) the version
     * @link(String) the type
     * @link(String) the observation
     * @link(Date)
     * @link(TraitementProgramme) the traitement programme
     * @link(Integer) the version traitement realisee number
     * @link(Integer) the annee realisee number
     * @link(Integer) the periode realisee number
     * @link(Tempo) the tempo
     */
    public AbstractIntervention(final VersionFile version, final String type, final String observation, final LocalDate date, final Integer versionTraitementRealiseeNumber, final Integer anneeRealiseeNumber, final Integer periodeRealiseeNumber, final Tempo tempo) {
        super();
        this.version = version;
        this.tempo = tempo;
        this.type = type;
        this.observation = observation;
        this.setDate(date);
        this.versionTraitementRealiseeNumber = versionTraitementRealiseeNumber;
        this.anneeRealiseeNumber = anneeRealiseeNumber;
        this.periodeRealiseeNumber = periodeRealiseeNumber;
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(AbstractIntervention o) {
        if (o == null) {
            return -1;
        }
        if (!this.getClass().isInstance(o)) {
            return this.getType().compareTo(o.getType());
        }
        int compareDate = getDate().compareTo(o.getDate());
        if (compareDate != 0) {
            return compareDate;
        }
        return getSuiviParcelle().compareTo(o.getSuiviParcelle());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractIntervention other = (AbstractIntervention) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return Objects.equals(this.suiviParcelle, other.suiviParcelle);
    }

    /**
     * Gets the annee realisee number.
     *
     * @return the anneeRealiseeNumber
     */
    public Integer getAnneeRealiseeNumber() {
        return this.anneeRealiseeNumber;
    }

    /**
     * Sets the annee realisee number.
     *
     * @param anneeRealiseeNumber the anneeRealiseeNumber to set
     */
    public void setAnneeRealiseeNumber(final Integer anneeRealiseeNumber) {
        this.anneeRealiseeNumber = anneeRealiseeNumber;
    }

    /**
     * Gets the date @link(Date).
     *
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * Sets the date @link(Date).
     *
     * @param date the date to set
     */
    public void setDate(final LocalDate date) {
        this.date = date;
    }

    /**
     * Gets the id <long>.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id <long>.
     *
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the observation.
     *
     * @return the observation
     */
    public String getObservation() {
        return this.observation;
    }

    /**
     * Sets the observation.
     *
     * @param observation the observation to set
     */
    public void setObservation(final String observation) {
        this.observation = observation;
    }

    /**
     * Gets the periode realisee number.
     *
     * @return the periodeRealiseeNumber
     */
    public Integer getPeriodeRealiseeNumber() {
        return this.periodeRealiseeNumber;
    }

    /**
     * Sets the periode realisee number.
     *
     * @param periodeRealiseeNumber the periodeRealiseeNumber to set
     */
    public void setPeriodeRealiseeNumber(final Integer periodeRealiseeNumber) {
        this.periodeRealiseeNumber = periodeRealiseeNumber;
    }

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle
     */
    public void setSuiviParcelle(SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Gets the tempo {@link Tempo}.
     *
     * @return the tempo
     */
    public Tempo getTempo() {
        return this.tempo;
    }

    /**
     * Sets the tempo {@link Tempo}.
     *
     * @param tempo the tempo to set
     */
    public void setTempo(final Tempo tempo) {
        this.tempo = tempo;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets the type.
     *
     * @param type the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the version @link(VersionFile).
     *
     * @return the version
     */
    public VersionFile getVersion() {
        return this.version;
    }

    /**
     * Sets the version @link(VersionFile).
     *
     * @param version the version to set
     */
    public void setVersion(final VersionFile version) {
        this.version = version;
    }

    /**
     * Gets the version traitement realisee number.
     *
     * @return the versionTraitementRealiseeNumber
     */
    public Integer getVersionTraitementRealiseeNumber() {
        return this.versionTraitementRealiseeNumber;
    }

    /**
     * Sets the version traitement realisee number.
     *
     * @param versionTraitementRealiseeNumber the
     *                                        versionTraitementRealiseeNumber to set
     */
    public void setVersionTraitementRealiseeNumber(
            final Integer versionTraitementRealiseeNumber) {
        this.versionTraitementRealiseeNumber = versionTraitementRealiseeNumber;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[]{"id"});
    }
}
