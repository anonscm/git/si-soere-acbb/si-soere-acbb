package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class Fne.
 * <p>
 * entity for fne
 */
@Entity
@Table(name = Fne.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        Fne.ATTRIBUTE_JPA_TYPE, Fne.ATTRIBUTE_JPA_RANG, Tempo.ID_JPA,
        AbstractIntervention.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "fne_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "fne_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "fne_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "fne_parcelle_idx", columnList = SuiviParcelle.ID_JPA)})
@DiscriminatorValue(Fne.FNE_DISCRIMINATOR)
@PrimaryKeyJoinColumn(name = Fne.ID_JPA, referencedColumnName = "itk_id")
public class Fne extends AbstractIntervention implements IMesureITK<Fne, ValeurFne> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "fauche_non_exportee_fne";
    /**
     * The Constant SEMIS @link(String).
     */
    public static final String FNE_DISCRIMINATOR = "fauche_non_exportee";
    /**
     * The Constant ATTRIBUTE_JPA_RANG.
     */
    public static final String ATTRIBUTE_JPA_RANG = "rang";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant ID_JPA @link(String).
     */
    static final String ID_JPA = "fne_id";

    /**
     * The rang @link(int).
     */
    @Column(name = Fne.ATTRIBUTE_JPA_RANG, nullable = false)
    int rang = 1;

    /**
     * The fne values @link(List<MesureRecFau>).
     */
    @OneToMany(mappedBy = ValeurFne.ATTRIBUTE_JPA_FNE, cascade = ALL)
    List<ValeurFne> valeursFne = new LinkedList();

    /**
     * Instantiates a new Fne.
     */
    public Fne() {
        super();
        this.setType(Fne.FNE_DISCRIMINATOR);
    }

    /**
     * Instantiates a new fne.
     *
     * @param versionFile
     * @param observation
     * @param date
     * @param rotation
     * @param annee
     * @param periode
     * @param tempo
     * @link{VersionFile the version file
     * @link{String the observation
     * @link{Date the date
     * @link{int the rotation
     * @link{int the annee
     * @link
     * @link{TraitementProgramme the traitement programme
     * @link{Tempo the tempo
     */
    public Fne(final VersionFile versionFile, final String observation, final LocalDate date, final int rotation, final int annee, final int periode, final Tempo tempo) {
        super(versionFile, Fne.FNE_DISCRIMINATOR, observation, date, rotation, annee, periode);
        this.setTempo(tempo);
    }

    /**
     * @return the rang
     */
    public int getRang() {
        return this.rang;
    }

    /**
     * @param rang the rang to set
     */
    public void setRang(int rang) {
        this.rang = rang;
    }

    /**
     * @return
     */
    @Override
    public Fne getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurFne> getValeurs() {
        return this.valeursFne;
    }

    /**
     * Gets the valeursfne.
     *
     * @return the valeursfne
     */
    public List<ValeurFne> getValeursfne() {
        return this.valeursFne;
    }

    /**
     * Sets the valeurs fne.
     *
     * @param valeursFne the new valeurs fne
     */
    public void setValeursFne(final List<ValeurFne> valeursFne) {
        this.valeursFne = valeursFne;
    }

}
