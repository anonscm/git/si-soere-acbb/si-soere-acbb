/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface ILocalPublicationDAO.
 * <p>
 * interface of DAO use to delete file
 *
 * @author Tcherniatinsky Philippe
 */
public interface ILocalPublicationDAO extends IVersionFileDAO {

    /**
     * Removes the version.
     *
     * @param version
     * @throws PersistenceException delete the file {@link VersionFile} the version
     * @link(VersionFile) the version
     * @link(VersionFile) the version
     */
    void removeVersion(VersionFile version) throws PersistenceException;
}
