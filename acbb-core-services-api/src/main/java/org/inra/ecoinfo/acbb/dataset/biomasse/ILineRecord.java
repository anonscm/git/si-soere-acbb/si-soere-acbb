package org.inra.ecoinfo.acbb.dataset.biomasse;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import java.time.LocalDate;
import java.util.List;

/**
 * @param <T>
 * @author ptcherniati
 */
public interface ILineRecord<T extends ACBBVariableValue<IValeurQualitative>> {

    /**
     * @return
     */
    Integer getAnneeNumber();

    /**
     * @return
     */
    LocalDate getDate();

    /**
     * @return
     */
    AbstractIntervention getIntervention();

    /**
     * @return
     */
    String getObservation();

    /**
     * @return
     */
    long getOriginalLineNumber();

    /**
     * @return
     */
    Parcelle getParcelle();

    /**
     * @return
     */
    Integer getPeriodeNumber();

    /**
     * @return
     */
    Integer getRotationNumber();

    /**
     * @return
     */
    SuiviParcelle getSuiviParcelle();

    /**
     * @return
     */
    InterventionType getTypeIntervention();

    /**
     * @return
     */
    List<T> getVariablesValues();

    /**
     * @return
     */
    VersionDeTraitement getVersionDeTraitement();

}
