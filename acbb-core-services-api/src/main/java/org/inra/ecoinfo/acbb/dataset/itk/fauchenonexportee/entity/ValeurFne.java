package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;

/**
 * The Class ValeurSemis.
 */
@Entity
@Table(name = ValeurFne.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, Fne.ID_JPA}),
        indexes = {
                @Index(name = "vfne_vq_idx", columnList = ListeACBB.ID_JPA)
                ,
                @Index(name = "vfne_variable_idx", columnList = RealNode.ID_JPA)
                ,
                @Index(name = "vfne_fne_idx", columnList = Fne.ID_JPA)})
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurFne.ID_JPA), name = "id")})
public class ValeurFne extends ValeurIntervention {

    static final String ATTRIBUTE_JPA_FNE = "intervention";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "valeur_fauche_non_exportee_vfne";

    /**
     * The Constant ID_JPA.
     */
    static final String ID_JPA = "vfne_id";

    static final String FNE = "fne";

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Fne.ID_JPA, referencedColumnName = AbstractIntervention.ID_JPA, nullable = false)
    Fne intervention;

    /**
     *
     */
    public ValeurFne() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param fne
     */
    public ValeurFne(final float valeur, final RealNode realNode, final Fne fne) {
        super(valeur, realNode);
        this.intervention = fne;
    }

    /**
     * @param valeur
     * @param realNode
     * @param fne
     */
    public ValeurFne(final IValeurQualitative valeur, final RealNode realNode, final Fne fne) {
        super(valeur, realNode);
        this.intervention = fne;
    }

    /**
     * @return the recFau
     */
    public AbstractIntervention getFne() {
        return this.intervention;
    }

    /**
     * @param fne
     */
    public void setFne(Fne fne) {
        this.intervention = fne;
    }

}
