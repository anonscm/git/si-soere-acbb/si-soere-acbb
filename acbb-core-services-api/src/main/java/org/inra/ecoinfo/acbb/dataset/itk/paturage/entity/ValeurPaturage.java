package org.inra.ecoinfo.acbb.dataset.itk.paturage.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;

/**
 * The Class ValeurSemis.
 */
@Entity
@Table(name = ValeurPaturage.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, Paturage.ID_JPA}),
        indexes = {
                @Index(name = "vpat_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vpat_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vpat_pat_idx", columnList = Paturage.ID_JPA)})
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurPaturage.ID_JPA), name = "id")})
public class ValeurPaturage extends ValeurIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "valeur_paturage_vpat";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vpat_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Paturage.ID_JPA, referencedColumnName = Paturage.ID_JPA, nullable = false)
    Paturage paturage;

    /**
     *
     */
    public ValeurPaturage() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param paturage
     */
    public ValeurPaturage(final float valeur, final RealNode realNode, final Paturage paturage) {
        super(valeur, realNode);
        this.paturage = paturage;
    }

    /**
     * @param valeur
     * @param realNode
     * @param paturage
     */
    public ValeurPaturage(final IValeurQualitative valeur, final RealNode realNode, final Paturage paturage) {
        super(valeur, realNode);
        this.paturage = paturage;
    }

    /**
     * Gets the mesure semis.
     *
     * @return the mesure semis
     */
    public Paturage getPaturage() {
        return this.paturage;
    }

    /**
     * Sets the mesure semis.
     *
     * @param paturage
     */
    public void setPaturage(final Paturage paturage) {
        this.paturage = paturage;
    }

}
