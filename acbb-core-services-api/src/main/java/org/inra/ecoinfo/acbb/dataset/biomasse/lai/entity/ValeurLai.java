package org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurLai.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, Lai.ID_JPA}),
        indexes = {
                @Index(name = "vlai_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vlai_methode_idx", columnList = AbstractMethode.ID_JPA),
                @Index(name = "vlai_lai_idx", columnList = Lai.ID_JPA),
                @Index(name = "vlai_es_idx", columnList = ValeurLai.ATTRIBUTE_JPA_BIOMASSE_ES)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurLai.ID_JPA), name = "id")})
public class ValeurLai extends ValeurBiomasse<ValeurLai> {

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_BIOMASSE_ES = "lai_es";
    static final String ATTRIBUTE_JPA_LAI = "lai";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "valeur_lai_vlai";
    /**
     * The Constant ID_JPA.
     */
    static final String ID_JPA = "vlai_id";
    /**
     * The Constant AI_NAME.
     */
    static final String LAI_NAME = "lai";
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Lai.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA, nullable = false)
    Lai lai;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = true)
    @JoinColumn(name = ValeurLai.ATTRIBUTE_JPA_BIOMASSE_ES, referencedColumnName = ListeVegetation.ID_JPA)
    BiomasseEntreeSortie biomasseEntreeSortie;

    Float mediane;

    /**
     * Instantiates a new valeur ai.
     */
    public ValeurLai() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param lai
     * @param qualityIndex
     * @param methode
     * @param biomasseEntreeSortie the value of biomasseEntreeSortie
     * @param mediane              //
     */
    public ValeurLai(Float valeur, RealNode realNode, int measureNumber, float standardDeviation, Lai lai, int qualityIndex, AbstractMethode methode, BiomasseEntreeSortie biomasseEntreeSortie, Float mediane) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.lai = lai;
        this.biomasseEntreeSortie = biomasseEntreeSortie;
        this.mediane = mediane;
    }

    @Override
    public int compareTo(ValeurLai o) {
        int comparemesure = this.getLai().compareTo(o.getLai());
        int compareValeur = this.getDatatypeVariableUnite()
                .getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return compareValeur;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurBiomasse<?> other = (ValeurBiomasse<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return
     */
    public Lai getLai() {
        return this.lai;
    }

    /**
     * @param lai
     */
    public void setLai(Lai lai) {
        this.lai = lai;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    /**
     * @return
     */
    public BiomasseEntreeSortie getBiomasseEntreeSortie() {
        return biomasseEntreeSortie;
    }

    /**
     * @param biomasseEntreeSortie
     */
    public void setBiomasseEntreeSortie(BiomasseEntreeSortie biomasseEntreeSortie) {
        this.biomasseEntreeSortie = biomasseEntreeSortie;
    }

    /**
     * @return
     */
    public Float getMediane() {
        return mediane;
    }

    /**
     * @param mediane
     */
    public void setMediane(Float mediane) {
        this.mediane = mediane;
    }

}
