/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PaturageTypeAnimaux.
 */
@Entity
@DiscriminatorValue(RecFauRepousse.REC_FAU_REPOUSSE)
public class RecFauRepousse extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "recFau_repousse_id";

    /**
     * The Constant SEMIS_COUVERT_VEGETAL.
     */
    public static final String REC_FAU_REPOUSSE = "recfau_repousse";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
