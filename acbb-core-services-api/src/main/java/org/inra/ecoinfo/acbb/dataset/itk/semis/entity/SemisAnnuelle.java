/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class SemisAnnuelle.
 */
@Entity
@DiscriminatorValue(SemisAnnuelle.SEMIS_ANNUELLE)
public class SemisAnnuelle extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_annuelle_id";

    /**
     * The Constant SEMIS_ANNUELLE.
     */
    public static final String SEMIS_ANNUELLE = "sem_culture_annuelle";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
