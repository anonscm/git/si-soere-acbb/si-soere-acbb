package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity;

import org.inra.ecoinfo.acbb.dataset.biomasse.IMesureBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = HauteurVegetal.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        HauteurVegetalNature.HAV_NATURE, AbstractBiomasse.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "hv_version_idx", columnList = VersionFile.ID_JPA)
                ,
                @Index(name = "hv_nature_idx", columnList = HauteurVegetalNature.HAV_NATURE)
                ,
                @Index(name = "hv_itk_idx", columnList = AbstractIntervention.ID_JPA)
                ,
                @Index(name = "hv_date_idx", columnList = AbstractBiomasse.ATTRIBUTE_JPA_DATE)
                ,
                @Index(name = "hv_date_idx", columnList = HauteurVegetal.ATTRIBUTE_JPA_BIOMASSE_ES)
                ,
                @Index(name = "hv_suiviparcelle_idx", columnList = SuiviParcelle.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = HauteurVegetal.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA)
public class HauteurVegetal extends AbstractBiomasse implements
        IMesureBiomasse<HauteurVegetal, ValeurHauteurVegetal> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "hauteur_vegetal";
    /**
     * The Constant hauteur_vegetal.
     */
    public static final String HAUTEUR_VEGETAL = "HauteurVegetal";
    /**
     * The Constant BUNDLE_NAME.
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.messages";
    /**
     * The Constant PROPERTY_MSG_DESCRIPTION.
     */
    public static final String PROPERTY_MSG_DESCRIPTION = "PROPERTY_MSG_DESCRIPTION";
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_BIOMASSE_ES = "hav_es";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The valeurs hauteurvegetal.
     */
    @OneToMany(mappedBy = ValeurHauteurVegetal.ATTRIBUTE_JPA_HH, cascade = ALL)
    List<ValeurHauteurVegetal> valeursHV = new LinkedList();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = HauteurVegetalNature.HAV_NATURE, referencedColumnName = ListeVegetation.ID_JPA, nullable = true)
    HauteurVegetalNature hauteurVegetalNature;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = HauteurVegetal.ATTRIBUTE_JPA_BIOMASSE_ES, referencedColumnName = ListeVegetation.ID_JPA, nullable = true)
    BiomasseEntreeSortie biomasseEntreeSortie;

    // Constructeur hauteur vegetal

    /**
     *
     */
    public HauteurVegetal() {
        super();
        this.setType(HauteurVegetal.HAUTEUR_VEGETAL);
    }

    /**
     * @param valeursHV
     */
    public HauteurVegetal(List<ValeurHauteurVegetal> valeursHV) {
        super();
        this.setType(HauteurVegetal.HAUTEUR_VEGETAL);
        this.valeursHV = valeursHV;
    }

    /**
     * @param version
     * @param natureCouvert
     * @param observation
     * @param nature
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param serie
     * @param periodeRealiseeNumber
     * @param suiviParcelle
     * @param interventionType
     * @param intervention
     * @param biomasseEntreeSortie
     */
    public HauteurVegetal(VersionFile version, String natureCouvert, HauteurVegetalNature nature, String observation, LocalDate date, String serie,
                          Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber,
                          Integer periodeRealiseeNumber, SuiviParcelle suiviParcelle,
                          InterventionType interventionType, AbstractIntervention intervention,
                          BiomasseEntreeSortie biomasseEntreeSortie) {
        super(natureCouvert, version, HauteurVegetal.HAUTEUR_VEGETAL, observation, date, serie,
                versionTraitementRealiseeNumber, anneeRealiseeNumber, periodeRealiseeNumber,
                suiviParcelle, interventionType, intervention);
        this.hauteurVegetalNature = nature;
        this.biomasseEntreeSortie = biomasseEntreeSortie;
    }

    /**
     * @return
     */
    @Override
    public HauteurVegetal getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurHauteurVegetal> getValeurs() {
        return this.valeursHV;
    }

    /**
     * @return
     */
    public List<ValeurHauteurVegetal> getValeursvaleursHH() {
        return this.valeursHV;
    }

    // A VOIR

    /**
     * @param valeursHV
     */
    public void setValeursHauteurVegetal(List<ValeurHauteurVegetal> valeursHV) {
        this.valeursHV = valeursHV;
    }

    /**
     * @return
     */
    public HauteurVegetalNature getHauteurVegetalNature() {
        return hauteurVegetalNature;
    }

    /**
     * @param hauteurVegetalNature
     */
    public void setHauteurVegetalNature(HauteurVegetalNature hauteurVegetalNature) {
        this.hauteurVegetalNature = hauteurVegetalNature;
    }

    /**
     * @return
     */
    public BiomasseEntreeSortie getBiomasseEntreeSortie() {
        return this.biomasseEntreeSortie;
    }

    /**
     * @param biomasseEntreeSortie
     */
    public void setBiomasseEntreeSortie(BiomasseEntreeSortie biomasseEntreeSortie) {
        this.biomasseEntreeSortie = biomasseEntreeSortie;
    }

}
