/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PhytNamm.
 */
@Entity
@DiscriminatorValue(PhytNamm.NAMM)
public class PhytNamm extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "namm";

    /**
     * The Constant NAMM.
     */
    public static final String NAMM = "namm";

    /**
     * The Constant serialVersionUID <long>.
     */
    private static final long serialVersionUID = 1L;

}
