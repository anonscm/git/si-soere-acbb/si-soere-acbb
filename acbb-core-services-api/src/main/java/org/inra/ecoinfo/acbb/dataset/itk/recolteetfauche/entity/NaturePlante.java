/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class AutNature.
 */
@Entity
@DiscriminatorValue(NaturePlante.REC_FAU_NATURE_PLANTE)
public class NaturePlante extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "rec_fau_nature_plante_id";

    /**
     * The Constant AUT_NATURE.
     */
    public static final String REC_FAU_NATURE_PLANTE = "nature_plante";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
