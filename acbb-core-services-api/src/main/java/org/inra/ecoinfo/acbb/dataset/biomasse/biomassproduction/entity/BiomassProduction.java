package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity;

import org.inra.ecoinfo.acbb.dataset.biomasse.IMesureBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.MasseVegClass;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = BiomassProduction.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"mv_class_id", AbstractBiomasse.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "bpt_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "bpt_mvclass_idx", columnList = MasseVegClass.ID_JPA),
                @Index(name = "bpt_itk_idx", columnList = AbstractIntervention.ID_JPA),
                @Index(name = "bpt_date_idx", columnList = AbstractBiomasse.ATTRIBUTE_JPA_DATE),
                @Index(name = "bpt_suiviparcelle_idx", columnList = SuiviParcelle.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = BiomassProduction.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA)
public class BiomassProduction extends AbstractBiomasse implements
        IMesureBiomasse<BiomassProduction, ValeurBiomassProduction> {

    /**
     *
     */
    static public final String NAME_ENTITY_JPA = "biomass_production";

    /**
     * The Constant BIOMASS_PRODUCTION.
     */
    static public final String BIOMASS_PRODUCTION = "biomassProduction";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.messages";

    /**
     * The Constant PROPERTY_MSG_DESCRIPTION.
     */
    static final String PROPERTY_MSG_DESCRIPTION = "PROPERTY_MSG_DESCRIPTION";
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ID_JPA, referencedColumnName = MasseVegClass.ID_JPA, nullable = false)
    MasseVegClass masseVegClass;

    /**
     * The valeurs BiomassProduction.
     */
    @OneToMany(mappedBy = ValeurBiomassProduction.ATTRIBUTE_JPA_BPT, cascade = ALL)
    List<ValeurBiomassProduction> valeursBiomassProduction = new LinkedList();

    LocalDate dateDebutRepousse;

    /**
     *
     */
    public BiomassProduction() {
    }

    /**
     * @param natureCouvert
     * @param MasseVegClass
     * @param dateDebutRepousse
     * @param version
     * @param type
     * @param observation
     * @param date
     * @param serie
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     * @param suiviParcelle
     * @param interventionType
     * @param intervention
     */
    public BiomassProduction(String natureCouvert, MasseVegClass MasseVegClass,
                             LocalDate dateDebutRepousse, VersionFile version, String type, String observation,
                             LocalDate date, String serie, Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber,
                             Integer periodeRealiseeNumber, SuiviParcelle suiviParcelle,
                             InterventionType interventionType, AbstractIntervention intervention) {
        super(natureCouvert, version, BiomassProduction.BIOMASS_PRODUCTION, observation, date, serie,
                versionTraitementRealiseeNumber, anneeRealiseeNumber, periodeRealiseeNumber,
                suiviParcelle, interventionType, intervention);
        this.masseVegClass = MasseVegClass;
        this.dateDebutRepousse = dateDebutRepousse;
        this.type = type;
    }

    /**
     * @return
     */
    public LocalDate getDateDebutRepousse() {
        return this.dateDebutRepousse;
    }

    /**
     * @param dateDebutRepousse
     */
    public void setDateDebutRepousse(LocalDate dateDebutRepousse) {
        this.dateDebutRepousse = dateDebutRepousse;
    }

    // A VOIR

    /**
     * @return
     */
    public MasseVegClass getMasseVegClass() {
        return this.masseVegClass;
    }

    /**
     * @param MasseVegClass
     */
    public void setMasseVegClass(MasseVegClass MasseVegClass) {
        this.masseVegClass = MasseVegClass;
    }

    /**
     * @return
     */
    @Override
    public BiomassProduction getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurBiomassProduction> getValeurs() {
        return this.valeursBiomassProduction;
    }

    /**
     * @return
     */
    public List<ValeurBiomassProduction> getValeursBiomassProduction() {
        return this.valeursBiomassProduction;
    }

    /**
     * @param valeursBiomassProduction
     */
    public void setValeursBiomassProduction(List<ValeurBiomassProduction> valeursBiomassProduction) {
        this.valeursBiomassProduction = valeursBiomassProduction;
    }

    /**
     * @param valeursBiomassProduction
     */
    public void setValeurBiomassProduction(List<ValeurBiomassProduction> valeursBiomassProduction) {
        this.valeursBiomassProduction = valeursBiomassProduction;
    }

}
