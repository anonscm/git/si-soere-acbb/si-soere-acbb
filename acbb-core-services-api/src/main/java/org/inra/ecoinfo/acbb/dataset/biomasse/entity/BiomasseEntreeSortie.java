/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.entity;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(BiomasseEntreeSortie.BIOMASSE_ENTREE_SORTIE)
public class BiomasseEntreeSortie extends ListeVegetation {

    /**
     *
     */
    public static final String BIOMASSE_ENTREE_SORTIE = "biomasse_es";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
