/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.Serializable;

/**
 * interface objects used to test the frame header file.
 *
 * @author Tcherniatinsky Philippe
 */
public interface ITestHeaders extends Serializable {

    /**
     * The Constant SEPARATOR.
     */
    char SEPARATOR = ';';

    /**
     * Test headers.
     *
     * @param parser            the parser
     * @param versionFile
     * @param requestProperties
     * @param encoding          the encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return the last line number of the header
     * @throws BusinessException test the frame header of a file {@link VersionFile} the version file
     *                           {@link IRequestPropertiesACBB} the request properties {@link BadsFormatsReport}
     *                           the bads formats report {@link DatasetDescriptorACBB} the dataset descriptor
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     */
    long testHeaders(CSVParser parser, VersionFile versionFile,
                     IRequestPropertiesACBB requestProperties, String encoding,
                     BadsFormatsReport badsFormatsReport, DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException;

}
