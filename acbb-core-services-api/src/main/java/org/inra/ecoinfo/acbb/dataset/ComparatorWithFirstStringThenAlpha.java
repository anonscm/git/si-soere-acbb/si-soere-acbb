/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset;

import java.util.Comparator;

/**
 * @author tcherniatinsky
 */
public class ComparatorWithFirstStringThenAlpha implements Comparator<String> {

    private final Object firstString;

    /**
     * @param firstString
     */
    public ComparatorWithFirstStringThenAlpha(Object firstString) {
        this.firstString = firstString;
    }

    @Override
    public int compare(String o1, String o2) {
        if (o1.equals(o2)) {
            return 0;
        } else if (o1.equals(firstString)) {
            return -1;
        } else if (o2.equals(firstString)) {
            return 1;
        } else {
            return o1.compareTo(o2);
        }
    }


}
