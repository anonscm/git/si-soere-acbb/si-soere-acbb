package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.util.List;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurPhytosanitaire.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, Phytosanitaire.ID_JPA}),
        indexes = {
                @Index(name = "vphyt_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vphyt_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vphyt_phyt_idx", columnList = Phytosanitaire.ID_JPA)})
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurPhytosanitaire.ID_JPA), name = "id")})
public class ValeurPhytosanitaire extends ValeurIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "valeur_phytosanitaire_vphyt";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vphyt_id";

    /**
     *
     */
    public static final String NAME_ENTITY_VALEUR_LISTE_ITINERAIRE = "vphyt_vq";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Phytosanitaire.ID_JPA, referencedColumnName = Phytosanitaire.ID_JPA, nullable = false)
    Phytosanitaire phytosanitaire;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = ListeItineraire.class)
    @JoinTable(name = ValeurPhytosanitaire.NAME_ENTITY_VALEUR_LISTE_ITINERAIRE, joinColumns = @JoinColumn(name = ValeurPhytosanitaire.ID_JPA, referencedColumnName = ValeurPhytosanitaire.ID_JPA), inverseJoinColumns = @JoinColumn(name = ListeACBB.ID_JPA, referencedColumnName = ListeACBB.ID_JPA))
    List<ListeACBB> valeursQualitatives;

    /**
     *
     */
    public ValeurPhytosanitaire() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param phytosanitaire
     */
    public ValeurPhytosanitaire(final float valeur, final RealNode realNode, final Phytosanitaire phytosanitaire) {
        super(valeur, realNode);
        this.phytosanitaire = phytosanitaire;
    }

    /**
     * @param valeurs
     * @param realNode
     * @param phytosanitaire
     */
    public ValeurPhytosanitaire(final List<? extends IValeurQualitative> valeurs, final RealNode realNode, final Phytosanitaire phytosanitaire) {
        super(valeurs, realNode);
        this.phytosanitaire = phytosanitaire;
    }

    /**
     * @param valeur
     * @param realNode
     * @param phytosanitaire
     */
    public ValeurPhytosanitaire(final ListeItineraire valeur, final RealNode realNode, final Phytosanitaire phytosanitaire) {
        super(valeur, realNode);
        this.phytosanitaire = phytosanitaire;
    }

    /**
     * @return
     */
    public Phytosanitaire getPhytosanitaire() {
        return this.phytosanitaire;
    }

    /**
     * @param phytosanitaire
     */
    public void setPhytosanitaire(final Phytosanitaire phytosanitaire) {
        this.phytosanitaire = phytosanitaire;
    }

    /**
     * @return
     */
    @Override
    public List getValeursQualitatives() {
        return this.valeursQualitatives;
    }

    /**
     * @param valeursQualitatives
     */
    @Override
    public void setValeursQualitatives(List valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

}
