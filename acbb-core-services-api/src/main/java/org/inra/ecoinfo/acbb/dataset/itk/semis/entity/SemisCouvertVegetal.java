package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class SemisCouvertVegetal.
 */
@Entity
@DiscriminatorValue(SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL)
public class SemisCouvertVegetal extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_couvert_vegetal";

    /**
     * The Constant SEMIS_COUVERT_VEGETAL.
     */
    public static final String SEMIS_COUVERT_VEGETAL = "sem_couvert_vegetal";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * @return
     */
    @Override
    public String toString() {
        return "SemisCouvertVegetal{" + this.getValeur() + '}';
    }

}
