/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.utils.Column;

import java.util.Map;

/**
 * The Interface IRequestPropertiesITK.
 */
public interface IRequestPropertiesITK extends IRequestPropertiesACBB {

    /**
     * Gets the value columns.
     *
     * @return the value columns
     */
    Map<Integer, Column> getValueColumns();

}
