/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.identification.entity;

/**
 * @author ptcherniati
 */
public enum Sites {

    /**
     *
     */
    ESTREES_MONS("Estrées-Mons"),

    /**
     *
     */
    LAQUEUILLE("Laqueuille"),

    /**
     *
     */
    LUSIGNAN("Lusignan"),

    /**
     *
     */
    THEIX("Theix");
    private final String nom;

    Sites(String nom) {
        this.nom = nom;
    }

    /**
     * @return
     */
    public String getNom() {
        return nom;
    }

    @Override
    public String toString() {
        return nom;
    }

}
