package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.PhytNamm;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * The Class MesureSemis.
 */
@Entity
@Table(name = MesureSemis.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        AbstractIntervention.ID_JPA, MesureSemis.ATTRIBUTE_JPA_SPECIES,
        MesureSemis.ATTRIBUTE_JPA_VARIETE}),
        indexes = {
                @Index(name = "msem_sem", columnList = Semis.ID_JPA),
                @Index(name = "msem_variete", columnList = MesureSemis.ATTRIBUTE_JPA_VARIETE),
                @Index(name = "msem_espece", columnList = MesureSemis.ATTRIBUTE_JPA_SPECIES)})
public class MesureSemis implements Serializable, IMesureITK<Semis, ValeurSemis> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "mesure_semis_msem";

    /**
     * The Constant ATTRIBUTE_JPA_VARIETE.
     */
    public static final String ATTRIBUTE_JPA_SPECIES = "espece";

    /**
     * The Constant ATTRIBUTE_JPA_VARIETE.
     */
    public static final String ATTRIBUTE_JPA_VARIETE = "variete";

    /**
     * The Constant ATTRIBUTE_JPA_NAMM.
     */
    public static final String ATTRIBUTE_JPA_NAMM = "namm";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "msem_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = MesureSemis.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    /**
     * The semis @link(Semis).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = AbstractIntervention.ID_JPA, referencedColumnName = AbstractIntervention.ID_JPA, nullable = false)
    Semis semis;
    /**
     * The mesure semis @link(List<ValeurSemis>).
     */
    @OneToMany(mappedBy = "mesureSemis", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    List<ValeurSemis> valeursSemis = new LinkedList();
    /**
     * The variete @link(ListeItineraire).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = MesureSemis.ATTRIBUTE_JPA_VARIETE, referencedColumnName = ListeItineraire.ID_JPA, nullable = false)
    ListeItineraire variete;
    /**
     * The variete @link(ListeItineraire).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = MesureSemis.ATTRIBUTE_JPA_SPECIES, referencedColumnName = ListeItineraire.ID_JPA, nullable = false)
    SemisEspece espece;
    /**
     * The namm @link(PhytNamm).
     */
    @Transient
    private PhytNamm namm;

    /**
     * Instantiates a new mesure semis.
     */
    /**
     * Instantiates a new semis.
     *
     * @param tempo
     * @link(VersionFile)
     * @link(String)
     * @link(Date)
     * @link(int)
     * @link(int)
     * @link(int)
     * @link(TraitementProgramme)
     * @link(ValeurSemisAttr)
     * @link(List<SemisObjectif>)
     * @link(SemisCouvertVegetal) the couvert vegetal {@link VersionFile} the version file
     * {@link String} the observation {@link Date} the date
     * {@link AnneeRealisee} the annee realisee {@link PeriodeRealisee}
     * the periode realisee {@link TraitementProgramme} the traitement
     * programme {@link ValeurSemisAttr} the depth {@link List
     * <SemisObjectif>} the objectives {@link SemisCouvertVegetal} the
     * covert
     */
    public MesureSemis() {
        super();
    }

    /**
     * Instantiates a new mesure semis.
     *
     * @param semis
     * @link(Semis) the semis
     */
    public MesureSemis(final Semis semis) {
        super();
        this.semis = semis;
    }

    /**
     * Instantiates a new mesure semis.
     *
     * @param espece2
     * @param variete2
     * @param semis
     * @link(SemisEspece) the espece2
     * @link(ListeItineraire) the variete2
     * @link(Semis) the semis
     */
    public MesureSemis(SemisEspece espece2, ListeItineraire variete2, Semis semis) {
        super();
        this.semis = semis;
        this.espece = espece2;
        this.variete = variete2;
    }

    /**
     * Gets the variete @link(ListeItineraire).
     *
     * @return the variete @link(ListeItineraire)
     */
    public SemisEspece getEspece() {
        if (this.espece != null) {
            return this.espece;
        }
        for (ValeurSemis valeur : this.getvaleursSemis()) {
            if (valeur.isQualitative() && valeur.getValeurQualitative() instanceof SemisEspece) {
                this.espece = (SemisEspece) valeur.getValeurQualitative();
            }
        }
        return this.espece;
    }

    /**
     * Gets the id <long>.
     *
     * @return the id <long>
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the namm.
     *
     * @return the namm
     */
    public PhytNamm getNamm() {
        if (this.namm != null) {
            return this.namm;
        }
        for (ValeurSemis valeur : this.getvaleursSemis()) {
            if (valeur.isQualitative() && valeur.getValeurQualitative() instanceof PhytNamm) {
                this.namm = (PhytNamm) valeur.getValeurQualitative();
            }
        }
        return this.namm;

    }

    /**
     * Gets the semis @link(Semis).
     *
     * @return the semis @link(Semis)
     */
    public AbstractIntervention getSemis() {
        return this.semis;
    }

    /**
     * Sets the semis @link(Semis).
     *
     * @param semis the new semis @link(Semis)
     */
    public void setSemis(final Semis semis) {
        this.semis = semis;
    }

    /**
     * @return
     */
    @Override
    public Semis getSequence() {
        return this.semis;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurSemis> getValeurs() {
        return this.valeursSemis;
    }

    /**
     * Gets the valeurs semis.
     *
     * @return the valeurs semis
     */
    public List<ValeurSemis> getvaleursSemis() {
        return this.valeursSemis;
    }

    /**
     * Gets the variete @link(ListeItineraire).
     *
     * @return the variete @link(ListeItineraire)
     */
    public ListeItineraire getVariete() {
        if (this.variete != null) {
            return this.variete;
        }
        Collection<ListeItineraire> varietes = this.espece.getVarietes().values();
        for (ValeurSemis attribut : this.getvaleursSemis()) {
            if (attribut.isQualitative() && varietes.contains(attribut.getValeurQualitative())) {
                this.variete = (ListeItineraire) attribut.getValeurQualitative();
                break;
            }
        }
        return this.variete;
    }

    /**
     * Sets the mesure semis.
     *
     * @param mesureSemis the new mesure semis
     */
    public void setMesureSemis(final List<ValeurSemis> mesureSemis) {
        this.valeursSemis = mesureSemis;
    }

}
