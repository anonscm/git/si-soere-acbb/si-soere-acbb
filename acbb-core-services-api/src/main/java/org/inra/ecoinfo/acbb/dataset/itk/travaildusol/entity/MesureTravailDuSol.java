package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * The Class MesureTravailDuSol.
 */
@Entity
@Table(name = MesureTravailDuSol.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        TravailDuSol.ID_JPA, MesureTravailDuSol.ATTRIBUTE_JPA_NUMERO}),
        indexes = {
                @Index(name = "mwsol_wsol", columnList = TravailDuSol.ID_JPA)})
public class MesureTravailDuSol implements Serializable,
        IMesureITK<TravailDuSol, ValeurTravailDuSol> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "mesure_travail_du_sol_mwsol";

    /**
     * The Constant MWSOL_WSOLOBJ_NAME_ENTITY_JPA.
     */
    public static final String MWSOL_WSOLOBJ_NAME_ENTITY_JPA = "mwsol_wsolobj";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mwsol_id";

    /**
     * The Constant ATTRIBUTE_JPA_NUMERO.
     */
    public static final String ATTRIBUTE_JPA_NUMERO = "numero";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE_OUTIL.
     */
    public static final String ATTRIBUTE_JPA_TYPE_OUTIL = "type_outil";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = MesureTravailDuSol.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The travail du sol @link(TravailDuSol).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = TravailDuSol.ID_JPA, referencedColumnName = TravailDuSol.ID_JPA, nullable = false)
    TravailDuSol travailDuSol;

    /**
     * The mesure travail du sol @link(List<ValeurTravailDuSol>).
     */
    @OneToMany(mappedBy = "mesureTravailDuSol", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    List<ValeurTravailDuSol> valeursTravailDuSol = new LinkedList();

    /**
     * The numero @link(int).
     */
    @Column(name = MesureTravailDuSol.ATTRIBUTE_JPA_NUMERO, nullable = false)
    int numero = 1;

    /**
     * The observation @link(String).
     */
    @Column(name = TravailDuSol.ATTRIBUTE_JPA_OBSERVATION, nullable = false, columnDefinition = "TEXT")
    String observation = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * Instantiates a new mesure travail du sol.
     */
    public MesureTravailDuSol() {
        super();
    }

    /**
     * Instantiates a new mesure travail du sol.
     *
     * @param travailDuSol the travail du sol
     * @param observation  the observation
     * @param numero       the numero
     */
    public MesureTravailDuSol(final TravailDuSol travailDuSol, final String observation,
                              final int numero) {
        super();
        this.travailDuSol = travailDuSol;
        this.numero = numero;
        this.observation = observation;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the numero.
     *
     * @return the numero
     */
    public int getNumero() {
        return this.numero;

    }

    /**
     * Sets the numero.
     *
     * @param numero the new numero
     */
    public void setNumero(final int numero) {
        this.numero = numero;
    }

    /**
     * Gets the observation.
     *
     * @return the observation
     */
    public String getObservation() {
        return this.observation;
    }

    /**
     * Sets the observation.
     *
     * @param observation the new observation
     */
    public void setObservation(final String observation) {
        this.observation = observation;
    }

    /**
     * @return
     */
    @Override
    public TravailDuSol getSequence() {
        return this.travailDuSol;
    }

    /**
     * Gets the travail du sol.
     *
     * @return the travail du sol
     */
    public TravailDuSol getTravailDuSol() {
        return this.travailDuSol;
    }

    /**
     * Sets the travail du sol.
     *
     * @param travailDuSol the new travail du sol
     */
    public void setTravailDuSol(final TravailDuSol travailDuSol) {
        this.travailDuSol = travailDuSol;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurTravailDuSol> getValeurs() {
        return this.valeursTravailDuSol;
    }

    /**
     * Gets the mesure travail du sol.
     *
     * @return the mesure travail du sol
     */
    public List<ValeurTravailDuSol> getValeursTravailDuSol() {
        return this.valeursTravailDuSol;
    }

    /**
     * Sets the mesure travail du sol.
     *
     * @param mesureTravailDuSol the new mesure travail du sol
     */
    public void setValeursTravailDuSol(final List<ValeurTravailDuSol> mesureTravailDuSol) {
        this.valeursTravailDuSol = mesureTravailDuSol;
    }

}
