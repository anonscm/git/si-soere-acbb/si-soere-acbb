/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol;

import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;

import java.util.List;

/**
 * The Interface ISemisParameter.
 */
public interface IWsolParameter {

    /**
     * Gets the valeurs semis.
     *
     * @return the valeurs semis
     */
    List<ValeurTravailDuSol> getvaleursTravailDuSol();

    /**
     * @return
     */
    List<VariableACBB> getVariablesTravailDuSol();

    /**
     * @param variablesTravailDuSol
     */
    void setVariablesTravailDuSol(List<VariableACBB> variablesTravailDuSol);

    /**
     * Sets the valeur semis.
     *
     * @param valeursTravailDuSol
     */
    void setValeurTravailDuSol(List<ValeurTravailDuSol> valeursTravailDuSol);

}
