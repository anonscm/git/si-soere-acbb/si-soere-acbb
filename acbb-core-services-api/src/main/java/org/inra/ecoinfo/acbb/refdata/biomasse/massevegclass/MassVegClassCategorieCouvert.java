/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT)
public class MassVegClassCategorieCouvert extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_CATEGORIE_COUVERT = "mav_class_categorie_couvert";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_categorie_couvert";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
