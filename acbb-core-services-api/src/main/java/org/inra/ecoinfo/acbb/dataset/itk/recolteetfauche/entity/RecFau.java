package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class RecFau.
 */
@Entity
@Table(name = RecFau.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RecFau.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, AbstractIntervention.ATTRIBUTE_JPA_DATE,
        RecFau.ATTRIBUTE_JPA_PASSAGE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "recfau_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "recfau_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "recfau_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "recfau_parcelle_idx", columnList = SuiviParcelle.ID_JPA)})
@DiscriminatorValue(RecFau.RECOLTE_ET_FAUCHE_DESRIMINATOR)
@PrimaryKeyJoinColumn(name = RecFau.ID_JPA, referencedColumnName = "itk_id")
public class RecFau extends AbstractIntervention implements IMesureITK<RecFau, ValeurRecFau> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "recolte_et_fauche_recfau";
    /**
     * The Constant SEMIS @link(String).
     */
    public static final String RECOLTE_ET_FAUCHE_DESRIMINATOR = "recolte_et_fauche";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant ATTRIBUTE_JPA_PASSAGE @link(String).
     */
    static final String ATTRIBUTE_JPA_PASSAGE = "recfau_passage";

    @Column(name = RecFau.ATTRIBUTE_JPA_PASSAGE, nullable = false)
    Integer passage;

    /**
     * The mesures recfau @link(List<MesureRecFau>).
     */
    @OneToMany(mappedBy = ValeurRecFau.ATTRIBUTE_JPA_REC_FAU, cascade = ALL)
    List<ValeurRecFau> valeursRecFau = new LinkedList();

    /**
     * Instantiates a new semis.
     */
    public RecFau() {
        super();
        this.setType(RecFau.RECOLTE_ET_FAUCHE_DESRIMINATOR);
    }

    /**
     * Instantiates a new recfau.
     *
     * @param versionFile
     * @param periode
     * @param date
     * @param annee
     * @param rotation
     * @param observation
     * @param passage
     * @param tempo
     * @link(VersionFile) the version file
     * @link(String)
     * @link(Date) the date debut
     * @link(int) the version traitement realisee number
     * @link(int) the annee realisee number
     * @link(int)
     * @link(TraitementProgramme)
     * @link(Date) the date fin
     * @link(int)
     * @link(RecFauTypeComplementation) the type complementation
     * @link(Tempo) the tempo
     */
    public RecFau(final VersionFile versionFile, final String observation, final LocalDate date, final int rotation, final int annee, final int periode, int passage, final Tempo tempo) {
        super(versionFile, RecFau.RECOLTE_ET_FAUCHE_DESRIMINATOR, observation, date, rotation,
                annee, periode);
        this.setPassage(passage);
        this.setTempo(tempo);
    }

    /**
     * @return the passage
     */
    public Integer getPassage() {
        return this.passage;
    }

    /**
     * @param passage the passage to set
     */
    public void setPassage(Integer passage) {
        this.passage = passage;
    }

    /**
     * @return
     */
    @Override
    public RecFau getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurRecFau> getValeurs() {
        return this.valeursRecFau;
    }

    /**
     * Gets the mesures recfau.
     *
     * @return the mesuresRecFau
     */
    public List<ValeurRecFau> getValeursRecFau() {
        return this.valeursRecFau;
    }

    /**
     * @param valeursRecFau
     */
    public void setValeursRecFau(final List<ValeurRecFau> valeursRecFau) {
        this.valeursRecFau = valeursRecFau;
    }

}
