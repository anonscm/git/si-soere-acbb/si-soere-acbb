/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class AutNature.
 */
@Entity
@DiscriminatorValue(AutNature.AUT_NATURE)
public class AutNature extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "aut_nature_id";

    /**
     * The Constant AUT_NATURE.
     */
    public static final String AUT_NATURE = "aut_nature";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
