package org.inra.ecoinfo.acbb.dataset.itk.paturage.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class Paturage.
 */
@Entity
@Table(name = Paturage.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        Paturage.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, AbstractIntervention.ATTRIBUTE_JPA_DATE,
        Paturage.ATTRIBUTE_JPA_END_DATE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "pat_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "pat_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "pat_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "pat_parcelle_idx", columnList = SuiviParcelle.ID_JPA)})
@DiscriminatorValue(Paturage.PATURAGE_DESCRIMINATOR)
@PrimaryKeyJoinColumn(name = Paturage.ID_JPA, referencedColumnName = "itk_id")
public class Paturage extends AbstractIntervention implements IMesureITK<Paturage, ValeurPaturage> {

    /**
     * The Constant SEMIS @link(String).
     */
    public static final String PATURAGE_DESCRIMINATOR = "paturage";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "paturage_pat";
    /**
     * The Constant ATTRIBUTE_JPA_BEGIN_DATE.
     */
    static final String ATTRIBUTE_JPA_BEGIN_DATE = "pat_date_debut";

    /**
     * The Constant ATTRIBUTE_JPA_END_DATE.
     */
    static final String ATTRIBUTE_JPA_END_DATE = "pat_date_fin";

    /**
     * The Constant ATTRIBUTE_JPA_PASSAGE @link(String).
     */
    static final String ATTRIBUTE_JPA_PASSAGE = "pat_passage";

    /**
     * The date fin @link(Date).
     */
    @Column(name = Paturage.ATTRIBUTE_JPA_END_DATE)
    LocalDate dateFin;

    /**
     * The mesures paturage @link(List<MesurePaturage>).
     */
    @OneToMany(mappedBy = "paturage", cascade = ALL)
    List<ValeurPaturage> valeursPaturage = new LinkedList();

    /**
     * Instantiates a new semis.
     */
    public Paturage() {
        super();
        this.setType(Paturage.PATURAGE_DESCRIMINATOR);
    }

    /**
     * Instantiates a new paturage.
     *
     * @param versionFile
     * @param periode
     * @param annee
     * @param rotation
     * @param observation
     * @param dateDebut
     * @param dateFin
     * @param tempo
     * @link(VersionFile) the version file
     * @link(String) the observation
     * @link(Date) the date debut
     * @link(int) the version traitement realisee number
     * @link(int) the annee realisee number
     * @link(int)
     * @link(TraitementProgramme) the traitement programme
     * @link(Date)
     * @link(int)
     * @link(PaturageTypeComplementation) the type complementation
     * @link(Tempo) the tempo
     */
    public Paturage(final VersionFile versionFile, final String observation, final LocalDate dateDebut, final int rotation, final int annee, final int periode, final LocalDate dateFin, final Tempo tempo) {
        super(versionFile, Paturage.PATURAGE_DESCRIMINATOR, observation, dateDebut, rotation,
                annee, periode);
        this.setDateFin(dateFin);
        this.setTempo(tempo);
    }

    /**
     * Gets the date debut.
     *
     * @return the dateDeFin
     */
    public LocalDate getDateDebut() {
        return this.getDate();
    }

    /**
     * Sets the date debut.
     *
     * @param dateDebut the new date debut
     */
    public void setDateDebut(final LocalDate dateDebut) {
        this.setDate(dateDebut);
    }

    /**
     * Gets the date de fin.
     *
     * @return the dateDeFin
     */
    public LocalDate getDateDeFin() {
        return this.dateFin;
    }

    /**
     * @return
     */
    @Override
    public Paturage getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurPaturage> getValeurs() {
        return this.valeursPaturage;
    }

    /**
     * Gets the mesures paturage.
     *
     * @return the mesuresPaturage
     */
    public List<ValeurPaturage> getValeursPaturage() {
        return this.valeursPaturage;
    }

    /**
     * @param valeursPaturage
     */
    public void setValeursPaturage(final List<ValeurPaturage> valeursPaturage) {
        this.valeursPaturage = valeursPaturage;
    }

    /**
     * Sets the date fin.
     *
     * @param dateFin the new date fin
     */
    public void setDateFin(final LocalDate dateFin) {
        this.dateFin = dateFin;
    }

}
