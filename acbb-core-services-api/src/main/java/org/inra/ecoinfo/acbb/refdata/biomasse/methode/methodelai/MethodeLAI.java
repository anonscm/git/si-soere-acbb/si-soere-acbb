/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodelai;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import javax.persistence.Entity;
import javax.persistence.Table;

// import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
/**
 * The Class MethodeLai.
 */

/**
 * @author koyao
 */
@Entity
@Table(name = MethodeLAI.NAME_ENTITY_JPA)
public class MethodeLAI extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_lai";

    // CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public MethodeLAI() {
        super();
    }

    /**
     * @param libelle
     * @param definition
     * @param numberId
     */
    public MethodeLAI(String libelle, String definition, Long numberId) {
        super(libelle, definition, numberId);
    }

    /**
     * @param definition
     */
    public void update(String definition) {
        this.definition = definition;
    }

}
