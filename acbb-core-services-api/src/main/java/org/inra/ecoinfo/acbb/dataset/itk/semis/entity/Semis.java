package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class Semis.
 */
@Entity
@Table(name = Semis.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        Semis.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, Semis.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "sem_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "sem_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "sem_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "sem_parcelle_idx", columnList = SuiviParcelle.ID_JPA)})
@DiscriminatorValue(Semis.SEMIS_DESCRIMINATOR)
//@AttributeOverrides(value = { @AttributeOverride(column = @Column(name = Semis.ID_JPA), name = "id") })
public class Semis extends AbstractIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "semis_sem";
    /**
     * The Constant SEMIS_DESCRIMINATOR @link(String).
     */
    public static final String SEMIS_DESCRIMINATOR = "semis";

    /**
     * The Constant ATTRIBUTE_JPA_COUVERT_VEGETAL.
     */
    public static final String ATTRIBUTE_JPA_COUVERT_VEGETAL = "sem_couvert_vegetal";

    /**
     * The Constant ATTRIBUTE_JPA_PROFONDEUR.
     */
    public static final String ATTRIBUTE_JPA_PROFONDEUR = "sem_profondeur";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The mesures semis @link(List<MesureSemis>).
     */
    @OneToMany(mappedBy = "semis", cascade = ALL)
    @OrderBy("variete")
    List<MesureSemis> mesuresSemis = new LinkedList();
    /**
     * The mesures semis @link(List<MesureSemis>).
     */
    @OneToMany(mappedBy = "semis", cascade = ALL)
    List<ValeurSemisAttr> attributs = new LinkedList();
    /**
     * The objectifs @link(List<SemisObjectif>).
     */
    @Transient
    private List<SemisObjectif> objectifs;

    /**
     * Instantiates a new semis.
     */
    public Semis() {
        super();
        this.setType(Semis.SEMIS_DESCRIMINATOR);
    }

    /**
     * Instantiates a new semis.
     *
     * @param versionFile
     * @param observation
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     * @param tempo
     * @link(VersionFile) the version file
     * @link(String) the observation
     * @link(Date) the date
     * @link(int) the version traitement realisee number
     * @link(int) the annee realisee number
     * @link(int)
     * @link(TraitementProgramme) the traitement programme
     * @link(Tempo) the tempo
     */
    public Semis(final VersionFile versionFile, final String observation, final LocalDate date, final int versionTraitementRealiseeNumber, final int anneeRealiseeNumber, final int periodeRealiseeNumber, final Tempo tempo) {
        super(versionFile, Semis.SEMIS_DESCRIMINATOR, observation, date,
                versionTraitementRealiseeNumber, anneeRealiseeNumber, periodeRealiseeNumber);
        this.setTempo(tempo);
    }

    /**
     * Adds the objectifs.
     *
     * @param objectifs
     * @link(List<SemisObjectif>) the objectifs
     */
    public void addObjectifs(List<SemisObjectif> objectifs) {
        for (SemisObjectif objectif : objectifs) {
            if (this.getObjectifs().contains(objectif)) {
                this.getObjectifs().add(objectif);
            }
        }
    }

    /**
     * Gets the mesures semis @link(List<MesureSemis>).
     *
     * @return the attributs
     */
    public List<ValeurSemisAttr> getAttributs() {
        return this.attributs;
    }

    /**
     * Sets the mesures semis @link(List<MesureSemis>).
     *
     * @param attributs the attributs to set
     */
    public void setAttributs(List<ValeurSemisAttr> attributs) {
        this.attributs = attributs;
    }

    /**
     * Gets the couvert vegetal.
     *
     * @return the couvert vegetal
     */
    public SemisCouvertVegetal getCouvertVegetal() {
        for (ValeurSemisAttr attribut : this.getAttributs()) {
            if (attribut.isQualitative()
                    && attribut.getValeurQualitative() instanceof SemisCouvertVegetal) {
                return (SemisCouvertVegetal) attribut.getValeurQualitative();
            }
        }
        return null;
    }

    /**
     * Gets the mesures semis.
     *
     * @return the mesures semis
     */
    public List<MesureSemis> getMesuresSemis() {
        return this.mesuresSemis;
    }

    /**
     * Sets the mesures semis.
     *
     * @param mesuresSemis the new mesures semis
     */
    public void setMesuresSemis(final List<MesureSemis> mesuresSemis) {
        this.mesuresSemis = mesuresSemis;
    }

    /**
     * Gets the objectifs @link(List<SemisObjectif>).
     *
     * @return the objectifs
     */
    public List<SemisObjectif> getObjectifs() {
        if (this.objectifs != null) {
            return this.objectifs;
        }
        this.objectifs = new LinkedList();
        for (ValeurSemisAttr attribut : this.getAttributs()) {
            if (attribut.isQualitative()
                    && attribut.getValeurQualitative() instanceof SemisObjectif) {
                this.objectifs.add((SemisObjectif) attribut.getValeurQualitative());
            }
        }
        return this.objectifs;
    }

    /**
     * Gets the depeth {@link ValeurSemisAttr}.
     *
     * @return the depth {@link ValeurSemisAttr}
     */
    public Float getProfondeur() {
        for (ValeurSemisAttr attribut : this.getAttributs()) {
            if (!attribut.isQualitative()) {
                return attribut.getValeur();
            }
        }
        return null;
    }

    /**
     * Checks if is culture annuelle.
     *
     * @return true, if is culture annuelle
     */
    public boolean isCultureAnnuelle() {
        for (ValeurSemisAttr attribut : this.getAttributs()) {
            if (attribut.isQualitative()
                    && attribut.getValeurQualitative() instanceof SemisAnnuelle) {
                return Boolean.TRUE.toString().equalsIgnoreCase(
                        ((SemisAnnuelle) attribut.getValeurQualitative()).getValeur());
            }
        }
        return false;
    }

}
