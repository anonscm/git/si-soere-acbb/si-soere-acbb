package org.inra.ecoinfo.acbb.refdata.bloc;

import org.inra.ecoinfo.acbb.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class Bloc.
 */
@Entity
@Table(name = Bloc.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        SiteACBB.ID_JPA, "code", Bloc.BLOC_NAME_PARENT_ID}))
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@AttributeOverrides({
        @AttributeOverride(name = AbstractRecurentObject.RECURENT_NAME_ID, column = @Column(name = Bloc.ID_JPA))
        ,
        @AttributeOverride(name = AbstractRecurentObject.RECURENT_NAME_PARENT_ID, column = @Column(name = Bloc.BLOC_NAME_PARENT_ID))
        ,
        @AttributeOverride(name = AbstractRecurentObject.RECURENT_NAME_CODE_ID, column = @Column(unique = false))})
@AssociationOverride(name = AbstractRecurentObject.RECURENT_NAME_COLUMN_PARENT_ID, joinColumns = @JoinColumn(name = Bloc.BLOC_NAME_PARENT_ID, nullable = true))
public class Bloc extends AbstractRecurentObject<Bloc> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "bloc_bloc";

    /**
     * The Constant ATTRIBUTE_JPA_DESCRIPTION @link(String).
     */
    public static final String ATTRIBUTE_JPA_DESCRIPTION = "description";

    /**
     * The Constant BLOC_NAME_PARENT_ID.
     */
    public static final String BLOC_NAME_PARENT_ID = AbstractRecurentObject.RECURENT_NAME_PARENT_ID;

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "bloc_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The site @link(SiteACBB).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SiteACBB.ID_JPA, referencedColumnName = SiteACBB.ID_JPA, nullable = false)
    SiteACBB site;

    /**
     * The parcelles @link(List<Parcelle>).
     */
    @OneToMany(mappedBy = "bloc", cascade = ALL)
    List<Parcelle> parcelles = new LinkedList();

    /**
     * The date creation @link(Date).
     */
    @Column(nullable = false)
    LocalDate dateCreation;

    /**
     * The description @link(String).
     */
    @Column(name = Bloc.ATTRIBUTE_JPA_DESCRIPTION)
    String description = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * Instantiates a new bloc.
     */
    public Bloc() {
        super();
    }

    /**
     * Instantiates a new bloc.
     *
     * @param nom          the nom
     * @param dateCreation the date creation
     * @param description  the description
     * @param parent       the parent
     */
    public Bloc(final String nom, final LocalDate dateCreation, final String description,
                final Bloc parent) {
        super();
        this.setParent(parent);
        this.setNom(nom);
        this.setDateCreation(dateCreation);
        this.setDescription(description);
    }

    /**
     * @return
     */
    public String getBlocName() {
        if (this.parent != null) {
            return this.parent.getNom();
        } else {
            return this.nom;
        }
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public LocalDate getDateCreation() {
        return this.dateCreation;
    }

    /**
     * Sets the date creation.
     *
     * @param dateCreation the new date creation
     */
    public void setDateCreation(final LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description == null ? org.apache.commons.lang.StringUtils.EMPTY : description;
    }

    /**
     * Gets the parcelles.
     *
     * @return the parcelles
     */
    public List<Parcelle> getParcelles() {
        return this.parcelles;
    }

    /**
     * Sets the parcelles.
     *
     * @param parcelles the new parcelles
     */
    public void setParcelles(final List<Parcelle> parcelles) {
        this.parcelles = parcelles;
    }

    /**
     * @return
     */
    public String getRepetitionName() {
        if (this.parent != null) {
            return this.nom;
        } else {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public SiteACBB getSite() {
        return this.site;
    }

    /**
     * Sets the site.
     *
     * @param siteACBB the new site
     */
    public void setSite(final SiteACBB siteACBB) {
        this.site = siteACBB;
    }
}
