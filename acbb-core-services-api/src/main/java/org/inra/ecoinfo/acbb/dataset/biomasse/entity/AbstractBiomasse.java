/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.entity;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import static javax.persistence.CascadeType.*;

/**
 * @author ptcherniati
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorFormula(value = AbstractBiomasse.ATTRIBUTE_JPA_TYPE)
@DiscriminatorValue(AbstractBiomasse.OTHER)
public abstract class AbstractBiomasse implements Serializable, Comparable<AbstractBiomasse> {

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "biomasse_id";

    /**
     * The Constant OTHER.
     */
    public static final String OTHER = "OTHER";

    /**
     * The Constant VARIABLE_NAME_ID @link(String).
     */
    public static final String VARIABLE_NAME_ID = "id";

    /**
     * The Constant ATTRIBUTE_JPA_ANNEE.
     */
    public static final String ATTRIBUTE_JPA_ANNEE = "annee";
    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TYPE = "type";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_INTERVENTION_TYPE = "intervention_type";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_INTERVENTION_DATE = "intervention_date";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TEMPO = "tempo";

    /**
     * The Constant ATTRIBUTE_JPA_OBSERVATION.
     */
    public static final String ATTRIBUTE_JPA_OBSERVATION = "observation";

    /**
     * The Constant NEW_LINE.
     */
    protected static final String NEW_LINE = "%n";

    static final String RESOURCE_PATH = "%s/%s/%s/%s";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The type.
     */
    @Column(name = AbstractBiomasse.ATTRIBUTE_JPA_TYPE, nullable = false)
    protected String type;

    /**
     * The observation.
     */
    @Column(name = AbstractBiomasse.ATTRIBUTE_JPA_OBSERVATION, nullable = true, columnDefinition = "TEXT")
    protected String observation = org.apache.commons.lang.StringUtils.EMPTY;
    /**
     * The intervention type.
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = InterventionType.ID_JPA, referencedColumnName = ListeACBB.ID_JPA, nullable = true)
    protected InterventionType interventionType;
    /**
     * The intervention.
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = AbstractIntervention.ID_JPA, referencedColumnName = AbstractIntervention.ID_JPA, nullable = true)
    protected AbstractIntervention intervention;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = AbstractBiomasse.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    Long id;
    /**
     * The version @link(VersionFile).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(value = LazyToOneOption.PROXY)
    VersionFile version;

    /**
     * The date @link(Date).
     */
    @Column(name = AbstractBiomasse.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate date;

    String serie;

    /**
     * The version traitement realisee number @link(Integer).
     */
    Integer versionTraitementRealiseeNumber;

    /**
     * The annee realisee number @link(Integer).
     */
    Integer anneeRealiseeNumber;

    /**
     * The periode realisee number @link(Integer).
     */
    Integer periodeRealiseeNumber;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SuiviParcelle.ID_JPA, referencedColumnName = SuiviParcelle.ID_JPA, nullable = false)
    SuiviParcelle suiviParcelle;
    String natureCouvert;

    /**
     * Instantiates a new intervention.
     */
    public AbstractBiomasse() {
        super();
    }

    /**
     * @param natureCouvert
     * @param version
     * @param type
     * @param observation
     * @param date
     * @param serie
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     * @param suiviParcelle
     * @param interventionType
     * @param intervention
     */
    public AbstractBiomasse(String natureCouvert, VersionFile version, String type, String observation, LocalDate date, String serie,
                            Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber,
                            Integer periodeRealiseeNumber, SuiviParcelle suiviParcelle,
                            InterventionType interventionType, AbstractIntervention intervention) {
        super();
        this.version = version;
        this.type = type;
        this.observation = observation;
        this.date = date;
        this.serie = serie;
        this.versionTraitementRealiseeNumber = versionTraitementRealiseeNumber;
        this.anneeRealiseeNumber = anneeRealiseeNumber;
        this.periodeRealiseeNumber = periodeRealiseeNumber;
        this.suiviParcelle = suiviParcelle;
        this.intervention = intervention;
        this.interventionType = interventionType;
        this.natureCouvert = natureCouvert;

    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(AbstractBiomasse o) {
        if (o == null) {
            return -1;
        }
        if (!this.getClass().isInstance(o)) {
            return this.getType().compareTo(o.getType());
        }
        final int compareSite = this.getSuiviParcelle().getParcelle().getSite().getCode()
                .compareTo(o.getSuiviParcelle().getParcelle().getSite().getCode());
        final int comparetraitement = this.getSuiviParcelle().getTraitement().getCode()
                .compareTo(o.getSuiviParcelle().getTraitement().getCode());
        final int compareParcelle = this.getSuiviParcelle().getParcelle().getCode()
                .compareTo(o.getSuiviParcelle().getParcelle().getCode());
        if (compareSite != 0) {
            return compareSite;
        } else if (comparetraitement != 0) {
            return comparetraitement;
        } else if (compareParcelle != 0) {
            return compareParcelle;
        }
        return this.getDate().compareTo(o.date);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractBiomasse other = (AbstractBiomasse) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return Objects.equals(this.suiviParcelle, other.suiviParcelle);
    }

    /**
     * @return
     */
    public Integer getAnneeRealiseeNumber() {
        return this.anneeRealiseeNumber;
    }

    /**
     * @param anneeRealiseeNumber
     */
    public void setAnneeRealiseeNumber(Integer anneeRealiseeNumber) {
        this.anneeRealiseeNumber = anneeRealiseeNumber;
    }

    /**
     * @return
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     */
    public AbstractIntervention getIntervention() {
        return this.intervention;
    }

    /**
     * @param intervention
     */
    public void setIntervention(AbstractIntervention intervention) {
        this.intervention = intervention;
    }

    /**
     * @return
     */
    public InterventionType getInterventionType() {
        return this.interventionType;
    }

    /**
     * @param interventionType
     */
    public void setInterventionType(InterventionType interventionType) {
        this.interventionType = interventionType;
    }

    /**
     * @return
     */
    public String getObservation() {
        return this.observation;
    }

    /**
     * @param observation
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * @return
     */
    public Integer getPeriodeRealiseeNumber() {
        return this.periodeRealiseeNumber;
    }

    /**
     * @param periodeRealiseeNumber
     */
    public void setPeriodeRealiseeNumber(Integer periodeRealiseeNumber) {
        this.periodeRealiseeNumber = periodeRealiseeNumber;
    }

    /**
     * @return
     */
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * @param suiviParcelle
     */
    public void setSuiviParcelle(SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * @return
     */
    public String getType() {
        return this.type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return
     */
    public VersionFile getVersion() {
        return this.version;
    }

    /**
     * @param version
     */
    public void setVersion(VersionFile version) {
        this.version = version;
    }

    /**
     * @return
     */
    public Integer getVersionTraitementRealiseeNumber() {
        return this.versionTraitementRealiseeNumber;
    }

    /**
     * @param versionTraitementRealiseeNumber
     */
    public void setVersionTraitementRealiseeNumber(Integer versionTraitementRealiseeNumber) {
        this.versionTraitementRealiseeNumber = versionTraitementRealiseeNumber;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[]{"id"});
    }

    /**
     * @return
     */
    public String getNatureCouvert() {
        return this.natureCouvert;
    }

    /**
     * @param natureCouvert
     */
    public void setNatureCouvert(String natureCouvert) {
        this.natureCouvert = natureCouvert;
    }

    /**
     * @return
     */
    public String getSerie() {
        return serie;
    }

    /**
     * @param serie
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }
}
