/*
 *
 */
package org.inra.ecoinfo.acbb.utils.vo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class SynthesisVariableVO.
 */
public class SynthesisVariableVO implements Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The dates plots @link(List<DatePlotVO>).
     */
    List<DatePlotVO> datesPlots = new LinkedList();

    /**
     * The variable @link(String).
     */
    String variable;

    /**
     * Instantiates a new synthesis variable vo.
     */
    public SynthesisVariableVO() {
        super();
    }

    /**
     * Instantiates a new synthesis variable vo.
     *
     * @param variable the variable
     */
    public SynthesisVariableVO(final String variable) {
        super();
        this.variable = variable;
    }

    /**
     * Gets the dates plots.
     *
     * @return the dates plots
     */
    public List<DatePlotVO> getDatesPlots() {
        return this.datesPlots;
    }

    /**
     * Sets the dates plots.
     *
     * @param datesPlots the new dates plots
     */
    public void setDatesPlots(final List<DatePlotVO> datesPlots) {
        this.datesPlots = datesPlots;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public String getVariable() {
        return this.variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final String variable) {
        this.variable = variable;
    }

}
