/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.versiontraitement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The Class VersionDeTraitement.
 */
@Entity
@Table(name = VersionDeTraitement.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        TraitementProgramme.ID_JPA, VersionDeTraitement.ATTRIBUTE_JPA_VERSION}),
        indexes = {
                @Index(name = "vdt_date_deb_idx", columnList = "dateDebutVersionTraitement")
                ,
                @Index(name = "vdt_date_fin_idx", columnList = "dateFinVersionTraitement")
                ,
                @Index(name = "vdt_date_deb_tra_idx", columnList = "dateDebutVersionTraitement," + TraitementProgramme.ID_JPA)
                ,
                @Index(name = "vdt_tra_idx", columnList = TraitementProgramme.ID_JPA)})
public class VersionDeTraitement implements Serializable, Comparable<VersionDeTraitement> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "version_traitement_vtra";

    /**
     * The Constant NAME_VTRA_MOD_ENTITY_JPA.
     */
    public static final String NAME_VTRA_MOD_ENTITY_JPA = "version_traitement_modalite_vtmod";

    /**
     * The Constant ATTRIBUTE_JPA_MODALITES.
     */
    public static final String ATTRIBUTE_JPA_MODALITES = "modalites";

    /**
     * The Constant ATTRIBUTE_JPA_COMMENTAIRE.
     */
    public static final String ATTRIBUTE_JPA_COMMENTAIRE = "commentaire";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vtra_id";

    /**
     * The Constant ATTRIBUTE_JPA_VERSION.
     */
    public static final String ATTRIBUTE_JPA_VERSION = "version";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The versions de traitementrealisees
     *
     * @link(Map<Integer,VersionTraitementRealisee>).
     */
    @Transient
    @OneToMany(targetEntity = VersionTraitementRealisee.class)
    @MapKey(name = VersionTraitementRealisee.ATTRIBUTE_JPA_NUMERO)
    final Map<Integer, VersionTraitementRealisee> versionsDeTraitementrealisees = new HashMap();
    /**
     * The id <long>.
     */
    @Id
    @Column(name = VersionDeTraitement.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    /**
     * The version @link(int).
     */
    @Column(nullable = false, name = VersionDeTraitement.ATTRIBUTE_JPA_VERSION)
    @NaturalId
    int version;
    /**
     * The date debut version traitement @link(Date).
     */
    @Column(nullable = false)
    LocalDate dateDebutVersionTraitement;
    /**
     * The date fin version traitement @link(Date).
     */
    LocalDate dateFinVersionTraitement;
    /**
     * The commentaire @link(String).
     */
    @Column(nullable = false, name = VersionDeTraitement.ATTRIBUTE_JPA_COMMENTAIRE)
    String commentaire;
    /**
     * The traitement programme @link(TraitementProgramme).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = TraitementProgramme.ID_JPA, referencedColumnName = TraitementProgramme.ID_JPA, nullable = false)
    @NaturalId
    TraitementProgramme traitementProgramme;
    /**
     * The modalites @link(List<Modalite>).
     */
    @ManyToMany(targetEntity = Modalite.class)
    @JoinTable(uniqueConstraints = @UniqueConstraint(columnNames = {VersionDeTraitement.ID_JPA,
            Modalite.ID_JPA}), name = VersionDeTraitement.NAME_VTRA_MOD_ENTITY_JPA, joinColumns = @JoinColumn(name = VersionDeTraitement.ID_JPA, referencedColumnName = VersionDeTraitement.ID_JPA), inverseJoinColumns = @JoinColumn(name = Modalite.ID_JPA, referencedColumnName = Modalite.ID_JPA))
    @OrderBy(value = "code")
    SortedSet<Modalite> modalites = new TreeSet();

    /**
     * Instantiates a new version de traitement.
     */
    public VersionDeTraitement() {
        super();
    }

    /**
     * Instantiates a new version de traitement.
     *
     * @param version                    the version
     * @param dateDebutVersionTraitement the date debut version traitement
     * @param dateFinVersionTraitement   the date fin version traitement
     * @param commentaire                the commentaire
     * @param traitementProgramme        the traitement programme
     * @param modalites                  the modalites
     */
    public VersionDeTraitement(final int version, final LocalDate dateDebutVersionTraitement,
                               final LocalDate dateFinVersionTraitement, final String commentaire,
                               final TraitementProgramme traitementProgramme, final SortedSet<Modalite> modalites) {
        super();
        this.version = version;
        this.setDateDebutVersionTraitement(dateDebutVersionTraitement);
        this.setDateFinVersionTraitement(dateFinVersionTraitement);
        this.commentaire = commentaire;
        this.traitementProgramme = traitementProgramme;
        this.modalites = modalites;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(VersionDeTraitement) the o
     */
    @Override
    public int compareTo(final VersionDeTraitement o) {
        if (this.traitementProgramme.equals(o.traitementProgramme)) {
            return o.traitementProgramme.compareTo(this.traitementProgramme);
        }
        return Integer.compare(this.version, o.version);
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful @see java.lang.Object#equals(java.lang.Object)
     * @link(Object) the obj
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id"});
    }

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return this.commentaire;
    }

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Gets the date debut version traitement.
     *
     * @return the date debut version traitement
     */
    public LocalDate getDateDebutVersionTraitement() {
        return dateDebutVersionTraitement;
    }

    /**
     * Sets the date debut version traitement.
     *
     * @param dateDebutVersionTraitement the new date debut version traitement
     */
    public void setDateDebutVersionTraitement(final LocalDate dateDebutVersionTraitement) {
        this.dateDebutVersionTraitement = dateDebutVersionTraitement;
    }

    /**
     * Gets the date fin version traitement.
     *
     * @return the date fin version traitement
     */
    public LocalDate getDateFinVersionTraitement() {
        return this.dateFinVersionTraitement;
    }

    /**
     * Sets the date fin version traitement.
     *
     * @param dateFinVersionTraitement the new date fin version traitement
     */
    public void setDateFinVersionTraitement(final LocalDate dateFinVersionTraitement) {
        this.dateFinVersionTraitement = dateFinVersionTraitement;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the modalites.
     *
     * @return the modalites
     */
    public SortedSet<Modalite> getModalites() {
        return this.modalites;
    }

    /**
     * Sets the modalites.
     *
     * @param modalites the new modalites
     */
    public void setModalites(final SortedSet<Modalite> modalites) {
        this.modalites = modalites;
    }

    /**
     * Gets the traitement programme.
     *
     * @return the traitement programme
     */
    public TraitementProgramme getTraitementProgramme() {
        return this.traitementProgramme;
    }

    /**
     * Sets the traitement programme.
     *
     * @param traitementProgramme the new traitement programme
     */
    public void setTraitementProgramme(final TraitementProgramme traitementProgramme) {
        this.traitementProgramme = traitementProgramme;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /**
     * Gets the versions de traitement realisees.
     *
     * @return the versionDeTraitementrealisee
     */
    public Map<Integer, VersionTraitementRealisee> getVersionsDeTraitementRealisees() {
        return this.versionsDeTraitementrealisees;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
