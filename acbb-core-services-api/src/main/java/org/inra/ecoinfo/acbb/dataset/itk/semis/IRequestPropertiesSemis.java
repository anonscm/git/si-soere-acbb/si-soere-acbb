/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;
import org.inra.ecoinfo.utils.Column;

import java.util.Map;

/**
 * The Interface IRequestPropertiesSemis.
 */
public interface IRequestPropertiesSemis extends IRequestPropertiesITK {

    /**
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK#getValueColumns()
     */
    @Override
    Map<Integer, Column> getValueColumns();

}
