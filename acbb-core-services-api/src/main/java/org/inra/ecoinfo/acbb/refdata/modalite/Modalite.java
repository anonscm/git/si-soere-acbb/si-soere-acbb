package org.inra.ecoinfo.acbb.refdata.modalite;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.variablededescription.VariableDescription;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The Class Modalite.
 */
@Entity
@Table(name = Modalite.NAME_ENTITY_JPA, uniqueConstraints = {@UniqueConstraint(columnNames = {
        Modalite.ATTRIBUTE_JPA_CODE, VariableDescription.ID_JPA})},
        indexes = {
                @Index(name = "mod_var_idx", columnList = VariableDescription.ID_JPA),
                @Index(name = "mod_vtra_code_idx", columnList = VariableDescription.ID_JPA + "," + Modalite.ATTRIBUTE_JPA_CODE)})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorFormula(value = "case when code ~* '^r[0-9]*' then '" + Rotation.ROTATION
        + "' else '" + Modalite.OTHER + "' end")
@DiscriminatorValue(Modalite.OTHER)
public class Modalite implements Serializable, Comparable<Modalite> {

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String OTHER = "Other";

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ATTRIBUTE_JPA_CODE = "code";

    /**
     * The Constant ATTRIBUTE_JPA_NOM.
     */
    public static final String ATTRIBUTE_JPA_NOM = "nom";

    /**
     * The Constant ATTRIBUTE_JPA_DESCRIPTION.
     */
    public static final String ATTRIBUTE_JPA_DESCRIPTION = "description";

    /**
     * The Constant ATTRIBUTE_JPA_VERSION_TRAITEMENT.
     */
    public static final String ATTRIBUTE_JPA_VERSION_TRAITEMENT = "version_traitement";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "modalite_mod";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mod_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    // ATTRIBUTS
    /**
     * The id <long>.
     */
    @Id
    @Column(name = Modalite.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The nom @link(String).
     */
    @Column(nullable = false, name = Modalite.ATTRIBUTE_JPA_NOM)
    String nom;

    /**
     * The code @link(String).
     */
    @Column(nullable = false, name = Modalite.ATTRIBUTE_JPA_CODE)
    @NaturalId
    String code;

    /**
     * The description @link(String).
     */
    @Column(nullable = true, name = Modalite.ATTRIBUTE_JPA_DESCRIPTION)
    String description;

    /**
     * The variable description @link(VariableDescription).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VariableDescription.ID_JPA, referencedColumnName = VariableDescription.ID_JPA, nullable = true)
    @NaturalId
    VariableDescription variableDescription;

    /**
     * The version de traitements @link(List<VersionDeTraitement>).
     */
    @ManyToMany(mappedBy = "modalites", fetch = FetchType.LAZY)
    @OrderBy(value = "traitementProgramme")
    SortedSet<VersionDeTraitement> versionDeTraitements = new TreeSet();

    /**
     * Instantiates a new modalite.
     */
    public Modalite() {
        super();
    }

    /**
     * Instantiates a new modalite.
     *
     * @param nom         the nom
     * @param code        the code
     * @param description the description
     */
    public Modalite(final String nom, final String code, final String description) {
        super();
        this.nom = nom;
        this.code = code;
        this.description = description;
    }

    @Override
    public int compareTo(Modalite o) {
        if (this.equals(o)) {
            return 0;
        }
        return this.code.compareTo(o == null ? org.apache.commons.lang.StringUtils.EMPTY : o.code);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Modalite other = (Modalite) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return Objects.equals(this.variableDescription, other.variableDescription);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * SortedSets the code.ex.printStackTrace();
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * SortedSets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * SortedSets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * SortedSets the nom.
     *
     * @param nom the new nom
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * Gets the variable description.
     *
     * @return the variable description
     */
    public VariableDescription getVariableDescription() {
        return this.variableDescription;
    }

    /**
     * SortedSets the variable description.
     *
     * @param variableDescription the new variable description
     */
    public void setVariableDescription(final VariableDescription variableDescription) {
        this.variableDescription = variableDescription;
    }

    /**
     * Gets the version de traitements.
     *
     * @return the version de traitements
     */
    public SortedSet<VersionDeTraitement> getVersionDeTraitements() {
        return this.versionDeTraitements;
    }

    /**
     * SortedSets the version de traitements.
     *
     * @param versionDeTraitements the new version de traitements
     */
    public void setVersionDeTraitements(
            final SortedSet<VersionDeTraitement> versionDeTraitements) {
        this.versionDeTraitements = versionDeTraitements;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[]{"versionDeTraitements"});
    }

    @Override
    public String toString() {
        return this.nom;
    }

}
