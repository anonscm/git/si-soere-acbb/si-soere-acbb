/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;
import java.util.Map;

/**
 * @param <T>
 * @author tcherniatinsky
 */
public interface IVariableManager<T> {

    /**
     * @param linkedList
     * @param intervals
     * @return
     */
    Map<String, List<NodeDataSet>> getAvailableVariablesByTreatmentAndDatesInterval(List<T> linkedList, List<IntervalDate> intervals);

}
