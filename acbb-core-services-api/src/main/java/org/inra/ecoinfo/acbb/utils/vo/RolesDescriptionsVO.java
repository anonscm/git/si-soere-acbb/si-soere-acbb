/**
 * OREILacs project - see LICENCE.txt for use created: 26 avr. 2010 15:01:10
 */
package org.inra.ecoinfo.acbb.utils.vo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class RolesDescriptionsVO.
 *
 * @author "Antoine Schellenberger"
 */
public class RolesDescriptionsVO implements Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The plugin id @link(String).
     */
    String pluginId;

    /**
     * The roles @link(List<String>).
     */
    List<String> roles = new LinkedList();

    /**
     * Instantiates a new roles descriptions vo.
     *
     * @param pluginId the plugin id
     */
    public RolesDescriptionsVO(final String pluginId) {
        super();
        this.pluginId = pluginId;
    }

    /**
     * Gets the plugin id.
     *
     * @return the plugin id
     */
    public String getPluginId() {
        return this.pluginId;
    }

    /**
     * Sets the plugin id.
     *
     * @param pluginId the new plugin id
     */
    public void setPluginId(final String pluginId) {
        this.pluginId = pluginId;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public List<String> getRoles() {
        return this.roles;
    }

    /**
     * Sets the roles.
     *
     * @param roles the new roles
     */
    public void setRoles(final List<String> roles) {
        this.roles = roles;
    }

}
