/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassCouvertVegetalType.MAV_CLASS_COUVERT_VEGETAL)
public class MassVegClassCouvertVegetalType extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_COUVERT_VEGETAL = "mav_class_couv_veg";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_couv_veg_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
