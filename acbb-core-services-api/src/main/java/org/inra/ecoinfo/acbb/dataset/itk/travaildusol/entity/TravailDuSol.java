package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.utils.StringUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class TravailDuSol.
 */
@Entity
@Table(name = TravailDuSol.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        TravailDuSol.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, TravailDuSol.ATTRIBUTE_JPA_DATE,
        TravailDuSol.ATTRIBUTE_JPA_RANG, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "wsol_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "wsol_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "wsol_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "wsol_parcelle_idx", columnList = SuiviParcelle.ID_JPA)})
@DiscriminatorValue(TravailDuSol.TRAVAIL_DU_SOL_DESCRIMINATOR)
//@AttributeOverrides(value = { @AttributeOverride(column = @Column(name = TravailDuSol.ID_JPA), name = "id") })
@PrimaryKeyJoinColumn(name = TravailDuSol.ID_JPA, referencedColumnName = AbstractIntervention.VARIABLE_NAME_ID)
public class TravailDuSol extends AbstractIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "travail_du_sol_wsol";

    /**
     * The Constant TRAVAIL_DU_SOL_DESCRIMINATOR @link(String).
     */
    public static final String TRAVAIL_DU_SOL_DESCRIMINATOR = "travail_du_sol";

    /**
     * The Constant ATTRIBUTE_JPA_RANG.
     */
    public static final String ATTRIBUTE_JPA_RANG = "rang";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The rang @link(int).
     */
    @Column(name = TravailDuSol.ATTRIBUTE_JPA_RANG, nullable = false)
    int rang = 1;

    /**
     * The mesures travail du sol @link(List<MesureTravailDuSol>).
     */
    @OneToMany(mappedBy = "travailDuSol", cascade = ALL)
    @OrderBy("numero")
    List<MesureTravailDuSol> mesuresTravailDuSol = new LinkedList();

    /**
     * Instantiates a new travail du sol.
     */
    public TravailDuSol() {
        super();
        this.type = TravailDuSol.TRAVAIL_DU_SOL_DESCRIMINATOR;
    }

    /**
     * Instantiates a new travail du sol.
     *
     * @param versionFile                     the version file
     * @param observation                     the observation
     * @param date                            the date
     * @param versionTraitementRealiseeNumber
     * @param rang
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     * @link(int) the version traitement realisee number
     * @link(int) the annee realisee number
     * @link(int) the periode realisee number
     */
    public TravailDuSol(final VersionFile versionFile, final String observation, final LocalDate date, final int versionTraitementRealiseeNumber, final int anneeRealiseeNumber, final int periodeRealiseeNumber, final Integer rang) {
        super(versionFile, TravailDuSol.TRAVAIL_DU_SOL_DESCRIMINATOR, observation, date,
                versionTraitementRealiseeNumber, anneeRealiseeNumber, periodeRealiseeNumber);
        this.rang = rang;
    }

    /**
     * Gets the mesures travail du sol.
     *
     * @return the mesures travail du sol
     */
    public List<MesureTravailDuSol> getMesuresTravailDuSol() {
        return this.mesuresTravailDuSol;
    }

    /**
     * Sets the mesures travail du sol.
     *
     * @param mesuresTravailDuSol the new mesures travail du sol
     */
    public void setMesuresTravailDuSol(final List<MesureTravailDuSol> mesuresTravailDuSol) {
        this.mesuresTravailDuSol = mesuresTravailDuSol;
    }

    /**
     * Gets the observation.
     *
     * @return the observation
     * @see org.inra.ecoinfo.acbb.dataset.itk.entity.Intervention#getObservation()
     */
    @Override
    public String getObservation() {
        final StringBuffer observations = new StringBuffer();
        for (final MesureTravailDuSol mesure : this.mesuresTravailDuSol) {
            if (observations.length() > 0) {
                observations.append(StringUtils.NEW_LINE);
            }
            observations.append(mesure.getObservation());
        }
        return this.observation;
    }

    /**
     * Gets the rang.
     *
     * @return the rang
     */
    public int getRang() {
        return this.rang;
    }

    /**
     * Sets the rang.
     *
     * @param rang the new rang
     */
    public void setRang(final int rang) {
        this.rang = rang;
    }

}
