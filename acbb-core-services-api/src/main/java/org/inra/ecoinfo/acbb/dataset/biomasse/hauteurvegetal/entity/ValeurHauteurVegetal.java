package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity;

import org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity.BiomassProduction;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author ptcherniati
 */

@Entity
@Table(name = ValeurHauteurVegetal.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, HauteurVegetal.ID_JPA}),
        indexes = {@Index(name = "vhv_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vhv_methode_idx", columnList = AbstractMethode.ID_JPA),
                @Index(name = "vhv_bpt_idx", columnList = BiomassProduction.ID_JPA)
        })
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurHauteurVegetal.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurHauteurVegetal extends ValeurBiomasse<ValeurHauteurVegetal> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_HH = "hauteurVegetal";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_hauteur_vegetal_vhv";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_BIOMASSE_NATURE = "hav_nature";

    /**
     *
     */
    public static final String ID_JPA = "vhv_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = HauteurVegetal.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA, nullable = false)
    HauteurVegetal hauteurVegetal;

    Float mediane;

    /*
     * @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
     *
     * @JoinColumn(name = HauteurVegetal.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA,
     * nullable = false) HauteurVegetal hauteur_vegetal;
     */
    // LES CONSTRUCTEURS DE LA SUPER CLASSE

    /**
     *
     */
    public ValeurHauteurVegetal() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param hauteurVegetal
     * @param qualityIndex
     * @param methodeHauteurVegetal
     * @param mediane
     */
    public ValeurHauteurVegetal(Float valeur, RealNode realNode, int measureNumber, float standardDeviation, HauteurVegetal hauteurVegetal, int qualityIndex, AbstractMethode methodeHauteurVegetal, Float mediane) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methodeHauteurVegetal);
        this.hauteurVegetal = hauteurVegetal;
        this.mediane = mediane;
    }

    // GETTERS ET SETTTERS

    @Override
    public int compareTo(ValeurHauteurVegetal o) {
        int comparemesure = this.getHauteurVegetal().compareTo(o.getHauteurVegetal());
        int compareValeur = this.getDatatypeVariableUnite()
                .getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return compareValeur;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurBiomasse<?> other = (ValeurBiomasse<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return the hauteur_vegetal
     */
    public HauteurVegetal getHauteurVegetal() {
        return this.hauteurVegetal;
    }

    /**
     * @param hauteur_vegetal the hauteur_vegetal to set
     */
    public void setHauteurVegetal(HauteurVegetal hauteur_vegetal) {
        this.hauteurVegetal = hauteur_vegetal;
    }

    /**
     * @return
     */
    public Float getMediane() {
        return this.mediane;
    }

    /**
     * @param mediane
     */
    public void setMediane(Float mediane) {
        this.mediane = mediane;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }

}
