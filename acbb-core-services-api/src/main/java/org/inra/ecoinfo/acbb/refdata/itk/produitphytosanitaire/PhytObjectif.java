/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PhytObjectif.
 */
@Entity
@DiscriminatorValue(PhytObjectif.PHYTOSANITAIRE_OBJECTIF)
public class PhytObjectif extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "phytosanitaire_objectif";

    /**
     * The Constant PHYTOSANITAIRE_OBJECTIF.
     */
    public static final String PHYTOSANITAIRE_OBJECTIF = "phytosanitaire_objectif";

    /**
     * The Constant serialVersionUID <long>.
     */
    private static final long serialVersionUID = 1L;

}
