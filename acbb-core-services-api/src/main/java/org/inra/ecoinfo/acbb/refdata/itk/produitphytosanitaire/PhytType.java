/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PhytType.
 */
@Entity
@DiscriminatorValue(PhytType.PHYTOSANITAIRE_TYPE)
public class PhytType extends ListeItineraire {

    /**
     * The Constant PHYTOSANITAIRE_TYPE.
     */
    public static final String PHYTOSANITAIRE_TYPE = "phytosanitaire_type";

    /**
     * The Constant serialVersionUID <long>.
     */
    private static final long serialVersionUID = 1L;

}
