package org.inra.ecoinfo.acbb.dataset.itk.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * The Class PeriodeRealisee.
 */
@Entity
@Table(name = PeriodeRealisee.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        AnneeRealisee.ID_JPA, PeriodeRealisee.ATTRIBUTE_JPA_NUMERO}),
        indexes = {
                @Index(name = "periode_annee_idx", columnList = AnneeRealisee.ID_JPA),
                @Index(name = "periode_couvert_idx", columnList = SemisCouvertVegetal.ID_JPA),
                @Index(name = "periode_datedebut_idx", columnList = PeriodeRealisee.ATTRIBUTE_JPA_DATE_DEBUT),
                @Index(name = "periode_datefin_idx", columnList = PeriodeRealisee.ATTRIBUTE_JPA_DATE_FIN)
        })
public class PeriodeRealisee implements Serializable, Comparable<PeriodeRealisee> {

    /**
     * The Constant ATTRIBUTE_JPA_NUMERO.
     */
    public static final String ATTRIBUTE_JPA_NUMERO = "numero";

    /**
     * The Constant ATTRIBUTE_JPA_DATE_DEBUT.
     */
    public static final String ATTRIBUTE_JPA_DATE_DEBUT = "dateDeDebut";

    /**
     * The Constant ATTRIBUTE_JPA_DATE_FIN.
     */
    public static final String ATTRIBUTE_JPA_DATE_FIN = "dateDeFin";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "periode_realisee_preal";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "preal_id";

    /**
     * The Constant NAME_ENTITY_ITK_PREAL @link(String).
     */
    public static final String NAME_ENTITY_SEMIS_PREAL = "semis_preal";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ANNEE_REALISEE = "anneeRealisee";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = PeriodeRealisee.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The numero @link(int).
     */
    @Column(nullable = false, name = PeriodeRealisee.ATTRIBUTE_JPA_NUMERO)
    @NaturalId
    int numero;

    /**
     * The date de debut @link(Date).
     */
    @Column(nullable = true, name = PeriodeRealisee.ATTRIBUTE_JPA_DATE_DEBUT)
    LocalDate dateDeDebut;

    /**
     * The date de fin @link(Date).
     */
    @Column(nullable = true, name = PeriodeRealisee.ATTRIBUTE_JPA_DATE_FIN)
    LocalDate dateDeFin;

    /**
     * The annee realisee @link(AnneeRealisee).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinColumn(name = AnneeRealisee.ID_JPA, referencedColumnName = AnneeRealisee.ID_JPA, nullable = false)
    @NaturalId
    AnneeRealisee anneeRealisee;

    /**
     * The couvert vegetal @link(SemisCouvertVegetal).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SemisCouvertVegetal.ID_JPA, referencedColumnName = ListeItineraire.ID_JPA, nullable = true)
    SemisCouvertVegetal couvertVegetal;

    @Transient
    @OneToMany(mappedBy = Tempo.ATTRIBUTE_JPA_PERIODE_REALISEE)
    List<Tempo> tempos = new LinkedList();

    /**
     * Instantiates a new periode realisee.
     */
    public PeriodeRealisee() {
        super();
    }

    /**
     * Instantiates a new periode realisee.
     *
     * @param numero
     * @param anneeRealisee
     * @link(int) the numero
     * @link(AnneeRealisee) the annee realisee
     */
    public PeriodeRealisee(final int numero, final AnneeRealisee anneeRealisee) {
        super();
        assert anneeRealisee != null : "on doit passer une année réalisée";
        this.numero = numero;
        this.anneeRealisee = anneeRealisee;
        this.anneeRealisee.getPeriodesRealisees().put(numero, this);
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(PeriodeRealisee) the o
     */
    @Override
    public int compareTo(final PeriodeRealisee o) {
        if (!this.anneeRealisee.equals(o.anneeRealisee)) {
            return this.anneeRealisee.compareTo(o.anneeRealisee);
        }
        if (this.numero == o.numero) {
            return 0;
        } else {
            return this.numero > o.numero ? 1 : -1;
        }
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful @see java.lang.Object#equals(java.lang.Object)
     * @link(Object) the obj
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id"});
    }

    /**
     * Gets the annee realisee.
     *
     * @return the annee realisee
     */
    public AnneeRealisee getAnneeRealisee() {
        return this.anneeRealisee;
    }

    /**
     * Sets the annee realisee.
     *
     * @param anneeRealisee the new annee realisee
     */
    public void setAnneeRealisee(final AnneeRealisee anneeRealisee) {
        this.anneeRealisee = anneeRealisee;
    }

    /**
     * Gets the bindeds itk.
     *
     * @return the bindeds itk
     */
    public Set<AbstractIntervention> getBindedsITK() {
        Set<AbstractIntervention> interventions = new TreeSet();
        for (Tempo tempo : this.getTempos()) {
            interventions.addAll(tempo.getBindedsITK());
        }
        return interventions;
    }

    /**
     * Gets the couvert vegetal @link(SemisCouvertVegetal).
     *
     * @return the couvertVegetal
     */
    public SemisCouvertVegetal getCouvertVegetal() {
        return this.couvertVegetal;
    }

    /**
     * Sets the couvert vegetal @link(SemisCouvertVegetal).
     *
     * @param couvertVegetal the couvertVegetal to set
     */
    public void setCouvertVegetal(final SemisCouvertVegetal couvertVegetal) {
        this.couvertVegetal = couvertVegetal;
    }

    /**
     * Gets the date de debut.
     *
     * @return the date de debut
     */
    public LocalDate getDateDeDebut() {
        return this.dateDeDebut;
    }

    /**
     * Sets the date de debut.
     *
     * @param dateDeDebut the new date de debut
     */
    public void setDateDeDebut(final LocalDate dateDeDebut) {
        this.dateDeDebut = dateDeDebut;
    }

    /**
     * Gets the date de fin.
     *
     * @return the date de fin
     */
    public LocalDate getDateDeFin() {
        return this.dateDeFin;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     */
    public void setDateDeFin(final LocalDate dateDeFin) {
        this.dateDeFin = dateDeFin;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the numero.
     *
     * @return the numero
     */
    public int getNumero() {
        return this.numero;
    }

    /**
     * Sets the numero.
     *
     * @param numero the new numero
     */
    public void setNumero(final int numero) {
        this.numero = numero;
    }

    /**
     * @return the tempos
     */
    public List<Tempo> getTempos() {
        return this.tempos;
    }

    /**
     * @param tempos the tempos to set
     */
    public void setTempos(List<Tempo> tempos) {
        this.tempos = tempos;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
