/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PaturageTypeAnimaux.
 */
@Entity
@DiscriminatorValue(PaturageNaturePlante.PATURAGE_NATURE_PLANTE)
public class PaturageNaturePlante extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "pat_nature_plante";

    /**
     * The Constant SEMIS_COUVERT_VEGETAL.
     */
    public static final String PATURAGE_NATURE_PLANTE = "pat_nature_plante";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
