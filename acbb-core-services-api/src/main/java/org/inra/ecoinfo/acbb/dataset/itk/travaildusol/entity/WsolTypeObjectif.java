/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class WsolTypeObjectif.
 */
@Entity
@DiscriminatorValue(WsolTypeObjectif.WSOL_OBJECTIF)
public class WsolTypeObjectif extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "wsol_obj_id";

    /**
     * The Constant WSOL_OBJECTIF.
     */
    public static final String WSOL_OBJECTIF = "wsol_objectifs";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
