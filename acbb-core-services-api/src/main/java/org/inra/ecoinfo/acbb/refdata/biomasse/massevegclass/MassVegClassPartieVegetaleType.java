/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE)
public class MassVegClassPartieVegetaleType extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_PART_VEG_TYPE = "mav_class_part_veg";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_part_veg_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
