package org.inra.ecoinfo.acbb.dataset.itk.travaildusol;

import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;

/**
 * @author ptcherniati
 */
public interface IRequestPropertiesWSol extends IRequestPropertiesITK {

}
