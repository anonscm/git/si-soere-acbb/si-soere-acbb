/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class WsolTypeOutil.
 */
@Entity
@DiscriminatorValue(WsolTypeOutil.WSOL_TYPE_OUTIL)
public class WsolTypeOutil extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "wsol_type_outils_id";

    /**
     * The Constant WSOL_TYPE_OUTIL.
     */
    public static final String WSOL_TYPE_OUTIL = "wsol_type_outil";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
