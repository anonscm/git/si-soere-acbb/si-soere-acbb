package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurFertilisant.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, Fertilisant.ID_JPA}),
        indexes = {
                @Index(name = "vfert_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vfert_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vfert_fert_idx", columnList = Fertilisant.ID_JPA)})
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurFertilisant.ID_JPA), name = "id")})
public class ValeurFertilisant extends ValeurIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "valeur_fertilisant_vfert";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "fert_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Fertilisant.ID_JPA, referencedColumnName = Fertilisant.ID_JPA, nullable = false)
    Fertilisant fertilisant;

    /**
     *
     */
    public ValeurFertilisant() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param fertilisant
     */
    public ValeurFertilisant(final float valeur, final RealNode realNode, final Fertilisant fertilisant) {
        super(valeur, realNode);
        this.fertilisant = fertilisant;
    }

    /**
     * @param valeur
     * @param realNode
     * @param fertilisant
     */
    public ValeurFertilisant(final IValeurQualitative valeur, final RealNode realNode, final Fertilisant fertilisant) {
        super(valeur, realNode);
        this.fertilisant = fertilisant;
    }

    /**
     * @return
     */
    public Fertilisant getFertilisant() {
        return this.fertilisant;
    }

    /**
     * @param fertilisant
     */
    public void setFertilisant(final Fertilisant fertilisant) {
        this.fertilisant = fertilisant;
    }

}
