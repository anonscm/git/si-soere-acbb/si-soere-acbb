package org.inra.ecoinfo.acbb.refdata.datatypevariableunite;

import org.inra.ecoinfo.acbb.refdata.suivivariable.SuiviVariable;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class DatatypeVariableUniteACBB.
 */
@Entity
@Table(name = DatatypeVariableUniteACBB.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {Variable.ID_JPA, DataType.ID_JPA, Unite.ID_JPA}),
        indexes = {
                @Index(name = "dvu_uni_idx", columnList = Unite.ID_JPA)
                ,
                @Index(name = "dvu_var_idx", columnList = Variable.ID_JPA)
                ,
                @Index(name = "dvu_dty_idx", columnList = DataType.ID_JPA)})
@PrimaryKeyJoinColumn(name = DatatypeVariableUniteACBB.ID_JPA)
public class DatatypeVariableUniteACBB extends Nodeable implements Serializable, Comparable<INodeable> {

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "vdt_id";

    /**
     *
     */
    public static final String DISCRIMINATOR = "DatatypeVariableUnite";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "datatype_variable_unite_acbb_dvu";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;
    /**
     * The valeur min @link(Float).
     */
    @Column(name = "vdt_valeur_min")
    Float valeurMin;
    /**
     * The valeur max @link(Float).
     */
    @Column(name = "vdt_valeur_max")
    Float valeurMax;
    /**
     * The suivis variables @link(List<SuiviVariable>).
     */
    @OneToMany(mappedBy = "datatypeVariableUnite", cascade = ALL)
    List<SuiviVariable> suivisVariables = new LinkedList();
    /**
     * The datatype @link(DataType).
     */
    @XmlElement(name = "datatype")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = DataType.ID_JPA, referencedColumnName = DataType.ID_JPA, nullable = false)
    private DataType datatype;
    /**
     * The unite @link(Unite).
     */
    @XmlElement(name = "unite")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Unite.ID_JPA, referencedColumnName = Unite.ID_JPA, nullable = false)
    private Unite unite;
    /**
     * The variable @link(Variable).
     */
    @XmlElement(name = "variable")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Variable.ID_JPA, referencedColumnName = Variable.ID_JPA, nullable = false)
    private Variable variable;

    /**
     * Instantiates a new datatype unite variable acbb.
     */
    public DatatypeVariableUniteACBB() {
        super();
    }

    /**
     * Instantiates a new datatype unite variable acbb.
     *
     * @param datatype the datatype
     * @param unite    the unite
     * @param variable the variable
     * @param minValue the min value
     * @param maxValue the max value
     */
    public DatatypeVariableUniteACBB(final DataType datatype, final Unite unite,
                                     final Variable variable, final Float minValue, final Float maxValue) {
        super();
        this.datatype = datatype;
        this.unite = unite;
        this.variable = variable;
        setCode(String.format("%s_%s_%s", datatype.getCode(), variable.getCode(), unite.getCode()));
        this.valeurMin = minValue;
        this.valeurMax = maxValue;
    }

    @Override
    public int compareTo(INodeable o) {
        if (o == null || !(o instanceof DatatypeVariableUniteACBB)) {
            return -1;
        }
        DatatypeVariableUniteACBB oo = (DatatypeVariableUniteACBB) o;
        if (this.getDatatype().compareTo(oo.getDatatype()) != 0) {
            return this.getDatatype().compareTo(oo.getDatatype());
        }
        if (this.getVariable().compareTo(oo.getVariable()) != 0) {
            return this.getVariable().compareTo(oo.getVariable());
        }
        return this.getUnite().getCode().compareTo(oo.getUnite().getCode());
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DatatypeVariableUniteACBB other = (DatatypeVariableUniteACBB) obj;
        return !(!Objects.equals(this.getDatatype(), other.getDatatype())
                && !Objects.equals(this.getVariable(), other.getVariable())
                && !Objects.equals(this.getUnite().getCode(), other.getUnite().getCode()));
    }

    /**
     * Gets the suivis variables.
     *
     * @return the suivis variables
     */
    public List<SuiviVariable> getSuivisVariables() {
        return this.suivisVariables;
    }

    /**
     * Sets the suivis variables.
     *
     * @param suivisVariables the new suivis variables
     */
    public void setSuivisVariables(final List<SuiviVariable> suivisVariables) {
        this.suivisVariables = suivisVariables;
    }

    /**
     * Gets the valeur max.
     *
     * @return the valeur max
     */
    public Float getValeurMax() {
        return this.valeurMax;
    }

    /**
     * Sets the valeur max.
     *
     * @param valeurMax the new valeur max
     */
    public void setValeurMax(final Float valeurMax) {
        this.valeurMax = valeurMax;
    }

    /**
     * Gets the valeur min.
     *
     * @return the valeur min
     */
    public Float getValeurMin() {
        return this.valeurMin;
    }

    /**
     * Sets the valeur min.
     *
     * @param valeurMin the new valeur min
     */
    public void setValeurMin(final Float valeurMin) {
        this.valeurMin = valeurMin;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.getDatatype());
        hash = 311 * hash + Objects.hashCode(this.getVariable());
        hash = 3_111 * hash + Objects.hashCode(this.getUnite().getCode());
        return hash;
    }

    /**
     * Gets the datatype.
     *
     * @return the datatype
     */
    public DataType getDatatype() {
        return datatype;
    }

    /**
     * Sets the datatype.
     *
     * @param datatype the new datatype
     */
    public void setDatatype(final DataType datatype) {
        this.datatype = datatype;
    }

    /**
     * Gets the unite.
     *
     * @return the unite
     */
    public Unite getUnite() {
        return unite;
    }

    /**
     * Sets the unite.
     *
     * @param unite the new unite
     */
    public void setUnite(final Unite unite) {
        this.unite = unite;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public Variable getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final Variable variable) {
        this.variable = variable;
    }

    /**
     * @return
     */
    @Override
    public Class<? extends Nodeable> getNodeableType() {
        return DatatypeVariableUniteACBB.class;
    }

    /**
     * @return
     */
    @Override
    public String getName() {
        return this.variable.getName();
    }

}
