package org.inra.ecoinfo.acbb.dataset.itk.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

/**
 * The Class AnneeRealisee.
 */
@Entity
@Table(name = AnneeRealisee.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        VersionTraitementRealisee.ID_JPA, AnneeRealisee.ATTRIBUTE_JPA_ANNEE}),
        indexes = {
                @Index(name = "annee_annee_idx", columnList = VersionTraitementRealisee.ID_JPA)
                ,
                @Index(name = "annee_couvert_idx", columnList = SemisCouvertVegetal.ID_JPA)
                ,
                @Index(name = "annee_datedebut_idx", columnList = AnneeRealisee.ATTRIBUTE_JPA_DATE_DEBUT)
                ,
                @Index(name = "annee_datefin_idx", columnList = AnneeRealisee.ATTRIBUTE_JPA_DATE_FIN)
        })
public class AnneeRealisee implements Serializable, Comparable<AnneeRealisee> {

    /**
     * The Constant ATTRIBUTE_JPA_ANNEE.
     */
    public static final String ATTRIBUTE_JPA_ANNEE = "annee";

    /**
     * The Constant ATTRIBUTE_JPA_DATE_DEBUT.
     */
    public static final String ATTRIBUTE_JPA_DATE_DEBUT = "dateDeDebut";

    /**
     * The Constant ATTRIBUTE_JPA_DATE_FIN.
     */
    public static final String ATTRIBUTE_JPA_DATE_FIN = "dateDeFin";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "annee_realisee_areal";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "areal_id";

    /**
     * The Constant ATTRIBUTE_JPA_VERSION_DE_TRAITEMENT_REALISEE @link(String).
     */
    public static final String ATTRIBUTE_JPA_VERSION_DE_TRAITEMENT_REALISEE = "versionTraitementRealise";

    /**
     * The Constant NAME_ENTITY_ITK_AREAL @link(String).
     */
    public static final String NAME_ENTITY_SEMIS_AREAL = "semis_areal";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = AnneeRealisee.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The annee @link(int).
     */
    @Column(nullable = false, name = AnneeRealisee.ATTRIBUTE_JPA_ANNEE)
    @NaturalId
    int annee;

    /**
     * The date de debut @link(Date).
     */
    @Column(nullable = true, name = AnneeRealisee.ATTRIBUTE_JPA_DATE_DEBUT)
    LocalDate dateDeDebut;

    /**
     * The date de fin @link(Date).
     */
    @Column(nullable = true, name = AnneeRealisee.ATTRIBUTE_JPA_DATE_FIN)
    LocalDate dateDeFin;

    /**
     * The version traitement realise @link(VersionTraitementRealise).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinColumn(name = VersionTraitementRealisee.ID_JPA, referencedColumnName = VersionTraitementRealisee.ID_JPA, nullable = false)
    @NaturalId
    VersionTraitementRealisee versionTraitementRealise;

    /**
     * The couvert vegetal @link(SemisCouvertVegetal).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SemisCouvertVegetal.ID_JPA, referencedColumnName = ListeItineraire.ID_JPA, nullable = true)
    SemisCouvertVegetal couvertVegetal;

    /**
     * The periods realisees @link(Map<Integer,AnneeRealisee>).
     */
    @OneToMany(mappedBy = PeriodeRealisee.ATTRIBUTE_JPA_ANNEE_REALISEE)
    @MapKey(name = PeriodeRealisee.ATTRIBUTE_JPA_NUMERO)
    Map<Integer, PeriodeRealisee> periodesRealisees = new HashMap();

    /**
     * The tempos.
     */
    @Transient
    @OneToMany(mappedBy = Tempo.ATTRIBUTE_JPA_ANNEE_REALISEE)
    List<Tempo> tempos = new LinkedList();

    /**
     * Instantiates a new annee realisee.
     */
    public AnneeRealisee() {
        super();
    }

    /**
     * Instantiates a new annee realisee.
     *
     * @param annee
     * @param versionTraitementRealise
     * @link(int) the annee
     * @link(VersionTraitementRealisee) the version traitement realise
     */
    public AnneeRealisee(final int annee, final VersionTraitementRealisee versionTraitementRealise) {
        super();
        assert versionTraitementRealise != null : "on doit passer une version de traitement réalisée";
        this.annee = annee;
        this.versionTraitementRealise = versionTraitementRealise;
        this.versionTraitementRealise.getAnneesRealisees().put(annee, this);
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(AnneeRealisee) the o
     */
    @Override
    public int compareTo(final AnneeRealisee o) {
        if (!this.versionTraitementRealise.equals(o.versionTraitementRealise)) {
            return this.versionTraitementRealise.compareTo(o.versionTraitementRealise);
        }
        if (this.annee != o.annee) {
            return this.annee > o.annee ? 1 : -1;
        } else {
            return this.dateDeDebut.equals(o.dateDeDebut) ? this.dateDeFin.compareTo(o.dateDeFin)
                    : this.dateDeDebut.compareTo(o.dateDeDebut);
        }
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful @see java.lang.Object#equals(java.lang.Object)
     * @link(Object) the obj
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id"});
    }

    /**
     * Gets the annee.
     *
     * @return the annee
     */
    public int getAnnee() {
        return this.annee;
    }

    /**
     * Sets the annee.
     *
     * @param annee the new annee
     */
    public void setAnnee(final int annee) {
        this.annee = annee;
    }

    /**
     * Gets the bindeds itk.
     *
     * @return the bindeds itk
     */
    public Set<AbstractIntervention> getBindedsITK() {
        Set<AbstractIntervention> interventions = new TreeSet();
        for (Tempo tempo : this.getTempos()) {
            interventions.addAll(tempo.getBindedsITK());
        }
        return interventions;
    }

    /**
     * Gets the couvert vegetal @link(SemisCouvertVegetal).
     *
     * @return the couvetVegetal
     */
    public SemisCouvertVegetal getCouvertVegetal() {
        return this.couvertVegetal;
    }

    /**
     * Sets the couvert vegetal @link(SemisCouvertVegetal).
     *
     * @param couvetVegetal the couvetVegetal to set
     */
    public void setCouvertVegetal(final SemisCouvertVegetal couvetVegetal) {
        this.couvertVegetal = couvetVegetal;
    }

    /**
     * Gets the date de debut.
     *
     * @return the date de debut
     */
    public LocalDate getDateDeDebut() {
        return dateDeDebut;
    }

    /**
     * Sets the date de debut.
     *
     * @param dateDeDebut the new date de debut
     */
    public void setDateDeDebut(final LocalDate dateDeDebut) {
        this.dateDeDebut = dateDeDebut;
    }

    /**
     * Gets the date de fin.
     *
     * @return the date de fin
     */
    public LocalDate getDateDeFin() {
        return dateDeFin;
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     */
    public void setDateDeFin(final LocalDate dateDeFin) {
        this.dateDeFin = dateDeFin;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the periodesRealisees
     */
    public Map<Integer, PeriodeRealisee> getPeriodesRealisees() {
        return this.periodesRealisees;
    }

    /**
     * @param periodesRealisees the periodesRealisees to set
     */
    public void setPeriodesRealisees(final Map<Integer, PeriodeRealisee> periodesRealisees) {
        this.periodesRealisees = periodesRealisees;
    }

    /**
     * @return the tempos
     */
    public List<Tempo> getTempos() {
        return this.tempos;
    }

    /**
     * @param tempos the tempos to set
     */
    public void setTempos(List<Tempo> tempos) {
        this.tempos = tempos;
    }

    /**
     * Gets the version traitement realise.
     *
     * @return the version traitement realise
     */
    public VersionTraitementRealisee getVersionTraitementRealise() {
        return this.versionTraitementRealise;
    }

    /**
     * Sets the version traitement realise.
     *
     * @param versionTraitementRealise the new version traitement realise
     */
    public void setVersionTraitementRealise(
            final VersionTraitementRealisee versionTraitementRealise) {
        this.versionTraitementRealise = versionTraitementRealise;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
