package org.inra.ecoinfo.acbb.refdata.parcelle;

import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.utils.StringUtils;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.regex.Pattern;

/**
 * The Class Parcelle.
 */
@Entity
@Table(name = Parcelle.NAME_ENTITY_JPA, uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                SiteACBB.ID_JPA, Parcelle.ATTRIBUTE_JPA_NAME})},
        indexes = {
                @Index(name = "par_sit_idx", columnList = SiteACBB.ID_JPA)
                ,
                @Index(name = "par_bloc_idx", columnList = Bloc.ID_JPA)
                ,
                @Index(name = "par_code_site_idx", columnList = SiteACBB.ID_JPA + "," + Parcelle.ATTRIBUTE_JPA_NAME)})
@DiscriminatorColumn(name = "dtype", discriminatorType = DiscriminatorType.STRING)
@PrimaryKeyJoinColumn(name = Parcelle.ID_JPA)
public class Parcelle extends Nodeable implements Serializable, Comparable<INodeable> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "parcelle_par";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "par_id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_DESCRIPTION = "commentaire";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NAME = "nom";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    private static final String PATTERN_CODE = "%s_%s";
    /**
     *
     */
    static public Pattern pattern = Pattern.compile("(.*?)([0-9]*$)");
    /**
     * The id <long>.
     */
    @Id
    @Column(name = Parcelle.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    // ATTRIBUTS
    /**
     * The site @link(SiteACBB).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SiteACBB.ID_JPA, referencedColumnName = SiteACBB.ID_JPA, nullable = false)
    SiteACBB site;
    /**
     * The bloc @link(Bloc).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Bloc.ID_JPA, referencedColumnName = Bloc.ID_JPA, nullable = true)
    Bloc bloc;
    /**
     * The nom @link(String).
     */
    @Column(name = Parcelle.ATTRIBUTE_JPA_NAME, nullable = false)
    String nom;
    /**
     * The date creation @link(Date).
     */
    @Column(nullable = true, columnDefinition = "DATE")
    LocalDate dateCreation;
    /**
     * The surface @link(Float).
     */
    @Column(nullable = true)
    Float surface;
    /**
     * The commentaire @link(String).
     */
    @Column(name = Parcelle.ATTRIBUTE_JPA_DESCRIPTION, columnDefinition = "TEXT", nullable = true)
    String commentaire;

    /**
     * Instantiates a new parcelle.
     */
    public Parcelle() {
        super();
    }

    // CONSTRUCTEURS

    /**
     * Instantiates a new parcelle.
     *
     * @param nomParcelle  the nom parcelle
     * @param surface      the surface
     * @param dateCreation the date creation
     * @param commentaire  the commentaire
     */
    public Parcelle(final String nomParcelle, final Float surface, final LocalDate dateCreation,
                    final String commentaire) {
        super();
        this.setName(nomParcelle);
        this.surface = surface;
        this.setDateCreation(dateCreation);
        this.commentaire = commentaire;
    }

    /**
     * @param name
     * @param site
     * @return
     */
    public static final String getCodeFromNameAndSite(String name, SiteACBB site) {
        return String.format(PATTERN_CODE, site.getCode(), Utils.createCodeFromString(name));
    }

    // METHODES

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(Parcelle) the o
     */
    @Override
    public int compareTo(final INodeable o) {
        if (o == null || !(o instanceof Parcelle)) {
            return -1;
        }
        Parcelle oo = (Parcelle) o;
        if (this.getSite().compareTo(oo.getSite()) != 0) {
            return this.getSite().compareTo(oo.getSite());
        }
        return this.getAlphanumeCode().compareTo(oo.getAlphanumeCode());
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final Parcelle other = (Parcelle) obj;
        if (getCode() == null && other.getCode() != null) {
            return false;
        } else if (!getCode().equals(other.getCode())) {
            return false;
        } else return this.site.getCode().equals(other.site.getCode());
    }

    /**
     * Gets the bloc.
     *
     * @return the bloc
     */
    public Bloc getBloc() {
        return this.bloc;
    }

    /**
     * Sets the bloc.
     *
     * @param bloc the new bloc
     */
    public void setBloc(final Bloc bloc) {
        this.bloc = bloc;
    }

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return this.commentaire;
    }

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public LocalDate getDateCreation() {
        return dateCreation;
    }

    /**
     * Sets the date creation.
     *
     * @param dateCreation the new date creation
     */
    public void setDateCreation(final LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    @Override
    public String getName() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom the new nom
     */
    public void setName(final String nom) {
        this.nom = nom;
        this.setCode(getCompleteCode());
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public SiteACBB getSite() {
        return this.site;
    }

    /**
     * Sets the site.
     *
     * @param siteACBB the new site
     */
    public void setSite(final SiteACBB siteACBB) {
        this.site = siteACBB;
        this.setCode(getCompleteCode());
    }

    /**
     * Gets the surface.
     *
     * @return the surface
     */
    public Float getSurface() {
        return this.surface;
    }

    /**
     * Sets the surface.
     *
     * @param surface the new surface
     */
    public void setSurface(final Float surface) {
        this.surface = surface;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getCode() == null ? 0 : getCode().hashCode());
        result = prime * result + (this.site == null ? 0 : this.site.hashCode());
        return result;
    }

    /**
     * @return
     */
    protected String getAlphanumeCode() {
        return StringUtils.orderedString(getCode());
    }

    @Override
    public String toString() {
        return getCode();
    }

    /**
     * @return
     */
    @Override
    public Class<? extends Nodeable> getNodeableType() {
        return Parcelle.class;
    }

    private String getCompleteCode() {
        if (site == null) {
            return getCode();
        }
        return getCodeFromNameAndSite(nom, site);
    }

}
