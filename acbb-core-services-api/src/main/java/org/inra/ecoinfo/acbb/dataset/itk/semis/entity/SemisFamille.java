/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class SemisFamille.
 */
@Entity
@DiscriminatorValue(SemisFamille.SEMIS_FAMILLE)
public class SemisFamille extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_famille_id";

    /**
     * The Constant SEMIS_FAMILLE.
     */
    public static final String SEMIS_FAMILLE = "sem_famille";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
