package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The Class MasseVegClass.
 */

/**
 * @author koyao
 */
@Entity
@Table(name = MasseVegClass.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {MasseVegClass.ATTRIBUTE_JPA_CODE}),
        indexes = {
                @Index(name = "massevegclass_cat_idx", columnList = MasseVegClass.ATTRIBUTE_JPA_CATEGORIE_COUVERT)
                ,
                @Index(name = "massevegclass_type_idx", columnList = MasseVegClass.ATTRIBUTE_JPA_TYPE_MASSE_VEGETALE)
                ,
                @Index(name = "massevegclass_as_idx", columnList = MasseVegClass.ATTRIBUTE_JPA_AERIEN_SOUTERRAIN)
                ,
                @Index(name = "massevegclass_pp_idx", columnList = MasseVegClass.ATTRIBUTE_JPA_PARTIE_PLANTE)
                ,
                @Index(name = "massevegclass_dev_idx", columnList = MasseVegClass.ATTRIBUTE_JPA_DEVENIR)
                ,
                @Index(name = "massevegclass_couv_idx", columnList = MasseVegClass.ATTRIBUTE_JPA_COUVERT_VEGETAL)
                ,
                @Index(name = "massevegclass_dvu_idx", columnList = DatatypeVariableUniteACBB.ID_JPA)})
public class MasseVegClass implements Serializable, Comparable<MasseVegClass> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "mv_classification";

    /**
     *
     */
    public static final String ID_JPA = "mv_class_id";

    /**
     * The Constant ATTRIBUTE_JPA_.
     */
    public static final String ATTRIBUTE_JPA_LIBELLE = "libelle";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_DEFINITION = "definition";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_CATEGORIE_COUVERT = "mav_class_categorie_couvert";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_AERIEN_SOUTERRAIN = "mav_class_aerien_souterrain";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_TYPE_MASSE_VEGETALE = "mav_class_type_masse_vegetale";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PARTIE_PLANTE = "mav_class_partie_plante";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PARTIE_VEGETALE = "mav_class_part_veg";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_DEVENIR = "mav_class_devenir";

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ATTRIBUTE_JPA_CODE = "code";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_COUVERT_VEGETAL = "couvertvegetal_vq_id";

    // LES CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = MasseVegClass.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = MasseVegClass.ATTRIBUTE_JPA_LIBELLE, nullable = false)
    private String libelle;
    private String code;
    /* annuelle ou pérenne */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ATTRIBUTE_JPA_CATEGORIE_COUVERT, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    private MassVegClassCategorieCouvert categorieCouvert;
    /* catégorie de couvert */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ATTRIBUTE_JPA_TYPE_MASSE_VEGETALE, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    private MassVegClassTypeMasseVegetale typeMasseVegetale;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ATTRIBUTE_JPA_AERIEN_SOUTERRAIN, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    private MassVegClassAerienSouterrain aerien;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ATTRIBUTE_JPA_PARTIE_PLANTE, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    private MassVegClassPartieVegetaleType partiesPlante;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ATTRIBUTE_JPA_DEVENIR, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    /* Clé etrangère pourquoi possibilité d'être nulle */
    private MassVegClassDevenir devenir;
    @Column(name = MasseVegClass.ATTRIBUTE_JPA_DEFINITION, columnDefinition = "TEXT", nullable = false)
    private String definition;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MasseVegClass.ATTRIBUTE_JPA_COUVERT_VEGETAL, referencedColumnName = ListeVegetation.ID_JPA, nullable = false)
    private MassVegClassCouvertVegetalType couvertVegetal;
    // LES PROPRIETES
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = DatatypeVariableUniteACBB.ID_JPA, referencedColumnName = DatatypeVariableUniteACBB.ID_JPA, nullable = false)
    private DatatypeVariableUniteACBB datatypeVariableUnite;

    // LES CONSTRUCTEURS

    /**
     *
     */
    public MasseVegClass() {
        super();
    }

    /**
     * @param libelle
     * @param code
     * @param categorieCouvert
     * @param typeMasseVegetale
     * @param aerien
     * @param partiesPlante
     * @param devenir
     * @param couvertVegetal
     * @param definition
     * @param datatypeVariableUnite
     */
    public MasseVegClass(String libelle, String code,
                         MassVegClassCategorieCouvert categorieCouvert,
                         MassVegClassTypeMasseVegetale typeMasseVegetale, MassVegClassAerienSouterrain aerien,
                         MassVegClassPartieVegetaleType partiesPlante, MassVegClassDevenir devenir,
                         MassVegClassCouvertVegetalType couvertVegetal, String definition,
                         DatatypeVariableUniteACBB datatypeVariableUnite) {
        this.libelle = libelle;
        this.code = code;
        this.categorieCouvert = categorieCouvert;
        this.typeMasseVegetale = typeMasseVegetale;
        this.aerien = aerien;
        this.partiesPlante = partiesPlante;
        this.devenir = devenir;
        this.couvertVegetal = couvertVegetal;
        this.definition = definition;
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

    @Override
    public int compareTo(MasseVegClass o) {
        if (o == null) {
            return -1;
        }
        return this.code.compareTo(o.getCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final MasseVegClass other = (MasseVegClass) obj;
        return Objects.equals(this.code, other.code);
    }

    /**
     * @return
     */
    public MassVegClassAerienSouterrain getAerien() {
        return this.aerien;
    }

    /**
     * @param aerien
     */
    public void setAerien(MassVegClassAerienSouterrain aerien) {
        this.aerien = aerien;
    }

    /**
     * @return
     */
    public MassVegClassCategorieCouvert getCategorieCouvert() {
        return this.categorieCouvert;
    }

    /**
     * @param categorieCouvert
     */
    public void setCategorieCouvert(MassVegClassCategorieCouvert categorieCouvert) {
        this.categorieCouvert = categorieCouvert;
    }

    /**
     * @return
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return
     */
    public MassVegClassCouvertVegetalType getCouvertVegetal() {
        return this.couvertVegetal;
    }

    /**
     * @param couvertVegetal
     */
    public void setCouvertVegetal(MassVegClassCouvertVegetalType couvertVegetal) {
        this.couvertVegetal = couvertVegetal;
    }

    /**
     * @return
     */
    public DatatypeVariableUniteACBB getDatatypeVariableUnite() {
        return this.datatypeVariableUnite;
    }

    /**
     * @return
     */
    public String getDefinition() {
        return this.definition;
    }

    /**
     * @param definition
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    /**
     * @return
     */
    public MassVegClassDevenir getDevenir() {
        return this.devenir;
    }

    /**
     * @param devenir
     */
    public void setDevenir(MassVegClassDevenir devenir) {
        this.devenir = devenir;
    }

    /**
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return
     */
    public MassVegClassPartieVegetaleType getPartiesPlante() {
        return this.partiesPlante;
    }

    /**
     * @param partiesPlante
     */
    public void setPartiesPlante(MassVegClassPartieVegetaleType partiesPlante) {
        this.partiesPlante = partiesPlante;
    }

    /**
     * @return
     */
    public MassVegClassTypeMasseVegetale getTypeMasseVegetale() {
        return this.typeMasseVegetale;
    }

    /**
     * @param typeMasseVegetale
     */
    public void setTypeMasseVegetale(MassVegClassTypeMasseVegetale typeMasseVegetale) {
        this.typeMasseVegetale = typeMasseVegetale;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.code);
        return hash;
    }

    /**
     * @param datatypeVariableUnite
     */
    public void setdatatypeVariableUnite(DatatypeVariableUniteACBB datatypeVariableUnite) {
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

    /**
     * @param libelle
     * @param code
     * @param categorieCouvert
     * @param typeMasseVegetale
     * @param aerien
     * @param partiesPlante
     * @param devenir
     * @param couvertVegetal
     * @param definition
     * @param datatypeVariableUnite
     */
    public void update(String libelle, String code, MassVegClassCategorieCouvert categorieCouvert,
                       MassVegClassTypeMasseVegetale typeMasseVegetale, MassVegClassAerienSouterrain aerien,
                       MassVegClassPartieVegetaleType partiesPlante, MassVegClassDevenir devenir,
                       MassVegClassCouvertVegetalType couvertVegetal, String definition,
                       DatatypeVariableUniteACBB datatypeVariableUnite) {
        this.libelle = libelle;
        this.code = code;
        this.categorieCouvert = categorieCouvert;
        this.typeMasseVegetale = typeMasseVegetale;
        this.aerien = aerien;
        this.partiesPlante = partiesPlante;
        this.devenir = devenir;
        this.couvertVegetal = couvertVegetal;
        this.definition = definition;
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

}
