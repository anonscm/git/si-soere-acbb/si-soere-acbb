/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class WsolTypeOutil.
 */
@Entity
@DiscriminatorValue(FertForme.FERT_FORME)
public class FertForme extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "fert_forme_id";

    /**
     * The Constant WSOL_TYPE_OUTIL.
     */
    public static final String FERT_FORME = "fertilisant_forme";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
