/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis.jpa;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;
import java.util.Optional;

/**
 * @author ptcherniati
 */
public interface IIntervalDateSynthesisDAO {

    Optional<IntervalDate> getIntervalDateAvailable(List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypes);

}
