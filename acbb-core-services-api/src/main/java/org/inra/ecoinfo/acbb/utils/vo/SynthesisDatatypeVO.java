/*
 *
 */
package org.inra.ecoinfo.acbb.utils.vo;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The Class SynthesisDatatypeVO.
 */
public class SynthesisDatatypeVO implements Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The site @link(String).
     */
    String site;

    /**
     * The datatype @link(String).
     */
    String datatype;

    /**
     * The date min @link(Date).
     */
    LocalDateTime dateMin;

    /**
     * The date max @link(Date).
     */
    LocalDateTime dateMax;

    /**
     * Instantiates a new synthesis datatype vo.
     */
    public SynthesisDatatypeVO() {
        super();
    }

    /**
     * Instantiates a new synthesis datatype vo.
     *
     * @param synthesisDatatype the synthesis datatype
     * @param datatype          the datatype
     */
    public SynthesisDatatypeVO(final GenericSynthesisDatatype synthesisDatatype,
                               final String datatype) {
        this.site = synthesisDatatype.getSite();
        this.datatype = datatype;
        this.dateMin = synthesisDatatype.getMinDate();
        this.dateMax = synthesisDatatype.getMaxDate();
    }

    /**
     * Instantiates a new synthesis datatype vo.
     *
     * @param site     the site
     * @param datatype the datatype
     * @param dateMin  the date min
     * @param dateMax  the date max
     */
    public SynthesisDatatypeVO(final String site, final String datatype,
                               final LocalDateTime dateMin, final LocalDateTime dateMax) {
        super();
        this.site = site;
        this.datatype = datatype;
        this.setDateMin(dateMin);
        this.setDateMax(dateMax);
    }

    /**
     * Gets the datatype.
     *
     * @return the datatype
     */
    public String getDatatype() {
        return this.datatype;
    }

    /**
     * Sets the datatype.
     *
     * @param datatype the new datatype
     */
    public void setDatatype(final String datatype) {
        this.datatype = datatype;
    }

    /**
     * Gets the date max.
     *
     * @return the date max
     */
    public LocalDateTime getDateMax() {
        return this.dateMax;
    }

    /**
     * Sets the date max.
     *
     * @param dateMax the new date max
     */
    public void setDateMax(final LocalDateTime dateMax) {
        this.dateMax = dateMax;
    }

    /**
     * Gets the date min.
     *
     * @return the date min
     */
    public LocalDateTime getDateMin() {
        return this.dateMin;
    }

    /**
     * Sets the date min.
     *
     * @param dateMin the new date min
     */
    public void setDateMin(final LocalDateTime dateMin) {
        this.dateMin = dateMin;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public String getSite() {
        return this.site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }

}
