/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variablededescription;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class VariableDeConduite.
 */
@Entity
@DiscriminatorValue(VariableDescription.VARIABLE_DE_CONDUITE)
public class VariableDeConduite extends VariableDescription {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
