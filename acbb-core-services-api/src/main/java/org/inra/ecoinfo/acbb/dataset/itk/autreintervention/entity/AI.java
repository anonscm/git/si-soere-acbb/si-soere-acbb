package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class AI.
 * <p>
 * Record one line of the ai file
 */
@Entity
@Table(name = AI.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                AI.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, AbstractIntervention.ATTRIBUTE_JPA_DATE,
                SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "ai_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "ai_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "ai_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "ai_parcelle_idx", columnList = SuiviParcelle.ID_JPA)})
@DiscriminatorValue(AI.AI_DISCRIMINATOR)
@PrimaryKeyJoinColumn(name = AI.ID_JPA, referencedColumnName = "itk_id")
public class AI extends AbstractIntervention implements IMesureITK<AI, ValeurAI> {

    /**
     * The Constant AI_DISCRIMINATOR.
     */
    public static final String AI_DISCRIMINATOR = "autre_intervention";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "autre_intervention_ai";
    /**
     * The Constant ID_JPA.
     */
    static final String ID_JPA = "ai_id";

    /**
     * The valeurs ai.
     */
    @OneToMany(mappedBy = ValeurAI.ATTRIBUTE_JPA_AI, cascade = ALL)
    List<ValeurAI> valeursAI = new LinkedList();

    /**
     * Instantiates a new AI.
     */
    public AI() {
        super();
        this.setType(AI.AI_DISCRIMINATOR);
    }

    /**
     * Instantiates a new ai.
     *
     * @param versionFile
     * @param observation
     * @param date
     * @param rotation
     * @param annee
     * @param periode
     * @param tempo
     * @link{VersionFile the version file
     * @link{String the observation
     * @link{Date the date
     * @link{int the rotation
     * @link{int the annee
     * @link
     * @link{TraitementProgramme the traitement programme
     * @link{Tempo the tempo
     */
    public AI(final VersionFile versionFile, final String observation, final LocalDate date, final int rotation, final int annee, final int periode, final Tempo tempo) {
        super(versionFile, AI.AI_DISCRIMINATOR, observation, date, rotation, annee, periode);
        this.setTempo(tempo);
    }

    /**
     * @return
     */
    @Override
    public AI getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurAI> getValeurs() {
        return this.valeursAI;
    }

    /**
     * Gets the valeurs ai.
     *
     * @return the valeurs ai
     */
    public List<ValeurAI> getValeursAI() {
        return this.valeursAI;
    }

    /**
     * Sets the valeurs ai.
     *
     * @param valeursAI the new valeurs ai
     */
    public void setValeursAI(List<ValeurAI> valeursAI) {
        this.valeursAI = valeursAI;
    }

}
