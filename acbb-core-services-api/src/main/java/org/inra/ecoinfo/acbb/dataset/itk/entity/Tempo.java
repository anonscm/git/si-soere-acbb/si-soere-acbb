package org.inra.ecoinfo.acbb.dataset.itk.entity;

import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * The Class Tempo.
 * <p>
 * describe a real period
 */
@Entity
@Table(name = Tempo.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        VersionDeTraitement.ID_JPA, VersionTraitementRealisee.ID_JPA, AnneeRealisee.ID_JPA,
        PeriodeRealisee.ID_JPA}),
        indexes = {
                @Index(name = "temp_vdt_idx", columnList = VersionDeTraitement.ID_JPA),
                @Index(name = "temp_parcelle_idx", columnList = Parcelle.ID_JPA),
                @Index(name = "temp_vdr_idx", columnList = VersionTraitementRealisee.ID_JPA),
                @Index(name = "temp_annee_idx", columnList = AnneeRealisee.ID_JPA),
                @Index(name = "temp_periode_idx", columnList = AnneeRealisee.ID_JPA)})
public class Tempo implements Serializable {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "tempo_temp";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "tempo_id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ANNEE_REALISEE = "anneeRealisee";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PERIODE_REALISEE = "periodeRealisee";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ROTATION_REALISEE = "versionTraitementRealisee";

    /**
     * The id <long>.
     */
    @Id
    @Column(name = Tempo.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The version de traitement @link(VersionDeTraitement).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionDeTraitement.ID_JPA, referencedColumnName = VersionDeTraitement.ID_JPA, nullable = false)
    @NaturalId
    VersionDeTraitement versionDeTraitement;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Parcelle.ID_JPA, referencedColumnName = Parcelle.ID_JPA, nullable = false)
    Parcelle parcelle;

    /**
     * The version traitement realisee @link(VersionTraitementRealisee).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionTraitementRealisee.ID_JPA, referencedColumnName = VersionTraitementRealisee.ID_JPA, nullable = true)
    @NaturalId
    VersionTraitementRealisee versionTraitementRealisee;

    /**
     * The annee realisee @link(AnneeRealisee).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = AnneeRealisee.ID_JPA, referencedColumnName = AnneeRealisee.ID_JPA, nullable = true)
    @NaturalId
    AnneeRealisee anneeRealisee;

    @OneToMany(mappedBy = AbstractIntervention.ATTRIBUTE_JPA_TEMPO)
    @MapKey(name = "id")
    Map<Long, AbstractIntervention> interventions;

    /**
     * The periode realisee @link(PeriodeRealisee).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = PeriodeRealisee.ID_JPA, referencedColumnName = PeriodeRealisee.ID_JPA, nullable = true)
    @NaturalId
    PeriodeRealisee periodeRealisee;

    @Transient
    boolean inErrors = false;

    /**
     * Instantiates a new tempo.
     */
    public Tempo() {
        super();
    }

    /**
     * Instantiates a new tempo.
     *
     * @param versionDeTraitement
     * @param parcelle
     * @param versionTraitementRealisee
     * @param anneeRealisee
     * @param periodeRealisee
     * @link(VersionDeTraitement) the version de traitement
     * @link(VersionTraitementRealisee) the version traitement realisee
     * @link(AnneeRealisee) the annee realisee
     * @link(PeriodeRealisee) the periode realisee
     */
    public Tempo(final VersionDeTraitement versionDeTraitement, final Parcelle parcelle,
                 final VersionTraitementRealisee versionTraitementRealisee,
                 final AnneeRealisee anneeRealisee, final PeriodeRealisee periodeRealisee) {
        super();
        assert versionDeTraitement != null;
        this.versionDeTraitement = versionDeTraitement;
        this.parcelle = parcelle;
        assert versionTraitementRealisee == null
                || versionDeTraitement.equals(versionTraitementRealisee.getVersionDeTraitement());
        assert anneeRealisee == null
                || anneeRealisee.getVersionTraitementRealise().equals(versionTraitementRealisee);
        assert periodeRealisee == null || periodeRealisee.getAnneeRealisee().equals(anneeRealisee);
        this.versionTraitementRealisee = versionTraitementRealisee;
        this.anneeRealisee = anneeRealisee;
        this.periodeRealisee = periodeRealisee;
    }

    /**
     * Gets the annee realisee.
     *
     * @return the anneeRealisee
     */
    public AnneeRealisee getAnneeRealisee() {
        return this.anneeRealisee;
    }

    /**
     * Sets the annee realisee.
     *
     * @param anneeRealisee the anneeRealisee to set
     */
    public void setAnneeRealisee(final AnneeRealisee anneeRealisee) {
        assert anneeRealisee == null
                || this.versionTraitementRealisee.getAnneesRealisees().containsValue(anneeRealisee) : "l'année ne correspond pas à la version réalisée";
        this.anneeRealisee = anneeRealisee;
        if (anneeRealisee != null) {
            this.versionTraitementRealisee.getAnneesRealisees().put(anneeRealisee.getAnnee(),
                    anneeRealisee);
        }
    }

    /**
     * Gets the bindeds itk.
     *
     * @return the bindeds itk
     */
    public Set<AbstractIntervention> getBindedsITK() {
        return this.getInterventions() == null ? new TreeSet<>() : new TreeSet(
                this.getInterventions().values());
    }

    /**
     * Gets the id <long>.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id <long>.
     *
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the interventions
     */
    protected Map<Long, AbstractIntervention> getInterventions() {
        return this.interventions;
    }

    /**
     * @param interventions the interventions to set
     */
    protected void setInterventions(Map<Long, AbstractIntervention> interventions) {
        this.interventions = interventions;
    }

    /**
     * @return
     */
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    /**
     * @param parcelle
     */
    public void setParcelle(Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * Gets the periode realisee.
     *
     * @return the periodeRealisee
     */
    public PeriodeRealisee getPeriodeRealisee() {
        return this.periodeRealisee;
    }

    /**
     * Sets the periode realisee.
     *
     * @param periodeRealisee the periodeRealisee to set
     */
    public void setPeriodeRealisee(final PeriodeRealisee periodeRealisee) {
        assert periodeRealisee == null
                || this.anneeRealisee.getPeriodesRealisees().containsValue(periodeRealisee) : "la période ne correspond pas à l'année réalisée";
        this.periodeRealisee = periodeRealisee;
        if (periodeRealisee != null) {
            this.anneeRealisee.getPeriodesRealisees().put(periodeRealisee.getNumero(),
                    periodeRealisee);
        }
    }

    /**
     * Gets the version de traitement.
     *
     * @return the versionDeTraitement
     */
    public VersionDeTraitement getVersionDeTraitement() {
        return this.versionDeTraitement;
    }

    /**
     * Sets the version de traitement.
     *
     * @param versionDeTraitement the versionDeTraitement to set
     */
    public void setVersionDeTraitement(final VersionDeTraitement versionDeTraitement) {
        assert this.anneeRealisee == null
                || this.versionTraitementRealisee.getAnneesRealisees().containsValue(
                this.anneeRealisee) : "l'annee réalisee ne correspond pas à la version";
        this.versionDeTraitement = versionDeTraitement;
    }

    /**
     * Gets the version traitement realisee.
     *
     * @return the versionTraitementRealisee
     */
    public VersionTraitementRealisee getVersionTraitementRealisee() {
        return this.versionTraitementRealisee;
    }

    /**
     * Sets the version traitement realisee.
     *
     * @param versionTraitementRealisee the versionTraitementRealisee to set
     */
    public void setVersionTraitementRealisee(
            final VersionTraitementRealisee versionTraitementRealisee) {
        this.versionTraitementRealisee = versionTraitementRealisee;
    }

    /**
     * Checks if is bind by itk.
     *
     * @return the isBindByITK
     * @link(int) the itk id
     */
    public boolean hasBindByITK() {
        return this.getInterventions() != null && this.getInterventions().size() > 0;
    }

    /**
     * Checks if is bind by itk.
     *
     * @param itkId
     * @return the isBindByITK
     * @link(int) the itk id
     */
    public boolean isBindByITK(final long itkId) {
        return this.getInterventions() != null && this.getInterventions().containsKey(itkId);
    }

    /**
     * @return the inErrors
     */
    public boolean isInErrors() {
        return this.inErrors;
    }

    /**
     * @param inErrors the inErrors to set
     */
    public void setInErrors(final boolean inErrors) {
        this.inErrors = inErrors;
    }

}
