/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.dataset.IRecorder;

/**
 * The Interface IRecorderACBB.
 * <p>
 * interface objects used for testing, submit, publish and delete file versions
 *
 * @author Tcherniatinsky Philippe
 */
public interface IRecorderACBB extends IRecorder {

    /**
     * Sets the delete record.
     *
     * @param deleteRecord the new delete record {@link IDeleteRecord} setter for an object use to delete a
     *                     version of file
     */
    void setDeleteRecord(IDeleteRecord deleteRecord);

    /**
     * Sets the process record.
     *
     * @param processRecord the new process record {@link IProcessRecord} setter for an object use to process
     *                      the record of file
     */
    void setProcessRecord(IProcessRecord processRecord);

    /**
     * Sets the request properties name.
     *
     * @param requestPropertiesName the new request properties name {@link IRequestPropertiesACBB} use by
     *                              getRequestProperties() to get a new instance of IRequestProperties
     */
    void setRequestPropertiesName(String requestPropertiesName);

    /**
     * Sets the test format.
     *
     * @param testFormat the new test format {@link ITestFormat} setter for an object use to test ht format
     *                   of file
     */
    void setTestFormat(ITestFormat testFormat);
}
