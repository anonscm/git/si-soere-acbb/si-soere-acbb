package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class TravailDuSol.
 */
@Entity
@Table(name = Fertilisant.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        Fertilisant.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, Fertilisant.ATTRIBUTE_JPA_DATE,
        Fertilisant.ATTRIBUTE_JPA_RANG, FertType.ID_JPA, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "fert_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "fert_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "fert_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "fert_parcelle_idx", columnList = SuiviParcelle.ID_JPA),
                @Index(name = "fert_ferttype_idx", columnList = FertType.ID_JPA)})
@DiscriminatorValue(Fertilisant.FERTILISANT_DISCRIMINATOR)
//@AttributeOverrides(value = { @AttributeOverride(column = @Column(name = Fertilisant.ID_JPA), name = "id") })
@PrimaryKeyJoinColumn(name = Fertilisant.ID_JPA, referencedColumnName = AbstractIntervention.VARIABLE_NAME_ID)
public class Fertilisant extends AbstractIntervention implements
        IMesureITK<Fertilisant, ValeurFertilisant> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "fertilisant_fert";

    /**
     * The Constant TRAVAIL_DU_SOL @link(String).
     */
    public static final String FERTILISANT_DISCRIMINATOR = "fertilisant";

    /**
     * The Constant ATTRIBUTE_JPA_RANG.
     */
    public static final String ATTRIBUTE_JPA_RANG = "rang";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The rang @link(int).
     */
    @Column(name = Fertilisant.ATTRIBUTE_JPA_RANG, nullable = false)
    int rang = 1;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = FertType.ID_JPA, referencedColumnName = ListeItineraire.ID_JPA, nullable = false)
    FertType fertType;

    /**
     * The mesures travail du sol @link(List<MesureTravailDuSol>).
     */
    @OneToMany(mappedBy = "fertilisant", cascade = ALL)
    List<ValeurFertilisant> valeursFertilisant = new LinkedList();

    /**
     *
     */
    public Fertilisant() {
        super();
        this.type = Fertilisant.FERTILISANT_DISCRIMINATOR;
    }

    /**
     * @param versionFile
     * @param observation
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     */
    public Fertilisant(final VersionFile versionFile, final String observation, final LocalDate date, final int versionTraitementRealiseeNumber, final int anneeRealiseeNumber, final int periodeRealiseeNumber) {
        super(versionFile, Fertilisant.FERTILISANT_DISCRIMINATOR, observation, date,
                versionTraitementRealiseeNumber, anneeRealiseeNumber, periodeRealiseeNumber);

    }

    /**
     * @return
     */
    public FertType getFertType() {
        return this.fertType;
    }

    /**
     * @param fertType
     */
    public void setFertType(FertType fertType) {
        this.fertType = fertType;
    }

    /**
     * Gets the rang.
     *
     * @return the rang
     */
    public int getRang() {
        return this.rang;
    }

    /**
     * Sets the rang.
     *
     * @param rang the new rang
     */
    public void setRang(final int rang) {
        this.rang = rang;
    }

    /**
     * @return
     */
    @Override
    public Fertilisant getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurFertilisant> getValeurs() {
        return this.valeursFertilisant;
    }

    /**
     * @return
     */
    public List<ValeurFertilisant> getValeursFertilisant() {
        return this.valeursFertilisant;
    }

    /**
     * @param valeursFertilisant
     */
    public void setValeursFertilisant(final List<ValeurFertilisant> valeursFertilisant) {
        this.valeursFertilisant = valeursFertilisant;
    }

}
