/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.listeitineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(InterventionType.INTERVENTION_TYPE)
public class InterventionType extends ListeItineraire {

    /**
     *
     */
    public static final String INTERVENTION_TYPE = "intervention_type";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "intervention_type";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
