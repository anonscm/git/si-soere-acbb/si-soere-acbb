package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.dataset.itk.entity.PeriodeRealisee;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;

import java.util.Optional;

/**
 * A factory for creating IPeriodeRealisee objects.
 *
 * @param <T> the generic type
 */
public interface IPeriodeRealiseeFactory<T extends ILineRecord> {

    /**
     * Creates a new IPeriodeRealisee object.
     *
     * @param anneeRealisee
     * @param errorsReport
     * @link{AnneeRealisee the annee realisee
     * @link{ErrorsReport the errors report
     * @link(AnneeRealisee) the annee realisee
     * @link(ErrorsReport) the errors report
     */
    void createDefaultPeriode(AnneeRealisee anneeRealisee, ErrorsReport errorsReport);

    /**
     * Gets the default periode.
     *
     * @param returnAnneeRealisee
     * @return the default periode
     * @link{AnneeRealisee the return annee realisee
     * @link(AnneeRealisee) the return annee realisee
     */
    PeriodeRealisee getDefaultPeriode(AnneeRealisee returnAnneeRealisee);

    /**
     * Gets the or create periode realisee.
     *
     * @param line
     * @param errorsReport
     * @return the or create periode realisee
     * @link{T the line
     * @link{ErrorsReport the errors report
     * @link(T) the line
     * @link(ErrorsReport) the errors report
     */
    Optional<PeriodeRealisee> getOrCreatePeriodeRealisee(T line, ErrorsReport errorsReport);

}
