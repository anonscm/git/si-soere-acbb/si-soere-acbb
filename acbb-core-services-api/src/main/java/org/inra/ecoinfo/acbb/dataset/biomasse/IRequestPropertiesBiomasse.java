/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.utils.Column;

import java.util.Map;

/**
 * The Interface IRequestPropertiesBiomasse.
 */
public interface IRequestPropertiesBiomasse extends IRequestPropertiesACBB {

    /**
     * Gets the value columns.
     *
     * @return the value columns
     */
    Map<Integer, Column> getValueColumns();

}
