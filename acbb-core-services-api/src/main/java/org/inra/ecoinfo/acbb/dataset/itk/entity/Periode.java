package org.inra.ecoinfo.acbb.dataset.itk.entity;

/**
 * @author ptcherniati
 */
public class Periode {

    private VersionTraitementRealisee versionTraitementRealisee;
    private AnneeRealisee anneeRealisee;
    private PeriodeRealisee periodeRealisee;

    /**
     *
     */
    public Periode() {
        super();
    }

    /**
     * @return the anneeRealisee
     */
    public AnneeRealisee getAnneeRealisee() {
        return this.anneeRealisee;
    }

    /**
     * @param anneeRealisee the anneeRealisee to set
     */
    public void setAnneeRealisee(AnneeRealisee anneeRealisee) {
        this.anneeRealisee = anneeRealisee;
    }

    /**
     * @return the periodeRealisee
     */
    public PeriodeRealisee getPeriodeRealisee() {
        return this.periodeRealisee;
    }

    /**
     * @param periodeRealisee the periodeRealisee to set
     */
    public void setPeriodeRealisee(PeriodeRealisee periodeRealisee) {
        this.periodeRealisee = periodeRealisee;
    }

    /**
     * @return the versionTraitementRealisee
     */
    public VersionTraitementRealisee getVersionTraitementRealisee() {
        return this.versionTraitementRealisee;
    }

    /**
     * @param versionTraitementRealisee the versionTraitementRealisee to set
     */
    public void setVersionTraitementRealisee(VersionTraitementRealisee versionTraitementRealisee) {
        this.versionTraitementRealisee = versionTraitementRealisee;
    }

}
