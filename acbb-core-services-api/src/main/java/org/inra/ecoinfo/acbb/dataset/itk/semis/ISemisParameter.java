/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.ValeurSemis;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;

import java.util.List;

/**
 * The Interface ISemisParameter.
 */
public interface ISemisParameter {

    /**
     * Gets the valeurs semis.
     *
     * @return the valeurs semis
     */
    List<ValeurSemis> getvaleursSemis();

    /**
     * @return
     */
    List<VariableACBB> getVariablesSemis();

    /**
     * @param variablesSemis
     */
    void setVariablesSemis(List<VariableACBB> variablesSemis);

    /**
     * Sets the valeur semis.
     *
     * @param valeursSemis the new valeur semis
     */
    void setValeurSemis(List<ValeurSemis> valeursSemis);

}
