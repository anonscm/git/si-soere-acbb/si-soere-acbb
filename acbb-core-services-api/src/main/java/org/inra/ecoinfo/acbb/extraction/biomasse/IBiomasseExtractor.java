package org.inra.ecoinfo.acbb.extraction.biomasse;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author vkoyao
 */
public interface IBiomasseExtractor {

    /**
     * @param parameters
     * @throws BusinessException
     */
    void extract(IParameter parameters) throws BusinessException;

    long getExtractionSize(IParameter parameters);

    /**
     * @return
     */
    String getExtractCode();

}
