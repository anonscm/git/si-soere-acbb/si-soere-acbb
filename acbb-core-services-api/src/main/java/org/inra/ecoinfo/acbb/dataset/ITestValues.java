/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.Serializable;

/**
 * object interface used to test the consistency of the values ​​of a file.
 *
 * @author Tcherniatinsky Philippe
 */
public interface ITestValues extends Serializable {

    /**
     * test the data of a file.
     *
     * @param startline         int the startline
     * @param parser            the parser
     * @param versionFile
     * @param requestProperties
     * @param encoding          the encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException the business exception {@link VersionFile} the version file
     *                           {@link IRequestPropertiesACBB} the request properties {@linkBadsFormatsReport}
     *                           the bads formats report {@link DatasetDescriptorACBB} the dataset descriptor
     *                           {@link String} the datatype name
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(String) the datatype name
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(String) the datatype name
     */
    void testValues(long startline, CSVParser parser, VersionFile versionFile,
                    IRequestPropertiesACBB requestProperties, String encoding,
                    BadsFormatsReport badsFormatsReport, DatasetDescriptorACBB datasetDescriptor,
                    String datatypeName) throws BusinessException;

}
