package org.inra.ecoinfo.acbb.identification.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.ResourceBundle;
import org.inra.ecoinfo.identification.entity.IRightRequest;

/**
 * @author ptcherniati
 */
@Entity(name = RightsRequest.NAME_ENTITY)
@Table(indexes = {
        @Index(name = "rightrequest_utilisateur_idx", columnList = RightsRequest.UTILISATEUR_ID_JPA)})
public class RightsRequest implements IRightRequest,Serializable {

    /**
     *
     */
    public static final String ID_JPA = "rightsrequest";

    /**
     *
     */
    public static final String NAME_ENTITY = "acbb_rightsrequest";

    /**
     *
     */
    public static final String UTILISATEUR_ID_JPA = "usr_id";
    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(RightsRequest.class);
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.identification.message";
    private static final String PROPERTY_MSG_REQUEST_TO_STRING = "PROPERTY_MSG_REQUEST_TO_STRING";
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = RightsRequest.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @CreationTimestamp
    private LocalDateTime createDate;
    private boolean validated;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String nom;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String prenom;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String email;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String fonction;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String institution;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String workAdress;
    @Column(nullable = true)
    private String phone;
    @Column(nullable = true)
    private String relation;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String title;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String objectives;
    @Column(nullable = false)
    private String[] sites;
    @Column(nullable = false)
    private String treatment;
    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String requiredData;
    @Column(nullable = false)
    @NotNull
    private LocalDateTime startDate = null;
    @Column(nullable = false)
    @NotNull
    private LocalDateTime endDate = null;
    @Column(nullable = false)
    private String additionalsInformations;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = UTILISATEUR_ID_JPA, referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false)
    private Utilisateur utilisateur;

    /**
     *
     */
    public RightsRequest() {
    }

    /**
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    /**
     * @return
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * @param validated
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    /**
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return
     */
    public String getFonction() {
        return fonction;
    }

    /**
     * @param fonction
     */
    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    /**
     * @return
     */
    public String getInstitution() {
        return institution;
    }

    /**
     * @param institution
     */
    public void setInstitution(String institution) {
        this.institution = institution;
    }

    /**
     * @return
     */
    public String getWorkAdress() {
        return workAdress;
    }

    /**
     * @param workAdress
     */
    public void setWorkAdress(String workAdress) {
        this.workAdress = workAdress;
    }

    /**
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return
     */
    public String getRelation() {
        return relation;
    }

    /**
     * @param relation
     */
    public void setRelation(String relation) {
        this.relation = relation;
    }

    /**
     * @return
     */
    public String getObjectives() {
        return objectives;
    }

    /**
     * @param objectives
     */
    public void setObjectives(String objectives) {
        this.objectives = objectives;
    }

    /**
     * @return
     */
    public String getTreatment() {
        return treatment;
    }

    /**
     * @param treatment
     */
    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    /**
     * @return
     */
    public String getRequiredData() {
        return requiredData;
    }

    /**
     * @param requiredData
     */
    public void setRequiredData(String requiredData) {
        this.requiredData = requiredData;
    }

    /**
     * @return
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * @return
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    /**
     * @return
     */
    public String getAdditionalsInformations() {
        return additionalsInformations;
    }

    /**
     * @param additionalsInformations
     */
    public void setAdditionalsInformations(String additionalsInformations) {
        this.additionalsInformations = additionalsInformations;
    }

    /**
     * @return
     */
    public Utilisateur getUser() {
        return utilisateur;
    }

    /**
     * @param utilisateur
     */
    public void setUser(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_SOURCE_PATH, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
        String message = resourceBundle.getString(PROPERTY_MSG_REQUEST_TO_STRING);
        String sd = DateUtil.getUTCDateTextFromLocalDateTime(startDate, DateUtil.DD_MM_YYYY);
        String ed = DateUtil.getUTCDateTextFromLocalDateTime(endDate, DateUtil.DD_MM_YYYY);
        message = String.format(message,
                email, fonction, institution, workAdress, phone, relation, title, objectives, getSitesNames(), treatment, requiredData, sd, ed);
        return message;

    }

    /**
     * @return
     */
    public String getSitesNames() {
        return org.apache.commons.lang.StringUtils.join(sites, ", ");
    }

    /**
     * @return
     */
    public String[] getSites() {
        return sites;
    }

    /**
     * @param sites
     */
    public void setSites(String[] sites) {
        this.sites = sites;
    }
}
