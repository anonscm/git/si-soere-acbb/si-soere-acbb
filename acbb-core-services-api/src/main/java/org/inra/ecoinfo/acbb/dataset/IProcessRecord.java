/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.Serializable;

/**
 * The Interface IprocessRecord.
 * <p>
 * interface objects used to publish versions of files
 *
 * @author Tcherniatinsky Philippe
 */
public interface IProcessRecord extends Serializable {

    /**
     * Process record of the file.
     *
     * @param parser                the parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding          the file encoding
     * @param datasetDescriptorACBB
     * @throws BusinessException the business exception {@link VersionFile} the version file
     *                           {@link IRequestPropertiesACBB} the request properties
     *                           {@link DatasetDescriptorACBB} the {@link DatasetDescriptorACBB}
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(DatasetDescriptorACBB) the dataset descriptor acbb
     */
    void processRecord(CSVParser parser, VersionFile versionFile,
                       IRequestPropertiesACBB requestProperties, String fileEncoding,
                       DatasetDescriptorACBB datasetDescriptorACBB) throws BusinessException;

}
