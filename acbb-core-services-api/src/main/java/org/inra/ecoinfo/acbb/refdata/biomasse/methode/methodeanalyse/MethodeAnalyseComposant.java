/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity(name = MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS)
@DiscriminatorValue(MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS)
public class MethodeAnalyseComposant extends ListeVegetation {

    /**
     *
     */
    public static final String MET_ANALYSE_COMPOSANTS = "met_analyse_biomasse_element";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "met_anal_biomasse_element";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
