/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PaturageTypeAnimaux.
 */
@Entity
@DiscriminatorValue(PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX)
public class PaturageTypeAnimaux extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "pat_type_animaux";

    /**
     *
     */
    public static final String PATURAGE_TYPE_ANIMAUX = "pat_type_animaux";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
