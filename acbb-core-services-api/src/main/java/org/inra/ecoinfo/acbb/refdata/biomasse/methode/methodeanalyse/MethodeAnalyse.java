/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.*;
import java.util.List;

/**
 * The Class MethodeAnalyse.
 */
@Entity(name = MethodeAnalyse.NAME_ENTITY_JPA)
@Table(name = MethodeAnalyse.NAME_ENTITY_JPA)
public class MethodeAnalyse extends AbstractMethode {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "methode_analyse_biomasse";

    /**
     *
     */
    public static final String NAME_ENTITY_MET_ANAL_MET_ANAL_COMPOSANT = "met_analyse_biomassse_met_anal_composant";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_EQUATION_QUALITE_PREDICTION = "met_equation_qualite_prediction";

    // CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    @Column(name = MethodeAnalyse.ATTRIBUTE_JPA_EQUATION_QUALITE_PREDICTION)
    private String equationQualitePrediction = org.apache.commons.lang.StringUtils.EMPTY;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = MethodeAnalyseComposant.class)
    @JoinTable(name = MethodeAnalyse.NAME_ENTITY_MET_ANAL_MET_ANAL_COMPOSANT, joinColumns = @JoinColumn(name = MethodeAnalyse.ID_JPA, referencedColumnName = MethodeAnalyse.ID_JPA), inverseJoinColumns = @JoinColumn(name = MethodeAnalyseComposant.ID_JPA, referencedColumnName = ListeVegetation.ID_JPA))
    private List<MethodeAnalyseComposant> composants;

    // LES CONSTRUCTEURS

    /**
     * Constructor
     */
    public MethodeAnalyse() {
        super();
    }

    /**
     * @param libelle
     * @param definition
     * @param equationQualitePrediction
     * @param numberId
     * @param composants
     */
    public MethodeAnalyse(String libelle, String definition, String equationQualitePrediction,
                          Long numberId, List<MethodeAnalyseComposant> composants) {
        super(libelle, definition, numberId);
        this.equationQualitePrediction = equationQualitePrediction;
        this.composants = composants;
    }

    /**
     * @return
     */
    public String getEquationQualitePrediction() {
        return equationQualitePrediction;
    }

    /**
     * @param equationQualitePrediction
     */
    public void setEquationQualitePrediction(String equationQualitePrediction) {
        this.equationQualitePrediction = equationQualitePrediction;
    }

    /**
     * @return
     */
    public List<MethodeAnalyseComposant> getComposants() {
        return composants;
    }

    /**
     * @param composants
     */
    public void setComposants(List<MethodeAnalyseComposant> composants) {
        this.composants = composants;
    }

    /**
     * @param composants
     */
    public void update(List<MethodeAnalyseComposant> composants) {
        this.composants = composants;

    }
}
