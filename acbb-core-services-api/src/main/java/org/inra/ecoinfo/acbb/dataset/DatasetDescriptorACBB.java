/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class DatasetDescriptorACBB.
 * <p>
 * descriptor of the data of a file
 * <p>
 * extends {@link DatasetDescriptor}
 */
public class DatasetDescriptorACBB extends DatasetDescriptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetDescriptorACBB.class
            .getName());

    /**
     * The en tete @link(int).
     */
    int enTete;

    /**
     * The headers line @link(int).
     */
    int headersLine = -1;

    /**
     * The min line @link(int).
     */
    int minLine = -1;

    /**
     * The Max line @link(int).
     */
    int maxLine = -1;

    /**
     * The mins @link(Map<String,Float>).
     */
    Map<String, Float> mins = new HashMap();

    /**
     * The maxs @link(Map<String,Float>).
     */
    Map<String, Float> maxs = new HashMap();

    /**
     * The variable name @link(String).
     */
    String variableName;

    /**
     * Gets the column nom.
     *
     * @param nom
     * @return {@link Column} the column {@link String} the nom
     * @link(String) the nom
     */
    public Column getColumn(final String nom) {
        for (final Column column : this.getColumns()) {
            if (column.getName().equals(nom)) {
                return column;
            }
        }
        return null;
    }

    /**
     * Gets the column name.
     *
     * @param columnIndex int the column
     * @return {@link String} the column name
     */
    public String getColumnName(final int columnIndex) {
        return this.getColumns().get(columnIndex).getName();
    }

    /**
     * Gets the en tete.
     *
     * @return the length of header
     */
    public int getEnTete() {
        return this.enTete;
    }

    /**
     * Sets the en tete.
     *
     * @param enTete int the new en tete
     */
    public final void setEnTete(final int enTete) {
        this.enTete = enTete;
    }

    /**
     * Sets the en tete.
     *
     * @param enTete the new en tete @link(int) {@link String} the new en tete
     */
    public final void setEnTete(final String enTete) {
        this.enTete = Integer.parseInt(enTete);
    }

    /**
     * Gets the headers line.
     *
     * @return the headers line number
     */
    public int getHeadersLine() {
        return this.headersLine;
    }

    /**
     * Sets the headers line number from {@link String}.
     *
     * @param headersLine the new headers line @link(int) {@link String} the new headers line
     */
    public final void setHeadersLine(final String headersLine) {
        try {
            this.headersLine = Integer.parseInt(headersLine);
        } catch (final NumberFormatException e) {
            DatasetDescriptorACBB.LOGGER.info("can't parse headersLine");
        }
    }

    /**
     * Gets the max line.
     *
     * @return the max line number
     */
    public int getMaxLine() {
        return this.maxLine;
    }

    /**
     * Sets the max line.
     *
     * @param maxLine number from {@link String} the new max line
     */
    public final void setMaxLine(final String maxLine) {
        try {
            this.maxLine = Integer.parseInt(maxLine);
        } catch (final NumberFormatException e) {
            DatasetDescriptorACBB.LOGGER.info("can't parse maxLine");
        }
    }

    /**
     * Gets the maxs.
     *
     * @return the {@link Map} of max values of {@link Column}
     */
    public Map<String, Float> getMaxs() {
        return this.maxs;
    }

    /**
     * Sets the {@link Map} of max values of {@link Column}.
     *
     * @param maxs the maxs
     */
    public final void setMaxs(final Map<String, Float> maxs) {
        this.maxs = maxs;
    }

    /**
     * Gets the min line.
     *
     * @return the min line number
     */
    public int getMinLine() {
        return this.minLine;
    }

    /**
     * Sets the min line.
     *
     * @param minLine number from {@link String} the new min line
     */
    public final void setMinLine(final String minLine) {
        try {
            this.minLine = Integer.parseInt(minLine);
        } catch (final NumberFormatException e) {
            DatasetDescriptorACBB.LOGGER.info("can't parse minLine");
        }
    }

    /**
     * Gets the mins.
     *
     * @return the {@link Map} of min values of {@link Column}
     */
    public Map<String, Float> getMins() {
        return this.mins;
    }

    /**
     * Sets the {@link Map} of min values of {@link Column}.
     *
     * @param mins the mins
     */
    public final void setMins(final Map<String, Float> mins) {
        this.mins = mins;
    }

    /**
     * Gets the referenced column of th columnIndex {@link Column}.
     *
     * @param columnIndex the i
     * @return the referenced column
     */
    public int getReferencedColumn(final int columnIndex) {
        if (columnIndex > this.getColumns().size()) {
            return -1;
        }
        final String referencedName = this.getColumns().get(columnIndex).getRefVariableName();
        if (referencedName != null) {
            return this.getColumns().indexOf(this.getColumn(referencedName));
        }
        return -1;
    }

    /**
     * Gets the variable name.
     *
     * @return {@link String} the variable name
     */
    public String getVariableName() {
        return this.variableName;
    }

    /**
     * Sets the variable name.
     *
     * @param variableName the new variable name
     */
    public final void setVariableName(final String variableName) {
        this.variableName = variableName;
    }

}
