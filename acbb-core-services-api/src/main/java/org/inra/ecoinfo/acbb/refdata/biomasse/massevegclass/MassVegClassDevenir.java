/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassDevenir.MAV_CLASS_DEVENIR)
public class MassVegClassDevenir extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_DEVENIR = "mav_class_devenir";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_devenir";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
