/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variablededescription;

import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.modalite.Modalite;
import org.inra.ecoinfo.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class VariableDescription.
 */
@Entity
@Table(name = VariableDescription.NAME_ENTITY_JPA, uniqueConstraints = {
        @UniqueConstraint(columnNames = {VariableDescription.ATTRIBUTE_JPA_CODE})},
        indexes = {
                @Index(name = "vd_code_idx", columnList = "code")})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorFormula(value = VariableDescription.ATTRIBUTE_JPA_TYPE)
public class VariableDescription implements Serializable, Comparable<VariableDescription> {

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ATTRIBUTE_JPA_CODE = "code";

    /**
     * The Constant ATTRIBUTE_JPA_NOM.
     */
    public static final String ATTRIBUTE_JPA_NAME = "nom";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TYPE = "type";

    /**
     * The Constant ATTRIBUTE_JPA_DESCRIPTION.
     */
    public static final String ATTRIBUTE_JPA_DESCRIPTION = "description";

    /**
     * The Constant VARIABLE_DE_FORCAGE.
     */
    public static final String VARIABLE_DE_FORCAGE = "variable_de_forcage";

    /**
     * The Constant VARIABLE_DE_CONDUITE.
     */
    public static final String VARIABLE_DE_CONDUITE = "variable_de_conduite";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "variable_description_vdes";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vdes_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = VariableDescription.ID_JPA)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    /**
     * The nom @link(String).
     */
    @Column(nullable = false, name = VariableDescription.ATTRIBUTE_JPA_NAME)
    String nom;

    /**
     * The code @link(String).
     */
    @Column(nullable = false, name = VariableDescription.ATTRIBUTE_JPA_CODE)
    @NaturalId
    String code;

    /**
     * The type @link(String).
     */
    @Column(nullable = false, name = VariableDescription.ATTRIBUTE_JPA_TYPE)
    String type = VariableDescription.VARIABLE_DE_FORCAGE;

    /**
     * The description @link(String).
     */
    @Column(nullable = true, name = VariableDescription.ATTRIBUTE_JPA_DESCRIPTION)
    String description;

    /**
     * The modalites @link(List<Modalite>).
     */
    @OneToMany(mappedBy = "variableDescription", cascade = ALL)
    List<Modalite> modalites = new LinkedList();

    /**
     * Instantiates a new variable description.
     */
    public VariableDescription() {
        super();
    }

    /**
     * Instantiates a new variable description.
     *
     * @param nom         the nom
     * @param description the description
     * @param type        the type
     */
    public VariableDescription(final String nom, final String description, final String type) {
        super();
        this.setNom(nom);
        this.type = type;
        this.description = description;
    }

    @Override
    public int compareTo(VariableDescription o) {
        if (o == null) {
            return -1;
        }
        return this.type.compareTo(o.type) == 0 ? this.code.compareTo(o.code) : this.type
                .compareTo(o.type);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final VariableDescription other = (VariableDescription) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return Objects.equals(this.type, other.type);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the modalites.
     *
     * @return the modalites
     */
    public List<Modalite> getModalites() {
        return this.modalites;
    }

    /**
     * Sets the modalites.
     *
     * @param variableDescriptions the new modalites
     */
    public void setModalites(final List<Modalite> variableDescriptions) {
        this.modalites = variableDescriptions;
    }

    /**
     * Gets the new variable de conduite.
     *
     * @param nom         the nom
     * @param description the description
     * @return the new variable de conduite
     */
    public VariableDescription getNewVariableDeConduite(final String nom, final String description) {
        return new VariableDescription(nom, VariableDescription.VARIABLE_DE_CONDUITE, description);
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom the new nom
     */
    public void setNom(final String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.code);
        hash = 29 * hash + Objects.hashCode(this.type);
        return hash;
    }

}
