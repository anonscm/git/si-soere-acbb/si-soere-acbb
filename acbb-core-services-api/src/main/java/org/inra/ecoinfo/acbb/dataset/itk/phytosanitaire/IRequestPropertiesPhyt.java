package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire;

import org.inra.ecoinfo.acbb.dataset.itk.IRequestPropertiesITK;

/**
 * @author ptcherniati
 */
public interface IRequestPropertiesPhyt extends IRequestPropertiesITK {

}
