/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassTypeUtilisation.MAV_CLASS_TYPE_UTILISATION_TYPE)
public class MassVegClassTypeUtilisation extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_TYPE_UTILISATION_TYPE = "mav_class_type_utilisation";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_type_utilisation";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
