/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.variablededescription;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class VariableDeForcage.
 */
@Entity
@DiscriminatorValue(VariableDescription.VARIABLE_DE_FORCAGE)
public class VariableDeForcage extends VariableDescription {

    /**
     * The Constant ROTATION_VARIABLE_DE_FORCAGE.
     */
    public static final String ROTATION_VARIABLE_DE_FORCAGE = "rotation";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
