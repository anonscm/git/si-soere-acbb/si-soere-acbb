package org.inra.ecoinfo.acbb.refdata.listesacbb;

import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.BiomasseEntreeSortie;
import org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal.entity.HauteurVegetalNature;
import org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity.LaiNature;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AutNature;
import org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity.AutType;
import org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity.FneType;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertEtat;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertForme;
import org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity.FertType;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.PaturageTypeAnimaux;
import org.inra.ecoinfo.acbb.dataset.itk.paturage.entity.PaturageTypeComplementation;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.NaturePlante;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFauNaturePlante;
import org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity.RecFauRepousse;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.*;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.WsolTypeObjectif;
import org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity.WsolTypeOutil;
import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;
import org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass.*;
import org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeanalyse.MethodeAnalyseComposant;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.InterventionType;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.PhytNamm;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.PhytObjectif;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.PhytType;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The Class ListeACBB.
 */
@Entity
@Table(name = ValeurQualitative.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {ValeurQualitative.ATTRIBUTE_JPA_VALUE, ValeurQualitative.ATTRIBUTE_JPA_CODE}))
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorFormula(value = ListeACBB.CASES)
@DiscriminatorValue("null")
public class ListeACBB implements IValeurQualitative, Serializable, Comparable<ListeACBB> {

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ATTRIBUTE_JPA_CODE = ValeurQualitative.ATTRIBUTE_JPA_CODE;

    /**
     * The Constant ATTRIBUTE_JPA_VALEUR.
     */
    public static final String ATTRIBUTE_JPA_VALEUR = ValeurQualitative.ATTRIBUTE_JPA_VALUE;

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = ValeurQualitative.NAME_ENTITY_JPA;

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = ValeurQualitative.ID_JPA;

    /**
     * The Constant SEPARATOR_CASES.
     */
    protected static final String SEPARATOR_CASES = "', '";
    /**
     * The Constant CASES.
     */
    static final String CASES = "case when typeListe = '"
            + ListeItineraire.LISTE_ITINERAIRE_TYPE
            + "' and code in ('"
            + InterventionType.INTERVENTION_TYPE
            + ListeACBB.SEPARATOR_CASES
            + RecFauRepousse.REC_FAU_REPOUSSE
            + ListeACBB.SEPARATOR_CASES
            + RecFauNaturePlante.RECFAU_NATURE_PLANTE
            + ListeACBB.SEPARATOR_CASES
            + NaturePlante.REC_FAU_NATURE_PLANTE
            + ListeACBB.SEPARATOR_CASES
            + FneType.FNE_TYPE
            + ListeACBB.SEPARATOR_CASES
            + WsolTypeObjectif.WSOL_OBJECTIF
            + ListeACBB.SEPARATOR_CASES
            + WsolTypeOutil.WSOL_TYPE_OUTIL
            + ListeACBB.SEPARATOR_CASES
            + SemisObjectif.SEMIS_OBJECTIF
            + ListeACBB.SEPARATOR_CASES
            + SemisAnnuelle.SEMIS_ANNUELLE
            + ListeACBB.SEPARATOR_CASES
            + SemisCouvertVegetal.SEMIS_COUVERT_VEGETAL
            + ListeACBB.SEPARATOR_CASES
            + SemisEspece.SEMIS_SPECIES
            + ListeACBB.SEPARATOR_CASES
            + SemisFamille.SEMIS_FAMILLE
            + ListeACBB.SEPARATOR_CASES
            + SemisEnrob.SEMIS_ENROB
            + ListeACBB.SEPARATOR_CASES
            + PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX
            + ListeACBB.SEPARATOR_CASES
            + PaturageTypeComplementation.PATURAGE_TYPE_COMPLEMENTATION
            + ListeACBB.SEPARATOR_CASES
            + FertType.FERT_TYPE
            + ListeACBB.SEPARATOR_CASES
            + FertForme.FERT_FORME
            + ListeACBB.SEPARATOR_CASES
            + FertEtat.FERT_ETAT
            + ListeACBB.SEPARATOR_CASES
            + AutType.AUT_TYPE
            + ListeACBB.SEPARATOR_CASES
            + AutNature.AUT_NATURE
            + ListeACBB.SEPARATOR_CASES
            + PhytNamm.NAMM
            + ListeACBB.SEPARATOR_CASES
            + PhytObjectif.PHYTOSANITAIRE_OBJECTIF
            + ListeACBB.SEPARATOR_CASES
            + PhytType.PHYTOSANITAIRE_TYPE
            + ListeACBB.SEPARATOR_CASES
            + "') "
            + " then code  "
            + "\n"
            + "when typeListe = '"
            + ListeVegetation.LISTE_VEGETATION_TYPE
            + "' and code in ('"
            + MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassDevenir.MAV_CLASS_DEVENIR
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassTypeMasseVegetale.MAV_CLASS_TYPE_MASSE_VEGETALE
            + ListeACBB.SEPARATOR_CASES
            + MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassCouvertVegetalType.MAV_CLASS_COUVERT_VEGETAL
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassTypeUtilisation.MAV_CLASS_TYPE_UTILISATION_TYPE
            + ListeACBB.SEPARATOR_CASES
            + BiomasseEntreeSortie.BIOMASSE_ENTREE_SORTIE
            + ListeACBB.SEPARATOR_CASES
            + LaiNature.LAI_NATURE
            + ListeACBB.SEPARATOR_CASES
            + HauteurVegetalNature.HAV_NATURE
            + "') "
            + " then code "
            + " when typeListe = '"
            + ListeItineraire.LISTE_ITINERAIRE_TYPE
            + "' and code in (select vq.valeur from "
            + ValeurQualitative.NAME_ENTITY_JPA
            + " as vq "
            + " where vq.code = '"
            + SemisEspece.SEMIS_SPECIES
            + "') "
            + " then '"
            + SemisVariete.SEMIS_VARIETE
            + "' when typeListe = '"
            + ListeItineraire.LISTE_ITINERAIRE_TYPE
            + "' and code in (select vq.valeur from "
            + ValeurQualitative.NAME_ENTITY_JPA
            + " as vq "
            + " where vq.code = '"
            + PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX
            + "') "
            + " then '"
            + PaturageTypeAnimaux.PATURAGE_TYPE_ANIMAUX
            + "' "
            + "\n"
            + "when typeListe = '"
            + ListeVegetation.LISTE_VEGETATION_TYPE
            + "' and code in ('"
            + MassVegClassCategorieCouvert.MAV_CLASS_CATEGORIE_COUVERT
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassPartieVegetaleType.MAV_CLASS_PART_VEG_TYPE
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassDevenir.MAV_CLASS_DEVENIR
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassTypeMasseVegetale.MAV_CLASS_TYPE_MASSE_VEGETALE
            + ListeACBB.SEPARATOR_CASES
            + MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassCouvertVegetalType.MAV_CLASS_COUVERT_VEGETAL
            + ListeACBB.SEPARATOR_CASES
            + MassVegClassTypeUtilisation.MAV_CLASS_TYPE_UTILISATION_TYPE
            + ListeACBB.SEPARATOR_CASES
            + BiomasseEntreeSortie.BIOMASSE_ENTREE_SORTIE
            + ListeACBB.SEPARATOR_CASES
            + LaiNature.LAI_NATURE
            + ListeACBB.SEPARATOR_CASES
            + HauteurVegetalNature.HAV_NATURE
            + "') "
            + " then code "
            + "\n"
            + "when typeListe = 'liste_soil' and code in ('met_analyse_soil_element','met_texture_soil_element') "
            + " then code "
            + "else typeListe end";

    /**
     * The Constant serialVersionUID <long>.
     */
    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    protected String typeListe = "null";
    // ATTRIBUTS
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = IValeurQualitative.ID_JPA)
    protected Long id;
    /**
     * The valeur @link(String).
     */
    @Column(nullable = false, name = IValeurQualitative.ATTRIBUTE_JPA_VALUE)
    @NaturalId
    private String valeur;
    /**
     * The id @link(Long).
     */
    /**
     * Instantiates a new liste itineraire.
     */
    /**
     * The code @link(String).
     */
    @Column(nullable = false, name = IValeurQualitative.ATTRIBUTE_JPA_CODE)
    @NaturalId
    private String code;

    /**
     *
     */
    public ListeACBB() {
        super();
    }

    /**
     * Instantiates a new valeur qualitative.
     *
     * @param value the value
     * @link(String) the value
     */
    public ListeACBB(final String code, final String value) {
        super();
        this.code = code;
        this.valeur = value;
    }

    /**
     * Gets the code.
     *
     * @return the code
     * @see org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getCode()
     */
    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     * @see org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     * @see org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getValeur()
     */
    @Override
    public String getValeur() {
        return valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     * @see org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#setValeur(java.lang.String)
     */
    @Override
    public void setValeur(final String valeur) {
        this.valeur = valeur;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.valeur);
        hash = 67 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public int compareTo(ListeACBB o) {
        if (o == null) {
            return -1;
        }
        if (this.code != null && this.valeur != null && this.code.compareTo(o.code) == 0) {
            return this.valeur.compareTo(o.valeur);
        } else if (this.code != null) {
            return this.code.compareTo(o.code);
        } else {
            return +1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ListeACBB other = (ListeACBB) obj;
        if (!Objects.equals(this.valeur, other.valeur)) {
            return false;
        }
        return Objects.equals(this.code, other.code);
    }

    public void setTypeListe(String typeListe) {
        this.typeListe = typeListe;
    }
}
