/*
 *
 */
package org.inra.ecoinfo.acbb.utils.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ExtractionParametersVO.
 */
public class ExtractionParametersVO implements Serializable {
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The name @link(String).
     */
    String name;

    /**
     * The value min @link(Boolean).
     */
    Boolean valueMin = false;

    /**
     * The value max @link(Boolean).
     */
    Boolean valueMax = false;

    /**
     * The target value @link(List<String>).
     */
    List<String> targetValue = new ArrayList();

    /**
     * The uncertainty @link(List<String>).
     */
    List<String> uncertainty = new ArrayList();

    /**
     * Instantiates a new extraction parameters vo.
     */
    public ExtractionParametersVO() {
        super();
    }

    /**
     * Instantiates a new extraction parameters vo.
     *
     * @param name the name
     */
    public ExtractionParametersVO(final String name) {
        super();
        this.name = name;

    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the target value.
     *
     * @return the target value
     */
    public List<String> getTargetValue() {
        return this.targetValue;
    }

    /**
     * Sets the target value.
     *
     * @param targetValue the new target value
     */
    public void setTargetValue(final List<String> targetValue) {
        this.targetValue = targetValue;
    }

    /**
     * Gets the uncertainty.
     *
     * @return the uncertainty
     */
    public List<String> getUncertainty() {
        return this.uncertainty;
    }

    /**
     * Sets the uncertainty.
     *
     * @param uncertainty the new uncertainty
     */
    public void setUncertainty(final List<String> uncertainty) {
        this.uncertainty = uncertainty;
    }

    /**
     * Gets the value max.
     *
     * @return the value max
     */
    public Boolean getValueMax() {
        return this.valueMax;
    }

    /**
     * Sets the value max.
     *
     * @param valueMax the new value max
     */
    public void setValueMax(final Boolean valueMax) {
        this.valueMax = valueMax;
    }

    /**
     * Gets the value min.
     *
     * @return the value min
     */
    public Boolean getValueMin() {
        return this.valueMin;
    }

    /**
     * Sets the value min.
     *
     * @param valueMin the new value min
     */
    public void setValueMin(final Boolean valueMin) {
        this.valueMin = valueMin;
    }

}
