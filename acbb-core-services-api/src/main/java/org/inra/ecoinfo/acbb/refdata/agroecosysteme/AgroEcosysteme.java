package org.inra.ecoinfo.acbb.refdata.agroecosysteme;

import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class AgroEcosysteme.
 */
@Entity
@Table(name = AgroEcosysteme.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = AgroEcosysteme.ID_JPA)
public class AgroEcosysteme extends Nodeable implements Comparable<INodeable>, Serializable {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "agroecosysteme_agr";
    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "agr_id";
    /**
     * The Constant ATTRIBUTE_JPA_NAME.
     */
    public static final String ATTRIBUTE_JPA_NAME = "nom";
    /**
     * The Constant ATTRIBUTE_JPA_DESCRIPTION.
     */
    public static final String ATTRIBUTE_JPA_DESCRIPTION = AgroEcosysteme.ATTRIBUTE_JPA_NAME;
    private static final long serialVersionUID = 1L;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = AgroEcosysteme.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The nom @link(String).
     */
    @Column(nullable = false, name = AgroEcosysteme.ATTRIBUTE_JPA_NAME)
    String nom;

    /**
     * The region @link(String).
     */
    @Column(nullable = true)
    String region;

    /**
     * The departement @link(String).
     */
    @Column(nullable = true)
    String departement;

    /**
     * The sites @link(List<SiteACBB>).
     */
    @OneToMany(mappedBy = "agroEcosysteme", cascade = ALL)
    List<SiteACBB> sites = new LinkedList();

    /**
     * Instantiates a new agro ecosysteme.
     */
    public AgroEcosysteme() {
        super();
    }

    /**
     * Instantiates a new agro ecosysteme.
     *
     * @param nom         the nom
     * @param region      the region
     * @param departement the departement
     */
    public AgroEcosysteme(final String nom, final String region, final String departement) {
        super();
        this.setName(nom);
        this.region = region;
        this.departement = departement;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(AgroEcosysteme) the o
     */
    @Override
    public int compareTo(final INodeable o) {
        return getCode().compareTo(o.getCode());
    }

    /**
     * Gets the departement.
     *
     * @return the departement
     */
    public String getDepartement() {
        return this.departement;
    }

    /**
     * Sets the departement.
     *
     * @param departement the new departement
     */
    public void setDepartement(final String departement) {
        this.departement = departement;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    @Override
    public String getName() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom the new nom
     */
    public void setName(final String nom) {
        this.nom = nom;
        setCode(Utils.createCodeFromString(nom));
    }

    /**
     * Gets the region.
     *
     * @return the region
     */
    public String getRegion() {
        return this.region;
    }

    /**
     * Sets the region.
     *
     * @param region the new region
     */
    public void setRegion(final String region) {
        this.region = region;
    }

    /**
     * Gets the sites.
     *
     * @return the sites
     */
    public List<SiteACBB> getSites() {
        return this.sites;
    }

    /**
     * Sets the sites.
     *
     * @param sitesACBB the new sites
     */
    public void setSites(final List<SiteACBB> sitesACBB) {
        this.sites = sitesACBB;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(getCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgroEcosysteme other = (AgroEcosysteme) obj;
        return Objects.equals(getCode(), other.getCode());
    }

    /**
     * @return
     */
    @Override
    public Class<? extends Nodeable> getNodeableType() {
        return AgroEcosysteme.class;
    }

}
