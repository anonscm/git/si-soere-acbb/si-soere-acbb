package org.inra.ecoinfo.acbb.dataset.itk.phytosanitaire.entity;

import org.inra.ecoinfo.acbb.dataset.itk.IMesureITK;
import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire.ProduitPhytosanitaire;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class TravailDuSol.
 */
@Entity
@Table(name = Phytosanitaire.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        Phytosanitaire.ATTRIBUTE_JPA_TYPE, Tempo.ID_JPA, Phytosanitaire.ATTRIBUTE_JPA_DATE,
        Phytosanitaire.ATTRIBUTE_JPA_RANG, SuiviParcelle.ID_JPA}),
        indexes = {
                @Index(name = "phyt_version_idx", columnList = VersionFile.ID_JPA),
                @Index(name = "phyt_tempo_idx", columnList = Tempo.ID_JPA),
                @Index(name = "phyt_date_idx", columnList = AbstractIntervention.ATTRIBUTE_JPA_DATE),
                @Index(name = "phyt_parcelle_idx", columnList = SuiviParcelle.ID_JPA),
                @Index(name = "phyt_produit_idx", columnList = ProduitPhytosanitaire.ID_JPA)})
@DiscriminatorValue(Phytosanitaire.PHYTOSANITAIRE_DESCRIMINATOR)
//@AttributeOverrides(value = { @AttributeOverride(column = @Column(name = Phytosanitaire.ID_JPA), name = "id") })
@PrimaryKeyJoinColumn(name = Phytosanitaire.ID_JPA, referencedColumnName = AbstractIntervention.VARIABLE_NAME_ID)
public class Phytosanitaire extends AbstractIntervention implements
        IMesureITK<Phytosanitaire, ValeurPhytosanitaire> {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "phytosanitaire_phyt";

    /**
     * The Constant TRAVAIL_DU_SOL @link(String).
     */
    public static final String PHYTOSANITAIRE_DESCRIMINATOR = "phytosanitaire";

    /**
     * The Constant ATTRIBUTE_JPA_RANG.
     */
    public static final String ATTRIBUTE_JPA_RANG = "rang";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The rang @link(int).
     */
    @Column(name = Phytosanitaire.ATTRIBUTE_JPA_RANG, nullable = false)
    int rang = 1;

    @OneToMany(mappedBy = "phytosanitaire", cascade = ALL)
    List<ValeurPhytosanitaire> valeursPhytosanitaire = new LinkedList();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProduitPhytosanitaire.ID_JPA, referencedColumnName = ProduitPhytosanitaire.ID_JPA, nullable = false)
    ProduitPhytosanitaire produitPhytosanitaire;

    /**
     *
     */
    public Phytosanitaire() {
        super();
        this.type = Phytosanitaire.PHYTOSANITAIRE_DESCRIMINATOR;
    }

    /**
     * @param versionFile
     * @param observation
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param periodeRealiseeNumber
     */
    public Phytosanitaire(final VersionFile versionFile, final String observation, final LocalDate date, final int versionTraitementRealiseeNumber, final int anneeRealiseeNumber, final int periodeRealiseeNumber) {
        super(versionFile, Phytosanitaire.PHYTOSANITAIRE_DESCRIMINATOR, observation, date,
                versionTraitementRealiseeNumber, anneeRealiseeNumber, periodeRealiseeNumber);
    }

    /**
     * @return the produitPhytosanitaire
     */
    public ProduitPhytosanitaire getProduitPhytosanitaire() {
        return this.produitPhytosanitaire;
    }

    /**
     * @param produitPhytosanitaire the produitPhytosanitaire to set
     */
    public void setProduitPhytosanitaire(ProduitPhytosanitaire produitPhytosanitaire) {
        this.produitPhytosanitaire = produitPhytosanitaire;
    }

    /**
     * Gets the rang.
     *
     * @return the rang
     */
    public int getRang() {
        return this.rang;
    }

    /**
     * Sets the rang.
     *
     * @param rang the new rang
     */
    public void setRang(final int rang) {
        this.rang = rang;
    }

    /**
     * @return
     */
    @Override
    public Phytosanitaire getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurPhytosanitaire> getValeurs() {
        return this.valeursPhytosanitaire;
    }

    /**
     * @return
     */
    public List<ValeurPhytosanitaire> getValeursPhytosanitaire() {
        return this.valeursPhytosanitaire;
    }

    /**
     * @param valeursPhytosanitaires
     */
    public void setValeursPhytosanitaire(
            final List<ValeurPhytosanitaire> valeursPhytosanitaires) {
        this.valeursPhytosanitaire = valeursPhytosanitaires;
    }

}
