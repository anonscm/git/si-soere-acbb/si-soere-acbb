/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassAerienSouterrain.MAV_CLASS_AERIEN_SOUTERRAIN)
public class MassVegClassAerienSouterrain extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_AERIEN_SOUTERRAIN = "mav_class_aerien_souterrain";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_aerein_souterrain_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
