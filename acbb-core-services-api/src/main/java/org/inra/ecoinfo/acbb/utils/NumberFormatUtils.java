/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author tcherniatinsky
 */
public class NumberFormatUtils {

    private static final Map<Locale, NumberFormat> NUMBER_FORMATS = new HashMap();

    /**
     * @param locale
     * @return
     */
    public static final NumberFormat getLocalInstance(Locale locale) {
        final NumberFormat numberFormat = NumberFormatUtils.NUMBER_FORMATS
                .computeIfAbsent(locale, k -> NumberFormat.getInstance(locale));
        numberFormat.setMaximumFractionDigits(3);
        return numberFormat;
    }

    /**
     * @return
     */
    public static final NumberFormat getLocalInstance() {
        Locale locale = getLocaleForUser();
        final NumberFormat numberFormat = NumberFormatUtils.NUMBER_FORMATS
                .computeIfAbsent(
                        locale,
                        k -> NumberFormat.getInstance(locale));
        numberFormat.setMaximumFractionDigits(3);
        return numberFormat;
    }

    private static Locale getLocaleForUser() {
        Locale locale = ACBBUtils.getLocalizationManager()
                .map(localisationmanager -> localisationmanager.getUserLocale())
                .map(userLocale -> userLocale.getLocale())
                .orElse(Locale.ENGLISH);
        return locale;
    }

    /**
     * @param floatString
     * @return
     * @throws NumberFormatException
     */
    public final static Float parseFloat(String floatString) throws NumberFormatException {
        return parseValue(floatString).floatValue();
    }

    /**
     * @param intString
     * @return
     * @throws NumberFormatException
     */
    public final static Integer parseInt(String intString) throws NumberFormatException {
        return parseValue(intString).intValue();
    }

    /**
     * @param doubleString
     * @return
     * @throws NumberFormatException
     */
    public final static Double parseDouble(String doubleString) throws NumberFormatException {
        return parseValue(doubleString).doubleValue();
    }

    /**
     * @param byteString
     * @return
     * @throws NumberFormatException
     */
    public final static Byte parseByte(String byteString) throws NumberFormatException {
        return parseValue(byteString).byteValue();
    }

    /**
     * @param longString
     * @return
     * @throws NumberFormatException
     */
    public final static Long parseLong(String longString) throws NumberFormatException {
        return parseValue(longString).longValue();
    }

    /**
     * @param floatString
     * @param locale
     * @return
     * @throws NumberFormatException
     */
    public final static Float parseFloat(String floatString, Locale locale) throws NumberFormatException {
        return parseValue(locale, floatString).floatValue();
    }

    /**
     * @param doubleString
     * @param locale
     * @return
     * @throws NumberFormatException
     */
    public final static Double parseDouble(String doubleString, Locale locale) throws NumberFormatException {
        return parseValue(locale, doubleString).doubleValue();
    }

    /**
     * @param byteString
     * @param locale
     * @return
     * @throws NumberFormatException
     */
    public final static Byte parseByte(String byteString, Locale locale) throws NumberFormatException {
        return parseValue(locale, byteString).byteValue();
    }

    /**
     * @param longString
     * @param locale
     * @return
     * @throws NumberFormatException
     */
    public final static Long parseLong(String longString, Locale locale) throws NumberFormatException {
        return parseValue(locale, longString).longValue();
    }

    private static Number parseValue(Locale locale, String StringValue) throws NumberFormatException {
        try {
            return getLocalInstance(locale).parse(StringValue);
        } catch (Exception e) {
            try {
                return getLocalInstance(Locale.FRANCE.equals(locale) ? Locale.ENGLISH : Locale.FRANCE).parse(StringValue);
            } catch (ParseException ex) {
                throw new NumberFormatException(ex.getLocalizedMessage());
            }
        }
    }

    private static Number parseValue(String StringValue) throws NumberFormatException {
        Locale locale = getLocaleForUser();
        try {
            return parseValue(locale, StringValue);
        } catch (Exception e) {
            try {
                return getLocalInstance(Locale.FRANCE.equals(locale) ? Locale.ENGLISH : Locale.FRANCE).parse(StringValue);
            } catch (ParseException ex) {
                throw new NumberFormatException(ex.getLocalizedMessage());
            }
        }
    }

    /**
     * @param intString
     * @param locale
     * @return
     * @throws NumberFormatException
     */
    public final Integer parseInt(String intString, Locale locale) throws NumberFormatException {
        return parseValue(locale, intString).intValue();
    }
}
