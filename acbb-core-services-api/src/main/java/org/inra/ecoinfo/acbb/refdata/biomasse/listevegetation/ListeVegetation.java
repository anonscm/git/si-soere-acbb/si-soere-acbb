/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation;

import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;

import javax.persistence.*;

/**
 * The Class ListeVegetation.
 */

/**
 * @author koyao
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {ListeACBB.ATTRIBUTE_JPA_VALUE, ListeACBB.ATTRIBUTE_JPA_CODE, ListeACBB.ATTRIBUTE_JPA_TYPE_LIST})
},
        indexes = {
                @Index(name = "vq_code_idx", columnList = ListeACBB.ATTRIBUTE_JPA_CODE),
                @Index(name = "vq_code_idx", columnList = ListeACBB.ATTRIBUTE_JPA_CODE + "," + ListeACBB.ATTRIBUTE_JPA_VALEUR)
        }
)
@DiscriminatorValue(ListeVegetation.LISTE_VEGETATION_TYPE)
public class ListeVegetation extends ListeACBB {

    /**
     *
     */
    public static final String LISTE_VEGETATION_TYPE = "liste_vegetation";
    /**
     * The Constant SEPARATOR_CASES.
     */
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public ListeVegetation() {
        super();
    }

    /**
     * Instantiates a new listeItineraire.
     *
     * @param nom
     * @param value
     * @link(String) the nom
     * @link(String) the value
     */
    public ListeVegetation(final String nom, final String value) {
        super(nom, value);
        this.typeListe = ListeVegetation.LISTE_VEGETATION_TYPE;
    }
}
