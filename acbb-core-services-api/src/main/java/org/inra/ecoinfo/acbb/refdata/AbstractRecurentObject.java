package org.inra.ecoinfo.acbb.refdata;

import org.inra.ecoinfo.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class AbstractRecurentObject.
 *
 * @param <T> the generic type
 */
@MappedSuperclass
public abstract class AbstractRecurentObject<T extends AbstractRecurentObject<T>> implements Serializable, Cloneable, Comparable<T> {

    /**
     * The Constant CST_PROPERTY_RECURENT_SEPARATOR @link(String).
     */
    public static final String CST_PROPERTY_RECURENT_SEPARATOR = Utils.SEPARATOR_URL;
    /**
     * The Constant CST_PROPERTY_RECURENT_SEPARATOR_IN_FILE_NAME @link(String).
     */
    public static final String CST_PROPERTY_RECURENT_SEPARATOR_IN_SITE_NAME = "-";
    /**
     * The Constant CST_PROPERTY_RECURENT_SEPARATOR_IN_FILE_NAME @link(String).
     */
    public static final String CST_PROPERTY_RECURENT_SEPARATOR_IN_FILE_NAME = "_";
    /**
     * The Constant RECURENT_NAME_CODE_ID @link(String).
     */
    public static final String RECURENT_NAME_CODE_ID = "code";
    /**
     * The Constant RECURENT_NAME_COLUMN_PARENT_ID @link(String).
     */
    public static final String RECURENT_NAME_COLUMN_PARENT_ID = "parent";
    /**
     * The Constant RECURENT_NAME_ID @link(String).
     */
    public static final String RECURENT_NAME_ID = "id";
    /**
     * The Constant RECURENT_NAME_PARENT_ID @link(String).
     */
    public static final String RECURENT_NAME_PARENT_ID = "parent_id";
    /**
     * The Constant ENTITY_FIELD_NAME @link(String).
     */
    public static final String ENTITY_FIELD_NAME = "nom";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;
    /**
     * The code @link(String).
     */
    @Column(name = AbstractRecurentObject.RECURENT_NAME_CODE_ID, nullable = false, unique = true)
    protected String code = "";
    /**
     * The nom @link(String).
     */
    @Column(name = AbstractRecurentObject.ENTITY_FIELD_NAME, nullable = false, unique = false)
    protected String nom = "";
    /**
     * The parent @link(T).
     */
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = true)
    @JoinColumn(name = AbstractRecurentObject.RECURENT_NAME_PARENT_ID, referencedColumnName = AbstractRecurentObject.RECURENT_NAME_ID, nullable = true)
    protected T parent;
    /**
     * The children @link(List<T>).
     */
    @OneToMany(cascade = {MERGE, PERSIST, REFRESH}, mappedBy = "parent")
    @OrderBy("nom")
    private List<T> children = new LinkedList<>();

    /**
     * Instantiates a new abstract recurent object.
     */
    protected AbstractRecurentObject() {
        super();
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @link(AbstractRecurentObject) the o
     */
    @Override
    @SuppressWarnings({})
    public int compareTo(final T o) {
        int compare = -1;
        if (equals(o)) {
            compare = 0;
        } else if (o != null && getPath().compareTo(o.getPath()) != 0) {
            return getCode().compareTo(o.getCode());
        }
        return compare;
    }

    /**
     * Equals.
     *
     * @param obj the obj
     * @return true, if successful
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if (this == obj) {
            equals = true;
        } else if (obj == null) {
            equals = false;
        } else if (getClass() != obj.getClass()) {
            equals = false;
        } else if (obj instanceof AbstractRecurentObject && code != null && ((AbstractRecurentObject) obj).code != null && code.equals(((AbstractRecurentObject) obj).code)) {
            equals = true;
        }
        return equals;
    }

    /**
     * Gets the children.
     *
     * @return the children
     */
    public List<T> getChildren() {
        return children;
    }

    /**
     * Sets the children.
     *
     * @param children the new children
     */
    @SuppressWarnings("unchecked")
    public void setChildren(final List<T> children) {
        for (final T t : children) {
            t.setParent((T) this);
        }
        this.children = children;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the display path.
     *
     * @return the display path
     */
    public String getDisplayPath() {
        return getParent() != null ? new StringBuilder(parent.getDisplayPath()).append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR).append(nom).toString() : nom;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom the new nom
     */
    public void setNom(final String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(getDisplayPath());
    }

    /**
     * Gets the parent.
     *
     * @return the parent
     */
    public T getParent() {
        return parent;
    }

    /**
     * Sets the parent.
     *
     * @param parent the new parent
     */
    @SuppressWarnings("unchecked")
    public void setParent(final T parent) {
        if (parent == null) {
            return;
        }
        if (!parent.getChildren().contains(this)) {
            parent.getChildren().add((T) this);
        }
        this.parent = parent;
        setPath();
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
        if (code == null) {
            setPath();
        }
        return code;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (code == null ? 0 : code.hashCode());
        return result;
    }

    /**
     * Sets the path.
     */
    public void setPath() {
        code = Utils.createCodeFromString(getDisplayPath());
    }
}