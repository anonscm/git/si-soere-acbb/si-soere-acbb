/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;

/**
 * The Interface IRequestPropertiesAI.
 */
public interface IRequestPropertiesBiomassProduction extends IRequestPropertiesBiomasse {

}
