package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;

/**
 * The Class ValeurSemis.
 * <p>
 * record the values of an ai LINE
 */
@Entity
@Table(name = ValeurAI.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, AI.ID_JPA}),
        indexes = {
                @Index(name = "vai_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vai_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vai_ai_idx", columnList = AI.ID_JPA)})
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurAI.ID_JPA), name = "id")})
public class ValeurAI extends ValeurIntervention {

    /**
     * The Constant ATTRIBUTE_JPA_AI.
     */
    static final String ATTRIBUTE_JPA_AI = "ai";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "valeur_autre_intervention_vai";

    /**
     * The Constant ID_JPA.
     */
    static final String ID_JPA = "vai_id";

    /**
     * The Constant AI_NAME.
     */
    static final String AI_NAME = "ai";

    /**
     * The ai.
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = AI.ID_JPA, referencedColumnName = AbstractIntervention.ID_JPA, nullable = false)
    AI ai;

    /**
     * Instantiates a new valeur ai.
     */
    public ValeurAI() {
        super();
    }

    /**
     * Instantiates a new valeur ai.
     *
     * @param valeur
     * @param realNode
     * @param ai
     * @link
     * @link
     * @link{AI the ai
     */
    public ValeurAI(final float valeur, final RealNode realNode, final AI ai) {
        super(valeur, realNode);
        this.ai = ai;
    }

    /**
     * Instantiates a new valeur ai.
     *
     * @param valeur
     * @param realNode
     * @param ai
     * @link
     * @link
     * @link{AI the ai
     */
    public ValeurAI(final IValeurQualitative valeur, final RealNode realNode, final AI ai) {
        super(valeur, realNode);
        this.ai = ai;
    }

    /**
     * Gets the ai.
     *
     * @return the recFau
     */
    public AI getAI() {
        return this.ai;
    }

    /**
     * Sets the ai.
     *
     * @param ai the new ai
     */
    public void setAI(AI ai) {
        this.ai = ai;
    }

}
