/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fauchenonexportee.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class AutNature.
 */
@Entity
@DiscriminatorValue(FneType.FNE_TYPE)
public class FneType extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "fne_type_id";

    /**
     * The Constant AUT_NATURE.
     */
    public static final String FNE_TYPE = "fauche_non_exportee_type";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
