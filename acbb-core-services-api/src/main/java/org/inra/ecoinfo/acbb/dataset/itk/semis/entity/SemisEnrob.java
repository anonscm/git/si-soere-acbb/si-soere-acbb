/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class SemisEnrob.
 */
@Entity
@DiscriminatorValue(SemisEnrob.SEMIS_ENROB)
public class SemisEnrob extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_enrob_id";

    /**
     * The Constant SEMIS_ENROB.
     */
    public static final String SEMIS_ENROB = "sem_enrob";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
