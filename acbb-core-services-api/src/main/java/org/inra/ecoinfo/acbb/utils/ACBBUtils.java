/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.utils;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.DateUtil;

import java.util.Collection;
import java.util.Optional;

/**
 * @author tcherniatinsky
 */
public class ACBBUtils {

    /**
     *
     */
    public static final int CST_INVALID_BAD_MEASURE = -9999;
    /**
     *
     */
    public static final String PROPERTY_CST_BAD_VALUE = "NV";
    /**
     *
     */
    public static final int CST_INVALID_EMPTY_MEASURE = -99999;
    /**
     *
     */
    public static final String PROPERTY_CST_EMPTY_MEASURE = "-99999";

    /**
     *
     */
    public static final String PROPERTY_BINDED_MESSAGE = "PROPERTY_BINDED_MESSAGE";

    /**
     *
     */
    public static final String PROPERTY_UNKNOWN_INTERVENTION = "PROPERTY_UNKNOWN_INTERVENTION";

    /**
     *
     */
    public static final String BUNDLE_PATH = "org.inra.ecoinfo.acbb.utils.messages";
    /**
     * The Constant PROPERTY_CST_NOT_AVALAIBALE.
     */
    public static final String PROPERTY_CST_NOT_AVALAIBALE = "NA";
    /**
     *
     */
    public static IDatasetConfiguration datasetConfiguration;
    /**
     *
     */
    public static ILocalizationManager localizationManager;

    private static <T extends Nodeable> T getNodeableByType(Dataset dataset, Class<T> type) {
        final RealNode parcelleNode = dataset.getRealNode().getNodeByNodeableTypeResource(type);
        return parcelleNode == null ? null : (T) parcelleNode.getNodeable();
    }

    /**
     * @param dataset
     * @return
     */
    public static AgroEcosysteme getAgroEcosystemeFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, AgroEcosysteme.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static VariableACBB getVariableFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, VariableACBB.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static Theme getThemeFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, Theme.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static SiteACBB getSiteFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, SiteACBB.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static Parcelle getParcelleFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, Parcelle.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static DataType getDatatypeFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, DataType.class);
    }

    /**
     * @param number
     * @return
     */
    public static String getNAIfBadValueOrNVIfEmptyValue(Float number) {
        if (number == null || CST_INVALID_EMPTY_MEASURE == number.intValue()) {
            return ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE;
        } else if (CST_INVALID_BAD_MEASURE == number.intValue()) {
            return PROPERTY_CST_BAD_VALUE;
        }
        return NumberFormatUtils.getLocalInstance().format(number);
    }

    /**
     * @param number
     * @return
     */
    public static String getNAIfBadValueOrNVIfEmptyValue(Integer number) {
        if (number == null || CST_INVALID_EMPTY_MEASURE == number) {
            return ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE;
        } else if (CST_INVALID_BAD_MEASURE == number) {
            return PROPERTY_CST_BAD_VALUE;
        }
        return number.toString();
    }

    /**
     * @param bindedsITK
     * @return
     */
    public static String getBindedsMessages(Collection<AbstractIntervention> bindedsITK) {
        java.lang.StringBuilder messages = new java.lang.StringBuilder();
        for (AbstractIntervention intervention : bindedsITK) {
            messages.append(String.format(StringUtils.NEW_LINE)).append(getBindedMessages(intervention));
        }

        messages.append(String.format(StringUtils.NEW_LINE));
        return messages.toString();
    }

    /**
     * @param intervention
     * @return
     */
    public static String getBindedMessages(AbstractIntervention intervention) {
        if (intervention == null) {
            return getLocalizationManager().
                    map(localizationManager -> localizationManager.getMessage(BUNDLE_PATH, PROPERTY_UNKNOWN_INTERVENTION))
                    .orElse("");
        }
        long version = intervention.getVersion().getVersionNumber();
        String fileName = intervention.getVersion().getDataset().buildDownloadFilename(datasetConfiguration);
        String date = DateUtil.getUTCDateTextFromLocalDateTime(intervention.getDate(), DateUtil.DD_MM_YYYY);
        return String.format(
                getLocalizationManager().
                        map(localizationManager -> localizationManager.getMessage(BUNDLE_PATH, PROPERTY_BINDED_MESSAGE))
                        .orElse("%s,%s,%s"),
                version, fileName, date);
    }

    /**
     * @return
     */
    public static IDatasetConfiguration getDatasetConfiguration() {
        if (datasetConfiguration == null) {
            datasetConfiguration = ApplicationContextHolder.getContext().getBean(IDatasetConfiguration.class);
        }
        return datasetConfiguration;
    }

    /**
     * @return
     */
    public static Optional<ILocalizationManager> getLocalizationManager() {
        if (localizationManager == null) {
            localizationManager = ApplicationContextHolder.getContext().getBean(ILocalizationManager.class);
        }
        return Optional.ofNullable(localizationManager);
    }

}
