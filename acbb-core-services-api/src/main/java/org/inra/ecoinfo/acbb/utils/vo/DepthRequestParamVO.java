/*
 *
 */
package org.inra.ecoinfo.acbb.utils.vo;

import org.inra.ecoinfo.localization.ILocalizationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.ValueHolder;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.xml.bind.ValidationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.*;

/**
 * The Class DepthRequestParamVO.
 */
public class DepthRequestParamVO implements Serializable {

    /**
     * The Constant PROPERTY_MSG_RANGE_DEPTHS_SELECTED @link(String).
     */
    public static final String PROPERTY_MSG_RANGE_DEPTHS_SELECTED = "PROPERTY_MSG_RANGE_DEPTHS_SELECTED";
    /**
     * The Constant PROPERTY_MSG_CHOICE_DEPTHS_SELECTED @link(String).
     */
    public static final String PROPERTY_MSG_CHOICE_DEPTHS_SELECTED = "PROPERTY_MSG_CHOICE_DEPTHS_SELECTED";
    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.utils.vo.messages";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant PATTERN_STRING_DEPTH_SUMMARY @link(String).
     */
    static final String PATTERN_STRING_DEPTH_SUMMARY = "   %s: %d";
    /**
     * The Constant PROPERTY_MSG_MAX @link(String).
     */
    static final String PROPERTY_MSG_MAX = "PROPERTY_CST_MAXIMUM";

    /**
     * The Constant PROPERTY_MSG_MIN @link(String).
     */
    static final String PROPERTY_MSG_MIN = "PROPERTY_CST_MINIMUM";

    /**
     * The Constant PROPERTY_MSG_BAD_DEPTH @link(String).
     */
    static final String PROPERTY_MSG_BAD_DEPTH = "PROPERTY_MSG_BAD_DEPTH";

    /**
     * The Constant PROPERTY_MSG_BAD_RATE @link(String).
     */
    static final String PROPERTY_MSG_BAD_RATE = "PROPERTY_MSG_BAD_RATE";
    /**
     * The Constant PROPERTY_MSG_NO_CHOICE_DEPTHS_SELECTED @link(String).
     */
    static final String PROPERTY_MSG_NO_CHOICE_DEPTHS_SELECTED = "PROPERTY_MSG_NO_CHOICE_DEPTHS_SELECTED";
    private static final Logger LOGGER = LoggerFactory.getLogger(DepthRequestParamVO.class);
    /**
     * The availables depth @link(SortedSet<ProfondeurJSF>).
     */
    final SortedSet<Integer> availablesDepth = new TreeSet();
    /**
     * The selecteds depths @link(SortedSet<Integer>).
     */
    final SortedSet<Integer> selectedsDepths = new TreeSet();
    /**
     * The localization manager @link(ILocalizationManager).
     */
    final ILocalizationManager localizationManager;
    /**
     * The depth min @link(Integer).
     */
    Integer depthMin = 0;
    /**
     * The depth max @link(Integer).
     */
    Integer depthMax = 0;
    List<String> choicesDepth = Arrays.asList(PROPERTY_MSG_RANGE_DEPTHS_SELECTED, PROPERTY_MSG_CHOICE_DEPTHS_SELECTED);
    /**
     * The choice depth @link(Boolean).
     */
    String choiceDepth = PROPERTY_MSG_RANGE_DEPTHS_SELECTED;
    /**
     * The valid min m ax depht @link(Boolean).
     */
    Boolean validMinMAxDepht = true;
    /**
     * The selected depth @link(int).
     */
    int selectedDepth;

    /**
     * Instantiates a new depth request param vo.
     *
     * @param localizationManager the localization manager
     */
    public DepthRequestParamVO(final ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    /**
     * @return
     */
    public List<String> getChoicesDepth() {
        return choicesDepth;
    }

    /**
     * @param choicesDepth
     */
    public void setChoicesDepth(List<String> choicesDepth) {
        this.choicesDepth = choicesDepth;
    }

    /**
     * Adds the profondeur.
     *
     * @param profondeur the profondeur
     */
    public void addProfondeur(final Integer profondeur) {
        this.availablesDepth.add(profondeur);
    }

    /**
     * Builds the summary.
     *
     * @param printStream the print stream
     */
    public void buildSummary(final PrintStream printStream) {

        if (this.getChoiceDepth()) {
            printStream.println(this.localizationManager.getMessage(
                    DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                    DepthRequestParamVO.PROPERTY_MSG_CHOICE_DEPTHS_SELECTED));
            for (final Integer depth : this.selectedsDepths) {
                printStream.print(String.format("%d%s", depth,
                        this.selectedsDepths.last().equals(depth) ? org.apache.commons.lang.StringUtils.EMPTY : ", "));
            }
        } else {
            printStream.println(this.localizationManager.getMessage(
                    DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                    DepthRequestParamVO.PROPERTY_MSG_RANGE_DEPTHS_SELECTED));
            printStream.println(String.format(DepthRequestParamVO.PATTERN_STRING_DEPTH_SUMMARY,
                    this.localizationManager.getMessage(DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_MIN), this.getDepthMin()));
            printStream.println(String.format(DepthRequestParamVO.PATTERN_STRING_DEPTH_SUMMARY,
                    this.localizationManager.getMessage(DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_MAX), this.getDepthMax()));
        }
        printStream.println();

    }

    /**
     * Gets the availables depth.
     *
     * @return the availables depth
     */
    public SortedSet<Integer> getAvailablesDepth() {
        return this.availablesDepth;
    }

    /**
     * Gets the availables depth list.
     *
     * @return the availables depth list
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Integer> getAvailablesDepthList() {
        return new LinkedList(this.availablesDepth);
    }

    /**
     * Gets the choice depth.
     *
     * @return the choice depth
     */
    public boolean getChoiceDepth() {
        return this.choiceDepth.equals(PROPERTY_MSG_CHOICE_DEPTHS_SELECTED);
    }

    /**
     * Sets the choice depth.
     *
     * @param choiceDepth
     */
    public void setChoiceDepth(final boolean choiceDepth) {
        this.choiceDepth = choiceDepth ? PROPERTY_MSG_CHOICE_DEPTHS_SELECTED : PROPERTY_MSG_RANGE_DEPTHS_SELECTED;
    }

    /**
     * Gets the depth max.
     *
     * @return the depth max
     */
    public Integer getDepthMax() {
        return this.depthMax;
    }

    /**
     * Sets the depth max.
     *
     * @param depthMax the new depth max
     */
    public void setDepthMax(final Integer depthMax) {
        this.depthMax = depthMax;
    }

    /**
     * Gets the depth min.
     *
     * @return the depth min
     */
    public Integer getDepthMin() {
        return this.depthMin;
    }

    /**
     * Sets the depth min.
     *
     * @param depthMin the new depth min
     */
    public void setDepthMin(final Integer depthMin) {
        this.depthMin = depthMin;
    }

    /**
     * Gets the checks if is depth step valid.
     *
     * @return the checks if is depth step valid
     */
    public boolean getIsDepthStepValid() {
        return this.choiceDepth.equals(PROPERTY_MSG_RANGE_DEPTHS_SELECTED) && this.validMinMAxDepht
                || this.choiceDepth.equals(PROPERTY_MSG_CHOICE_DEPTHS_SELECTED) && !this.selectedsDepths.isEmpty();
    }

    /**
     * Gets the selected depth.
     *
     * @return the selected depth
     */
    public int getSelectedDepth() {
        return this.selectedDepth;
    }

    /**
     * Sets the selected depth.
     *
     * @param selectedDepth the new selected depth
     */
    public void setSelectedDepth(final int selectedDepth) {
        this.selectedDepth = selectedDepth;
    }

    /**
     * Gets the selecteds depths.
     *
     * @return the selecteds depths
     */
    public SortedSet<Integer> getSelectedsDepths() {
        return this.selectedsDepths;
    }

    /**
     * Gets the summary html.
     *
     * @return the summary html
     */
    public String getSummaryHTML() {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintStream printStream = new PrintStream(bos);
        if (this.getChoiceDepth()) {
            if (this.selectedsDepths.isEmpty()) {
                printStream
                        .println(String
                                .format("<div style='display:block; color:red; font-size:14px; font-weight:bold'>%s</div>%n<br />",
                                        this.localizationManager
                                                .getMessage(
                                                        DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                                                        DepthRequestParamVO.PROPERTY_MSG_NO_CHOICE_DEPTHS_SELECTED)));
            } else {
                printStream.println(String.format(
                        "<div style='display:block'>%s</div>%n<br /><ul>", this.localizationManager
                                .getMessage(DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                                        DepthRequestParamVO.PROPERTY_MSG_CHOICE_DEPTHS_SELECTED)));
                for (final Integer profondeur : this.selectedsDepths) {
                    printStream.println(String.format("<li>%d</li>%n", profondeur));
                }
                printStream.println("</ul>\n");
            }
        } else {
            printStream.println(String.format("<div style='display:block'>%s:</div>",
                    this.localizationManager.getMessage(DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_RANGE_DEPTHS_SELECTED)));
            printStream.println(String.format(
                    "<ul ><li>".concat(DepthRequestParamVO.PATTERN_STRING_DEPTH_SUMMARY).concat(
                            "</li>"), this.localizationManager.getMessage(
                            DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_MIN), this.getDepthMin()));
            printStream.println(String.format(
                    "<li>".concat(DepthRequestParamVO.PATTERN_STRING_DEPTH_SUMMARY).concat(
                            "</li></ul>"), this.localizationManager.getMessage(
                            DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_MAX), this.getDepthMax()));
        }
        printStream.println();
        try {
            bos.flush();
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return bos.toString();
    }

    /**
     * Gets the valid min m ax depht.
     *
     * @return the valid min m ax depht
     */
    public Boolean getValidMinMAxDepht() {
        return this.validMinMAxDepht;
    }

    /**
     * Sets the valid min m ax depht.
     *
     * @param validMinMAxDepht the new valid min m ax depht
     */
    public void setValidMinMAxDepht(final Boolean validMinMAxDepht) {
        this.validMinMAxDepht = validMinMAxDepht;
    }

    /**
     * Checks if is valid depht.
     *
     * @return the boolean
     */
    public Boolean isValidDepht() {
        return this.choiceDepth.equals(PROPERTY_MSG_RANGE_DEPTHS_SELECTED) && !this.selectedsDepths.isEmpty() || this.validMinMAxDepht;
    }

    /**
     * Validate depth.
     *
     * @param context   the context
     * @param component the component
     * @param value     the value
     * @throws ValidationException the validation exception
     */
    public void validateDepth(final FacesContext context, final UIComponent component,
                              final Object value) throws ValidationException {
        int localDepthMin = 0;
        int localDepthMax = 0;
        final String depthMinString = ((ValueHolder) component.getParent().findComponent("depthMin"))
                .getValue().toString();
        final String depthMaxString = ((ValueHolder) component.getParent().findComponent("depthMax"))
                .getValue().toString();

        try {
            localDepthMin = Integer.parseInt(depthMinString);
            localDepthMax = Integer.parseInt(depthMaxString);
        } catch (final NumberFormatException e) {
            ((EditableValueHolder) component).setValid(false);
            this.validMinMAxDepht = false;
            context.addMessage(
                    component.getClientId(context),
                    new FacesMessage(this.localizationManager.getMessage(
                            DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_BAD_DEPTH)));
        }
        if (localDepthMin > localDepthMax) {
            ((EditableValueHolder) component).setValid(false);
            this.validMinMAxDepht = false;
            context.addMessage(
                    component.getClientId(context),
                    new FacesMessage(this.localizationManager.getMessage(
                            DepthRequestParamVO.BUNDLE_SOURCE_PATH,
                            DepthRequestParamVO.PROPERTY_MSG_BAD_RATE)));
        } else {
            this.validMinMAxDepht = true;
            ((EditableValueHolder) component).setValid(true);
        }

    }

    /**
     * @param event
     */
    public void selectDepthes(ValueChangeEvent event) {
        String[] depthes = (String[]) event.getNewValue();
        selectedsDepths.clear();
        Arrays.stream(depthes).forEach(depth -> selectedsDepths.add(Integer.parseInt(depth)));
    }

}
