/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.lai.entity;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(LaiNature.LAI_NATURE)
public class LaiNature extends ListeVegetation {

    /**
     *
     */
    public static final String LAI_NATURE = "lai_nature";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
