package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.Objects;

/**
 * The Class SemisEspece.
 */
@Entity
@DiscriminatorValue(SemisVariete.SEMIS_VARIETE)
public class SemisVariete extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sem_var";

    /**
     * The Constant SEMIS_VARIETE.
     */
    public static final String SEMIS_VARIETE = "sem_variete";

    /**
     * The Constant serialVersionUID <long>.
     */
    private static final long serialVersionUID = 1L;
    @Transient
    SemisEspece espece;

    /**
     * @return
     */
    public SemisEspece getEspece() {
        return this.espece;
    }

    /**
     * @param espece
     */
    public void setEspece(SemisEspece espece) {
        this.espece = espece;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(super.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
