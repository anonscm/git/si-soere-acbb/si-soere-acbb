package org.inra.ecoinfo.acbb.refdata.suivivariable;

import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Class SuiviVariable.
 */
@Entity
@Table(name = SuiviVariable.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        DatatypeVariableUniteACBB.ID_JPA, SuiviParcelle.ID_JPA}))
public class SuiviVariable implements Serializable {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "suivi_variable_sva";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sva_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = SuiviVariable.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = SuiviParcelle.ID_JPA, referencedColumnName = SuiviParcelle.ID_JPA, nullable = false)
    @NaturalId
    SuiviParcelle suiviParcelle;

    /**
     * The datatype unite variable @link(DatatypeVariableUniteACBB).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = DatatypeVariableUniteACBB.ID_JPA, referencedColumnName = DatatypeVariableUniteACBB.ID_JPA, nullable = false)
    @NaturalId
    DatatypeVariableUniteACBB datatypeVariableUnite;

    /**
     * The date debut @link(Date).
     */
    @Column(name = "sva_date_debut", nullable = false)
    LocalDate dateDebut;

    /**
     * The date fin @link(Date).
     */
    @Column(name = "sva_date_fin")
    LocalDate dateFin;

    /**
     * Instantiates a new suivi variable.
     */
    public SuiviVariable() {
        super();
    }

    /**
     * Gets the datatype unite variable.
     *
     * @return the datatype unite variable
     */
    public DatatypeVariableUniteACBB getdatatypeVariableUnite() {
        return this.datatypeVariableUnite;
    }

    /**
     * Gets the date debut.
     *
     * @return the date debut
     */
    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    /**
     * Sets the date debut.
     *
     * @param dateDebut the new date debut
     */
    public void setDateDebut(final LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets the date fin.
     *
     * @return the date fin
     */
    public LocalDate getDateFin() {
        return this.dateFin;
    }

    /**
     * Sets the date fin.
     *
     * @param dateFin the new date fin
     */
    public void setDateFin(final LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle
     */
    public void setSuiviParcelle(final SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Sets the datatype unite variable.
     *
     * @param datatypeVariableUniteACBB the new datatype unite variable
     */
    public void setdatatypeVariableUnite(
            final DatatypeVariableUniteACBB datatypeVariableUniteACBB) {
        this.datatypeVariableUnite = datatypeVariableUniteACBB;
    }

}
