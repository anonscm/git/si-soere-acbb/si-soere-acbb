/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse;

import org.inra.ecoinfo.acbb.extraction.jsf.ITreatmentManager;
import org.inra.ecoinfo.acbb.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

/**
 * @author vkoyao
 */

/**
 * The Interface IIKDatasetManager.
 */
public interface IBiomasseDatasetManager extends ITreatmentManager, IVariableManager<TraitementProgramme> {
}
