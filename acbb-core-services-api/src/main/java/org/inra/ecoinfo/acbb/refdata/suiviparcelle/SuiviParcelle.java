/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.suiviparcelle;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * entity that store the link between {@link TraitementProgramme} and a
 * {@link Parcelle} The Class SuiviParcelle.
 */
@Entity
@Table(name = SuiviParcelle.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        Parcelle.ID_JPA, TraitementProgramme.ID_JPA,
        SuiviParcelle.NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT}),
        indexes = {
                @Index(name = "spa_par_idx", columnList = Parcelle.ID_JPA)
                ,
                @Index(name = "spa_tra_idx", columnList = TraitementProgramme.ID_JPA)
                ,
                @Index(name = "spa_date_idx", columnList = SuiviParcelle.NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT)
                ,
                @Index(name = "spa_date_par_idx", columnList = SuiviParcelle.NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT + "," + Parcelle.ID_JPA)
                ,
                @Index(name = "spa_date_par_tra_idx", columnList = SuiviParcelle.NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT + "," + Parcelle.ID_JPA + "," + TraitementProgramme.ID_JPA)})
public class SuiviParcelle implements Serializable, Comparable<SuiviParcelle> {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "spa_id";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "suivi_parcelle_spa";

    /**
     * The Constant NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT.
     */
    public static final String NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT = "dateDebutTraitement";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id {@link Long}.
     */
    @Id
    @Column(name = SuiviParcelle.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The plot {@link Parcelle}.
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Parcelle.ID_JPA, referencedColumnName = Parcelle.ID_JPA, nullable = false)
    @NaturalId
    Parcelle parcelle;

    /**
     * The start date of the traitement on the plot{@link Date}.
     */
    @Column(nullable = false, name = SuiviParcelle.NAME_ENTITY_FIELD_NAME_DATE_DEBUT_TRAITEMENT)
    LocalDate dateDebutTraitement;

    /**
     * The experimental treatement {@link TraitementProgramme}.
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = TraitementProgramme.ID_JPA, referencedColumnName = TraitementProgramme.ID_JPA, nullable = false)
    @NaturalId
    TraitementProgramme traitement;

    /**
     * Instantiates a new suivi parcelle.
     */
    public SuiviParcelle() {
        super();
    }

    /**
     * Instantiates a new suivi parcelle.
     *
     * @param traitementProgramme
     * @param parcelle
     * @param dateDebutTraitment
     * @link(TraitementProgramme) the traitement programme
     * @link(Parcelle) the parcelle
     * @link(Date) the date debut traitment {@link TraitementProgramme} the traitement programme
     * {@link Parcelle} the parcelle {@link Date} the start of the treatment on the plot
     */
    public SuiviParcelle(final TraitementProgramme traitementProgramme, final Parcelle parcelle,
                         final LocalDate dateDebutTraitment) {
        super();
        this.traitement = traitementProgramme;
        this.parcelle = parcelle;
        this.setDateDebutTraitement(dateDebutTraitment);
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int
     * @link(SuiviParcelle) the o
     * @link(SuiviParcelle) the o
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final SuiviParcelle o) {
        if (o == null) {
            return -1;
        }
        final int compareParcelle = this.getParcelle().compareTo(o.getParcelle());
        if (compareParcelle != 0) {
            return this.getParcelle().compareTo(o.getParcelle());
        }
        return this.getTraitement().compareTo(o.getTraitement());
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful
     * @link(Object) the obj
     * @link(Object) the obj
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id"});
    }

    /**
     * Gets the start date of the traitement on the plot{@link Date}.
     *
     * @return the start date of the traitement on the plot{@link Date}
     */
    public LocalDate getDateDebutTraitement() {
        return this.dateDebutTraitement;
    }

    /**
     * Sets the start date of the traitement on the plot{@link Date}.
     *
     * @param dateDebutTraitement the new start date of the traitement on the
     *                            plot{@link Date}
     */
    public void setDateDebutTraitement(final LocalDate dateDebutTraitement) {
        this.dateDebutTraitement = dateDebutTraitement;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id {@link Long} {@link Long} the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the plot {@link Parcelle}.
     *
     * @return the parcelle
     */
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    /**
     * Sets the parcelle.
     *
     * @param parcelle the new plot {@link Parcelle} {@link Parcelle} the new
     *                 plot
     */
    public void setParcelle(final Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * Gets the experimentale treatment {@link TraitementProgramme}.
     *
     * @return the traitement
     */
    public TraitementProgramme getTraitement() {
        return this.traitement;
    }

    /**
     * Sets the experimental treatment.
     *
     * @param traitement the new experimental treatement {@link TraitementProgramme}
     *                   {@link TraitementProgramme} the new traitement
     */
    public void setTraitement(final TraitementProgramme traitement) {
        this.traitement = traitement;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SuiviParcelle [" + this.parcelle.getSite().getName() + ", "
                + this.traitement.getNom() + ", " + this.parcelle.getName() + "]";
    }

}
