/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.fertilisant.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class WsolTypeOutil.
 */
@Entity
@DiscriminatorValue(FertEtat.FERT_ETAT)
public class FertEtat extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "fert_etat_id";

    /**
     * The Constant WSOL_TYPE_OUTIL.
     */
    public static final String FERT_ETAT = "fertilisant_etat";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
