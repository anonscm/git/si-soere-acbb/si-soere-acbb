/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.listeitineraire;

import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class ListeItineraire.
 */
@Entity
@DiscriminatorValue(ListeItineraire.LISTE_ITINERAIRE_TYPE)
public class ListeItineraire extends ListeACBB {

    /**
     *
     */
    public static final String LISTE_ITINERAIRE_TYPE = "liste_itineraire";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public ListeItineraire() {
        super();
    }

    /**
     * Instantiates a new listeItineraire.
     *
     * @param nom
     * @param value
     * @link(String) the nom
     * @link(String) the value
     */
    public ListeItineraire(final String nom, final String value) {
        super(nom, value);
        this.typeListe = ListeItineraire.LISTE_ITINERAIRE_TYPE;
    }
}
