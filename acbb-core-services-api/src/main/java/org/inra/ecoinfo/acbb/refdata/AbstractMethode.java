/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.refdata;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {AbstractMethode.ATTRIBUTE_JPA_NUMBER_ID})})
public abstract class AbstractMethode implements Comparable<AbstractMethode>, Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "method_id";
    /**
     * The Constant ATTRIBUTE_JPA_.
     */
    @Column(name = AbstractMethode.ATTRIBUTE_JPA_NUMBER_ID, nullable = false, unique = true)
    public static final String ATTRIBUTE_JPA_NUMBER_ID = "number_id";
    /**
     *
     */
    @Column(nullable = false, unique = true)
    public static final String ATTRIBUTE_JPA_LIBELLE = "libelle";
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_DEFINITION = "definition";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Column(name = AbstractMethode.ATTRIBUTE_JPA_NUMBER_ID)
    @NaturalId
    protected Long numberId;

    /**
     *
     */
    @Column(columnDefinition = "TEXT")
    protected String libelle = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     *
     */
    @Column(columnDefinition = "TEXT")
    protected String definition = org.apache.commons.lang.StringUtils.EMPTY;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = AbstractMethode.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    Long id;

    /**
     *
     */
    public AbstractMethode() {
        super();
    }

    /**
     * @param libelle
     * @param definition
     * @param numberId
     */
    public AbstractMethode(String libelle, String definition, Long numberId) {
        super();
        this.numberId = numberId;
        this.libelle = libelle;
        this.definition = definition;
    }

    @Override
    public int compareTo(AbstractMethode o) {
        if (o == null) {
            return -1;
        }
        return this.getNumberId().compareTo(o.getNumberId());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final AbstractMethode other = (AbstractMethode) obj;
        return Objects.equals(this.numberId, other.numberId);
    }

    /**
     * @return the definition
     */
    public String getDefinition() {
        return getSanitizedString(definition);
    }

    // GETTERS ET SETTERS

    /**
     * @param definition the definition to set
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return getSanitizedString(libelle);
    }

    /**
     * @param libelle the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return
     */
    public Long getNumberId() {
        return this.numberId;
    }

    /**
     * @param numberId
     */
    public void setNumberId(Long numberId) {
        this.numberId = numberId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.numberId);
        return hash;
    }

    /**
     * @param field
     * @return
     */
    protected String getSanitizedString(String field) {
        String localResult = field == null ? "" : field;
        if (localResult.contains("\n")) {
            localResult = String.format("\"%s\"", localResult);
        }
        return localResult;
    }

}
