package org.inra.ecoinfo.acbb.dataset.biomasse.biomassproduction.entity;

import org.inra.ecoinfo.acbb.dataset.biomasse.entity.AbstractBiomasse;
import org.inra.ecoinfo.acbb.dataset.biomasse.entity.ValeurBiomasse;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurBiomassProduction.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                RealNode.ID_JPA, BiomassProduction.ID_JPA}),
        indexes = {
                @Index(name = "vbpt_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vbpt_methode_idx", columnList = AbstractMethode.ID_JPA),
                @Index(name = "vbpt_bpt_idx", columnList = BiomassProduction.ID_JPA)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurBiomassProduction.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurBiomassProduction extends ValeurBiomasse<ValeurBiomassProduction> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_BPT = "biomassProduction";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_biomassProduction_vbpt";

    /**
     *
     */
    public static final String ID_JPA = "vbpt_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = BiomassProduction.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA, nullable = false)
    BiomassProduction biomassProduction;

    /*
     * @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
     *
     * @JoinColumn(name = BiomassProduction.ID_JPA, referencedColumnName = AbstractBiomasse.ID_JPA,
     * nullable = false) BiomassProduction biomassProduction;
     */
    // LES CONSTRUCTEURS DE LA SUPER CLASSE

    /**
     *
     */
    public ValeurBiomassProduction() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param biomassProduction
     * @param qualityIndex
     * @param methode
     */
    public ValeurBiomassProduction(Float valeur, RealNode realNode, int measureNumber, float standardDeviation, BiomassProduction biomassProduction, int qualityIndex, AbstractMethode methode) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.biomassProduction = biomassProduction;
    }

    @Override
    public int compareTo(ValeurBiomassProduction o) {
        int comparemesure = this.getBiomassProduction().compareTo(o.getBiomassProduction());
        int compareValeur = this.getDatatypeVariableUnite()
                .getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return compareValeur;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurBiomasse<?> other = (ValeurBiomasse<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return the biomassProduction
     */
    public BiomassProduction getBiomassProduction() {
        return this.biomassProduction;
    }

    /**
     * @param biomassProduction the biomassProduction to set
     */
    public void setBiomassProduction(BiomassProduction biomassProduction) {
        this.biomassProduction = biomassProduction;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }
}
