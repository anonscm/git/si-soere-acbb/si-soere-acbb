package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AbstractIntervention;
import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;

import java.util.List;

/**
 * @param <T>
 * @param <V>
 * @author ptcherniati
 */
public interface IMesureITK<T extends AbstractIntervention, V extends ValeurIntervention> {

    /**
     * @return
     */
    T getSequence();

    /**
     * @return
     */
    List<V> getValeurs();
}
