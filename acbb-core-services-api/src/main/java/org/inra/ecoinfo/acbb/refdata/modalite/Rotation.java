/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.modalite;

import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;

import javax.persistence.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Entity Class Rotation.
 * <p>
 * Describe the alternating vegetal covered and the rotation in a plot if exists
 * </p>
 *
 * @author philippe Tcherniatinsky
 */
@Entity
@DiscriminatorValue(Rotation.ROTATION)
public class Rotation extends Modalite {

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ROTATION = "Rotation";

    /**
     *
     */
    public static final String PATTERN_COUVERT = "([^0-9>]+)(>?)(\\d*)";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The couverts vegetal @link(Map<Integer,SemisCouvertVegetal>).
     * <p>
     * the alternating vegetal covered
     * </P>
     */
    @ManyToMany(targetEntity = SemisCouvertVegetal.class)
    @JoinTable(name = "rotation_couvert", joinColumns = @JoinColumn(name = Modalite.ID_JPA), inverseJoinColumns = @JoinColumn(name = SemisCouvertVegetal.ID_JPA))
    @OrderColumn(columnDefinition = "integer", nullable = false, name = "mapkey")
    Map<Integer, SemisCouvertVegetal> couvertsVegetal = new TreeMap();

    /**
     * The name of the repeated couvert @link(String).
     */
    String repeatedCouvert = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * <p>
     * true if there's an infinite repeated cover
     * </p>
     * The infinite repeated couvert @link(Boolean).
     */
    Boolean infiniteRepeatedCouvert = false;

    /**
     *
     */
    public Rotation() {
    }

    /**
     * @param nom
     * @param code
     * @param description
     */
    public Rotation(String nom, String code, String description) {
        super(nom, code, description);
    }

    /**
     * Gets the couverts vegetal.
     * <p>
     * the alternance of sowing vover names
     * </p>
     *
     * @return the couvertsVegetal
     */
    public Map<Integer, SemisCouvertVegetal> getCouvertsVegetal() {
        return this.couvertsVegetal;
    }

    /**
     * Sets the couverts vegetal.
     *
     * @param couvertsVegetal the couvertsVegetal to set
     */
    public void setCouvertsVegetal(final Map<Integer, SemisCouvertVegetal> couvertsVegetal) {
        this.couvertsVegetal = couvertsVegetal;
    }

    /**
     * Gets the repeated couvert. The name of the repeated couvert
     *
     * @return the repeatedCouvert
     * @link(String).
     */
    public String getRepeatedCouvert() {
        return this.repeatedCouvert;
    }

    /**
     * Sets the repeated couvert.
     *
     * @param repeatedCouvert the repeatedCouvert to set
     */
    public void setRepeatedCouvert(final String repeatedCouvert) {
        this.repeatedCouvert = repeatedCouvert;
    }

    /**
     * Checks if is infinite repeated couvert.
     * <p>
     * true if there's an infinite repeated cover
     * </p>
     * The infinite repeated couvert @link(Boolean).
     *
     * @return the infiniteRepeatedCouvert
     */
    public Boolean isInfiniteRepeatedCouvert() {
        return this.infiniteRepeatedCouvert;
    }

    /**
     * Sets the infinite repeated couvert.
     *
     * @param infiniteRepeatedCouvert the infiniteRepeatedCouvert to set
     */
    public void setInfiniteRepeatedCouvert(final Boolean infiniteRepeatedCouvert) {
        this.infiniteRepeatedCouvert = infiniteRepeatedCouvert;
    }

    /**
     * Update repeated couverts.
     *
     * @param isInfiniteRepeatedCouvert
     * @param repetitionOfCover
     * @param couvert
     * @param index
     * @return the int
     * @link(Matcher) the matches
     * @link(SemisCouvertVegetal) the couvert
     * @link(int) the index
     */
    public int updateRepeatedCouverts(final boolean isInfiniteRepeatedCouvert,
                                      final int repetitionOfCover, final SemisCouvertVegetal couvert, final int index) {
        int returnIndex = index;
        this.getCouvertsVegetal().put(returnIndex, couvert);
        returnIndex++;
        if (repetitionOfCover > 1) {
            this.setRepeatedCouvert(couvert.getValeur());
            for (int i = 0; i < repetitionOfCover - 1; i++) {
                this.getCouvertsVegetal().put(returnIndex, couvert);
                returnIndex++;
            }
            this.setInfiniteRepeatedCouvert(isInfiniteRepeatedCouvert);
        }
        return returnIndex;
    }

}
