package org.inra.ecoinfo.acbb.utils;

import org.inra.ecoinfo.acbb.dataset.IInfoReport;

/**
 * The Class InfoReport.
 *
 * @author Tcherniatinsky Philippe
 * @see org.inra.ecoinfo.acbb.dataset.IInfoReport
 */
public class InfoReport implements IInfoReport {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant CST_HYPHEN @link(String).
     */
    static final String CST_HYPHEN = "-";

    /**
     * The Constant CST_NEW_LINE @link(String).
     */
    static final String CST_NEW_LINE = "\n";

    /**
     * The infos messages @link(String).
     */
    String infosMessages = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     * Instantiates a new infos report.
     */
    public InfoReport() {
        super();
    }

    /**
     * @param infoMessage
     * @see org.inra.ecoinfo.acbb.dataset.IInfoReport#addInfoMessage(java.lang.String)
     */
    @Override
    public void addInfoMessage(final String infoMessage) {
        this.infosMessages = this.infosMessages.concat(InfoReport.CST_HYPHEN).concat(infoMessage)
                .concat(InfoReport.CST_NEW_LINE);

    }

    /**
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.IInfoReport#getInfoMessages()
     */
    @Override
    public String getInfoMessages() {
        return this.infosMessages;
    }

    /**
     * @return
     * @see org.inra.ecoinfo.acbb.dataset.IInfoReport#hasInfo()
     */
    @Override
    public boolean hasInfo() {
        return this.infosMessages.length() > 0;
    }

}
