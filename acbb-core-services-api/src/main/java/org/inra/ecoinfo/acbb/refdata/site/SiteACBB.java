package org.inra.ecoinfo.acbb.refdata.site;

import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.bloc.Bloc;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class SiteACBB.
 */
@Entity(name = "SiteACBB")
@Table(name = SiteACBB.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = SiteACBB.ID_JPA)
public class SiteACBB extends Site {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "id";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "site_acbb_sit";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The ville @link(String).
     */
    @Column(nullable = true)
    String ville;

    /**
     * The adresse @link(String).
     */
    @Column(nullable = true)
    String adresse;

    /**
     * The agro ecosysteme @link(AgroEcosysteme).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = AgroEcosysteme.ID_JPA, referencedColumnName = AgroEcosysteme.ID_JPA, nullable = false)
    AgroEcosysteme agroEcosysteme;

    /**
     * The coordonnees @link(String).
     */
    @Column(nullable = true)
    String coordonnees;

    /**
     * The milieu @link(String).
     */
    String milieu;

    /**
     * The climat @link(String).
     */
    String climat;

    /**
     * The pluviometrie moyenne @link(Float).
     */
    Float pluviometrieMoyenne;

    /**
     * The temperature moyenne @link(Float).
     */
    Float temperatureMoyenne;

    /**
     * The vent dominant @link(String).
     */
    String ventDominant;

    /**
     * The vitesse moyenne @link(Float).
     */
    Float vitesseMoyenne;

    /**
     * The altitude moyenne @link(Float).
     */
    Float altitudeMoyenne;

    /**
     * The date mise en service @link(Date).
     */
    LocalDate dateMiseEnService;

    /**
     * The type de sol @link(String).
     */
    String typeDeSol;

    /**
     * The profondeur moyenne sol @link(Float).
     */
    Float profondeurMoyenneSol;

    /**
     * The traitements @link(List<TraitementProgramme>).
     */
    @OneToMany(mappedBy = "site", cascade = ALL)
    List<TraitementProgramme> traitements = new LinkedList();

    /**
     * The parcelles @link(List<Parcelle>).
     */
    @OneToMany(mappedBy = "site", cascade = ALL)
    List<Parcelle> parcelles = new LinkedList();

    /**
     * The blocs @link(List<Bloc>).
     */
    @OneToMany(mappedBy = "site", cascade = ALL)
    List<Bloc> blocs = new LinkedList();

    /**
     * Instantiates a new site acbb.
     */
    public SiteACBB() {
        super();
    }

    /**
     * Instantiates a new site acbb.
     *
     * @param nom                  the nom
     * @param ville                the ville
     * @param adresse              the adresse
     * @param coordonnees          the coordonnees
     * @param milieu               the milieu
     * @param climat               the climat
     * @param pluviometrieMoyenne  the pluviometrie moyenne
     * @param temperatureMoyenne   the temperature moyenne
     * @param ventDominant         the vent dominant
     * @param vitesseMoyenne       the vitesse moyenne
     * @param altitudeMoyenne      the altitude moyenne
     * @param dateMiseEnService    the date mise en service
     * @param typeDeSol            the type de sol
     * @param profondeurMoyenneSol the profondeur moyenne sol
     * @param agroEcosysteme       the agro ecosysteme
     */
    public SiteACBB(final String nom, final String ville, final String adresse,
                    final String coordonnees, final String milieu, final String climat,
                    final Float pluviometrieMoyenne, final Float temperatureMoyenne,
                    final String ventDominant, final Float vitesseMoyenne, final Float altitudeMoyenne,
                    final LocalDate dateMiseEnService, final String typeDeSol, final Float profondeurMoyenneSol,
                    final AgroEcosysteme agroEcosysteme) {
        super(Utils.createCodeFromString(nom), null, null);
        this.setAgroEcosysteme(agroEcosysteme);
        this.setName(nom);
        this.ville = ville;
        this.adresse = adresse;
        this.coordonnees = coordonnees;
        this.milieu = milieu;
        this.climat = climat;
        this.pluviometrieMoyenne = pluviometrieMoyenne;
        this.temperatureMoyenne = temperatureMoyenne;
        this.ventDominant = ventDominant;
        this.vitesseMoyenne = vitesseMoyenne;
        this.altitudeMoyenne = altitudeMoyenne;
        this.setDateMiseEnService(dateMiseEnService);
        this.typeDeSol = typeDeSol;
        this.profondeurMoyenneSol = profondeurMoyenneSol;
    }

    /**
     * Gets the adresse.
     *
     * @return the adresse
     */
    public String getAdresse() {
        return this.adresse;
    }

    /**
     * Sets the adresse.
     *
     * @param adresse the new adresse
     */
    public void setAdresse(final String adresse) {
        this.adresse = adresse;
    }

    /**
     * Gets the agro ecosysteme.
     *
     * @return the agro ecosysteme
     */
    public AgroEcosysteme getAgroEcosysteme() {
        return this.agroEcosysteme;
    }

    /**
     * Sets the agro ecosysteme.
     *
     * @param agroEcosysteme the new agro ecosysteme
     */
    public void setAgroEcosysteme(final AgroEcosysteme agroEcosysteme) {
        this.agroEcosysteme = agroEcosysteme;
    }

    /**
     * Gets the altitude moyenne.
     *
     * @return the altitude moyenne
     */
    public Float getAltitudeMoyenne() {
        return this.altitudeMoyenne;
    }

    /**
     * Sets the altitude moyenne.
     *
     * @param altitudeMoyenne the new altitude moyenne
     */
    public void setAltitudeMoyenne(final Float altitudeMoyenne) {
        this.altitudeMoyenne = altitudeMoyenne;
    }

    /**
     * Gets the blocs.
     *
     * @return the blocs
     */
    public List<Bloc> getBlocs() {
        return this.blocs;
    }

    /**
     * Sets the blocs.
     *
     * @param blocs the new blocs
     */
    public void setBlocs(final List<Bloc> blocs) {
        this.blocs = blocs;
    }

    /**
     * Gets the climat.
     *
     * @return the climat
     */
    public String getClimat() {
        return this.climat;
    }

    /**
     * Sets the climat.
     *
     * @param climat the new climat
     */
    public void setClimat(final String climat) {
        this.climat = climat;
    }

    /**
     * Gets the coordonnees.
     *
     * @return the coordonnees
     */
    public String getCoordonnees() {
        return this.coordonnees;
    }

    /**
     * Sets the coordonnees.
     *
     * @param coordonnees the new coordonnees
     */
    public void setCoordonnees(final String coordonnees) {
        this.coordonnees = coordonnees;
    }

    /**
     * Gets the date mise en service.
     *
     * @return the date mise en service
     */
    public LocalDate getDateMiseEnService() {
        return this.dateMiseEnService;
    }

    /**
     * Sets the date mise en service.
     *
     * @param dateMiseEnService the new date mise en service
     */
    public void setDateMiseEnService(final LocalDate dateMiseEnService) {
        this.dateMiseEnService = dateMiseEnService;
    }

    /**
     * Gets the milieu.
     *
     * @return the milieu
     */
    public String getMilieu() {
        return this.milieu;
    }

    /**
     * Sets the milieu.
     *
     * @param milieu the new milieu
     */
    public void setMilieu(final String milieu) {
        this.milieu = milieu;
    }

    /**
     * Gets the parcelles.
     *
     * @return the parcelles
     */
    public List<Parcelle> getParcelles() {
        return this.parcelles;
    }

    /**
     * Sets the parcelles.
     *
     * @param parcelles the new parcelles
     */
    public void setParcelles(final List<Parcelle> parcelles) {
        this.parcelles = parcelles;
    }

    /**
     * Gets the pluviometrie moyenne.
     *
     * @return the pluviometrie moyenne
     */
    public Float getPluviometrieMoyenne() {
        return this.pluviometrieMoyenne;
    }

    /**
     * Sets the pluviometrie moyenne.
     *
     * @param pluviometrieMoyenne the new pluviometrie moyenne
     */
    public void setPluviometrieMoyenne(final Float pluviometrieMoyenne) {
        this.pluviometrieMoyenne = pluviometrieMoyenne;
    }

    /**
     * Gets the profondeur moyenne sol.
     *
     * @return the profondeur moyenne sol
     */
    public Float getProfondeurMoyenneSol() {
        return this.profondeurMoyenneSol;
    }

    /**
     * Sets the profondeur moyenne sol.
     *
     * @param profondeurMoyenneSol the new profondeur moyenne sol
     */
    public void setProfondeurMoyenneSol(final Float profondeurMoyenneSol) {
        this.profondeurMoyenneSol = profondeurMoyenneSol;
    }

    /**
     * Gets the temperature moyenne.
     *
     * @return the temperature moyenne
     */
    public Float getTemperatureMoyenne() {
        return this.temperatureMoyenne;
    }

    /**
     * Sets the temperature moyenne.
     *
     * @param temperatureMoyenne the new temperature moyenne
     */
    public void setTemperatureMoyenne(final Float temperatureMoyenne) {
        this.temperatureMoyenne = temperatureMoyenne;
    }

    /**
     * Gets the traitements.
     *
     * @return the traitements
     */
    public List<TraitementProgramme> getTraitements() {
        return this.traitements;
    }

    /**
     * Sets the traitements.
     *
     * @param traitements the new traitements
     */
    public void setTraitements(final List<TraitementProgramme> traitements) {
        this.traitements = traitements;
    }

    /**
     * Gets the type de sol.
     *
     * @return the type de sol
     */
    public String getTypeDeSol() {
        return this.typeDeSol;
    }

    /**
     * Sets the type de sol.
     *
     * @param typeDeSol the new type de sol
     */
    public void setTypeDeSol(final String typeDeSol) {
        this.typeDeSol = typeDeSol;
    }

    /**
     * Gets the vent dominant.
     *
     * @return the vent dominant
     */
    public String getVentDominant() {
        return this.ventDominant;
    }

    /**
     * Sets the vent dominant.
     *
     * @param ventDominant the new vent dominant
     */
    public void setVentDominant(final String ventDominant) {
        this.ventDominant = ventDominant;
    }

    /**
     * Gets the ville.
     *
     * @return the ville
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * Sets the ville.
     *
     * @param ville the new ville
     */
    public void setVille(final String ville) {
        this.ville = ville;
    }

    /**
     * Gets the vitesse moyenne.
     *
     * @return the vitesse moyenne
     */
    public Float getVitesseMoyenne() {
        return this.vitesseMoyenne;
    }

    /**
     * Sets the vitesse moyenne.
     *
     * @param vitesseMoyenne the new vitesse moyenne
     */
    public void setVitesseMoyenne(final Float vitesseMoyenne) {
        this.vitesseMoyenne = vitesseMoyenne;
    }

    /**
     * Sets the nom.
     *
     * @param nom the new nom
     * @see org.inra.ecoinfo.refdata.AbstractRecurentObject#setNom(java.lang
     * .String)
     */
    @Override
    public void setName(final String nom) {
        super.setName(nom);
    }

    /**
     * To string.
     *
     * @return the string
     * @see org.inra.ecoinfo.refdata.site.Site#toString()
     */
    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public int compareTo(INodeable o) {
        if (o == null || !(o instanceof SiteACBB)) {
            return -1;
        }
        SiteACBB oo = (SiteACBB) o;
        return this.getPath().compareTo(oo.getPath());
    }

    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) SiteACBB.class;
    }
}
