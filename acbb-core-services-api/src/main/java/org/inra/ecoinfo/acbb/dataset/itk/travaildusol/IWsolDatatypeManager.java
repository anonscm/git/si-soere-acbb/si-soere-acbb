/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.travaildusol;

/**
 * The Interface ISemisDatatypeManager.
 */
public interface IWsolDatatypeManager {

    /**
     * The Constant CODE_DATATYPE_SEMIS.
     */
    String CODE_DATATYPE_TRAVAIL_DU_SOL = "travail_du_sol";
}
