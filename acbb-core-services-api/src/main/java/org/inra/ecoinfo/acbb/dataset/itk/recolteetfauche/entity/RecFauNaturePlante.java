/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PaturageTypeAnimaux.
 */
@Entity
@DiscriminatorValue(RecFauNaturePlante.RECFAU_NATURE_PLANTE)
public class RecFauNaturePlante extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "recfau_nat_plte";

    /**
     * The Constant SEMIS_COUVERT_VEGETAL.
     */
    public static final String RECFAU_NATURE_PLANTE = "recfau_nat_plte";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
