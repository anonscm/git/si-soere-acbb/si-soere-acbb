package org.inra.ecoinfo.acbb.dataset.itk.travaildusol.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.util.List;

/**
 * The Class ValeurTravailDuSol.
 */
@Entity
@Table(name = ValeurTravailDuSol.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, MesureTravailDuSol.ID_JPA}),
        indexes = {
                @Index(name = "vwsol_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vwsol_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vwsol_mwsol_idx", columnList = MesureTravailDuSol.ID_JPA)})
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurTravailDuSol.ID_JPA), name = "id")})
public class ValeurTravailDuSol extends ValeurIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "valeur_travail_du_sol_vwsol";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vwsol_id";

    /**
     *
     */
    public static final String NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE = "vwsol_vq";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The mesure travail du sol @link(MesureTravailDuSol).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureTravailDuSol.ID_JPA, referencedColumnName = MesureTravailDuSol.ID_JPA, nullable = false)
    MesureTravailDuSol mesureTravailDuSol;

    /**
     * The valeurs qualitatives @link(List<? extends IValeurQualitative>).
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = ListeItineraire.class)
    @JoinTable(name = ValeurTravailDuSol.NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE, joinColumns = @JoinColumn(name = ValeurTravailDuSol.ID_JPA, referencedColumnName = ValeurTravailDuSol.ID_JPA), inverseJoinColumns = @JoinColumn(name = ListeACBB.ID_JPA, referencedColumnName = ListeACBB.ID_JPA))
    List<ListeACBB> valeursQualitatives;

    /**
     * Instantiates a new valeur travail du sol.
     */
    public ValeurTravailDuSol() {
        super();
    }

    /**
     * Instantiates a new valeur travail du sol.
     *
     * @param valeur             the valeur
     * @param realNode           the datatype variable unite
     * @param mesureTravailDuSol the mesure travail du sol
     */
    public ValeurTravailDuSol(final float valeur,
                              final RealNode realNode,
                              final MesureTravailDuSol mesureTravailDuSol) {
        super(valeur, realNode);
        this.mesureTravailDuSol = mesureTravailDuSol;
    }

    /**
     * @param valeursQualitatives
     * @param localDvu
     * @param mesureTravailDuSol2
     */
    public ValeurTravailDuSol(List<? extends IValeurQualitative> valeursQualitatives,
                              RealNode localDvu, MesureTravailDuSol mesureTravailDuSol2) {
        super(valeursQualitatives, localDvu);
        this.mesureTravailDuSol = mesureTravailDuSol2;
    }

    /**
     * @param valeurQualitative
     * @param localDvu
     * @param mesureTravailDuSol2
     */
    public ValeurTravailDuSol(ListeItineraire valeurQualitative, RealNode localDvu,
                              MesureTravailDuSol mesureTravailDuSol2) {
        super(valeurQualitative, localDvu);
        this.mesureTravailDuSol = mesureTravailDuSol2;
    }

    /**
     * Gets the mesure travail du sol.
     *
     * @return the mesure travail du sol
     */
    public MesureTravailDuSol getMesureTravailDuSol() {
        return this.mesureTravailDuSol;
    }

    /**
     * Sets the mesure travail du sol.
     *
     * @param mesureTravailDuSol the new mesure travail du sol
     */
    public void setMesureTravailDuSol(final MesureTravailDuSol mesureTravailDuSol) {
        this.mesureTravailDuSol = mesureTravailDuSol;
    }

    /**
     * @return @see
     * org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention#getValeursQualitatives()
     */
    @Override
    public List getValeursQualitatives() {
        return this.valeursQualitatives;
    }

    /**
     * @param valeursQualitatives
     * @see org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention#setValeursQualitatives(java.util.List)
     */
    @Override
    public void setValeursQualitatives(List valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

}
