package org.inra.ecoinfo.acbb.dataset.itk.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * The Class VersionTraitementRealise.
 */
@Entity
@Table(name = VersionTraitementRealisee.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        VersionDeTraitement.ID_JPA, Parcelle.ID_JPA, VersionTraitementRealisee.ATTRIBUTE_JPA_NUMERO}),
        indexes = {
                @Index(name = "vtr_vdt_idx", columnList = VersionDeTraitement.ID_JPA),
                @Index(name = "vdr_parcelle_idx", columnList = Parcelle.ID_JPA)
        })
public class VersionTraitementRealisee implements Serializable,
        Comparable<VersionTraitementRealisee> {

    /**
     * The Constant NAME_ENTITY_ITK_TREAL @link(String).
     */
    public static final String NAME_ENTITY_SEMIS_TREAL = "semis_treal";

    /**
     * The Constant ATTRIBUTE_JPA_NUMERO.
     */
    public static final String ATTRIBUTE_JPA_NUMERO = "numero";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "version_traitement_realisee_treal";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "treal_id";

    /**
     * The Constant ATTRIBUTE_JPA_VERSION_DE_TRAITEMENT @link(String).
     */
    public static final String ATTRIBUTE_JPA_VERSION_DE_TRAITEMENT = "versionDeTraitement";

    /**
     * The Constant ATTRIBUTE_JPA_VERSION_DE_TRAITEMENT @link(String).
     */
    public static final String ATTRIBUTE_JPA_PARCELLE = "parcelle";
    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.itk.entity.messages";
    /**
     * The Constant PATTERN_VERSION_FILE_NAME @link(String).
     */
    static final String PATTERN_VERSION_FILE_NAME = "PATTERN_VERSION_FILE_NAME";
    /**
     * The Constant PROPERTY_CST_ARROW_TAB @link(String).
     */
    static final String PROPERTY_CST_ARROW_TAB = " -> ";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = VersionTraitementRealisee.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The numero @link(int).
     */
    @Column(nullable = false, name = VersionTraitementRealisee.ATTRIBUTE_JPA_NUMERO)
    @NaturalId
    int numero;

    /**
     * The version de traitement @link(VersionDeTraitement).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionDeTraitement.ID_JPA, referencedColumnName = VersionDeTraitement.ID_JPA, nullable = false)
    @NaturalId
    VersionDeTraitement versionDeTraitement;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Parcelle.ID_JPA, referencedColumnName = Parcelle.ID_JPA, nullable = false)
    @NaturalId
    Parcelle parcelle;

    /**
     * The annees realisees @link(Map<Integer,AnneeRealisee>).
     */
    @OneToMany(mappedBy = AnneeRealisee.ATTRIBUTE_JPA_VERSION_DE_TRAITEMENT_REALISEE)
    @MapKey(name = AnneeRealisee.ATTRIBUTE_JPA_ANNEE)
    Map<Integer, AnneeRealisee> anneesRealisees = new HashMap();

    @Transient
    @OneToMany(mappedBy = Tempo.ATTRIBUTE_JPA_ROTATION_REALISEE)
    List<Tempo> tempos = new LinkedList();

    /**
     * Instantiates a new version traitement realise.
     */
    public VersionTraitementRealisee() {
        super();
    }

    /**
     * Instantiates a new version traitement realisee.
     *
     * @param numero
     * @param parcelle
     * @param versionDeTraitement
     * @link(int) the numero
     * @link(VersionDeTraitement) the version de traitement
     */
    public VersionTraitementRealisee(final int numero,
                                     final VersionDeTraitement versionDeTraitement, final Parcelle parcelle) {
        super();
        this.numero = numero;
        this.versionDeTraitement = versionDeTraitement;
        this.parcelle = parcelle;
    }

    /**
     * Compare to.
     *
     * @param o
     * @return the int @see java.lang.Comparable#compareTo(java.lang.Object)
     * @link(VersionTraitementRealisee) the o
     */
    @Override
    public int compareTo(final VersionTraitementRealisee o) {
        if (!this.parcelle.equals(o.parcelle)) {
            return this.parcelle.compareTo(o.parcelle);
        }
        if (!this.versionDeTraitement.equals(o.versionDeTraitement)) {
            return this.versionDeTraitement.compareTo(o.versionDeTraitement);
        }
        if (this.numero == o.numero) {
            return 0;
        } else {
            return this.numero > o.numero ? 1 : -1;
        }
    }

    /**
     * Equals.
     *
     * @param obj
     * @return true, if successful @see java.lang.Object#equals(java.lang.Object)
     * @link(Object) the obj
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id"});
    }

    /**
     * Gets the annees realisees.
     *
     * @return the anneesRealisees
     */
    public Map<Integer, AnneeRealisee> getAnneesRealisees() {
        return this.anneesRealisees;
    }

    /**
     * Gets the bindeds itk.
     *
     * @return the bindeds itk
     */
    public Set<AbstractIntervention> getBindedsITK() {
        Set<AbstractIntervention> interventions = new TreeSet();
        for (Tempo tempo : this.getTempos()) {
            interventions.addAll(tempo.getBindedsITK());
        }
        return interventions;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the numero.
     *
     * @return the numero
     */
    public int getNumero() {
        return this.numero;
    }

    /**
     * Sets the numero.
     *
     * @param numero the new numero
     */
    public void setNumero(final int numero) {
        this.numero = numero;
    }

    /**
     * @return
     */
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    /**
     * @param parcelle
     */
    public void setParcelle(Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * @return the tempos
     */
    public List<Tempo> getTempos() {
        return this.tempos;
    }

    /**
     * @param tempos the tempos to set
     */
    public void setTempos(List<Tempo> tempos) {
        this.tempos = tempos;
    }

    /**
     * Gets the version de traitement.
     *
     * @return the version de traitement
     */
    public VersionDeTraitement getVersionDeTraitement() {
        return this.versionDeTraitement;
    }

    /**
     * Sets the version de traitement.
     *
     * @param versionDeTraitement the new version de traitement
     */
    public void setVersionDeTraitement(final VersionDeTraitement versionDeTraitement) {
        this.versionDeTraitement = versionDeTraitement;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
