package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.ACBBVariableValue;
import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import java.time.LocalDate;
import java.util.List;

/**
 * @param <T>
 * @param <L>
 * @author ptcherniati
 */
public interface ILineRecord<T extends ILineRecord, L extends IValeurQualitative> {

    /**
     * @return
     */
    int getAnneeNumber();

    /**
     * @return
     */
    LocalDate getDate();

    /**
     * @return
     */
    String getObservation();

    /**
     * @return
     */
    long getOriginalLineNumber();

    /**
     * @return
     */
    Parcelle getParcelle();

    /**
     * @return
     */
    int getPeriodeNumber();

    /**
     * @return
     */
    int getRotationNumber();

    /**
     * @return
     */
    SuiviParcelle getSuiviParcelle();

    /**
     * @return
     */
    Tempo getTempo();

    /**
     * @param tempo
     */
    void setTempo(final Tempo tempo);

    /**
     * @return
     */
    List<ACBBVariableValue<L>> getVariablesValues();

}
