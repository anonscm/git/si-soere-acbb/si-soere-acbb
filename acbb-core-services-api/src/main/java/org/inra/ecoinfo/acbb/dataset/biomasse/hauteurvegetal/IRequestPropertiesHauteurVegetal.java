/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.biomasse.hauteurvegetal;

import org.inra.ecoinfo.acbb.dataset.biomasse.IRequestPropertiesBiomasse;

/**
 * The Interface IRequestPropertiesAI.
 */
public interface IRequestPropertiesHauteurVegetal extends IRequestPropertiesBiomasse {

}
