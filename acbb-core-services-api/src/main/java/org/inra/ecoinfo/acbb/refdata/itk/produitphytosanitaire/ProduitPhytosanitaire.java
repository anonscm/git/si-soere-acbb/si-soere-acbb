/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.itk.produitphytosanitaire;

import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The Class ProduitPhytosanitaire.
 */
@Entity
@Table(name = ProduitPhytosanitaire.NAME_ENTITY_JPA,
        indexes = {
                @Index(name = "produitphyt_type_idx", columnList = ProduitPhytosanitaire.ATTRIBUTE_JPA_TYPE),
                @Index(name = "produitphyt_namm_idx", columnList = ProduitPhytosanitaire.ATTRIBUTE_JPA_NAMM)})
public class ProduitPhytosanitaire implements Serializable, Comparable<ProduitPhytosanitaire> {

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ATTRIBUTE_JPA_CODE = "code";

    /**
     * The Constant ATTRIBUTE_JPA_NOM.
     */
    public static final String ATTRIBUTE_JPA_NOM = "nom";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TYPE = "type";

    /**
     * The Constant ATTRIBUTE_JPA_NAMM.
     */
    public static final String ATTRIBUTE_JPA_NAMM = "namm";

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "produit_phytosanitaire_pphyt";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "pphyt_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    // ATTRIBUTS
    /**
     * The id <long>.
     */
    @Id
    @Column(name = ProduitPhytosanitaire.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The nom @link(String).
     */
    @Column(nullable = false, unique = true, name = ProduitPhytosanitaire.ATTRIBUTE_JPA_NOM)
    String nom;

    /**
     * The code @link(String).
     */
    @Column(nullable = false, unique = true, name = ProduitPhytosanitaire.ATTRIBUTE_JPA_CODE)
    String code;

    /**
     * The type @link(PhytType).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProduitPhytosanitaire.ATTRIBUTE_JPA_TYPE, referencedColumnName = ListeItineraire.ID_JPA, nullable = false)
    PhytType type;

    /**
     * The namm @link(PhytNamm).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProduitPhytosanitaire.ATTRIBUTE_JPA_NAMM, referencedColumnName = ListeItineraire.ID_JPA, nullable = false)
    @NaturalId
    PhytNamm namm;

    /**
     * Instantiates a new produit phytosanitaire.
     */
    public ProduitPhytosanitaire() {
        super();
    }

    /**
     * Instantiates a new produit phytosanitaire.
     *
     * @param nom  the nom
     * @param code the code
     * @param type the type
     * @param namm the namm
     */
    public ProduitPhytosanitaire(final String nom, final String code, final PhytType type,
                                 final PhytNamm namm) {
        super();
        this.nom = nom;
        this.code = code;
        this.type = type;
        this.namm = namm;
    }

    @Override
    public int compareTo(ProduitPhytosanitaire o) {
        if (o == null) {
            return -1;
        }
        return this.getNom().compareTo(o.getNom());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ProduitPhytosanitaire other = (ProduitPhytosanitaire) obj;
        return Objects.equals(this.nom, other.nom);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the namm.
     *
     * @return the namm
     */
    public PhytNamm getNamm() {
        return this.namm;
    }

    /**
     * Sets the namm.
     *
     * @param namm the new namm
     */
    public void setNamm(final PhytNamm namm) {
        this.namm = namm;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom the new nom
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public PhytType getType() {
        return this.type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(final PhytType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.nom);
        return hash;
    }

}
