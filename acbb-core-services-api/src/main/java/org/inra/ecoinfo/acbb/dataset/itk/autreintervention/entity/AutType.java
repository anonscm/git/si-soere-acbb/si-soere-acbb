/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.autreintervention.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class AutNature.
 */
@Entity
@DiscriminatorValue(AutType.AUT_TYPE)
public class AutType extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "aut_type_id";

    /**
     * The Constant AUT_NATURE.
     */
    public static final String AUT_TYPE = "autre_intervention_type";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
}
