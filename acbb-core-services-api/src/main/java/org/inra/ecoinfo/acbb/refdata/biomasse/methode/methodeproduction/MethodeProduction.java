/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.methode.methodeproduction;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class Bloc.
 */
@Entity
@Table(name = MethodeProduction.NAME_ENTITY_JPA)
public class MethodeProduction extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_production";

    // LES CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public MethodeProduction() {
        super();
    }

    // LES CONSTRUCTEURS

    /**
     * Instantiates a new bloc.
     *
     * @param definition
     * @param libelle
     * @param numberId
     */
    // GETTERS ET SETTERS
    public MethodeProduction(String definition, String libelle, Long numberId) {
        super(libelle, definition, numberId);
    }

    /**
     * @param definition
     */
    public void update(String definition) {
        this.definition = definition;
    }

}
