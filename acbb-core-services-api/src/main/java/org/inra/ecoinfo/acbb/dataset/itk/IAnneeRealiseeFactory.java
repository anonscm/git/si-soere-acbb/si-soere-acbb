package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.AnneeRealisee;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;

/**
 * A factory for creating IAnneeRealisee objects.
 *
 * @param <T> the generic type
 */
public interface IAnneeRealiseeFactory<T extends ILineRecord> {

    /**
     * Gets the or create annee realisee.
     *
     * @param line
     * @param errorsReport
     * @param infoReport
     * @return the or create annee realisee
     * @link{T the line
     * @link{ErrorsReport the errors report
     * @link{InfoReport the info report
     * @link(T) the line
     * @link(ErrorsReport) the errors report
     * @link(InfoReport) the info report
     * @link(T) the line
     * @link(ErrorsReport) the errors report
     */
    AnneeRealisee getOrCreateAnneeRealisee(T line, ErrorsReport errorsReport, InfoReport infoReport);

    /**
     * Gets the periode realisee factory.
     *
     * @return the periode realisee factory
     */
    IPeriodeRealiseeFactory<T> getPeriodeRealiseeFactory();

}
