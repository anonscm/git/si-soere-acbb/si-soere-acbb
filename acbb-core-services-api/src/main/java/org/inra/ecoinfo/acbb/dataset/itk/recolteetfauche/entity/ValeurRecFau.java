package org.inra.ecoinfo.acbb.dataset.itk.recolteetfauche.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;

/**
 * The Class ValeurSemis.
 */
@Entity
@Table(name = ValeurRecFau.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, RecFau.ID_JPA}),
        indexes = {
                @Index(name = "vrecfau_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vrecfau_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vrecfau_recfau_idx", columnList = RecFau.ID_JPA)})
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurRecFau.ID_JPA), name = "id")})
public class ValeurRecFau extends ValeurIntervention {

    static final String ATTRIBUTE_JPA_REC_FAU = "recFau";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static final String NAME_ENTITY_JPA = "valeur_recolte_et_fauche_vrecfau";

    /**
     * The Constant ID_JPA.
     */
    static final String ID_JPA = "vrecfau_id";

    static final String REC_FAU = "recFau";

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RecFau.ID_JPA, referencedColumnName = RecFau.ID_JPA, nullable = false)
    RecFau recFau;

    /**
     *
     */
    public ValeurRecFau() {
        super();
    }

    /**
     * @param valeur
     * @param realNode
     * @param recFau
     */
    public ValeurRecFau(final float valeur, final RealNode realNode, final RecFau recFau) {
        super(valeur, realNode);
        this.recFau = recFau;
    }

    /**
     * @param valeur
     * @param realNode
     * @param recFau
     */
    public ValeurRecFau(final IValeurQualitative valeur, final RealNode realNode, final RecFau recFau) {
        super(valeur, realNode);
        this.recFau = recFau;
    }

    /**
     * @return the recFau
     */
    public RecFau getRecFau() {
        return this.recFau;
    }

    /**
     * @param recFau the recFau to set
     */
    public void setRecFau(RecFau recFau) {
        this.recFau = recFau;
    }

}
