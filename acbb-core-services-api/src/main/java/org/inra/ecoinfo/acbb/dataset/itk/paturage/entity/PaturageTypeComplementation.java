/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.paturage.entity;

import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PaturageTypeAnimaux.
 */
@Entity
@DiscriminatorValue(PaturageTypeComplementation.PATURAGE_TYPE_COMPLEMENTATION)
public class PaturageTypeComplementation extends ListeItineraire {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "pat_type_complementation";

    /**
     * The Constant SEMIS_COUVERT_VEGETAL.
     */
    public static final String PATURAGE_TYPE_COMPLEMENTATION = "pat_type_complementation";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
