/*
 *
 */
package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.Serializable;

/**
 * The Interface IDeleteRecord. interface objects used to remove file versions
 *
 * @author Tcherniatinsky Philippe
 */
public interface IDeleteRecord extends Serializable {

    /**
     * Delete record.
     *
     * @param versionFile
     * @throws BusinessException delete the version of versionFile {@link VersionFile} the version file
     * @link(VersionFile) the version file
     * @link(VersionFile) the version file
     */
    void deleteRecord(VersionFile versionFile) throws BusinessException;

}
