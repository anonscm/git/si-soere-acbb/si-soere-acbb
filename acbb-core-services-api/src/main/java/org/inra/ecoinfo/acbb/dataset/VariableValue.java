package org.inra.ecoinfo.acbb.dataset;

import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

/**
 * The Class VariableValue.
 */
public class VariableValue {

    /**
     * The variable @link(VariableACBB).
     */
    DatatypeVariableUniteACBB dvu;

    /**
     * The value @link(String).
     */
    String value;

    /**
     * The flag @link(Boolean).
     */
    Boolean flag = false;

    /**
     * The quality class @link(int).
     */
    Integer qualityClass;

    /**
     * Instantiates a new variable value.
     *
     * @param value the value
     */
    public VariableValue(final String value) {
        this.value = value;
    }

    /**
     * Instantiates a new variable value.
     *
     * @param dvu
     * @param value the value
     */
    public VariableValue(final DatatypeVariableUniteACBB dvu, final String value) {
        super();
        this.dvu = dvu;
        this.value = value;
    }

    /**
     * Gets the quality class.
     *
     * @return the quality class
     */
    public Integer getQualityClass() {
        return this.qualityClass;
    }

    /**
     * Sets the quality class.
     *
     * @param qualityClass the new quality class
     */
    public void setQualityClass(final Integer qualityClass) {
        this.qualityClass = qualityClass;
        this.flag = true;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public DatatypeVariableUniteACBB getDatatypeVariableUnite() {
        return this.dvu;
    }

    /**
     * @return
     */
    public boolean isQualitative() {
        return this.dvu != null && this.dvu.getVariable().getIsQualitative();
    }

    /**
     * Checks if is quality class.
     *
     * @return the boolean
     */
    public Boolean isQualityClass() {
        return this.flag;
    }

    /**
     * Sets the variable.
     *
     * @param dvu the new variable
     */
    public void setDAtatypeUniteVaraible(final DatatypeVariableUniteACBB dvu) {
        this.dvu = dvu;
    }

}
