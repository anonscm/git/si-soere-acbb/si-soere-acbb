package org.inra.ecoinfo.acbb.dataset.itk.semis.entity;

import org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention;
import org.inra.ecoinfo.acbb.refdata.itk.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.util.List;

/**
 * The Class ValeurSemis.
 */
@Entity
@Table(name = ValeurSemis.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, MesureSemis.ID_JPA}),
        indexes = {
                @Index(name = "vsem_vq_idx", columnList = ListeACBB.ID_JPA),
                @Index(name = "vsem_variable_idx", columnList = RealNode.ID_JPA),
                @Index(name = "vsem_msem_idx", columnList = MesureSemis.ID_JPA)})
@AttributeOverrides(value = {@AttributeOverride(column = @Column(name = ValeurSemis.ID_JPA), name = "id")})
public class ValeurSemis extends ValeurIntervention {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "valeur_semis_vsem";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vsem_id";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE @link(String).
     */
    private static final String NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE = "vsem_vq";
    /**
     * The mesure semis @link(MesureSemis).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureSemis.ID_JPA, referencedColumnName = MesureSemis.ID_JPA, nullable = false)
    MesureSemis mesureSemis;

    /**
     * The valeurs qualitatives @link(List<? extends IValeurQualitative>).
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = ListeItineraire.class)
    @JoinTable(name = ValeurSemis.NAME_ENTITY_VALEUR_ATTR_LISTE_ITINERAIRE, joinColumns = @JoinColumn(name = ValeurSemis.ID_JPA, referencedColumnName = ValeurSemis.ID_JPA), inverseJoinColumns = @JoinColumn(name = ListeACBB.ID_JPA, referencedColumnName = ListeACBB.ID_JPA))
    List<ListeACBB> valeursQualitatives;

    /**
     * Instantiates a new valeur semis.
     */
    public ValeurSemis() {
        super();
    }

    /**
     * Instantiates a new valeur semis.
     *
     * @param valeur
     * @param realNode
     * @param mesureSemis
     * @link(Float)
     * @link(DatatypeVariableUnite)
     * @link(MesureSemis) the mesure semis
     */
    public ValeurSemis(final Float valeur, final RealNode realNode, final MesureSemis mesureSemis) {
        super(valeur, realNode);
        this.mesureSemis = mesureSemis;
    }

    /**
     * Instantiates a new valeur semis.
     *
     * @param valeur
     * @param realNode
     * @param mesureSemis
     * @link(ListeItineraire)
     * @link(DatatypeVariableUnite)
     * @link(MesureSemis) the mesure semis
     */
    public ValeurSemis(final ListeItineraire valeur, final RealNode realNode, final MesureSemis mesureSemis) {
        super(valeur, realNode);
        this.mesureSemis = mesureSemis;
    }

    /**
     * Gets the mesure semis.
     *
     * @return the mesure semis
     */
    public MesureSemis getMesureSemis() {
        return this.mesureSemis;
    }

    /**
     * Sets the mesure semis.
     *
     * @param mesureSemis the new mesure semis
     */
    public void setMesureSemis(final MesureSemis mesureSemis) {
        this.mesureSemis = mesureSemis;
    }

    /**
     * @return @see
     * org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention#getValeursQualitatives()
     */
    @Override
    public List getValeursQualitatives() {
        return this.valeursQualitatives;
    }

    /**
     * @param valeursQualitatives
     * @see org.inra.ecoinfo.acbb.dataset.itk.entity.ValeurIntervention#setValeursQualitatives(java.util.List)
     */
    @Override
    public void setValeursQualitatives(List valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

}
