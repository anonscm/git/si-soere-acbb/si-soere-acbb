/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.Collection;
import java.util.List;

/**
 * @author tcherniatinsky
 */
public interface ISiteManager {

    /**
     * @param intervalsDate
     * @return
     */
    Collection<? extends SiteACBB> getAvailableSites(List<IntervalDate> intervalsDate);

}
