package org.inra.ecoinfo.acbb.utils;

import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 * @author ptcherniati
 */
public class ACBBMessages {
    /**
     * The Constant ACBB_DATASET_BUNDLE_NAME.
     */
    public static final String ACBB_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.messages";
    /**
     *
     */
    public static final String PATTERN_1_FIELD = ";%s";
    private static ILocalizationManager localizationManager;

    private ACBBMessages() {
    }

    /**
     * Gets the aCBB message.
     *
     * @param key
     * @return the ACBB message from key {@link String} the key
     * @link(String) the key
     */
    public static final String getACBBMessage(final String key) {
        return ACBBMessages.getACBBMessageWithBundle(ACBBMessages.ACBB_DATASET_BUNDLE_NAME, key);
    }

    /**
     * Gets the aCBB message.
     *
     * @param bundlePath
     * @param key
     * @return the ACBB message from key
     * @link(String) the bundle path
     * @link(String) the key
     * @link(String) the bundle path {@link String} the key
     */
    public static final String getACBBMessageWithBundle(final String bundlePath, final String key) {
        return ACBBMessages.localizationManager == null ? key : ACBBMessages.localizationManager
                .getMessage(bundlePath, key);
    }

    /**
     * @param localizationManager
     */
    public static void setLocalizationManager(ILocalizationManager localizationManager) {
        ACBBMessages.localizationManager = localizationManager;
    }

}
