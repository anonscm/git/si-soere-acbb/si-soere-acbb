/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.biomasse.massevegclass;

import org.inra.ecoinfo.acbb.refdata.biomasse.listevegetation.ListeVegetation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue(MassVegClassTypeMasseVegetale.MAV_CLASS_TYPE_MASSE_VEGETALE)
public class MassVegClassTypeMasseVegetale extends ListeVegetation {

    /**
     *
     */
    public static final String MAV_CLASS_TYPE_MASSE_VEGETALE = "mav_class_type_masse_vegetale";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mav_class_type_masse_vegetale";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
