/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * @author ptcherniati
 */
@MappedSuperclass
public abstract class SynthesisValueWithParcelle extends GenericSynthesisValue {

    String parcelleCode;

    public SynthesisValueWithParcelle() {
    }

    public SynthesisValueWithParcelle(String parcelleCode, LocalDateTime date, String site, String variable, Float valueFloat, String valueString, Long idNode) {
        super(date, site, variable, valueFloat, valueString, idNode);
        this.parcelleCode = parcelleCode;
    }

    public SynthesisValueWithParcelle(String parcelleCode, LocalDateTime date, String site, String variable, Float valueFloat, String valueString, Long idNode, Boolean isMean) {
        super(date, site, variable, valueFloat, valueString, idNode, isMean);
        this.parcelleCode = parcelleCode;
    }

    public String getParcelleCode() {
        return parcelleCode;
    }

    public void setParcelleCode(String parcelleCode) {
        this.parcelleCode = parcelleCode;
    }

}
