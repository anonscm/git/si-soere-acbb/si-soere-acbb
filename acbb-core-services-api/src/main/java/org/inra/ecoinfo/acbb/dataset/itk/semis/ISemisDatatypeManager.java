/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.semis;

/**
 * The Interface ISemisDatatypeManager.
 */
public interface ISemisDatatypeManager {

    /**
     * The Constant CODE_DATATYPE_SEMIS.
     */
    String CODE_DATATYPE_SEMIS = "semis";
}
