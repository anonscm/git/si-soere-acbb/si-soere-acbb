package org.inra.ecoinfo.acbb.refdata.traitement;

import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.StringUtils;
import org.inra.ecoinfo.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.ALL;

/**
 * The Class TraitementProgramme.
 */
@Entity
@Table(name = TraitementProgramme.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
        SiteACBB.ID_JPA, TraitementProgramme.ATTRIBUTE_JPA_CODE}),
        indexes = {
                @Index(name = "tra_sit_idx", columnList = SiteACBB.ID_JPA)
                ,
                @Index(name = "tra_dd_idx", columnList = "dateDebutTraitement")
                ,
                @Index(name = "tra_df_idx", columnList = "dateFinTraitement")
                ,
                @Index(name = "tra_code_sit_idx", columnList = SiteACBB.ID_JPA + ",code")})
public class TraitementProgramme implements Serializable, Comparable<TraitementProgramme> {

    /**
     * The Constant ATTRIBUTE_JPA_CODE @link(String).
     */
    public static final String ATTRIBUTE_JPA_CODE = "code";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NAME = "nom";

    /**
     * The Constant ATTRIBUTE_JPA_AFFICHGAE @link(String).
     */
    public static final String ATTRIBUTE_JPA_AFFICHAGE = "affichage";

    /**
     * The Constant ATTRIBUTE_JPA_DESCRIPTION @link(String).
     */
    public static final String ATTRIBUTE_JPA_DESCRIPTION = "description";

    /**
     * The Constant ATTRIBUTE_JPA_DUREE @link(String).
     */
    public static final String ATTRIBUTE_JPA_DUREE = "duree";

    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "traitement_tra";

    /**
     * The Constant NAME_ENTITY_TRAITEMENT_PARCELLES_JPA @link(String).
     */
    public static final String NAME_ENTITY_TRAITEMENT_PARCELLES_JPA = "suivi_parcelle_spa";

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "tra_id";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The id @link(Long).
     */
    @Id
    @Column(name = TraitementProgramme.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The site @link(SiteACBB).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SiteACBB.ID_JPA, referencedColumnName = SiteACBB.ID_JPA, nullable = false)
    @NaturalId
    SiteACBB site;

    /**
     * The nom @link(String).
     */
    @Column(nullable = false, name = TraitementProgramme.ATTRIBUTE_JPA_NAME)
    String nom;

    /**
     * The code @link(String).
     */
    @Column(nullable = false)
    @NaturalId
    String code;

    /**
     * The affichage @link(String).
     */
    @Column(nullable = false, name = TraitementProgramme.ATTRIBUTE_JPA_AFFICHAGE)
    String affichage;

    /**
     * The description @link(String).
     */
    @Column(nullable = false, name = TraitementProgramme.ATTRIBUTE_JPA_DESCRIPTION, columnDefinition = "TEXT")
    String description;

    /**
     * The date debut traitement @link(Date).
     */
    @Column(nullable = false)
    LocalDate dateDebutTraitement;

    /**
     * The date fin traitement @link(Date).
     */
    LocalDate dateFinTraitement;

    /**
     * The duree @link(String).
     */
    @Column(nullable = true, name = TraitementProgramme.ATTRIBUTE_JPA_DUREE)
    String duree;

    /**
     * The version de traitements @link(List<VersionDeTraitement>).
     */
    @OneToMany(mappedBy = "traitementProgramme", cascade = ALL)
    List<VersionDeTraitement> versionDeTraitements = new LinkedList();

    /**
     * Instantiates a new traitement programme.
     */
    public TraitementProgramme() {
        super();
    }

    /**
     * Instantiates a new traitement programme.
     *
     * @param nomTraitement
     * @param affichage
     * @param description
     * @param duree
     * @param dateDebut
     * @param dateFin
     * @link(String) the nom traitement
     * @link(String) the affichage
     * @link(String) the description
     * @link(String) the duree
     * @link(Date) the date debut
     * @link(Date) the date fin
     */
    public TraitementProgramme(final String nomTraitement, final String affichage,
                               final String description, final String duree, final LocalDate dateDebut, final LocalDate dateFin) {
        super();
        this.setNom(nomTraitement);
        this.affichage = affichage;
        this.description = description;
        this.duree = duree;
        this.setDateDebutTraitement(dateDebut);
        this.setDateFinTraitement(dateFin);
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final TraitementProgramme o) {
        if (o == null) {
            return -1;
        }
        if (this.getId().equals(o.getId())) {
            return 0;
        }
        if (this.getSite().compareTo(o.getSite()) != 0) {
            return this.getSite().compareTo(o.getSite());
        }
        return getAlphanumeAffichage().compareTo(o.getAlphanumeAffichage());
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        TraitementProgramme rhs = (TraitementProgramme) obj;
        return Objects.equals(this.id, rhs.id) && Objects.equals(this.site, rhs.site)
                && Objects.equals(this.code, rhs.code);
    }

    @Override
    public String toString() {
        return getAffichage();
    }

    /**
     * Gets the value for display of the treatment.
     *
     * @return the value for display of the treatment
     */
    public String getAffichage() {
        return this.affichage;
    }

    /**
     * Sets the value for display of the treatment.
     *
     * @param affichage the new value for display of the treatment
     */
    public void setAffichage(final String affichage) {
        this.affichage = affichage;
    }

    /**
     * Gets the code of the treatment.
     *
     * @return the code of the treatment
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code of the treatment.
     *
     * @param code the new code of the treatment
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the begin date {@link Date} traitement of the treatment.
     *
     * @return the begin date {@link Date} traitement of the treatment
     */
    public LocalDate getDateDebutTraitement() {
        return this.dateDebutTraitement;
    }

    /**
     * Sets the begin date {@link Date} traitement of the treatment.
     *
     * @param dateDebutTraitement the new begin date {@link Date} traitement of
     *                            the treatment
     */
    public void setDateDebutTraitement(final LocalDate dateDebutTraitement) {
        this.dateDebutTraitement = dateDebutTraitement;
    }

    /**
     * Gets the end date {@link Date} of the treatment.
     *
     * @return the end date {@link Date} of the treatment
     */
    public LocalDate getDateFinTraitement() {
        return this.dateFinTraitement;
    }

    /**
     * Sets the end date {@link Date} of the treatment.
     *
     * @param dateFinTraitement the new end date {@link Date} of the treatment
     */
    public void setDateFinTraitement(final LocalDate dateFinTraitement) {
        this.dateFinTraitement = dateFinTraitement;
    }

    /**
     * Gets the description of the treatment.
     *
     * @return the description of the treatment
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description of the treatment.
     *
     * @param description the new description of the treatment
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the length of the treatment .
     *
     * @return the length of the treatment
     */
    public String getDuree() {
        return this.duree;
    }

    /**
     * Sets the length of the treatment .
     *
     * @param duree the new length of the treatment
     */
    public void setDuree(final String duree) {
        this.duree = duree;
    }

    /**
     * Gets the id {@link Long}.
     *
     * @return the id {@link Long}
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id {@link Long}.
     *
     * @param id the new id {@link Long}
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the name of the treatment.
     *
     * @return the name of the treatment
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the name of the treatment.
     *
     * @param nom the new name of the treatment
     */
    public void setNom(final String nom) {
        this.nom = nom;
        this.setCode(Utils.createCodeFromString(nom));
    }

    /**
     * Gets the site {@link SiteACBB}.
     *
     * @return the site {@link SiteACBB}
     */
    public SiteACBB getSite() {
        return this.site;
    }

    /**
     * Sets the site {@link SiteACBB}.
     *
     * @param siteACBB the new site {@link SiteACBB}
     */
    public void setSite(final SiteACBB siteACBB) {
        this.site = siteACBB;
    }

    /**
     * Gets the versions of the treatment {@link VersionDeTraitement}.
     *
     * @return the versions of the treatment {@link VersionDeTraitement}
     */
    public List<VersionDeTraitement> getVersionDeTraitements() {
        return this.versionDeTraitements;
    }

    /**
     * Sets the versions of the treatment {@link VersionDeTraitement}.
     *
     * @param versionDeTraitements the new versions of the treatment
     *                             {@link VersionDeTraitement}
     */
    public void setVersionDeTraitements(final List<VersionDeTraitement> versionDeTraitements) {
        this.versionDeTraitements = versionDeTraitements;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        result = prime * result + (this.site == null ? 0 : this.site.hashCode());
        result = prime * result + (this.code == null ? 0 : this.code.hashCode());
        return result;
    }

    /**
     * @return
     */
    protected String getAlphanumeAffichage() {
        return StringUtils.orderedString(nom);
    }

}
