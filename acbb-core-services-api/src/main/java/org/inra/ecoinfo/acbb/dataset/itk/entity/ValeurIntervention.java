/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.itk.entity;

import org.hibernate.annotations.Check;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class ValeurIntervention.
 * <p>
 * generic entity for record values
 */
@MappedSuperclass
@Check(constraints = "(valeurQualitative is null and valeur is not null) or (valeurQualitative is not null and valeur is null)")
public abstract class ValeurIntervention implements Serializable {

    /**
     * The Constant ATTRIBUTE_JPA_VALUE.
     */
    public static final String ATTRIBUTE_JPA_VALUE = "value";

    /**
     *
     */
    protected static final String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The valeur @link(float).
     */
    @Column(name = ValeurIntervention.ATTRIBUTE_JPA_VALUE)
    Float valeur;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = ListeACBB.class)
    @JoinColumn(name = ListeACBB.ID_JPA, referencedColumnName = ListeACBB.ID_JPA, nullable = true)
    IValeurQualitative valeurQualitative;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    /**
     * Instantiates a new valeur intervention.
     */
    public ValeurIntervention() {
        super();
    }

    /**
     * Instantiates a new valeur intervention.
     *
     * @param valeur   the valeur
     * @param realNode the datatype variable unite
     */
    public ValeurIntervention(final Float valeur, final RealNode realNode) {
        super();
        this.valeur = valeur;
        this.realNode = realNode;
    }

    /**
     * Instantiates a new valeur intervention.
     *
     * @param valeur   the valeur
     * @param realNode the datatype variable unite
     */
    public ValeurIntervention(final IValeurQualitative valeur,
                              final RealNode realNode) {
        super();
        this.valeurQualitative = valeur;
        this.realNode = realNode;
    }

    /**
     * Instantiates a new valeur intervention.
     *
     * @param valeurs
     * @param realNode
     * @link(List<IValeurQualitative>)@link(RealNode) the datatype variable unite
     */
    public ValeurIntervention(final List<? extends IValeurQualitative> valeurs,
                              final RealNode realNode) {
        super();
        this.setValeursQualitatives(valeurs);
        this.realNode = realNode;
    }

    /**
     * Gets the datatype RealNodevariable unite.
     *
     * @return the datatype variable unite
     */
    public RealNode getRealNode() {
        return this.realNode;
    }

    /**
     * Sets the datatype variable unite.
     *
     * @param realNode the new datatype variable unite
     */
    public void setRealNode(final RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     */
    public Float getValeur() {
        return this.valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     */
    public void setValeur(final Float valeur) {
        this.valeur = valeur;
    }

    /**
     * Gets the valeur qualitative.
     *
     * @return the valeur qualitative
     */
    public IValeurQualitative getValeurQualitative() {
        return this.valeurQualitative;
    }

    /**
     * @param valeurQualitative
     */
    public void setValeurQualitative(final IValeurQualitative valeurQualitative) {
        this.valeurQualitative = valeurQualitative;
    }

    /**
     * Gets the valeurs qualitatives.
     *
     * @return the valeurs qualitatives must be overriden if neccessary
     */
    public List<? extends IValeurQualitative> getValeursQualitatives() {
        return null;
    }

    /**
     * @param valeursQualitatives the valeursQualitatives to set must be overriden if neccessary
     */
    public void setValeursQualitatives(List<? extends IValeurQualitative> valeursQualitatives) {
    }

    private String getValeursQualitativesToString() {
        List<String> values = new LinkedList();
        for (IValeurQualitative vq : this.getValeursQualitatives()) {
            values.add(vq.getValeur());
        }
        return org.apache.commons.lang.StringUtils.join(values, ',');
    }

    /**
     * @return
     */
    public String getValeurToString() {
        if (this.isQualitative()) {
            return this.getValeursQualitatives() == null || this.getValeursQualitatives().isEmpty() ? this.valeurQualitative
                    .getValeur() : this.getValeursQualitativesToString();
        } else {
            return this.valeur.toString();
        }
    }

    /**
     * @return
     */
    public boolean isQualitative() {
        return getDatatypeVariableUnite().getVariable().getIsQualitative();
    }

    /**
     * @return
     */
    protected DatatypeVariableUniteACBB getDatatypeVariableUnite() {
        return (DatatypeVariableUniteACBB) this.realNode.getNodeable();
    }
}
