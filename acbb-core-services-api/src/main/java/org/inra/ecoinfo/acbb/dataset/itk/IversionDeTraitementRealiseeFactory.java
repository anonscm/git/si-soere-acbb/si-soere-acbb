package org.inra.ecoinfo.acbb.dataset.itk;

import org.inra.ecoinfo.acbb.dataset.itk.entity.Tempo;
import org.inra.ecoinfo.acbb.dataset.itk.entity.VersionTraitementRealisee;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.versiontraitement.VersionDeTraitement;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.acbb.utils.InfoReport;

/**
 * A factory for creating IversionDeTraitementRealisee objects.
 *
 * @param <T> the generic type
 */
public interface IversionDeTraitementRealiseeFactory<T extends ILineRecord> {

    /**
     * Gets the or create versiontraitement realise.
     *
     * @param line
     * @param errorsReport
     * @param infoReport
     * @return the or create versiontraitement realise
     * @link{T the line
     * @link{ErrorsReport the errors report
     * @link{InfoReport the info report
     * @link(T) the line
     * @link(ErrorsReport) the errors report
     * @link(InfoReport) the info report
     */
    VersionTraitementRealisee getOrCreateVersiontraitementRealise(T line,
                                                                  ErrorsReport errorsReport, InfoReport infoReport);

    /**
     * Gets the tempo.
     *
     * @param line
     * @param versionTraitement
     * @param parcelle
     * @param errorsReport
     * @param infoReport
     * @return the tempo
     * @link{T the line
     * @link{VersionDeTraitement the version traitement
     * @link{Parcelle the parcelle
     * @link{ErrorsReport the errors report
     * @link{InfoReport the info report
     * @link(T) the line
     * @link(VersionDeTraitement) the version traitement
     * @link(ErrorsReport) the errors report
     * @link(InfoReport) the info report
     */
    Tempo getTempo(T line, VersionDeTraitement versionTraitement, Parcelle parcelle,
                   ErrorsReport errorsReport, InfoReport infoReport);

}
