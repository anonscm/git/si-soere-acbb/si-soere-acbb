/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import org.inra.ecoinfo.filecomp.jsf.ItemFile;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;

/**
 * @author tcherniatinsky
 */
public interface IAssociateMAnager {

    /**
     * @param nodeIds
     * @param intervalsDate
     * @return
     */
    List<ItemFile> getFileCompFromPathesAndIntervalsDate(List<Long> nodeIds, List<IntervalDate> intervalsDate);

}
