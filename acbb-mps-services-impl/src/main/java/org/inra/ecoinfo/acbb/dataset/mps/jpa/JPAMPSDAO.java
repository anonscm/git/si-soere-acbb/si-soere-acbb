/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.jpa;

import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.acbb.dataset.mps.IMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.entity.*;
import org.inra.ecoinfo.acbb.extraction.jpa.AbstractACBBExtractionJPA;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB_;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

/**
 * The Class JPASMPDAO.
 *
 * @param <S>
 * @param <M>
 */
@SuppressWarnings({"unchecked", "rawtypes"})
abstract public class JPAMPSDAO<S extends SequenceMPS, M extends MesureMPS> extends AbstractACBBExtractionJPA<M> implements IMPSDAO<S, M> {

    /**
     * Extract org.inra.ecoinfo.acbb.dataset.smp.
     *
     * @param selectedTraitements
     * @param user
     * @param intervals
     * @param selectedProfondeurs
     * @return the list
     * @link(List<TraitementProgramme>) the selected traitements
     * @link(List<IntervalDate>) the intervals
     * @link(SortedSet<Integer>) the selected profondeurs
     */
    @Override
    public List<M> extractMPS(final List<TraitementProgramme> selectedTraitements, final List<IntervalDate> intervals, final SortedSet<Integer> selectedProfondeurs, IUser user) {
        CriteriaQuery<M> query = buildQuery(selectedTraitements, intervals, selectedProfondeurs, false, user);
        return getResultList(query);
    }

    @Override
    public Long sizeMPS(final List<TraitementProgramme> selectedTraitements, final List<IntervalDate> intervals, final SortedSet<Integer> selectedProfondeurs, IUser user)
            throws PersistenceException, BusinessException {
        CriteriaQuery<Long> query = buildQuery(selectedTraitements, intervals, selectedProfondeurs, true, user);
        return entityManager.createQuery(query).getSingleResult();
    }

    private CriteriaQuery buildQuery(final List<TraitementProgramme> selectedTraitements, final List<IntervalDate> intervals, final SortedSet<Integer> selectedProfondeurs, boolean isCount, IUser user) {
        if (selectedTraitements == null || selectedTraitements.isEmpty() || intervals == null
                || intervals.isEmpty()) {
            return null;
        }

        CriteriaQuery query = isCount ? builder.createQuery(Long.class) : builder.createQuery(getMesureEntityClass());
        Root<S> s = query.from(getSequenceEntityClass());
        Root<? extends SousSequenceMPS> ss = query.from(getSousSequenceEntityClass());
        Root<M> m = query.from(getMesureEntityClass());
        ArrayList<Predicate> and = new ArrayList();
        and.add(builder.equal(m.join("sousSequenceMPS"), ss));
        and.add(builder.equal(ss.join("sequenceMPS"), s));
        and.add(s.join(SequenceMPS_.suiviParcelle).join(SuiviParcelle_.traitement).in(selectedTraitements));
        if (!CollectionUtils.isEmpty(selectedProfondeurs)) {
            and.add(ss.get("profondeur").in(selectedProfondeurs));
        }
        for (IntervalDate intervalDate : intervals) {
            LocalDateTime minDate, maxDate;
            if (intervals.size() > 0) {
                minDate = intervalDate.getBeginDate();
                maxDate = intervalDate.getEndDate();
                and.add(builder.between(s.get(SequenceMPS_.date), minDate.toLocalDate(), maxDate.toLocalDate()));
            }
        }
        if (!user.getIsRoot()) {
            Root<? extends ValeurMPS> v = query.from(getValeurEntityClass());
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            and.add(builder.equal(v.join("mesureMPS"), v));
            and.add(builder.equal(nds.join(NodeDataSet_.realNode), v.join(ValeurMPS_.realNode)));
            addRestrictiveRequestOnRoles(user, query, and, builder, nds, s.get(SequenceMPS_.date));
        }
        query.distinct(true);
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        if (isCount) {
            return query.select(builder.count(s));
        } else {
            return query.select(m);
        }
    }

    /**
     * Gets the availables profondeurs by.
     *
     * @param selectedTraitements
     * @param user
     * @param intervals
     * @param utilisateur
     * @return the availables profondeurs by @see
     * org.inra.ecoinfo.acbb.dataset.mps.smp.ISMPDAO#getAvailablesProfondeursBy
     * (java.util.List, java.util.List,
     * org.inra.ecoinfo.identification.entity.Utilisateur)
     * @link(List<TraitementProgramme>) the selected traitements
     * @link(List<IntervalDate>) the intervals
     * @link(Utilisateur) the utilisateur
     */
    @Override
    public List<Integer> getAvailablesProfondeursBy(
            final List<TraitementProgramme> selectedTraitements, final List<IntervalDate> intervals, final IUser utilisateur, IUser user) {
        if (selectedTraitements == null || selectedTraitements.isEmpty() || intervals == null
                || intervals.isEmpty()) {
            return null;
        }

        CriteriaQuery query = builder.createQuery(Integer.class);
        Root<? extends SousSequenceMPS> ss = query.from(getSousSequenceEntityClass());
        Join<? extends SousSequenceMPS, S> s = ss.join("sequenceMPS");
        ArrayList<Predicate> and = new ArrayList();
        ArrayList<Predicate> orDate = new ArrayList();
        intervals.stream().forEach(
                i -> orDate.add(builder.between(s.get(SequenceMPS_.date), i.getBeginDate().toLocalDate(), i.getEndDate().toLocalDate()))
        );
        and.add(builder.or(orDate.toArray(new Predicate[and.size()])));
        and.add(s.join(SequenceMPS_.suiviParcelle).get(SuiviParcelle_.traitement).in(selectedTraitements));
        if (!user.getIsRoot()) {
            Join<S, VersionFile> version = s.join(SequenceMPS_.version);
            Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            and.add(builder.equal(nds.join(NodeDataSet_.realNode), dataset.get(Dataset_.realNode)));
            addRestrictiveRequestOnRoles(user, query, and, builder, nds, s.get(SequenceMPS_.date));
        }
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        query.select(ss.get("profondeur"));
        query.distinct(true);
        return getResultList(query);
    }

    /**
     * Gets the columns.
     *
     * @param selectedTraitements
     * @param intervals
     * @param selectedProfondeurs
     * @return the columns
     * @link(List<TraitementProgramme>) the selected traitements
     * @link(List<IntervalDate>) the intervals
     * @link(SortedSet<Integer>) the selected profondeurs
     */
    @Override
    public List<Tuple> getColumns(final List<TraitementProgramme> selectedTraitements,
                                  final List<IntervalDate> intervals, final SortedSet<Integer> selectedProfondeurs) {
        if (selectedTraitements == null || selectedTraitements.isEmpty() || intervals == null
                || intervals.isEmpty()) {
            return null;
        }

        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<S> s = query.from(getSequenceEntityClass());
        Root<? extends SousSequenceMPS> ss = query.from(getSousSequenceEntityClass());
        Root<M> m = query.from(getMesureEntityClass());
        Root<? extends ValeurMPS> v = query.from(getValeurEntityClass());
        ArrayList<Predicate> and = new ArrayList();
        and.add(builder.equal(ss.get("sequenceMPS"), s));
        and.add(builder.equal(m.get("sousSequenceMPS"), ss));
        and.add(builder.equal(v.get("mesureMPS"), m));
        and.add(s.get(SequenceMPS_.suiviParcelle).get(SuiviParcelle_.traitement).in(selectedTraitements));
        if (!selectedProfondeurs.isEmpty()) {
            and.add(ss.get("profondeur").in(selectedProfondeurs));
        }
        for (IntervalDate intervalDate : intervals) {
            LocalDateTime minDate, maxDate;
            if (intervals.size() > 0) {
                minDate = intervalDate.getBeginDate();
                maxDate = intervalDate.getEndDate();
                and.add(builder.between(s.get(SequenceMPS_.date), minDate.toLocalDate(), maxDate.toLocalDate()));
            }
        }
        Root<DatatypeVariableUniteACBB> dvu = query.from(DatatypeVariableUniteACBB.class);
        and.add(builder.equal(v.get(ValeurMPS_.realNode).get(RealNode_.nodeable), dvu));
        Expression<String> concat = builder.concat(dvu.get(DatatypeVariableUniteACBB_.variable).get(VariableACBB_.affichage), "_");
        concat = builder.concat(concat, ss.get("profondeur"));
        concat = builder.concat(concat, "_");
        concat = builder.concat(concat, v.get(ValeurMPS_.numRepetition).as(String.class));
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        query.multiselect(s.get(SequenceMPS_.version), concat);
        query.distinct(true);
        return getResultList(query);
    }

    /**
     * @return
     */
    abstract protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass();

    /**
     * @return
     */
    abstract protected Class<S> getSequenceEntityClass();

    /**
     * @return
     */
    abstract protected Class<? extends SousSequenceMPS> getSousSequenceEntityClass();

    /**
     * @return
     */
    abstract protected Class<M> getMesureEntityClass();

    /**
     * @return
     */
    abstract protected Class<? extends ValeurMPS> getValeurEntityClass();

    /**
     * @return
     */
    abstract protected String getVariableAffichage();

    @Override
    protected boolean isParcelleDatatype() {
        return false;
    }

}
