/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.jpa;

import org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Query;

/**
 * The Class JPAPublicationMPSDAO.
 */
public abstract class JPAPublicationMPSDAO extends JPAVersionFileDAO implements
        ILocalPublicationDAO {

    /**
     * The Constant QUERY_DELETE_SEQUENCE @link(String).
     */
    static final String QUERY_DELETE_SEQUENCE = "delete from %s sft where sft.version=:version";

    /**
     * The Constant QUERY_DELETE_SOUS_SEQUENCE @link(String).
     */
    static final String QUERY_DELETE_SOUS_SEQUENCE = "delete from %s where  sequenceMPS in ("
            + "select id from %s sft where sft.version=:version)";

    /**
     * The Constant QUERY_DELETE_MEASURE @link(String).
     */
    static final String QUERY_DELETE_MEASURE = "delete from %s where  sousSequenceMPS in ("
            + "select id from %s mft where  sequenceMPS in ("
            + "select id from %s sft where sft.version=:version))";

    /**
     * The Constant QUERY_DELETE_VALEUR @link(String).
     */
    static final String QUERY_DELETE_VALEUR = "delete from %s where mesureMPS in ("
            + "select id from %s mft where  sousSequenceMPS in ("
            + "select id from %s mft where  sequenceMPS in ("
            + "select id from %s sft where sft.version=:version)))";

    /**
     * Gets the mesure classname.
     *
     * @return the mesure classname
     */
    String getMesureClassname() {
        return "Mesure".concat(this.getVariable());
    }

    /**
     * Gets the sequence classname.
     *
     * @return the sequence classname
     */
    String getSequenceClassname() {
        return "Sequence".concat(this.getVariable());
    }

    /**
     * Gets the sous sequence classname.
     *
     * @return the sous sequence classname
     */
    String getSousSequenceClassname() {
        return "SousSequence".concat(this.getVariable());
    }

    /**
     * Gets the valeur classname.
     *
     * @return the valeur classname
     */
    String getValeurClassname() {
        return "Valeur".concat(this.getVariable());
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    protected abstract String getVariable();

    /**
     * Removes the version.
     *
     * @param version
     * @throws PersistenceException the persistence exception @see
     *                              org.inra.ecoinfo.acbb.dataset.ILocalPublicationDAO#removeVersion(org.
     *                              inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @link(VersionFile) the version
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        final String requeteDeleteValeur = String.format(JPAPublicationMPSDAO.QUERY_DELETE_VALEUR,
                this.getValeurClassname(), this.getMesureClassname(),
                this.getSousSequenceClassname(), this.getSequenceClassname());
        Query query = this.entityManager.createQuery(requeteDeleteValeur);
        query.setParameter("version", version);
        query.executeUpdate();
        final String requeteDeleteMesure = String.format(JPAPublicationMPSDAO.QUERY_DELETE_MEASURE,
                this.getMesureClassname(), this.getSousSequenceClassname(),
                this.getSequenceClassname());
        query = this.entityManager.createQuery(requeteDeleteMesure);
        query.setParameter("version", version);
        query.executeUpdate();
        final String requeteDeleteSousSequence = String.format(
                JPAPublicationMPSDAO.QUERY_DELETE_SOUS_SEQUENCE, this.getSousSequenceClassname(),
                this.getSequenceClassname());
        query = this.entityManager.createQuery(requeteDeleteSousSequence);
        query.setParameter("version", version);
        query.executeUpdate();
        final String requeteDeleteSequence = String.format(
                JPAPublicationMPSDAO.QUERY_DELETE_SEQUENCE, this.getSequenceClassname());
        query = this.entityManager.createQuery(requeteDeleteSequence);
        query.setParameter("version", version);
        query.executeUpdate();
    }

}
