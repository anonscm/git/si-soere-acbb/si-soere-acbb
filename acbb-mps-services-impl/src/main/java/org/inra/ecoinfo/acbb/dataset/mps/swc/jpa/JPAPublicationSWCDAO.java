/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.swc.jpa;


import org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAPublicationMPSDAO;

/**
 * The Class JPAPublicationSWCDAO.
 */
public class JPAPublicationSWCDAO extends JPAPublicationMPSDAO {

    /**
     * The Constant SWC_NAME_VARIABLE.
     */
    public static final String SWC_NAME_VARIABLE = "SWC";

    /**
     * Gets the variable.
     *
     * @return the variable
     * @see org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAPublicationMPSDAO#getVariable()
     */
    @Override
    protected String getVariable() {
        return JPAPublicationSWCDAO.SWC_NAME_VARIABLE;
    }

}
