/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp;

import org.inra.ecoinfo.acbb.dataset.mps.IMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.MesureSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SequenceSMP;

/**
 * The Interface ISMPDAO.
 */
@SuppressWarnings("rawtypes")
public interface ISMPDAO extends IMPSDAO<SequenceSMP, MesureSMP> {
}
