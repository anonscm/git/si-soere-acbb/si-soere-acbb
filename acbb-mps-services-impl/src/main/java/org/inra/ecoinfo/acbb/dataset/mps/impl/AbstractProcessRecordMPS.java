package org.inra.ecoinfo.acbb.dataset.mps.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.VariableValue;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.acbb.dataset.impl.CleanerValues;
import org.inra.ecoinfo.acbb.dataset.impl.EndOfCSVLine;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.mps.*;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.utils.ErrorsReport;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.Map.Entry;

/**
 * process the record of flux files data.
 *
 * @param <S>  the generic type {@link SequenceMPS}
 * @param <SS> the generic type {@link SousSequenceMPS}
 * @param <M>  the generic type {@link MesureMPS}
 * @param <V>  the value type {@link ValeurMPS}
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class
 * ProcessRecordFlux.
 */
@SuppressWarnings("rawtypes")
public abstract class AbstractProcessRecordMPS<S extends SequenceMPS, SS extends SousSequenceMPS, M extends MesureMPS, V extends ValeurMPS>
        extends AbstractProcessRecord {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The sequence mpsdao {@link ISequenceMPSDAO}.
     */
    ISequenceMPSDAO<S> sequenceMPSDAO;

    /**
     * The sous sequence mpsdao {@link ISousSequenceMPSDAO}.
     */
    ISousSequenceMPSDAO<S, SS> sousSequenceMPSDAO;

    /**
     * The mesure mpsdao {@link IMesureMPSDAO}.
     */
    IMesureMPSDAO<SS, M> mesureMPSDAO;

    /**
     * The version file helper resolver @link(IVersionFileHelperResolver).
     */
    IVersionFileHelperResolver versionFileHelperResolver;

    /**
     * Instantiates a new process record flux.
     */
    public AbstractProcessRecordMPS() {
        super();
    }

    /**
     * Builds the variable header and skip header.
     *
     * @param parser            the parser
     * @param datasetDescriptor
     * @return the variable acbb
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     */
    protected Optional<VariableACBB> buildVariableHeaderAndSkipHeader(final CSVParser parser,
                                                                      final DatasetDescriptorACBB datasetDescriptor) throws IOException {
        for (int i = 0; i < datasetDescriptor.getEnTete(); i++) {
            parser.getLine();
        }
        return this.variableDAO.getByAffichage(this.getVariableName(datasetDescriptor)).map(v -> (VariableACBB) v);
    }

    /**
     * Gets the variable name.
     *
     * @param datasetDescriptor
     * @return the variable name
     * @link(DatasetDescriptor) the dataset descriptor
     * @link(DatasetDescriptor) the dataset descriptor
     * @link(DatasetDescriptor) the dataset descriptor
     */
    protected String getVariableName(final DatasetDescriptor datasetDescriptor) {
        return datasetDescriptor == null ? null : ((DatasetDescriptorACBB) datasetDescriptor)
                .getVariableName();
    }

    /**
     * New mesure instance.
     *
     * @param sousSequence
     * @param time
     * @param originalLineNumber <int> the original line number
     * @return the new builded mesure {@link SousSequenceMPS} the sous sequence {@link Date} the
     * time
     * @link(SS) the sous sequence
     * @link(Date) the time
     */
    public abstract M newMesureInstance(SS sousSequence, LocalTime time, int originalLineNumber);

    /**
     * New sequence instance.
     *
     * @param version
     * @param suiviParcelle
     * @param date
     * @return the new builded sequence {@link VersionFile} the version {@link SuiviParcelle} the
     * suivi parcelle {@link Date} the date
     * @link(VersionFile) the version
     * @link(SuiviParcelle) the suivi parcelle
     * @link(Date) the date
     */
    public abstract S newSequenceInstance(VersionFile version, SuiviParcelle suiviParcelle,
                                          LocalDate date);

    /**
     * New sous sequence instance.
     *
     * @param sequence
     * @param profondeur <int> the profondeur
     * @return the new builded sous sequence {@link SequenceMPS} the sequence
     * @link(S) the sequence
     */
    public abstract SS newSousSequenceInstance(S sequence, int profondeur);

    /**
     * New valeur instance.
     *
     * @param realNode
     * @param value
     * @param qualityClass
     * @param numRepetition <int> the num repetition
     * @param mesure
     * @return the nw builded valeur {@link VariableACBB} the variable {@link Float} the value
     * {@link Integer} the quality class
     * @link(VariableACBB) the variable
     * @link(Float) the value
     * @link(Integer) the quality class
     */
    public abstract V newValeurInstance(RealNode realNode, Float value, Integer qualityClass,
                                        int numRepetition, M mesure);

    /**
     * Process record.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException the business exception
     * @link(CSVParser)
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the file encoding
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     * org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
                              final IRequestPropertiesACBB requestProperties, final String fileEncoding,
                              final DatasetDescriptorACBB datasetDescriptor) throws BusinessException {
        VersionFile finalVersionFile = versionFile;
        super.processRecord(parser, finalVersionFile, requestProperties, fileEncoding,
                datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        final IRequestPropertiesMPS requestPropertiesMPS = (IRequestPropertiesMPS) requestProperties;
        Map<Long, Map<String, Parcelle>> dbParcelle = new HashMap();
        Map<Long, Map<String, TraitementProgramme>> dbTraitements = new HashMap();
        Map<Parcelle, Map<LocalDate, SuiviParcelle>> dbSuiviParcelle = new HashMap();
        try {
            // Création du fichier de dépôt en base

            String[] values = null;
            final VariableACBB dbVariable = this.buildVariableHeaderAndSkipHeader(parser,
                    datasetDescriptor).orElse(null);
            finalVersionFile = this.versionFileDAO.merge(finalVersionFile);
            final Map<SuiviParcelle, Map<LocalDate, List<LineRecord>>> lines = new HashMap();
            Parcelle parcelle;
            TraitementProgramme traitement;
            SuiviParcelle suiviParcelle = null;
            long lineCount = datasetDescriptor.getEnTete();
            while ((values = parser.getLine()) != null) {
                lineCount++;
                final List<VariableValueMPS> variablesValues = new LinkedList();
                final CleanerValues cleanerValues = new CleanerValues(values);
                int index = 0;
                // On parcourt chaque colonne d'une ligne
                parcelle = this.readParcelle(requestPropertiesMPS, cleanerValues, errorsReport,
                        lineCount, index, dbParcelle);
                traitement = this.readTraitement(requestPropertiesMPS, cleanerValues, errorsReport,
                        lineCount, index, dbTraitements);
                final LocalDateTime datetime;
                try {
                    String dateString = cleanerValues.nextToken();
                    String timeString = cleanerValues.nextToken();
                    datetime = RecorderACBB.getDateTimeLocalFromDateStringaAndTimeString(dateString, timeString);
                } catch (BusinessException e) {
                    errorsReport.addErrorMessage(e.getMessage(), e);
                    continue;
                }
                suiviParcelle = this.retrieveSuiviParcelle(errorsReport, parcelle, traitement,
                        lineCount, datetime.toLocalDate(), dbSuiviParcelle);
                VariableValueMPS variableValue = null;
                ExpectedColumn column;
                for (int i = datasetDescriptor.getUndefinedColumn(); requestPropertiesMPS
                        .getExpectedColums().containsKey(i)
                        && cleanerValues.currentTokenIndex() < values.length; i++) {
                    column = requestPropertiesMPS.getExpectedColums().get(i);
                    if (!RecorderACBB.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())) {
                        continue;
                    }
                    variableValue = new VariableValueMPS(cleanerValues.nextToken(), column);
                    if (column.hasQualityClass()) {
                        final String qualityClass = cleanerValues.nextToken();
                        if (!Strings.isNullOrEmpty(qualityClass)) {
                            variableValue.setQualityClass(Integer.parseInt(qualityClass));
                        }
                        i++;
                    }
                    variablesValues.add(variableValue);
                }
                if (!errorsReport.hasErrors()) {
                    LocalDate date;
                    LocalTime time;
                    date = datetime.toLocalDate();
                    time = datetime.toLocalTime();
                    final LineRecord line = new LineRecord(date, time, variablesValues,
                            (int) lineCount, suiviParcelle);
                    if (!lines.containsKey(suiviParcelle)) {
                        lines.put(suiviParcelle, new HashMap<>());
                    }
                    if (!lines.get(suiviParcelle).containsKey(date)) {
                        lines.get(suiviParcelle).put(date,
                                new LinkedList<>());
                    }
                    lines.get(suiviParcelle).get(date).add(line);
                }
            }
            Map<Variable, RealNode> dvus = datatypeVariableUniteACBBDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
            RealNode realNode = dvus.get(dbVariable);
            BuildMesuresHelper buildMesuresHelper = new BuildMesuresHelper(finalVersionFile,
                    realNode, lines, errorsReport);
            buildMesuresHelper.build();
            if (errorsReport.hasErrors()) {
                AbstractOutputBuilder.LOGGER.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | NumberFormatException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Read parcelle.
     *
     * @param requestPropertiesMPS
     * @param cleanerValues
     * @param errorsReport
     * @param lineCount
     * @param dbParcelle
     * @return the parcelle
     * @throws PersistenceException the persistence exception
     * @link(IRequestPropertiesMPS) the request properties mps
     * @link(CleanerValues) the cleaner values
     * @link(ErrorsReport) the errors report
     * @link(long) the line count
     * @link(IRequestPropertiesMPS) the request properties mps
     * @link(CleanerValues) the cleaner values
     * @link(ErrorsReport) the errors report
     * @link(long) the line count
     */
    protected Parcelle readParcelle(final IRequestPropertiesMPS requestPropertiesMPS,
                                    final CleanerValues cleanerValues, final ErrorsReport errorsReport,
                                    final long lineCount, int index, Map<Long, Map<String, Parcelle>> dbParcelle)
            throws PersistenceException {
        Parcelle parcelle = null;
        try {
            String nomParcelle = null;
            try {
                nomParcelle = Utils.createCodeFromString(cleanerValues.nextToken());
            } catch (EndOfCSVLine ex) {
                errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
            }
            if (dbParcelle.containsKey(requestPropertiesMPS.getSite().getId())
                    && dbParcelle.get(requestPropertiesMPS.getSite().getId()).containsKey(
                    nomParcelle)) {
                return dbParcelle.get(requestPropertiesMPS.getSite().getId()).get(nomParcelle);
            }
            parcelle = this.parcelleDAO.getByNKey(Parcelle.getCodeFromNameAndSite(nomParcelle, requestPropertiesMPS.getSite())).orElse(null);
            if (parcelle == null) {
                errorsReport.addErrorMessage(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_PLOT),
                        nomParcelle, lineCount, 1, requestPropertiesMPS.getSite().getName()));
            }
            if (!dbParcelle.containsKey(requestPropertiesMPS.getSite().getId())) {
                dbParcelle.put(requestPropertiesMPS.getSite().getId(),
                        new HashMap<>());
            }
            dbParcelle.get(requestPropertiesMPS.getSite().getId()).put(nomParcelle, parcelle);
        } catch (final IndexOutOfBoundsException e) {
            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_PLOT), lineCount,
                    1));
        }
        return parcelle;
    }

    /**
     * Read traitement.
     *
     * @param requestPropertiesMPS
     * @param cleanerValues
     * @param errorsReport
     * @param lineCount
     * @param dbTraitements
     * @return the traitement programme
     * @throws PersistenceException the persistence exception
     * @link(IRequestPropertiesMPS) the request properties mps
     * @link(CleanerValues) the cleaner values
     * @link(ErrorsReport) the errors report
     * @link(long) the line count
     * @link(IRequestPropertiesMPS) the request properties mps
     * @link(CleanerValues) the cleaner values
     * @link(ErrorsReport) the errors report
     * @link(long) the line count
     */
    protected TraitementProgramme readTraitement(final IRequestPropertiesMPS requestPropertiesMPS,
                                                 final CleanerValues cleanerValues, final ErrorsReport errorsReport,
                                                 final long lineCount, int index, Map<Long, Map<String, TraitementProgramme>> dbTraitements)
            throws PersistenceException {
        TraitementProgramme traitement;
        String nomTraitement = null;
        try {
            try {
                nomTraitement = Utils.createCodeFromString(cleanerValues.nextToken());
            } catch (EndOfCSVLine ex) {
                errorsReport.addErrorMessage(ex.setMessage(lineCount, index).getMessage());
            }
            if (dbTraitements.containsKey(requestPropertiesMPS.getSite().getId())
                    && dbTraitements.get(requestPropertiesMPS.getSite().getId()).containsKey(
                    nomTraitement)) {
                return dbTraitements.get(requestPropertiesMPS.getSite().getId()).get(nomTraitement);
            }
            traitement = this.traitementDAO.getByNKey(requestPropertiesMPS.getSite().getId(),
                    nomTraitement).orElse(null);
            if (traitement == null) {
                errorsReport.addErrorMessage(String.format(
                        RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_TRAITEMENT),
                        nomTraitement, lineCount, 2, requestPropertiesMPS.getSite().getName()));
            }
            if (!dbTraitements.containsKey(requestPropertiesMPS.getSite().getId())) {
                dbTraitements.put(requestPropertiesMPS.getSite().getId(),
                        new HashMap<>());
            }
            dbTraitements.get(requestPropertiesMPS.getSite().getId())
                    .put(nomTraitement, traitement);
        } catch (final IndexOutOfBoundsException e) {
            errorsReport.addErrorMessage(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_TREATMENT),
                    lineCount, 1));
            return null;
        }
        return traitement;
    }

    private SuiviParcelle retrieveSuiviParcelle(final ErrorsReport errorsReport, Parcelle parcelle,
                                                TraitementProgramme traitement, long lineCount, final LocalDate date,
                                                Map<Parcelle, Map<LocalDate, SuiviParcelle>> dbSuiviParcelle) {
        SuiviParcelle suiviParcelle;
        if (dbSuiviParcelle.containsKey(parcelle)
                && dbSuiviParcelle.get(parcelle).containsKey(date)) {
            return dbSuiviParcelle.get(parcelle).get(date);
        }
        suiviParcelle = this.suiviParcelleDAO.retrieveSuiviParcelle(parcelle, date).orElse(null);
        if (suiviParcelle == null) {
            errorsReport
                    .addErrorMessage(String.format(
                            RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISSING_MONITORING_PLOT_WITH_LINE_NUMBER),
                            lineCount, parcelle.getName(), traitement.getAffichage()));
        }
        if (!dbSuiviParcelle.containsKey(parcelle)) {
            dbSuiviParcelle.put(parcelle, new HashMap<>());
        }
        dbSuiviParcelle.get(parcelle).put(date, suiviParcelle);
        return suiviParcelle;
    }

    /**
     * Sets the mesure mpsdao {@link IMesureMPSDAO}.
     *
     * @param mesureMPSDAO the new mesure mpsdao {@link IMesureMPSDAO}
     */
    public void setMesureMPSDAO(final IMesureMPSDAO<SS, M> mesureMPSDAO) {
        this.mesureMPSDAO = mesureMPSDAO;
    }

    /**
     * Sets the sequence mpsdao {@link ISequenceMPSDAO}.
     *
     * @param sequenceMPSDAO the new sequence mpsdao {@link ISequenceMPSDAO}
     */
    public void setSequenceMPSDAO(final ISequenceMPSDAO<S> sequenceMPSDAO) {
        this.sequenceMPSDAO = sequenceMPSDAO;
    }

    /**
     * Sets the sous sequence mpsdao {@link ISousSequenceMPSDAO}.
     *
     * @param sousSequenceMPSDAO the new sous sequence mpsdao
     *                           {@link ISousSequenceMPSDAO}
     */
    public void setSousSequenceMPSDAO(final ISousSequenceMPSDAO<S, SS> sousSequenceMPSDAO) {
        this.sousSequenceMPSDAO = sousSequenceMPSDAO;
    }

    /**
     * Sets the version file helper resolver @link(IVersionFileHelperResolver).
     *
     * @param versionFileHelperResolver the new version file helper resolver
     * @link(IVersionFileHelperResolver)
     */
    public void setVersionFileHelperResolver(
            final IVersionFileHelperResolver versionFileHelperResolver) {
        this.versionFileHelperResolver = versionFileHelperResolver;
    }

    /**
     * The Class buildMesuresHelper.
     * <p>
     * used for building the data from file
     */
    protected class BuildMesuresHelper {

        /**
         * The lines.
         */
        Map<SuiviParcelle, Map<LocalDate, List<LineRecord>>> lines;
        /**
         * The errors report.
         */
        ErrorsReport errorsReport;
        /**
         * The version.
         */
        VersionFile version;
        /**
         * The variable.
         */
        RealNode realNode;

        /**
         * Instantiates a new builds the mesures helper.
         * <p>
         * from the List<LineRecord> lines} makes record in db
         *
         * @param version
         * @param variable
         * @param lines2
         * @param errorsReport
         * @link(VersionFile) the version
         * @link(VariableACBB) the variable
         * @link(List<LineRecord>) the lines
         * @link(ErrorsReport) the errors report {@link VersionFile} the version
         * {@link VariableACBB} the variable {@link List<LineRecord>
         * lines} the lines {@link ErrorsReport} the errors report
         */
        BuildMesuresHelper(final VersionFile version, final RealNode realNode,
                           final Map<SuiviParcelle, Map<LocalDate, List<LineRecord>>> lines2,
                           final ErrorsReport errorsReport) {
            super();
            this.version = version;
            this.realNode = realNode;
            this.lines = lines2;
            this.errorsReport = errorsReport;
            this.build();
        }

        /**
         * Adds the error for insertion mesure.
         *
         * @param mesure
         * @link(M) the mesure {@link MesureMPS}) the mesure
         */
        void addErrorForInsertionMesure(final M mesure) {
            try {
                final Object[] doublon = mesureMPSDAO.getLinePublicationNameDoublon(mesure);
                final VersionFile versionInDB = (VersionFile) doublon[0];
                final IVersionFileHelper versionFileHelper = versionFileHelperResolver
                        .resolveByDatatype(versionInDB.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getNodeable().getCode());
                final String filename = versionFileHelper.buildDownloadFilename(versionInDB);
                this.errorsReport
                        .addErrorMessage(String.format(
                                RecorderACBB
                                        .getACBBMessage(RecorderACBB.PROPERTY_MSG_DOUBLON_DATE_TIME_FOLLOW_VARIABLE_DEPTH),
                                mesure.getLineNumber(), mesure.getSousSequenceMPS()
                                        .getSequenceMPS().getDate(), mesure.getSousSequenceMPS()
                                        .getProfondeur(),
                                mesure.getSousSequenceMPS().getSequenceMPS().getSuiviParcelle()
                                        .getParcelle().getName(), mesure.getSousSequenceMPS()
                                        .getSequenceMPS().getSuiviParcelle().getTraitement()
                                        .getNom(), mesure.getHeure(), filename,
                                ((Number) doublon[1]).intValue()));
            } catch (final NoVersionFileHelperResolvedException z) {
                this.errorsReport.addErrorMessage(String.format(RecorderACBB
                        .getACBBMessage(RecorderACBB.PROPERTY_MSG_ERROR_INSERTION_MEASURE), mesure
                        .getLineNumber()));
            }
        }

        /**
         * Builds the db data.
         */
        @SuppressWarnings(value = "unchecked")
        void build() {
            S sequence = null;
            SS sousSequence;
            V valeur;
            M mesure;
            Iterator<Entry<SuiviParcelle, Map<LocalDate, List<LineRecord>>>> linesSuiviParcelleIterator = this.lines
                    .entrySet().iterator();
            int i = 0;
            while (linesSuiviParcelleIterator.hasNext()) {
                if (++i % 50 == 0) {
                    try {
                        sequenceMPSDAO.flush();
                        version = versionFileDAO.merge(version);
                        datatypeVariableUniteACBBDAO.mergeRealNode(realNode);
                    } catch (PersistenceException e) {
                        AbstractOutputBuilder.LOGGER.debug("can't persit seqence", e);
                    }
                }
                Entry<SuiviParcelle, Map<LocalDate, List<LineRecord>>> linesSuiviParcelleEntry = linesSuiviParcelleIterator
                        .next();
                SuiviParcelle suiviParcelle = linesSuiviParcelleEntry.getKey();
                Iterator<Entry<LocalDate, List<LineRecord>>> linesDateIterator = linesSuiviParcelleEntry
                        .getValue().entrySet().iterator();
                while (linesDateIterator.hasNext()) {
                    final Entry<LocalDate, List<LineRecord>> linesDateEntry = linesDateIterator.next();
                    LocalDate date = linesDateEntry.getKey();
                    sequence = this.buildSequence(suiviParcelle, date);
                    Iterator<LineRecord> lineIterator = linesDateEntry.getValue().iterator();
                    while (lineIterator.hasNext()) {
                        LineRecord line = lineIterator.next();
                        for (final VariableValueMPS variableValue : line.getVariablesValues()) {
                            if (org.apache.commons.lang.StringUtils.isEmpty(variableValue.getValue())) {
                                continue;
                            }
                            sousSequence = this.getOrBuidSousSequence(sequence,
                                    variableValue.column.getProfondeur());
                            mesure = this.getOrBuidMesure(sousSequence, line.time,
                                    line.originalLineNumber);
                            valeur = newValeurInstance(realNode,
                                    Float.parseFloat(variableValue.getValue()),
                                    variableValue.getQualityClass(), variableValue.getColumn()
                                            .getNumeroRepetition(), mesure);
                            mesure.getValeursMPS().add(valeur);
                            /*
                             * try { mesureMPSDAO.saveOrUpdate(mesure); } catch
                             * (PersistenceException e) {
                             * errorsReport.addErrorMessage("erreur ligne " +
                             * line.originalLineNumber); }
                             */
                        }
                        lineIterator.remove();
                    }
                    try {
                        sequenceMPSDAO.saveOrUpdate(sequence);
                    } catch (PersistenceException e) {
                        AbstractOutputBuilder.LOGGER.debug("can't persit seqence", e);
                    }
                    linesDateIterator.remove();
                }
                linesSuiviParcelleIterator.remove();
            }
        }

        /**
         * Gets or build the sequence.
         *
         * @param suiviParcelle
         * @param date
         * @return the or build sequence
         * @link(SuiviParcelle) the suivi parcelle
         * @link(Date) the date
         * @link(SuiviParcelle) the suivi parcelle
         * @link(Date) the date
         */
        S buildSequence(SuiviParcelle suiviParcelle, final LocalDate date) {
            try {
                suiviParcelle = suiviParcelleDAO.merge(suiviParcelle);
            } catch (PersistenceException e) {
                AbstractOutputBuilder.LOGGER.error("unable to merge object", e);
            }
            S newSequenceInstance;
            try {
                newSequenceInstance = newSequenceInstance(version, suiviParcelle, date);
            } catch (DateTimeException e) {
                AbstractOutputBuilder.LOGGER.error("can't switch date timezone, e");
                return null;
            }
            return newSequenceInstance;
        }

        /**
         * Gets or buid the mesure.
         *
         * @param sousSequence
         * @param time
         * @param originalLineNumber <int> the original line number
         * @return the or buid mesure {@link SousSequenceMPS} the sous sequence
         * @link(SS) the sous sequence
         * @link(Date) the time
         * @link(Date) the time
         */
        @SuppressWarnings(value = "unchecked")
        M getOrBuidMesure(final SS sousSequence, final LocalTime time, final int originalLineNumber) {
            final List<M> mesures = sousSequence.getMesuresMPS();
            LocalTime localTime = null;
            try {
                localTime = time;
            } catch (DateTimeException e1) {
                AbstractOutputBuilder.LOGGER.error("can't switch time timezone", e1);
            }
            for (final M mesure : mesures) {
                if (mesure.getHeure().equals(localTime)) {
                    return mesure;
                }
            }
            return newMesureInstance(sousSequence, localTime, originalLineNumber);
        }

        /**
         * Gets or buid the sous sequence.
         *
         * @param sequence
         * @param profondeur <int> the profondeur
         * @return the or buid sous sequence {@link SequenceMPS} the sequence
         * @link(S) the sequence
         */
        @SuppressWarnings(value = "unchecked")
        SS getOrBuidSousSequence(final S sequence, final int profondeur) {
            final List<SS> sousSequences = sequence.getSousSequencesMPS();
            for (final SS sousSequence : sousSequences) {
                if (sousSequence.getProfondeur() == profondeur) {
                    return sousSequence;
                }
            }
            return newSousSequenceInstance(sequence, profondeur);
        }
    }

    /**
     * The Class LineRecord.
     * <p>
     * record one line of flux data
     */
    class LineRecord implements Comparable<LineRecord> {

        /**
         * The date @link(Date).
         */
        LocalDate date;
        /**
         * The time @link(Date).
         */
        LocalTime time;
        /**
         * The original line number @link(int).
         */
        int originalLineNumber;
        /**
         * The variables values @link(List<VariableValueMPS>).
         */
        List<VariableValueMPS> variablesValues;
        /**
         * The suivi parcelle @link(SuiviParcelle).
         */
        SuiviParcelle suiviParcelle;

        /**
         * Instantiates a new line record.
         *
         * @param date
         * @param time
         * @param variablesValues
         * @param originalLineNumber <int> the original line number
         * @param suiviParcelle
         * @link(Date) the date
         * @link(Date) the time
         * @link(List<VariableValueMPS>) the variables values
         * @link(SuiviParcelle) the suivi parcelle {@link Date} the date {@link Date} the time
         * {@link List <VariableValueMPS>} the variables values null null null
         * null null null null null {@link SuiviParcelle} the suivi parcelle
         */
        LineRecord(final LocalDate date, final LocalTime time, final List<VariableValueMPS> variablesValues,
                   final int originalLineNumber, final SuiviParcelle suiviParcelle) {
            super();

            this.date = date;
            this.time = time;
            this.variablesValues = variablesValues;
            this.originalLineNumber = originalLineNumber;
            this.suiviParcelle = suiviParcelle;
        }

        /**
         * Compare to.
         *
         * @param o
         * @return the int
         * @link(LineRecord) the o
         * @link(LineRecord) the o
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        @Override
        public int compareTo(final LineRecord o) {
            if (this.originalLineNumber == o.getOriginalLineNumber()) {
                return 0;
            }
            return this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }

        /**
         * Copy.
         *
         * @param line
         * @link(LineRecord) the line {@link LineRecord} the line
         */
        public void copy(final LineRecord line) {
            this.date = line.getDate();
            this.time = line.getTime();
            this.variablesValues = line.getVariablesValues();
            this.originalLineNumber = line.getOriginalLineNumber();
            this.suiviParcelle = line.getSuiviParcelle();
        }

        /**
         * Equals.
         *
         * @param obj
         * @return true, if successful
         * @link(Object) the obj
         * @link(Object) the obj
         * @link(Object) the obj
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            @SuppressWarnings("unchecked") final LineRecord other = (LineRecord) obj;
            if (this.date == null) {
                if (other.date != null) {
                    return false;
                }
            } else if (!this.date.equals(other.date)) {
                return false;
            }
            if (this.originalLineNumber != other.originalLineNumber) {
                return false;
            }
            if (this.suiviParcelle == null) {
                if (other.suiviParcelle != null) {
                    return false;
                }
            } else if (!this.suiviParcelle.equals(other.suiviParcelle)) {
                return false;
            }
            if (this.time == null) {
                if (other.time != null) {
                    return false;
                }
            } else if (!this.time.equals(other.time)) {
                return false;
            }
            if (this.variablesValues == null) {
                return other.variablesValues == null;
            } else return this.variablesValues.equals(other.variablesValues);
        }

        /**
         * Gets the date.
         *
         * @return the date
         */
        public LocalDate getDate() {
            return this.date;
        }

        /**
         * Gets the original line number.
         *
         * @return the original line number
         */
        public int getOriginalLineNumber() {
            return this.originalLineNumber;
        }

        /**
         * Gets the suivi parcelle.
         *
         * @return the suivi parcelle
         */
        public SuiviParcelle getSuiviParcelle() {
            return this.suiviParcelle;
        }

        /**
         * Sets the suivi parcelle.
         *
         * @param suiviParcelle the new suivi parcelle @link(SuiviParcelle)
         *                      {@link SuiviParcelle} the new suivi parcelle
         */
        public void setSuiviParcelle(final SuiviParcelle suiviParcelle) {
            this.suiviParcelle = suiviParcelle;
        }

        /**
         * Gets the time.
         *
         * @return the time
         */
        public LocalTime getTime() {
            return this.time;
        }

        /**
         * Sets the time.
         *
         * @param time the new time @link(Date) {@link Date} the new time
         */
        public void setTime(final LocalTime time) {
            this.time = time;
        }

        /**
         * Gets the variables values.
         *
         * @return the variables values
         */
        public List<VariableValueMPS> getVariablesValues() {
            return this.variablesValues;
        }

        /**
         * Sets the variables values.
         *
         * @param variablesValues the new variables values
         * @link(List<VariableValueMPS>) {@link VariableValue} the new variables values
         */
        public void setVariablesValues(final List<VariableValueMPS> variablesValues) {
            this.variablesValues = variablesValues;
        }

        /**
         * Hash code.
         *
         * @return the int
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (this.date == null ? 0 : this.date.hashCode());
            result = prime * result + this.originalLineNumber;
            result = prime * result
                    + (this.suiviParcelle == null ? 0 : this.suiviParcelle.hashCode());
            result = prime * result + (this.time == null ? 0 : this.time.hashCode());
            result = prime * result
                    + (this.variablesValues == null ? 0 : this.variablesValues.hashCode());
            return result;
        }
    }

    /**
     * The Class VariableValueMPS.
     * <p>
     * the value for une cell data
     */
    class VariableValueMPS extends VariableValue {

        /**
         * The column @link(ExpectedColumn).
         */
        ExpectedColumn column;

        /**
         * Instantiates a new variable value mps.
         *
         * @param value
         * @param expectedColumn
         * @link(String) the value
         * @link(ExpectedColumn) the expected column {@link String} the value {@link ExpectedColumn}
         * the expected column
         */
        VariableValueMPS(final String value, final ExpectedColumn expectedColumn) {
            super(value);
            this.column = expectedColumn;
        }

        /**
         * Gets the column.
         *
         * @return the column
         */
        public ExpectedColumn getColumn() {
            return this.column;
        }

        /**
         * Sets the column.
         *
         * @param column the new column @link(ExpectedColumn)
         *               {@link ExpectedColumn} the new column
         */
        public void setColumn(final ExpectedColumn column) {
            this.column = column;
        }
    }

}
