/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalTime;
import java.util.Optional;

/**
 * The Class JPAMesureSMPDAO.
 */
public class JPAMesureSMPDAO extends AbstractJPADAO<MesureSMP> implements
        IMesureMPSDAO<SousSequenceSMP, MesureSMP> {

    /**
     * Gets the by n key.
     *
     * @param sousSequence
     * @param heure
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO#getByNKey(org.inra.ecoinfo
     * .acbb.dataset.mps.entity.SousSequenceMPS, java.time.LocalDateTime)
     * @link(SousSequenceSMP) the sous sequence
     * @link(Date) the heure
     */
    @Override
    public Optional<MesureSMP> getByNKey(final SousSequenceSMP sousSequence, final LocalTime heure) {
        CriteriaQuery<MesureSMP> query = builder.createQuery(MesureSMP.class);
        Root<MesureSMP> m = query.from(MesureSMP.class);
        query.where(
                builder.equal(m.join(MesureSMP_.sousSequenceMPS), sousSequence),
                builder.equal(m.get(MesureSMP_.heure), heure)
        );
        query.select(m);
        return getOptional(query);
    }

    /**
     * Gets the line publication name doublon.
     *
     * @param mesure
     * @return the line publication name doublon @see
     * org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO#getLinePublicationNameDoublon
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS)
     * @link(MesureSMP) the mesure
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final MesureSMP mesure) {
        this.beginNewTransaction();
        CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
        Root<MesureSMP> m = query.from(MesureSMP.class);
        query.where(
                builder.equal(m.join(MesureSMP_.sousSequenceMPS).join(SousSequenceSMP_.sequenceMPS).join(SequenceSMP_.suiviParcelle), mesure.getSousSequenceMPS().getSequenceMPS().getSuiviParcelle()),
                builder.equal(m.join(MesureSMP_.heure), mesure.getHeure()),
                builder.equal(m.join(MesureSMP_.sousSequenceMPS).get(SousSequenceSMP_.profondeur), mesure.getSousSequenceMPS().getProfondeur()),
                builder.equal(m.join(MesureSMP_.sousSequenceMPS).join(SousSequenceSMP_.sequenceMPS).get(SequenceSMP_.date), mesure.getSousSequenceMPS().getSequenceMPS().getDate())
        );
        query.multiselect(m.join(MesureSMP_.sousSequenceMPS).join(SousSequenceSMP_.sequenceMPS).join(SequenceSMP_.version).join(VersionFile_.dataset).get(Dataset_.id), m.get(MesureSMP_.lineNumber));
        return getFirstOrNull(query);
    }
}
