/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.ts.jpa;


import org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAPublicationMPSDAO;

/**
 * The Class JPAPublicationTSDAO.
 */
public class JPAPublicationTSDAO extends JPAPublicationMPSDAO {

    /**
     * The Constant TS_NAME_VARIABLE.
     */
    public static final String TS_NAME_VARIABLE = "TS";

    /**
     * Gets the variable.
     *
     * @return the variable
     * @see org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAPublicationMPSDAO#getVariable()
     */
    @Override
    protected String getVariable() {
        return JPAPublicationTSDAO.TS_NAME_VARIABLE;
    }
}
