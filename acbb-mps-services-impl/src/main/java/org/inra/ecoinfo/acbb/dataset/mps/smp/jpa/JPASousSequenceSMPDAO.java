/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.ISousSequenceMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SequenceSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SousSequenceSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SousSequenceSMP_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPASousSequenceSMPDAO.
 */
public class JPASousSequenceSMPDAO extends AbstractJPADAO<SousSequenceSMP> implements
        ISousSequenceMPSDAO<SequenceSMP, SousSequenceSMP> {
    /**
     * Gets the by nkey.
     *
     * @param sequence
     * @param profondeur
     * @return the by nkey @see
     * org.inra.ecoinfo.acbb.dataset.mps.ISousSequenceMPSDAO#getByNkey(org.inra
     * .ecoinfo.acbb.dataset.mps.entity.SequenceMPS, int)
     * @link(SequenceSMP) the sequence
     * @link(int) the profondeur
     */
    @Override
    public Optional<SousSequenceSMP> getByNkey(final SequenceSMP sequence, final int profondeur) {
        CriteriaQuery<SousSequenceSMP> query = builder.createQuery(SousSequenceSMP.class);
        Root<SousSequenceSMP> sssmp = query.from(SousSequenceSMP.class);
        query.where(
                builder.equal(sssmp.join(SousSequenceSMP_.sequenceMPS), sequence),
                builder.equal(sssmp.get(SousSequenceSMP_.profondeur), profondeur)
        );
        query.select(sssmp);
        return getOptional(query);
    }
}
