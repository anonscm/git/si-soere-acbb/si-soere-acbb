/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps.impl;

import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * The Class MPSOutputClassicDisplay.
 */
public class MPSOutputClassicDisplay extends MPSOutputsBuildersResolver {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.mps.messages";
    /**
     * The Constant PROPERTY_MSG_OUTPUT_HEADER.
     */
    protected static final String PROPERTY_MSG_OUTPUT_HEADER = "PROPERTY_MSG_OUTPUT_HEADER";
    /**
     * The Constant PROPERTY_MSG_OUTPUT_HEADER2.
     */
    protected static final String PROPERTY_MSG_OUTPUT_HEADER2 = "PROPERTY_MSG_OUTPUT_HEADER2";
    /**
     * The Constant CST_SEMICOLON @link(String).
     */
    static final String CST_SEMICOLON = ";";
    /**
     * The Constant PATTERN_3_EMPTY_FIELDS @link(String).
     */
    static final String PATTERN_3_EMPTY_FIELDS = ";;;";
    /**
     * The Constant PATTERN_3_FIELDS_NA @link(String).
     */
    static final String PATTERN_3_FIELDS_NA = String.format(";%s;%s;%s", ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE, ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE, ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE);
    /**
     * The Constant PATTERN_FIELD_QC @link(String).
     */
    static final String PATTERN_FIELD_QC = ";qc";
    /**
     * The Constant CST_MPS @link(String).
     */
    static final String CST_MPS = "mps";
    private static final Logger LOGGER = LoggerFactory.getLogger(MPSOutputClassicDisplay.class
            .getName());

    /**
     * Instantiates a new mPS output classic display.
     */
    public MPSOutputClassicDisplay() {
        super();
    }

    /**
     * Builds the body.
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java
     *                           .lang.String, java.util.Map, java.util.Map)
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, File> buildBody(final String headers,
                                          final Map<String, List> resultsDatasMap, final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Builds the header.
     *
     * @param outputHelper
     * @link(BuildOutputHelper) the output helper
     * @see org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver# buildHeader
     * (org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver
     * .BuildOutputHelper)
     */

    @Override
    void buildHeader(final BuildOutputHelper outputHelper) {
        if (outputHelper.headers != null && !outputHelper.headers.isEmpty()) {
            outputHelper.printStream.print(this.localizationManager.getMessage(
                    MPSOutputEuropeanDisplay.BUNDLE_SOURCE_PATH,
                    MPSOutputEuropeanDisplay.PROPERTY_MSG_OUTPUT_HEADER));
        }
        /*
        final SortedSet<ColumnHeader> columnsHeaders = outputHelper.columnsHeader;
        Iterator<ColumnHeader> itColumn = columnsHeaders.iterator();
        ColumnHeader columnHeader;
        while (itColumn.hasNext()) {
            columnHeader = itColumn.next();
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD,
                    columnHeader.toString());
            if (outputHelper.hasQualityClass.containsKey(columnHeader.toString())) {
                outputHelper.printStream.print(MPSOutputEuropeanDisplay.PATTERN_FIELD_QC);
            }
        }*/
        outputHelper.headers
                .forEach(h -> {
                    addVariableHeader(outputHelper, h);
                });
    }

    private void addVariableHeader(final BuildOutputHelper outputHelper, String h) {
        String[] split = h.split("_");
        outputHelper.printStream.printf(this.localizationManager.getMessage(
                MPSOutputClassicDisplay.BUNDLE_SOURCE_PATH,
                MPSOutputClassicDisplay.PROPERTY_MSG_OUTPUT_HEADER2), split[0]);
        outputHelper.printStream.print(PATTERN_FIELD_QC);
    }

    /**
     * Builds the header.
     *
     * @param requestMetadatasMap
     * @return the string
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java
     *                           .util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected String buildHeader(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Display columns variable.
     *
     * @param outputHelper
     * @param valeur
     * @param column
     * @link(BuildOutputHelper) the output helper
     * @link(ValeurMPS) the valeur
     * @link(ColumnHeader) the column
     * @see org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver#
     * displayColumnsVariable
     * (org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver
     * .BuildOutputHelper, org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS,
     * org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS.ColumnHeader)
     */
    @Override
    @SuppressWarnings("rawtypes")
    protected void displayColumnsVariable(final BuildOutputHelper outputHelper, BuildOutputHelper.Line line, int column) {
        String[] header = line.getColonnesNames().get(column).split("_");
        String qc = line.getQcs().get(column);
        qc = qc == "null" ? "NC" : qc;
        float value = line.getValeurs().get(column).matches("[0-9\\.]*") ? Float.parseFloat(line.getValeurs().get(column)) : ACBBUtils.CST_INVALID_EMPTY_MEASURE;
        final String values = line.getColonnesNames().contains(column) ? line.getColonnesNames().get(column) : "NC_NC_NC";
        String[] columns = values.split("_");
        outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, header[2]);
        outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, ACBBUtils.getNAIfBadValueOrNVIfEmptyValue(value));
        outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, header[1]);
        outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, "NC".equals(qc) ? ACBBUtils.PROPERTY_CST_NOT_AVALAIBALE : qc);
    }

    /**
     * Display empty column variable.
     *
     * @param outputHelper
     * @param valeur
     * @param column
     * @link(BuildOutputHelper) the output helper
     * @link(ValeurMPS) the valeur
     * @link(ColumnHeader) the column
     * @see org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver#
     * displayEmptyColumnVariable
     * (org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver
     * .BuildOutputHelper, org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS,
     * org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS.ColumnHeader)
     */
    @Override
    @SuppressWarnings("rawtypes")
    protected void displayEmptyColumnVariable(final BuildOutputHelper outputHelper, BuildOutputHelper.Line line, int column) {
        outputHelper.printStream.print(MPSOutputClassicDisplay.PATTERN_3_FIELDS_NA);
        outputHelper.printStream.print(RecorderACBB.PROPERTY_CST_NOT_AVALAIBALE_FIELD);
    }

    /**
     * Gets the aCBB message.
     *
     * @param key
     * @return the aCBB message
     * @link(String) the key
     * @link(String) the key
     */
    String getACBBMessage(final String key) {
        return this.getLocalizationManager().getMessage(RecorderACBB.ACBB_DATASET_BUNDLE_NAME, key);
    }

    /**
     * Gets the extention.
     *
     * @return the extention
     * @see org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver#
     * getExtention()
     */
    @Override
    protected String getExtention() {
        return MPSOutputsBuildersResolver.EXTENSION_CSV;
    }

    /**
     * Gets the prefix file name.
     *
     * @return the prefix file name
     * @see org.inra.ecoinfo.acbb.extraction.mps.impl.MPSOutputsBuildersResolver#
     * getPrefixFileName()
     */
    @Override
    protected String getPrefixFileName() {
        return MPSOutputClassicDisplay.CST_MPS;
    }
}
