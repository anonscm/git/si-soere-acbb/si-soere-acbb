package org.inra.ecoinfo.acbb.dataset.mps.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestValues;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.time.DateTimeException;
import java.util.Map;

/**
 * The Class TestValuesFlux.
 * <p>
 * implementation of {@link ITestValues}
 *
 * @author Tcherniatinsky Philippe
 * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues
 * @see org.inra.ecoinfo.acbb.dataset.ITestValues
 */
public class TestValuesMPS extends GenericTestValues {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new test values flux.
     */
    public TestValuesMPS() {
        super();
    }

    /**
     * Check other type value.
     *
     * @param values
     * @param index
     * @param column
     * @param badsFormatsReport
     * @param lineNumber            <long> the line number
     * @param value
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param requestPropertiesACBB
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String)
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the index
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @see org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#checkOtherTypeValue(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, long, int, java.lang.String,
     * org.inra.ecoinfo.dataset.Column, java.util.Map)
     */
    @Override
    protected void checkOtherTypeValue(final String[] values,
                                       final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index,
                                       final String value, final Column column,
                                       final Map<String, DatatypeVariableUniteACBB> variablesTypesDonnees,
                                       final DatasetDescriptorACBB datasetDescriptor,
                                       final IRequestPropertiesACBB requestPropertiesACBB) {
        int finalIndex = index;
        Column finalColumn = column;
        if (finalIndex < datasetDescriptor.getUndefinedColumn()
                || finalIndex > datasetDescriptor.getColumns().size() - 1) {
            return;
        }
        final IRequestPropertiesMPS requestPropertiesMPS = (IRequestPropertiesMPS) requestPropertiesACBB;
        String date = null;
        finalColumn = requestPropertiesMPS.getExpectedColums().get(finalIndex);
        if (finalColumn.isFlag()
                && RecorderACBB.PROPERTY_CST_DATE_TYPE.equals(finalColumn.getFlagType())
                && "date".equals(finalColumn.getName())) {
            final String time = values[++finalIndex];
            try {
                date = requestPropertiesMPS.dateToStringYYYYMMJJHHMMSS(value, time);
                requestPropertiesMPS.addDate(date);
            } catch (final DateTimeException | BadExpectedValueException e) {
                badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_INTERVAL_DATE_TIME),
                        value, time, lineNumber)));
            }
        } else if (finalColumn.isFlag()
                && RecorderACBB.PROPERTY_CST_DATE_TYPE.equals(finalColumn.getFlagType())) {
            try {
                DateUtil.readLocalDateFromText(value, DateUtil.DD_MM_YYYY);
            } catch (final DateTimeException e) {
                badsFormatsReport
                        .addException(new BusinessException(
                                String.format(
                                        finalIndex == 3 ? RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_PLANTING_DATE)
                                                : RecorderACBB
                                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_DATE_HARVEST),
                                        value, lineNumber, finalIndex + 1, finalColumn.getName(),
                                        RecorderACBB.PROPERTY_CST_FRENCH_DATE)));
            }
        }
        if (finalColumn.isFlag() && "quality class".equals(finalColumn.getFlagType())) {
            int qualityClass;
            try {
                String qc = values[finalIndex];
                if (qc == null) {
                    return;
                }
                qualityClass = Integer.parseInt(qc);
                if (qualityClass != 0 || qualityClass != 1 || qualityClass != 2
                        || qualityClass != 3) {
                    badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_QUALITY_CLASS),
                            lineNumber, finalIndex)));
                }
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BusinessException(String.format(RecorderACBB
                                .getACBBMessage(RecorderACBB.PROPERTY_MSG_INVALID_QUALITY_CLASS),
                        lineNumber, finalIndex)));
            }
        }
    }

    /**
     * Test values.
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.dataset.impl.GenericTestValues#testValues(long,
     *                           com.Ostermiller.util.CSVParser,
     *                           org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *                           org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *                           org.inra.ecoinfo.dataset.BadsFormatsReport,
     *                           org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB, java.lang.String)
     * @link(long) the startline
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(String) the datatype name
     */
    @Override
    public void testValues(final long startline, final CSVParser parser,
                           final VersionFile versionFile, final IRequestPropertiesACBB requestProperties,
                           final String encoding, final BadsFormatsReport badsFormatsReport,
                           final DatasetDescriptorACBB datasetDescriptor, final String datatypeName)
            throws BusinessException {
        super.testValues(startline, parser, versionFile, requestProperties, encoding,
                badsFormatsReport, datasetDescriptor, datatypeName);
    }
}
