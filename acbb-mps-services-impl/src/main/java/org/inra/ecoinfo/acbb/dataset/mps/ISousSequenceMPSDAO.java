/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;

import java.util.Optional;

/**
 * The Interface ISousSequenceMPSDAO.
 *
 * @param <S4>  the generic type
 * @param <SS4> the generic type
 */
@SuppressWarnings("rawtypes")
public interface ISousSequenceMPSDAO<S4 extends SequenceMPS, SS4 extends SousSequenceMPS> extends
        IDAO<SS4> {

    /**
     * Gets the by nkey.
     *
     * @param sequenceMPS the sequence mps
     * @param profondeur  the profondeur
     * @return the by nkey
     */
    Optional<SS4> getByNkey(S4 sequenceMPS, int profondeur);
}
