/*
 *s
 */
package org.inra.ecoinfo.acbb.extraction.mps.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.jobs.IStatusBar;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.Query;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

/**
 * The Class JPASMPDAO.
 *
 * @param <S>
 * @param <M>
 */
public class JPAMPSNativeDAO extends AbstractJPADAO<Object> implements IDAO<Object>, INativeMPSDAO {

    public static final String EXTRACT_REQUEST = "with \n"
            + "            selected as ( \n"
            + "            	select \n"
            + "		'%5$s' locale,%n"
            + "		'%6$s' utilisateur,\n"
            + "		'%7$s' noespece,\n"
            + "		'%8$s' nodatedebut,\n"
            + "		'%9$s' nodatefin"
            + "            ),\n"
            + "cae as (\n"
            + "	select isroot isroot, null datestart, null dateend,null realnode, case when isroot then login else null end login from utilisateur u\n"
            + "	join selected on utilisateur=login\n"
            + "	union\n"
            + "	select\n"
            + "        false,\n"
            + "        datestart,\n"
            + "        dateend,\n"
            + "        realnode,\n"
            + "        groupe.group_name \n"
            + "    from\n"
            + "        compositeactivityextraction  \n"
            + "    join\n"
            + "        composite_node_data_set \n"
            + "            on composite_node_data_set.branch_node_id=idnode  \n"
            + "	join groupe on groupe.group_name=login\n"
            + "	join group_utilisateur gu on gu.group_id = groupe.id\n"
            + "	join utilisateur u on u.id = gu.usr_id\n"
            + "    join\n"
            + "        selected \n"
            + "            on utilisateur=u.login),\n"
            + "mps as (\n"
            + "	select  noespece, nodatedebut, nodatefin, locale, var.affichage varaffichage, spa.spa_id, date, heure,profondeur, num_repetition,valeur,qc,\n"
            + "			site.code site, par.nom parcelle, coalesce(bloc.code, '') bloc, blocp.nom blocRepetition, tra.nom tnom, tra.affichage taffichage,v.id realnode  \n"
            + "	from selected \n"
            + "	join variable var on var.affichage in ('SMP') and var.var_id in (%3$s) \n"
            + "	join sequence_smp_ssmp s  on s.date between cast(:datedebut as date)  and cast(:datefin as date)\n"
            + "	join sous_sequence_smp_sssmp ss on s.ssmp_id = ss.ssmp_id and profondeur in (%4$s)\n"
            + "	join mesure_smp_msmp m on m.sssmp_id = ss.sssmp_id\n"
            + "	join valeur_smp_vsmp v using(msmp_id)\n"
            + " join utilisateur u on u.login = utilisateur\n"
            + "	left join cae  on cae.login=u.login\n"
            + "	and cae.isroot or (cae.realnode=v.id and (cae.datestart is null or s.date>=cae.datestart) and (cae.dateend is null or s.date<=cae.dateend))\n"
            + "	join suivi_parcelle_spa spa on spa.spa_id = s.spa_id and tra_id in(%2$s) \n"
            + "	join parcelle_par par ON par.par_id = spa.par_id and par.id in (%1$s)\n"
            + "	join composite_nodeable site on site.id= par.id \n"
            + "	left join bloc_bloc bloc using(bloc_id)\n"
            + "	left join bloc_bloc blocp on blocp.bloc_id = bloc.parent_id\n"
            + "	join traitement_tra tra using(tra_id)\n"
            + " where u.isroot or cae.login is not null\n"
            + "union \n"
            + "	select  noespece, nodatedebut, nodatefin, locale, var.affichage varaffichage, spa.spa_id, date, heure,profondeur, num_repetition,valeur,qc,\n"
            + "			site.code site, par.nom parcelle, coalesce(bloc.code, '') bloc, blocp.nom blocRepetition, tra.nom tnom, tra.affichage taffichage,v.id realnode  \n"
            + "	from selected \n"
            + "	join variable var on var.affichage in ('TS') and var.var_id in (%3$s) \n"
            + "	join sequence_ts_sts s  on s.date between cast(:datedebut as date)  and cast(:datefin as date)\n"
            + "	join sous_sequence_ts_ssts ss on s.sts_id = ss.sts_id and profondeur in (%4$s)\n"
            + "	join mesure_ts_mts m on m.ssts_id = ss.ssts_id\n"
            + "	join valeur_ts_vts v using(mts_id)\n"
            + " join utilisateur u on u.login = utilisateur\n"
            + "	left join cae  on cae.login=u.login\n"
            + "	and cae.isroot or (cae.realnode=v.id and (cae.datestart is null or s.date>=cae.datestart) and (cae.dateend is null or s.date<=cae.dateend))\n"
            + " join suivi_parcelle_spa spa on spa.spa_id = s.spa_id and tra_id in(%2$s)\n"
            + "	join parcelle_par par ON par.par_id = spa.par_id and par.id in (%1$s)\n"
            + "	join composite_nodeable site on site.id= par.id\n"
            + "	left join bloc_bloc bloc using(bloc_id)\n"
            + "	left join bloc_bloc blocp on blocp.bloc_id = bloc.parent_id\n"
            + "	join traitement_tra tra using(tra_id)\n"
            + " where u.isroot or cae.login is not null\n"
            + "union\n"
            + "	select  noespece, nodatedebut, nodatefin, locale, var.affichage varaffichage, spa.spa_id, date, heure,profondeur, num_repetition,valeur,qc,\n"
            + "			site.code site, par.nom parcelle, coalesce(bloc.code, '') bloc, blocp.nom blocRepetition, tra.nom tnom, tra.affichage taffichage,v.id realnode  \n"
            + "	from selected \n"
            + "	join variable var on var.affichage in ('SWC') and var.var_id in (%3$s) \n"
            + "	join sequence_swc_sswc s  on s.date between cast(:datedebut as date)  and cast(:datefin as date)\n"
            + "	join sous_sequence_swc_ssswc ss on s.sswc_id = ss.sswc_id and profondeur in (%4$s)\n"
            + "	join mesure_swc_mswc m on m.ssswc_id = ss.ssswc_id\n"
            + "	join valeur_swc_vswc v using(mswc_id)\n"
            + " join utilisateur u on u.login = utilisateur\n"
            + "	left join cae  on cae.login=u.login\n"
            + "	and cae.isroot or (cae.realnode=v.id and (cae.datestart is null or s.date>=cae.datestart) and (cae.dateend is null or s.date<=cae.dateend))\n"
            + " join suivi_parcelle_spa spa on spa.spa_id = s.spa_id and tra_id in(%2$s)\n"
            + "	join parcelle_par par ON par.par_id = spa.par_id and par.id in (%1$s)\n"
            + "	join composite_nodeable site on site.id= par.id\n"
            + "	left join bloc_bloc bloc using(bloc_id)\n"
            + "	left join bloc_bloc blocp on blocp.bloc_id = bloc.parent_id\n"
            + "	join traitement_tra tra using(tra_id)\n"
            + " where u.isroot or cae.login is not null\n"
            + "),\n"
            + "/*couverts végétal des cultures temporaires principales*/ \n"
            + "	ct as ( \n"
            + "		select distinct spa_id, preal_id, areal.datededebut, areal.datedefin, vq.valeur as sem_couvert_vegetal\n"
            + "		from semis_sem \n"
            + "		join suivi_parcelle_spa spa using(spa_id) \n"
            + "		join tempo_temp temp using(tempo_id) \n"
            + "		left join annee_realisee_areal areal using(areal_id)\n"
            + "		left join valeur_qualitative vq on sem_couvert_vegetal=vq.vq_id\n"
            + "		order by spa_id, areal.datededebut\n"
            + "	), \n"
            + "/*définition des utilisations pour l'ensemble des parcelles*/\n"
            + "	selection as ( \n"
            + "		select distinct vtra_id, tra_id, par_id, mod.code \n"
            + "		from suivi_parcelle_spa spa \n"
            + "		join traitement_tra tra using(tra_id) \n"
            + "		join version_traitement_vtra vtra using(tra_id) \n"
            + "		join version_traitement_modalite_vtmod using(vtra_id) \n"
            + "		join modalite_mod mod using(mod_id)\n"
            + "	), \n"
            + "\n"
            + "/*définition des prairies permanentes*/ \n"
            + "	pp as ( \n"
            + "		select distinct vtra_id, tra_id, par_id \n"
            + "		from selection \n"
            + "		where vtra_id not in (\n"
            + "			select vtra_id \n"
            + "			from selection \n"
            + "			where code ~ '(U[0A]) '\n"
            + "			or code like 'R%%'\n"
            + "		) \n"
            + "	), \n"
            + "\n"
            + "/*définition des sols nus*/ \n"
            + "	sn as (\n"
            + "		select distinct vtra_id, tra_id, par_id \n"
            + "		from selection where code ='U0'\n"
            + "	), \n"
            + "\n"
            + "/*définition des abandons*/ \n"
            + "\n"
            + "	ab as (\n"
            + "		select distinct vtra_id, tra_id, par_id \n"
            + "		from selection where code ='UA'\n"
            + "	), \n"
            + "\n"
            + "/*récoltes*/ \n"
            + "	recolte as ( \n"
            + "		select spa_id, spa.tra_id, treal.par_id, rec_fau.date \n"
            + "		from recolte_et_fauche_recfau rec_fau\n"
            + "		join tempo_temp using(tempo_id) \n"
            + "		join version_traitement_realisee_treal treal using(treal_id) \n"
            + "		join version_traitement_vtra vtra on vtra.vtra_id = treal.vtra_id \n"
            + "		join suivi_parcelle_spa spa using(spa_id) \n"
            + "		where spa.tra_id not in (select tra_id from pp) \n"
            + "		order by spa_id, date\n"
            + "	), \n"
            + "\n"
            + "/*semis*/ \n"
            + "	datedebutcouverts as ( \n"
            + "		select spa_id, spa.tra_id, spa.par_id, datededebut, couvert from(\n"
            + "\n"
            + "			/* cas des cultures temporaires principales*/\n"
            + "			 select spa_id, datededebut, sem_couvert_vegetal as couvert\n"
            + "			 from ct \n"
            + "\n"
            + "			 union \n"
            + "\n"
            + "			/* cas des cultures temporaires secondaires*/\n"
            + "			 select spa_id , preal.datededebut, vq.valeur as couvert\n"
            + "			 from ct \n"
            + "			 join periode_realisee_preal preal using(preal_id) \n"
            + "			 join valeur_qualitative vq on preal.sem_couvert_vegetal = vq.vq_id \n"
            + "			 where preal.sem_couvert_vegetal is not null \n"
            + "\n"
            + "			 union \n"
            + "\n"
            + "			 /*cas des prairies permanentes*/\n"
            + "			 select distinct spa_id, tra.datedebuttraitement,'PP' as couvert\n"
            + "			 from pp \n"
            + "			 join suivi_parcelle_spa using(tra_id)\n"
            + "			 join traitement_tra tra using(tra_id) /* la date de debut de prairie est celle de début du traitement */\n"
            + "\n"
            + "			 union\n"
            + "\n"
            + "			 /*cas des prairies permanente fauchées*/\n"
            + "			 select spa_id, date, 'PP' from recolte\n"
            + "			 where par_id in (select par_id from pp) /*la date de début est la date de fauche*/\n"
            + "\n"
            + "			 union \n"
            + "\n"
            + "			 select spa_id, null,'S0' from sn as couvert\n"
            + "			 join suivi_parcelle_spa using(tra_id) \n"
            + "\n"
            + "			 union \n"
            + "\n"
            + "			 select spa_id, null,'SA' from ab as couvert\n"
            + "			 join suivi_parcelle_spa using(tra_id) ) as req\n"
            + "			 join suivi_parcelle_spa spa using(spa_id)\n"
            + "	), \n"
            + "\n"
            + "/*couverts pour cultures temporaires*/ \n"
            + "	couverts as ( \n"
            + "		select spa_id, couvert, datededebut, min(date) datedefin from (		 \n"
            + "			select distinct spa_id, couvert, datededebut, date  \n"
            + "			from datedebutcouverts  \n"
            + "			left join recolte using(spa_id)\n"
            + "			where datededebut < date and couvert != 'PT') as req1\n"
            + "		group by spa_id, couvert, datededebut\n"
            + "\n"
            + "		union\n"
            + "\n"
            + "		select spa_id,  'PT' couvert, min(datededebut) datededebut, max(datedefin)  datedefin from annee_realisee_areal \n"
            + "		join tempo_temp using(treal_id)\n"
            + "		join semis_sem using(tempo_id)\n"
            + "		join suivi_parcelle_spa spa using(spa_id)\n"
            + "		left join valeur_qualitative vq on vq_id = sem_couvert_vegetal\n"
            + "		group by valeur, spa_id\n"
            + "		having valeur='PT'\n"
            + "\n"
            + "		union\n"
            + "\n"
            + "		select debut.spa_id,  'PP' couvert, debut.datededebut, min(fin.datededebut)  datedefin from datedebutcouverts debut\n"
            + "		left join datedebutcouverts fin on debut.spa_id = fin.spa_id and debut.datededebut < fin.datededebut\n"
            + "		group by  debut.spa_id, debut.datededebut, debut.couvert\n"
            + "		having debut.couvert = 'PP'\n"
            + "\n"
            + "	),\n"
            + "localeSite as (select l1.defaultstring, jsonb_object_agg(\n"
            + "	l1.localization, \n"
            + "	coalesce(\n"
            + "		case \n"
            + "			when l1.localization = 'en'\n"
            + "		then\n"
            + "			coalesce(l1.localestring, l2.localestring)\n"
            + "		else\n"
            + "			l1.localestring\n"
            + "		end,\n"
            + "		l1.defaultstring\n"
            + "	) \n"
            + ") over(partition by l1.defaultstring) jsonb\n"
            + "from localisation l1 ,\n"
            + "lateral (select * from localisation l2 where l1.entite=l2.entite and l1.colonne= l2.colonne and l1.defaultstring=l2.defaultstring and l2.localization='en' ) l2\n"
            + "WHERE l1.entite  ='siteacbb' and l1.colonne = 'name'\n"
            + "),\n"
            + "localeParcelle as(\n"
            + "	select l1.defaultstring, jsonb_object_agg(\n"
            + "	l1.localization, \n"
            + "	coalesce(\n"
            + "		case \n"
            + "			when l1.localization = 'en'\n"
            + "		then\n"
            + "			coalesce(l1.localestring, l2.localestring)\n"
            + "		else\n"
            + "			l1.localestring\n"
            + "		end,\n"
            + "		l1.defaultstring\n"
            + "	)\n"
            + ") over(partition by l1.defaultstring) jsonb\n"
            + "from localisation l1 ,\n"
            + "lateral (select * from localisation l2 where l1.entite=l2.entite and l1.colonne= l2.colonne and l1.defaultstring=l2.defaultstring and l2.localization='en' ) l2\n"
            + "WHERE l1.entite  ='parcelle' and l1.colonne = 'name'\n"
            + "),\n"
            + "localeTraAffichage as (\n"
            + "select l1.defaultstring, jsonb_object_agg(\n"
            + "	l1.localization, \n"
            + "	coalesce(\n"
            + "		case \n"
            + "			when l1.localization = 'en'\n"
            + "		then\n"
            + "			coalesce(l1.localestring, l2.localestring)\n"
            + "		else\n"
            + "			l1.localestring\n"
            + "		end,\n"
            + "		l1.defaultstring\n"
            + "	)\n"
            + ") over(partition by l1.defaultstring) jsonb\n"
            + "from localisation l1 ,\n"
            + "lateral (select * from localisation l2 where l1.entite=l2.entite and l1.colonne= l2.colonne and l1.defaultstring=l2.defaultstring and l2.localization='en' ) l2\n"
            + "WHERE l1.entite  ='traitement_tra' and l1.colonne = 'affichage'\n"
            + "),\n"
            + "localeTraNom as (\n"
            + "select l1.defaultstring, jsonb_object_agg(\n"
            + "	l1.localization, \n"
            + "	coalesce(\n"
            + "		case \n"
            + "			when l1.localization = 'en'\n"
            + "		then\n"
            + "			coalesce(l1.localestring, l2.localestring)\n"
            + "		else\n"
            + "			l1.localestring\n"
            + "		end,\n"
            + "		l1.defaultstring\n"
            + "	)\n"
            + ") over(partition by l1.defaultstring) jsonb\n"
            + "from localisation l1 ,\n"
            + "lateral (select * from localisation l2 where l1.entite=l2.entite and l1.colonne= l2.colonne and l1.defaultstring=l2.defaultstring and l2.localization='en' ) l2\n"
            + "WHERE l1.entite  ='traitement_tra' and l1.colonne = 'nom'\n"
            + "),\n"
            + "localeCouverts as (\n"
            + "	select l1.defaultstring, jsonb_object_agg(\n"
            + "	l1.localization, \n"
            + "	coalesce(\n"
            + "		case \n"
            + "			when l1.localization = 'en'\n"
            + "		then\n"
            + "			coalesce(l1.localestring, l2.localestring)\n"
            + "		else\n"
            + "			l1.localestring\n"
            + "		end,\n"
            + "		l1.defaultstring\n"
            + "	)\n"
            + "	) over(partition by l1.defaultstring) jsonb\n"
            + "	from valeur_qualitative\n"
            + "	join localisation l1 on l1.entite  ='valeur_qualitative' and valeur=defaultstring  and code ='sem_couvert_vegetal',\n"
            + "	lateral (select * from localisation l2 where l1.entite=l2.entite and l1.colonne= l2.colonne and l1.defaultstring=l2.defaultstring and l2.localization='en' ) l2\n"
            + "),\n"
            + "valeurs as ( "
            + "select \n"
            + "			coalesce(jsonb_extract_path_text(ls.jsonb, locale), site) site, \n"
            + "			coalesce(jsonb_extract_path_text(lp.jsonb, locale), parcelle) parcelle, \n"
            + "			cast(bloc as text), blocRepetition, \n"
            + "			coalesce(jsonb_extract_path_text(ltn.jsonb, locale), tnom) || '(' || coalesce(jsonb_extract_path_text(lta.jsonb, locale), taffichage) || ')' traitement, \n"
            + "			coalesce(jsonb_extract_path_text(co.jsonb, locale), couvert, noespece) couvert, \n"
            + "			coalesce(to_char(datededebut, 'dd/MM/yyyy'), nodatedebut) datededebut, \n"
            + "			coalesce(to_char(datedefin, 'dd/MM/yyyy'), nodatefin) datedefin,  \n"
            + "			case when heure=cast('00:00:00' as time) then to_char(date-1, 'dd/MM/yyyy') else to_char(date, 'dd/MM/yyyy') end date,\n"
            + "			case when heure=cast('00:00:00' as time) then '24:00:00' else to_char(heure, 'HH24:MI:ss') end heure,\n"
            + "			jsonb_object_agg(distinct varaffichage||'_'||num_repetition||'_'||profondeur,  mps.valeur) valeur,\n"
            + "			jsonb_object_agg(distinct varaffichage||'_'||num_repetition||'_'||profondeur,  qc) qc\n"
            + "		from mps\n"
            + "		left join couverts on couverts.spa_id=mps.spa_id and (datededebut is null or date >= datededebut) and \n"
            + "		(datedefin is null or date<=datedefin)\n"
            + "		left join localesite ls on ls.defaultstring=site\n"
            + "		left join localeparcelle lp on lp.defaultstring=parcelle\n"
            + "		left join localetraAffichage lta on lta.defaultstring=taffichage\n"
            + "		left join localeTraNom ltn on ltn.defaultstring=tnom\n"
            + "		left join localeCouverts co on co.defaultstring=couvert\n"
            + "		group by  \n"
            + "			locale,\n"
            + "			mps.spa_id, \n"
            + "			ls.jsonb ,site,\n"
            + "			lp.jsonb, parcelle,\n"
            + "			lta.jsonb, taffichage,\n"
            + "			ltn.jsonb, tnom,\n"
            + "			mps.bloc, blocRepetition, \n"
            + "			date,heure,\n"
            + "			co.jsonb, couvert, noespece, datededebut, nodatedebut, datedefin, nodatefin\n"
            + "		order by mps.spa_id, date+heure\n"
            + "	),\n"
            + "	colonne as (\n"
            + "		select string_agg(t, ',' order by  split_part(t, '_',1),split_part(t, '_',2)\\:\\:integer,split_part(t, '_',3)\\:\\:integer) colonnes from (\n"
            + "		select distinct jsonb_object_keys(valeur) t from valeurs )t\n"
            + "		)\n"
            + "	select  colonnes, site, parcelle, bloc, coalesce(blocRepetition, ''), traitement, couvert, datededebut, datedefin, date,heure,valeur\\:\\:text, qc\\:\\:text from colonne, valeurs";

    @Override
    public Map<String, Query> extractMPS(IStatusBar statusBar, List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals, SortedSet<Integer> selectedProfondeurs, List<VariableACBB> selectedMPSVariables, IUser user) {
        Optional.ofNullable(statusBar).ifPresent(sb -> sb.setProgress(0));
        String format = String.format(EXTRACT_REQUEST,
                selectedTraitements.stream().map(t -> t.getSite()).map(s -> s.getId()).map(l -> Long.toString(l)).distinct().collect(Collectors.joining(",")),
                selectedTraitements.stream().map(t -> t.getId()).map(l -> Long.toString(l)).distinct().collect(Collectors.joining(",")),
                selectedMPSVariables.stream().map(t -> t.getId()).map(l -> Long.toString(l)).distinct().collect(Collectors.joining(",")),
                selectedProfondeurs.stream().map(l -> Long.toString(l)).distinct().collect(Collectors.joining(",")),
                user.getLanguage().toLowerCase(),
                user.getLogin(),
                RecorderACBB
                        .getACBBMessageWithBundle(VegetalPeriode.BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH,
                                VegetalPeriode.PROPERTY_MSG_NO_DEFINED_COUVERT),
                RecorderACBB
                        .getACBBMessageWithBundle(VegetalPeriode.BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH,
                                VegetalPeriode.PROPERTY_MSG_NO_DEFINED_SOWING_DATE),
                RecorderACBB
                        .getACBBMessageWithBundle(VegetalPeriode.BUNDLE_VEGETEBLE_PERIODE_SOURCE_PATH,
                                VegetalPeriode.PROPERTY_MSG_NO_DEFINED_HARVEST_DATE)
        );
        Query statement = entityManager.createNativeQuery(format);
        IntervalDate intervalDate = intervals.get(0);
        Map<String, Query> streams = new HashMap();
        LocalDateTime dd = intervalDate.getBeginDate();
        LocalDateTime df = dd.with(lastDayOfYear());
        df = intervalDate.getEndDate().isBefore(df) ? intervalDate.getEndDate() : df;
        final int differentiel = 5 / (intervalDate.getEndDate().getYear() - intervalDate.getBeginDate().getYear() + 1);
        int count = 0;
        while (!dd.isAfter(df)) {
            final Query query;
            try {
                query = statement
                        .setParameter("datedebut", DateUtil.getUTCDateTextFromLocalDateTime(dd, "yyyy-MM-dd"))
                        .setParameter("datefin", DateUtil.getUTCDateTextFromLocalDateTime(df, "yyyy-MM-dd"));
                streams.put(DateUtil.getUTCDateTextFromLocalDateTime(df, DateUtil.YYYY), query);
            } catch (Exception e) {
                LOGGER.debug(e.getCause().getMessage());
            } finally {
                count++;
                final int localCount = count;
                Optional.ofNullable(statusBar).ifPresent(sb -> sb.setProgress(differentiel * localCount));
            }
            dd = dd.plus(1, ChronoUnit.YEARS).with(firstDayOfYear());
            df = dd.with(lastDayOfYear());
            df = intervalDate.getEndDate().isBefore(df) ? intervalDate.getEndDate() : df;
        }

        return streams;
    }
}
