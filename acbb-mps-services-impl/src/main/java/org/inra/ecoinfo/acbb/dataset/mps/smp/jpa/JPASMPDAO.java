/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp.jpa;

import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.smp.ISMPDAO;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.MesureSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SequenceSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SousSequenceSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.ValeurSMP;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.smp.SynthesisValue;

/**
 * The Class JPASMPDAO.
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class JPASMPDAO extends JPAMPSDAO<SequenceSMP, MesureSMP> implements ISMPDAO {

    /**
     *
     */
    public static final String VARIABLE_SMP_AFFICHAGE = "SMP";

    /**
     * @return
     */
    @Override
    protected String getVariableAffichage() {
        return VARIABLE_SMP_AFFICHAGE;
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected Class<SequenceSMP> getSequenceEntityClass() {
        return SequenceSMP.class;
    }

    @Override
    protected Class<? extends SousSequenceMPS> getSousSequenceEntityClass() {
        return SousSequenceSMP.class;
    }

    @Override
    protected Class<MesureSMP> getMesureEntityClass() {
        return MesureSMP.class;
    }

    @Override
    protected Class<? extends ValeurMPS> getValeurEntityClass() {
        return ValeurSMP.class;
    }
}
