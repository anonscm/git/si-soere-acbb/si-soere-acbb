package org.inra.ecoinfo.acbb.dataset.mps;

import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import javax.persistence.Tuple;
import java.util.List;
import java.util.SortedSet;

/**
 * @param <S>
 * @param <M>
 * @author ptcherniati
 */
@SuppressWarnings("rawtypes")
public interface IMPSDAO<S extends SequenceMPS, M extends MesureMPS> {

    /**
     * Extract org.inra.ecoinfo.acbb.dataset.smp.
     *
     * @param selectedTraitements the selected traitements
     * @param intervals           the intervals
     * @param selectedProfondeurs the selected profondeurs
     * @param user
     * @return the list
     */
    List<M> extractMPS(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals, SortedSet<Integer> selectedProfondeurs, IUser user);

    /**
     * Extract org.inra.ecoinfo.acbb.dataset.smp. size
     *
     * @param selectedTraitements the selected traitements
     * @param intervals           the intervals
     * @param selectedProfondeurs the selected profondeurs
     * @param user
     * @return the number of line of extract
     * @throws PersistenceException the persistence exception
     * @throws BusinessException    the business exception
     */
    Long sizeMPS(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals, SortedSet<Integer> selectedProfondeurs, IUser user) throws PersistenceException, BusinessException;

    /**
     * Gets the availables profondeurs by.
     *
     * @param traitement  the traitement
     * @param intervals   the intervals
     * @param utilisateur the utilisateur
     * @param user
     * @return the availables profondeurs by
     */
    List<Integer> getAvailablesProfondeursBy(List<TraitementProgramme> traitement, List<IntervalDate> intervals, IUser utilisateur, IUser user);

    /**
     * Gets the availables traitements for periode.
     *
     * @param intervals the intervals
     * @param user
     * @return the availables traitements for periode
     */
    List<TraitementProgramme> getAvailablesTraitementsForPeriode(List<IntervalDate> intervals, IUser user);

    /**
     * Gets the available variable by traitement.
     *
     * @param traitement  the traitement
     * @param intervals   the intervals
     * @param utilisateur the utilisateur
     * @return the available variable by traitement
     */
    List<NodeDataSet> getAvailableVariableByTraitement(List<TraitementProgramme> traitement, List<IntervalDate> intervals, IUser utilisateur);

    /**
     * Gets the columns.
     *
     * @param selectedTraitements the selected traitements
     * @param intervals           the intervals
     * @param selectedProfondeurs the selected profondeurs
     * @return the columns
     * @throws PersistenceException the persistence exception
     */
    List<Tuple> getColumns(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals,
                           SortedSet<Integer> selectedProfondeurs) throws PersistenceException;

}
