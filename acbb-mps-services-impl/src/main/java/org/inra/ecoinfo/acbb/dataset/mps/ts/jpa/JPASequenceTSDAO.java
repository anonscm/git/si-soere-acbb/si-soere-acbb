/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.ts.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SequenceTS_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASequenceTSDAO.
 */
public class JPASequenceTSDAO extends AbstractJPADAO<SequenceTS> implements
        ISequenceMPSDAO<SequenceTS> {

    /**
     * Gets the by nkey.
     *
     * @param version
     * @param suiviParcelle
     * @param date
     * @return the by nkey @see
     * org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO#getByNkey(org.inra.
     * ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle, java.time.LocalDateTime)
     * @link(VersionFile) the version
     * @link(SuiviParcelle) the suivi parcelle
     * @link(Date) the date
     */
    @Override
    public Optional<SequenceTS> getByNkey(final VersionFile version, final SuiviParcelle suiviParcelle,
                                          final LocalDate date) {
        CriteriaQuery<SequenceTS> query = builder.createQuery(SequenceTS.class);
        Root<SequenceTS> sswc = query.from(SequenceTS.class);
        query.where(
                builder.equal(sswc.join(SequenceTS_.version), version),
                builder.equal(sswc.join(SequenceTS_.suiviParcelle), suiviParcelle),
                builder.equal(sswc.get(SequenceTS_.date), date)
        );
        query.select(sswc);
        return getOptional(query);
    }

    /**
     * @param date
     * @param versionFile
     * @return
     */
    @Override
    public Optional<SequenceTS> getSequence(LocalDate date, VersionFile versionFile) {
        CriteriaQuery<SequenceTS> query = builder.createQuery(SequenceTS.class);
        Root<SequenceTS> sswc = query.from(SequenceTS.class);
        query.where(
                builder.equal(sswc.join(SequenceTS_.version), versionFile),
                builder.equal(sswc.get(SequenceTS_.date), date)
        );
        query.select(sswc);
        return getOptional(query);
    }

}
