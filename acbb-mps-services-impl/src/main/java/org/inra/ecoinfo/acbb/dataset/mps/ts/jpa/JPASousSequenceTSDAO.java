/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.ts.jpa;


import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.ISousSequenceMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SousSequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SousSequenceTS_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPASousSequenceTSDAO.
 */
public class JPASousSequenceTSDAO extends AbstractJPADAO<SousSequenceTS> implements
        ISousSequenceMPSDAO<SequenceTS, SousSequenceTS> {

    /**
     * Gets the by nkey.
     *
     * @param sequence
     * @param profondeur
     * @return the by nkey @see
     * org.inra.ecoinfo.acbb.dataset.mps.ISousSequenceMPSDAO#getByNkey(org.inra
     * .ecoinfo.acbb.dataset.mps.entity.SequenceMPS, int)
     * @link(SequenceTS) the sequence
     * @link(int) the profondeur
     */
    @Override
    public Optional<SousSequenceTS> getByNkey(final SequenceTS sequence, final int profondeur) {
        CriteriaQuery<SousSequenceTS> query = builder.createQuery(SousSequenceTS.class);
        Root<SousSequenceTS> ssts = query.from(SousSequenceTS.class);
        query.where(
                builder.equal(ssts.join(SousSequenceTS_.sequenceMPS), sequence),
                builder.equal(ssts.get(SousSequenceTS_.profondeur), profondeur)
        );
        query.select(ssts);
        return getOptional(query);
    }

}
