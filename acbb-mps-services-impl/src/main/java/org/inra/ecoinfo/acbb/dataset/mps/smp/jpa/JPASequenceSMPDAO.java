/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SequenceSMP;
import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.SequenceSMP_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASequenceSMPDAO.
 */
public class JPASequenceSMPDAO extends AbstractJPADAO<SequenceSMP> implements
        ISequenceMPSDAO<SequenceSMP> {

    /**
     * Gets the by nkey.
     *
     * @param version
     * @param suiviParcelle
     * @param date
     * @return the by nkey @see
     * org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO#getByNkey(org.inra.
     * ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle, java.time.LocalDateTime)
     * @link(VersionFile) the version
     * @link(SuiviParcelle) the suivi parcelle
     * @link(Date) the date
     */
    @Override
    public Optional<SequenceSMP> getByNkey(final VersionFile version, final SuiviParcelle suiviParcelle,
                                           final LocalDate date) {
        CriteriaQuery<SequenceSMP> query = builder.createQuery(SequenceSMP.class);
        Root<SequenceSMP> sswc = query.from(SequenceSMP.class);
        query.where(
                builder.equal(sswc.join(SequenceSMP_.version), version),
                builder.equal(sswc.join(SequenceSMP_.suiviParcelle), suiviParcelle),
                builder.equal(sswc.get(SequenceSMP_.date), date)
        );
        query.select(sswc);
        return getOptional(query);
    }

    /**
     * @param date
     * @param versionFile
     * @return
     */
    @Override
    public Optional<SequenceSMP> getSequence(LocalDate date, VersionFile versionFile) {
        CriteriaQuery<SequenceSMP> query = builder.createQuery(SequenceSMP.class);
        Root<SequenceSMP> sswc = query.from(SequenceSMP.class);
        query.where(
                builder.equal(sswc.join(SequenceSMP_.version), versionFile),
                builder.equal(sswc.get(SequenceSMP_.date), date)
        );
        query.select(sswc);
        return getOptional(query);
    }

}
