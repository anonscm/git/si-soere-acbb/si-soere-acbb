package org.inra.ecoinfo.acbb.extraction.mps.impl;

import org.inra.ecoinfo.acbb.dataset.mps.smp.ISMPDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.ISWCDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.ITSDAO;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.itk.IITKExtractionDAO;
import org.inra.ecoinfo.acbb.extraction.mps.jpa.INativeMPSDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.jobs.IStatusBar;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.acbb.extraction.cst.ConstantACBBExtraction;

/**
 * The Class MPSExtractor.
 */
public class MPSExtractor extends AbstractExtractor {

    // extraction
    /**
     * The Constant CST_RESULTS.
     */
    public static final String CST_RESULTS = "CST_RESULTS=extractionResults";
    /**
     * The Constant CST_RESULT_EXTRACTION_SMP_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_SMP_CODE = "extractionResultSMP";
    /**
     * The Constant CST_RESULT_EXTRACTION_SWC_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_SWC_CODE = "extractionResultSwc";
    /**
     * The Constant CST_RESULT_EXTRACTION_TS_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_TS_CODE = "extractionResultTS";
    /**
     * The Constant MSG_EXTRACTION_ABORTED @link(String).
     */
    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";
    /**
     * The variable acbbdao.
     */
    protected IVariableACBBDAO variableACBBDAO;
    /**
     * The SMP datatatype name @link(String).
     */
    String smpDatatatypeName;

    /**
     * The Swc datatatype name @link(String).
     */
    String swcDatatatypeName;

    /**
     * The Ts datatatype name @link(String).
     */
    String tsDatatatypeName;

    /**
     * The org.inra.ecoinfo.acbb.dataset.smp dao.
     */
    ISMPDAO smpDAO;

    /**
     * The org.inra.ecoinfo.acbb.dataset.swc dao.
     */
    ISWCDAO swcDAO;

    INativeMPSDAO nativeMPSDAO;
    /**
     * The ts dao.
     */
    ITSDAO tsDAO;
    IITKExtractionDAO itkExtractionDAO;

    private static DatesFormParamVO getDateYearsParamVO(final MPSParameterVO mpsParameterVO) {
        return (DatesFormParamVO) mpsParameterVO.getParameters().get(DatesFormParamVO.class.getSimpleName());
    }

    /**
     * Extract.
     *
     * @param parameters
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#extract(org.inra.ecoinfo
     *                           .extraction.IParameter)
     * @link(IParameter) the parameters
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public void extract(final IParameter parameters) throws BusinessException {
        final MPSParameterVO mpsParameterVO = (MPSParameterVO) parameters;
        final List<TraitementProgramme> selectedTreatment = (List<TraitementProgramme>) mpsParameterVO.getParameters().get(TraitementProgramme.class.getSimpleName());
        final List<IntervalDate> intervalDates = getDateYearsParamVO(mpsParameterVO).intervalsDate();
        final SortedSet<Integer> selectedDepth = mpsParameterVO.getSelectedDepth();
        final IUser currentUser = policyManager.getCurrentUser();
        List<VariableACBB> selectedMPSVariables = ((List<String>)parameters.getParameters().get(ConstantACBBExtraction.DATATYPE_NAME_INDEX)).stream()
                .map(code->(List<VariableACBB>)mpsParameterVO.getParameters().get(code))
                .flatMap(List::stream)
                .collect(Collectors.toList());
        IStatusBar statusBar = (IStatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        Map<String, Query> streamResult = nativeMPSDAO.extractMPS(statusBar, selectedTreatment, intervalDates, selectedDepth, selectedMPSVariables, currentUser);
        parameters.getParameters().put("MPS", streamResult);
    }

    /**
     * Extract datas.
     *
     * @param requestMetadatasMap
     * @return the map
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#extractDatas(java.
     *                           util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> extractDatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        return null;
    }

    /**
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        return 10_000_000L;
        /*Long size = 0L;
        final MPSParameterVO mpsParameterVO = (MPSParameterVO) parameters;
        for (final VariableACBB variable : mpsParameterVO.getSelectedMPSVariables()) {
            if (mpsParameterVO.getAffichage() == 0) {
                return 0;
            }
            try {
                if (variable.getAffichage().equals(this.smpDatatatypeName)) {
                    size += this.smpDAO.sizeMPS(mpsParameterVO.getSelectedsTraitements(),
                            getDateYearsParamVO(mpsParameterVO)
                                    .intervalsDate(), mpsParameterVO.getSelectedDepth(), policyManager.getCurrentUser());
                } else if (variable.getAffichage().equals(this.swcDatatatypeName)) {
                    size += this.swcDAO.sizeMPS(mpsParameterVO.getSelectedsTraitements(),
                            getDateYearsParamVO(mpsParameterVO)
                                    .intervalsDate(), mpsParameterVO.getSelectedDepth(), policyManager.getCurrentUser());
                } else if (variable.getAffichage().equals(this.tsDatatatypeName)) {
                    size += this.tsDAO.sizeMPS(mpsParameterVO.getSelectedsTraitements(),
                            getDateYearsParamVO(mpsParameterVO)
                                    .intervalsDate(), mpsParameterVO.getSelectedDepth(), policyManager.getCurrentUser());

                }
            } catch (final PersistenceException | BusinessException e) {
                size = -1L;
                break;
            }
        }
        return size * 2_500;*/
    }

    /**
     * Prepare request metadatas.
     *
     * @param requestMetadatasMap
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas
     *                           (java.util.Map)
     * @link(Map<String,Object>) the request metadatas map
     */
    @Override
    protected void prepareRequestMetadatas(final Map<String, Object> requestMetadatasMap)
            throws BusinessException {

    }

    /**
     * @param itkExtractionDAO
     */
    public void setItkExtractionDAO(IITKExtractionDAO itkExtractionDAO) {
        this.itkExtractionDAO = itkExtractionDAO;
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset.smp dao.
     *
     * @param smpDAO the new org.inra.ecoinfo.acbb.dataset.smp dao
     */
    public final void setSmpDAO(final ISMPDAO smpDAO) {
        this.smpDAO = smpDAO;
    }

    /**
     * Sets the sMP datatatype name.
     *
     * @param sMPDatatatypeName the new sMP datatatype name
     */
    public final void setSMPDatatatypeName(final String sMPDatatatypeName) {
        this.smpDatatatypeName = sMPDatatatypeName;
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset.swc dao.
     *
     * @param swcDAO the new org.inra.ecoinfo.acbb.dataset.swc dao
     */
    public final void setSwcDAO(final ISWCDAO swcDAO) {
        this.swcDAO = swcDAO;
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset.swc datatatype name.
     *
     * @param swcDatatatypeName the new org.inra.ecoinfo.acbb.dataset.swc
     *                          datatatype name
     */
    public final void setSwcDatatatypeName(final String swcDatatatypeName) {
        this.swcDatatatypeName = swcDatatatypeName;
    }

    /**
     * Sets the ts dao.
     *
     * @param tsDAO the new ts dao
     */
    public final void setTsDAO(final ITSDAO tsDAO) {
        this.tsDAO = tsDAO;
    }

    /**
     * Sets the ts datatatype name.
     *
     * @param tsDatatatypeName the new ts datatatype name
     */
    public final void setTsDatatatypeName(final String tsDatatatypeName) {
        this.tsDatatatypeName = tsDatatatypeName;
    }

    /**
     * Sets the variable acbbdao.
     *
     * @param variableACBBDAO the new variable acbbdao
     */
    public void setVariableACBBDAO(final IVariableACBBDAO variableACBBDAO) {
        this.variableACBBDAO = variableACBBDAO;
    }

    public void setNativeMPSDAO(INativeMPSDAO nativeMPSDAO) {
        this.nativeMPSDAO = nativeMPSDAO;
    }
}
