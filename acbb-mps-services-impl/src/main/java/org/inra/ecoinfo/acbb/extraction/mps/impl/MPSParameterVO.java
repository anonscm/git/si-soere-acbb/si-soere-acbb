/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps.impl;

import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.extraction.IDateFormParameter;
import org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

import java.util.*;

/**
 * The Class MPSParameterVO.
 */
public class MPSParameterVO extends DefaultParameter implements IMPSParameter {

    /**
     * The columns @link(Map<VersionFile,List<String>>).
     */
    final Map<VersionFile, List<String>> columns = new HashMap();
    /**
     * The selecteds traitements @link(List<TraitementProgramme>).
     */
    List<TraitementProgramme> selectedsTraitements = new LinkedList();
    /**
     * The commentaires @link(String).
     */
    String commentaires;
    /**
     * The dates years continuous form param vo.
     *
     * @link(DatesYearsContinuousFormParamVO).
     */
    DatesFormParamVO datesYearsContinuousFormParamVO;
    /**
     * The affichage @link(int).
     */
    int affichage;
    /**
     * The selected mps variables @link(List<VariableACBB>).
     */
    List<VariableACBB> selectedMPSVariables = new LinkedList();
    /**
     * The mesures mps @link(List<ISecurityPath>).
     */
    List<MesureMPS> mesuresMPS = new LinkedList();
    /**
     * The selecteds depths @link(SortedSet<Integer>).
     */
    SortedSet<Integer> selectedsDepths;

    /**
     * Instantiates a new mPS parameter vo.
     */
    public MPSParameterVO() {
    }

    public MPSParameterVO(Map<String, Object> metadatasMap) {
       super.setParameters(metadatasMap);
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public int getAffichage() {
        return this.affichage;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public final void setAffichage(final int affichage) {
        this.affichage = affichage;
    }

    /**
     * Gets the columns @link(Map<VersionFile,List<String>>).
     *
     * @return the columns @link(Map<VersionFile,List<String>>) @see org.
     * inra.ecoinfo.acbb.extraction.mps.IMPSParameter#getColumns()
     */
    @Override
    public Map<VersionFile, List<String>> getColumns() {
        return this.columns;
    }

    /**
     * Gets the commentaires.
     *
     * @return the commentaires
     */
    public String getCommentaires() {
        return this.commentaires;
    }

    /**
     * Sets the commentaires.
     *
     * @param commentaires the new commentaires
     */
    public final void setCommentaires(final String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     * Gets the dates years continuous form param vo.
     *
     * @return the dates years continuous form param vo
     * @see org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter# getDatesYearsContinuousFormParamVO()
     */
    @Override
    public IDateFormParameter getDatesYearsContinuousFormParamVO() {
        return this.datesYearsContinuousFormParamVO;
    }

    /**
     * Sets the dates years continuous form param vo.
     *
     * @param datesYearsContinuousFormParamVO the new dates years continuous form param vo
     * @see org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter# setDatesYearsContinuousFormParamVO
     * (org.inra.ecoinfo.acbb.ui.flex.vo.DatesYearsContinuousFormParamVO)
     */
    @Override
    public final void setDatesYearsContinuousFormParamVO(
            final IDateFormParameter datesYearsContinuousFormParamVO) {
        this.datesYearsContinuousFormParamVO = (DatesFormParamVO) datesYearsContinuousFormParamVO;
    }

    /**
     * Gets the extraction type code.
     *
     * @return the extraction type code
     * @see org.inra.ecoinfo.extraction.IParameter#getExtractionTypeCode()
     */
    @Override
    public String getExtractionTypeCode() {
        return IMPSParameter.MPS_EXTRACTION_TYPE_CODE;
    }

    /**
     * @return
     */
    public List<MesureMPS> getMesuresMPS() {
        return this.mesuresMPS;
    }

    /**
     * @param list
     */
    public final void setMesuresMPS(final List<MesureMPS> list) {
        this.mesuresMPS = list;
    }

    /**
     * Gets the selected depth.
     *
     * @return the selected depth
     * @see org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter#getSelectedDepth()
     */
    @Override
    public SortedSet<Integer> getSelectedDepth() {
        return this.selectedsDepths;
    }

    /**
     * Gets the selected mps variables @link(List<VariableACBB>).
     *
     * @return the selected mps variables @link(List<VariableACBB>)
     * @see org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter#getSelectedMPSVariables ()
     */
    @Override
    public List<VariableACBB> getSelectedMPSVariables() {
        return this.selectedMPSVariables;
    }

    /**
     * Sets the selected mps variables @link(List<VariableACBB>).
     *
     * @param selectedMPSVariables the new selected mps variables
     * @link(List<VariableACBB>) @see
     * org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter#setSelectedMPSVariables
     * (java.util.LinkedList)
     */
    @Override
    public final void setSelectedMPSVariables(final List<VariableACBB> selectedMPSVariables) {
        this.selectedMPSVariables = selectedMPSVariables;
    }

    /**
     * Gets the selecteds traitements @link(List<TraitementProgramme>).
     *
     * @return the selecteds traitements @link(List<TraitementProgramme>)
     * @see org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter#getSelectedsTraitements ()
     */
    @Override
    public List<TraitementProgramme> getSelectedsTraitements() {
        return this.selectedsTraitements;
    }

    /**
     * Sets the selecteds traitements @link(List<TraitementProgramme>).
     *
     * @param selectedsTraitements the new selecteds traitements
     * @link(List<TraitementProgramme>) @see
     * org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter#setSelectedsTraitements
     * (java.util.LinkedList)
     */
    @Override
    public final void setSelectedsTraitements(final List<TraitementProgramme> selectedsTraitements) {
        this.selectedsTraitements = selectedsTraitements;
    }

    /**
     * Sets the selecteds depths @link(SortedSet<Integer>).
     *
     * @param depths the new selecteds depths @link(SortedSet<Integer>)
     * @see org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter#setSelectedsDepths
     * (java.util.SortedSet)
     */
    @Override
    public final void setSelectedsDepths(final SortedSet<Integer> depths) {
        this.selectedsDepths = depths;
    }
}
