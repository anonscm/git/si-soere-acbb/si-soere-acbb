/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The Interface ISequenceMPSDAO.
 *
 * @param <S4> the generic type
 */
@SuppressWarnings("rawtypes")
public interface ISequenceMPSDAO<S4 extends SequenceMPS> extends IDAO<S4> {

    /**
     * Gets the by nkey.
     *
     * @param version       the version
     * @param suiviParcelle the suivi parcelle
     * @param date          the date
     * @return the by nkey
     */
    Optional<S4> getByNkey(VersionFile version, SuiviParcelle suiviParcelle, LocalDate date);

    /**
     * @param date
     * @param versionFile
     * @return
     */
    Optional<S4> getSequence(LocalDate date, VersionFile versionFile);
}
