/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.mps.jpa;

import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.jobs.IStatusBar;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.IntervalDate;

import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

/**
 * @author ptcherniati
 */
public interface INativeMPSDAO {

    Map<String, Query> extractMPS(IStatusBar statusBar, List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals, SortedSet<Integer> selectedProfondeurs, List<VariableACBB> selectedMPSVariables, IUser user);

}
