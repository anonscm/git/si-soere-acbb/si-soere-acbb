/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.swc.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalTime;
import java.util.Optional;

/**
 * The Class JPAMesureSWCDAO.
 */
public class JPAMesureSWCDAO extends AbstractJPADAO<MesureSWC> implements
        IMesureMPSDAO<SousSequenceSWC, MesureSWC> {

    /**
     * Gets the by n key.
     *
     * @param sousSequence
     * @param heure
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO#getByNKey(org.inra.ecoinfo
     * .acbb.dataset.mps.entity.SousSequenceMPS, java.time.LocalDateTime)
     * @link(SousSequenceSWC) the sous sequence
     * @link(Date) the heure
     */
    @Override
    public Optional<MesureSWC> getByNKey(final SousSequenceSWC sousSequence, final LocalTime heure) {
        CriteriaQuery<MesureSWC> query = builder.createQuery(MesureSWC.class);
        Root<MesureSWC> m = query.from(MesureSWC.class);
        query.where(
                builder.equal(m.join(MesureSWC_.sousSequenceMPS), sousSequence),
                builder.equal(m.get(MesureSWC_.heure), heure)
        );
        query.select(m);
        return getOptional(query);
    }

    /**
     * Gets the line publication name doublon.
     *
     * @param mesure
     * @return the line publication name doublon @see
     * org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO#getLinePublicationNameDoublon
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS)
     * @link(MesureSWC) the mesure
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final MesureSWC mesure) {
        this.beginNewTransaction();
        CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
        Root<MesureSWC> m = query.from(MesureSWC.class);
        query.where(
                builder.equal(m.join(MesureSWC_.sousSequenceMPS).join(SousSequenceSWC_.sequenceMPS).join(SequenceSWC_.suiviParcelle), mesure.getSousSequenceMPS().getSequenceMPS().getSuiviParcelle()),
                builder.equal(m.get(MesureSWC_.heure), mesure.getHeure()),
                builder.equal(m.join(MesureSWC_.sousSequenceMPS).get(SousSequenceSWC_.profondeur), mesure.getSousSequenceMPS().getProfondeur()),
                builder.equal(m.join(MesureSWC_.sousSequenceMPS).join(SousSequenceSWC_.sequenceMPS).get(SequenceSWC_.date), mesure.getSousSequenceMPS().getSequenceMPS().getDate())
        );
        query.multiselect(m.join(MesureSWC_.sousSequenceMPS).join(SousSequenceSWC_.sequenceMPS).join(SequenceSWC_.version).join(VersionFile_.dataset).get(Dataset_.id), m.get(MesureSWC_.lineNumber));
        return getFirstOrNull(query);
    }
}
