package org.inra.ecoinfo.acbb.dataset.mps.ts.impl;

import org.inra.ecoinfo.acbb.dataset.mps.impl.AbstractProcessRecordMPS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.MesureTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SousSequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.ValeurTS;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * process the record of flux files data.
 *
 * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractProcessRecord
 * @see org.inra.ecoinfo.acbb.dataset.IProcessRecord The Class ProcessRecordFlux.
 */
public class ProcessRecordTS extends
        AbstractProcessRecordMPS<SequenceTS, SousSequenceTS, MesureTS, ValeurTS> {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * New mesure instance.
     *
     * @param sousSequence
     * @param time
     * @param originalLineNumber
     * @return the mesure ts @see
     * org.inra.ecoinfo.acbb.dataset.mps.AbstractMPSRecorder#newMesureInstance
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS, java.time.LocalDateTime, int)
     * @link(SousSequenceTS) the sous sequence
     * @link(Date) the time
     * @link(int) the original line number
     */
    @Override
    public MesureTS newMesureInstance(final SousSequenceTS sousSequence, final LocalTime time,
                                      final int originalLineNumber) {
        return new MesureTS(sousSequence, time, originalLineNumber);
    }

    /**
     * New sequence instance.
     *
     * @param version
     * @param suiviParcelle
     * @param date
     * @return the sequence ts @see
     * org.inra.ecoinfo.acbb.dataset.mps.AbstractMPSRecorder#newSequenceInstance
     * (org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle, java.time.LocalDateTime)
     * @link(VersionFile) the version
     * @link(SuiviParcelle) the suivi parcelle
     * @link(Date) the date
     */
    @Override
    public SequenceTS newSequenceInstance(final VersionFile version,
                                          final SuiviParcelle suiviParcelle, final LocalDate date) {
        return new SequenceTS(version, suiviParcelle, date);
    }

    /**
     * New sous sequence instance.
     *
     * @param sequence
     * @param profondeur
     * @return the sous sequence ts @see
     * org.inra.ecoinfo.acbb.dataset.mps.AbstractMPSRecorder#newSousSequenceInstance
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.SequenceMPS, int)
     * @link(SequenceTS) the sequence
     * @link(int) the profondeur
     */
    @Override
    public SousSequenceTS newSousSequenceInstance(final SequenceTS sequence, final int profondeur) {
        return new SousSequenceTS(sequence, profondeur);
    }

    /**
     * New valeur instance.
     *
     * @param realNode
     * @param mesure
     * @param value
     * @param qualityClass
     * @param numRepetition
     * @return the valeur ts @see
     * org.inra.ecoinfo.acbb.dataset.mps.AbstractMPSRecorder#newValeurInstance
     * (org.inra.ecoinfo.acbb.refdata.variable.VariableACBB, java.lang.Float,
     * java.lang.Integer, int)
     * @link(VariableACBB) the variable
     * @link(Float) the value
     * @link(Integer) the quality class
     * @link(int) the num repetition
     */
    @Override
    public ValeurTS newValeurInstance(final RealNode realNode, final Float value,
                                      final Integer qualityClass, final int numRepetition, final MesureTS mesure) {
        final ValeurTS valeur = new ValeurTS(value, qualityClass);
        valeur.setRealNode(realNode);
        valeur.setQualityClass(qualityClass);
        valeur.setNumRepetition(numRepetition);
        valeur.setMesureMPS(mesure);
        return valeur;
    }
}
