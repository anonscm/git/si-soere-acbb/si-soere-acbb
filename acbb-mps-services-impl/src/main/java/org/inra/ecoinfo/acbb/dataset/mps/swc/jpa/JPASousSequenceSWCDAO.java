/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.swc.jpa;


import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.ISousSequenceMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SequenceSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SousSequenceSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SousSequenceSWC_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * The Class JPASousSequenceSWCDAO.
 */
public class JPASousSequenceSWCDAO extends AbstractJPADAO<SousSequenceSWC> implements
        ISousSequenceMPSDAO<SequenceSWC, SousSequenceSWC> {

    /**
     * Gets the by nkey.
     *
     * @param sequence
     * @param profondeur
     * @return the by nkey @see
     * org.inra.ecoinfo.acbb.dataset.mps.ISousSequenceMPSDAO#getByNkey(org.inra
     * .ecoinfo.acbb.dataset.mps.entity.SequenceMPS, int)
     * @link(SequenceSWC) the sequence
     * @link(int) the profondeur
     */
    @Override
    public Optional<SousSequenceSWC> getByNkey(final SequenceSWC sequence, final int profondeur) {
        CriteriaQuery<SousSequenceSWC> query = builder.createQuery(SousSequenceSWC.class);
        Root<SousSequenceSWC> ssswc = query.from(SousSequenceSWC.class);
        query.where(
                builder.equal(ssswc.join(SousSequenceSWC_.sequenceMPS), sequence),
                builder.equal(ssswc.get(SousSequenceSWC_.profondeur), profondeur)
        );
        query.select(ssswc);
        return getOptional(query);
    }

}
