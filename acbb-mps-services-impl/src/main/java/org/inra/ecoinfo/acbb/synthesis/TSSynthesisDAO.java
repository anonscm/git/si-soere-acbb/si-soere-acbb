/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.*;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.synthesis.ts.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.ts.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author tcherniatinsky
 */
public class TSSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurTS> v = query.from(ValeurTS.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<SousSequenceTS, SequenceTS> s = v.join(ValeurTS_.mesureMPS).join(MesureTS_.sousSequenceMPS).join(SousSequenceTS_.sequenceMPS);
        Path<String> parcelleCode = s.join(SequenceTS_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        Join<ValeurTS, RealNode> varRn = v.join(ValeurTS_.realNode);
        Join<RealNode, RealNode> dtyRn = varRn.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = dtyRn.join(RealNode_.parent).join(RealNode_.parent);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurTS_.value);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(SequenceTS_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                dateMesure,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                builder.avg(valeur),
                                idNode
                        )
                )
                .where(
                        builder.equal(node.get(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

}
