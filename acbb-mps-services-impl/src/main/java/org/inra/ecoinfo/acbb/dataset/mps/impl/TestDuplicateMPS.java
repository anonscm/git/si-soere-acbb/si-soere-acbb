/**
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.impl;

import org.inra.ecoinfo.acbb.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The Class TestDuplicateSemis.
 *
 * implementation for semis of {@link AbstractTestDuplicate}
 *
 * test the existence of duplicates in semis files
 *
 * @author Tcherniatinsky Philippe
 */
public class TestDuplicateMPS extends AbstractTestDuplicate {

    /**
     * The Constant ACBB_DATASET_SEM_BUNDLE_NAME.
     */
    public static final String ACBB_DATASET_MPS_BUNDLE_NAME = "org.inra.ecoinfo.acbb.dataset.mps.messages";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE @link(String).
     */
    static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The Constant KEY_PATTERN @link(String).
     */
    static final String KEY_PATTERN = "%s_%s_%s";

    /**
     * The line @link(SortedMap<String,Long>).
     */
    final SortedMap<String, Long> line;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateMPS() {
        this.line = new TreeMap();
    }

    /**
     * Adds the line.
     *
     * @param parcelleString
     * @param dateTimeString
     * @link(String) the parcelle string
     * @param traitementString
     * @link(String)
     * @link(String) the date debut traitement string
     * @param dateString
     * @link(String) the date string
     * @param timeString
     * @link(String) the time string
     * @param lineNumber
     *            the line number
     */
    protected void addLine(final String parcelleString, final String traitementString,
                           final String dateTimeString, final String dateString, final String timeString,
                           final long lineNumber) {
        final String key = String.format(TestDuplicateMPS.KEY_PATTERN, parcelleString,
                traitementString, dateTimeString);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderACBB.getACBBMessageWithBundle(
                    TestDuplicateMPS.ACBB_DATASET_MPS_BUNDLE_NAME,
                    TestDuplicateMPS.PROPERTY_MSG_DOUBLON_LINE), lineNumber, parcelleString,
                    traitementString, dateString, timeString, this.line.get(key)));
        }
    }

    /**
     * Adds the line.
     *
     * @param values
     * @param versionFile
     * @param dates
     * @link(String[]) the values
     * @param lineNumber
     *            long the line number {@link String[]} the values
     */
    @Override
    public void addLine(final String[] values, final long lineNumber, String[] dates,
                        VersionFile versionFile) {
        String dateString = values[2];
        String timeString = values[3];
        String dateTimeString = String.format("%-"
                        + RecorderACBB.DD_MM_YYYY_HHMMSS_READ.length() + "."
                        + RecorderACBB.DD_MM_YYYY_HHMMSS_READ.length() + "s",
                dateString + timeString + ":00");
        this.addLine(values[0], values[1], dateTimeString, dateString, timeString, lineNumber + 1);
        if (dates != null) {
            this.testLineForDuplicatesDateInDB(dates, dateString, timeString, lineNumber,
                    versionFile);
        }
    }

    /**
     *
     * @param sequenceMPSDAO
     */
    public void setSequenceMPSDAO(ISequenceMPSDAO sequenceMPSDAO) {
    }

    /**
     *
     * @param dateDBString
     * @param timeDBString
     * @param lineNumber
     * @param versionFile
     */
    @Override
    protected void testLineForDuplicatesLineinDb(String dateDBString, String timeDBString,
                                                 long lineNumber, VersionFile versionFile) {
        /*
         * Date date; try { date = DateUtil.getSimpleDateFormatDateLocale().parse(dateDBString);
         * DateUtil.getSimpleDateFormatTimeLocale().parse(timeDBString + ":00"); SequenceMPS
         * sequenceMPS = sequenceMPSDAO.getSequence(date, versionFile); if (sequenceMPS != null) {
         * for (Object sousSequenceMPS : sequenceMPS.getSousSequencesMPS()) { if (((SousSequenceMPS)
         * sousSequenceMPS).get.equals(time)) { errorsReport.addErrorMessage("duplicate Date"); } }
         *
         * } } catch (DateTimeException e) { LOGGER.info("can't test line", e); }
         */
    }
}
