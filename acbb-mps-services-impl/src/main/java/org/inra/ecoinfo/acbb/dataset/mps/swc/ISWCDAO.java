/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.swc;

import org.inra.ecoinfo.acbb.dataset.mps.IMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.MesureSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SequenceSWC;

/**
 * The Interface ISWCDAO.
 */
@SuppressWarnings("rawtypes")
public interface ISWCDAO extends IMPSDAO<SequenceSWC, MesureSWC> {
}
