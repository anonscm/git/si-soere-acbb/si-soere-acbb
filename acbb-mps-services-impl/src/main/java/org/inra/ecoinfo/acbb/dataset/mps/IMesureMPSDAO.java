/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;

import java.time.LocalTime;
import java.util.Optional;

/**
 * The Interface IMesureMPSDAO.
 *
 * @param <SS5> the generic type
 * @param <M4>  the generic type
 */
@SuppressWarnings("rawtypes")
public interface IMesureMPSDAO<SS5 extends SousSequenceMPS, M4 extends MesureMPS> extends IDAO<M4> {

    /**
     * Gets the by n key.
     *
     * @param sousSequence the sous sequence
     * @param heure        the heure
     * @return the by n key
     */
    Optional<M4> getByNKey(SS5 sousSequence, LocalTime heure);

    /**
     * Gets the line publication name doublon.
     *
     * @param mesure the mesure
     * @return the line publication name doublon
     */
    Object[] getLinePublicationNameDoublon(M4 mesure);
}
