/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.swc.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SequenceSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SequenceSWC_;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Optional;

/**
 * The Class JPASequenceSWCDAO.
 */
public class JPASequenceSWCDAO extends AbstractJPADAO<SequenceSWC> implements
        ISequenceMPSDAO<SequenceSWC> {

    /**
     * Gets the by nkey.
     *
     * @param version
     * @param suiviParcelle
     * @param date
     * @return the by nkey @see
     * org.inra.ecoinfo.acbb.dataset.mps.ISequenceMPSDAO#getByNkey(org.inra.
     * ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle, java.time.LocalDateTime)
     * @link(VersionFile) the version
     * @link(SuiviParcelle) the suivi parcelle
     * @link(Date) the date
     */
    @Override
    public Optional<SequenceSWC> getByNkey(final VersionFile version, final SuiviParcelle suiviParcelle,
                                           final LocalDate date) {
        CriteriaQuery<SequenceSWC> query = builder.createQuery(SequenceSWC.class);
        Root<SequenceSWC> sswc = query.from(SequenceSWC.class);
        query.where(
                builder.equal(sswc.join(SequenceSWC_.version), version),
                builder.equal(sswc.join(SequenceSWC_.suiviParcelle), suiviParcelle),
                builder.equal(sswc.get(SequenceSWC_.date), date)
        );
        query.select(sswc);
        return getOptional(query);
    }

    /**
     * @param date
     * @param versionFile
     * @return
     */
    @Override
    public Optional<SequenceSWC> getSequence(LocalDate date, VersionFile versionFile) {
        CriteriaQuery<SequenceSWC> query = builder.createQuery(SequenceSWC.class);
        Root<SequenceSWC> sswc = query.from(SequenceSWC.class);
        query.where(
                builder.equal(sswc.join(SequenceSWC_.version), versionFile),
                builder.equal(sswc.get(SequenceSWC_.date), date)
        );
        query.select(sswc);
        return getOptional(query);
    }
}
