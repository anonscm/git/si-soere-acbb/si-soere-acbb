package org.inra.ecoinfo.acbb.dataset.mps.swc.jpa;

import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.ISWCDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.MesureSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SequenceSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.SousSequenceSWC;
import org.inra.ecoinfo.acbb.dataset.mps.swc.entity.ValeurSWC;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.swc.SynthesisValue;

/**
 * The Class JPASWCDAO.
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class JPASWCDAO extends JPAMPSDAO<SequenceSWC, MesureSWC> implements ISWCDAO {

    /**
     *
     */
    public static final String VARIABLE_SWC_AFFICHAGE = "SWC";

    /**
     * @return
     */
    @Override
    protected String getVariableAffichage() {
        return VARIABLE_SWC_AFFICHAGE;
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected Class<SequenceSWC> getSequenceEntityClass() {
        return SequenceSWC.class;
    }

    @Override
    protected Class<? extends SousSequenceMPS> getSousSequenceEntityClass() {
        return SousSequenceSWC.class;
    }

    @Override
    protected Class<MesureSWC> getMesureEntityClass() {
        return MesureSWC.class;
    }

    @Override
    protected Class<? extends ValeurMPS> getValeurEntityClass() {
        return ValeurSWC.class;
    }
}
