package org.inra.ecoinfo.acbb.dataset.mps.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.mps.ExpectedColumn;
import org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class TestHeadersFlux.
 *
 * @author Tcherniatinsky Philippe
 */
public class TestHeadersMPS extends GenericTestHeader {

    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * Instantiates a new test headers flux.
     */
    public TestHeadersMPS() {
        super();
    }

    private long addUnexpectedColumn(Matcher matcher, BadsFormatsReport badsFormatsReport,
                                     DatasetDescriptor datasetDescriptor, String value, int index,
                                     IRequestPropertiesMPS requestPropertiesMPS, String[] values, long lineNumber,
                                     String[] valuesMin, String[] valuesMax) {
        long returnLineNumber = lineNumber;
        boolean hasQualityClass;
        int nombreRepetition;
        int profondeur;
        Float minValue = null;
        Float maxValue = null;
        nombreRepetition = Integer.parseInt(matcher.group(2));
        profondeur = Integer.parseInt(matcher.group(3));
        hasQualityClass = false;
        maxValue = this.testValuesMax(badsFormatsReport, index, valuesMax, returnLineNumber,
                maxValue);
        minValue = this.testValuesmin(badsFormatsReport, index, lineNumber, valuesMin, minValue);
        this.testQualityClass(datasetDescriptor, value, index, requestPropertiesMPS, values,
                hasQualityClass, nombreRepetition, profondeur, minValue, maxValue);
        return returnLineNumber;
    }

    /**
     * Gets the datatype name.
     *
     * @param datasetDescriptor
     * @return the datatype name
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     */
    String getDatatypeName(final DatasetDescriptorACBB datasetDescriptor) {
        return datasetDescriptor.getName();
    }

    /**
     * Gets the matcher.
     *
     * @param nomVariable       the nom variable
     * @param datasetDescriptor
     * @return the matcher
     * @link(DatasetDescriptor) the dataset descriptor
     */
    public Matcher getMatcher(final String nomVariable, final DatasetDescriptor datasetDescriptor) {
        return Pattern.compile(this.getVariablePattern(datasetDescriptor)).matcher(nomVariable);
    }

    /**
     * Gets the variable name.
     *
     * @param datasetDescriptor
     * @return the variable name
     * @link(DatasetDescriptor) the dataset descriptor
     * @link(DatasetDescriptor) the dataset descriptor
     */
    protected String getVariableName(final DatasetDescriptor datasetDescriptor) {
        return datasetDescriptor == null ? null : ((DatasetDescriptorACBB) datasetDescriptor)
                .getVariableName();
    }

    /**
     * Gets the variable pattern.
     *
     * @param datasetDescriptor
     * @return the variable pattern
     * @link(DatasetDescriptor) the dataset descriptor
     * @link(DatasetDescriptor) the dataset descriptor
     */
    public String getVariablePattern(final DatasetDescriptor datasetDescriptor) {
        return String.format(RecorderACBB.PATTERN_VARIABLE_EUROPEAN_NOM,
                this.getVariableName(datasetDescriptor),
                RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC);
    }

    private boolean hasQualityClass(Matcher matcher) {
        boolean hasQC = false;
        if (matcher.matches()) {
            hasQC = RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC.equals(matcher.group(0));
            hasQC = hasQC && matcher.group(1) == null;
            hasQC = hasQC && matcher.group(2) == null;
            hasQC = hasQC && matcher.group(3) == null;
        }
        return hasQC;
    }

    /**
     * Read line header.
     *
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param datasetDescriptor
     * @param requestPropertiesMPS
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(BadsFormatsReport) the bads formats report
     * @link(CSVParser) the parser
     * @link(long) the line number
     * @link(DatasetDescriptor) the dataset descriptor
     * @link(IRequestPropertiesMPS) the request properties mps
     */
    long readLineHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser,
                        final long lineNumber, final DatasetDescriptor datasetDescriptor,
                        final IRequestPropertiesMPS requestPropertiesMPS) throws IOException {
        int index = 0;
        long returnLineNumber = lineNumber;
        // On parcourt chaque colonne d'une ligne
        final String[] values = parser.getLine();
        returnLineNumber++;
        String[] valuesMax = parser.getLine();
        returnLineNumber++;
        String[] valuesMin = parser.getLine();
        returnLineNumber++;
        String value;
        ExpectedColumn expectedColumn;
        Matcher matcher = null;
        for (index = 0; index < values.length; index++) {
            value = values[index];
            if (org.apache.commons.lang.StringUtils.EMPTY.equals(value)) {
                break;
            }
            if (index < datasetDescriptor.getUndefinedColumn()) {
                if (!datasetDescriptor.getColumns().get(index).getName().equals(value)) {
                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderACBB
                                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_COLUMN_HEADER),
                                            returnLineNumber, index + 1, value, datasetDescriptor
                                                    .getColumns().get(index).getName())));
                }
                continue;
            }
            expectedColumn = requestPropertiesMPS.getPossiblesColums().get(value);
            if (expectedColumn == null) {
                matcher = this.getMatcher(value, datasetDescriptor);
                if (!matcher.matches() || matcher.group(1) == null || matcher.group(2) == null
                        || matcher.group(3) == null) {
                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderACBB
                                                    .getACBBMessage(RecorderACBB.PROPERTY_MSG_MISMACH_COLUMN_HEADER_EUROPEAN_NOM),
                                            returnLineNumber, index + 1, value, this
                                                    .getVariableName(datasetDescriptor))));
                    continue;
                }
                returnLineNumber = this.addUnexpectedColumn(matcher, badsFormatsReport,
                        datasetDescriptor, value, index, requestPropertiesMPS, values,
                        returnLineNumber, valuesMin, valuesMax);
            }
        }
        return returnLineNumber;
    }

    /**
     * Test headers.
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return the long
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.acbb.dataset.ITestHeaders#testHeaders(com.Ostermiller
     *                           .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *                           org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB, java.lang.String,
     *                           org.inra.ecoinfo.dataset.BadsFormatsReport,
     *                           org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the request properties
     * @link(String) the encoding
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
                            final IRequestPropertiesACBB requestProperties, final String encoding,
                            final BadsFormatsReport badsFormatsReport, final DatasetDescriptorACBB datasetDescriptor)
            throws BusinessException {
        super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport,
                datasetDescriptor);
        final IRequestPropertiesMPS requestPropertiesMPS = (IRequestPropertiesMPS) requestProperties;
        requestPropertiesMPS.initDate();
        long lineNumber = 0;
        try {
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    Utils.createCodeFromString(this.getDatatypeName(datasetDescriptor)));
            lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber,
                    requestProperties);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, requestProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, requestProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, requestPropertiesMPS);
        } catch (final IOException e) {
            this.getLogger().debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }

    private void testQualityClass(DatasetDescriptor datasetDescriptor, String value, int index,
                                  IRequestPropertiesMPS requestPropertiesMPS, String[] values, boolean hasQualityClass,
                                  int nombreRepetition, int profondeur, Float minValue, Float maxValue) {
        Matcher matcher;
        String nextValue;
        if (index + 1 < values.length) {
            nextValue = values[index + 1];
            matcher = this.getMatcher(nextValue, datasetDescriptor);
            if (this.hasQualityClass(matcher)) {
                hasQualityClass = true;
                index++;
            }
            requestPropertiesMPS.addExpectedColum(index - (hasQualityClass ? 1 : 0), value,
                    hasQualityClass, profondeur, nombreRepetition, minValue, maxValue);
        }
    }

    private Float testValuesMax(BadsFormatsReport badsFormatsReport, int index, String[] valuesMax,
                                long returnLineNumber, Float maxValue) {
        try {
            if (valuesMax[index] != null && !org.apache.commons.lang.StringUtils.EMPTY.equals(valuesMax[index])) {
                maxValue = Float.parseFloat(valuesMax[index]);
            }
        } catch (final NumberFormatException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_BAD_MAX_VALUE_FORMAT),
                    returnLineNumber + 1, index + 1, valuesMax[index])));
        }
        return maxValue;
    }

    private Float testValuesmin(BadsFormatsReport badsFormatsReport, int index, long lineNumber,
                                String[] valuesMin, Float minValue) {
        try {
            if (valuesMin[index] != null && !org.apache.commons.lang.StringUtils.EMPTY.equals(valuesMin[index])) {
                minValue = Float.parseFloat(valuesMin[index]);
            }
        } catch (final NumberFormatException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderACBB.getACBBMessage(RecorderACBB.PROPERTY_MSG_BAD_MIN_VALUE_FORMAT),
                    lineNumber + 2, index + 1, valuesMin[index])));
        }
        return minValue;
    }

}
