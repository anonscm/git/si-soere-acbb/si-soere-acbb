/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps.impl;

import org.inra.ecoinfo.acbb.dataset.mps.smp.ISMPDAO;
import org.inra.ecoinfo.acbb.dataset.mps.swc.ISWCDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.ITSDAO;
import org.inra.ecoinfo.acbb.extraction.mps.IMPSDatasetManager;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.*;

/**
 * The Class MPSDatasetManager.
 */
public class MPSDatasetManager extends DefaultDatasetManager implements IMPSDatasetManager {

    /**
     *
     */
    public static final String VARIABLE_SWC_CODE = "humidite_volumique_du_sol";

    /**
     * The Constant VARIABLE_SMP_CODE.
     */
    public static final String VARIABLE_SMP_CODE = "tension_de_l_eau_dans_le_sol";

    /**
     * The Constant VARIABLE_TS_CODE.
     */
    public static final String VARIABLE_TS_CODE = "temperature_du_sol";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The org.inra.ecoinfo.acbb.dataset.smp dao @link(ISMPDAO).
     */
    ISMPDAO smpDAO;

    /**
     * The org.inra.ecoinfo.acbb.dataset.swc dao @link(ISWCDAO).
     */
    ISWCDAO swcDAO;

    /**
     * The ts dao @link(ITSDAO).
     */
    ITSDAO tsDAO;

    /**
     * Instantiates a new mPS dataset manager.
     */
    public MPSDatasetManager() {
        super();
    }

    /**
     * Gets the availables depth by traitement and variables.
     *
     * @param selectedTraitements
     * @param intervals
     * @param variables
     * @return the availables depth by traitement and variables @see
     * org.inra.ecoinfo.acbb.extraction.mps.IMPSDatasetManager#
     * getAvailablesDepthByTraitementAndVariables(java.util.List,
     * java.util.List, java.util.List)
     * @link(List<TraitementProgramme>) the selected traitements
     * @link(List<IntervalDate>) the intervals
     * @link(List<VariableACBB>) the variables
     */
    @Override
    public List<Integer> getAvailablesDepthByTraitementAndVariables(
            final List<TraitementProgramme> selectedTraitements,
            final List<IntervalDate> intervals, final List<VariableACBB> variables) {
        final Set<Integer> availablesDephts = new TreeSet();
        for (final VariableACBB variable : variables) {
            if (variable.getCode().equals(VARIABLE_SMP_CODE)) {
                availablesDephts.addAll(this.smpDAO.getAvailablesProfondeursBy(selectedTraitements,
                        intervals, this.policyManager.getCurrentUser(), policyManager.getCurrentUser()));
            }
            if (variable.getCode().equals(VARIABLE_SWC_CODE)) {
                availablesDephts.addAll(this.swcDAO.getAvailablesProfondeursBy(selectedTraitements,
                        intervals, this.policyManager.getCurrentUser(), policyManager.getCurrentUser()));
            }
            if (variable.getCode().equals(VARIABLE_TS_CODE)) {
                availablesDephts.addAll(this.tsDAO.getAvailablesProfondeursBy(selectedTraitements,
                        intervals, this.policyManager.getCurrentUser(), policyManager.getCurrentUser()));
            }
        }
        return new LinkedList<>(availablesDephts);
    }

    /**
     * @param selectedTraitements
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByTreatmentAndDatesInterval(List<TraitementProgramme> selectedTraitements, List<IntervalDate> intervals) {
        final Map<String, List<NodeDataSet>> availablesVariables = new HashMap();
        List<NodeDataSet> availableVariableACBB;
        availableVariableACBB = this.smpDAO.getAvailableVariableByTraitement(selectedTraitements,
                intervals, this.policyManager.getCurrentUser());
        if (availableVariableACBB != null) {
            availablesVariables.put(VARIABLE_SMP_CODE, availableVariableACBB);
        }
        availableVariableACBB = this.swcDAO.getAvailableVariableByTraitement(selectedTraitements,
                intervals, this.policyManager.getCurrentUser());
        if (availableVariableACBB != null) {
            availablesVariables.put(VARIABLE_SWC_CODE, availableVariableACBB);
        }
        availableVariableACBB = this.tsDAO.getAvailableVariableByTraitement(selectedTraitements,
                intervals, this.policyManager.getCurrentUser());
        if (availableVariableACBB != null) {
            availablesVariables.put(VARIABLE_TS_CODE, availableVariableACBB);
        }
        return availablesVariables;
    }

    /**
     * Gets the available traitements.
     *
     * @param intervals
     * @return the available traitements @see
     * org.inra.ecoinfo.acbb.extraction.mps.IMPSDatasetManager#
     * getAvailableTraitements(java.util.List)
     * @link(List<IntervalDate>) the intervals
     */
    @Override
    public List<TraitementProgramme> getAvailableTraitements(final List<IntervalDate> intervals) {
        final List<TraitementProgramme> availablesTraitements = new LinkedList();
        availablesTraitements.addAll(this.smpDAO.getAvailablesTraitementsForPeriode(intervals, policyManager.getCurrentUser()));
        availablesTraitements.addAll(this.swcDAO.getAvailablesTraitementsForPeriode(intervals, policyManager.getCurrentUser()));
        availablesTraitements.addAll(this.tsDAO.getAvailablesTraitementsForPeriode(intervals, policyManager.getCurrentUser()));
        return availablesTraitements;
    }

    /**
     * Sets the security context @link(IPolicyManager).
     *
     * @param policyManager the new security context @link(IPolicyManager)
     * @see org.inra.ecoinfo.MO#setPolicyManager(org.inra.ecoinfo.security .
     * IPolicyManager)
     */
    @Override
    public final void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset.smp dao.
     *
     * @param smpDAO the new org.inra.ecoinfo.acbb.dataset.smp dao
     */
    public final void setSmpDAO(final ISMPDAO smpDAO) {
        this.smpDAO = smpDAO;
    }

    /**
     * Sets the org.inra.ecoinfo.acbb.dataset.swc dao.
     *
     * @param swcDAO the new org.inra.ecoinfo.acbb.dataset.swc dao
     */
    public final void setSwcDAO(final ISWCDAO swcDAO) {
        this.swcDAO = swcDAO;
    }

    /**
     * Sets the ts dao.
     *
     * @param tsDAO the new ts dao
     */
    public final void setTsDAO(final ITSDAO tsDAO) {
        this.tsDAO = tsDAO;
    }
}
