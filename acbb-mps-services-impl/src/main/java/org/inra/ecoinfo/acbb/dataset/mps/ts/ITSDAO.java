/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.ts;

import org.inra.ecoinfo.acbb.dataset.mps.IMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.MesureTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SequenceTS;

/**
 * The Interface ITSDAO.
 */
@SuppressWarnings("rawtypes")
public interface ITSDAO extends IMPSDAO<SequenceTS, MesureTS> {
}
