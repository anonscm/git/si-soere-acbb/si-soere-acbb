/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.ts.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.*;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalTime;
import java.util.Optional;

/**
 * The Class JPAMesureTSDAO.
 */
public class JPAMesureTSDAO extends AbstractJPADAO<MesureTS> implements
        IMesureMPSDAO<SousSequenceTS, MesureTS> {

    /**
     * Gets the by n key.
     *
     * @param sousSequence
     * @param heure
     * @return the by n key @see
     * org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO#getByNKey(org.inra.ecoinfo
     * .acbb.dataset.mps.entity.SousSequenceMPS, java.time.LocalDateTime)
     * @link(SousSequenceTS) the sous sequence
     * @link(Date) the heure
     */
    @Override
    public Optional<MesureTS> getByNKey(final SousSequenceTS sousSequence, final LocalTime heure) {
        CriteriaQuery<MesureTS> query = builder.createQuery(MesureTS.class);
        Root<MesureTS> m = query.from(MesureTS.class);
        query.where(
                builder.equal(m.join(MesureTS_.sousSequenceMPS), sousSequence),
                builder.equal(m.get(MesureTS_.heure), heure)
        );
        query.select(m);
        return getOptional(query);
    }

    /**
     * Gets the line publication name doublon.
     *
     * @param mesure
     * @return the line publication name doublon @see
     * org.inra.ecoinfo.acbb.dataset.mps.IMesureMPSDAO#getLinePublicationNameDoublon
     * (org.inra.ecoinfo.acbb.dataset.mps.entity.MesureMPS)
     * @link(MesureTS) the mesure
     */
    @Override
    public Object[] getLinePublicationNameDoublon(final MesureTS mesure) {
        this.beginNewTransaction();
        CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
        Root<MesureTS> m = query.from(MesureTS.class);
        query.where(
                builder.equal(m.join(MesureTS_.sousSequenceMPS).join(SousSequenceTS_.sequenceMPS).join(SequenceTS_.suiviParcelle), mesure.getSousSequenceMPS().getSequenceMPS().getSuiviParcelle()),
                builder.equal(m.get(MesureTS_.heure), mesure.getHeure()),
                builder.equal(m.join(MesureTS_.sousSequenceMPS).get(SousSequenceTS_.profondeur), mesure.getSousSequenceMPS().getProfondeur()),
                builder.equal(m.join(MesureTS_.sousSequenceMPS).join(SousSequenceTS_.sequenceMPS).get(SequenceTS_.date), mesure.getSousSequenceMPS().getSequenceMPS().getDate())
        );
        query.multiselect(m.join(MesureTS_.sousSequenceMPS).join(SousSequenceTS_.sequenceMPS).join(SequenceTS_.version).join(VersionFile_.dataset).get(Dataset_.id), m.get(MesureTS_.lineNumber));
        return getFirstOrNull(query);
    }
}
