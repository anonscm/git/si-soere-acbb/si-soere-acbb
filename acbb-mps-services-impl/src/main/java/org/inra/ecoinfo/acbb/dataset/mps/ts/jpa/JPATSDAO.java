package org.inra.ecoinfo.acbb.dataset.mps.ts.jpa;

import org.inra.ecoinfo.acbb.dataset.mps.entity.SousSequenceMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAMPSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.ITSDAO;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.MesureTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.SousSequenceTS;
import org.inra.ecoinfo.acbb.dataset.mps.ts.entity.ValeurTS;
import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;
import org.inra.ecoinfo.acbb.synthesis.ts.SynthesisValue;

/**
 * The Class JPATSDAO.
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class JPATSDAO extends JPAMPSDAO<SequenceTS, MesureTS> implements ITSDAO {

    /**
     *
     */
    public static final String VARIABLE_TS_AFFICHAGE = "TS";

    /**
     * @return
     */
    @Override
    protected String getVariableAffichage() {
        return VARIABLE_TS_AFFICHAGE;
    }

    /**
     * @return
     */
    @Override
    protected Class<? extends SynthesisValueWithParcelle> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected Class<SequenceTS> getSequenceEntityClass() {
        return SequenceTS.class;
    }

    @Override
    protected Class<? extends SousSequenceMPS> getSousSequenceEntityClass() {
        return SousSequenceTS.class;
    }

    @Override
    protected Class<MesureTS> getMesureEntityClass() {
        return MesureTS.class;
    }

    @Override
    protected Class<? extends ValeurMPS> getValeurEntityClass() {
        return ValeurTS.class;
    }
}
