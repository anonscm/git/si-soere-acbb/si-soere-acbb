/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.synthesis;

import org.inra.ecoinfo.acbb.dataset.mps.smp.entity.*;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle_;
import org.inra.ecoinfo.acbb.synthesis.smp.SynthesisDatatype;
import org.inra.ecoinfo.acbb.synthesis.smp.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author tcherniatinsky
 */
public class SMPSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurSMP> v = query.from(ValeurSMP.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<SousSequenceSMP, SequenceSMP> s = v.join(ValeurSMP_.mesureMPS).join(MesureSMP_.sousSequenceMPS).join(SousSequenceSMP_.sequenceMPS);
        Path<String> parcelleCode = s.join(SequenceSMP_.suiviParcelle).join(SuiviParcelle_.parcelle).get(Nodeable_.code);
        Join<ValeurSMP, RealNode> varRn = v.join(ValeurSMP_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurSMP_.value);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = s.get(SequenceSMP_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                dateMesure,
                                sitePath,
                                parcelleCode,
                                variableCode,
                                builder.avg(valeur),
                                idNode
                        )
                )
                .where(
                        builder.equal(node.get(NodeDataSet_.realNode), varRn),
                        builder.or(
                                builder.isNull(valeur),
                                builder.gt(valeur, -9999)
                        )
                )
                .groupBy(
                        dateMesure,
                        sitePath,
                        parcelleCode,
                        variableCode,
                        dateMesure,
                        idNode
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(parcelleCode),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

}
