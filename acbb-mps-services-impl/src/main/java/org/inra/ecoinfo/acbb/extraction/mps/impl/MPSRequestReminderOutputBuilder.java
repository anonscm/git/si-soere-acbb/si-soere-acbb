/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps.impl;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.extraction.AbstractRequestReminder;
import org.inra.ecoinfo.acbb.extraction.cst.ConstantACBBExtraction;
import org.slf4j.MDC;

/**
 * The Class MPSRequestReminderOutputBuilder.
 */
public class MPSRequestReminderOutputBuilder extends AbstractRequestReminder {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.acbb.extraction.mps.messages";
    /**
     * The Constant CST_REQUEST_REMINDER @link(String).
     */
    static final String CST_REQUEST_REMINDER = "request_reminder";
    /**
     * The Constant PROPERTY_MSG_HEADER @link(String).
     */
    static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DATES @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_DATES = "PROPERTY_MSG_SELECTED_DATES";
    /**
     * The Constant PROPERTY_MSG_SELECTED_TREATMENT @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_TREATMENT = "PROPERTY_MSG_SELECTED_TREATMENT";
    /**
     * The Constant PROPERTY_MSG_SELECTED_VARIABLE @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_VARIABLE = "PROPERTY_MSG_SELECTED_VARIABLE";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DEPTH @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_DEPTH = "PROPERTY_MSG_SELECTED_DEPTH";
    /**
     * The Constant PROPERTY_MSG_SELECTED_DISPLAY @link(String).
     */
    static final String PROPERTY_MSG_SELECTED_DISPLAY = "PROPERTY_MSG_SELECTED_DISPLAY";
    /**
     * The Constant PROPERTY_MSG_COMMENT @link(String).
     */
    static final String PROPERTY_MSG_COMMENT = "PROPERTY_MSG_COMMENT";
    /**
     * The Constant PROPERTY_MSG_EUROPEAN_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_EUROPEAN_FORMAT = "PROPERTY_MSG_EUROPEAN_FORMAT";
    /**
     * The Constant PROPERTY_MSG_CLASSIC_FORMAT @link(String).
     */
    static final String PROPERTY_MSG_CLASSIC_FORMAT = "PROPERTY_MSG_CLASSIC_FORMAT";
    private static final Logger LOGGER = LoggerFactory.getLogger(MPSRequestReminderOutputBuilder.class.getName());

    /**
     * Builds the output data.
     *
     * @param parameters
     * @param outputPrintStreamMap
     * @link(IParameter) the parameters
     * @link(Map<String,PrintStream>) the output print stream map @see
     * org.inra.ecoinfo.acbb.extraction .mps.impl.MPSOutputsBuildersResolver#
     * buildOutputData(org.inra.ecoinfo.extraction.IParameter, java.util.Map)
     */
    @Override
    protected void buildOutputData(final Map<String, Object> requestMetadatasMap,
            final Map<String, PrintStream> outputPrintStreamMap) {
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            final PrintStream outputStream = outputPrintStreamMap.get(datatypeNameEntry.getKey());
            buildOutputHeader(outputStream);
            this.outputPrintDates(outputStream, getDates(requestMetadatasMap));;
            this.outputPrintTraitements(outputStream, getTreatments(requestMetadatasMap));
            outputStream.println(RecorderACBB.getACBBMessageWithBundle(
                    BUNDLE_REMINDER_SOURCE_PATH,
                    PROPERTY_MSG_SELECTED_VARIABLES));

            List<VariableACBB> selectedMPSVariables = ((List<String>) requestMetadatasMap.get(ConstantACBBExtraction.DATATYPE_NAME_INDEX)).stream()
                    .sorted()
                    .map(code -> (List<VariableACBB>) requestMetadatasMap.get(code))
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
            this.outputPrintVariables(outputStream, selectedMPSVariables);

            MDC.put("variables", String.format("%s:( %s)", "MPS",
                    selectedMPSVariables.stream()
                            .map(v -> v.getAffichage())
                            .collect(Collectors.joining(", ")))
            );
            MPSParameterVO mpsParameter = (MPSParameterVO) requestMetadatasMap.get(IParameter.class.getSimpleName());
            this.outputPrintProfondeurs(outputStream, mpsParameter.getSelectedDepth());
            MDC.put("profondeurs", mpsParameter.getSelectedDepth().stream().map(p->p.toString()).collect(Collectors.joining(";"))
            );
            this.outputPrintAffichage(outputStream, mpsParameter.getAffichage());
            this.outputPrintFileComp(outputStream, getFileComp(requestMetadatasMap));
            this.outputPrintCommentaire(outputStream, getCommentaire(requestMetadatasMap));
        }
    }

    /**
     * Output print affichage.
     *
     * @param outputStream
     * @param affichage
     * @link(PrintStream) the output steam
     * @link(int) the affichage
     * @link(PrintStream) the output steam
     * @link(int) the affichage
     */
    void outputPrintAffichage(final PrintStream outputStream, final int affichage) {
        outputStream.println(this.getLocalizationManager().getMessage(
                MPSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                MPSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DISPLAY));
        outputStream.printf(
                "\t%s",
                affichage == 1 ? this.getLocalizationManager().getMessage(
                                MPSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                                MPSRequestReminderOutputBuilder.PROPERTY_MSG_EUROPEAN_FORMAT) : this
                                .getLocalizationManager().getMessage(
                                        MPSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                                        MPSRequestReminderOutputBuilder.PROPERTY_MSG_CLASSIC_FORMAT));
        outputStream.println();
        outputStream.println();
    }

    /**
     * Output print profondeurs.
     *
     * @param outputStream
     * @param selectedDepth
     * @link(PrintStream) the output steam
     * @link(SortedSet<Integer>) the selected depth
     * @link(PrintStream) the output steam
     * @link(SortedSet<Integer>) the selected depth
     */
    void outputPrintProfondeurs(final PrintStream outputStream,
            final SortedSet<Integer> selectedDepth) {
        outputStream.println(this.getLocalizationManager().getMessage(
                MPSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                MPSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DEPTH));
        outputStream.print("\t");
        int i = 1;
        for (final Integer depth : selectedDepth) {
            outputStream.printf("%d%s", depth, i == selectedDepth.size() ? org.apache.commons.lang.StringUtils.EMPTY : ", ");
            i++;
        }
        outputStream.println();
        outputStream.println();
    }

    /**
     * Output print variables.
     *
     * @param outputStream
     * @param variables
     * @link(PrintStream) the output steam
     * @link(List<VariableACBB>) the variables
     * @link(PrintStream) the output steam
     * @link(List<VariableACBB>) the variables
     */
    void outputPrintVariables(final PrintStream outputStream, final List<VariableACBB> variables) {
        outputStream.println(this.getLocalizationManager().getMessage(
                MPSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                MPSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLE));
        final Properties propertiesNom = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        String localizedNom;
        for (final VariableACBB variable : variables) {
            localizedNom = propertiesNom.getProperty(variable.getName());
            outputStream.printf("\t%s (%s)", variable.getAffichage(),
                    Strings.isNullOrEmpty(localizedNom) ? variable.getName() : localizedNom);
            outputStream.println();
        }
        outputStream.println();
        outputStream.println();
    }

    @Override
    protected String getLocalBundle() {
        return BUNDLE_SOURCE_PATH;
    }
}
