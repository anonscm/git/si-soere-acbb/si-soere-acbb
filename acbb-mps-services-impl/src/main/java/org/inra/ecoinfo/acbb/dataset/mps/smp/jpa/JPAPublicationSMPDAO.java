/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.smp.jpa;


import org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAPublicationMPSDAO;

/**
 * The Class JPAPublicationSMPDAO.
 */
public class JPAPublicationSMPDAO extends JPAPublicationMPSDAO {

    /**
     * The Constant SMP_NAME_VARIABLE.
     */
    public static final String SMP_NAME_VARIABLE = "SMP";

    /**
     * Gets the variable.
     *
     * @return the variable
     * @see org.inra.ecoinfo.acbb.dataset.mps.jpa.JPAPublicationMPSDAO#getVariable()
     */
    @Override
    protected String getVariable() {
        return JPAPublicationSMPDAO.SMP_NAME_VARIABLE;
    }

}
