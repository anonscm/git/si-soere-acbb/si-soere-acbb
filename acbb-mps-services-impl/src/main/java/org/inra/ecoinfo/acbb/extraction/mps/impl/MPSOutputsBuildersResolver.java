/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.mps.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.inra.ecoinfo.acbb.dataset.itk.semis.entity.SemisCouvertVegetal;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS;
import org.inra.ecoinfo.acbb.dataset.mps.entity.ValeurMPS.ColumnHeader;
import org.inra.ecoinfo.acbb.extraction.itk.IITKDatasetManager;
import org.inra.ecoinfo.acbb.extraction.itk.impl.VegetalPeriode;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.acbb.utils.ACBBMessages;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.jobs.IStatusBar;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.zip.ZipOutputStream;

/**
 * The Class MPSOutputsBuildersResolver.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class MPSOutputsBuildersResolver extends AbstractOutputBuilder implements
        IOutputsBuildersResolver {

    /**
     * The LOGGER.
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(MPSOutputsBuildersResolver.class);
    /**
     * The Constant PATTERN_HEADER @link(String).
     */
    static final String PATTERN_HEADER = "%s;%s;%s;%s;%s(%s)";
    /**
     * The Constant EXTRACTION @link(String).
     */
    static final String EXTRACTION = "extraction";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant SEPARATOR_TEXT.
     */
    static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant EXTENSION_ZIP.
     */
    static final String EXTENSION_ZIP = ".zip";
    /**
     * The Constant FILENAME_REMINDER.
     */
    static final String FILENAME_REMINDER = "recapitulatif_extraction";
    /**
     * The Constant EXTENSION_CSV.
     */
    static final String EXTENSION_CSV = ".csv";
    /**
     * The Constant EXTENSION_TXT.
     */
    static final String EXTENSION_TXT = ".txt";
    /**
     * The Constant MAP_INDEX_0.
     */
    static final String MAP_INDEX_0 = "0";
    /**
     * The Constant REQUEST_REMINDER @link(String).
     */
    @SuppressWarnings("unused")
    static final String REQUEST_REMINDER = "RequestReminder";
    private static final int KO = 1_024;
    /**
     * The request reminder output builder.
     */
    MPSRequestReminderOutputBuilder requestReminderOutputBuilder;
    IITKDatasetManager itkDatasetManager;

    static Stream<Object[]> checkNonEmptyStream(Stream<Object[]> stream) throws NoSuchElementException {
        Iterator<Object[]> iterator = stream.iterator();
        if (iterator.hasNext()) {
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
        } else {
            throw new NoSuchElementException("empty stream");
        }
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException                                                 the business exception
     * @throws org.inra.ecoinfo.extraction.exception.NoExtractionResultException
     */
    public void buidOutputFile(final IParameter parameters) throws BusinessException,
            NoSuchElementException {
        Map<String, Stream<Object[]>> results = (Map<String, Stream<Object[]>>) parameters.getParameters().get("MPS");

        final Set<String> datatypeNames = results.keySet().stream()
                .map(s -> String.format("%s_%s", this.getPrefixFileName(), s))
                .collect(Collectors.toSet());
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                MPSOutputsBuildersResolver.SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        this.buildOutputData(parameters, outputPrintStreamMap, filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     * Builds the body.
     *
     * @param outputHelper the output helper
     */
    void buildBody(final BuildOutputHelper outputHelper) {
        outputHelper.resultStream.forEach(line -> {
            if (outputHelper.headers != null) {
                buildHeader(outputHelper);
                outputHelper.headers = null;
                outputHelper.printStream.println();
            }
            outputHelper.printStream.print(line.getSite());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getParcelle());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getBloc());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getRepetition());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getTraitement());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getCouvert());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getDatedesemis());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getDatederecolte());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getDate());
            outputHelper.printStream.printf(ACBBMessages.PATTERN_1_FIELD, line.getHeure());
            for (int i = 0; i < line.valeurs.size(); i++) {
                if ("NC".equals(line.valeurs.get(i))) {
                    outputHelper.printStream.print(";;");
                } else if ("null".equals(line.valeurs.get(i))) {
                    this.displayEmptyColumnVariable(outputHelper, line, i);
                } else {
                    this.displayColumnsVariable(outputHelper, line, i);
                }
            }
            outputHelper.printStream.println();
        });
    }

    /**
     * Builds the header.
     *
     * @param outputHelper the output helper
     */
    abstract void buildHeader(BuildOutputHelper outputHelper);

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     *                           org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     *                           .extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        RObuildZipOutputStream robuildZipOutputStream = null;
        try {
            robuildZipOutputStream = super.buildOutput(parameters);
            parameters.getParameters().put(MPSExtractor.CST_RESULTS, parameters.getResults());
            NoExtractionResultException noDataToExtract;
            try (ZipOutputStream zipOutputStream = robuildZipOutputStream.getZipOutputStream()) {
                noDataToExtract = null;
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                } finally {
                    Optional.ofNullable((StatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode()))
                            .ifPresent(sc -> sc.setProgress(100));
                }
                this.requestReminderOutputBuilder.buildOutput(parameters);
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters).getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error(e1.getMessage(), e1);
        }
        return robuildZipOutputStream;
    }

    /**
     * Builds the output data.
     *
     * @param parameters           the parameters
     * @param outputPrintStreamMap the output print stream map
     * @throws org.inra.ecoinfo.extraction.exception.NoExtractionResultException
     */
    protected void buildOutputData(final IParameter parameters,
                                   final Map<String, PrintStream> outputPrintStreamMap, Map<String, File> fileMap) throws NoExtractionResultException {
        int filesCount = outputPrintStreamMap.size();
        int counter2 = 0;
        IStatusBar statusBar = (IStatusBar) parameters.getParameters().get(parameters.getExtractionTypeCode());
        for (final Entry<String, PrintStream> datatypeNameEntry : outputPrintStreamMap.entrySet()) {
            try {
                final BuildOutputHelper buildOutputHelper = new BuildOutputHelper(
                        (MPSParameterVO) parameters, outputPrintStreamMap.get(datatypeNameEntry.getKey()), datatypeNameEntry.getKey());
                this.buildBody(buildOutputHelper);
            } catch (NoSuchElementException e) {
                LOGGER.info(String.format("no extraction result for %s", datatypeNameEntry.getKey()));
                fileMap.remove(datatypeNameEntry.getKey());
                continue;
            } finally {
                counter2++;
                final int localCounter = counter2;
                Optional.ofNullable(statusBar).ifPresent(sb -> sb.setProgress(5 + (95 / (filesCount)) * localCounter));
            }
        }
    }

    /**
     * Display columns variable.
     *
     * @param outputHelper the output helper
     * @param valeur       the valeur
     * @param column       the column
     */
    protected abstract void displayColumnsVariable(BuildOutputHelper outputHelper, BuildOutputHelper.Line line, int column);

    /**
     * Display empty column variable.
     *
     * @param outputHelper the output helper
     * @param valeur       the valeur
     * @param column       the column
     */
    protected abstract void displayEmptyColumnVariable(BuildOutputHelper outputHelper, BuildOutputHelper.Line line, int column);

    /**
     * Gets the extention.
     *
     * @return the extention
     */
    protected abstract String getExtention();

    /**
     * Gets the prefix file name.
     *
     * @return the prefix file name
     */
    protected abstract String getPrefixFileName();

    /**
     * Resolve outputs builders.
     *
     * @param metadatasMap
     * @return the list @see
     * org.inra.ecoinfo.extraction.IOutputsBuildersResolver#resolveOutputsBuilders
     * (java.util.Map)
     * @link(Map<String,Object>) the metadatas map
     */
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(final Map<String, Object> metadatasMap) {
        final List<IOutputBuilder> outputsBuilders = new LinkedList();
        outputsBuilders.add(this.requestReminderOutputBuilder);
        return outputsBuilders;
    }

    /**
     * @param itkDatasetManager
     */
    public void setItkDatasetManager(IITKDatasetManager itkDatasetManager) {
        this.itkDatasetManager = itkDatasetManager;
    }

    /**
     * Sets the request reminder output builder.
     *
     * @param requestReminderOutputBuilder the new request reminder output
     *                                     builder
     */
    public final void setRequestReminderOutputBuilder(
            final MPSRequestReminderOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager the new security context
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#setPolicyManager
     * (org.inra.ecoinfo.security.IPolicyManager)
     */
    @Override
    public final void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * The Class BuildOutputHelper.
     */
    protected class BuildOutputHelper {

        private final Stream<Line> resultStream;
        /**
         * The parameter.
         */
        MPSParameterVO parameter;
        /**
         * The print stream.
         */
        PrintStream printStream;
        /**
         * valeurs de résultats triées par lignes.
         */
        SortedMap<SuiviParcelle, SortedMap<LocalDateTime, SortedMap<LocalDateTime, SortedSet<ValeurMPS>>>> results = new TreeMap();
        /**
         * liste des colonnes ayant des qualityclass pondérées par versions.
         */
        SortedMap<String, SortedSet<Long>> hasQualityClass = new TreeMap();
        /**
         * liste des en tête de colonnes triée par variable, profondeur et
         * numRépétition.
         */
        List<String> headers = new LinkedList();
        SortedSet<ColumnHeader> columnsHeader = new TreeSet();
        Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> couverts;
        private int affichage = 0;

        /**
         * Instantiates a new builds the output helper.
         *
         * @param parameter   the parameter
         * @param printStream the print stream
         * @throws NoExtractionResultException the no extract data found
         *                                     exception
         */
        BuildOutputHelper(final MPSParameterVO parameter, final PrintStream printStream, String key)
                throws NoSuchElementException {
            super();
            this.parameter = parameter;
            this.printStream = printStream;
            this.couverts = (Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>>) parameter
                    .getParameters().get(SemisCouvertVegetal.class.getSimpleName());
            this.resultStream = ((Stream<Object[]>) checkNonEmptyStream(
                    ((Map<String, Query>) parameter.getParameters().get("MPS"))
                            .get(key.replaceAll(".*_", ""))
                            .getResultStream()
            ))
                    .map(line -> convert(line, headers))
                    .filter(l -> l.isValid());
            this.affichage = parameter.getAffichage();
        }

        public int getAffichage() {
            return affichage;
        }

        private Line convert(Object[] o, List<String> headers) {
            List<String> oo = Stream.of(o)
                    .map(s -> (String) s)
                    .collect(Collectors.toList());
            try {
                Line line = new Line(o);
                if (headers != null && headers.isEmpty()) {
                    headers.addAll(line.colonnesNames);
                }
                return line;
            } catch (IOException ex) {
                return new Line();
            }
        }

        public JsonNode getJSONNode(String jsonString) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = mapper.getFactory();
            JsonParser parser = factory.createParser(jsonString);
            return mapper.readTree(parser);
        }

        /**
         * @return the couverts
         */
        public Map<Parcelle, SortedMap<LocalDate, VegetalPeriode>> getCouverts() {
            return this.couverts;
        }

        class Line {

            private final List<String> valeurs = new ArrayList<>();
            private final List<String> qcs = new ArrayList<>();
            private boolean valid = false;
            private List<String> fields = new ArrayList<>();
            private List<String> colonnesNames = new ArrayList<>();
            private String site = null;
            private String parcelle = null;
            private String bloc = null;
            private String repetition = null;
            private String traitement = null;
            private String couvert = null;
            private String datedesemis = null;
            private String datederecolte = null;
            private String date = null;
            private String heure = null;

            protected Line(Object[] o) throws IOException {
                fields = Stream.of(o)
                        .map(s -> (String) s)
                        .collect(Collectors.toList());
                this.site = fields.get(1);
                this.parcelle = fields.get(2);
                this.bloc = fields.get(3);
                this.repetition = fields.get(4);
                this.traitement = fields.get(5);
                this.couvert = fields.get(6);
                this.datedesemis = fields.get(7);
                this.datederecolte = fields.get(8);
                this.date = fields.get(9);
                this.heure = fields.get(10);
                init();
                this.valid = true;
            }

            private Line() {
                super();
            }

            protected void init() throws IOException {
                colonnesNames = Stream.of(fields.get(0).split(",")).collect(Collectors.toList());
                JsonNode jsonValeurs = getJSONNode(fields.get(11));
                JsonNode jsonQC = getJSONNode(fields.get(12));
                for (String field : colonnesNames) {
                    if (jsonValeurs.hasNonNull(field)) {
                        final String valeurText = jsonValeurs.get(field).asText();
                        final String qcText = jsonQC.get(field).asText();
                        if ("null".equals(valeurText)) {
                            valeurs.add("null");
                            qcs.add("null");
                        } else {
                            valeurs.add(valeurText);
                            qcs.add(qcText);
                        }
                    } else {
                        valeurs.add("NC");
                        qcs.add("NC");
                    }
                }
            }

            public boolean isValid() {
                return valid;
            }

            public List<String> getFields() {
                return fields;
            }

            public List<String> getColonnesNames() {
                return colonnesNames;
            }

            public String getSite() {
                return site;
            }

            public String getParcelle() {
                return parcelle;
            }

            public String getBloc() {
                return bloc;
            }

            public String getRepetition() {
                return repetition;
            }

            public String getTraitement() {
                return traitement;
            }

            public String getCouvert() {
                return couvert;
            }

            public String getDatedesemis() {
                return datedesemis;
            }

            public String getDatederecolte() {
                return datederecolte;
            }

            public String getDate() {
                return date;
            }

            public String getHeure() {
                return heure;
            }

            public List<String> getValeurs() {
                return valeurs;
            }

            public List<String> getQcs() {
                return qcs;
            }

        }
    }
}
