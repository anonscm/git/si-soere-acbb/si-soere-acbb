/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.mps.impl;

import org.inra.ecoinfo.acbb.dataset.DatasetDescriptorACBB;
import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.ITestDuplicates;
import org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB;
import org.inra.ecoinfo.acbb.dataset.impl.RecorderACBB;
import org.inra.ecoinfo.acbb.dataset.mps.ExpectedColumn;
import org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class RequestPropertiesMPS.
 */
public class RequestPropertiesMPS extends AbstractRequestPropertiesACBB implements
        IRequestPropertiesMPS, Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     * The possible colums @link(Map<String,ExpectedColumn>).
     */
    final Map<String, ExpectedColumn> possibleColums = new HashMap();
    /**
     * The expected colums @link(Map<Integer,ExpectedColumn>).
     */
    final Map<Integer, ExpectedColumn> expectedColums = new TreeMap();
    /**
     * The datatype @link(String).
     */
    String datatype = null;

    /**
     * Instantiates a new request properties mps.
     */
    public RequestPropertiesMPS() {
        super();
    }

    /**
     * Adds the date.
     *
     * @param stringDate
     * @throws BadExpectedValueException the bad expected value exception
     * @throws DateTimeException         the parse exception
     * @link(String) the string date
     * @link(String) the string date
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#addDate
     * (java.lang.String)
     */
    @Override
    public final void addDate(final String stringDate) throws BadExpectedValueException,
            DateTimeException {
        try {
            if (this.getDates() != null && stringDate != null) {
                this.getDates().put(stringDate, true);
            }
        } catch (final Exception e) {
            String message = org.apache.commons.lang.StringUtils.EMPTY;
            String formatDate;
            String formatDateDebut;
            String formatDateFin;
            formatDate = DateUtil.getUTCDateTextFromLocalDateTime(DateUtil.readLocalDateFromText(stringDate, RecorderACBB.YYYYMMJJHHMMSS), DateUtil.DD_MM_YYYY_HH_MM);
            formatDateDebut = DateUtil.getUTCDateTextFromLocalDateTime(this.getDateDeDebut(), DateUtil.DD_MM_YYYY);
            formatDateFin = DateUtil.getUTCDateTextFromLocalDateTime(this.getDateDeFin(), DateUtil.DD_MM_YYYY);
            message = String.format(
                    this.getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_DATE_OFF_LIMITS), formatDate,
                    formatDateDebut, formatDateFin);
            throw new BadExpectedValueException(message, e);
        }
    }

    /**
     * Adds the expected colum.
     *
     * @param index
     * @param name
     * @param hasQualityClass
     * @param profondeur
     * @param numeroRepetition
     * @param minValue
     * @param maxValue
     * @link(int) the index
     * @link(String) the name
     * @link(boolean) the has quality class
     * @link(int) the profondeur
     * @link(int) the numero repetition
     * @link(Float) the min value
     * @link(Float) the max value
     * @link(int) the index
     * @link(String) the name
     * @link(boolean) the has quality class
     * @link(int) the profondeur
     * @link(int) the numero repetition
     * @link(Float) the min value
     * @link(Float) the max value
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#addExpectedColum (int,
     * java.lang.String, boolean, int, int, java.lang.Float, java.lang.Float)
     */
    @Override
    public final void addExpectedColum(final int index, final String name,
                                       final boolean hasQualityClass, final int profondeur, final int numeroRepetition,
                                       final Float minValue, final Float maxValue) {
        ExpectedColumn expectedColum = new ExpectedColumn(name, hasQualityClass, profondeur,
                numeroRepetition, minValue, maxValue);
        this.possibleColums.put(name, expectedColum);
        this.expectedColums.put(index, expectedColum);
        if (expectedColum.hasQualityClass()) {
            expectedColum = new ExpectedColumn();
            expectedColum.setName(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC);
            expectedColum.setFlag(true);
            expectedColum.setFlagType(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_TYPE);
            expectedColum.setRefVariableName(name);
            if (!this.possibleColums.containsKey(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC)) {
                this.possibleColums.put(RecorderACBB.PROPERTY_CST_QUALITY_CLASS_GENERIC,
                        expectedColum);
            }
            this.expectedColums.put(index + 1, expectedColum);
        }
    }

    /**
     * Gets the datatype @link(String).
     *
     * @return the datatype @link(String)
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#getDatatype()
     */
    @Override
    public final String getDatatype() {
        return this.datatype;
    }

    /**
     * Sets the datatype @link(String).
     *
     * @param datatype the new datatype @link(String)
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#setDatatype(java
     * .lang.String)
     */
    @Override
    public final void setDatatype(final String datatype) {
        this.datatype = datatype;

    }

    /**
     * Gets the date de debut.
     *
     * @return the date de debut
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#
     * getDateDeDebut()
     */
    @Override
    public final LocalDateTime getDateDeDebut() {
        return super.getDateDeDebut();
    }

    /**
     * Sets the date de debut.
     *
     * @param dateDeDebut the new date de debut
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#
     * setDateDeDebut(java.time.LocalDateTime)
     */
    @Override
    public final void setDateDeDebut(final LocalDateTime dateDeDebut) {
        super.setDateDeDebut(dateDeDebut);
        this.initDate();
    }

    /**
     * Gets the expected colums @link(Map<Integer,ExpectedColumn>).
     *
     * @return the expected colums @link(Map<Integer,ExpectedColumn>)
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#getExpectedColums
     * ()
     */
    @Override
    public final Map<Integer, ExpectedColumn> getExpectedColums() {
        return this.expectedColums;
    }

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#
     * getLocalizationManager()
     */
    @Override
    public final ILocalizationManager getLocalizationManager() {
        return super.getLocalizationManager();
    }

    /**
     * Gets the nom de fichier.
     *
     * @param version
     * @return the nom de fichier
     * @link(VersionFile) the version
     * @link(VersionFile) the version
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#
     * getNomDeFichier(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public final String getNomDeFichier(final VersionFile version) {
        if (version == null) {
            return null;
        }
        final Dataset dataset = version.getDataset();
        if (ACBBUtils.getSiteFromDataset(dataset) == null
                || ACBBUtils.getDatatypeFromDataset(dataset) == null
                || dataset.getDateDebutPeriode() == null || dataset.getDateFinPeriode() == null) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier.append(ACBBUtils.getSiteFromDataset(dataset).getPath())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(this.getDatatype())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DateUtil.DD_MM_YYYY))
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DateUtil.DD_MM_YYYY))
                    .append(AbstractRequestPropertiesACBB.CST_DOT)
                    .append(IRequestPropertiesACBB.FORMAT_FILE);
        } catch (final Exception e) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        return nomFichier.toString();
    }

    /**
     * Gets the possible colums.
     *
     * @return the possible colums
     */
    public final Map<String, ExpectedColumn> getPossibleColums() {
        return this.possibleColums;
    }

    /**
     * Gets the possibles colums.
     *
     * @return the possibles colums
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#getPossiblesColums
     * ()
     */
    @Override
    public final Map<String, ExpectedColumn> getPossiblesColums() {
        return this.possibleColums;
    }

    /**
     * @return
     */
    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicateMPS();
    }

    /**
     * Inits the dataset descriptor.
     *
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#initDatasetDescriptor
     * (org.inra.ecoinfo.acbb.dataset.impl.DatasetDescriptorACBB)
     */
    @Override
    public final void initDatasetDescriptor(final DatasetDescriptorACBB datasetDescriptor) {
        final int mandatoryColumnCount = datasetDescriptor.getUndefinedColumn();
        int columnNumber = 0;
        for (final Column column : datasetDescriptor.getColumns()) {
            if (columnNumber < mandatoryColumnCount) {
                this.expectedColums.put(datasetDescriptor.getColumns().indexOf(column),
                        new ExpectedColumn(column));
                this.possibleColums.put(column.getName().trim(), new ExpectedColumn(column));
                columnNumber++;
            } else {
                break;
            }
        }
    }

    /**
     * Sets the date de fin.
     *
     * @param dateDeFin the new date de fin
     * @see org.inra.ecoinfo.acbb.dataset.impl.AbstractRequestPropertiesACBB#setDateDeFin
     * (java.time.LocalDateTime)
     */
    @Override
    public final void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.DAYS, 1);
    }

    /**
     * Test nom fichier.
     *
     * @param nomfichier
     * @param badsFormatsReport
     * @link(String) the nomfichier
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the nomfichier
     * @link(BadsFormatsReport) the bads formats report
     * @see org.inra.ecoinfo.acbb.dataset.mps.IRequestPropertiesMPS#testNomFichier
     * (java.lang.String, org.inra.ecoinfo.dataset.BadsFormatsReport)
     */
    @Override
    public final void testNomFichier(final String nomfichier,
                                     final BadsFormatsReport badsFormatsReport) {

        final Matcher matches = Pattern.compile(AbstractRequestPropertiesACBB.PATTERN_FILE_NAME)
                .matcher(nomfichier);
        String originalSiteChemin = org.apache.commons.lang.StringUtils.EMPTY;
        String originalDatatypeFrequence = org.apache.commons.lang.StringUtils.EMPTY;
        String originalDateDeDebut = org.apache.commons.lang.StringUtils.EMPTY;
        String originalDateDeFin = org.apache.commons.lang.StringUtils.EMPTY;
        if (!matches.matches()) {
            badsFormatsReport.addException(new BadExpectedValueException(this
                    .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_BAD_NAME_FILE)));
        } else {
            originalSiteChemin = matches.group(1);
            originalDatatypeFrequence = matches.group(2);
            originalDateDeDebut = matches.group(3);
            originalDateDeFin = matches.group(4);
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier.append(this.getSite().getPath())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(this.getDatatype())
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(getDateDeDebut(), DateUtil.DD_MM_YYYY))
                    .append(AbstractRequestPropertiesACBB.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(getDateDeFin(), DateUtil.DD_MM_YYYY))
                    .append(AbstractRequestPropertiesACBB.CST_DOT)
                    .append(IRequestPropertiesACBB.FORMAT_FILE);
            final Matcher matches2 = Pattern.compile(
                    AbstractRequestPropertiesACBB.PATTERN_FILE_NAME).matcher(nomFichier);
            String siteChemin = org.apache.commons.lang.StringUtils.EMPTY;
            String datatypeFrequence = org.apache.commons.lang.StringUtils.EMPTY;
            String dateDeDebut = org.apache.commons.lang.StringUtils.EMPTY;
            String dateDeFin = org.apache.commons.lang.StringUtils.EMPTY;
            if (!matches2.matches()) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_NAME_FILE)));
            } else {
                siteChemin = matches2.group(1);
                datatypeFrequence = matches2.group(2);
                dateDeDebut = matches2.group(3).replace(AbstractRequestPropertiesACBB.CST_SLASH,
                        AbstractRequestPropertiesACBB.CST_HYPHEN);
                dateDeFin = matches2.group(4).replace(AbstractRequestPropertiesACBB.CST_SLASH,
                        AbstractRequestPropertiesACBB.CST_HYPHEN);
            }
            if (!originalSiteChemin.equals(siteChemin)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_SITE)));
            }
            if (!originalDatatypeFrequence.equals(datatypeFrequence)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_DATATYPE)));
            }
            if (!originalDateDeDebut.equals(dateDeDebut)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_BEGIN_DATE)));
            }
            if (!originalDateDeFin.equals(dateDeFin)) {
                badsFormatsReport.addException(new BadExpectedValueException(this
                        .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                                IRequestPropertiesACBB.PROPERTY_MSG_BAD_END_DATE)));
            }
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new BadExpectedValueException(this
                    .getLocalizationManager().getMessage(IRequestPropertiesACBB.BUNDLE_NAME,
                            IRequestPropertiesACBB.PROPERTY_MSG_BAD_NAME_FILE)));
        }
    }
}
