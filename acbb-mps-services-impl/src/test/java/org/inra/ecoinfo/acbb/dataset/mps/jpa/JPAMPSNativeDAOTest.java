/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.mps.jpa;

import org.junit.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * @author ptcherniati
 */
public class JPAMPSNativeDAOTest {

    public JPAMPSNativeDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of extractMPS method, of class JPAMPSNativeDAO.
     */
    @Test
    public void testPattern() {

        String pattern = "		array[%1$s] selectedSite,\n"
                + "		array[%2$s] selectedTraitement,\n"
                + "		'%3$tY-%3$tm-%3$td'::date datedebut,\n"
                + "		'%3$tY-%4$tm-%4$td'::date datefin,\n"
                + "		array[%5$s] selectedVariable,\n"
                + "		array[%6$s] profondeurs,\n"
                + "		'%7$s' locale,\n"
                + "		'%8$s' utilisateur";
        final String result = "		array[25,15,18] selectedSite,\n"
                + "		array[2,5] selectedTraitement,\n"
                + "		'1985-03-12'::date datedebut,\n"
                + "		'1985-11-05'::date datefin,\n"
                + "		array[25,31] selectedVariable,\n"
                + "		array[4,7,17] profondeurs,\n"
                + "		'fr' locale,\n"
                + "		'philippe' utilisateur";
        List<Long> selectedSites = Stream.of(new Long[]{25l, 15l, 18l}).collect(Collectors.toList());
        List<Long> selectedTreatments = Stream.of(new Long[]{2l, 5l}).collect(Collectors.toList());
        LocalDate startDate = LocalDate.of(1985, Month.MARCH, 12);
        LocalDate endDate = LocalDate.of(2021, Month.NOVEMBER, 5);
        List<Long> selectedVariables = Stream.of(new Long[]{25l, 31l}).collect(Collectors.toList());
        List<Long> selectedDepths = Stream.of(new Long[]{4L, 7l, 17l}).collect(Collectors.toList());
        String locale = "fr";
        String user = "philippe";
        String format = String.format(pattern,
                selectedSites.stream().map(l -> Long.toString(l)).collect(Collectors.joining(",")),
                selectedTreatments.stream().map(l -> Long.toString(l)).collect(Collectors.joining(",")),
                startDate,
                endDate,
                selectedVariables.stream().map(l -> Long.toString(l)).collect(Collectors.joining(",")),
                selectedDepths.stream().map(l -> Long.toString(l)).collect(Collectors.joining(",")),
                locale, user
        );
        assertEquals(result, format);
    }

}
