package org.inra.ecoinfo.acbb.dataset.smp;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.acbb.ACBBTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.dataset.UnLoadDatasetFixture;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ACBBTransactionalTestFixtureExecutionListener.class})
public class UnLoadDatasetSMPFixture extends UnLoadDatasetFixture {


    /**
     *
     */
    public UnLoadDatasetSMPFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }
}
