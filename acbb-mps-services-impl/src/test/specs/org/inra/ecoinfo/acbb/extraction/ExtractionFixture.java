package org.inra.ecoinfo.acbb.extraction;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.acbb.ACBBTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.acbb.extraction.mps.IMPSDatasetManager;
import org.inra.ecoinfo.acbb.extraction.mps.IMPSParameter;
import org.inra.ecoinfo.acbb.extraction.mps.impl.MPSParameterVO;
import org.inra.ecoinfo.acbb.refdata.site.ISiteACBBDAO;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.IVariableACBBDAO;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.acbb.extraction.cst.ConstantACBBExtraction;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ACBBTransactionalTestFixtureExecutionListener.class})
public class ExtractionFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    ITraitementDAO traitementDAO;
    ISiteACBBDAO siteACBBDAO;
    ILocalizationManager localizationManager;
    IVariableACBBDAO variableACBBDAO;
    IMPSDatasetManager mpsDatasetManager;

    /**
     *
     */
    public ExtractionFixture() {
        super();
        MockitoAnnotations.initMocks(this);
        this.traitementDAO = ACBBTransactionalTestFixtureExecutionListener.traitementDAO;
        this.siteACBBDAO = ACBBTransactionalTestFixtureExecutionListener.siteACBBDAO;
        this.localizationManager = ACBBTransactionalTestFixtureExecutionListener.localizationManager;
        this.variableACBBDAO = ACBBTransactionalTestFixtureExecutionListener.variableACBBDAO;
        this.mpsDatasetManager = (IMPSDatasetManager) ACBBTransactionalTestFixtureExecutionListener.applicationContext.getBean("MPSDatasetManager");
    }

    /**
     * @param listeSitesTraitementCodes
     * @param variablesListNames
     * @param minDepth
     * @param maxDepth
     * @param dateDedebut
     * @param datedeFin
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    public String extract(String listeSitesTraitementCodes, String variablesListNames, String minDepth, String maxDepth, String dateDedebut, String datedeFin, String filecomps, String commentaire, String affichage) throws BusinessException,
            PersistenceException,
            DateTimeException,
            BadExpectedValueException {
        List<SiteACBB> sites = new LinkedList();
        List<TraitementProgramme> traitementProgrammes = new LinkedList();
        for (String traitementSiteCode : listeSitesTraitementCodes.split(",")) {
            String[] listeTraitementSiteCode = traitementSiteCode.split("@");
            SiteACBB site = this.siteACBBDAO.getByPath(listeTraitementSiteCode[1])
                    .map(s -> (SiteACBB) s)
                    .orElseThrow(BusinessException::new);
            traitementProgrammes.add(this.traitementDAO.getByNKey(site.getId(), listeTraitementSiteCode[0]).orElseThrow(BusinessException::new));
        }
        final IMPSParameter parameters = new MPSParameterVO();
        parameters.getParameters().put(TraitementProgramme.class.getSimpleName(), traitementProgrammes);
        List<VariableACBB> variableACBB = new LinkedList();
        parameters.getParameters().put(ConstantACBBExtraction.DATATYPE_NAME_INDEX, new LinkedList<String>());
        for (String variableAffichage : variablesListNames.split(",")) {
            VariableACBB variable = null;
            switch (variableAffichage) {
                case "smp":
                    variable = (VariableACBB) this.variableACBBDAO.getByAffichage("SMP").orElseThrow(BusinessException::new);
                    parameters.getParameters().put("SMP", Stream.of(variable).collect(Collectors.toList()));
                    ((List<String>) parameters.getParameters().get(ConstantACBBExtraction.DATATYPE_NAME_INDEX)).add("SMP");
                    break;
                case "swc":
                    variable = (VariableACBB) this.variableACBBDAO.getByAffichage("SWC").orElseThrow(BusinessException::new);
                    parameters.getParameters().put("SWC", Stream.of(variable).collect(Collectors.toList()));
                    ((List<String>) parameters.getParameters().get(ConstantACBBExtraction.DATATYPE_NAME_INDEX)).add("SWC");
                    break;
                case "ts":
                    variable = (VariableACBB) this.variableACBBDAO.getByAffichage("TS").orElseThrow(BusinessException::new);
                    parameters.getParameters().put("TS", Stream.of(variable).collect(Collectors.toList()));
                    ((List<String>) parameters.getParameters().get(ConstantACBBExtraction.DATATYPE_NAME_INDEX)).add("TS");
                    break;
                default:
                    break;
            }
            if (variable == null) {
                continue;
            }
            variableACBB.add(variable);
        }
        parameters.setSelectedMPSVariables(variableACBB);
        final DatesFormParamVO datesForm1ParamVO = new DatesFormParamVO(this.localizationManager);
        final List<Map<String, String>> periods = this.buildNewMapPeriod(dateDedebut, datedeFin);
        datesForm1ParamVO.setPeriods(periods);
        datesForm1ParamVO.setDateStart(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateDedebut));
        datesForm1ParamVO.setDateEnd(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, datedeFin));
        parameters.getParameters().put(DatesFormParamVO.class.getSimpleName(), datesForm1ParamVO);
        int depthInf = Integer.parseInt(minDepth);
        int depthSup = Integer.parseInt(maxDepth);
        List<Integer> depths = this.mpsDatasetManager.getAvailablesDepthByTraitementAndVariables(
                traitementProgrammes,
                datesForm1ParamVO.intervalsDate(),
                variableACBB);
        TreeSet selectedDepths = (TreeSet) depths.stream()
                .filter(d -> depthInf <= d && d <= depthSup)
                .collect(Collectors.toCollection(() -> new TreeSet()));
        parameters.setSelectedsDepths(selectedDepths);
        parameters.setCommentaire(commentaire);
        try {
            ACBBTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException | BusinessException e) {
            return "false : " + e.getMessage();
        }
        return "true";

    }
}
