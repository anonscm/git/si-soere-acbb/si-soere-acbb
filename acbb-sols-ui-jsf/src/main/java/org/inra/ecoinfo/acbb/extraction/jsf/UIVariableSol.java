/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import org.inra.ecoinfo.acbb.extraction.soils.ISoilDatasetManager;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author tcherniatinsky
 */
public class UIVariableSol extends UIVariable {

    public final String DATATYPE_TEXTURE = "sol_texture";
    Map<Long, MethodJSF> methods = new HashMap();
    List<MethodJSF> availablesMethods = new LinkedList<>();
    MethodJSF methodSelected;

    Properties propertiesMethodNames;

    /**
     * @return
     */
    public Map<String, Map<Long, VariableJSF>> getVariables() {
        return variables;
    }

    public List<MethodJSF> getAvailablesMethods() {
        return availablesMethods;
    }

    /**
     * Adds the all variables.
     *
     * @return the string
     */
    public final String addAllVariablesSoil() {
        variables.clear();
        for (Entry<String, List<VariableJSF>> variableJSFEntry : this.availablesVariables.entrySet()) {
            for (VariableJSF variableJSF : variableJSFEntry.getValue()) {
                variableJSF.selected = true;
                if (!variables.containsKey(variableJSFEntry.getKey())) {
                    variables.put(variableJSFEntry.getKey(),
                            new HashMap());
                }
                variables.get(variableJSFEntry.getKey()).put(variableJSF.getVariable().getId(), variableJSF);
            }
        }
        for (MethodJSF method : availablesMethods) {
            methods.put(method.getMethod().getId(), method);
            method.selected = true;
        }
        return null;
    }

    public Map<Long, MethodJSF> getMethods() {
        return methods;
    }

    /**
     * Removes the all variables.
     *
     * @return the string
     */
    public final String removeAllVariablesSoil() {
        variables.clear();
        methods.clear();
        for (final Entry<String, List<VariableJSF>> variableJSFEntry : availablesVariables
                .entrySet()) {
            for (VariableJSF variableJSF : variableJSFEntry.getValue()) {
                variableJSF.selected = false;
            }
        }
        for (MethodJSF method : availablesMethods) {
            method.selected = false;
        }
        return null;
    }

    /**
     * Update variables availables.
     *
     * @param variableManager
     * @param uiTreatment
     * @param uiDate
     */
    public final void updateVariablesAvailablesSoil(IVariableManager variableManager, UIDate uiDate, UITreatment uiTreatment) {
        availablesVariables.clear();
        Map<String, List<NodeDataSet>> availableVariables = variableManager.getAvailableVariablesByTreatmentAndDatesInterval(uiTreatment.getListTraitement(), uiDate.getDatesForm1ParamVO().intervalsDate());
        updateVariables(availableVariables);
        Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> availableVariablesByTreatmentAndDatesIntervalByMethod = ((ISoilDatasetManager) variableManager).getAvailableVariablesByTreatmentAndDatesIntervalByMethod(uiTreatment.getListTraitement(), uiDate.getDatesForm1ParamVO().intervalsDate());
        updateVariablesByMethod(availableVariablesByTreatmentAndDatesIntervalByMethod);
    }

    private void updateVariablesByMethod(Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> availableVariables) {
        availableVariables.entrySet().stream().forEach((entry)
                -> {
            final MethodeSoilTexture method = entry.getKey();
            Map<VariableACBB, Set<NodeDataSet>> nodesByVariables = entry.getValue();
            availablesMethods.add(new MethodJSF(method, nodesByVariables));
        });
    }

    /**
     * Gets the id variable selected.
     *
     * @return the id variable selected
     */
    public Long getIdVariableSelected() {
        return variableSelected.variable.getId();
    }

    /**
     * @param localizationManager
     */
    public void initVariable(ILocalizationManager localizationManager) {
        this.propertiesVariablesNames = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        this.propertiesMethodNames = localizationManager.newProperties(MethodeSoilTexture.NAME_ENTITY_JPA, MethodeSoilTexture.ATTRIBUTE_JPA_LIBELLE);
        Properties localizedDatatypes = localizationManager.newProperties(Nodeable.getLocalisationEntite(DataType.class), Nodeable.ENTITE_COLUMN_NAME);
        for (Map.Entry<Object, Object> entry : localizedDatatypes.entrySet()) {
            String key = Utils.createCodeFromString((String) entry.getKey());
            String value = (String) entry.getValue();
            this.localizedDatatypes.put(key, value);
        }
    }

    /**
     * @param datatype
     * @return
     */
    public List<VariableACBB> getVariablesForDatatype(String datatype) {
        datatype = localizedDatatypes.get(datatype);
        final List<VariableACBB> variablesACBB = new LinkedList();
        if (variables.containsKey(datatype)) {
            for (final VariableJSF variable : this.variables.get(datatype).values()) {
                variablesACBB.add(variable.getVariable());
            }
        }
        return variablesACBB;
    }

    /**
     * Gets the variable selected.
     *
     * @return the variable selected
     */
    public VariableJSF getVariableSelected() {
        return this.variableSelected;
    }

    public MethodJSF getMethodSelected() {
        return methodSelected;
    }

    /**
     * Sets the variable selected.
     *
     * @param variableSelected the new variable selected
     */
    public final void setMethodSelected(final MethodJSF methodSelected) {
        this.methodSelected = methodSelected;
    }

    /**
     * Gets the variable step is valid.
     *
     * @return the variable step is valid
     */
    public Boolean getVariableStepIsValid() {
        return !this.variables.isEmpty() || !this.methods.isEmpty();
    }

    /**
     * @param metadatasMap
     */
    public void addVariablestoMap(Map<String, Object> metadatasMap) {
        getVariables().forEach((datatypeName, variablesEntry) -> {
            variablesEntry.forEach((id, variableJSF) -> {
                String datatypeCode = Variable.class.getSimpleName().toLowerCase().concat(variableJSF.getDatatype());
                if (metadatasMap.get(datatypeCode) == null) {
                    metadatasMap.put(datatypeCode, new LinkedList());
                }
                ((Collection<VariableACBB>) metadatasMap.get(datatypeCode)).add(variableJSF.variable);
            });
        });
        getMethods().forEach((Long datatypeName, MethodJSF methodEntry) -> {
            String methodeCode = MethodeSoilTexture.class.getSimpleName();
            String datatypeCode = Variable.class.getSimpleName().toLowerCase().concat(methodEntry.getDatatype());
            methodEntry.getVariables().stream()
                    .forEach((VariableJSF variable) -> {
                        ((TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>) metadatasMap
                                .computeIfAbsent(methodeCode, k -> new TreeMap<MethodeSoilTexture, SortedSet<VariableACBB>>()))
                                .computeIfAbsent(methodEntry.getMethod(), k -> new TreeSet<>())
                                .add(variable.getVariable());
                        ((Collection<VariableACBB>) metadatasMap
                                .computeIfAbsent(datatypeCode, k -> new LinkedList<>()))
                                .add(variable.getVariable());
                    });
        });
    }


    public String getTextureDatatatype() {
        return localizedDatatypes.get(DATATYPE_TEXTURE);
    }

    /**
     * Select variable.
     *
     * @param variableSelected the variable selected
     */
    public final void selectMethod(final MethodJSF methodSelected) {
        if (methodSelected.getSelected()) {
            methods.remove(methodSelected.method.getId());
            methodSelected.setSelected(false);
        } else {
            methods.put(methodSelected.method.getId(), methodSelected);
            methodSelected.setSelected(true);
        }
    }

    public class MethodJSF {

        /**
         * The selected @link(boolean).
         */
        boolean selected = false;
        /**
         * The variable @link(VariableACBB).
         */
        MethodeSoilTexture method;
        List<VariableJSF> variables = new LinkedList();
        String datatype = DATATYPE_TEXTURE;

        private MethodJSF(MethodeSoilTexture method, Map<VariableACBB, Set<NodeDataSet>> nodesByVariables) {
            variables = new LinkedList();
            this.method = method;
            for (Entry<VariableACBB, Set<NodeDataSet>> entry : nodesByVariables.entrySet()) {
                VariableACBB variable = entry.getKey();
                Set<NodeDataSet> nodesVariable = entry.getValue();
                variables.add(new VariableJSF(variable, nodesVariable, datatype));
            }
        }

        /**
         * @return the datatype
         */
        public String getDatatype() {
            return this.datatype;
        }

        /**
         * @param datatype the datatype to set
         */
        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

        /**
         * Gets the selected.
         *
         * @return the selected
         */
        public boolean getSelected() {
            return this.selected;
        }

        /**
         * Sets the selected.
         *
         * @param selected the new selected
         */
        public final void setSelected(final boolean selected) {
            this.selected = selected;
        }

        public MethodeSoilTexture getMethod() {
            return method;
        }

        public void setMethod(MethodeSoilTexture method) {
            this.method = method;
        }

        public List<VariableJSF> getVariables() {
            return variables;
        }

        public void setVariables(List<VariableJSF> variables) {
            this.variables = variables;
        }

        /**
         * Gets the localized name.
         *
         * @return the localized name
         */
        public String getLocalizedName() {
            final String localizedName = propertiesMethodNames.getProperty(method.getLibelle(), method.getLibelle());
            return String.format("%s (%s)", method.getNumberId(), localizedName);
        }
    }

}
