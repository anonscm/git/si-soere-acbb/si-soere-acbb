/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.contentschemistry.jsf;

import org.inra.ecoinfo.acbb.dataset.soil.impl.AbstractProcessRecordSoil;
import org.inra.ecoinfo.acbb.extraction.jsf.*;
import org.inra.ecoinfo.acbb.extraction.soil.contentschemistry.impl.ContentChemistryBuildOutputDisplay;
import org.inra.ecoinfo.acbb.extraction.soil.impl.SoilParameterVO;
import org.inra.ecoinfo.acbb.extraction.soils.ISoilDatasetManager;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.acbb.synthesis.jpa.IIntervalDateSynthesisDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;

/**
 * The Class UIBeanMPS.
 */
@ManagedBean(name = "uiSol")
@ViewScoped
public class UIBeanSol extends AbstractUIBeanForSteps implements Serializable {

    // TODO
    /**
     * Autre type de données @ManagedProperty
     */
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    @ManagedProperty(value = "#{contentChemistryDatatype}")
    public String contentChemistryDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{soilDensityDatatype}")
    public String soilDensityDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{soilStockDatatype}")
    public String soilStockDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{soilTextureDatatype}")
    public String soilTextureDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{soilCoarseDatatype}")
    public String soilCoarseDatatype;
    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{soilDatasetManager}")
    ISoilDatasetManager soilDatasetManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;


    @ManagedProperty(value = "#{intervalDateSynthesisDAO}")
    IIntervalDateSynthesisDAO intervalDateSynthesisDAO;
    List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypeClasses = Stream.of(
            org.inra.ecoinfo.acbb.synthesis.soilcoarse.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.soildensity.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.soilstock.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.soiltexture.SynthesisDatatype.class
    ).collect(Collectors.toList());

    /**
     * The parameters request @link(ParametersRequest).
     */
    ParametersRequest parametersRequest = new ParametersRequest();

    UITreatment uiTreatment;

    UIDate uiDate;

    UIVariableSol uiVariable;

    UIAssociate uIAssociate;

    // GETTERS SETTERS - BEANS

    /**
     * Instantiates a new uI bean mps.
     */
    public UIBeanSol() {
        super();
    }

    /**
     * @return
     */
    public UITreatment getUiTreatment() {
        return uiTreatment;
    }

    /**
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     * @return
     */
    public UIVariableSol getUiVariable() {
        return uiVariable;
    }

    /**
     * @return
     */
    public UIAssociate getUiAssociate() {
        return uIAssociate;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        // TODO
        uiDate.addDatestoMap(metadatasMap);
        uiTreatment.addTreatmenttoMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        final Optional<List<VariableACBB>> opt = Optional.ofNullable((List<VariableACBB>) metadatasMap.get(Variable.class.getSimpleName().toLowerCase().concat(contentChemistryDatatype)));
        if (opt.isPresent()) {
            List<VariableACBB> extraVariables = opt.get()
                    .stream()
                    .filter(variable -> AbstractProcessRecordSoil.CONSTANT_HUMIDITE_RESIDUELLE.equals(variable.getAffichage()) || ContentChemistryBuildOutputDisplay.SOIL_SAMPLES.equals(variable.getAffichage()))
                    .collect(Collectors.toList());
            metadatasMap.put(Variable.class.getSimpleName().toLowerCase().concat("extra_variables"), extraVariables);
        }
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final SoilParameterVO parameters = new SoilParameterVO(metadatasMap);
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, 1);
        return null;
    }

    /**
     * @return
     */
    public List<UIVariableSol.VariableJSF> getAvailablesVariablesSoilDatasetManager() {
        if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(contentChemistryDatatype)) {
            return uiVariable.getAvailablesVariables().get(contentChemistryDatatype);
        } else if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(soilDensityDatatype)) {
            return uiVariable.getAvailablesVariables().get(soilDensityDatatype);
        } else if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(soilStockDatatype)) {
            return uiVariable.getAvailablesVariables().get(soilStockDatatype);
        } else if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(soilCoarseDatatype)) {
            return uiVariable.getAvailablesVariables().get(soilCoarseDatatype);
        } else if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(soilTextureDatatype)) {
            return uiVariable.getAvailablesVariables().get(soilTextureDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * autre type de données
     */
    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (getStep()) {
            case 1:
                return uiDate.getDateStepIsValid();
            case 2:
                return uiTreatment.getTraitementStepIsValid();
            case 3:
                return uiVariable.getVariableStepIsValid();
            case 4:
                return uiVariable.getVariableStepIsValid();
            default:
                break;
        }
        return false;
    }

    /**
     * @return the parametersRequest
     */
    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * @param parametersRequest the parametersRequest to set
     */
    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    public void initProperties() {
        setEnabledMotivation(true);
        uiTreatment = new UITreatment();
        uiTreatment.initProperties(localizationManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager, intervalDateSynthesisDAO, synthesisDatatypeClasses);
        uiVariable = new UIVariableSol();
        uiVariable.initVariable(localizationManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "soil";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 2:
                uiTreatment.updateAgroAvailables(soilDatasetManager, uiDate.getDatesForm1ParamVO().intervalsDate());
                break;
            case 3:
                uiVariable.updateVariablesAvailablesSoil(soilDatasetManager, uiDate, uiTreatment);
                break;
            case 4:
                final String dateStartString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateStart();
                final String dateEndString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateEnd();
                Map<Long, UIVariable.VariableJSF> variables = uiVariable.getVariables().entrySet().stream()
                        .map(e -> e.getValue().entrySet())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                IntervalDate intervalDate = null;
                try {
                    intervalDate = new IntervalDate(dateStartString, dateEndString, DateUtil.DD_MM_YYYY);
                    uIAssociate.caseassociate(variables, intervalDate, fileCompManager);
                } catch (BadExpectedValueException ex) {
                    LoggerFactory.getLogger(UIBeanSol.class.getName()).error(ex.getMessage(), ex);
                } catch (DateTimeParseException ex) {
                    LoggerFactory.getLogger(UIBeanSol.class.getName()).error(ex.getMessage(), ex);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * @param soilDatasetManager the soilDatasetManager to set
     */
    public void setSoilDatasetManager(ISoilDatasetManager soilDatasetManager) {
        this.soilDatasetManager = soilDatasetManager;
    }

    /**
     * @param contentChemistryDatatype
     */
    public void setContentChemistryDatatype(String contentChemistryDatatype) {
        this.contentChemistryDatatype = contentChemistryDatatype;
    }

    /**
     * @param soilDensityDatatype
     */
    public void setSoilDensityDatatype(String soilDensityDatatype) {
        this.soilDensityDatatype = soilDensityDatatype;
    }

    /**
     * @param soilStockDatatype
     */
    public void setSoilStockDatatype(String soilStockDatatype) {
        this.soilStockDatatype = soilStockDatatype;
    }

    /**
     * @param soilTextureDatatype
     */
    public void setSoilTextureDatatype(String soilTextureDatatype) {
        this.soilTextureDatatype = soilTextureDatatype;
    }

    /**
     * @param soilCoarseDatatype
     */
    public void setSoilCoarseDatatype(String soilCoarseDatatype) {
        this.soilCoarseDatatype = soilCoarseDatatype;
    }

    /**
     * @param extractionManager the extractionManager to set
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * @param localizationManager the localizationManager to set
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * @param notificationsManager the notificationsManager to set
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    public void setIntervalDateSynthesisDAO(IIntervalDateSynthesisDAO intervalDateSynthesisDAO) {
        this.intervalDateSynthesisDAO = intervalDateSynthesisDAO;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The european format @link(boolean).
         */
        boolean format = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getFormat() {
            return this.format;
        }

        /**
         * Sets the european format.
         *
         * @param format
         */
        public final void setFormat(final boolean format) {
            this.format = format;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiDate.getDateStepIsValid() && uiTreatment.getTraitementStepIsValid()
                    && uiVariable.getVariableStepIsValid();
        }
    }
}
