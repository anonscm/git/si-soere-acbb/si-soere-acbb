
Ce fichier r�capitule vos param�tres d'extraction.

//Date et heure d'extraction : 28/05/2020 12:26:35

********************************************************************************
*                                                                              *
*                                                                              *
* V�rifiez que vous avez bien les droits sur l'ensemble des donn�es demand�es. *
*           Les donn�es extraites peuvent-�tre incompl�tes.                    *
*                                                                              *
*                                                                              *
********************************************************************************

P�riode s�lectionn�e :
	du 25/04/2005 au 04/01/2006

Traitements selectionn�es :
	Rotation prairie culture - Lusignan : t�moin cultures annuelles (T1)
	Rotation prairie culture - Lusignan : prairie 3ans N+ fauche (T2)


Variables selectionn�es : 
	Variables de travail du sol :

		wsol_depth (profondeur de travail)
		wsol_objectifs (Objectifs de travail du sol)
		wsol_type_outil (Type d'outil de travail du sol)


Commentaire d'extraction :
	extraction de travail du sol Lusignan
