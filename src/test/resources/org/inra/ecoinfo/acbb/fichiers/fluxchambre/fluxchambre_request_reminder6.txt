Ce fichier récapitule vos paramètres d'extraction

Date et heure d'extraction:
22/02/2012 11:18:19

Sites selectionnées:
   Laqueuille

Variables selectionnées: 
   moyenne des flux de dioxyde de carbone
   moyenne de la concentration en vapeur d'eau
   moyenne de la concentration en protoxyde d'azote
   Moyenne de la concentration en méthane mesurée

Période sélectionnée:
    du 06/05/2010 au 10/05/2010

Commentaire d'extraction:
   extraction de flux en chambres
