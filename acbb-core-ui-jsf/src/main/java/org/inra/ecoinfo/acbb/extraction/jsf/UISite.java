/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.utils.IntervalDate;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tcherniatinsky
 */
public class UISite {

    public static final String ICON_BLANK = "ui-icon ui-icon-blank";
    public static final String ICON_CHECK = "ui-icon ui-icon-check";
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";

    /**
     * The properties names site @link(Properties).
     */
    static Properties propertiesNamesSite;
    static Properties propertiesNamesAgroEcosysteme;
    /**
     * The sites sites @link(Map<Long,TraitementVO>).
     */
    final Map<Long, SiteVO> sites = new HashMap();

    /**
     * The availables agroecosystemes.
     */
    TreeNode availablesAgroecosystemes = new DefaultTreeNode("Root", null);
    private TreeNode selectedNode;
    private ILocalizationManager localizationManager;

    /**
     * Gets the sites sites.
     *
     * @return the sites sites
     */
    public Map<Long, SiteVO> getSites() {
        return this.sites;
    }

    public Map<String, List<SiteVO>> getSitesByAgroecosystems() {
        return sites.values().stream()
                .collect(
                        Collectors.groupingBy(
                                t -> ((NodeableNode) ((NodeableNode) t.getParent())).getLocalizedAffichage()
                        )
                );
    }

    /**
     * Adds the all sites.
     *
     * @return the string
     */
    public final String addAllSites() {
        sites.clear();
        for (final TreeNode agroNode : this.availablesAgroecosystemes.getChildren()) {
            for (final TreeNode siteNode : agroNode.getChildren()) {
                SiteVO site = (SiteVO) siteNode.getData();
                site.setSelected(true);
                sites.put(site.site.getId(), site);
                setExpanded(siteNode, true);
            }
        }
        return null;
    }

    /**
     * Removes the all sites.
     *
     * @return the string
     */
    public final String removeAllSites() {
        sites.clear();
        this.availablesAgroecosystemes.getChildren().forEach((agroNode) -> {
            agroNode.getChildren().forEach((siteNode) -> {
                SiteVO siteVO = (SiteVO) siteNode.getData();
                siteVO.setSelected(false);
                setExpanded(siteNode, false);
            });
        });
        return null;
    }

    /**
     * Inits the properties.
     *
     * @param localizationManager
     */
    public void initProperties(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        propertiesNamesSite = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        propertiesNamesAgroEcosysteme = localizationManager.newProperties(Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME);
    }

    /**
     * Select site.
     *
     * @param event
     */
    public final void selectNode(NodeSelectEvent event) {
        TreeNode siteLeaf = event.getTreeNode();
        System.out.println("selectNode " + this + " " + siteLeaf.getRowKey());
        final String type = siteLeaf.getType();
        if (!NodeType.SITE.equals(type)) {
            ((NodeableNode) siteLeaf).getChildren().forEach(child -> selectSite((SiteVO) child, ((NodeableNode) siteLeaf).getState().equals(ICON_BLANK)));
        } else {
            final SiteVO siteVO = (SiteVO) siteLeaf;
            selectSite(siteVO, !(siteVO).isSelected());
        }
    }

    private final void selectSite(SiteVO site, boolean selected) {
        site.setSelected(selected);
        if (selected) {
            sites.remove(site.getSite().getId());
            sites.put(site.getSite().getId(), site);
        } else {
            sites.put(site.getSite().getId(), site);
            site.setExpanded(true);
            sites.remove(site.getSite().getId());
        }
    }

    /**
     * Update agro availables.
     *
     * @param siteManager
     * @param intervalsDate
     */
    public final void updateAgroAvailables(ISiteManager siteManager, List<IntervalDate> intervalsDate) {
        this.availablesAgroecosystemes = new DefaultTreeNode("Root", null);
        final Set<SiteACBB> availablesSites = new TreeSet();
        availablesSites.addAll(siteManager.getAvailableSites(intervalsDate));
        Map<AgroEcosysteme, TreeNode> agroecosystemesNodes = new HashMap();
        TreeNode agroecosystemeTreeNode;
        for (final SiteACBB site : availablesSites) {
            AgroEcosysteme agroecosysteme = site.getAgroEcosysteme();
            if (!agroecosystemesNodes.containsKey(agroecosysteme)) {
                final NodeableNode agroNode = new NodeableNode(agroecosysteme, availablesAgroecosystemes);
                agroNode.setType(NodeType.AGROECOSYSTEME);
                agroecosystemesNodes.put(agroecosysteme, agroNode);
            }
            agroecosystemeTreeNode = agroecosystemesNodes.get(agroecosysteme);
            new SiteVO(site, NodeType.SITE, agroecosystemeTreeNode);
        }
    }

    /**
     * Gets the site step is valid.
     *
     * @return the site step is valid
     */
    public Boolean getSiteStepIsValid() {
        return !this.sites.isEmpty();
    }

    /**
     * @param id
     */
    public final void selectSite(Long id) {
        SiteVO site = sites.get(id);
        sites.remove(id);
        site.setSelected(false);
    }

    /**
     * @param node
     * @param expanded
     */
    public void setExpanded(TreeNode node, boolean expanded) {
        node.setExpanded(expanded);
        if (node.getParent() != null) {
            setExpanded(node.getParent(), expanded);
        }
    }

    /**
     * Gets the availables agroecosystemes.
     *
     * @return the availables agroecosystemes
     */
    public final TreeNode getAvailablesAgroecosystemes() {
        return this.availablesAgroecosystemes;
    }

    /**
     * @return
     */
    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode
     */
    public void setSelectedNode(TreeNode selectedNode) {
        Optional.ofNullable(selectedNode).ifPresent(sn -> System.out.println("setSelectedNode  : " + sn.getData()));
        this.selectedNode = selectedNode;
    }

    /**
     * Gets the list site.
     *
     * @return the list site
     */
    public List<SiteACBB> getListSites() {
        final List<SiteACBB> sites = new LinkedList();
        for (final SiteVO site : this.sites.values()) {
            sites.add(site.site);
        }
        return sites;
    }

    /**
     * @param metadatasMap
     */
    public void addSitetoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(SiteACBB.class.getSimpleName(), getListSites());
    }

    /**
     * The Class NodeType.
     */
    public static class NodeType {

        /**
         * The agroecosysteme.
         */
        public static final String AGROECOSYSTEME = "agroecosysteme";
        /**
         * The site.
         */
        public static final String SITE = "site";

        private NodeType() {
        }

    }

    /**
     *
     */
    public class NodeableNode extends DefaultTreeNode implements Comparable<NodeableNode>, TreeNode {

        /**
         * The site.
         */
        INodeable nodeable;

        boolean nodeableNodeSelected;

        public NodeableNode(INodeable nodeable, TreeNode parent) {
            super(nodeable, parent);
            this.nodeable = nodeable;
        }

        public NodeableNode(INodeable nodeable, String type, Object data, TreeNode parent) {
            super(type, data, parent);
            this.nodeable = nodeable;
        }

        /**
         * @param nodeable
         */
        public NodeableNode(INodeable nodeable) {
            this.nodeable = nodeable;
        }

        /**
         * @return
         */
        public boolean isSelected() {
            return nodeableNodeSelected;
        }

        /**
         * @param selected
         */
        public void setSelected(boolean selected) {
            this.nodeableNodeSelected = selected;
        }

        /**
         * @return
         */
        public String getName() {
            return localizationManager.getLocalName(nodeable);
        }

        @Override
        public int compareTo(NodeableNode t) {
            return getName().compareTo(t.getName());
        }

        public String getState() {
            if (getType().equals(NodeType.AGROECOSYSTEME)) {
                if (getChildren().stream().allMatch(child -> child.isSelected())) {
                    return ICON_CHECK;
                }
                if (getChildren().stream().anyMatch(child -> child.isSelected())) {
                    return ICON_INDETERMINATE;
                }
            } else {
                return isSelected() ? ICON_CHECK : ICON_BLANK;
            }
            return ICON_BLANK;
        }

        @Override
        public Object getData() {
            return this;
        }

        /**
         * Gets the localized affichage.
         *
         * @return the localized affichage
         */
        public String getLocalizedAffichage() {
            return ((nodeable instanceof SiteACBB) ? propertiesNamesSite : propertiesNamesAgroEcosysteme).getProperty(this.nodeable.getName(), this.nodeable.getName());
        }

        @Override
        public String toString() {
            return "NodeableNode{" + "nodeable=" + nodeable + ", nodeableNodeSelected=" + nodeableNodeSelected + '}';
        }
    }

    /**
     *
     */
    public class SiteVO extends NodeableNode {

        /**
         * The site.
         */
        SiteACBB site;

        public SiteVO(INodeable nodeable, Object data, TreeNode parent) {
            super(nodeable, NodeType.SITE, data, parent);
            this.site = (SiteACBB) nodeable;
        }

        /**
         * Gets the localized affichage.
         *
         * @return the localized affichage
         */
        public String getLocalizedAffichage() {
            return propertiesNamesSite.getProperty(this.site.getName(), this.site.getName());
        }

        /**
         * Gets the localized display path.
         *
         * @return the localized display path
         */
        public String getLocalizedDisplayPath() {
            final String localizedName = propertiesNamesSite.getProperty(this.site.getAgroEcosysteme().getName(), this.site.getAgroEcosysteme().getName());
            return Strings.isNullOrEmpty(localizedName) ? this.site
                    .getAgroEcosysteme().getName() : localizedName.concat(
                            PatternConfigurator.ANCESTOR_SEPARATOR).concat(this.site.getName());
        }

        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return localizationManager.getLocalName(this.site);
        }

        /**
         * Gets the site.
         *
         * @return the site
         */
        public SiteACBB getSite() {
            return this.site;
        }
    }
}
