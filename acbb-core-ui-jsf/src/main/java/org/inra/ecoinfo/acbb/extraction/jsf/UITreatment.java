/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.refdata.agroecosysteme.AgroEcosysteme;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.utils.IntervalDate;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tcherniatinsky
 */
public class UITreatment {

    public static final String ICON_BLANK = "ui-icon ui-icon-blank";
    public static final String ICON_CHECK = "ui-icon ui-icon-check";
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";

    /**
     * The properties affichage traitement @link(Properties).
     */
    static Properties propertiesAffichageTraitement;

    /**
     * The properties names site @link(Properties).
     */
    static Properties propertiesNamesSite;
    static Properties propertiesNamesAgroecosysteme;
    static Properties propertiesNamesTreatment;
    /**
     * The sites traitements @link(Map<Long,TraitementVO>).
     */
    final Map<Long, TraitementVO> sitesTraitements = new HashMap();

    /**
     * The availables agroecosystemes.
     */
    TreeNode availablesAgroecosystemes = new DefaultTreeNode("Root", null);
    private TreeNode selectedNode;
    private ILocalizationManager localizationManager;

    /**
     * Gets the sites traitements.
     *
     * @return the sites traitements
     */
    public Map<Long, TraitementVO> getSitesTraitements() {

        return this.sitesTraitements;
    }
    
    public Map<String, Map<String, List<TraitementVO>>> getTreatmentBySitesAndAgroecosystems(){
        return sitesTraitements.values().stream()
                .collect(Collectors.groupingBy(
                        t -> ((NodeableNode) ((NodeableNode) t.getParent()).getParent()).getLocalizedDisplayPath(),
                        Collectors.collectingAndThen(
                                Collectors.toList(),
                                t1 -> t1.stream().collect(
                                        Collectors.groupingBy(
                                                t2 -> ((NodeableNode) t2.getParent()).getLocalizedDisplayPath()
                                        )
                                )
                        )
                )
                );
    }

    /**
     * Adds the all sites.
     *
     * @return the string
     */
    public final String addAllSites() {
        sitesTraitements.clear();
        for (final TreeNode agroNode : this.availablesAgroecosystemes.getChildren()) {
            for (final TreeNode siteNode : agroNode.getChildren()) {
                for (final TreeNode traitementLeafNode : siteNode.getChildren()) {
                    TraitementVO traitement = (TraitementVO) traitementLeafNode.getData();
                    traitement.setSelected(true);
                    sitesTraitements.put(traitement.traitement.getId(), traitement);
                    setExpanded(traitementLeafNode, true);
                }
            }
        }
        return null;
    }

    /**
     * Removes the all sites.
     *
     * @return the string
     */
    public final String removeAllSites() {
        sitesTraitements.clear();
        this.availablesAgroecosystemes.getChildren().forEach((agroNode) -> {
            agroNode.getChildren().forEach((siteNode) -> {
                for (final TreeNode traitementLeafNode : siteNode.getChildren()) {
                    TraitementVO traitement = (TraitementVO) traitementLeafNode.getData();
                    traitement.setSelected(false);
                    setExpanded(traitementLeafNode, false);
                }
            });
        });
        return null;
    }

    /**
     * Inits the properties.
     *
     * @param localizationManager
     */
    public void initProperties(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        propertiesNamesSite = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        propertiesAffichageTraitement = localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, "affichage");
        propertiesNamesTreatment = localizationManager.newProperties(TraitementProgramme.NAME_ENTITY_JPA, "nom");
        propertiesNamesAgroecosysteme = localizationManager.newProperties(Nodeable.getLocalisationEntite(AgroEcosysteme.class), Nodeable.ENTITE_COLUMN_NAME);
    }

    /**
     * Select traitement.
     *
     * @param event
     */
    public final void selectNode(NodeSelectEvent event) {
        TreeNode selectedNode = event.getTreeNode();
        System.out.println("selectNode " + this + " " + selectedNode.getRowKey());
        final String type = selectedNode.getType();
        boolean isBlank = ((NodeableNode) selectedNode).getState().equals(ICON_BLANK);
        if (NodeType.AGROECOSYSTEME.equals(type)) {
            ((NodeableNode) selectedNode).getChildren().forEach(
                    child0 -> child0.getChildren().forEach(
                            child -> selectTreatment((TraitementVO) child, isBlank)
                    )
            );
        } else if (NodeType.SITE.equals(type)) {
            ((NodeableNode) selectedNode).getChildren().forEach(
                    child -> selectTreatment((TraitementVO) child, isBlank)
            );
        } else {
            final TraitementVO traitementVO = (TraitementVO) selectedNode;
            selectTreatment(traitementVO, !(traitementVO).isSelected());
        }
    }

    public String removeTraitement(TraitementVO treatment) {
        selectTreatment(treatment, false);
        return "";
    }

    private final void selectTreatment(TraitementVO treatment, boolean selected) {
        treatment.setSelected(selected);
        if (selected) {
            sitesTraitements.put(treatment.getTraitement().getId(), treatment);
        } else {
            sitesTraitements.remove(treatment.getTraitement().getId());
        }
        treatment.setSelected(selected);
        setExpanded(treatment, selected);
    }

    /**
     * Update agro availables.
     *
     * @param treatmentManager
     * @param intervalsDate
     */
    public final void updateAgroAvailables(ITreatmentManager treatmentManager, List<IntervalDate> intervalsDate) {
        this.availablesAgroecosystemes = new DefaultTreeNode("Root", null);
        final Set<TraitementProgramme> availablesTraitements = new TreeSet();
        availablesTraitements.addAll(treatmentManager.getAvailableTraitements(intervalsDate));
        Map<SiteACBB, TreeNode> sitesNodes = new HashMap();
        Map<AgroEcosysteme, TreeNode> agroecosystemesNodes = new HashMap();
        TreeNode agroecosystemeTreeNode, siteTreeNode;
        for (final TraitementProgramme traitement : availablesTraitements) {
            SiteACBB site = traitement.getSite();
            AgroEcosysteme agroecosysteme = site.getAgroEcosysteme();
            if (!agroecosystemesNodes.containsKey(agroecosysteme)) {
                final NodeableNode agroEcosystemNode = new NodeableNode(agroecosysteme, availablesAgroecosystemes);
                agroEcosystemNode.setType(NodeType.AGROECOSYSTEME);
                agroecosystemesNodes.put(agroecosysteme, agroEcosystemNode);
            }
            agroecosystemeTreeNode = agroecosystemesNodes.get(agroecosysteme);
            if (!sitesNodes.containsKey(site)) {
                final NodeableNode siteNode = new NodeableNode(site, agroecosystemeTreeNode);
                siteNode.setType(NodeType.SITE);
                sitesNodes.put(site, siteNode);
            }
            siteTreeNode = sitesNodes.get(site);
            new TraitementVO(new TraitementNodeable(traitement), NodeType.TRAITEMENT, siteTreeNode);
        }
    }

    /**
     * Gets the traitement step is valid.
     *
     * @return the traitement step is valid
     */
    public Boolean getTraitementStepIsValid() {
        return !this.sitesTraitements.isEmpty();
    }

    /**
     * @param id
     */
    public final void selectTraitement(Long id) {
        /*TraitementVO traitement = sitesTraitements.get(id);
        sitesTraitements.remove(id);
        traitement.setSelected(false);*/
    }

    /**
     * @param node
     * @param expanded
     */
    public void setExpanded(TreeNode node, boolean expanded) {
        node.setExpanded(expanded);
        if (node.getParent() != null) {
            setExpanded(node.getParent(), expanded);
        }
    }

    /**
     * Gets the availables agroecosystemes.
     *
     * @return the availables agroecosystemes
     */
    public final TreeNode getAvailablesAgroecosystemes() {
        return this.availablesAgroecosystemes;
    }

    /**
     * @return
     */
    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode
     */
    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    /**
     * Gets the list traitement.
     *
     * @return the list traitement
     */
    public List<TraitementProgramme> getListTraitement() {
        final List<TraitementProgramme> traitements = new LinkedList();
        for (final TraitementVO traitement : this.sitesTraitements.values()) {
            traitements.add(traitement.getTraitement());
        }
        return traitements;
    }

    /**
     * @param metadatasMap
     */
    public void addSitetoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(SiteACBB.class.getSimpleName(), getListTraitement());
    }

    /**
     * @param metadatasMap
     */
    public void addTreatmenttoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(TraitementProgramme.class.getSimpleName(), getListTraitement());
    }

    /**
     * The Class NodeType.
     */
    public static class NodeType {

        /**
         * The agroecosysteme.
         */
        public static final String AGROECOSYSTEME = "agroecosysteme";
        /**
         * The site.
         */
        public static final String SITE = "site";
        /**
         * The traitement.
         */
        public static final String TRAITEMENT = "traitement";

        private NodeType() {
        }
    }

    /**
     *
     */
    public class NodeableNode extends DefaultTreeNode implements Comparable<NodeableNode>, TreeNode {

        /**
         * The traitement.
         */
        INodeable nodeable;

        public NodeableNode(INodeable nodeable, TreeNode parent) {
            super(nodeable, parent);
            this.nodeable = nodeable;
        }

        public NodeableNode(INodeable nodeable, String type, Object data, TreeNode parent) {
            super(type, data, parent);
            this.nodeable = nodeable;
        }

        /**
         * @param nodeable
         */
        public NodeableNode(INodeable nodeable) {
            this.nodeable = nodeable;
        }

        /**
         * @return
         */
        public String getName() {
            return localizationManager.getLocalName(nodeable);
        }

        public String getLocalizedDisplayPath() {
            return propertiesAffichageTraitement.getProperty(getName(), getName());
        }

        @Override
        public int compareTo(NodeableNode t) {
            return getName().compareTo(t.getName());
        }

        public String getState() {
            if (getType().equals(NodeType.AGROECOSYSTEME)) {
                if (getChildren().stream()
                        .map(sn -> ((NodeableNode) sn).getChildren())
                        .flatMap(List::stream)
                        .allMatch(child -> child.isSelected())) {
                    return ICON_CHECK;
                }
                if (getChildren().stream()
                        .map(sn -> ((NodeableNode) sn).getChildren())
                        .flatMap(List::stream)
                        .anyMatch(child -> child.isSelected())) {
                    return ICON_INDETERMINATE;
                }

            } else if (getType().equals(NodeType.SITE)) {
                if (getChildren().stream().allMatch(child -> child.isSelected())) {
                    return ICON_CHECK;
                }
                if (getChildren().stream().anyMatch(child -> child.isSelected())) {
                    return ICON_INDETERMINATE;
                }
            } else {
                return isSelected() ? ICON_CHECK : ICON_BLANK;
            }
            return ICON_BLANK;
        }

        @Override
        public Object getData() {
            return this;
        }

        @Override
        public String toString() {
            return "NodeableNode{" + "nodeable=" + nodeable + ", nodeableNodeSelected=" + isSelected() + '}';
        }
    }

    public class TraitementNodeable extends Nodeable {

        private TraitementProgramme treatment;

        public TraitementNodeable(TraitementProgramme treatment) {
            super(treatment.getCode());
            this.treatment = treatment;
        }

        @Override
        public String getName() {
            return treatment.getNom();
        }

        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) TraitementNodeable.class;
        }

        public TraitementProgramme getTreatment() {
            return this.treatment;
        }
    }

    /**
     *
     */
    public class TraitementVO extends NodeableNode {

        /**
         * The traitement.
         */
        TraitementProgramme traitement;

        public TraitementVO(TraitementNodeable treatmentNodeable, Object data, TreeNode parent) {
            super(treatmentNodeable, NodeType.TRAITEMENT, data, parent);
            this.traitement = (TraitementProgramme) treatmentNodeable.getTreatment();
        }

        /**
         * Gets the localized affichage.
         *
         * @return the localized affichage
         */
        public String getLocalizedAffichage() {
            final String localizedAffichage = propertiesAffichageTraitement.getProperty(this.traitement.getAffichage(), this.traitement.getAffichage());
            final String localizedName = propertiesNamesTreatment.getProperty(this.traitement.getNom(), this.traitement.getNom());
            return String.format("%s (%s)", localizedName, localizedAffichage);
        }

        /**
         * Gets the localized display path.
         *
         * @return the localized display path
         */
        public String getLocalizedDisplayPath() {
            final String localizedName = propertiesNamesSite.getProperty(this.traitement.getSite().getAgroEcosysteme().getName(), this.traitement.getSite().getAgroEcosysteme().getName());
            return Strings.isNullOrEmpty(localizedName) ? this.traitement.getSite()
                    .getAgroEcosysteme().getName() : localizedName.concat(
                            PatternConfigurator.ANCESTOR_SEPARATOR).concat(
                                    this.traitement.getSite().getName());
        }

        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return this.traitement.getNom();
        }

        /**
         * Gets the traitement.
         *
         * @return the traitement
         */
        public TraitementProgramme getTraitement() {
            return this.traitement;
        }
    }
}
