/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.biomasse.jsf;

import java.time.format.DateTimeParseException;
import java.util.Collection;
import org.inra.ecoinfo.acbb.extraction.biomasse.IBiomasseDatasetManager;
import org.inra.ecoinfo.acbb.extraction.biomasse.impl.BiomasseParameterVO;
import org.inra.ecoinfo.acbb.extraction.jsf.UIAssociate;
import org.inra.ecoinfo.acbb.extraction.jsf.UIDate;
import org.inra.ecoinfo.acbb.extraction.jsf.UITreatment;
import org.inra.ecoinfo.acbb.extraction.jsf.UIVariable;
import org.inra.ecoinfo.acbb.extraction.jsf.UIVariable.VariableJSF;
import org.inra.ecoinfo.acbb.synthesis.jpa.IIntervalDateSynthesisDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;

/**
 * The Class UIBeanMPS.
 */
@ManagedBean(name = "uiBiomasse")
@ViewScoped
public class UIBeanBiomasse extends AbstractUIBeanForSteps implements Serializable {

    // TODO
    /**
     * Autre type de données @ManagedProperty
     */
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    @ManagedProperty(value = "#{hauteurVegetalDatatype}")
    public String hauteurVegetalDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{laiDatatype}")
    public String laiDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{biomassProductionDatatype}")
    public String biomassProductionDatatype;

    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{biomasseDatasetManager}")
    IBiomasseDatasetManager biomasseDatasetManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;

    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;

    /**
     * The parameters request @link(ParametersRequest).
     */
    @Transient
    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    @Transient
    JpaTransactionManager transactionManager;

    @ManagedProperty(value = "#{intervalDateSynthesisDAO}")
    IIntervalDateSynthesisDAO intervalDateSynthesisDAO;
    List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypeClasses = Stream.of(
            org.inra.ecoinfo.acbb.synthesis.biomasseproduction.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.hauteurvegetal.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.lai.SynthesisDatatype.class
    ).collect(Collectors.toList());

    UITreatment uiTreatment;

    UIDate uiDate;

    UIVariable uiVariable;

    UIAssociate uIAssociate;

    // GETTERS SETTERS - BEANS
    /**
     * Instantiates a new uI bean mps.
     */
    public UIBeanBiomasse() {
        super();
    }

    /**
     * @return
     */
    public UITreatment getUiTreatment() {
        return uiTreatment;
    }

    /**
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     * @return
     */
    public UIVariable getUiVariable() {
        return uiVariable;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        // TODO
        uiDate.addDatestoMap(metadatasMap);
        uiTreatment.addTreatmenttoMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final BiomasseParameterVO parameters = new BiomasseParameterVO(metadatasMap);
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, 1);
        return null;
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesBiomasseProduction() {
        if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(biomassProductionDatatype)) {
            return uiVariable.getAvailablesVariables().get(biomassProductionDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesHauteurVegetal() {
        if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(hauteurVegetalDatatype)) {
            return uiVariable.getAvailablesVariables().get(hauteurVegetalDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesLai() {
        if (uiVariable.getAvailablesVariables() != null && uiVariable.getAvailablesVariables().containsKey(laiDatatype)) {
            return uiVariable.getAvailablesVariables().get(laiDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * autre type de données
     */
    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (getStep()) {
            case 1:
                return uiDate.getDateStepIsValid();
            case 2:
                return uiTreatment.getTraitementStepIsValid();
            case 3:
                return uiVariable.getVariableStepIsValid();
            case 4:
                return uiVariable.getVariableStepIsValid();
            default:
                return false;
        }
    }

    /**
     * @return the parametersRequest
     */
    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * @param parametersRequest the parametersRequest to set
     */
    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    public void initProperties() {
        setEnabledMotivation(true);
        uiTreatment = new UITreatment();
        uiTreatment.initProperties(localizationManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager, intervalDateSynthesisDAO, synthesisDatatypeClasses);
        uiVariable = new UIVariable();
        uiVariable.initVariable(localizationManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "biomasse";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 2:
                uiTreatment.updateAgroAvailables(biomasseDatasetManager, uiDate.getDatesForm1ParamVO().intervalsDate());
                break;
            case 3:
                uiVariable.updateVariablesAvailables(biomasseDatasetManager, uiDate, uiTreatment);
                break;
            case 4:
                final String dateStartString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateStart();
                final String dateEndString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateEnd();
                Map<Long, UIVariable.VariableJSF> variables = uiVariable.getVariables().entrySet().stream()
                        .map(e -> e.getValue().entrySet())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                IntervalDate intervalDate = null;
                try {
                    intervalDate = new IntervalDate(dateStartString, dateEndString, DateUtil.DD_MM_YYYY);
                    uIAssociate.caseassociate(variables, intervalDate, fileCompManager);
                } catch (BadExpectedValueException ex) {
                    LoggerFactory.getLogger(UIBeanBiomasse.class.getName()).error(ex.getMessage(), ex);
                } catch (DateTimeParseException ex) {
                    LoggerFactory.getLogger(UIBeanBiomasse.class.getName()).error(ex.getMessage(), ex);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * @param biomasseDatasetManager the biomasseDatasetManager to set
     */
    public void setBiomasseDatasetManager(IBiomasseDatasetManager biomasseDatasetManager) {
        this.biomasseDatasetManager = biomasseDatasetManager;
    }

    /**
     * @param biomassProductionDatatype
     */
    public void setBiomassProductionDatatype(String biomassProductionDatatype) {
        this.biomassProductionDatatype = biomassProductionDatatype;
    }

    /**
     * @param extractionManager the extractionManager to set
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * @param hauteurVegetalDatatype the hauteurVegetalDatatype to set
     */
    public void setHauteurVegetalDatatype(String hauteurVegetalDatatype) {
        this.hauteurVegetalDatatype = hauteurVegetalDatatype;
    }

    /**
     * @param laiDatatype
     */
    public void setLaiDatatype(String laiDatatype) {
        this.laiDatatype = laiDatatype;
    }

    /**
     * @param localizationManager the localizationManager to set
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * @param notificationsManager the notificationsManager to set
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * @param transactionManager the transactionManager to set
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setIntervalDateSynthesisDAO(IIntervalDateSynthesisDAO intervalDateSynthesisDAO) {
        this.intervalDateSynthesisDAO = intervalDateSynthesisDAO;
    }

    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    public UIAssociate getUiAssociate() {
        return uIAssociate;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The european format @link(boolean).
         */
        boolean format = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getFormat() {
            return this.format;
        }

        /**
         * Sets the european format.
         *
         * @param format
         */
        public final void setFormat(final boolean format) {
            this.format = format;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiDate.getDateStepIsValid() && uiTreatment.getTraitementStepIsValid()
                    && uiVariable.getVariableStepIsValid();
        }
    }
}
