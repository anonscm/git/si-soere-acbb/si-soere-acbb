/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import com.google.common.base.Strings;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.inra.ecoinfo.acbb.extraction.cst.ConstantACBBExtraction;

/**
 * @author tcherniatinsky
 */
public class UIVariable {

    public static final String ICON_BLANK = "ui-icon ui-icon-blank";
    public static final String ICON_CHECK = "ui-icon ui-icon-check";
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";

    /**
     * The variables @link(Map<Long,VariableJSF>).
     */
    protected final Map<String, Map<Long, VariableJSF>> variables = new HashMap();

    /**
     *
     */
    public Map<String, String> localizedDatatypes = new HashMap();

    /**
     * The availables variables.
     */
    protected Map<String, List<VariableJSF>> availablesVariables = new HashMap();
    /**
     * The variable selected @link(VariableJSF).
     */
    protected VariableJSF variableSelected;
    /**
     * The properties variables names @link(Properties).
     */
    protected Properties propertiesVariablesNames;

    /**
     * @return
     */
    public Map<String, Map<Long, VariableJSF>> getVariables() {
        return variables;
    }

    /**
     * Adds the all variables.
     *
     * @return the string
     */
    public final String addAllVariables() {
        variables.clear();
        for (Entry<String, List<VariableJSF>> variableJSFEntry : this.availablesVariables.entrySet()) {
            for (VariableJSF variableJSF : variableJSFEntry.getValue()) {
                variableJSF.selected = true;
                if (!variables.containsKey(variableJSFEntry.getKey())) {
                    variables.put(variableJSFEntry.getKey(),
                            new HashMap());
                }
                variables.get(variableJSFEntry.getKey()).put(variableJSF.getVariable().getId(), variableJSF);
            }
        }
        return null;
    }
    
    public String select(List<VariableJSF> variables){
        String state = states(variables);
        variables.stream()
                .forEach(child -> selectVariable(child, ICON_CHECK.equals(state)));
        return "";
    }

    /**
     * Select variable.
     *
     * @param variableSelected the variable selected
     */
    public final void selectVariable(final VariableJSF variableSelected) {
        if (variableSelected.getSelected()) {
            variables.get(localizedDatatypes.get(variableSelected.datatype)).remove(
                    variableSelected.variable.getId());
            variableSelected.setSelected(false);
        } else {
            if (!variables.containsKey(localizedDatatypes.get(variableSelected.datatype))) {
                variables.put(localizedDatatypes.get(variableSelected.datatype),
                        new HashMap());
            }
            variables.get(localizedDatatypes.get(variableSelected.datatype)).put(
                    variableSelected.variable.getId(), variableSelected);
            variableSelected.setSelected(true);
        }
    }

    public final void selectVariable(final VariableJSF variableSelected, boolean isCheck) {
        if (isCheck) {
            variables.get(localizedDatatypes.get(variableSelected.datatype)).remove(
                    variableSelected.variable.getId());
            variableSelected.setSelected(false);
        } else {
            if (!variables.containsKey(localizedDatatypes.get(variableSelected.datatype))) {
                variables.put(localizedDatatypes.get(variableSelected.datatype),
                        new HashMap());
            }
            variables.get(localizedDatatypes.get(variableSelected.datatype)).put(
                    variableSelected.variable.getId(), variableSelected);
            variableSelected.setSelected(true);
        }
    }

    /**
     * Removes the all variables.
     *
     * @return the string
     */
    public final String removeAllVariables() {
        variables.clear();
        for (final Entry<String, List<VariableJSF>> variableJSFEntry : availablesVariables
                .entrySet()) {
            for (VariableJSF variableJSF : variableJSFEntry.getValue()) {
                variableJSF.selected = false;
            }
        }
        return null;
    }

    /**
     * Update variables availables.
     *
     * @param variableManager
     * @param uiTreatment
     * @param uiDate
     */
    public final void updateVariablesAvailables(IVariableManager variableManager, UIDate uiDate, UITreatment uiTreatment) {
        availablesVariables.clear();
        Map<String, List<NodeDataSet>> availableVariables = variableManager.getAvailableVariablesByTreatmentAndDatesInterval(uiTreatment.getListTraitement(), uiDate.getDatesForm1ParamVO().intervalsDate());
        updateVariables(availableVariables);
    }

    /**
     * Update variables availables.
     *
     * @param variableManager
     * @param uiSite
     * @param uiDate
     */
    public final void updateVariablesAvailables(IVariableManager variableManager, UIDate uiDate, UISite uiSite) {
        availablesVariables.clear();
        Map<String, List<NodeDataSet>> availableVariables = variableManager.getAvailableVariablesByTreatmentAndDatesInterval(uiSite.getListSites(), uiDate.getDatesForm1ParamVO().intervalsDate());
        updateVariables(availableVariables);
    }

    protected void updateVariables(Map<String, List<NodeDataSet>> availableVariables) {
        availableVariables.entrySet().stream().forEach((entry)
                -> {
            final String datatype = entry.getKey();
            List<NodeDataSet> nodes = entry.getValue();
            Map<Variable, Set<NodeDataSet>> nodesMap = new HashMap();
            nodes.stream().forEach(nds -> nodesMap.computeIfAbsent(((DatatypeVariableUniteACBB) nds.getRealNode().getNodeable()).getVariable(), k -> new HashSet()).add(nds));
            List<VariableJSF> variablesJSF = nodesMap.entrySet().stream()
                    .map(p -> new VariableJSF((VariableACBB) p.getKey(), p.getValue(), datatype))
                    .collect(Collectors.toList());
            final String localeString = localizedDatatypes.containsKey(datatype) ? localizedDatatypes.get(datatype) : datatype;
            availablesVariables.put(localeString, variablesJSF);
            if (availablesVariables.get(localeString).isEmpty()) {
                availablesVariables.remove(localeString);
            }
        });
    }

    /**
     * Gets the id variable selected.
     *
     * @return the id variable selected
     */
    public Long getIdVariableSelected() {
        return variableSelected.variable.getId();
    }

    /**
     * @param localizationManager
     */
    public void initVariable(ILocalizationManager localizationManager) {
        this.propertiesVariablesNames = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableACBB.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties localizedDatatypes = localizationManager.newProperties(Nodeable.getLocalisationEntite(DataType.class), Nodeable.ENTITE_COLUMN_NAME);
        for (Map.Entry<Object, Object> entry : localizedDatatypes.entrySet()) {
            String key = Utils.createCodeFromString((String) entry.getKey());
            String value = (String) entry.getValue();
            this.localizedDatatypes.put(key, value);
        }
    }

    /**
     * @param datatype
     * @return
     */
    public List<VariableACBB> getVariablesForDatatype(String datatype) {
        datatype = localizedDatatypes.get(datatype);
        final List<VariableACBB> variablesACBB = new LinkedList();
        if (variables.containsKey(datatype)) {
            for (final VariableJSF variable : this.variables.get(datatype).values()) {
                variablesACBB.add(variable.getVariable());
            }
        }
        return variablesACBB;
    }

    /**
     * Gets the variable selected.
     *
     * @return the variable selected
     */
    public VariableJSF getVariableSelected() {
        return this.variableSelected;
    }

    /**
     * Sets the variable selected.
     *
     * @param variableSelected the new variable selected
     */
    public final void setVariableSelected(final VariableJSF variableSelected) {
        this.variableSelected = variableSelected;
    }

    /**
     * Gets the variable step is valid.
     *
     * @return the variable step is valid
     */
    public Boolean getVariableStepIsValid() {
        return !this.variables.isEmpty();
    }

    /**
     * Gets the availables variables.
     *
     * @return the availables variables
     */
    public final Map<String, List<VariableJSF>> getAvailablesVariables() {
        return this.availablesVariables;
    }

    /**
     * @param metadatasMap
     */
    public void addVariablestoMap(Map<String, Object> metadatasMap) {
        List<String> datatypesNames = new LinkedList<>();
        getVariables().forEach((datatypeName, variablesEntry) -> {
            String datatypeCode = Utils.createCodeFromString(Variable.class.getSimpleName().toLowerCase().concat(datatypeName));
            datatypesNames.add(datatypeCode);
            variablesEntry.forEach((id, variableJSF) -> {
                if (metadatasMap.get(datatypeCode) == null) {
                    metadatasMap.put(datatypeCode, new LinkedList());
                }
                ((Collection<VariableACBB>) metadatasMap.get(datatypeCode)).add(variableJSF.variable);
            });
        });
        metadatasMap.put(ConstantACBBExtraction.DATATYPE_NAME_INDEX, datatypesNames);
    }

    public String states(List<VariableJSF> variables) {
        if (variables.stream()
                .allMatch(child -> child.getSelected())) {
            return ICON_CHECK;
        }
        if (variables.stream()
                .anyMatch(child -> child.getSelected())) {
            return ICON_INDETERMINATE;
        }

        return ICON_BLANK;
    }

    public String state(VariableJSF variable) {
        return variable.selected ? ICON_CHECK : ICON_BLANK;
    }

    /**
     * The Class VariableJSF.
     */
    public class VariableJSF {

        protected final Set<NodeDataSet> nodesVariable;

        /**
         * The selected @link(boolean).
         */
        protected boolean selected = false;
        /**
         * The variable @link(VariableACBB).
         */
        protected VariableACBB variable;
        protected String datatype;

        /**
         * Instantiates a new variable jsf.
         *
         * @param variable the variable
         * @param nodes
         * @param datatype
         */
        public VariableJSF(final VariableACBB variable, Set<NodeDataSet> nodes, String datatype) {
            super();
            this.nodesVariable = nodes;
            this.variable = variable;
            this.datatype = datatype;
        }

        /**
         * @return
         */
        public Set<NodeDataSet> getNodesVariable() {
            return nodesVariable;
        }

        /**
         * @return the datatype
         */
        public String getDatatype() {
            return this.datatype;
        }

        /**
         * @param datatype the datatype to set
         */
        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

        /**
         * Gets the localized name.
         *
         * @return the localized name
         */
        public String getLocalizedName() {
            final String localizedName = propertiesVariablesNames.getProperty(variable.getName(), variable.getName());
            return Strings.isNullOrEmpty(localizedName) ? this.variable.getName() : localizedName;
        }

        /**
         * Gets the selected.
         *
         * @return the selected
         */
        public boolean getSelected() {
            return this.selected;
        }

        /**
         * Sets the selected.
         *
         * @param selected the new selected
         */
        public final void setSelected(final boolean selected) {
            this.selected = selected;
        }

        /**
         * Gets the variable.
         *
         * @return the variable
         */
        public VariableACBB getVariable() {
            return this.variable;
        }

        /**
         * Sets the variable.
         *
         * @param variable the new variable
         */
        public final void setVariable(final VariableACBB variable) {
            this.variable = variable;
        }
    }

}
