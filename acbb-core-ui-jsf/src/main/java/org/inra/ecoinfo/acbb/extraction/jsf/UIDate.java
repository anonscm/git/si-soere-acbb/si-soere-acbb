/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.extraction.jsf;

import org.inra.ecoinfo.acbb.extraction.AbstractDatesFormParam;
import org.inra.ecoinfo.acbb.extraction.DatesFormParamVO;
import org.inra.ecoinfo.acbb.synthesis.jpa.IIntervalDateSynthesisDAO;
import org.inra.ecoinfo.acbb.utils.vo.DatesRequestParamVO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.IntervalDate;

import java.time.LocalDate;
import java.util.*;

/**
 * @author tcherniatinsky
 */
public class UIDate {

    /**
     * The helper form @link(HelperForm).
     */
    HelperForm helperForm = new HelperForm();
    /**
     * The dates request param @link(DatesRequestParamVO).
     */
    DatesRequestParamVO datesRequestParam;
    LocalDate minDate = null;
    LocalDate maxDate = null;

    /**
     *
     */
    public UIDate() {
    }

    // TODO

    /**
     * Adds the period years continuous.
     *
     * @return the string
     */
    public final String addPeriodYearsContinuous() {
        getDateYears().getPeriods().add(buildNewMapPeriod(Optional.empty()));
        return null;
    }

    /**
     * Builds the new map period.
     *
     * @return the map
     */
    protected final Map<String, String> buildNewMapPeriod(Optional<IntervalDate> intervalDateAvailable) {
        final Map<String, String> firstMap = new HashMap<>();
        firstMap.put(AbstractDatesFormParam.START_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        firstMap.put(AbstractDatesFormParam.END_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        intervalDateAvailable.ifPresent(id -> {
            getDatesForm1ParamVO().setDateStart(id.getBeginDate().toLocalDate());
            getDatesForm1ParamVO().setDateEnd(id.getEndDate().toLocalDate());
            firstMap.put(AbstractDatesFormParam.START_INDEX, id.getBeginDateToString());
            firstMap.put(AbstractDatesFormParam.END_INDEX, id.getEndDateToString());
            minDate = id.getBeginDate().toLocalDate();
            maxDate = id.getEndDate().toLocalDate();
        });
        return firstMap;
    }

    /**
     * @return the helperForm
     */
    public HelperForm getHelperForm() {
        return this.helperForm;
    }

    /**
     * @param helperForm the helperForm to set
     */
    public void setHelperForm(HelperForm helperForm) {
        this.helperForm = helperForm;
    }

    /**
     * Removes the period years continuous.
     *
     * @return the string
     */
    public final String removePeriodYearsContinuous() {
        if (getDateYears().getPeriods().size() <= 1) {
            return null;
        }
        getDateYears().getPeriods().remove(getDateYears().getPeriods().get(getDateYears().getPeriods().size() - 1));
        return null;
    }

    /**
     * Update date year continuous.
     *
     * @return the string
     */
    public final String updateDateYearContinuous() {
        final String key = this.helperForm.getKey();
        final Integer index = this.helperForm.getIndex();
        final String value = this.helperForm.getValue();
        getDateYears()
                .getPeriods().get(index).put(key, value);
        return null;
    }

    private DatesFormParamVO getDateYears() {
        return (DatesFormParamVO) datesRequestParam.getDatesFormParam();
    }

    /**
     * Gets the dates form1 param vo.
     *
     * @return the dates form1 param vo
     */
    public DatesFormParamVO getDatesForm1ParamVO() {
        return (DatesFormParamVO) this.datesRequestParam.getDatesFormParam();
    }

    /**
     * Gets the date step is valid.
     *
     * @return the date step is valid
     */
    public Boolean getDateStepIsValid() {
        return getDateYears().getIsValid();
    }

    /**
     * Gets the dates request param.
     *
     * @return the dates request param
     */
    public DatesRequestParamVO getDatesRequestParam() {
        return this.datesRequestParam;
    }

    /**
     * @param datesRequestParam
     */
    public void setDatesRequestParam(DatesRequestParamVO datesRequestParam) {
        this.datesRequestParam = datesRequestParam;
    }

    /**
     * Inits the dates request param.
     *
     * @param localizationManager
     */
    public void initDatesRequestParam(ILocalizationManager localizationManager, IIntervalDateSynthesisDAO intervalDateSynthesisDAO, List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypecClasses) {
        this.datesRequestParam = new DatesRequestParamVO(new DatesFormParamVO(localizationManager));
        Optional<IntervalDate> intervalDateAvailable = intervalDateSynthesisDAO.getIntervalDateAvailable(synthesisDatatypecClasses);
        getDateYears().setPeriods(new LinkedList<>());
        getDateYears().getPeriods().add(buildNewMapPeriod(intervalDateAvailable));
    }

    /**
     * @param metadatasMap
     */
    public void addDatestoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(DatesFormParamVO.class.getSimpleName(), getDatesRequestParam().getDatesFormParam());
    }

    public LocalDate getMinDate() {
        return minDate;
    }

    public LocalDate getMaxDate() {
        return maxDate;
    }

    /**
     * The Class HelperForm.
     */
    public static class HelperForm {

        /**
         * The index @link(Integer).
         */
        Integer index;
        /**
         * The key @link(String).
         */
        String key;
        /**
         * The value @link(String).
         */
        String value;

        /**
         * Gets the index.
         *
         * @return the index
         */
        public Integer getIndex() {
            return this.index;
        }

        /**
         * Sets the index.
         *
         * @param index the new index
         */
        public final void setIndex(final Integer index) {
            this.index = index;
        }

        /**
         * Gets the key.
         *
         * @return the key
         */
        public String getKey() {
            return this.key;
        }

        /**
         * Sets the key.
         *
         * @param key the new key
         */
        public final void setKey(final String key) {
            this.key = key;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }

        /**
         * Sets the value.
         *
         * @param value the new value
         */
        public final void setValue(final String value) {
            this.value = value;
        }
    }

}
