/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.pratiquesdegestion.jsf;

import java.io.Serializable;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.acbb.extraction.itk.IITKDatasetManager;
import org.inra.ecoinfo.acbb.extraction.itk.impl.ITKParameterVO;
import org.inra.ecoinfo.acbb.extraction.jsf.UIAssociate;
import org.inra.ecoinfo.acbb.extraction.jsf.UIDate;
import org.inra.ecoinfo.acbb.extraction.jsf.UITreatment;
import org.inra.ecoinfo.acbb.extraction.jsf.UIVariable;
import org.inra.ecoinfo.acbb.extraction.jsf.UIVariable.VariableJSF;
import org.inra.ecoinfo.acbb.synthesis.jpa.IIntervalDateSynthesisDAO;
import org.inra.ecoinfo.acbb.utils.vo.DatesRequestParamVO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIBeanMPS.
 */
@ManagedBean(name = "uiITK")
@ViewScoped
public class UIBeanITK extends AbstractUIBeanForSteps implements Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    @ManagedProperty(value = "#{semisDatatype}")
    public String semisDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{wsolDatatype}")
    public String wsolDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{paturageDatatype}")
    public String paturageDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{recFauDatatype}")
    public String recFauDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{aiDatatype}")
    public String aiDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{fneDatatype}")
    public String fneDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{fertDatatype}")
    public String fertDatatype;

    /**
     *
     */
    @ManagedProperty(value = "#{phytDatatype}")
    public String phytDatatype;
    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    @Transient
    IExtractionManager extractionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    @Transient
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{itkDatasetManager}")
    @Transient
    IITKDatasetManager itkDatasetManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    @Transient
    INotificationsManager notificationsManager;
    
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;

    /**
     * The parameters request @link(ParametersRequest).
     */
    @Transient
    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    @Transient
    JpaTransactionManager transactionManager;

    @ManagedProperty(value = "#{intervalDateSynthesisDAO}")
    IIntervalDateSynthesisDAO intervalDateSynthesisDAO;
    List<Class<? extends GenericSynthesisDatatype>> synthesisDatatypeClasses = Stream.of(
            org.inra.ecoinfo.acbb.synthesis.autreintervention.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.fauchenonexportee.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.fertilisant.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.paturage.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.phytosanitaire.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.recolteetfauche.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.semis.SynthesisDatatype.class,
            org.inra.ecoinfo.acbb.synthesis.travaildusol.SynthesisDatatype.class
    ).collect(Collectors.toList());

    UITreatment uiTreatment;

    UIDate uiDate;

    UIVariable uiVariable;

    UIAssociate uIAssociate;

    // GETTERS SETTERS - BEANS

    /**
     * Instantiates a new uI bean mps.
     */
    public UIBeanITK() {
        super();
    }

    /**
     * @return
     */
    public UITreatment getUiTreatment() {
        return uiTreatment;
    }

    /**
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     * @return
     */
    public UIVariable getUiVariable() {
        return uiVariable;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("static-access")
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        uiDate.addDatestoMap(metadatasMap);
        uiTreatment.addTreatmenttoMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final ITKParameterVO parameters = new ITKParameterVO(metadatasMap);
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, 1);
        return null;
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesAi() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(aiDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(aiDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesFertilisant() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(fertDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(fertDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesFne() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(fneDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(fneDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesPaturage() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(paturageDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(paturageDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesPhytosanitaire() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(phytDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(phytDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesRecFau() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(recFauDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(recFauDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesSemis() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(semisDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(semisDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * @return
     */
    public List<VariableJSF> getAvailablesVariablesTravailDuSol() {
        if (uiVariable.getAvailablesVariables() != null && this.uiVariable.getAvailablesVariables().containsKey(wsolDatatype)) {
            return this.uiVariable.getAvailablesVariables().get(wsolDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (getStep()) {
            case 1:
                return uiDate.getDateStepIsValid();
            case 2:
                return uiTreatment.getTraitementStepIsValid();
            case 3:
                return uiVariable.getVariableStepIsValid();
            case 4:
                return uiVariable.getVariableStepIsValid();
            default:
                return false;
        }
    }

    /**
     * Gets the parameters request.
     *
     * @return the parameters request
     */
    public final ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Sets the parameters request.
     *
     * @param parametersRequest the new parameters request
     */
    public final void setParametersRequest(final ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    public void initProperties() {
        setEnabledMotivation(true);
        uiTreatment = new UITreatment();
        uiTreatment.initProperties(localizationManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager, intervalDateSynthesisDAO, synthesisDatatypeClasses);
        uiVariable = new UIVariable();
        uiVariable.initVariable(localizationManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "ITK";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 2:
                uiTreatment.updateAgroAvailables(itkDatasetManager, uiDate.getDatesForm1ParamVO().intervalsDate());
                break;
            case 3:
                uiVariable.updateVariablesAvailables(itkDatasetManager, uiDate, uiTreatment);
                break;
            case 4:
                final String dateStartString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateStart();
                final String dateEndString = uiDate.getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0).getDateEnd();
                Map<Long, UIVariable.VariableJSF> variables = uiVariable.getVariables().entrySet().stream()
                        .map(e -> e.getValue().entrySet())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                IntervalDate intervalDate = null;
                try {
                    intervalDate = new IntervalDate(dateStartString, dateEndString, DateUtil.DD_MM_YYYY);
                    uIAssociate.caseassociate(variables, intervalDate, fileCompManager);
                } catch (BadExpectedValueException ex) {
                    LoggerFactory.getLogger(UIBeanITK.class.getName()).error(ex.getMessage(), ex);
                } catch (DateTimeParseException ex) {
                    LoggerFactory.getLogger(UIBeanITK.class.getName()).error(ex.getMessage(), ex);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * @param aiDatatype the aiDatatype to set
     */
    public void setAiDatatype(String aiDatatype) {
        this.aiDatatype = aiDatatype;
    }

    /**
     * Sets the extraction manager.
     *
     * @param extractionManager the new extraction manager
     */
    public final void setExtractionManager(final IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * @param fertDatatype
     */
    public void setFertDatatype(String fertDatatype) {
        this.fertDatatype = fertDatatype;
    }

    /**
     * @param fneDatatype the fneDatatype to set
     */
    public void setFneDatatype(String fneDatatype) {
        this.fneDatatype = fneDatatype;
    }

    /**
     * @param itkDatasetManager the itkDatasetManager to set
     */
    public void setItkDatasetManager(IITKDatasetManager itkDatasetManager) {
        this.itkDatasetManager = itkDatasetManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public final void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * @param paturageDatatype
     */
    public void setPaturageDatatype(String paturageDatatype) {
        this.paturageDatatype = paturageDatatype;
    }

    /**
     * @param phytDatatype
     */
    public void setPhytDatatype(String phytDatatype) {
        this.phytDatatype = phytDatatype;
    }

    /**
     * @param recFauDatatype
     */
    public void setRecFauDatatype(String recFauDatatype) {
        this.recFauDatatype = recFauDatatype;
    }

    /**
     * @param semisDatatype the semisDatatype to set
     */
    public void setSemisDatatype(String semisDatatype) {
        this.semisDatatype = semisDatatype;
    }

    /**
     * Sets the transaction manager.
     *
     * @param transactionManager the new transaction manager
     */
    public final void setTransactionManager(final JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * @param wsolDatatype the wsolDatatype to set
     */
    public void setWsolDatatype(String wsolDatatype) {
        this.wsolDatatype = wsolDatatype;
    }

    public void setIntervalDateSynthesisDAO(IIntervalDateSynthesisDAO intervalDateSynthesisDAO) {
        this.intervalDateSynthesisDAO = intervalDateSynthesisDAO;
    }

    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    public UIAssociate getUiAssociate() {
        return uIAssociate;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The dates request param @link(DatesRequestParamVO).
         */
        DatesRequestParamVO datesRequestParam;
        /**
         * The european format @link(boolean).
         */
        boolean format = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getFormat() {
            return this.format;
        }

        /**
         * Sets the european format.
         *
         * @param format
         */
        public final void setFormat(final boolean format) {
            this.format = format;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiDate.getDateStepIsValid() && uiTreatment.getTraitementStepIsValid()
                    && uiVariable.getVariableStepIsValid();
        }
    }
}
