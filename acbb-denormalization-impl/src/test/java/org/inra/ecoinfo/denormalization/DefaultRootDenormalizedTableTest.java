package org.inra.ecoinfo.denormalization;

import org.junit.*;
import org.mockito.MockitoAnnotations;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ptcherniati
 */
public class DefaultRootDenormalizedTableTest {

    DefaultRootDenormalizedTable rootInstance;
    TestDenormalizedTable a;
    TestDenormalizedTable b;
    TestDenormalizedTable a1;
    TestDenormalizedTable a2;
    TestDenormalizedTable a3;
    TestDenormalizedTable b1;
    TestDenormalizedTable b2;
    TestDenormalizedTable b3;

    /**
     *
     */
    public DefaultRootDenormalizedTableTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Test
    public void getTableName() {
        DefaultRootDenormalizedTable root = new DefaultRootDenormalizedTable();
        root.getChildrenTables().add(this.a);
        Assert.assertTrue(root.getChildrenTables().contains(this.a));

    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.rootInstance = new RootDenormalizedTableImpl();
        this.a = new TestDenormalizedTable("fluxmeteo", org.apache.commons.lang.StringUtils.EMPTY);
        this.b = new TestDenormalizedTable("mps", org.apache.commons.lang.StringUtils.EMPTY);
        this.a1 = new TestDenormalizedTable(
                "flux",
                "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,v.par_id ,v.bloc_id ,v.bloc ,v.repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date_mesure date_mesure,heure heure,null profondeur,valeur.vft_id valeur_id,valeur,flag_qualite qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,null num_repetition\n"
                        + "from toadd\n"
                        + "join sequence_flux_tours_sft \"sequence\" using(ivf_id)\n"
                        + "join versions v using(ivf_id, dty_id)\n"
                        + "join mesure_flux_tours_mft mesure using(sft_id)\n"
                        + "join valeur_flux_tours_vft valeur using(mft_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id");
        this.a2 = new TestDenormalizedTable(
                "chambres",
                "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,v.par_id ,v.bloc_id ,v.bloc ,v.repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,null heure,null profondeur,valeur.vfc_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,null num_repetition\n"
                        + "from valeur_flux_chambres_vfc valeur\n"
                        + "join mesure_flux_chambres_mfc mesure using(mfc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)");
        this.a3 = new TestDenormalizedTable(
                "meteo",
                "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,v.par_id ,v.bloc_id ,v.bloc ,v.repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date_mesure date_mesure,heure heure,null::integer profondeur,valeur.vme_id valeur_id,valeur,null::integer qc,null::character varying vq ,null::bigint vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,null::integer num_repetition\n"
                        + "from valeur_meteo_vme valeur\n"
                        + "join mesure_meteo_mme mesure using(mme_id)\n"
                        + "join sequence_meteo_sme \"sequence\" using(sme_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)");
        this.b1 = new TestDenormalizedTable(
                "ts",
                "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,par.par_id,bloc.bloc_id,bloc.nom bloc,CASE\n"
                        + "            WHEN bloc.code::text <> bloc.nom::text THEN bloc.nom\n"
                        + "            ELSE ''''::character varying\n"
                        + "        END  repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,heure heure,profondeur profondeur,valeur.vts_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,num_repetition\n"
                        + "from valeur_ts_vts valeur\n"
                        + "join mesure_ts_mts mesure using(mts_id)\n"
                        + "join sous_sequence_ts_ssts \"sous_sequence\" using(ssts_id)\n"
                        + "join sequence_ts_sts \"sequence\" using(sts_id)\n"
                        + "join suivi_parcelle_spa spa using(spa_id)\n"
                        + "join parcelle_par par using(par_id)\n"
                        + "JOIN bloc_bloc bloc USING (bloc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)");
        this.b2 = new TestDenormalizedTable(
                "smp",
                "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,par.par_id,bloc.bloc_id,bloc.nom bloc,CASE\n"
                        + "            WHEN bloc.code::text <> bloc.nom::text THEN bloc.nom\n"
                        + "            ELSE ''''::character varying\n"
                        + "        END  repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,heure heure,profondeur profondeur,valeur.vsmp_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,num_repetition\n"
                        + "from valeur_smp_vsmp valeur\n"
                        + "join mesure_smp_msmp mesure using(msmp_id)\n"
                        + "join sous_sequence_smp_sssmp \"sous_sequence\" using(sssmp_id)\n"
                        + "join sequence_smp_ssmp \"sequence\" using(ssmp_id)\n"
                        + "join suivi_parcelle_spa spa using(spa_id)\n"
                        + "join parcelle_par par using(par_id)\n"
                        + "JOIN bloc_bloc bloc USING (bloc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)");
        this.b3 = new TestDenormalizedTable(
                "swc",
                "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,par.par_id,bloc.bloc_id,bloc.nom bloc,CASE\n"
                        + "            WHEN bloc.code::text <> bloc.nom::text THEN bloc.nom\n"
                        + "            ELSE ''''::character varying\n"
                        + "        END  repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,heure heure,profondeur profondeur,valeur.vswc_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,num_repetition\n"
                        + "from valeur_swc_vswc valeur\n"
                        + "join mesure_swc_mswc mesure using(mswc_id)\n"
                        + "join sous_sequence_swc_ssswc \"sous_sequence\" using(ssswc_id)\n"
                        + "join sequence_swc_sswc \"sequence\" using(sswc_id)\n"
                        + "join suivi_parcelle_spa spa using(spa_id)\n"
                        + "join parcelle_par par using(par_id)\n"
                        + "JOIN bloc_bloc bloc USING (bloc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)");
        this.b.children.add(this.b3);
        this.b.children.add(this.b1);
        this.b.children.add(this.b2);
        this.a.children.add(this.a1);
        this.a.children.add(this.a2);
        this.a.children.add(this.a3);
        this.rootInstance.tables.add(this.a);
        this.rootInstance.tables.add(this.b);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addDenormalizedView method, of class DefaultRootDenormalizedView.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddDenormalizedTable() throws Exception {
        RootDenormalizedTableImpl instance = new RootDenormalizedTableImpl();
        instance.addDenormalizedTable(this.a);
        Assert.assertTrue(instance.tables.contains(this.a));
    }

    /**
     * Test of init method, of class DefaultRootDenormalizedView.
     */
    @Test
    public void testInit() {
        String createRequest = this.rootInstance.getCreateRequest();
        Assert.assertEquals("CREATE TABLE IF NOT EXISTS valeurs\n(\n  ivf_id bigint NOT NULL,\n"
                + "  date_mesure date,\n" + "  heure time without time zone,\n"
                + "  profondeur integer,\n" + "  valeur_id bigint NOT NULL,\n" + "  valeur real,\n"
                + "  qc integer,\n" + "  vq character varying,\n" + "  vq_id bigint,\n"
                + "  isqualitative boolean,\n" + "  var_id bigint,\n"
                + "  var_code character varying(255),\n"
                + "  var_affichage character varying(255),\n"
                + "  var_nom character varying(255),\n" + "  par_id bigint,\n"
                + "  parcelle character varying,\n" + "  par_code character varying,\n"
                + "  num_repetition integer,\n"
                + "  CONSTRAINT \"PK\" PRIMARY KEY (ivf_id, valeur_id),\n"
                + "  CONSTRAINT \"par_FK\" FOREIGN KEY (par_id)\n"
                + "      REFERENCES parcelle_par (par_id) MATCH SIMPLE\n"
                + "      ON UPDATE NO ACTION ON DELETE NO ACTION,\n"
                + "  CONSTRAINT \"var_FK\" FOREIGN KEY (var_id)\n"
                + "      REFERENCES variable_acbb_var (id) MATCH SIMPLE\n"
                + "      ON UPDATE NO ACTION ON DELETE NO ACTION\n" + ")\n" + "WITH (\n"
                + "  OIDS=FALSE\n" + ");\n" + "ALTER TABLE valeurs\n" + "  OWNER TO acbbuser;\n"
                + "\n" + "-- Index: par_code_profondeur_index\n" + "\n"
                + "CREATE INDEX par_code_profondeur_index\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (profondeur, par_code COLLATE pg_catalog.\"default\");\n" + "\n"
                + "-- Index: parcelle_profondeur_index\n" + "\n"
                + "-- DROP INDEX parcelle_profondeur_index;\n" + "\n"
                + "CREATE INDEX parcelle_profondeur_index\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (par_id, profondeur);\n" + "\n" + "-- Index: val_date_index\n" + "\n"
                + "-- DROP INDEX val_date_index;\n" + "\n" + "CREATE INDEX val_date_index\n"
                + "  ON valeurs\n" + "  USING btree\n" + "  (date_mesure);\n" + "\n"
                + "-- Index: val_date_time_index\n" + "\n" + "-- DROP INDEX val_date_time_index;\n"
                + "\n" + "CREATE INDEX val_date_time_index\n" + "  ON valeurs\n"
                + "  USING btree\n" + "  (date_mesure, heure);\n" + "\n"
                + "-- Index: val_parcelle_index\n" + "\n" + "-- DROP INDEX val_parcelle_index;\n"
                + "\n" + "CREATE INDEX val_parcelle_index\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (parcelle COLLATE pg_catalog.\"default\");\n" + "\n"
                + "-- Index: val_repetition_index\n" + "\n"
                + "-- DROP INDEX val_repetition_index;\n" + "\n"
                + "CREATE INDEX val_repetition_index\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (num_repetition);\n" + "\n" + "-- Index: val_var_affichage_index\n" + "\n"
                + "-- DROP INDEX val_var_affichage_index;\n" + "\n"
                + "CREATE INDEX val_var_affichage_index\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (var_affichage COLLATE pg_catalog.\"default\");\n" + "\n"
                + "-- Index: val_variable_index\n" + "\n" + "-- DROP INDEX val_variable_index;\n"
                + "\n" + "CREATE INDEX val_variable_index\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (var_affichage COLLATE pg_catalog.\"default\");\n" + "\n"
                + "-- Index: valeurs_par_id_idx\n" + "\n" + "-- DROP INDEX valeurs_par_id_idx;\n"
                + "\n" + "CREATE INDEX valeurs_par_id_idx\n" + "  ON valeurs\n" + "  USING btree\n"
                + "  (par_id);\n" + "\n", createRequest);
    }

    /**
     * Test of getSelect method, of class DefaultRootDenormalizedView.
     */
    @Test
    public void testRefreshTables() {
        String refreshRequest = this.rootInstance.getRefreshRequest();
        Assert.assertEquals(
                "with \n"
                        + "published AS \n"
                        + "      (SELECT DISTINCT insertion_version_file_ivf.ivf_id, agroecosysteme_site_theme_datatype.dty_id\n"
                        + "        FROM insertion_version_file_ivf\n"
                        + "        JOIN insertion_dataset_ids USING (ivf_id)\n"
                        + "        JOIN agroecosysteme_site_theme_datatype_parcelle USING (dtn_id)\n"
                        + "        JOIN agroecosysteme_site_theme_datatype USING (astd_id)\n"
                        + "        JOIN datatype dty ON dty.id = agroecosysteme_site_theme_datatype.dty_id),\n"
                        + "register AS \n"
                        + "      (SELECT DISTINCT v.ivf_id, v.dty_id\n"
                        + "        FROM valeurs v),\n"
                        + "toadd AS \n"
                        + "              (SELECT published.ivf_id, published.dty_id\n"
                        + "                FROM published\n"
                        + "     EXCEPT \n"
                        + "SELECT register.ivf_id, register.dty_id\n"
                        + "               FROM register)\n"
                        + "\n"
                        + "\n"
                        + "--insertion des valeurs meteo\n"
                        + "insert into valeurs \n"
                        + "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,v.par_id ,v.bloc_id ,v.bloc ,v.repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date_mesure date_mesure,heure heure,null profondeur,valeur.vft_id valeur_id,valeur,flag_qualite qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,null num_repetition\n"
                        + "from toadd\n"
                        + "join sequence_flux_tours_sft \"sequence\" using(ivf_id)\n"
                        + "join versions v using(ivf_id, dty_id)\n"
                        + "join mesure_flux_tours_mft mesure using(sft_id)\n"
                        + "join valeur_flux_tours_vft valeur using(mft_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "\n"
                        + "UNION\n"
                        + " \n"
                        + "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,v.par_id ,v.bloc_id ,v.bloc ,v.repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,null heure,null profondeur,valeur.vfc_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,null num_repetition\n"
                        + "from valeur_flux_chambres_vfc valeur\n"
                        + "join mesure_flux_chambres_mfc mesure using(mfc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)\n"
                        + "\n"
                        + "UNION\n"
                        + " \n"
                        + "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,v.par_id ,v.bloc_id ,v.bloc ,v.repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date_mesure date_mesure,heure heure,null::integer profondeur,valeur.vme_id valeur_id,valeur,null::integer qc,null::character varying vq ,null::bigint vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,null::integer num_repetition\n"
                        + "from valeur_meteo_vme valeur\n"
                        + "join mesure_meteo_mme mesure using(mme_id)\n"
                        + "join sequence_meteo_sme \"sequence\" using(sme_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)\n"
                        + "\n"
                        + "UNION\n"
                        + " \n"
                        + "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,par.par_id,bloc.bloc_id,bloc.nom bloc,CASE\n"
                        + "            WHEN bloc.code::text <> bloc.nom::text THEN bloc.nom\n"
                        + "            ELSE ''''::character varying\n"
                        + "        END  repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,heure heure,profondeur profondeur,valeur.vswc_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,num_repetition\n"
                        + "from valeur_swc_vswc valeur\n"
                        + "join mesure_swc_mswc mesure using(mswc_id)\n"
                        + "join sous_sequence_swc_ssswc \"sous_sequence\" using(ssswc_id)\n"
                        + "join sequence_swc_sswc \"sequence\" using(sswc_id)\n"
                        + "join suivi_parcelle_spa spa using(spa_id)\n"
                        + "join parcelle_par par using(par_id)\n"
                        + "JOIN bloc_bloc bloc USING (bloc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)\n"
                        + "\n"
                        + "UNION\n"
                        + " \n"
                        + "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,par.par_id,bloc.bloc_id,bloc.nom bloc,CASE\n"
                        + "            WHEN bloc.code::text <> bloc.nom::text THEN bloc.nom\n"
                        + "            ELSE ''''::character varying\n"
                        + "        END  repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,heure heure,profondeur profondeur,valeur.vts_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,num_repetition\n"
                        + "from valeur_ts_vts valeur\n"
                        + "join mesure_ts_mts mesure using(mts_id)\n"
                        + "join sous_sequence_ts_ssts \"sous_sequence\" using(ssts_id)\n"
                        + "join sequence_ts_sts \"sequence\" using(sts_id)\n"
                        + "join suivi_parcelle_spa spa using(spa_id)\n"
                        + "join parcelle_par par using(par_id)\n"
                        + "JOIN bloc_bloc bloc USING (bloc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)\n"
                        + "\n"
                        + "UNION\n"
                        + " \n"
                        + "select v.ivf_id,v.path,date_debut_periode,date_fin_periode,ids_id,dtn_id,astd_id,ast_id,agr_id,v.sit_id,v.the_id,v.dty_id,par.par_id,bloc.bloc_id,bloc.nom bloc,CASE\n"
                        + "            WHEN bloc.code::text <> bloc.nom::text THEN bloc.nom\n"
                        + "            ELSE ''''::character varying\n"
                        + "        END  repetition ,v.agr_code ,v.agroecosysteme,v.site_code,v.site,v.the_code ,v.theme ,v.dty_code ,v.datatype ,v.par_code ,v.parcelle ,\n"
                        + "date date_mesure,heure heure,profondeur profondeur,valeur.vsmp_id valeur_id,valeur,null qc,null vq ,null vq_id ,isqualitative,var.id ,var.code var_code ,var.affichage var_affichage ,var.nom var_nom ,num_repetition\n"
                        + "from valeur_smp_vsmp valeur\n"
                        + "join mesure_smp_msmp mesure using(msmp_id)\n"
                        + "join sous_sequence_smp_sssmp \"sous_sequence\" using(sssmp_id)\n"
                        + "join sequence_smp_ssmp \"sequence\" using(ssmp_id)\n"
                        + "join suivi_parcelle_spa spa using(spa_id)\n"
                        + "join parcelle_par par using(par_id)\n"
                        + "JOIN bloc_bloc bloc USING (bloc_id)\n"
                        + "left join versions v using(ivf_id)\n"
                        + "join variable_acbb_var var on var.id=valeur.id\n"
                        + "join toadd using(ivf_id,dty_id)\n" + ";", refreshRequest);
    }

    private class RootDenormalizedTableImpl extends DefaultRootDenormalizedTable {

        RootDenormalizedTableImpl() {
            this.bundlePath = "org.inra.ecoinfo.denormalization.messageTest";
        }
    }

    /**
     *
     */
    public class TestDenormalizedTable implements IDenormalizedTable {

        /**
         *
         */
        public List<IDenormalizedTable> children = new LinkedList();
        /**
         *
         */
        public String select;
        /**
         *
         */
        public String tableName;

        /**
         * @param tableName
         * @param select
         */
        public TestDenormalizedTable(String tableName, String select) {
            this.select = select;
            this.tableName = tableName;
        }

        /**
         * @return
         */
        @Override
        public List<IDenormalizedTable> getChildrenTables() {
            return this.children;
        }

        /**
         * @return
         */
        @Override
        public String getSelect() {
            return this.select;
        }

        /**
         * @return
         */
        @Override
        public String getTableName() {
            return this.tableName;
        }
    }

}
