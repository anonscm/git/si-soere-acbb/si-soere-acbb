/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.junit.*;

/**
 * @author ptcherniati
 */
public class DefaultDenormalizedViewTest {

    /**
     *
     */
    public DefaultDenormalizedViewTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getChildrenViews method, of class DefaultDenormalizedView.
     */
    @Test
    public void testGetChildrenViews() {
        DefaultDenormalizedView instance = new DefaultDenormalizedView();
        Assert.assertTrue(instance.getChildrenViews().isEmpty());
        DefaultDenormalizedView child = new DefaultDenormalizedView();
        instance.views.add(child);
        Assert.assertEquals(child, instance.getChildrenViews().get(0));
    }

    /**
     * Test of getSelect method, of class DefaultDenormalizedView.
     */
    @Test
    public void testGetSelect() {
        DefaultDenormalizedView instance = new DefaultDenormalizedView("name",
                "org.inra.ecoinfo.denormalization.messageTest");
        Assert.assertEquals("select * from table", instance.getSelect());
    }

    /**
     * Test of getViewName method, of class DefaultDenormalizedView.
     */
    @Test
    public void testGetViewName() {
        DefaultDenormalizedView instance = new DefaultDenormalizedView(null, null);
        Assert.assertNull(instance.getViewName());
        instance = new DefaultDenormalizedView("name",
                "org.inra.ecoinfo.denormalization.messageTest");
        Assert.assertEquals("name", instance.getViewName());
    }

}
