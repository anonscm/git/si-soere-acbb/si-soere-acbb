/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ptcherniati
 */
public class DefaultRootDenormalizedViewTest {


    @Spy
    DefaultRootDenormalizedView rootInstance;
    TestDenormalizedView a;
    TestDenormalizedView b;
    TestDenormalizedView a1;
    TestDenormalizedView a2;
    TestDenormalizedView a3;
    TestDenormalizedView b1;
    TestDenormalizedView b2;
    TestDenormalizedView b21;

    /**
     *
     */
    public DefaultRootDenormalizedViewTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.rootInstance = new DefaultRootDenormalizedView();
        DefaultRootDenormalizedView.viewsMap.clear();
        this.rootInstance.viewName = "root";
        this.rootInstance.setBundlePath("org.inra.ecoinfo.denormalization.messages");
        this.a = new TestMaterializedView("a", "select * from a");
        this.b = new TestDenormalizedView("b", "select * from b");
        this.a1 = new TestDenormalizedView("a1", "select * from a1");
        this.a2 = new TestMaterializedView("a2", "select * from a2");
        this.a3 = new TestMaterializedView("a3", "select * from a3");
        this.b1 = new TestMaterializedView("b1", "select * from b1");
        this.b2 = new TestDenormalizedView("b2", "select * from b2");
        this.b21 = new TestMaterializedView("b21", "select * from b21");
        this.b2.children.add(this.b21);
        this.b.children.add(this.b1);
        this.b.children.add(this.b2);
        this.a.children.add(this.a1);
        this.a.children.add(this.a2);
        this.a.children.add(this.a3);
        this.rootInstance.views.add(this.a);
        this.rootInstance.views.add(this.b);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addDenormalizedView method, of class DefaultRootDenormalizedView.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddDenormalizedView() throws Exception {
        DefaultRootDenormalizedView instance = new DefaultRootDenormalizedView();
        instance.viewName = "root";
        instance.addDenormalizedView(this.a);
        Assert.assertTrue(DefaultRootDenormalizedView.viewsMap.containsKey("a"));
        Assert.assertTrue(DefaultRootDenormalizedView.viewsMap.containsValue(this.a));
        try {
            instance.addDenormalizedView(this.a);
        } catch (BusinessException e) {
            Assert.assertEquals("a already in views", e.getMessage());
            return;
        }
        Assert.fail("Pas d'exception lancée pour doublon de vue");
    }

    /**
     * Test of getCreateViewSelect method, of class DefaultRootDenormalizedView.
     */
    @Test
    public void testGetCreateViewSelect() {
        // nominal
        IDenormalizedView view = new TestDenormalizedView("view", "select");
        DefaultRootDenormalizedView instance = new DefaultRootDenormalizedView();
        String expResult = "CREATE OR REPLACE VIEW view AS select;";
        String result = instance.getCreateViewSelect(view);
        Assert.assertEquals(expResult, result);
        // empty select
        view = new TestDenormalizedView("view", org.apache.commons.lang.StringUtils.EMPTY);
        result = instance.getCreateViewSelect(view);
        Assert.assertNull(result);
        // null select
        view = new TestDenormalizedView("view", null);
        result = instance.getCreateViewSelect(view);
        Assert.assertNull(result);
    }

    /**
     * Test of initAndGetCreateRequest method, of class
     * DefaultRootDenormalizedView.
     */
    @Test
    public void testInit() {
        String request = this.rootInstance.initAndGetCreateRequest();
        Assert.assertEquals("CREATE OR REPLACE MATERIALIZED VIEW a AS select * from a;\n"
                + "CREATE OR REPLACE VIEW a1 AS select * from a1;\n"
                + "CREATE OR REPLACE MATERIALIZED VIEW a2 AS select * from a2;\n"
                + "CREATE OR REPLACE MATERIALIZED VIEW a3 AS select * from a3;\n"
                + "CREATE OR REPLACE VIEW b AS select * from b;\n"
                + "CREATE OR REPLACE MATERIALIZED VIEW b1 AS select * from b1;\n"
                + "CREATE OR REPLACE VIEW b2 AS select * from b2;\n"
                + "CREATE OR REPLACE MATERIALIZED VIEW b21 AS select * from b21;\n" + org.apache.commons.lang.StringUtils.EMPTY, request);
        request = this.rootInstance.getRefreshRequest();
        Assert.assertEquals("REFRESH MATERIALIZED VIEW a;\n" + "REFRESH MATERIALIZED VIEW a2;\n"
                + "REFRESH MATERIALIZED VIEW a3;\n" + "REFRESH MATERIALIZED VIEW b1;\n"
                + "REFRESH MATERIALIZED VIEW b21;\n" + org.apache.commons.lang.StringUtils.EMPTY, request);
    }

    /**
     *
     */
    public class TestDenormalizedView implements IDenormalizedView {

        /**
         *
         */
        public List<IDenormalizedView> children = new LinkedList();
        /**
         *
         */
        public String select;
        /**
         *
         */
        public String viewName;

        /**
         * @param viewName
         * @param select
         */
        public TestDenormalizedView(String viewName, String select) {
            this.select = select;
            this.viewName = viewName;
        }

        /**
         * @return
         */
        @Override
        public List<IDenormalizedView> getChildrenViews() {
            return this.children;
        }

        /**
         * @return
         */
        @Override
        public String getSelect() {
            return this.select;
        }

        /**
         * @return
         */
        @Override
        public String getViewName() {
            return this.viewName;
        }
    }

    /**
     *
     */
    public class TestMaterializedView extends TestDenormalizedView implements IMaterializedView {

        /**
         * @param viewName
         * @param select
         */
        public TestMaterializedView(String viewName, String select) {
            super(viewName, select);
        }
    }

}
