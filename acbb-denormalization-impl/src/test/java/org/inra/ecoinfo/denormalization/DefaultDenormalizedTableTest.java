/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.junit.*;

import java.util.List;

/**
 * @author ptcherniati
 */
public class DefaultDenormalizedTableTest {

    /**
     *
     */
    public DefaultDenormalizedTableTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getChildrenTables method, of class DefaultDenormalizedTable.
     */
    @Test
    public void testGetChildrenTables() {
        DefaultDenormalizedTable instance = new DefaultDenormalizedTable();
        Assert.assertTrue(instance.getChildrenTables().isEmpty());
        DefaultDenormalizedTable child = new DefaultDenormalizedTable();
        instance.addChild(child);
        List<IDenormalizedTable> result = instance.getChildrenTables();
        Assert.assertEquals(child, result.get(0));
    }

    /**
     * Test of getSelect method, of class DefaultDenormalizedTable.
     */
    @Test
    public void testGetSelect() {
        DefaultDenormalizedTable instance = new DefaultDenormalizedTable(
                "org.inra.ecoinfo.denormalization.messageTest", "name");
        String expResult = "select * from table";
        String result = instance.getSelect();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getTableName method, of class DefaultDenormalizedTable.
     */
    @Test
    public void testGetTableName() {
        DefaultDenormalizedTable instance = new DefaultDenormalizedTable(null, null);
        Assert.assertNull(instance.getTableName());
        instance = new DefaultDenormalizedTable(null, "name");
        Assert.assertEquals("name", instance.getTableName());
    }

    /**
     * Test of setBundlePath method, of class DefaultDenormalizedTable.
     */
    @Test
    public void testSetBundlePath() {
        DefaultDenormalizedTable instance = new DefaultDenormalizedTable(
                "org.inra.ecoinfo.denormalization.messageTest", "name");
        instance.setBundlePath("org.inra.ecoinfo.denormalization.messageTest");
        Assert.assertEquals("select * from table", instance.getSelect());
    }

}
