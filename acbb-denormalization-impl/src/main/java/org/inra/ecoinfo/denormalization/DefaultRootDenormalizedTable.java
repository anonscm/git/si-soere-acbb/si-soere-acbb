/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.assertj.core.util.Strings;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author ptcherniati
 */
public class DefaultRootDenormalizedTable implements IRootDenormalizedTable {

    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.denormalization.messages";
    /**
     *
     */
    protected static final ResourceBundle BUNDLE = ResourceBundle
            .getBundle(DefaultRootDenormalizedTable.BUNDLE_PATH);
    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DefaultRootDenormalizedTable.class);
    private static final String PROPERTY_SQL_CREATE_TABLE = "PROPERTY_SQL_CREATE_TABLE";
    private static final String PROPERTY_SQL_REFRESH_TABLE = "PROPERTY_SQL_REFRESH_TABLE";
    private static final String PROPERTY_SQL_UNION = "PROPERTY_SQL_UNION";
    /**
     *
     */
    protected List<IDenormalizedTable> tables = new LinkedList();

    /**
     *
     */
    protected String selectRequest;

    /**
     *
     */
    protected String bundlePath;

    /**
     * @param table
     * @throws BusinessException
     */
    @Override
    public final void addDenormalizedTable(IDenormalizedTable table) throws BusinessException {
        if (!this.tables.contains(table)) {
            this.tables.add(table);
        }
    }

    /**
     * @return
     */
    protected List<IDenormalizedTable> getChildrenTables() {
        return this.tables;

    }

    /**
     * @return
     */
    @Override
    public String getCreateRequest() {
        String createRequest = ResourceBundle.getBundle(this.bundlePath).getString(
                DefaultRootDenormalizedTable.PROPERTY_SQL_CREATE_TABLE);
        if (createRequest != null) {
            return createRequest;
        }
        return null;
    }

    /**
     * @return
     */
    @Override
    public String getRefreshRequest() {
        String request = String.format(ResourceBundle.getBundle(this.bundlePath).getString(
                DefaultRootDenormalizedTable.PROPERTY_SQL_REFRESH_TABLE));
        List<String> requests = new LinkedList();
        for (IDenormalizedTable table : this.tables) {
            this.refresh(table, requests);
        }
        for (int i = 0; i < requests.size(); i++) {
            if (i > 0) {
                request += String.format(DefaultRootDenormalizedTable.BUNDLE
                        .getString(DefaultRootDenormalizedTable.PROPERTY_SQL_UNION));
            }
            request = String.format("%s %n%s%n", request, requests.get(i));
        }
        return request + ";";
    }

    /**
     * @param table
     * @param requests
     */
    protected void refresh(IDenormalizedTable table, List<String> requests) {
        String request = table.getSelect();
        if (!Strings.isNullOrEmpty(request)) {
            requests.add(request);
        }
        for (IDenormalizedTable denormalizedTable : table.getChildrenTables()) {
            this.refresh(denormalizedTable, requests);
        }
    }

    /**
     * @param bundlePath
     */
    public void setBundlePath(String bundlePath) {
        this.bundlePath = bundlePath;
    }
}
