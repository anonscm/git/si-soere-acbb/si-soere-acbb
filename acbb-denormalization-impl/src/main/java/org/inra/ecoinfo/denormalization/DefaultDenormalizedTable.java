/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author ptcherniati
 */
public class DefaultDenormalizedTable implements IDenormalizedTable {

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DefaultDenormalizedTable.class);
    private static final String PROPERTY_SQL_SELECT_REQUEST = "PROPERTY_SQL_SELECT_REQUEST";
    private final List<IDenormalizedTable> tables = new LinkedList();
    /**
     *
     */
    protected String tableName;

    /**
     *
     */
    protected ResourceBundle bundle;

    /**
     *
     */
    protected String select;

    /**
     *
     */
    public DefaultDenormalizedTable() {
        super();
    }

    /**
     * @param bundlePath
     * @param tableName
     */
    public DefaultDenormalizedTable(String bundlePath, String tableName) {
        super();
        this.setBundlePath(bundlePath);
        this.tableName = tableName;
    }

    /**
     * @param table
     */
    public void addChild(IDenormalizedTable table) {
        this.tables.add(table);
    }

    /**
     * @return
     */
    @Override
    public List<IDenormalizedTable> getChildrenTables() {
        return this.tables;
    }

    /**
     * @return
     */
    @Override
    public String getSelect() {
        return Strings.isNullOrEmpty(this.select) ? org.apache.commons.lang.StringUtils.EMPTY : this.select;
    }

    /**
     * @return
     */
    @Override
    public String getTableName() {
        return this.tableName;
    }

    /**
     * @param bundlePath
     */
    public void setBundlePath(String bundlePath) {
        try {
            this.bundle = ResourceBundle.getBundle(bundlePath);
            this.select = this.bundle == null ? org.apache.commons.lang.StringUtils.EMPTY : String.format(this.bundle
                    .getString(DefaultDenormalizedTable.PROPERTY_SQL_SELECT_REQUEST));
        } catch (Exception e) {
            DefaultDenormalizedTable.LOGGER.error("not found bundle {}", bundlePath);
        }
    }
}
