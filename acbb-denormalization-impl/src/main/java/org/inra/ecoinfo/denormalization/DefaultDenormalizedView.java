/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.assertj.core.util.Strings;

import java.util.*;

/**
 * @author ptcherniati
 */
public class DefaultDenormalizedView implements IDenormalizedView {

    private static final String PROPERTY_SQL_SELECT_VIEW_REQUEST = "PROPERTY_SQL_SELECT_VIEW_REQUEST";

    /**
     *
     */
    protected static Map<String, IDenormalizedView> viewsMap = new TreeMap();

    /**
     *
     */
    protected String bundlePath;

    /**
     *
     */
    protected List<IDenormalizedView> views = new LinkedList();

    /**
     *
     */
    protected String viewName;

    /**
     *
     */
    public DefaultDenormalizedView() {
        super();
    }

    /**
     * @param viewName
     * @param bundlePath
     */
    public DefaultDenormalizedView(String viewName, String bundlePath) {
        super();
        this.viewName = viewName;
        this.bundlePath = bundlePath;
    }

    /**
     * @return
     */
    @Override
    public List<IDenormalizedView> getChildrenViews() {
        return this.views;
    }

    /**
     * @return
     */
    @Override
    public String getSelect() {
        final ResourceBundle bundle = ResourceBundle.getBundle(this.bundlePath);
        final String select = this.bundlePath == null ? org.apache.commons.lang.StringUtils.EMPTY : String.format(bundle
                .getString(DefaultDenormalizedView.PROPERTY_SQL_SELECT_VIEW_REQUEST));
        return Strings.isNullOrEmpty(select) ? org.apache.commons.lang.StringUtils.EMPTY : select;
    }

    /**
     * @return
     */
    @Override
    public String getViewName() {
        return this.viewName;
    }
}
