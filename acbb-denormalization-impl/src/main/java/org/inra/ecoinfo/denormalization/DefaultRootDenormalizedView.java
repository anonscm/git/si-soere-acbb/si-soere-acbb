/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.denormalization;

import org.assertj.core.util.Strings;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author ptcherniati
 */
public class DefaultRootDenormalizedView implements IRootDenormalizedView {

    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.denormalization.messages";
    /**
     *
     */
    protected static final ResourceBundle BUNDLE = ResourceBundle
            .getBundle(DefaultRootDenormalizedView.BUNDLE_PATH);
    /**
     *
     */
    protected static final String PROPERTY_MSG_DUPLICATE_VIEW = "PROPERTY_MSG_DUPLICATE_VIEW";
    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DefaultRootDenormalizedView.class);
    private static final String PROPERTY_SQL_CREATE_OR_REPLACE_VIEW = "PROPERTY_SQL_CREATE_OR_REPLACE_VIEW";
    private static final String PROPERTY_SQL_MATERIEALIZED = "PROPERTY_SQL_MATERIEALIZED";
    private static final String PROPERTY_SQL_REFRESH_VIEW = "PROPERTY_SQL_REFRESH_VIEW";
    /**
     *
     */
    protected static Map<String, IDenormalizedView> viewsMap = new TreeMap();
    /**
     *
     */
    protected List<IDenormalizedView> views = new LinkedList();
    /**
     *
     */
    protected String selectRequest;
    /**
     *
     */
    protected String viewName;
    private String bundlePath;

    /**
     * @param view
     * @throws BusinessException
     */
    @Override
    public final void addDenormalizedView(IDenormalizedView view) throws BusinessException {
        final String viewName = view.getViewName();
        if (DefaultRootDenormalizedView.viewsMap.containsKey(viewName)) {
            final String message = String.format(DefaultRootDenormalizedView.BUNDLE
                    .getString(DefaultRootDenormalizedView.PROPERTY_MSG_DUPLICATE_VIEW), viewName);
            DefaultRootDenormalizedView.LOGGER.error(message);
            throw new BusinessException(message);
        }
        DefaultRootDenormalizedView.viewsMap.put(view.getViewName(), view);
    }

    private String execute(List<IDenormalizedView> views) {
        if (views == null || views.isEmpty()) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        String request = org.apache.commons.lang.StringUtils.EMPTY;
        for (IDenormalizedView view : views) {
            try {
                this.addDenormalizedView(view);
            } catch (BusinessException e) {
                continue;
            }
            String select = view.getSelect();
            if (select != null) {
                request += String.format("%s%n", this.getCreateViewSelect(view));
            }
            request += this.execute(view.getChildrenViews());
        }
        return request;
    }

    /**
     * @return
     */
    protected List<IDenormalizedView> getChildrenViews() {
        return this.views;
    }

    /**
     * @param view
     * @return
     */
    protected final String getCreateViewSelect(IDenormalizedView view) {
        String materialized = org.apache.commons.lang.StringUtils.EMPTY;
        if (Strings.isNullOrEmpty(view.getSelect())) {
            return null;
        } else if (view instanceof IMaterializedView) {
            materialized = String.format(ResourceBundle.getBundle(this.bundlePath).getString(
                    DefaultRootDenormalizedView.PROPERTY_SQL_MATERIEALIZED));
        }
        return String.format(DefaultRootDenormalizedView.BUNDLE
                        .getString(DefaultRootDenormalizedView.PROPERTY_SQL_CREATE_OR_REPLACE_VIEW),
                materialized, view.getViewName(), view.getSelect());
    }

    /**
     * @return
     */
    @Override
    public String getRefreshRequest() {
        if (this.views == null || this.views.isEmpty()) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        return this.refresh(this.views);
    }

    private String getViewName(IDenormalizedView view) {
        if (view == null) {
            return null;
        }
        for (Map.Entry<String, IDenormalizedView> entrySet : DefaultRootDenormalizedView.viewsMap
                .entrySet()) {
            String key = entrySet.getKey();
            if (view.equals(entrySet.getValue())) {
                return key;
            }
        }
        return null;
    }

    /**
     * @return
     */
    @Override
    public String initAndGetCreateRequest() {
        return this.execute(this.getChildrenViews());
    }

    /**
     * @param views
     * @return
     */
    protected String refresh(List<IDenormalizedView> views) {
        if (views == null || views.isEmpty()) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        String refresh = org.apache.commons.lang.StringUtils.EMPTY;
        for (IDenormalizedView view : views) {
            if (view instanceof IMaterializedView) {
                String viewName = this.getViewName(view);
                refresh += String.format(
                        ResourceBundle.getBundle(this.bundlePath).getString(
                                DefaultRootDenormalizedView.PROPERTY_SQL_REFRESH_VIEW), viewName)
                        + "\n";
            }
            refresh += this.refresh(view.getChildrenViews());
        }
        return refresh;
    }

    /**
     * @param bundlePath
     */
    public void setBundlePath(String bundlePath) {
        this.bundlePath = bundlePath;
    }
}
