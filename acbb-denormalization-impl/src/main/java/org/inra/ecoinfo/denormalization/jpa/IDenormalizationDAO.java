package org.inra.ecoinfo.denormalization.jpa;

/**
 * @author ptcherniati
 */
public interface IDenormalizationDAO {

    /**
     * @param request
     */
    void execute(String request);

}
