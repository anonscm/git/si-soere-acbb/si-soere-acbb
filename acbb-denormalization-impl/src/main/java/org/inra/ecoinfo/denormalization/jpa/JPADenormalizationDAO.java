package org.inra.ecoinfo.denormalization.jpa;

import com.google.common.base.Strings;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

/**
 * @author ptcherniati
 */
public class JPADenormalizationDAO extends AbstractJPADAO<VersionFile> implements
        IDenormalizationDAO {

    /**
     * @param request
     */
    @Override
    public void execute(String request) {
        if (Strings.isNullOrEmpty(request)) {
            return;
        }
        Session session = (Session) this.entityManager.getDelegate();
        Transaction t = null;
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        t = session.beginTransaction();
        NativeQuery query = session.createNativeQuery(request);
        try {
            query.executeUpdate();
            t.commit();
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error("{}{}", e.getMessage(), e.getCause() == null ? org.apache.commons.lang.StringUtils.EMPTY : e.getCause().getMessage());
            t.rollback();
        } finally {
            session.close();
        }

    }

}
