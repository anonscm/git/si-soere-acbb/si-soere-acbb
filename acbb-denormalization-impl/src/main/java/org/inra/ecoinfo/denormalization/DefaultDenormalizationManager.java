package org.inra.ecoinfo.denormalization;

import org.assertj.core.util.Strings;
import org.inra.ecoinfo.denormalization.jpa.IDenormalizationDAO;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * @author ptcherniati
 */
public class DefaultDenormalizationManager implements IDenormalizationManager {

    private final ExecutorService executor = Executors
            .newSingleThreadScheduledExecutor(new LowThreadFactory());
    private final List<IRootDenormalizedTable> rootDenormalizedTables = new LinkedList();
    private final List<IRootDenormalizedView> rootDenormalizedViews = new LinkedList();
    private IDenormalizationDAO denormalizationDAO;

    /**
     *
     */
    public DefaultDenormalizationManager() {
        super();
    }

    /**
     * @param rootDenormalizedTable
     */
    @Override
    public void addRootDenormalizedTable(IRootDenormalizedTable rootDenormalizedTable) {
        this.rootDenormalizedTables.add(rootDenormalizedTable);
    }

    /**
     * @param rootDenormalizedView
     */
    @Override
    public void addRootDenormalizedView(IRootDenormalizedView rootDenormalizedView) {
        this.rootDenormalizedViews.add(rootDenormalizedView);
    }

    private void execute(Runnable r) {
        this.executor.execute(r);
    }

    private void execute(final String request) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                denormalizationDAO.execute(request);
            }
        };
        this.execute(r);
    }

    /**
     *
     */
    @Override
    public void initTables() {
        for (IRootDenormalizedTable denormalizedTable : this.rootDenormalizedTables) {
            final String request = denormalizedTable.getCreateRequest();
            if (Strings.isNullOrEmpty(request)) {
                continue;
            }
            this.execute(request);
        }
    }

    /**
     *
     */
    @Override
    public void initViews() {

        for (IRootDenormalizedView denormalizedView : this.rootDenormalizedViews) {
            final String request = denormalizedView.initAndGetCreateRequest();
            if (Strings.isNullOrEmpty(request)) {
                continue;
            }
            this.execute(request);
        }
    }

    /**
     *
     */
    @Override
    public void refreshTables() {
        for (IRootDenormalizedTable denormalizedTable : this.rootDenormalizedTables) {
            final String request = denormalizedTable.getRefreshRequest();
            if (Strings.isNullOrEmpty(request)) {
                continue;
            }
            this.execute(request);
        }
    }

    /**
     *
     */
    @Override
    public void refreshViews() {
        for (IRootDenormalizedView denormalizedView : this.rootDenormalizedViews) {
            final String request = denormalizedView.getRefreshRequest();
            if (Strings.isNullOrEmpty(request)) {
                continue;
            }
            this.execute(request);
        }
    }

    /**
     * @param denormalizationDAO
     */
    public void setDenormalizationDAO(IDenormalizationDAO denormalizationDAO) {
        this.denormalizationDAO = denormalizationDAO;
    }
}

class LowThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setPriority(3);
        return t;
    }

}
