/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.CascadeType.*;

/**
 * The Class ValeurMeteo.
 */
@Entity
@Table(name = ValeurMeteo.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        MesureMeteo.ID_JPA, RealNode.ID_JPA}),
        indexes = {
                @Index(columnList = MesureMeteo.ID_JPA, name = "vmm_mm_idx"),
                @Index(columnList = RealNode.ID_JPA, name = "vmm_mm_idx")})
public class ValeurMeteo implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vme_id";
    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "valeur_meteo_vme";
    /**
     * The Constant ATTRIBUTE_JPA_VALUE.
     */
    public static final String ATTRIBUTE_JPA_VALUE = "valeur";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s";
    /**
     * The id <long>.
     */
    @Id
    @Column(name = ValeurMeteo.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The mesure meteo @link(MesureMeteo).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureMeteo.class)
    @JoinColumn(name = MesureMeteo.ID_JPA, referencedColumnName = MesureMeteo.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureMeteo mesureMeteo;

    /**
     * The value @link(Float).
     */
    @Column(name = ValeurMeteo.ATTRIBUTE_JPA_VALUE, nullable = false)
    Float value;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    /**
     * Instantiates a new valeur meteo.
     */
    public ValeurMeteo() {
    }


    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public RealNode getRealNode() {
        return this.realNode;
    }

    /**
     * Sets the variable.
     *
     * @param realNode
     */
    public void setRealNode(final RealNode realNode) {
        this.realNode = realNode;
    }


    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesure meteo.
     *
     * @return the mesure meteo
     */
    public MesureMeteo getMesureMeteo() {
        return this.mesureMeteo;
    }

    /**
     * Sets the mesure meteo.
     *
     * @param mesureMeteo the new mesure meteo
     */
    public void setMesureMeteo(final MesureMeteo mesureMeteo) {
        this.mesureMeteo = mesureMeteo;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }
}
