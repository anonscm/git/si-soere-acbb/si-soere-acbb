/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo;

import org.inra.ecoinfo.acbb.extraction.jsf.ISiteManager;
import org.inra.ecoinfo.acbb.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;

/**
 * The Interface IFluxMeteoDatasetManager.
 */
public interface IFluxMeteoDatasetManager extends ISiteManager, IVariableManager<SiteACBB> {
}
