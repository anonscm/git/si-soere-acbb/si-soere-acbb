/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres.entity;

import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The Class ValeurFluxChambre.
 */
@Entity
@Table(name = ValeurFluxChambre.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        RealNode.ID_JPA, MesureFluxChambre.ID_JPA}),
        indexes = {
                @Index(columnList = MesureFluxChambre.ID_JPA, name = "vmfc_mfc_idx"),
                @Index(columnList = RealNode.ID_JPA, name = "vmfc_mfc_idx")})
public class ValeurFluxChambre implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vfc_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "valeur_flux_chambres_vfc";

    /**
     *
     */
    public static final String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s/%s";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id.
     */
    @Id
    @Column(name = ValeurFluxChambre.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The mesure @link(MesureFluxChambre).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureFluxChambre.ID_JPA, referencedColumnName = MesureFluxChambre.ID_JPA, nullable = false)
    MesureFluxChambre mesure;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;
    /**
     * The valeur @link(Float).
     */
    @Column(nullable = true)
    Float valeur;


    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public RealNode getRealNode() {
        return this.realNode;
    }

    /**
     * Sets the variable.
     *
     * @param realNode
     */
    public void setRealNode(final RealNode realNode) {
        this.realNode = realNode;
    }


    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesure.
     *
     * @return the mesure
     */
    public MesureFluxChambre getMesure() {
        return this.mesure;
    }

    /**
     * Sets the mesure.
     *
     * @param mesure the new mesure
     */
    public void setMesure(final MesureFluxChambre mesure) {
        this.mesure = mesure;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     */
    public Float getValeur() {
        return this.valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     */
    public void setValeur(final Float valeur) {
        this.valeur = valeur;
    }
}
