/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.meteo;

/**
 * The Interface IMeteoDatatypeManager.
 */
public interface IMeteoDatatypeManager {

    /**
     * The Constant CODE_DATATYPE_METEO.
     */
    String CODE_DATATYPE_METEO = "meteorologie_semi-horaire";

    /**
     *
     */
    String CODE_SHORT_DATATYPE_METEO = "meteorologie-sh";
}
