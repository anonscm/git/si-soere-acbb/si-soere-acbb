/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.CascadeType.*;

/**
 * The Class ValeurFluxTours.
 */
@Entity
@Table(name = ValeurFluxTours.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        MesureFluxTours.ID_JPA, RealNode.ID_JPA}),
        indexes = {
                @Index(columnList = MesureFluxTours.ID_JPA, name = "vft_mft_idx"),
                @Index(columnList = RealNode.ID_JPA, name = "vft_mft_idx")})
public class ValeurFluxTours implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "vft_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "valeur_flux_tours_vft";

    /**
     * The Constant ATTRIBUTE_JPA_VALUE.
     */
    public static final String ATTRIBUTE_JPA_VALUE = "valeur";

    /**
     * The Constant ATTRIBUTE_JPA_QUALITY_CLASS.
     */
    public static final String ATTRIBUTE_JPA_QUALITY_CLASS = "flag_qualite";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s/%s";
    /**
     * The id <long>.
     */
    @Id
    @Column(name = ValeurFluxTours.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The mesure flux tours @link(MesureFluxTours).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureFluxTours.class)
    @JoinColumn(name = MesureFluxTours.ID_JPA, referencedColumnName = MesureFluxTours.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureFluxTours mesureFluxTours;

    /**
     * The value @link(Float).
     */
    @Column(name = ValeurFluxTours.ATTRIBUTE_JPA_VALUE, nullable = false)
    Float value;

    /**
     * The quality flag @link(int).
     */
    @Column(name = ValeurFluxTours.ATTRIBUTE_JPA_QUALITY_CLASS, nullable = true)
    Integer qualityFlag;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    /**
     * Instantiates a new valeur flux tours.
     */
    public ValeurFluxTours() {
        super();
    }


    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public RealNode getRealNode() {
        return this.realNode;
    }

    /**
     * Sets the variable.
     *
     * @param realNode
     */
    public void setRealNode(final RealNode realNode) {
        this.realNode = realNode;
    }


    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesure flux tours.
     *
     * @return the mesure flux tours
     */
    public MesureFluxTours getMesureFluxTours() {
        return this.mesureFluxTours;
    }

    /**
     * Sets the mesure flux tours.
     *
     * @param mesureFluxTours the new mesure flux tours
     */
    public void setMesureFluxTours(final MesureFluxTours mesureFluxTours) {
        this.mesureFluxTours = mesureFluxTours;
    }

    /**
     * Gets the quality flag.
     *
     * @return the quality flag
     */
    public Integer getQualityFlag() {
        return this.qualityFlag;
    }

    /**
     * Sets the quality flag.
     *
     * @param qualityFlag the new quality flag
     */
    public void setQualityFlag(final Integer qualityFlag) {
        this.qualityFlag = qualityFlag;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }
}
