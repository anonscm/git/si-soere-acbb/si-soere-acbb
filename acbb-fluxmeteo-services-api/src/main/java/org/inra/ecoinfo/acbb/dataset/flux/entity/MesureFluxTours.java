/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class MesureFluxTours.
 */
@Entity
@Table(name = MesureFluxTours.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceFluxTours.ID_JPA, MesureFluxTours.ATTRIBUTE_JPA_TIME}),
        indexes = {
                @Index(columnList = MesureFluxTours.ATTRIBUTE_JPA_TIME + "," + SequenceFluxTours.ID_JPA, name = "mft_sft_time_idx"),
                @Index(columnList = MesureFluxTours.ATTRIBUTE_JPA_TIME, name = "mft_time_idx")})
public class MesureFluxTours implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mft_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "mesure_flux_tours_mft";

    /**
     * The Constant ATTRIBUTE_JPA_TIME.
     */
    public static final String ATTRIBUTE_JPA_TIME = "heure";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    static final String RESOURCE_PATH = "%s/%s/%s/%s/%s";

    /**
     * The id <long>.
     */
    @Id
    @Column(name = MesureFluxTours.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The sequence flux tours @link(SequenceFluxTours).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SequenceFluxTours.ID_JPA, referencedColumnName = SequenceFluxTours.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceFluxTours sequenceFluxTours;

    /**
     * The time @link(Date).
     */
    @Column(name = MesureFluxTours.ATTRIBUTE_JPA_TIME, nullable = false)
    LocalTime heure;

    /**
     * The line indata file <long>.
     */
    @Column(name = "ligne_fichier_echange")
    Long ligneFichierEchange;

    /**
     * The values flux tours @link(List<ValeurFluxTours>).
     */
    @OneToMany(mappedBy = "mesureFluxTours", cascade = ALL)
    List<ValeurFluxTours> valeursFluxTours = new LinkedList();

    /**
     * Instantiates a new mesure flux tours.
     */
    public MesureFluxTours() {
        super();
    }

    /**
     * Instantiates a new mesure flux tours.
     *
     * @param sequenceFluxTours
     * @param time
     * @param ligneFichierEchange
     * @link(SequenceFluxTours)
     * @link(Date) the heure
     * @link(Long) the ligne fichier echange {@link SequenceFluxTours} the sequence flux tours
     * {@link Date} the time {@link Long} the ligne fichier echange
     */
    public MesureFluxTours(final SequenceFluxTours sequenceFluxTours, final LocalTime time,
                           final Long ligneFichierEchange) {
        super();
        this.sequenceFluxTours = sequenceFluxTours;
        this.heure = time;
        if (!sequenceFluxTours.getMesuresFluxTours().contains(this)) {
            sequenceFluxTours.getMesuresFluxTours().add(this);
        }
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     * Gets the time.
     *
     * @return the heure
     */
    public LocalTime getHeure() {
        return this.heure;
    }

    /**
     * Sets the time.
     *
     * @param heure the new heure
     */
    public void setHeure(final LocalTime heure) {
        this.heure = heure;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the line in data file.
     *
     * @return the ligne fichier echange
     */
    public Long getLigneFichierEchange() {
        return this.ligneFichierEchange;
    }

    /**
     * Sets the line in data file.
     *
     * @param ligneFichierEchange the new line indata file <long> {@link Long}
     *                            the new ligne fichier echange
     */
    public void setLigneFichierEchange(final Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     * Gets the sequence flux tours.
     *
     * @return the sequence flux tours
     */
    public SequenceFluxTours getSequenceFluxTours() {
        return this.sequenceFluxTours;
    }

    /**
     * Sets the sequence flux tours.
     *
     * @param sequenceFluxTours the new sequence flux tours
     * @link(SequenceFluxTours) {@link SequenceFluxTours} the new sequence flux tours
     */
    public void setSequenceFluxTours(final SequenceFluxTours sequenceFluxTours) {
        this.sequenceFluxTours = sequenceFluxTours;
    }

    /**
     * Gets the valeurs flux tours.
     *
     * @return the valeurs flux tours
     */
    public List<ValeurFluxTours> getValeursFluxTours() {
        return this.valeursFluxTours;
    }

    /**
     * Sets the valeurs flux tours.
     *
     * @param valeursFluxTours the new values flux tours
     * @link(List<ValeurFluxTours>) {@link List} the new valeurs flux tours
     */
    public void setValeursFluxTours(final List<ValeurFluxTours> valeursFluxTours) {
        this.valeursFluxTours = valeursFluxTours;
    }
}
