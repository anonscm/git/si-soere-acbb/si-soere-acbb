package org.inra.ecoinfo.acbb.dataset.meteo.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.acbb.utils.ACBBUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SequenceMeteo.
 */
@Entity
@Table(name = SequenceMeteo.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(columnNames = {
        VersionFile.ID_JPA, SiteACBB.ID_JPA, SequenceMeteo.ATTRIBUTE_JPA_DATE})},
        indexes = {
                @Index(columnList = VersionFile.ID_JPA + "," + SiteACBB.ID_JPA + "," + SequenceMeteo.ATTRIBUTE_JPA_DATE, name = "sm_version_site_date_idx"),
                @Index(columnList = SequenceMeteo.ATTRIBUTE_JPA_DATE, name = "sm_date_Index")})
public class SequenceMeteo implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sme_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sequence_meteo_sme";

    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date_mesure";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = SequenceMeteo.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The version @link(VersionFile).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    VersionFile version;

    /**
     * The site @link(SiteACBB).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SiteACBB.ID_JPA, referencedColumnName = SiteACBB.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SiteACBB site;

    /**
     * The date_mesure @link(Date).
     */
    @Column(name = SequenceMeteo.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate dateMesure;

    /**
     * The mesures meteo @link(List<MesureMeteo>).
     */
    @OneToMany(mappedBy = "sequenceMeteo", cascade = ALL)
    @OrderBy(MesureMeteo.ATTRIBUTE_JPA_TIME)
    List<MesureMeteo> mesuresMeteo = new LinkedList();

    /**
     * Instantiates a new sequence meteo.
     */
    public SequenceMeteo() {
        super();
    }

    /**
     * Instantiates a new sequence meteo.
     *
     * @param version    the version
     * @param dateMesure the date_mesure
     */
    public SequenceMeteo(final VersionFile version, final LocalDate dateMesure) {
        super();
        this.version = version;
        this.setSite(ACBBUtils.getSiteFromDataset(version.getDataset()));
        this.setDateMesure(dateMesure);
    }

    /**
     * Gets the date_mesure.
     *
     * @return the date_mesure
     */
    public LocalDate getDateMesure() {
        return this.dateMesure;
    }

    /**
     * Sets the date_mesure.
     *
     * @param dateMesure the new date_mesure
     */
    public void setDateMesure(final LocalDate dateMesure) {
        this.dateMesure = dateMesure;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesures meteo.
     *
     * @return the mesures meteo
     */
    public List<MesureMeteo> getMesuresMeteo() {
        return this.mesuresMeteo;
    }

    /**
     * Sets the mesures meteo.
     *
     * @param mesuresMeteo the new mesures meteo
     */
    public void setMesuresMeteo(final List<MesureMeteo> mesuresMeteo) {
        this.mesuresMeteo = mesuresMeteo;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public SiteACBB getSite() {
        return this.site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final SiteACBB site) {
        this.site = site;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public VersionFile getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final VersionFile version) {
        this.version = version;
        this.site = (ACBBUtils.getSiteFromDataset(version.getDataset()));
    }
}
