/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux;

/**
 * The Interface IFluxToursDatatypeManager.
 */
public interface IFluxToursDatatypeManager {

    /**
     * The Constant CODE_DATATYPE_FLUX_TOURS.
     */
    String CODE_DATATYPE_FLUX_TOURS = "flux_tours";
}
