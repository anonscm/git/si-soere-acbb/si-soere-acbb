/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.refdata.parcelle.Parcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * The Class SequenceFluxTours.
 */
@Entity
@Table(name = SequenceFluxTours.TABLE_NAME, uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                VersionFile.ID_JPA, Parcelle.ID_JPA, SequenceFluxTours.ATTRIBUTE_JPA_DATE})},
        indexes = {
                @Index(columnList = VersionFile.ID_JPA + "," + Parcelle.ID_JPA + "," + SequenceFluxTours.ATTRIBUTE_JPA_DATE, name = "sft_version_parcelle_date_idx"),
                @Index(columnList = SequenceFluxTours.ATTRIBUTE_JPA_DATE, name = "sft_date_Index")})
public class SequenceFluxTours implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "sft_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "sequence_flux_tours_sft";

    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date_mesure";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = SequenceFluxTours.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The version @link(VersionFile).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    VersionFile version;

    /**
     * The date_mesure @link(Date).
     */
    @Column(name = SequenceFluxTours.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate dateMesure;

    /**
     * The mesures flux tours @link(List<MesureFluxTours>).
     */
    @OneToMany(mappedBy = "sequenceFluxTours", cascade = ALL)
    @OrderBy(MesureFluxTours.ATTRIBUTE_JPA_TIME)
    List<MesureFluxTours> mesuresFluxTours = new LinkedList();

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = Parcelle.ID_JPA, referencedColumnName = Parcelle.ID_JPA, nullable = false)
    Parcelle parcelle;

    /**
     * Instantiates a new sequence flux tours.
     */
    public SequenceFluxTours() {
        super();
    }

    /**
     * @param version
     * @param dateMesure
     * @param parcelle
     */
    public SequenceFluxTours(final VersionFile version, final LocalDate dateMesure,
                             final Parcelle parcelle) {
        super();
        this.version = version;
        this.setDateMesure(dateMesure);
        this.parcelle = parcelle;
    }

    /**
     * Gets the date_mesure.
     *
     * @return the date_mesure
     */
    public LocalDate getDateMesure() {
        return this.dateMesure;
    }

    /**
     * Sets the date_mesure.
     *
     * @param dateMesure the new date_mesure
     */
    public void setDateMesure(final LocalDate dateMesure) {
        this.dateMesure = dateMesure;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the mesures flux tours.
     *
     * @return the mesures flux tours
     */
    public List<MesureFluxTours> getMesuresFluxTours() {
        return this.mesuresFluxTours;
    }

    /**
     * Sets the mesures flux tours.
     *
     * @param mesuresFluxTours the new mesures flux tours
     */
    public void setMesuresFluxTours(final List<MesureFluxTours> mesuresFluxTours) {
        this.mesuresFluxTours = mesuresFluxTours;
    }

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    public Parcelle getParcelle() {
        return this.parcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param parcelle
     */
    public void setParcelle(final Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public VersionFile getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final VersionFile version) {
        this.version = version;
    }
}
