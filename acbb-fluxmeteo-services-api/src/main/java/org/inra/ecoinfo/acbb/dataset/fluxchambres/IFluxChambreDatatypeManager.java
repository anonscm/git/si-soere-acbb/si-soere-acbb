/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres;

/**
 * The Interface IFluxChambreDatatypeManager.
 */
public interface IFluxChambreDatatypeManager {

    /**
     * The Constant CODE_DATATYPE_FLUX_CHAMBRES.
     */
    String CODE_DATATYPE_FLUX_CHAMBRES = "flux_chambres";
}
