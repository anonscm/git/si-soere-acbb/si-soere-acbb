/*
 *
 */
package org.inra.ecoinfo.acbb.synthesis.meteorologie;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * The Class SynthesisDatatype.
 */
@Entity(name = "MeteorologieSynthesisDatatype")
@Table(indexes = {@Index(name = "MeteorologieSynthesisDatatype_site_idx", columnList = "site")})
public class SynthesisDatatype extends GenericSynthesisDatatype {

    /**
     *
     */
    public SynthesisDatatype() {
        super();
    }

    /**
     * @param minDate
     * @param maxDate
     * @param site
     * @param idNode
     */
    public SynthesisDatatype(String site, LocalDateTime minDate, LocalDateTime maxDate, String idNodes) {
        super(minDate, maxDate, site, idNodes);
    }
}
