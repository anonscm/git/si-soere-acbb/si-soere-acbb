/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo;

import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IParameter;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * The Interface IFluxMeteoParameter.
 */
public interface IFluxMeteoParameter extends IParameter {

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    int getAffichage();

    /**
     * Gets the files map.
     *
     * @return the files map
     */
    Map<String, File> getFilesMap();

    /**
     * Gets the rythme.
     *
     * @return the rythme
     */
    String getRythme();

    /**
     * Gets the selected sites.
     *
     * @return the selected sites
     * @see org.inra.ecoinfo.acbb.extraction.fluxchambre.IFluxChambreParameter# getSelectedSites()
     */
    List<SiteACBB> getSelectedSites();

    /**
     * Sets the selected sites.
     *
     * @param selectedSites the new selected sites
     */
    void setSelectedSites(List<SiteACBB> selectedSites);

    /**
     * Sets the commentaires.
     *
     * @param commentaires the new commentaires
     */
    void setCommentaires(String commentaires);
}
