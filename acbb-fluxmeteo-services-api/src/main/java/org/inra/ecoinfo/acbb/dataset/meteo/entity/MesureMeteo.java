package org.inra.ecoinfo.acbb.dataset.meteo.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.*;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * The Class MesureMeteo.
 */
@Entity
@Table(name = MesureMeteo.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceMeteo.ID_JPA, MesureMeteo.ATTRIBUTE_JPA_TIME}),
        indexes = {
                @Index(columnList = MesureMeteo.ATTRIBUTE_JPA_TIME + "," + SequenceMeteo.ID_JPA, name = "mm_sm_time_idx"),
                @Index(columnList = MesureMeteo.ATTRIBUTE_JPA_TIME, name = "mm_time_idx")})
public class MesureMeteo implements Serializable {

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mme_id";

    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "mesure_meteo_mme";

    /**
     * The Constant ATTRIBUTE_JPA_TIME.
     */
    public static final String ATTRIBUTE_JPA_TIME = "heure";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    static final String RESOURCE_PATH = "%s/%s/%s/%s";

    /**
     * The id <long>.
     */
    @Id
    @Column(name = MesureMeteo.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The sequence meteo @link(SequenceMeteo).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = SequenceMeteo.ID_JPA, referencedColumnName = SequenceMeteo.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceMeteo sequenceMeteo;

    /**
     * The heure @link(Date).
     */
    @Column(name = MesureMeteo.ATTRIBUTE_JPA_TIME, nullable = false)
    LocalTime heure;

    /**
     * The ligne fichier echange <long>.
     */
    @Column(name = "ligne_fichier_echange")
    Long ligneFichierEchange;

    /**
     * The valeurs meteo @link(List<ValeurMeteo>).
     */
    @OneToMany(mappedBy = "mesureMeteo", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    List<ValeurMeteo> valeursMeteo = new LinkedList();
    Integer originalLine;

    /**
     *
     */
    public MesureMeteo() {
        super();
    }

    /**
     * Instantiates a new mesure meteo.
     *
     * @param sequenceMeteo       the sequence meteo
     * @param time
     * @param ligneFichierEchange the ligne fichier echange
     * @throws DateTimeException
     */
    public MesureMeteo(final SequenceMeteo sequenceMeteo, final LocalTime time,
                       final Long ligneFichierEchange) throws DateTimeException {
        super();
        this.sequenceMeteo = sequenceMeteo;
        if (!sequenceMeteo.getMesuresMeteo().contains(this)) {
            sequenceMeteo.getMesuresMeteo().add(this);
        }
        this.heure = time;
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     * Gets the heure.
     *
     * @return the heure
     */
    public LocalTime getHeure() {
        return this.heure;
    }

    /**
     * Sets the heure.
     *
     * @param heure the new heure
     */
    public void setHeure(final LocalTime heure) {
        this.heure = heure;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the ligne fichier echange.
     *
     * @return the ligne fichier echange
     */
    public Long getLigneFichierEchange() {
        return this.ligneFichierEchange;
    }

    /**
     * Sets the ligne fichier echange.
     *
     * @param ligneFichierEchange the new ligne fichier echange
     */
    public void setLigneFichierEchange(final Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     * @return
     */
    public Integer getOriginalLine() {
        return this.originalLine;
    }

    /**
     * @param originalLine
     */
    public void setOriginalLine(Integer originalLine) {
        this.originalLine = originalLine;
    }

    /**
     * Gets the sequence meteo.
     *
     * @return the sequence meteo
     */
    public SequenceMeteo getSequenceMeteo() {
        return this.sequenceMeteo;
    }

    /**
     * Sets the sequence meteo.
     *
     * @param sequenceMeteo the new sequence meteo
     */
    public void setSequenceMeteo(final SequenceMeteo sequenceMeteo) {
        this.sequenceMeteo = sequenceMeteo;
    }

    /**
     * Gets the valeurs meteo.
     *
     * @return the valeurs meteo
     */
    public List<ValeurMeteo> getValeursMeteo() {
        return this.valeursMeteo;
    }

    /**
     * Sets the valeurs meteo.
     *
     * @param valeursMeteo the new valeurs meteo
     */
    public void setValeursMeteo(final List<ValeurMeteo> valeursMeteo) {
        this.valeursMeteo = valeursMeteo;
    }
}
