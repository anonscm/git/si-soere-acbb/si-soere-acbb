/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.fluxmeteo;

import org.inra.ecoinfo.acbb.dataset.fluxchambres.entity.ValeurFluxChambre;
import org.inra.ecoinfo.acbb.refdata.site.SiteACBB;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.refdata.variable.Variable;

import java.util.List;

/**
 * The Interface IFluxChambreParameter.
 */
public interface IFluxChambreParameter extends IParameter {

    /**
     * Gets the selected flux chambre variables.
     *
     * @return the selected flux chambre variables
     */
    List<Variable> getSelectedFluxChambreVariables();

    /**
     * Sets the selected flux chambre variables.
     *
     * @param selectedVariables the new selected flux chambre variables
     */
    void setSelectedFluxChambreVariables(List<Variable> selectedVariables);

    // test SVN

    /**
     * Gets the selected sites.
     *
     * @return the selected sites
     */
    List<SiteACBB> getSelectedSites();

    /**
     * Gets the valeur mesure flux chambres.
     *
     * @return the valeur mesure flux chambres
     */
    List<ValeurFluxChambre> getValeurFluxChambres();

    /**
     * Sets the valeur flux chambres.
     *
     * @param valeurFluxChambres the new valeur flux chambres
     */
    void setValeurFluxChambres(List<ValeurFluxChambre> valeurFluxChambres);
}
