/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.flux;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;

import java.time.DateTimeException;

/**
 * The Interface IRequestPropertiesFluxTours.
 */
public interface IRequestPropertiesFluxTours extends IRequestPropertiesACBB {

    /**
     * Date to string.
     *
     * @param date the date
     * @param time the time
     * @return the string
     * @throws DateTimeException the parse exception
     */
    String dateToString(String date, String time) throws DateTimeException;

}
