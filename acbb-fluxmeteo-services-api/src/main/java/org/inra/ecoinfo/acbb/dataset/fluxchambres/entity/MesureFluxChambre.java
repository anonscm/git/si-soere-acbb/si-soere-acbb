/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.*;
import static javax.persistence.CascadeType.ALL;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * The Class MesureFluxChambre.
 */
@Entity
@Table(name = MesureFluxChambre.NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
        SuiviParcelle.ID_JPA, MesureFluxChambre.ATTRIBUTE_JPA_DATE}),
        indexes = {
                @Index(columnList = VersionFile.ID_JPA + "," + SuiviParcelle.ID_JPA + "," + MesureFluxChambre.ATTRIBUTE_JPA_DATE, name = "mfct_version_parcelle_date_idx"),
                @Index(columnList = MesureFluxChambre.ATTRIBUTE_JPA_DATE, name = "mfc_date_Index")})
public class MesureFluxChambre implements Serializable {

    /**
     * The Constant NAME_TABLE.
     */
    public static final String NAME_TABLE = "mesure_flux_chambres_mfc";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "mfc_id";

    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    static final String RESOURCE_PATH = "%s/%s/%s/%s/%s";

    /**
     * The id.
     */
    @Id
    @Column(name = MesureFluxChambre.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The date @link(Date).
     */
    @Column(name = MesureFluxChambre.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate date;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = SuiviParcelle.ID_JPA, referencedColumnName = SuiviParcelle.ID_JPA, nullable = false)
    SuiviParcelle suiviParcelle;

    /**
     * The version file @link(VersionFile).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    VersionFile versionFile;

    /**
     * The ligne fichier echange <long>.
     */
    @Column(name = "ligne_fichier_echange")
    Long ligneFichierEchange;

    /**
     * The nbr chambre @link(int).
     */
    @Column
    int nbrChambre;

    /**
     * The valeurs @link(List<ValeurFluxChambre>).
     */
    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    List<ValeurFluxChambre> valeurs = new LinkedList();

    /**
     * Instantiates a new mesure flux chambre.
     */
    public MesureFluxChambre() {
        super();
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDate date) {
        this.date = date;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the ligne fichier echange.
     *
     * @return the ligne fichier echange
     */
    public Long getLigneFichierEchange() {
        return this.ligneFichierEchange;
    }

    /**
     * Sets the ligne fichier echange.
     *
     * @param ligneFichierEchange the new ligne fichier echange
     */
    public void setLigneFichierEchange(final Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     * Gets the nbr chambre.
     *
     * @return the nbr chambre
     */
    public int getNbrChambre() {
        return this.nbrChambre;
    }

    /**
     * Sets the nbr chambre.
     *
     * @param nbrChambre the new nbr chambre
     */
    public void setNbrChambre(final int nbrChambre) {
        this.nbrChambre = nbrChambre;
    }

    /**
     * Gets the suivi parcelle.
     *
     * @return the suivi parcelle
     */
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * Sets the suivi parcelle.
     *
     * @param suiviParcelle the new suivi parcelle
     */
    public void setSuiviParcelle(final SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * Gets the valeurs.
     *
     * @return the valeurs
     */
    public List<ValeurFluxChambre> getValeurs() {
        return this.valeurs;
    }

    /**
     * Sets the valeurs.
     *
     * @param valeurs the new valeurs
     */
    public void setValeurs(final List<ValeurFluxChambre> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     * Gets the version file.
     *
     * @return the version file
     */
    public VersionFile getVersionFile() {
        return this.versionFile;
    }

    /**
     * Sets the version file.
     *
     * @param versionFile the new version file
     */
    public void setVersionFile(final VersionFile versionFile) {
        this.versionFile = versionFile;
    }
}
