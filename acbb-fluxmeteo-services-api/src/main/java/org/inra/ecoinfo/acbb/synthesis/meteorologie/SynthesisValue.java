package org.inra.ecoinfo.acbb.synthesis.meteorologie;

import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * The Class SynthesisValue.
 *
 * @author "Antoine Schellenberger"
 */
@Entity(name = "MeteorologieSynthesisValue")
@Table(indexes = {
        @Index(name = "MeteorologieSynthesisValue_site_idx", columnList = "site")
        ,
        @Index(name = "MeteorologieSynthesisValue_site_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends SynthesisValueWithParcelle {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    public SynthesisValue() {
    }

    public SynthesisValue(final LocalDate date, final String sitePath, final String variable,
                          final Double valueFloat, long idNode) {
        super(null, date == null ? null : date.atStartOfDay(), sitePath, variable, valueFloat == null ? null : valueFloat.floatValue(), null, idNode, false);
    }
}
