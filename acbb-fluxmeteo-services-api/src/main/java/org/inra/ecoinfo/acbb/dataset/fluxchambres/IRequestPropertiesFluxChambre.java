/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.fluxchambres;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;

/**
 * The Interface IRequestPropertiesFluxChambre.
 */
public interface IRequestPropertiesFluxChambre extends IRequestPropertiesACBB {

}
