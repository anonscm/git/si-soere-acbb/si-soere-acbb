package org.inra.ecoinfo.acbb.extraction.soils;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author vkoyao
 */
public interface ISoilExtractor {

    /**
     * @param parameters
     * @throws BusinessException
     */
    void extract(IParameter parameters) throws BusinessException;

    long getExtractionSize(IParameter parameters);

    /**
     * @return
     */
    String getExtractCode();

}
