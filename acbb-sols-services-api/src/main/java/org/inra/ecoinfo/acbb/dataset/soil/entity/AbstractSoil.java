/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.entity;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import static javax.persistence.CascadeType.*;

/**
 * @author ptcherniati
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorFormula(value = AbstractSoil.ATTRIBUTE_JPA_TYPE)
@DiscriminatorValue(AbstractSoil.OTHER)
public abstract class AbstractSoil implements Serializable, Comparable<AbstractSoil> {

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "stpc_id";

    /**
     * The Constant OTHER.
     */
    public static final String OTHER = "OTHER";

    /**
     * The Constant ATTRIBUTE_JPA_ANNEE.
     */
    public static final String ATTRIBUTE_JPA_ANNEE = "annee";

    /**
     * The Constant ATTRIBUTE_JPA_ANNEE.
     */
    public static final String ATTRIBUTE_JPA_ROTATION = "rotation";
    /**
     * The Constant ATTRIBUTE_JPA_DATE.
     */
    public static final String ATTRIBUTE_JPA_DATE = "date";

    /**
     * The Constant ATTRIBUTE_JPA_TYPE.
     */
    public static final String ATTRIBUTE_JPA_TYPE = "type";

    /**
     * The Constant ATTRIBUTE_JPA_OBSERVATION.
     */
    public static final String ATTRIBUTE_JPA_OBSERVATION = "observation";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_CAMPAIGN = "campaign";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LAYER = "layer";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LAYER_SUP = "layer_sup";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LAYER_INF = "layer_inf";

    /**
     * The Constant BIOMASS_PRODUCTION.
     */
    static public final String ATTRIBUTE_JPA_METHODE_SAMPLE = "methode_sample";

    /**
     * The Constant NEW_LINE.
     */
    protected static final String NEW_LINE = "%n";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The type.
     */
    @Column(name = AbstractSoil.ATTRIBUTE_JPA_TYPE, nullable = false)
    protected String type;

    /**
     * The observation.
     */
    @Column(name = AbstractSoil.ATTRIBUTE_JPA_OBSERVATION, nullable = true, columnDefinition = "TEXT")
    protected String observation = StringUtils.EMPTY;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = AbstractSoil.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    Long id;
    /**
     * The version @link(VersionFile).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(value = LazyToOneOption.PROXY)
    VersionFile version;

    /**
     * The date @link(Date).
     */
    @Column(name = AbstractSoil.ATTRIBUTE_JPA_DATE, nullable = false)
    LocalDate date;

    /**
     * The version traitement realisee number @link(Integer).
     */
    @Column(name = ATTRIBUTE_JPA_ROTATION)
    Integer versionTraitementRealiseeNumber;

    /**
     * The annee realisee number @link(Integer).
     */
    @Column(name = ATTRIBUTE_JPA_ANNEE)
    Integer anneeRealiseeNumber;

    /**
     * The suivi parcelle @link(SuiviParcelle).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SuiviParcelle.ID_JPA, referencedColumnName = SuiviParcelle.ID_JPA, nullable = false)
    SuiviParcelle suiviParcelle;

    @Column(name = ATTRIBUTE_JPA_CAMPAIGN)
    String campaignOrSet = StringUtils.EMPTY;
    @Column(name = ATTRIBUTE_JPA_LAYER)
    String layer = StringUtils.EMPTY;
    @Column(name = ATTRIBUTE_JPA_LAYER_INF)
    float layerInf;
    @Column(name = ATTRIBUTE_JPA_LAYER_SUP)
    float layerSup;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ATTRIBUTE_JPA_METHODE_SAMPLE, referencedColumnName = AbstractMethode.ID_JPA, nullable = false)
    AbstractMethode methodesample;

    /**
     * @param version
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param suiviParcelle
     * @param campaignOrSet
     * @param layerInf
     * @param layer
     * @param layerSup
     * @param methodeSample
     */
    public AbstractSoil(VersionFile version, LocalDate date, Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber, SuiviParcelle suiviParcelle, String campaignOrSet, String layer, float layerInf, float layerSup, AbstractMethode methodeSample) {
        this.version = version;
        this.date = date;
        this.versionTraitementRealiseeNumber = versionTraitementRealiseeNumber;
        this.anneeRealiseeNumber = anneeRealiseeNumber;
        this.campaignOrSet = campaignOrSet;
        this.suiviParcelle = suiviParcelle;
        this.layer = layer;
        this.layerInf = layerInf;
        this.layerSup = layerSup;
        this.methodesample = methodeSample;
    }

    /**
     * Instantiates a new intervention.
     */
    public AbstractSoil() {
        super();
    }

    /**
     * @return
     */
    public String getCampaignOrSet() {
        return campaignOrSet;
    }

    /**
     * @param campaignOrSet
     */
    public void setCampaignOrSet(String campaignOrSet) {
        this.campaignOrSet = campaignOrSet;
    }

    /**
     * @return
     */
    public String getLayer() {
        return layer;
    }

    /**
     * @param layer
     */
    public void setLayer(String layer) {
        this.layer = layer;
    }

    /**
     * @return
     */
    public float getLayerInf() {
        return layerInf;
    }

    /**
     * @param layerInf
     */
    public void setLayerInf(float layerInf) {
        this.layerInf = layerInf;
    }

    /**
     * @return
     */
    public float getLayerSup() {
        return layerSup;
    }

    /**
     * @param layerSup
     */
    public void setLayerSup(float layerSup) {
        this.layerSup = layerSup;
    }

    /**
     * @return
     */
    public Integer getAnneeRealiseeNumber() {
        return this.anneeRealiseeNumber;
    }

    /**
     * @param anneeRealiseeNumber
     */
    public void setAnneeRealiseeNumber(Integer anneeRealiseeNumber) {
        this.anneeRealiseeNumber = anneeRealiseeNumber;
    }

    /**
     * @return
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     */
    public String getObservation() {
        return this.observation;
    }

    /**
     * @param observation
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * @return
     */
    public SuiviParcelle getSuiviParcelle() {
        return this.suiviParcelle;
    }

    /**
     * @param suiviParcelle
     */
    public void setSuiviParcelle(SuiviParcelle suiviParcelle) {
        this.suiviParcelle = suiviParcelle;
    }

    /**
     * @return
     */
    public String getType() {
        return this.type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return
     */
    public VersionFile getVersion() {
        return this.version;
    }

    /**
     * @param version
     */
    public void setVersion(VersionFile version) {
        this.version = version;
    }

    /**
     * @return
     */
    public Integer getVersionTraitementRealiseeNumber() {
        return this.versionTraitementRealiseeNumber;
    }

    /**
     * @param versionTraitementRealiseeNumber
     */
    public void setVersionTraitementRealiseeNumber(Integer versionTraitementRealiseeNumber) {
        this.versionTraitementRealiseeNumber = versionTraitementRealiseeNumber;
    }

    public AbstractMethode getMethodesample() {
        return methodesample;
    }

    public void setMethodesample(AbstractMethode methodesample) {
        this.methodesample = methodesample;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.getDate());
        hash = 74 * hash + Objects.hashCode(this.getSuiviParcelle());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractSoil other = (AbstractSoil) obj;
        if (!Objects.equals(this.getDate(), other.getDate())) {
            return false;
        }
        return Objects.equals(this.getSuiviParcelle(), other.getSuiviParcelle());
    }
}
