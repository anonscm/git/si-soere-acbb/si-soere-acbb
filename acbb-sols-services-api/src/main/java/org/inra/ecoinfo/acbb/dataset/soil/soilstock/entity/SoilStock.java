package org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity;

import org.inra.ecoinfo.acbb.dataset.soil.IMesureSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = SoilStock.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(
                columnNames = {AbstractSoil.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA,
                        AbstractSoil.ATTRIBUTE_JPA_LAYER_INF, AbstractSoil.ATTRIBUTE_JPA_LAYER_SUP, AbstractSoil.ATTRIBUTE_JPA_METHODE_SAMPLE}),
        indexes = {
                @Index(name = "sst_version_idx", columnList = VersionFile.ID_JPA)
                ,
                @Index(name = "sst_date_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_DATE)
                ,
                @Index(name = "sst_suiviparcelle_idx", columnList = SuiviParcelle.ID_JPA)
                ,
                @Index(name = "sst_layer_sup_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_LAYER_SUP)
                ,
                @Index(name = "sst_layer_inf_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_LAYER_INF)
                ,
                @Index(name = "sst_methode_sample_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_METHODE_SAMPLE)
        })
@PrimaryKeyJoinColumn(name = SoilStock.ID_JPA, referencedColumnName = AbstractSoil.ID_JPA)
public class SoilStock extends AbstractSoil implements IMesureSoil<SoilStock, ValeurSoilStock> {

    /**
     *
     */
    static public final String NAME_ENTITY_JPA = "soil_stock";

    /**
     * The Constant BIOMASS_PRODUCTION.
     */
    static public final String STOCK = "stock";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = SoilStock.class.getPackage() + ".messages";
    /**
     * The valeurs BiomassProduction.
     */
    @OneToMany(mappedBy = ValeurSoilStock.ATTRIBUTE_JPA_STO, cascade = ALL)
    List<ValeurSoilStock> valeursStock = new LinkedList();

    /**
     *
     */
    public SoilStock() {
    }

    /**
     * @param version
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param suiviParcelle
     * @param campaignOrSet
     * @param layer
     * @param layerInf
     * @param layerSup
     * @param methodeSoilObtention
     * @param methodeSample
     */
    public SoilStock(VersionFile version, LocalDate date, Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber, SuiviParcelle suiviParcelle, String campaignOrSet, String layer, float layerInf, float layerSup, AbstractMethode methodeSample) {
        super(version, date, versionTraitementRealiseeNumber, anneeRealiseeNumber, suiviParcelle, campaignOrSet, layer, layerInf, layerSup, methodeSample);
        this.type = STOCK;
    }

    // A VOIR

    /**
     * @return
     */
    @Override
    public SoilStock getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurSoilStock> getValeurs() {
        return valeursStock;
    }

    @Override
    public int compareTo(AbstractSoil o) {
        final int compareToSuiviParcelle = getSuiviParcelle().compareTo(o.getSuiviParcelle());
        if (compareToSuiviParcelle != 0) {
            return compareToSuiviParcelle;
        }
        return getDate().compareTo(o.getDate());
    }

}
