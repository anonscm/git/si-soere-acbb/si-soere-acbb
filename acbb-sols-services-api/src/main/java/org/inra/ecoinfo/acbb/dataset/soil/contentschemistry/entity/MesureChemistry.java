package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity;

import org.inra.ecoinfo.acbb.dataset.soil.IMesureSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = MesureChemistry.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                DatatypeVariableUniteACBB.ID_JPA, AbstractSoil.ID_JPA}),
        indexes = {
                @Index(name = "mstpc_datatypeVariableUnite_idx", columnList = DatatypeVariableUniteACBB.ID_JPA)
                ,
                @Index(name = "mstpc_contentChemistry_idx", columnList = AbstractSoil.ID_JPA)
        })
@SuppressWarnings({"rawtypes", "serial"})
public class MesureChemistry implements Comparable<MesureChemistry>, IMesureSoil<ContentChemistry, ValeurChemistry> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_SPC = "contentChemistry";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "mesure_chemistry_mstpc";

    /**
     *
     */
    public static final String ID_JPA = "mstpc_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id <long>.
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ContentChemistry.ID_JPA, referencedColumnName = AbstractSoil.ID_JPA, nullable = false)
    ContentChemistry contentChemistry;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = DatatypeVariableUniteACBB.ID_JPA, referencedColumnName = DatatypeVariableUniteACBB.ID_JPA, nullable = false)
    DatatypeVariableUniteACBB datatypeVariableUnite;

    /**
     * The valeurs BiomassProduction.
     */
    @OneToMany(mappedBy = ValeurChemistry.ATTRIBUTE_JPA_MPC, cascade = ALL)
    List<ValeurChemistry> valeursChemistry = new LinkedList<>();


    /**
     *
     */
    public MesureChemistry() {
        super();
    }

    /**
     * @param contentChemistry
     * @param datatypeVariableUnite
     */
    public MesureChemistry(ContentChemistry contentChemistry, DatatypeVariableUniteACBB datatypeVariableUnite) {
        this.contentChemistry = contentChemistry;
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

    /**
     * @return
     */
    public List<ValeurChemistry> getValeursChemistry() {
        return valeursChemistry;
    }

    /**
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     */
    public DatatypeVariableUniteACBB getDatatypeVariableUnite() {
        return datatypeVariableUnite;
    }

    /**
     * @param datatypeVariableUnite
     */
    public void setDatatypeVariableUnite(DatatypeVariableUniteACBB datatypeVariableUnite) {
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

    @Override
    public int compareTo(MesureChemistry o) {
        int comparemesure = this.getContentChemistry()
                .compareTo(o.getContentChemistry());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return this.getDatatypeVariableUnite().getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final MesureChemistry other = (MesureChemistry) obj;
        if (!Objects.equals(this.contentChemistry, other.contentChemistry)) {
            return false;
        }
        return Objects.equals(this.datatypeVariableUnite, other.datatypeVariableUnite);
    }

    /**
     * @return the biomassProduction
     */
    public ContentChemistry getContentChemistry() {
        return this.contentChemistry;
    }

    /**
     * @param contentChemistry
     */
    public void setContentChemistry(ContentChemistry contentChemistry) {
        this.contentChemistry = contentChemistry;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getContentChemistry());
        hash = 90 * hash + Objects.hashCode(this.getDatatypeVariableUnite());
        return hash;
    }

    @Override
    public ContentChemistry getSequence() {
        return getContentChemistry();
    }

    @Override
    public List<ValeurChemistry> getValeurs() {
        return getValeursChemistry();
    }
}
