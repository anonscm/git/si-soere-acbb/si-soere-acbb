/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity;

import org.inra.ecoinfo.acbb.dataset.soil.IMesureSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */

@Entity
@Table(name = MesureSoilTexture.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                AbstractMethode.ID_JPA, SoilTexture.ID_JPA}),
        indexes = {
                @Index(name = "mstpc_methode_idx", columnList = AbstractMethode.ID_JPA)
                ,
                @Index(name = "mstpc_texture_idx", columnList = SoilTexture.ID_JPA)
        })
@SuppressWarnings({"rawtypes", "serial"})
public class MesureSoilTexture implements Serializable, Comparable<MesureSoilTexture>, IMesureSoil<SoilTexture, ValeurSoilTexture> {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "mesure_texture_mst";
    public static final String ID_JPA = "mstex_id";
    public static final String ATTRIBUTE_JPA_STEX = "texture";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @Column(name = MesureSoilTexture.ID_JPA)
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SoilTexture.ID_JPA, referencedColumnName = SoilTexture.ID_JPA, nullable = false)
    SoilTexture texture;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = AbstractMethode.ID_JPA, referencedColumnName = AbstractMethode.ID_JPA, nullable = true)
    MethodeSoilTexture methode;

    /**
     * The valeurs BiomassProduction.
     */
    @OneToMany(mappedBy = ValeurSoilTexture.ATTRIBUTE_JPA_MTE, cascade = ALL)
    List<ValeurSoilTexture> valeursTexture = new LinkedList<ValeurSoilTexture>();

    public MesureSoilTexture(SoilTexture texture, MethodeSoilTexture methode) {
        this.texture = texture;
        this.methode = methode;
    }

    public MesureSoilTexture() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SoilTexture getTexture() {
        return texture;
    }

    public void setTexture(SoilTexture texture) {
        this.texture = texture;
    }

    public MethodeSoilTexture getMethode() {
        return methode;
    }

    public void setMethode(MethodeSoilTexture methode) {
        this.methode = methode;
    }

    @Override
    public SoilTexture getSequence() {
        return getTexture();
    }

    @Override
    public List<ValeurSoilTexture> getValeurs() {
        return valeursTexture;
    }

    @Override
    public int compareTo(MesureSoilTexture o) {
        int comparetexture = this.getTexture()
                .compareTo(o.getTexture());
        if (comparetexture != 0) {
            return comparetexture;
        }
        return this.getMethode()
                .compareTo(o.getMethode());
    }

}
