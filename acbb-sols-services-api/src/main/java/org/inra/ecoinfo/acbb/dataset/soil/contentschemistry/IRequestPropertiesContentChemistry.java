/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry;

import org.inra.ecoinfo.acbb.dataset.soil.IRequestPropertiesSoil;

/**
 * The Interface IRequestPropertiesAI.
 */
public interface IRequestPropertiesContentChemistry extends IRequestPropertiesSoil {

}
