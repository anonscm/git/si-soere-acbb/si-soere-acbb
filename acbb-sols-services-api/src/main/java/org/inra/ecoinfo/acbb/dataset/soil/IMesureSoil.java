package org.inra.ecoinfo.acbb.dataset.soil;

import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;

import java.util.List;

/**
 * @param <T>
 * @param <V>
 * @author ptcherniati
 */
public interface IMesureSoil<T extends AbstractSoil, V extends ValeurSoil> {

    /**
     * @return
     */
    T getSequence();

    /**
     * @return
     */
    List<V> getValeurs();
}
