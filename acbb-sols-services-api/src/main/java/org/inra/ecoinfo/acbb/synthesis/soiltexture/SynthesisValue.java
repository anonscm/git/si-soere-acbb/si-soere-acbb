/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.acbb.synthesis.soiltexture;

import org.inra.ecoinfo.acbb.synthesis.SynthesisValueWithParcelle;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * The Class SynthesisValue.
 *
 * @author "Antoine Schellenberger"
 */
@Entity(name = "SoilTextureSynthesisValue")
@Table(indexes = {
        @Index(name = "SoilTextureValue_site_idx", columnList = "site")
        ,
        @Index(name = "SoilTextureValue_site_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends SynthesisValueWithParcelle {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    private String methods;
    public SynthesisValue() {
    }

    public SynthesisValue(final LocalDate date, final String sitePath, final String parcelleCode, final String variable,
                          final String methods, final Double valueFloat, long idNode) {
        super(parcelleCode, date == null ? null : date.atStartOfDay(), sitePath, variable, valueFloat == null ? null : valueFloat.floatValue(), null, idNode, false);
        this.methods = methods;
    }

}
