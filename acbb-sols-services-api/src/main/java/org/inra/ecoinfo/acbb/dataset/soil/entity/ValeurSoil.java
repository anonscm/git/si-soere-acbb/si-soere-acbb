/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil.entity;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.datatypevariableunite.DatatypeVariableUniteACBB;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * The Class ValeurIntervention.
 * <p>
 * generic entity for record values
 *
 * @param <T>
 */
@MappedSuperclass
public abstract class ValeurSoil<T extends ValeurSoil> implements Serializable, Comparable<T> {

    /**
     * The Constant ATTRIBUTE_JPA_VALUE.
     */
    public static final String ATTRIBUTE_JPA_VALUE = "value";

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The id <long>.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    /**
     * The valeur @link(float).
     */
    @Column(name = ValeurSoil.ATTRIBUTE_JPA_VALUE)
    Float valeur;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = AbstractMethode.ID_JPA, referencedColumnName = AbstractMethode.ID_JPA, nullable = true)
    AbstractMethode methode;

    int measureNumber;
    Float standardDeviation;
    int qualityIndex;

    /**
     * Instantiates a new valeur intervention.
     */
    public ValeurSoil() {
        super();
    }

    /**
     * Instantiates a new valeur intervention.
     *
     * @param valeur            the valeur
     * @param realNode          the datatype variable unite
     * @param measureNumber
     * @param qualityIndex
     * @param standardDeviation
     * @param methode
     */
    public ValeurSoil(final Float valeur, final RealNode realNode,
                      int measureNumber, float standardDeviation, int qualityIndex, AbstractMethode methode) {
        super();
        this.valeur = valeur;
        this.realNode = realNode;
        this.measureNumber = measureNumber;
        this.qualityIndex = qualityIndex;
        this.standardDeviation = standardDeviation;
        this.methode = methode;
    }

    /**
     * Instantiates a new valeur intervention.
     *
     * @param valeurs
     * @param realNode
     * @link(List<IValeurQualitative>) the valeurs
     * @link(RealNode) the datatype variable unite
     */
    public ValeurSoil(final List<? extends IValeurQualitative> valeurs,
                      final RealNode realNode) {
        super();
        this.setValeursQualitatives(valeurs);
        this.realNode = realNode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurSoil<?> other = (ValeurSoil<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * Gets the datatype variable unite.
     *
     * @return the datatype variable unite
     */
    public RealNode getRealNode() {
        return this.realNode;
    }

    /**
     * Sets the datatype variable unite.
     *
     * @param realNode the new datatype variable unite
     */
    public void setRealNode(final RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return
     */
    public int getMeasureNumber() {
        return this.measureNumber;
    }

    /**
     * @param measureNumber
     */
    public void setMeasureNumber(int measureNumber) {
        this.measureNumber = measureNumber;
    }

    /**
     * @return
     */
    public int getQualityIndex() {
        return this.qualityIndex;
    }

    /**
     * @param qualityIndex
     */
    public void setQualityIndex(int qualityIndex) {
        this.qualityIndex = qualityIndex;
    }

    /**
     * @return
     */
    public Float getStandardDeviation() {
        return this.standardDeviation;
    }

    /**
     * @param standardDeviation
     */
    public void setStandardDeviation(Float standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     */
    public Float getValeur() {
        return this.valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     */
    public void setValeur(final Float valeur) {
        this.valeur = valeur;
    }

    /**
     * Gets the valeurs qualitatives.
     *
     * @return the valeurs qualitatives must be overriden if neccessary
     */
    public List<? extends IValeurQualitative> getValeursQualitatives() {
        return null;
    }

    /**
     * @param valeursQualitatives the valeursQualitatives to set must be overriden if neccessary
     */
    public void setValeursQualitatives(List<? extends IValeurQualitative> valeursQualitatives) {
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    /**
     * @return
     */
    public AbstractMethode getMethode() {
        return methode;
    }

    /**
     * @param methode
     */
    public void setMethode(AbstractMethode methode) {
        this.methode = methode;
    }

    /**
     * @return
     */
    protected DatatypeVariableUniteACBB getDatatypeVariableUnite() {
        return (DatatypeVariableUniteACBB) this.realNode.getNodeable();
    }

}
