/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolstock;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class Bloc.
 */
@Entity
@Table(name = MethodeSoilStock.NAME_ENTITY_JPA)
public class MethodeSoilStock extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_sol_stock_calcule";

    /**
     * The Constant ID_JPA.
     */
    // LES CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    @Column(columnDefinition = "TEXT")
    String fichierPDF = "";

    /**
     *
     */
    public MethodeSoilStock() {
        super();
    }

    // LES CONSTRUCTEURS

    /**
     * Instantiates a new bloc.
     *
     * @param definition
     * @param libelle
     * @param fichierPDF
     * @param numberId
     */
    // GETTERS ET SETTERS
    public MethodeSoilStock(String definition, String libelle, Long numberId, String fichierPDF) {
        super(libelle, definition, numberId);
        this.fichierPDF = fichierPDF;
    }

    /**
     * @param definition
     * @param fichierPDF
     */
    public void update(String definition, String fichierPDF) {
        setDefinition(definition);
        this.fichierPDF = fichierPDF;
    }

    /**
     * @return
     */
    public String getFichierPDF() {
        return getSanitizedString(fichierPDF);
    }

    /**
     * @param fichierPDF
     */
    public void setFichierPDF(String fichierPDF) {
        this.fichierPDF = fichierPDF;
    }

}
