package org.inra.ecoinfo.acbb.dataset.soil.soilstock.entity;

import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurSoilStock.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                RealNode.ID_JPA, ValeurSoilStock.ID_JPA}),
        indexes = {
                @Index(name = "vsst_stock_idx", columnList = SoilStock.ID_JPA)
                ,
                @Index(name = "vsst_variable_idx", columnList = RealNode.ID_JPA)
                ,
                @Index(name = "vsst_methode_idx", columnList = AbstractMethode.ID_JPA)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurSoilStock.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurSoilStock extends ValeurSoil<ValeurSoilStock> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STO = "stock";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_stock_vsst";

    /**
     *
     */
    public static final String ID_JPA = "vsst_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SoilStock.ID_JPA, referencedColumnName = SoilStock.ID_JPA, nullable = false)
    SoilStock stock;

    /**
     * @param stock
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     * @param methode
     */
    public ValeurSoilStock(SoilStock stock, Float valeur, RealNode realNode, int measureNumber, float standardDeviation, int qualityIndex, AbstractMethode methode) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.stock = stock;
    }

    /**
     * @param stock
     * @param valeurs
     * @param realNode
     */
    public ValeurSoilStock(SoilStock stock, List<? extends IValeurQualitative> valeurs, RealNode realNode) {
        super(valeurs, realNode);
        this.stock = stock;
    }

    /**
     *
     */
    public ValeurSoilStock() {
        super();
    }

    @Override
    public int compareTo(ValeurSoilStock o) {
        int comparemesure = this.getStock()
                .compareTo(o.getStock());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return this.getDatatypeVariableUnite().getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
    }

    /**
     * @return
     */
    public SoilStock getStock() {
        return stock;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurSoil<?> other = (ValeurSoil<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }
}
