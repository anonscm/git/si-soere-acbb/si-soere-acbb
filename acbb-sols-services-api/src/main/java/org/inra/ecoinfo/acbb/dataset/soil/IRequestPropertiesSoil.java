/*
 *
 */
package org.inra.ecoinfo.acbb.dataset.soil;

import org.inra.ecoinfo.acbb.dataset.IRequestPropertiesACBB;
import org.inra.ecoinfo.utils.Column;

import java.util.Map;

/**
 * The Interface IRequestPropertiesSoil.
 */
public interface IRequestPropertiesSoil extends IRequestPropertiesACBB {

    /**
     * Gets the value columns.
     *
     * @return the value columns
     */
    Map<Integer, Column> getValueColumns();

}
