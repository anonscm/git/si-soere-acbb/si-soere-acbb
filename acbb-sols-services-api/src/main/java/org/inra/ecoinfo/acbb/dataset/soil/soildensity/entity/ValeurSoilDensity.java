package org.inra.ecoinfo.acbb.dataset.soil.soildensity.entity;

import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurSoilDensity.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                RealNode.ID_JPA, ValeurSoilDensity.ID_JPA}),
        indexes = {
                @Index(name = "vsde_density_idx", columnList = SoilDensity.ID_JPA)
                ,
                @Index(name = "vsde_variable_idx", columnList = RealNode.ID_JPA)
                ,
                @Index(name = "vsde_methode_idx", columnList = AbstractMethode.ID_JPA)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurSoilDensity.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurSoilDensity extends ValeurSoil<ValeurSoilDensity> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_DEN = "density";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_density_vsde";

    /**
     *
     */
    public static final String ID_JPA = "vsde_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SoilDensity.ID_JPA, referencedColumnName = SoilDensity.ID_JPA, nullable = false)
    SoilDensity density;

    /**
     * @param density
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     * @param methode
     */
    public ValeurSoilDensity(SoilDensity density, Float valeur, RealNode realNode, int measureNumber, float standardDeviation, int qualityIndex, AbstractMethode methode) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.density = density;
    }

    /**
     * @param density
     * @param valeurs
     * @param realNode
     */
    public ValeurSoilDensity(SoilDensity density, List<? extends IValeurQualitative> valeurs, RealNode realNode) {
        super(valeurs, realNode);
        this.density = density;
    }

    /**
     *
     */
    public ValeurSoilDensity() {
        super();
    }

    @Override
    public int compareTo(ValeurSoilDensity o) {
        int comparemesure = this.getDensity()
                .compareTo(o.getDensity());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return this.getDatatypeVariableUnite().getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
    }

    /**
     * @return
     */
    public SoilDensity getDensity() {
        return density;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurSoil<?> other = (ValeurSoil<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }
}
