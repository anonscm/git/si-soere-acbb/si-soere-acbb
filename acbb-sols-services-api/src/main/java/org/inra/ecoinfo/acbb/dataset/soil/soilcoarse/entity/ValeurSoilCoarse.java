package org.inra.ecoinfo.acbb.dataset.soil.soilcoarse.entity;

import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurSoilCoarse.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                RealNode.ID_JPA, ValeurSoilCoarse.ID_JPA}),
        indexes = {
                @Index(name = "vsco_coarse_idx", columnList = SoilCoarse.ID_JPA)
                ,
                @Index(name = "vsco_variable_idx", columnList = RealNode.ID_JPA)
                ,
                @Index(name = "vsco_methode_idx", columnList = AbstractMethode.ID_JPA)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurSoilCoarse.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurSoilCoarse extends ValeurSoil<ValeurSoilCoarse> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_COA = "coarse";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_coarse_vsco";

    /**
     *
     */
    public static final String ID_JPA = "vsco_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SoilCoarse.ID_JPA, referencedColumnName = SoilCoarse.ID_JPA, nullable = false)
    SoilCoarse coarse;

    /**
     * @param coarse
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     * @param methode
     */
    public ValeurSoilCoarse(SoilCoarse coarse, Float valeur, RealNode realNode, int measureNumber, float standardDeviation, int qualityIndex, AbstractMethode methode) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.coarse = coarse;
    }

    /**
     * @param coarse
     * @param valeurs
     * @param realNode
     */
    public ValeurSoilCoarse(SoilCoarse coarse, List<? extends IValeurQualitative> valeurs, RealNode realNode) {
        super(valeurs, realNode);
        this.coarse = coarse;
    }

    /**
     *
     */
    public ValeurSoilCoarse() {
        super();
    }

    @Override
    public int compareTo(ValeurSoilCoarse o) {
        int comparemesure = this.getCoarse()
                .compareTo(o.getCoarse());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return this.getDatatypeVariableUnite().getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
    }

    /**
     * @return
     */
    public SoilCoarse getCoarse() {
        return coarse;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurSoil<?> other = (ValeurSoil<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }
}
