/*
 *
 */
package org.inra.ecoinfo.acbb.extraction.soils;

import org.inra.ecoinfo.acbb.extraction.jsf.ITreatmentManager;
import org.inra.ecoinfo.acbb.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture.MethodeSoilTexture;
import org.inra.ecoinfo.acbb.refdata.traitement.TraitementProgramme;
import org.inra.ecoinfo.acbb.refdata.variable.VariableACBB;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vkoyao
 */

/**
 * The Interface IIKDatasetManager.
 */
public interface ISoilDatasetManager extends ITreatmentManager, IVariableManager<TraitementProgramme> {
    Map<MethodeSoilTexture, Map<VariableACBB, Set<NodeDataSet>>> getAvailableVariablesByTreatmentAndDatesIntervalByMethod(List<TraitementProgramme> linkedList, List<IntervalDate> intervals);

}
