package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity;

import org.inra.ecoinfo.acbb.dataset.soil.IMesureSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = SoilTexture.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(
                columnNames = {AbstractSoil.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA,
                        AbstractSoil.ATTRIBUTE_JPA_LAYER_INF, AbstractSoil.ATTRIBUTE_JPA_LAYER_SUP, AbstractSoil.ATTRIBUTE_JPA_METHODE_SAMPLE}),
        indexes = {
                @Index(name = "stx_version_idx", columnList = VersionFile.ID_JPA)
                ,
                @Index(name = "stx_date_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_DATE)
                ,
                @Index(name = "stx_suiviparcelle_idx", columnList = SuiviParcelle.ID_JPA)
                ,
                @Index(name = "stx_layer_sup_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_LAYER_SUP)
                ,
                @Index(name = "stx_layer_inf_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_LAYER_INF)
                ,
                @Index(name = "stx_methode_sample_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_METHODE_SAMPLE)
        })
@PrimaryKeyJoinColumn(name = SoilTexture.ID_JPA, referencedColumnName = AbstractSoil.ID_JPA)
public class SoilTexture extends AbstractSoil implements IMesureSoil<SoilTexture, ValeurSoilTexture> {

    /**
     *
     */
    static public final String NAME_ENTITY_JPA = "soil_texture";

    /**
     * The Constant BIOMASS_PRODUCTION.
     */
    static public final String TEXTURE = "texture";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = SoilTexture.class.getPackage() + ".messages";
    /**
     * The valeurs BiomassProduction.
     */
    @OneToMany(mappedBy = MesureSoilTexture.ATTRIBUTE_JPA_STEX, cascade = ALL)
    List<MesureSoilTexture> mesureSoilTextures = new LinkedList();

    /**
     *
     */
    public SoilTexture() {
    }

    /**
     * @param version
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param suiviParcelle
     * @param campaignOrSet
     * @param layer
     * @param layerInf
     * @param layerSup
     * @param methodeSoilObtention
     * @param methodeSample
     */
    public SoilTexture(VersionFile version, LocalDate date, Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber, SuiviParcelle suiviParcelle, String campaignOrSet, String layer, float layerInf, float layerSup, AbstractMethode methodeSample) {
        super(version, date, versionTraitementRealiseeNumber, anneeRealiseeNumber, suiviParcelle, campaignOrSet, layer, layerInf, layerSup, methodeSample);
        this.type = TEXTURE;
    }

    // A VOIR

    /**
     * @return
     */
    @Override
    public SoilTexture getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurSoilTexture> getValeurs() {
        return this.getMesures().stream()
                .map(m -> m.getValeurs())
                .flatMap(m -> m.stream())
                .collect(Collectors.toList());
    }

    /**
     * @return
     */
    public List<MesureSoilTexture> getMesures() {
        return mesureSoilTextures;
    }

    @Override
    public int compareTo(AbstractSoil o) {
        final int compareToSuiviParcelle = getSuiviParcelle().compareTo(o.getSuiviParcelle());
        if (compareToSuiviParcelle != 0) {
            return compareToSuiviParcelle;
        }
        return getDate().compareTo(o.getDate());
    }

}
