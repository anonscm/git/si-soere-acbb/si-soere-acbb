package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity;

import org.inra.ecoinfo.acbb.dataset.soil.IMesureSoil;
import org.inra.ecoinfo.acbb.dataset.soil.entity.AbstractSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.suiviparcelle.SuiviParcelle;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static javax.persistence.CascadeType.ALL;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ContentChemistry.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(
                columnNames = {AbstractSoil.ATTRIBUTE_JPA_DATE, SuiviParcelle.ID_JPA,
                        AbstractSoil.ATTRIBUTE_JPA_LAYER_INF, AbstractSoil.ATTRIBUTE_JPA_LAYER_SUP, AbstractSoil.ATTRIBUTE_JPA_METHODE_SAMPLE}),
        indexes = {
                @Index(name = "stpc_version_idx", columnList = VersionFile.ID_JPA)
                ,
                @Index(name = "stpc_date_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_DATE)
                ,
                @Index(name = "stpc_suiviparcelle_idx", columnList = SuiviParcelle.ID_JPA)
                ,
                @Index(name = "stpc_layer_sup_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_LAYER_SUP)
                ,
                @Index(name = "stpc_layer_inf_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_LAYER_INF)
                ,
                @Index(name = "stpc_methode_sample_idx", columnList = AbstractSoil.ATTRIBUTE_JPA_METHODE_SAMPLE)
        })
@PrimaryKeyJoinColumn(name = ContentChemistry.ID_JPA, referencedColumnName = AbstractSoil.ID_JPA)
public class ContentChemistry extends AbstractSoil implements IMesureSoil<ContentChemistry, ValeurChemistry> {

    /**
     *
     */
    static public final String NAME_ENTITY_JPA = "soil_chemistry";

    /**
     * The Constant BIOMASS_PRODUCTION.
     */
    static public final String chemistry_CONTENT = "chemistryContent";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The Constant BUNDLE_NAME.
     */
    static final String BUNDLE_NAME = ContentChemistry.class.getPackage() + ".messages";
    /**
     * The valeurs BiomassProduction.
     */
    @OneToMany(mappedBy = MesureChemistry.ATTRIBUTE_JPA_SPC, cascade = ALL)
    List<MesureChemistry> mesuresChemistry = new LinkedList();

    /**
     *
     */
    public ContentChemistry() {
    }

    /**
     * @param version
     * @param date
     * @param versionTraitementRealiseeNumber
     * @param anneeRealiseeNumber
     * @param suiviParcelle
     * @param campaignOrSet
     * @param layer
     * @param layerInf
     * @param layerSup
     * @param methodesample
     */
    public ContentChemistry(VersionFile version, LocalDate date, Integer versionTraitementRealiseeNumber, Integer anneeRealiseeNumber,
                            SuiviParcelle suiviParcelle, String campaignOrSet, String layer, float layerInf, float layerSup, AbstractMethode methodesample) {
        super(version, date, versionTraitementRealiseeNumber, anneeRealiseeNumber, suiviParcelle, campaignOrSet, layer, layerInf, layerSup, methodesample);
        this.type = chemistry_CONTENT;
    }

    /**
     * @return
     */
    public List<MesureChemistry> getMesures() {
        return mesuresChemistry;
    }

    /**
     * @param mesuresChemistry
     */
    public void setMesures(List<MesureChemistry> mesuresChemistry) {
        this.mesuresChemistry = mesuresChemistry;
    }

    // A VOIR

    /**
     * @return
     */
    @Override
    public ContentChemistry getSequence() {
        return this;
    }

    /**
     * @return
     */
    @Override
    public List<ValeurChemistry> getValeurs() {
        return this.getMesures().stream()
                .map(m -> m.getValeursChemistry())
                .flatMap(m -> m.stream())
                .collect(Collectors.toList());
    }

    @Override
    public int compareTo(AbstractSoil o) {
        final int compareToSuiviParcelle = getSuiviParcelle().compareTo(o.getSuiviParcelle());
        if (compareToSuiviParcelle != 0) {
            return compareToSuiviParcelle;
        }
        return getDate().compareTo(o.getDate());
    }

}
