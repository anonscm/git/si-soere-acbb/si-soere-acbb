/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;

import javax.persistence.*;
import java.util.List;

/**
 * The Class Bloc.
 */
@Entity(name = MethodeSoilTexture.NAME_ENTITY_JPA)
@Table(name = MethodeSoilTexture.NAME_ENTITY_JPA)
public class MethodeSoilTexture extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_sol_texture";

    /**
     *
     */
    public static final String NAME_ENTITY_MET_TEXT_MET_ANAL_COMPOSANT = "met_texture_soil_met_anal_composant";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_EQUATION_QUALITE_PREDICTION = "met_equation_qualite_prediction";

    // CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    String reference_donesol = "";
    String norme = "";
    String reference_las = "";
    String equationQualitePrediction = "";

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = MethodeTextureComposant.class)
    @JoinTable(name = MethodeSoilTexture.NAME_ENTITY_MET_TEXT_MET_ANAL_COMPOSANT, joinColumns = @JoinColumn(name = MethodeSoilTexture.ID_JPA), inverseJoinColumns = @JoinColumn(name = MethodeTextureComposant.ID_JPA, referencedColumnName = ListeSoil.ID_JPA))
    private List<MethodeTextureComposant> composante;

    // LES CONSTRUCTEURS

    /**
     *
     */
    public MethodeSoilTexture() {
        super();
    }

    /**
     * @param libelle
     * @param definition
     * @param equationQualitePrediction
     * @param numberId
     * @param composante
     * @param reference_donesol
     * @param norme
     * @param reference_las
     */
    public MethodeSoilTexture(String libelle, String definition, String equationQualitePrediction,
                              Long numberId, List<MethodeTextureComposant> composante, String reference_donesol, String norme, String reference_las) {
        super(libelle, definition, numberId);
        this.composante = composante;
        this.reference_donesol = reference_donesol;
        this.norme = norme;
        this.reference_las = reference_las;
        this.equationQualitePrediction = equationQualitePrediction;
    }

    /**
     * @return
     */
    public List<MethodeTextureComposant> getComposante() {
        return composante;
    }

    /**
     * @param composants
     */
    public void setComposante(List<MethodeTextureComposant> composants) {
        this.composante = composants;
    }

    /**
     * @param definition
     * @param composante
     * @param reference_las
     * @param reference_donesol
     * @param norme
     */
    public void update(String definition, List<MethodeTextureComposant> composante, String reference_donesol, String norme, String reference_las) {
        setDefinition(definition);
        this.composante = composante;
        this.reference_donesol = reference_donesol;
        this.norme = norme;
        this.reference_las = reference_las;

    }

    /**
     * @return
     */
    public String getReference_donesol() {
        return reference_donesol;
    }

    /**
     * @param reference_donesol
     */
    public void setReference_donesol(String reference_donesol) {
        this.reference_donesol = reference_donesol;
    }

    /**
     * @return
     */
    public String getNorme() {
        return norme;
    }

    /**
     * @param norme
     */
    public void setNorme(String norme) {
        this.norme = norme;
    }

    /**
     * @return
     */
    public String getReference_las() {
        return reference_las;
    }

    /**
     * @param reference_las
     */
    public void setReference_las(String reference_las) {
        this.reference_las = reference_las;
    }

    /**
     * @return
     */
    public String getEquationQualitePrediction() {
        return equationQualitePrediction;
    }

    /**
     * @param equationQualitePrediction
     */
    public void setEquationQualitePrediction(String equationQualitePrediction) {
        this.equationQualitePrediction = equationQualitePrediction;
    }

}
