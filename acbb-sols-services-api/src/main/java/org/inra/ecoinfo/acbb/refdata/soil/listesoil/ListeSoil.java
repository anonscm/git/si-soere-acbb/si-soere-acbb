/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.listesoil;

import org.inra.ecoinfo.acbb.refdata.listesacbb.ListeACBB;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class ListeItineraire.
 */
@Entity
@Table(name = ValeurQualitative.NAME_ENTITY_JPA)
@DiscriminatorValue(ListeSoil.LISTE_SOIL_TYPE)
public class ListeSoil extends ListeACBB {

    /**
     *
     */
    public static final String LISTE_SOIL_TYPE = "liste_soil";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public ListeSoil() {
        super();
    }

    /**
     * Instantiates a new listeItineraire.
     *
     * @param nom
     * @param value
     * @link(String) the nom
     * @link(String) the value
     */
    public ListeSoil(final String nom, final String value) {
        super(nom, value);
        this.typeListe = ListeSoil.LISTE_SOIL_TYPE;
    }
}
