/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodeSample;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class Bloc.
 */
@Entity
@Table(name = Methodesample.NAME_ENTITY_JPA)
public class Methodesample extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_echantillon";

    /**
     *
     */
    public static final String ATRRIBUTE_PRELEVEMENT = "prelevement";

    /**
     *
     */
    public static final String ATRRIBUTE_ASSEMBLAGE = "assemblage";

    /**
     *
     */
    public static final String ATRRIBUTE_PREPARATION = "preparation";

    /**
     * The Constant ID_JPA.
     */
    // LES CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    @Column(name = ATRRIBUTE_PRELEVEMENT, columnDefinition = "TEXT")
    String prelevement = "";
    @Column(name = ATRRIBUTE_ASSEMBLAGE, columnDefinition = "TEXT")
    String assemblage = "";
    @Column(name = ATRRIBUTE_PREPARATION, columnDefinition = "TEXT")
    String preparation = "";

    /**
     *
     */
    public Methodesample() {
        super();
    }

    // LES CONSTRUCTEURS

    /**
     * Instantiates a new bloc.
     *
     * @param definition
     * @param libelle
     * @param prelevement
     * @param numberId
     * @param assemblage
     * @param preparation
     */
    // GETTERS ET SETTERS
    public Methodesample(String definition, String libelle, Long numberId, String prelevement, String assemblage, String preparation) {
        super(libelle, definition, numberId);
        this.assemblage = assemblage;
        this.prelevement = prelevement;
        this.preparation = preparation;

    }

    /**
     * @param definition
     * @param prelevement
     * @param preparation
     * @param assemblage
     */
    public void update(String definition, String prelevement, String assemblage, String preparation) {
        setDefinition(definition);
        this.assemblage = assemblage;
        this.prelevement = prelevement;
    }

    /**
     * @return
     */
    public String getPrelevement() {
        return getSanitizedString(prelevement);
    }

    /**
     * @param prelevement
     */
    public void setPrelevement(String prelevement) {
        this.prelevement = prelevement;
    }

    /**
     * @return
     */
    public String getAssemblage() {
        return getSanitizedString(assemblage);
    }

    /**
     * @param assemblage
     */
    public void setAssemblage(String assemblage) {
        this.assemblage = assemblage;
    }

    /**
     * @return
     */
    public String getPreparation() {
        return getSanitizedString(preparation);
    }

    /**
     * @param preparation
     */
    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

}
