/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoltexture;

import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity(name = MethodeTextureComposant.MET_TEXTURE_COMPOSANTS)
@DiscriminatorValue(MethodeTextureComposant.MET_TEXTURE_COMPOSANTS)
public class MethodeTextureComposant extends ListeSoil {

    /**
     *
     */
    public static final String MET_TEXTURE_COMPOSANTS = "met_texture_soil_element";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "met_texture_soil_element";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
