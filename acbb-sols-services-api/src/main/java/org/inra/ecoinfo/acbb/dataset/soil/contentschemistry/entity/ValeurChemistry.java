package org.inra.ecoinfo.acbb.dataset.soil.contentschemistry.entity;

import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.DateUtil;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurChemistry.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                RealNode.ID_JPA, ValeurChemistry.ID_JPA}),
        indexes = {
                @Index(name = "vstpc_mesureChemistry_idx", columnList = MesureChemistry.ID_JPA)
                ,
                @Index(name = "vstpc_variable_idx", columnList = RealNode.ID_JPA)
                ,
                @Index(name = "vstpc_methode_idx", columnList = AbstractMethode.ID_JPA)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurChemistry.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurChemistry extends ValeurSoil<ValeurChemistry> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_MPC = "mesureChemistry";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_chemistry_vstpc";

    /**
     *
     */
    public static final String ID_JPA = "vstpc_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureChemistry.ID_JPA, referencedColumnName = MesureChemistry.ID_JPA, nullable = false)
    MesureChemistry mesureChemistry;

    /**
     * @param mesureChemistry
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     * @param methode
     */
    public ValeurChemistry(MesureChemistry mesureChemistry, Float valeur, RealNode realNode, int measureNumber, float standardDeviation, int qualityIndex, AbstractMethode methode) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.mesureChemistry = mesureChemistry;
    }

    /**
     * @param mesureChemistry
     * @param valeurs
     * @param realNode
     */
    public ValeurChemistry(MesureChemistry mesureChemistry, List<? extends IValeurQualitative> valeurs, RealNode realNode) {
        super(valeurs, realNode);
        this.mesureChemistry = mesureChemistry;
    }

    /**
     *
     */
    public ValeurChemistry() {
        super();
    }

    @Override
    public int compareTo(ValeurChemistry o) {
        int comparemesure = this.getMesureChemistry()
                .compareTo(o.getMesureChemistry());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return this.getDatatypeVariableUnite().getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurSoil<?> other = (ValeurSoil<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return the biomassProduction
     */
    public MesureChemistry getMesureChemistry() {
        return this.mesureChemistry;
    }

    /**
     * @param mesureChemistry
     */
    public void setMesureChemistry(MesureChemistry mesureChemistry) {
        this.mesureChemistry = mesureChemistry;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(mesureChemistry.getContentChemistry().getVersion().getDataset().getRealNode().getPath()).append(" ");
        builder.append(mesureChemistry.getContentChemistry().getSuiviParcelle().getParcelle().getCode()).append(" ");
        builder.append(mesureChemistry.getContentChemistry().getSuiviParcelle().getTraitement().getCode()).append(" ");
        builder.append(DateUtil.getUTCDateTextFromLocalDateTime(mesureChemistry.getContentChemistry().getDate().atStartOfDay(), DateUtil.DD_MM_YYYY)).append(" ");
        return builder.toString();
    }
}
