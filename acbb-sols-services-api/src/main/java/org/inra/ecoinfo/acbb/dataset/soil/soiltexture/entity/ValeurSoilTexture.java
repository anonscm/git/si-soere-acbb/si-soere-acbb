package org.inra.ecoinfo.acbb.dataset.soil.soiltexture.entity;

import org.inra.ecoinfo.acbb.dataset.soil.entity.ValeurSoil;
import org.inra.ecoinfo.acbb.refdata.AbstractMethode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurSoilTexture.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
                RealNode.ID_JPA, ValeurSoilTexture.ID_JPA, AbstractMethode.ID_JPA}),
        indexes = {
                @Index(name = "vmtx_mesure_texture_idx", columnList = MesureSoilTexture.ID_JPA)
                ,
                @Index(name = "vstx_variable_idx", columnList = RealNode.ID_JPA)
                ,
                @Index(name = "vstx_methode_idx", columnList = AbstractMethode.ID_JPA)
        })
@AttributeOverrides(value = {
        @AttributeOverride(column = @Column(name = ValeurSoilTexture.ID_JPA), name = "id")})
@SuppressWarnings({"rawtypes", "serial"})
public class ValeurSoilTexture extends ValeurSoil<ValeurSoilTexture> {

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_TEX = "texture";

    // Declaration des constantes
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_MTE = "mesureSoilTexture";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeur_texture_vstx";

    /**
     *
     */
    public static final String ID_JPA = "vstx_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureSoilTexture.ID_JPA, referencedColumnName = MesureSoilTexture.ID_JPA, nullable = false)
    MesureSoilTexture mesureSoilTexture;

    /**
     * @param texture
     * @param valeur
     * @param realNode
     * @param measureNumber
     * @param standardDeviation
     * @param qualityIndex
     * @param methode
     */
    public ValeurSoilTexture(MesureSoilTexture mesureSoilTexture, Float valeur, RealNode realNode, int measureNumber, float standardDeviation, int qualityIndex, AbstractMethode methode) {
        super(valeur, realNode, measureNumber, standardDeviation, qualityIndex, methode);
        this.mesureSoilTexture = mesureSoilTexture;
    }

    /**
     * @param texture
     * @param valeurs
     * @param realNode
     */
    public ValeurSoilTexture(MesureSoilTexture mesureSoilTexture, List<? extends IValeurQualitative> valeurs, RealNode realNode) {
        super(valeurs, realNode);
        this.mesureSoilTexture = mesureSoilTexture;
    }

    /**
     *
     */
    public ValeurSoilTexture() {
        super();
    }

    @Override
    public int compareTo(ValeurSoilTexture o) {
        int comparemesure = this.getMesureSoilTexture()
                .compareTo(o.getMesureSoilTexture());
        if (comparemesure != 0) {
            return comparemesure;
        }
        return this.getDatatypeVariableUnite().getVariable()
                .compareTo(o.getDatatypeVariableUnite().getVariable());
    }

    /**
     * @return
     */
    public MesureSoilTexture getMesureSoilTexture() {
        return mesureSoilTexture;
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValeurSoil<?> other = (ValeurSoil<?>) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.getId());
        return hash;
    }
}
