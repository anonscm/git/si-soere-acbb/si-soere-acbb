/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesolteneur;

import org.inra.ecoinfo.acbb.refdata.soil.listesoil.ListeSoil;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author ptcherniati
 */
@Entity(name = MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS)
@DiscriminatorValue(MethodeAnalyseComposant.MET_ANALYSE_COMPOSANTS)
public class MethodeAnalyseComposant extends ListeSoil {

    /**
     *
     */
    public static final String MET_ANALYSE_COMPOSANTS = "met_analyse_soil_element";

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = "met_anal_soil_element";
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

}
