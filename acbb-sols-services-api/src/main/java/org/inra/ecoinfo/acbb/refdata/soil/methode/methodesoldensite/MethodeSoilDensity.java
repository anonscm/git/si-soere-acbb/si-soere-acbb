/*
 *
 */
package org.inra.ecoinfo.acbb.refdata.soil.methode.methodesoldensite;

import org.inra.ecoinfo.acbb.refdata.AbstractMethode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class Bloc.
 */
@Entity
@Table(name = MethodeSoilDensity.NAME_ENTITY_JPA)
public class MethodeSoilDensity extends AbstractMethode {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "methode_sol_densite";

    /**
     *
     */
    public static final String ATRRIBUTE_COMMENT = "commentaires";

    /**
     * The Constant ID_JPA.
     */
    // LES CONSTANTES
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    String reference_donesol = "";
    String norme = "";

    /**
     *
     */
    public MethodeSoilDensity() {
        super();
    }

    // LES CONSTRUCTEURS

    /**
     * Instantiates a new bloc.
     *
     * @param definition
     * @param libelle
     * @param reference_donesol
     * @param numberId
     * @param norme
     */
    // GETTERS ET SETTERS
    public MethodeSoilDensity(String definition, String libelle, Long numberId, String reference_donesol, String norme) {
        super(libelle, definition, numberId);
        this.reference_donesol = reference_donesol;
        this.norme = norme;
    }

    /**
     * @param definition
     * @param reference_donesol
     * @param norme
     */
    public void update(String definition, String reference_donesol, String norme) {
        setDefinition(definition);
        this.reference_donesol = reference_donesol;
        this.norme = norme;
    }

    /**
     * @return
     */
    public String getReference_donesol() {
        return reference_donesol;
    }

    /**
     * @param reference_donesol
     */
    public void setReference_donesol(String reference_donesol) {
        this.reference_donesol = reference_donesol;
    }

    /**
     * @return
     */
    public String getNorme() {
        return norme;
    }

    /**
     * @param norme
     */
    public void setNorme(String norme) {
        this.norme = norme;
    }
}
